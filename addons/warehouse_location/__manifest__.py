{
    'name': 'Link between Location & Warehouse',
    'version': '0.2',
    'category': 'Warehouse',
    'license': "AGPL-3",
    'summary': "created from Current Stock Report for all Products in each Warehouse",
    'author': 'Itech Resources',
    'company': 'ItechResources',
    
    'depends': [
                'base',
                'stock',
                'sale',
                'purchase',
                'report_xlsx'
                ],
    'data': [
            'views/wh_location.xml',
            ],
    'images': ['static/description/banner.png'],
    'installable': True,
    'auto_install': False,
}
