# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from collections import namedtuple
import json
import time

from odoo import api, fields, models, _
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT
from odoo.tools.float_utils import float_compare
from odoo.addons.procurement.models import procurement
from odoo.exceptions import UserError

class PickingType(models.Model):
    _inherit = "stock.picking.type"

    serial = fields.Boolean('Serial No. Data')
    ask_task_info = fields.Boolean('Ask task info')
    attach_serial = fields.Boolean('Attach serial No.s')
    name_printing = fields.Text('Document Name for Printing')
    header_text = fields.Text('Document Header Text')
    so_required_in_delivery = fields.Boolean('SO Required in Delivery')
    footer_text = fields.Text('Document Footer Text')
    transaction_type_id = fields.Many2many('transaction.type', track_visibility="onchange")
    create_stock_location = fields.Boolean('Create Stock Location', track_visibility="onchange")
    party_changed = fields.Boolean('Party Can be Changed',track_visibility="onchange")
    challan_price_from = fields.Selection([('so','SO'),('product','Product')],'Challan Price From')
    price_multiplication_factor = fields.Float('Challan Price Multiplication Factor')  
    # -- stock picking for inter warehouse transfers
    inter_warehouse_transfer = fields.Boolean('Warehouse Transfer Type')
    
