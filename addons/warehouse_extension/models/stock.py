# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from datetime import datetime
from dateutil import relativedelta
import time
from collections import namedtuple

from odoo import api, fields, models, _
from odoo.addons import decimal_precision as dp
from odoo.addons.procurement.models import procurement
from odoo.exceptions import UserError
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT
from odoo.tools.float_utils import float_compare, float_round, float_is_zero


class StockPicking(models.Model):
    _inherit="stock.picking"





    def _prepare_pack_ops(self, quants, forced_qties):
        """ Prepare pack_operations, returns a list of dict to give at create """
        # TDE CLEANME: oh dear ...
        valid_quants = quants.filtered(lambda quant: quant.qty > 0)
        _Mapping = namedtuple('Mapping', ('product', 'package', 'owner', 'location', 'location_dst_id'))

        all_products = valid_quants.mapped('product_id') | self.env['product.product'].browse(p.id for p in forced_qties.keys()) | self.move_lines.mapped('product_id')
        computed_putaway_locations = dict(
            (product, self.location_dest_id.get_putaway_strategy(product) or self.location_dest_id.id) for product in all_products)

        product_to_uom = dict((product.id, product.uom_id) for product in all_products)
        picking_moves = self.move_lines.filtered(lambda move: move.state not in ('done', 'cancel'))
        for move in picking_moves:
            # If we encounter an UoM that is smaller than the default UoM or the one already chosen, use the new one instead.
            if move.product_uom != product_to_uom[move.product_id.id] and move.product_uom.factor > product_to_uom[move.product_id.id].factor:
                product_to_uom[move.product_id.id] = move.product_uom
        if len(picking_moves.mapped('location_id')) > 1:
            raise UserError(_('The source location must be the same for all the moves of the picking.'))
        if len(picking_moves.mapped('location_dest_id')) > 1:
            raise UserError(_('The destination location must be the same for all the moves of the picking.'))

        pack_operation_values = []
        # find the packages we can move as a whole, create pack operations and mark related quants as done
        top_lvl_packages = valid_quants._get_top_level_packages(computed_putaway_locations)
        for pack in top_lvl_packages:
            pack_quants = pack.get_content()
            pack_operation_values.append({
                'picking_id': self.id,
                'package_id': pack.id,
                'product_qty': 1.0,
                'location_id': pack.location_id.id,
                'location_dest_id': computed_putaway_locations[pack_quants[0].product_id],
                'owner_id': pack.owner_id.id,
            })
            valid_quants -= pack_quants

        # Go through all remaining reserved quants and group by product, package, owner, source location and dest location
        # Lots will go into pack operation lot object
        qtys_grouped = {}
        lots_grouped = {}
        for quant in valid_quants:
            key = _Mapping(quant.product_id, quant.package_id, quant.owner_id, quant.location_id, computed_putaway_locations[quant.product_id])
            qtys_grouped.setdefault(key, 0.0)
            qtys_grouped[key] += quant.qty
            if quant.product_id.tracking != 'none' and quant.lot_id:
                lots_grouped.setdefault(key, dict()).setdefault(quant.lot_id.id, 0.0)
                lots_grouped[key][quant.lot_id.id] += quant.qty
        # Do the same for the forced quantities (in cases of force_assign or incomming shipment for example)
        for product, qty in forced_qties.items():
            if qty <= 0.0:
                continue
            key = _Mapping(product, self.env['stock.quant.package'], self.owner_id, self.location_id, computed_putaway_locations[product])
            qtys_grouped.setdefault(key, 0.0)
            qtys_grouped[key] += qty

        # Create the necessary operations for the grouped quants and remaining qtys
        Uom = self.env['product.uom']
        product_id_to_vals = {}  # use it to create operations using the same order as the picking stock moves
        transfer_line_id = ''
        for mapping, qty in qtys_grouped.items():
            uom = product_to_uom[mapping.product.id]
	    for move in picking_moves:
	    	if move.product_id.id == mapping.product.id:
		    transfer_line_id = move.transfer_line_id.id
            val_dict = {
                'picking_id': self.id,
                'product_qty': mapping.product.uom_id._compute_quantity(qty, uom),
                'product_id': mapping.product.id,
                'package_id': mapping.package.id,
                'owner_id': mapping.owner.id,
                'location_id': mapping.location.id,
                'transfer_line_id':transfer_line_id,
                'location_dest_id': mapping.location_dst_id,
                'product_uom_id': uom.id,
                'pack_lot_ids': [
                    (0, 0, {
                        'lot_id': lot,
                        'qty': 0.0,
                        'qty_todo': mapping.product.uom_id._compute_quantity(lots_grouped[mapping][lot], uom)
                    }) for lot in lots_grouped.get(mapping, {}).keys()],
            }
            product_id_to_vals.setdefault(mapping.product.id, list()).append(val_dict)

        for move in self.move_lines.filtered(lambda move: move.state not in ('done', 'cancel')):
            values = product_id_to_vals.pop(move.product_id.id, [])
            pack_operation_values += values
        return pack_operation_values
    
    @api.multi
    def do_transfer(self):
        """ If no pack operation, we do simple action_done of the picking.
        Otherwise, do the pack operations. """
        # TDE CLEAN ME: reclean me, please
        self._create_lots_for_picking()

        no_pack_op_pickings = self.filtered(lambda picking: not picking.pack_operation_ids)
        no_pack_op_pickings.action_done()
        other_pickings = self - no_pack_op_pickings
        for picking in other_pickings:
            need_rereserve, all_op_processed = picking.picking_recompute_remaining_quantities()
            todo_moves = self.env['stock.move']
            toassign_moves = self.env['stock.move']

            # create extra moves in the picking (unexpected product moves coming from pack operations)
            if not all_op_processed:
                todo_moves |= picking._create_extra_moves()

            if need_rereserve or not all_op_processed:
                moves_reassign = any(x.origin_returned_move_id or x.move_orig_ids for x in picking.move_lines if x.state not in ['done', 'cancel'])
                if moves_reassign and picking.location_id.usage not in ("supplier", "production", "inventory"):
                    # unnecessary to assign other quants than those involved with pack operations as they will be unreserved anyways.
                    picking.with_context(reserve_only_ops=True, no_state_change=True).rereserve_quants(move_ids=picking.move_lines.ids)
                picking.do_recompute_remaining_quantities()

            # split move lines if needed
            if picking.barcode_transfer_id:
                for val in picking.pack_operation_product_ids:
                    if not val.transfer_line_id:
                         raise UserError(_('You can not validate it, because product line %s does not have transfer line id.')%(val.product_id.name))
            for move in picking.move_lines:
                rounding = move.product_id.uom_id.rounding
                remaining_qty = move.remaining_qty
                if move.state in ('done', 'cancel'):
                    # ignore stock moves cancelled or already done
                    continue
                elif move.state == 'draft':
                    toassign_moves |= move
                if float_compare(remaining_qty, 0,  precision_rounding=rounding) == 0:
                    if move.state in ('draft', 'assigned', 'confirmed'):
                        todo_moves |= move
                elif float_compare(remaining_qty, 0, precision_rounding=rounding) > 0 and float_compare(remaining_qty, move.product_qty, precision_rounding=rounding) < 0:
                    # TDE FIXME: shoudl probably return a move - check for no track key, by the way
                    new_move_id = move.split(remaining_qty)
                    new_move = self.env['stock.move'].with_context(mail_notrack=True).browse(new_move_id)
                    todo_moves |= move
                    # Assign move as it was assigned before
                    toassign_moves |= new_move

            # TDE FIXME: do_only_split does not seem used anymore
            if todo_moves and not self.env.context.get('do_only_split'):
                todo_moves.action_done()
            elif self.env.context.get('do_only_split'):
                picking = picking.with_context(split=todo_moves.ids)

            picking._create_backorder()
            if self.so_reference:
                stock_obj = self.env['stock.picking'].browse(self.so_reference.id)
                stock_obj.action_cancel()
        return True

  
    def action_qty(self):
        if self.pack_operation_ids:
            for val in self.pack_operation_ids:
                if val.product_id.tracking == 'none':
                    val.qty_done = val.product_qty
                else:
                    if val.pack_lot_ids:
                        for each in val.pack_lot_ids:
                            each.do_plus()
                self.code = True


    def action_party_changed(self):
        for each in self:
            if each.picking_type_id:
                each.update({'party_changed':each.picking_type_id.party_changed})



                        
    order_id = fields.Many2one('sale.order','SO#',track_visibility="onchange")
    purchase_order_id = fields.Many2one('purchase.order','PO#')
    transporter_id = fields.Many2one('res.partner','Transporter')
    customer_id = fields.Many2one('res.partner','Customer')
    vehicle_no = fields.Char('Vehicle no')

    party_changed  = fields.Boolean(compute='action_party_changed',method=True,type="boolean",string="Party changed")
    code = fields.Boolean('Code',track_visibility="onchange")
    so_reference = fields.Many2one('stock.picking','SO Reference')
    task_id = fields.Many2one('project.task','Task')
    transaction_type_id = fields.Many2one('transaction.type','Transaction Type',track_visibility="onchange")
    
class PackOperation(models.Model):
    _inherit = "stock.pack.operation"
    
#     tax_id = fields.Many2one('account.tax')
    transfer_line_id = fields.Many2one('barcode.transfer.line','Transfer Lines')
    serial_qty = fields.Float('Serial Qty')
                        
class StockMove(models.Model):
    _inherit = "stock.move"
    

    barcode = fields.Char('Barcode')
    product_barcode = fields.Char('Product Barcode')
    serial_id = fields.Many2one('stock.production.lot','Serial No')
    current_location_id = fields.Many2one('stock.location','Current Location')
#     barcode_serial_id = fields.Many2one('barcode.serial.line','Barcode Lines')
    transfer_line_id = fields.Many2one('barcode.transfer.line','Transfer Lines')
    
class Quant(models.Model):
    """ Quants are the smallest unit of stock physical instances """
    _inherit = "stock.quant"
    
        
    def quants_get_reservation(self, qty, move, pack_operation_id=False, lot_id=False, company_id=False, domain=None, preferred_domain_list=None):
        ''' This function tries to find quants for the given domain and move/ops, by trying to first limit
            the choice on the quants that match the first item of preferred_domain_list as well. But if the qty requested is not reached
            it tries to find the remaining quantity by looping on the preferred_domain_list (tries with the second item and so on).
            Make sure the quants aren't found twice => all the domains of preferred_domain_list should be orthogonal
        '''
        # TDE FIXME: clean me
        reservations = [(None, qty)]

        pack_operation = self.env['stock.pack.operation'].browse(pack_operation_id)
        location = pack_operation.location_id if pack_operation else move.location_id
        # don't look for quants in location that are of type production, supplier or inventory.
        if location.usage in ['inventory', 'production', 'supplier']:
            return reservations
            # return self._Reservation(reserved_quants, qty, qty, move, None)

        restrict_lot_id = lot_id if pack_operation else move.serial_id.id
        removal_strategy = move.get_removal_strategy()

        domain = self._quants_get_reservation_domain(
            move,
            pack_operation_id=pack_operation_id,
            lot_id=lot_id,
            company_id=company_id,
            initial_domain=domain)

        if not restrict_lot_id and not preferred_domain_list:
            meta_domains = [[]]
        elif restrict_lot_id and not preferred_domain_list:
            meta_domains = [[('lot_id', '=', restrict_lot_id)], [('lot_id', '=', False)]]
        elif restrict_lot_id and preferred_domain_list:
            lot_list = []
            no_lot_list = []
            for inner_domain in preferred_domain_list:
                lot_list.append(inner_domain + [('lot_id', '=', restrict_lot_id)])
                no_lot_list.append(inner_domain + [('lot_id', '=', False)])
            meta_domains = lot_list + no_lot_list
        else:
            meta_domains = preferred_domain_list

        res_qty = qty
        while (float_compare(res_qty, 0, precision_rounding=move.product_id.uom_id.rounding) and meta_domains):
            additional_domain = meta_domains.pop(0)
            reservations.pop()
            new_reservations = self._quants_get_reservation(
                res_qty, move,
                ops=pack_operation,
                domain=domain + additional_domain,
                removal_strategy=removal_strategy)
            for quant in new_reservations:
                if quant[0]:
                    res_qty -= quant[1]
            reservations += new_reservations
        return reservations
    
    def get_expected_date(self):
        
        for line in self:
            stock = []
            stock1 = []
            if line.history_ids:
                for val in line.history_ids:
                    stock.append(val.id)
            if stock:
                stock1 = list(set(stock))
            if stock1:
                for val1 in stock1:
                    stock_obj = self.env['stock.move'].browse(val1)
                    date = stock_obj.date 
                    line.update({'expected_date':date})
               
    
    expected_date = fields.Datetime(compute='get_expected_date',type="Datetime",method=True,string="Expected Date")
    
