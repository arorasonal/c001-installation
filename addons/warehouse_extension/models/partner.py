import datetime
from lxml import etree
import math
import pytz
import urlparse
from datetime import datetime, timedelta, date
from odoo import tools, api, fields, models, _
# from openerp.osv.expression import get_unaccent_wrapper
# from odoo.tools.translate import


class res_partner(models.Model):
    _inherit = 'res.partner'
    
    
    
    
    def action_existing(self):
        for each in self:
            if each.status == 'new':
                each.write({'status':'existing'})
                            
                
    def action_approved(self):
        for each in self:
            each.write({'accounts_approval':True,
                        'approved_date':datetime.today().strftime('%Y-%m-%d %H:%M:%S')})

    approved_date = fields.Datetime('Approved Date')
    status = fields.Selection([('new','New'),('existing','Existing')],'Status',default="new")
