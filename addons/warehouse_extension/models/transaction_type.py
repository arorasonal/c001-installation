import logging

from odoo import api, fields, models
from odoo import tools, _
import time
import math
from datetime import datetime,timedelta
from odoo.tools import DEFAULT_SERVER_DATE_FORMAT,DEFAULT_SERVER_DATETIME_FORMAT
from odoo.exceptions import ValidationError
from odoo.modules.module import get_module_resource


class transaction_type(models.Model):
    _name="transaction.type"
    _inherit = ['mail.thread', 'ir.needaction_mixin']
    _order="date DESC"
    
    date = fields.Datetime('Creation Date',default=lambda *a: datetime.now(),readonly=True,track_visibility="onchange")
    name = fields.Char('Name',required=True, track_visibility="onchange")
    description = fields.Text('Description',track_visibility="onchange")
    active = fields.Boolean('Active',default=True,track_visibility="onchange")
    picking_type_id = fields.Many2many('stock.picking.type',track_visibility="onchange")
    created_by = fields.Many2one('res.users','Created By',default=lambda self: self.env.user,readonly=True,track_visibility="onchange")
    _sql_constraints=[('unique_name','unique(name)','Name must be unique !')]
    
