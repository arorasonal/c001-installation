import logging

from odoo import api, fields, models
from odoo import tools, _
import time
import math
from time import gmtime, strftime
import odoo.addons.decimal_precision as dp
from datetime import datetime,timedelta,date
from odoo.tools import DEFAULT_SERVER_DATE_FORMAT,DEFAULT_SERVER_DATETIME_FORMAT
from odoo.exceptions import UserError, ValidationError
from odoo.modules.module import get_module_resource

# #################### #################### #################### ####################
# -- 19 Nov 2020 
# -- createing stock picking from BT , filling up partner_id & customer_id
# if order_id is known it means sales order is known
#   Set stock.picking.customer_id = so.delivery_address.parent_addres
#   Set stock_picking.partner_id = delivery address
# If order_id is not known it means sales order is not known
# If task_id is valid
#   set stock.picking.customer_id = task.delivery address.parent_address
#   Set stock_picking.partner_id = task.delivery address
# If task_id is null / blank
# if the destination location has a partner id associated with it
#   set stock.picking.customer_id = destination.location.partner id
# #################### #################### #################### ####################

class barcode_transfer(models.Model):
    _name="barcode.transfer"
    _inherit = ['mail.thread', 'ir.needaction_mixin']
    
    @api.model
    def create(self, vals):
        order = ' '
        seq = self.env['ir.sequence'].next_by_code('barcode_transfer')
        vals['name'] =  str(seq)
        barcode_transfer_id = super(barcode_transfer, self).create(vals)
        barcode_location_id = barcode_transfer_id.task_copy_id.installation_location_id
        if barcode_transfer_id.task_copy_id: 
            barcode_transfer_id.task_work_location_partner =  barcode_location_id.id
        return barcode_transfer_id

    # @api.multi
    # def write(self, vals):
    #     rec = super(barcode_transfer, self).write(vals)
    #     print('test++++++++++++++++')
    #     location_id = self.env['stock.location'].search([
    #         ('id','=',self.task_copy_id.installation_location_id.location_id.id)])
    #     print('location_id%%%%%%%%%%%%%%%', location_id, vals.get('task_copy_id.installation_location_id.location_id.parent_id.id'))
    #     if vals in ['task_copy_id']:
    #         print('test++++++++++++++++',vals['task_copy_id'],self.task_copy_id)
    #         # print('barcode_transfer_id',vals['task_copy_id'],vals['task_copy_id.installation_location_id'])
    #         self.task_work_location =  self.task_copy_id.installation_location_id.location_id
    #     # return barcode_transfer_id
    #     # pppp
    #     return rec
    
    @api.onchange('order_id')
    def _onchange_order_id(self):
        result={}
        picking_list = []
        if self.order_id:
            self.task_copy_id = ''
            if self.order_id.partner_shipping_id:
                if self.order_id.partner_shipping_id.sez == True:
                    self.sez = True
                picking = self.env['stock.picking'].search([('origin','=',self.order_id.name)])
                if picking:
                    for val in picking:
                        picking_list.append(val.id)
                        picking_id = val.id
                        result.update({'value':{'so_reference':picking_id,},
                                  'domain':{'so_reference':[('id','in',picking_list)]}
                                             
                                   })
        return result
    
    
    def action_import(self):
        s = 0
        location_id = ''
        for each in self:
            if each.source_barcode_transfer_id:
                if each.source_location_id:
                    if each.source_barcode_transfer_id.barcode_serial_lines:
                        for val2 in each.source_barcode_transfer_id.barcode_serial_lines:
                            if val2.serial_id:
                                quant = self.env['stock.quant'].search([('lot_id','=',val2.serial_id.id)])
                                if quant:
                                    for val3 in quant:
#                                         location_list.append(val3.location_id.id)
                                        location_id = val3.location_id.id
                            tax = []
                            if val2.tax_id:
                                for each1 in val2.tax_id:
                                    tax.append(each1.id)
                            self.env['barcode.serial.line'].create({'barcode_transfer_id':each.id,'barcode':val2.barcode,
                                                                    'rate':val2.rate,'order_line_id':val2.order_line_id.id,
                                                                    'so_product_id':val2.so_product_id.id,
                                                                    'product_barcode':val2.product_barcode,
                                                                    'product_id':val2.product_id.id,
                                                                    'source_barcode_serial_line_id':val2.id,
                                                                    'serial_id':val2.serial_id.id,'qty':val2.qty,
                                                                    'tax_id':[(6,0,tax)],'location_id':location_id,
                                                                    })
                            self.action_get_cost()
                            each.write({'code':True})
                else:
                    raise UserError(_('Please select the Source Location first.'))
            else:
                raise UserError(_('Please select the Source BT# first.'))
                        
                
                             
    barcode_transfer_id = fields.Many2one('barcode.transfer','Product Transfer',track_visibility="onchange")
    barcode = fields.Char('Barcode',track_visibility="onchange")
    tax_id = fields.Many2many('account.tax')
    so_product_id = fields.Many2one('product.product','SO Product')
    product_barcode = fields.Char('Product Barcode',track_visibility="onchange")
    product_id = fields.Many2one('product.product','Product',track_visibility="onchange")
    serial_id = fields.Many2one('stock.production.lot','Serial No',track_visibility="onchange")
    qty = fields.Float('Qty',default=1,track_visibility="onchange")
    picking_id = fields.Many2one('stock.picking','Delivery Note #',track_visibility="onchange")
    partner_id = fields.Many2one('res.partner','Partner',track_visibility="onchange")
    location_id = fields.Many2one('stock.location','Current Location',track_visibility="onchange")
    task_id = fields.Many2one('project.task','Task')
    task_copy_id = fields.Many2one('copy.project.task','Task')
    state = fields.Selection([('draft','Draft'),('validate','Validate'),('done','Done'),('cancel','Cancel')],'State',default="draft",track_visibility="onchange")
    ewo_required_in_delivery = fields.Boolean('EWO Required in Delivery')
    
                

            
           
#     def action_tax(self):
#         for each in self:
#             if each.tax_type == 'local':
#                 if each.barcode_serial_lines:
#                     for val in each.barcode_serial_lines:
#                         if val.tax_id.tax_type == 'local':
#                             val.tax_id = val.tax_id
#                         else:
#                             val.tax_id = val.tax_id.local_tax_id
#             elif each.tax_type == 'central':
#                 if each.barcode_serial_lines:
#                     for val in each.barcode_serial_lines:
#                         if val.tax_id.tax_type == 'central':
#                             val.tax_id = val.tax_id
#                         else:
#                             val.tax_id = val.tax_id.central_tax_id
#             else:
#                 raise UserError(_('Please select the tax type first.'))
                
                
            
    
    @api.onchange('picking_type_id')
    def _onchange_picking_type_id(self):
        result={}
        source_location_list = []
        destination_location_list = []
        source_location_id = ''
        destination_location_id = ''
        if self.picking_type_id:
            challan = self.picking_type_id.challan_price_from
            source_location_list.append(self.picking_type_id.default_location_src_id.id)
            destination_location_list.append(self.picking_type_id.default_location_dest_id.id)
            source_location_id = self.picking_type_id.default_location_src_id.id
            destination_location_id = self.picking_type_id.default_location_dest_id.id
            result.update({'value':{'source_location_id':source_location_id,
                                    'so_required_in_delivery':self.picking_type_id.so_required_in_delivery,
                                    'ewo_required_in_delivery':self.picking_type_id.ewo_required_in_delivery,
                                    'destination_location_id':destination_location_id,
                                    'challan_price_from':challan}
                               
                               })
        return result  
    
    def action_mark(self):
        product_list = []
        prioduct_list1 = []
        barcode_list = []
        tax = []
        tax1 = []
        product_ids = {}
        if self.state =="draft":
            for each in self:
                if each.barcode_serial_lines:
                    if each.order_id:
                        for val in each.barcode_serial_lines:
                            if not val.order_line_id:
                                raise UserError(_('Please fill the SO Product First.'))
                    for val in each.barcode_serial_lines:
                        if product_ids:
                            for key,value,in product_ids.items():
                                if key == val.product_id.id:
                                    if value != val.rate:
                                        raise UserError(_('There is different price for the product %s')%(val.product_id.name))
                                    else:
                                        product_ids[val.product_id.id] = val.rate
                                else:
                                    product_ids[val.product_id.id] = val.rate
                        else:
                            product_ids[val.product_id.id] = val.rate
                if each.picking_type_id.challan_price_from == 'so':
                    if each.barcode_serial_lines:
                        for val in each.barcode_serial_lines:
                            if not val.order_line_id:
                                val.rate = 0.0
                                val.tax_id = [(6,0,tax)]
                if each.tax_type == 'local':
                    if each.barcode_serial_lines:
                        for val in each.barcode_serial_lines:
                            if val.tax_id:
                                tax1 = []
                                for each44 in val.tax_id:
#                             /tax1 = []
                                    if each44.local_central == 'local':
#                                         for each44 in val.tax_id:
                                        tax1.append(each44.id)
                                    else:
                                        tax1.append(each44.local_tax_id.id)
                                val.tax_id = [(6,0,tax1)]
                elif each.tax_type == 'central':
                    if each.barcode_serial_lines:
                        for val in each.barcode_serial_lines:
                            if val.tax_id:
                                tax1 = []
                                for each44 in val.tax_id:
#                             /tax1 = []
                                    if each44.local_central == 'central':
#                                         for each44 in val.tax_id:
                                        tax1.append(each44.id)
                                    else:
                                        tax1.append(each44.central_tax_id.id)
                                val.tax_id = [(6,0,tax1)]
                else:
                    raise UserError(_('Please select the Local/Central Tax first.'))
            if self.barcode_serial_lines:
                for each in self.barcode_serial_lines:
                    if each.product_id.tracking != 'none':
                        if each.location_id.id != self.source_location_id.id:
                            raise UserError(_('The Source Location and Current Location is not matched for the product %s.') %
                                                    (each.product_id.name))
            if self.barcode_transfer_lines:
                for val in self.barcode_transfer_lines:
                    val.unlink()
            if self.barcode_serial_lines:
                for val1 in self.barcode_serial_lines:
                    if val1.barcode or val1.serial_id:
                        serial = self.env['barcode.serial.line'].search([('barcode','=',val1.barcode),('serial_id','=',val1.serial_id.id),('barcode_transfer_id','=',self.id)])
                        if serial:
                            if len(serial) > 1:
                               raise UserError(_('Barcode "%s" or Serial %s  is used in more than one line.') %
                                                    (val1.barcode,val1.serial_id.name))
            if self.barcode_serial_lines:
                for val in self.barcode_serial_lines:
                    product_list.append(val.product_id.id)
                if product_list:
                    product_list1 = list(set(product_list))
                if product_list1:
                    for val1 in product_list1:
                        product_obj = self.env['product.product'].browse(val1)
                        qty = 0.0
                        rate = 0.0
                        tax = []
                        serial = self.env['barcode.serial.line'].search([('barcode_transfer_id','=',self.id),('product_id','=',val1)])
                        if serial:
                            for val in serial:
                                if val.tax_id:
                                    qty = qty + val.qty
                                    rate = val.rate
                                    for each43 in val.tax_id:
                                        tax.append(each43.id)
#                                     if val.product_id.tracking == 'none':
#                                         qty = val.qty
#                                         rate = val.rate
#                                         for each43 in val.tax_id:
#                                             tax.append(each43.id)
#                                     else:
#                                         qty = qty + 1
#                                         rate = val.rate
#                                         for each43 in val.tax_id:
#                                             tax.append(each43.id)
                                else:
                                    qty = qty + val.qty
                                    rate = val.rate
#                                     if val.product_id.tracking == 'none':
#                                         qty = val.qty
#                                         rate = val.rate
#                                     else:
#                                         qty = qty + 1
#                                         rate = val.rate
#                         if tax:
                        p = self.env['barcode.transfer.line'].create({'barcode_transfer_id':self.id,'product_id':val1,'qty':qty,'rate':rate,
                                                                  'tax_id':[(6,0,tax)]})
#                         else:
#                             p = self.env['barcode.transfer.line'].create({'barcode_transfer_id':self.id,'product_id':val1,'qty':qty,'rate':rate,
#                                                                   })
                        if not p:
                            raise UserError(_('Barcode Transfer line for the product %s not created.')%(product_obj.name))

                        else:
                            serial1 = self.env['barcode.serial.line'].search([('barcode_transfer_id','=',self.id),('product_id','=',val1)])
                            if serial1:
                                for val45 in serial1:
                                    val45.write({'transfer_line_id':p.id})
            if self.barcode_serial_lines:
                for each in self.barcode_serial_lines:
                    each.write({'state':'validate'})
            self.write({'state':'validate'})
            
    def action_draft(self):
        if self.state == "validate":
            if self.tax_type == 'sez':
                self.write({'tax_code':True})
            if self.barcode_serial_lines:
                for each in self.barcode_serial_lines:
                    each.write({'state':'draft'})
            self.write({'state':'draft'})
            
    def action_cancel(self):
        if self.state == 'draft':
            if self.barcode_serial_lines:
                for each in self.barcode_serial_lines:
                    each.write({'state':'cancel'})
            self.write({'state':'cancel'})
    
    def action_validate(self):
        partner_id = False
        p = []
        s = ''
        tax = []
        picking_id = ''
        product_list = []
        product_list1 = []
        product_list2 = []
        transfer_list = []
        if self.barcode_transfer_lines:
            for each in self.barcode_transfer_lines:
                product_list1.append(each.product_id.id)
        if product_list1:
            product_list2 = list(set(product_list1))
        if self.barcode_transfer_lines:
            for val45 in self.barcode_transfer_lines:
                transfer_list.append(val45.id)
        if len(product_list2) != len(transfer_list):
            raise UserError(_('Please check Transfer line for all products are not created.'))
        if self.barcode_serial_lines:
            for val in self.barcode_serial_lines:
                if not val.transfer_line_id:
                    raise UserError(_('Barcode Serial line %s for Product %s not having Transfer Line.')%(val,val.product_id.name))
                tax = []
                if val.tax_id:
                    for val1 in val.tax_id:
                        tax.append(val1.id)
                product_list.append((0,False,{'product_id':val.product_id.id,'product_barcode':val.product_barcode,
                                              'product_uom_qty':val.qty,'product_uom':val.product_id.uom_id.id,'group_id':self.order_id.procurement_group_id.id,
                                              'name':val.product_id.name, 'barcode':val.barcode,'transfer_line_id':val.transfer_line_id.id,
                                             'serial_id':val.serial_id.id,'current_location_id':val.location_id.id}))
            if self.order_id:
                shipping_id = self.order_id.partner_shipping_id
            elif self.task_copy_id:
                shipping_id = self.task_copy_id.installation_location_id
            else:
                shipping_id = self.destination_location_id.customer_id

            picking_id = self.env['stock.picking'].create({'location_id':self.source_location_id.id,
                                                            'barcode_transfer_id':self.id,'partner_id':shipping_id.id,
                                                            'customer_id':shipping_id.parent_id.id if shipping_id.parent_id else shipping_id.id,
                                                            'order_id':self.order_id.id,'so_reference':self.so_reference.id,
                                                            'transaction_type_id':self.transaction_type_id.id,
                                                            'location_dest_id':self.destination_location_id.id,'origin':self.name,
                                                            'picking_type_id':self.picking_type_id.id,
                                                            'move_lines':product_list,'task_id':self.task_copy_id.task_id.id,
                                                            'note':self.task_copy_id.sudo().task_id.complaint_number
                                                            })
	  
            for each in self.barcode_serial_lines:
                each.write({'state':'done'})
        self.write({'picking_id':picking_id.id,'state':'done'})
        
    def action_get_cost(self):
        for each in self:
            if each.barcode_serial_lines:
                for val in each.barcode_serial_lines:
                    rate = 0.0
                    if val.product_id:
                        product_obj = self.env['product.template'].browse(val.product_id.product_tmpl_id.id)
                        if each.challan_price_from == 'product':
                            rate = val.product_id.standard_price * each.picking_type_id.price_multiplication_factor
                    val.write({'rate':rate})

    def attach_order_line(self):
        for each in self:
            count = 0
            order_line_id = ''
            if each.order_id:
                if each.order_id.order_line:
                    for val in each.order_id.order_line:
                        order_line_id = val.id
                        count = count + 1
                if count == 1:
                    if each.barcode_serial_lines:
                        for val1 in each.barcode_serial_lines:
                            val1.update({'order_line_id':order_line_id})




    source_location_id = fields.Many2one('stock.location','Source Location',track_visibility="onchange")
    destination_location_id = fields.Many2one('stock.location','Destination Location',track_visibility="onchange")
    name = fields.Char('Name',track_visibility="onchange")
    challan_price_from = fields.Selection([('so','SO'),('product','Product')],'challan Price From')
    so_reference = fields.Many2one('stock.picking','SO Reference')
    tax_type = fields.Selection([('local','Local'),('central','Central'),('sez','SEZ')],'Local/Central Tax',default='Local')
    order_id = fields.Many2one('sale.order','SO#',track_visibility="onchange")
    code = fields.Boolean('Code',track_visibility="onchange")
    tax_code = fields.Boolean('Tax Code',track_visibility="onchange")
    so_required_in_delivery = fields.Boolean('So Required in Delivery')
    # ewo_required_in_delivery = fields.Boolean('EWO Required in Delivery')
    source_barcode_transfer_id = fields.Many2one('barcode.transfer','Source BT#')
    transaction_type_id = fields.Many2one('transaction.type','Transaction Type',track_visibility="onchange")
    user_id = fields.Many2one('res.users','User',default=lambda self: self.env.user,track_visibility="onchange")
    picking_id = fields.Many2one('stock.picking','Refernce#',track_visibility="onchange")
    picking_type_id = fields.Many2one('stock.picking.type','Picking Type',track_visibility="onchange")
    ask_task_info_bt = fields.Boolean(string='Ask task info BT', related='picking_type_id.ask_task_info')
    state = fields.Selection([('draft','Draft'),('validate','Validate'),('done','Done'),('cancel','Cancel')],'State',default="draft",track_visibility="onchange")
    date = fields.Date('Date',default=lambda *a: datetime.now(),copy=False,track_visibility="onchange")
    barcode_transfer_lines = fields.One2many('barcode.transfer.line','barcode_transfer_id','Barcode Tramsfer Lines',ondelete='cascade',track_visibility="onchange")
    barcode_serial_lines = fields.One2many('barcode.serial.line','barcode_transfer_id','Barcode Serial Lines',ondelete='cascade',copy=True,track_visibility="onchange")
    sez = fields.Boolean('SEZ')
    task_work_location = fields.Many2one('stock.location',string='Work Location')
    task_work_location_partner = fields.Many2one('res.partner',string='Work Location')


    @api.onchange('task_copy_id')
    def onchange_task_id(self):
        # task = self.task_id.sudo()
        self.task_work_location_partner = False
        # if self.task_copy_id.installation_location_id.company_type == 'company':
        if self.task_copy_id.installation_location_id.id:
            # print('self.task_copy_id.installation_location_id',self.task_copy_id.installation_location_id)
            self.task_work_location_partner = self.task_copy_id.installation_location_id.id
        # else:
        #     if self.task_copy_id.installation_location_id.parent_id.location_id:
        #         print('self.task_copy_id.installation_location_id',self.task_copy_id.installation_location_id.parent_id.id)
        #         self.task_work_location_partner = self.task_copy_id.installation_location_id.parent_id.location_id.id
    
class barcode_transfer_line(models.Model):
    _name="barcode.transfer.line"
    _inherit = ['mail.thread', 'ir.needaction_mixin']
    
    @api.onchange('product_id')
    def _onchange_product_id(self):
        if self.product_id.tracking == 'serial':
            self.serial = True
            
    @api.depends('rate','qty','tax_id')
    def get_tax_amount(self):
        for each in self:
            cost = 0.0
            tax = 0.0
            tax_amount = 0.0
            if each.tax_id:
                for val1 in each.tax_id:
                    if val1.amount_type == 'group':
                        if val1.children_tax_ids:
                            for val2 in val1.children_tax_ids:
                                tax = tax + val2.amount
                        tax_amount =  (each.rate * each.qty * tax)/100
                    else:
                        tax_amount =  (each.rate * each.qty * val1.amount)/100
                each.update({'tax_amount':tax_amount})
                
                
    @api.depends('rate','qty','tax_amount')
    def _get_amount(self):
        for each in self:
            tax_amount = 0.0
            amount = 0.0
            amount = (each.rate * each.qty) + each.tax_amount
            each.update({'amount':amount})
            
    
    barcode_transfer_id = fields.Many2one('barcode.transfer','Barcode Transfer',track_visibility="onchange")
    product_id = fields.Many2one('product.product','Product',track_visibility="onchange")
    qty  = fields.Float('Qty',track_visibility="onchange")
    serial = fields.Boolean('Serial',track_visibility="onchange")
    tax_id = fields.Many2many('account.tax')
    rate  = fields.Float('Unit Price',digits=(12,2))
    tax_amount = fields.Float(compute='get_tax_amount',digits=(12,2),string="Tax Amount",type="Float",track_visibility="onchange")
    amount = fields.Float(compute='_get_amount',type="float",digits=(12,2),method=True,string="Amount")
    
class barcode_serial_line(models.Model):
    _name="barcode.serial.line"
    
    @api.onchange('barcode')
    def _onchange_barcode(self):
        result={}
        product_list = []
        serial_list = []
        location_list = []
        partner_list = []
        picking_list = []
        location_id = ''
        partner_id = ''
        picking_id = ''
        if self.barcode:
            serial = self.env['stock.production.lot'].search([('actual_barcode','=',self.barcode),('quant_ids.location_id','!=','Virtual Locations/Alteration')])
            if serial:
                for val in serial:
                    product_list.append(val.product_id.id)
                    serial_list.append(val.id)
                    quant = self.env['stock.quant'].search([('product_id','=',val.product_id.id),('lot_id','=',val.id)])
                    if quant:
                        for val2 in quant:
                            if val2.location_id.name != 'Virtual Locations/Alteration':
                                location_list.append(val2.location_id.id)
                                location_id = val2.location_id.id
                                if val2.location_id.usage == 'customer':
                                    if val2.history_ids:
                                        for val3 in val2.history_ids:
                                            if val3.location_dest_id == val2.location_id:
                                                picking_list.append(val3.picking_id.id)
                                                partner_list.append(val3.picking_id.partner_id.id)
                                                partner_id = val3.picking_id.partner_id.id
                                                picking_id = val3.picking_id.id

                    result.update({'value':{'product_id':val.product_id.id,'serial_id':val.id,
                                            'location_id':location_id,'qty':val.product_qty,
                                            'picking_id':picking_id,'partner_id':partner_id},
                               'domain':{
                                         
                                         'location_id':[('id','in',location_list)],
                                         'picking_id':[('id','in',picking_list)],
                                         'partner_id':[('id','in',partner_list)]}
                               })
        return result  
                    
    @api.onchange('serial_id')
    def _onchange_serial_id(self):
        result={}
        product_list = []
        serial_list = []
        location_list = []
        partner_list = []
        picking_list = []
        location_id = ''
        partner_id = ''
        picking_id = ''
        product_barcode = ''
        if self.serial_id:
            product_barcode = self.serial_id.actual_barcode
            product_list.append(self.serial_id.product_id.id)
            quant = self.env['stock.quant'].search([('lot_id','=',self.serial_id.id)])
            if quant:
                for val2 in quant:
                    location_list.append(val2.location_id.id)
                    location_id = val2.location_id.id
                    if val2.location_id.usage == 'customer':
                        if val2.history_ids:
                            for val3 in val2.history_ids:
                                if val3.location_dest_id == val2.location_id:
                                    picking_list.append(val3.picking_id.id)
                                    partner_list.append(val3.picking_id.partner_id.id)
                                    partner_id = val3.picking_id.partner_id.id
                                    picking_id = val3.picking_id.id
                    result.update({'value':{'product_id':self.serial_id.product_id.id,
                                            'barcode':product_barcode,
                                            'location_id':location_id,'qty':self.serial_id.product_qty,
                                            'picking_id':picking_id,'partner_id':partner_id},
                               'domain':{
                                         'location_id':[('id','in',location_list)],
                                         'picking_id':[('id','in',picking_list)],
                                         'partner_id':[('id','in',partner_list)]}
                               })
        return result  
    
    @api.onchange('product_id')
    def onchange_product_id(self):
        result = {}
        tax = []
        rate = 0.0
        product_barcode = ''
        if self.product_id:
            product_obj = self.env['product.template'].browse(self.product_id.product_tmpl_id.id)
            product_barcode = product_obj.barcode
            if self.barcode_transfer_id.challan_price_from == 'product':
                rate = self.product_id.standard_price * self.barcode_transfer_id.picking_type_id.price_multiplication_factor
                if product_obj.taxes_id:
                    for val in product_obj.taxes_id:
                        tax.append(val.id)
            if tax:
                result.update({'value':{'product_barcode':product_barcode,'tax_id':[(6,0,tax)],'rate':rate}})
            else:
                result.update({'value':{'product_barcode':product_barcode }})
        return result
    
    @api.onchange('product_barcode')
    def onchange_product_barcode(self):
        result={}
        product_list = []
        product_id = ''
        if self.product_barcode:
            product = self.env['product.template'].search([('barcode','=',self.product_barcode)])
            for val in product:
                prod = self.env['product.product'].search([('product_tmpl_id','=',val.id)])
                for each in prod:
                    product_list.append(each.id)
                    product_id = each.id
                result.update({'value':{'product_id':product_id},
                               'domain':{}
                               })
        return result
    
    @api.depends('rate','qty','tax_id')
    def get_tax_amount(self):
        for each in self:
            cost = 0.0
            tax = 0.0
            tax_amount = 0.0
            if each.tax_id:
                for val1 in each.tax_id:
                    if val1.amount_type == 'group':
                        if val1.children_tax_ids:
                            for val2 in val1.children_tax_ids:
                                tax = tax + val2.amount
                        tax_amount =  (each.rate * each.qty * tax)/100
                    else:
                        tax_amount =  (each.rate * each.qty * val1.amount)/100
                each.update({'tax_amount':tax_amount})
                
                
    @api.depends('rate','qty','tax_amount')
    def _get_amount(self):
        for each in self:
            tax_amount = 0.0
            amount = 0.0
            amount = (each.rate * each.qty) + each.tax_amount
            each.update({'amount':amount})
    
#     @api.depends('rate','qty')
#     def _get_amount(self):
#         price = 0.0
#         for each in self:
#             
#             price = each.rate * each.qty
#             each.update({'amount':price})
            
    @api.onchange('order_line_id','product_id')
    def _onchange_order_line_id(self):
        for each in self:
            tax = []
            if each.barcode_transfer_id.challan_price_from == 'so':
                if each.order_line_id:
                    for val in each.order_line_id.tax_id:
                        tax.append(val.id)
                    each.rate = (each.order_line_id.price_unit * each.barcode_transfer_id.picking_type_id.price_multiplication_factor)
                    each.tax_id = [(6,0,tax)]
#             else:
#                 product_obj = self.env['product.template'].browse(self.product_id.product_tmpl_id.id)
#                 if product_obj.taxes_id:
#                     for val in product_obj.taxes_id:
#                         tax.append(val.id)   
#                 each.tax_id = [(6,0,tax)] 
                
                
    barcode_transfer_id = fields.Many2one('barcode.transfer','Product Transfer',track_visibility="onchange")
    barcode = fields.Char('Barcode',track_visibility="onchange")
    tax_id = fields.Many2many('account.tax')
#     order_id = fields.Many2one('sale.order','SO#')
    rate  = fields.Float('Unit Price')
    order_line_id = fields.Many2one('sale.order.line','SO Product')
    transfer_line_id = fields.Many2one('barcode.transfer.line','Transfer Line')
    tax_amount = fields.Float(compute='get_tax_amount',string="Tax Amount",type="Float",track_visibility="onchange")
    amount = fields.Float(compute='_get_amount',type="float",method=True,string="Amount")
    so_product_id = fields.Many2one('product.product','SO Product')
    product_barcode = fields.Char('Product Barcode',track_visibility="onchange")
    product_id = fields.Many2one('product.product','Product',track_visibility="onchange")
    serial_id = fields.Many2one('stock.production.lot','Serial No',track_visibility="onchange")
    qty = fields.Float('Qty',default=1,track_visibility="onchange")
    picking_id = fields.Many2one('stock.picking','Delivery Note #',track_visibility="onchange")
    partner_id = fields.Many2one('res.partner','Partner',track_visibility="onchange")
    location_id = fields.Many2one('stock.location','Current Location',track_visibility="onchange")
    source_barcode_serial_line_id = fields.Many2one('barcode.serial.line','BArcode Serial Line#')
    state = fields.Selection([('draft','Draft'),('validate','Validate'),('done','Done'),('cancel','Cancel')],'State',default="draft",track_visibility="onchange")
    
    
class ResPartner(models.Model):
    _inherit = 'res.partner'

    @api.model
    def name_search(self, name, args=None, operator='ilike',limit=100):
        if 'barcode_transfer_id_context' in self._context and self._context.get('barcode_transfer_id_context') not in [False,None,'None','False']:
            partner_id = self.env['res.partner'].search([('id', '=', self._context.get('barcode_transfer_id_context'))])
            child_ids = [l for l in partner_id.child_ids]
            partners =  partner_id + child_ids
            args = [('id', 'in', partners.ids  )]
        return super(ResPartner, self).name_search(name, args, operator=operator, limit=limit)