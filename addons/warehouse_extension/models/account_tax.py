import time
import math

from odoo.osv import expression
from odoo.tools.float_utils import float_round as round
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT
from odoo.exceptions import UserError, ValidationError
from odoo import api, fields, models, _



class AccountTax(models.Model):
    _inherit = 'account.tax'
    _description = 'Tax'

    @api.onchange('local_central')
    def _onchange_local_central(self):
        if self.local_central:
            self.code_tax = True

    local_central = fields.Selection([('local','Local'),('central','Central')],'Local/Central Tax')
    code_tax = fields.Boolean('Code Tax')
    local_tax_id = fields.Many2one('account.tax','Local Tax')
    central_tax_id = fields.Many2one('account.tax','Central Tax')
    sez_tax_id = fields.Many2one('account.tax','SEZ Tax')
    sez = fields.Boolean('SEZ')
