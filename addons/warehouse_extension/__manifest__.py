{
    "name": "Warehouse Extension",
    "version": "1.0",
    'website': 'http://www.diracerp.in',
    'images': [ ],
    "depends": ['web','base','product','stock','project','sale'],
    "author": "Dirac ERP",
    "category": "Stock",
#     'sequence': 16,
    "description": """
    This module extending the CJPL warehouse functionalities. 
    """,
    'data': [
            'security/security_view.xml',
            'security/ir.model.access.csv',
            'view/sequence_view.xml',
            'view/product_view.xml',
            'view/stock_picking_type_view.xml',
            'view/barcode_transfer_view.xml',
            'view/stock_production_lot_view.xml',
            'view/stock_move_view.xml',
            'view/transaction_type_view.xml',
            'view/report_stock_picking.xml',
            'view/account_tax_view.xml',
            'view/res_partner_view.xml',
            # 'view/project_task_view.xml',

#           
                    ],
    'demo_xml': [],
     'js': [
            'static/src/js/view_list.js'
#            'static/src/js/view_list.js'
#     'static/src/js/subst.js'
    ],
    'css': [
#             'static/src/css/sample_kanban.css'
            ],
#     'pre_init_hook': 'pre_init_hook',
#     'post_init_hook': 'post_init_hook',
    'installable': True,
    'active': False,
    'auto_install': False,
#    'certificate': 'certificate',
}
