from odoo import api, fields, models, tools
# from odoo import netsvc
# from odoo import pooler
from odoo.tools.translate import _
from datetime import datetime, date
# from json import dumps
from dateutil.relativedelta import relativedelta
from odoo.exceptions import UserError, Warning, RedirectWarning
import time
import datetime


class hr_job(models.Model):

    _inherit = 'hr.job'

    max_employees = fields.Integer('Maximum Number of Employees')
    max_employees_fuzz = fields.Integer('Expected Turnover',
                                        help="Recruitment module will allow you to \
                                              create this number of additional applicants and \
                                              contracts above the maximum value. Use this \
                                              number as a buffer to have additional \
                                              employees on hand to cover employee turnover.")

    @api.multi
    def write(self, vals):
        value = vals.get('no_of_recruitment', False)
        if value and value < 0:
            vals['no_of_recruitment'] = 0
        return super(hr_job, self).write(vals)


# class hr_applicant(models.Model):

#     _name = 'hr.applicant'
#     _inherit = 'hr.applicant'

#     @api.model
#     def create(self, vals):
#         if vals.get('job_id', False):
#             data = self.env['hr.job'].read(vals['job_id'],
#                                            ['max_employees', 'no_of_employee', 'state',
#                                             'max_employees_fuzz'])
#             if data.get('state', False):
#                 if data['state'] != 'recruit' and int(data['no_of_employee']) >= (int(data['max_employees']) + data['max_employees_fuzz']):
#                     raise UserError('Job not open for recruitment!'
#                                     'You may not register applicants for jobs that are not recruiting.')
#         return super(hr_applicant, self).create(vals)

#     job_max_pay = fields.Float('Job Group Pay (Max)')
#     job_min_pay = fields.Float('Job Group Pay (Min)')


class HrRecruitmentRequest(models.Model):

    _name = 'hr.recruitment.request'
    _rec_name = 'request_no'
    _description = 'Request for recruitment of additional personnel'
    _inherit = 'mail.thread'

    def _default_user_id(self):
        return self.env.context.get('_default_user_id')

    # office_location = fields.Many2one('hr.employee.service.area', 'Service
    # Area')
    reason_recruitment = fields.Selection([('add_staff', 'Additional Staff'),
                                           ('replace', 'Replacement'),
                                           ('new_pos', 'New Postion')],
                                          'Recruitment Reason', default='add_staff')
    request_no = fields.Char('Recruitment Request', copy=False)
    name = fields.Char('Description', size=64)
    user_id = fields.Many2one('res.users', 'Requested By',
                              required=True, default=lambda self: self.env.user)
    department_id = fields.Many2one('hr.department',
                                    'Department',
                                    related='job_id.department_id', store=True)
    sub_dept_id = fields.Many2one('hr.sub.department', 'Sub Department')
    job_id = fields.Many2one('hr.job', 'Vacant Job Title')
    number = fields.Integer('Number to Recruit', required=True)
    project_id = fields.Many2one('project.project', 'Project')
    city = fields.Char('City')
    state_id = fields.Many2one('res.country.state', "State")
    close_date = fields.Datetime()
    #                                  'no_of_employee',
    #                                  type='Integer',
    #                                  string="Current number of position holders",
    #                                  readonly=True)
    # max_number = fields.Related('job_id', 'max_employees',
    #                              type='Integer',
    #                              string="Maximum Number of Employees",
    #                              readonly=True)
    reason = fields.Text('Reason for Request')
    refused_by = fields.Many2one('res.users',
                                 "Declined By",
                                 readonly=True)
    note = fields.Text("Reson For Request")
    # role = fields.Many2one('hr.role', string="Role", required="True")
    # request_date = fields.Datetime("Request Date", default=fields.Date.now)
    job_grade = fields.Integer('Job Grade')
    job_grade_pay = fields.Integer('Job Grade Pay')
    proposed_position_pay = fields.Integer('Proposed Position Pay')
    additional_reason = fields.Text('Reason for Additional Staff')
    replacement_reason = fields.Text('Justification for Replacement')
    position_reason = fields.Text(' Justification for New Position')
    exiting_emp_name = fields.Many2one('res.users', 'Name of Exiting Employee')
    exit_date = fields.Date('Exit Date')
    reason_for_departure = fields.Char('Reason for Departure')
    position_reporting_to = fields.Many2one(
        'res.users', 'Position Reporting to')
    emp_type = fields.Selection(
        [
            ('permanent', 'Permanent'),
            ('Temporary', 'Temporary')
        ], 'Employment Type')
    duration = fields.Float('Duration (Months)')
    emp_req_date = fields.Date('Employee Required Date')
    current_head_count = fields.Integer('Current Head Count in Department')
    approved_head_count = fields.Integer('Approved Head Count')
    full_year_budget = fields.Float('Full Year Budget')
    date_new_emp_required = fields.Date("Date when new Employee is required")
    budget_balance_date = fields.Float('Budget Balance to Date')
    number_existing_staff = fields.Char("Number of existing staff")
    state = fields.Selection(
        [
            ('draft', 'Draft'),
            ('refused', 'Rejected'),
            ('finance_approval', 'Awaiting HR Head Approval'),
            # ('unit_approval', 'Awaiting Unit Head'),
            # ('hr_approval', 'Awaiting HR Approval'),
            # ('recruitment', 'In Recruitment'),
            ('ceo_approval', 'Awaiting CEO/MD Approval'),
            ('approved', 'Approved'),
            ('done', 'Done'),
            ('cancel', 'Cancelled')
        ],
        'State', readonly=True, track_visibility='always', default='draft',)
    person_previous_worked = fields.Selection(
        [
            ('yes', "Yes"),
            ('no', "No")
        ], "Has this person previously worked for Radio Africa?")
    exit_resion = fields.Char("What was the reason for exit")
    # office_location = fields.Many2one('hr.employee.service.area',
    #                                   'Service Area')
    department_id = fields.Many2one('hr.department', 'Department')
    job_id = fields.Many2one('hr.job', 'Vacant Job Title')
    min = fields.Float("Proposed Annual Salary ")
    max = fields.Float("maximaum")
    qualification_desired = fields.Text("Qualification Desired")
    qualification_mandatory = fields.Text("Qualification Mandatory")
    new_position_revenue = fields.Selection(
        [
            ('revenue', 'Revenue generation'),
            ('support', 'Support')
        ], string="New Position Objective")
    expected_volume = fields.Float("Expected Business Volume")
    position_in_budget = fields.Char("Budget Position")
    manager_id = fields.Many2one('hr.employee', string="Manager")
    approval_rejection_remark = fields.Text("Approval/Rejaction remarks")
    # unit_head = fields.Many2one('hr.employee', string="Unit Head")
    # business_unit = fields.Char('Business Unit')

    _order = 'department_id, job_id'

    @api.multi
    def unlink(self):
        for move in self:
            if move.state != 'draft':
                raise Warning(_('You can only delete in Draft state'))
        return models.Model.unlink(self)

    def condition_exception(self):

        for req in self:
            if req.number + req.job_id.expected_employees > req.job_id.max_employees:
                return True
        return False

    def _state(self, state):
        self.write({'state': state})
        return True
        # commented by poonam #
        # job_obj = self.env['hr.job']

        # for req in self:
        #     if self.state == 'recruitment':
        #         job_obj.write(req.job_id.id, {'no_of_recruitment': req.number})
        #         job_obj.job_recruitement([req.job_id.id])
        #     elif self.state in ['done', 'cancel']:
        #         job_obj.job_open([req.job_id.id])

    def _state_subscribe_users(self, state):

        imd_obj = self.env['ir.model.data']
        return self._state(state)

    def signal_confirm(self):
        # may be needed###################
        # ir_model_data = self.env['ir.model.data']
        # template_id = ir_model_data.get_object_reference('hr_labour_recruitment',
        #                                                  'email_template_approval_notification_ceo')[1]
        # if template_id:
        #   self.env['email.template'].send_mail(template_id)
        template_id = self.env.ref(
            'hr_recruitment_request.email_template_approval_notification_ceo')
        users = self.env['res.users'].search([])
        mail = ''
        for user in users:
            if user.has_group('hr_employee_register.group_base_ceo_md'):
                mail += user.login + ','
        if template_id:
            # template_id.email_to = mail
            # template_id.email_from = 'erp.alerts@traqmatix.com'
            template_id.send_mail(self.id, force_send=True)
        self.write({
            'state': 'ceo_approval'
        })
        return True

    def signal_finance_approve(self):
        record = self
        for rec in self:
            if not rec.request_no:
                seq = self.env['ir.sequence'].next_by_code(
                    'hr.recruitment.request') or '/'
                self.write({
                    'request_no': seq
                })
        template_id = self.env.ref(
            'hr_recruitment_request.email_template_approval_notification')
        users = self.env['res.users'].search([])
        mail = ''
        for user in users:
            if user.has_group('project_financial_security.group_account_head'):
                mail += user.login + ','
        if template_id:
            # template_id.email_to = mail
            # template_id.email_from = 'erp.alerts@traqmatix.com'
            template_id.send_mail(self.id, force_send=True)
        self.write({
            'state': 'finance_approval',
        })
        return True

    def reset_to_draft(self):
        return self._state_subscribe_users('draft')

    def reset_to_approved(self):
        return self._state_subscribe_users('approved')

    def done_button(self):
        self.write({'state': 'done', 'close_date': fields.Datetime.now()})
        return True

    def cancel_button(self):
        if self.state == 'approved':
            if self.approval_rejection_remark is False:
                raise UserError(
                    'Kindly enter reason for cancel before cancel the request')
            self.write({'state': 'cancel', 'close_date': fields.Datetime.now()
                        })

    def signal_ceo_approve(self):
        self.write({'state': 'approved'})
        return True

    def signal_refuse(self):
        line = self
        user_id = self.env['res.users']
        if line.approval_rejection_remark is False:
            raise UserError(
                'Kindly enter reason for rejection before rejecting the request')
        else:
            self.write({'state': 'refused', 'refused_by': self.env.user.id})
        return True

    def ceo_refuse(self):
        line = self
        user_id = self.env['res.users']
        if line.approval_rejection_remark is False:
            raise UserError(
                'Kindly enter reason for rejection before rejecting the request')
        else:
            self.write({'state': 'refused', 'refused_by': self.env.user.id})
        return True
