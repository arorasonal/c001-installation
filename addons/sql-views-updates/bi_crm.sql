SELECT
    a.create_date::date AS lead_start_date,
    a.name AS lead,
    a.code AS lead_number,
    b.name AS team,
    c.name AS stage,
    a.type,
    d.name AS source,
    e.name AS campaign,
    f.name AS medium,
    a.date_deadline,
    a.mobile,
    a.email_from,
    a.contact_name,
    rp.name AS sales_person
FROM
    crm_lead a
    LEFT JOIN crm_team b ON b.id = a.team_id
    LEFT JOIN crm_stage c ON c.id = a.stage_id
    LEFT JOIN utm_source d ON d.id = a.source_id
    LEFT JOIN utm_campaign e ON e.id = a.campaign_id
    LEFT JOIN utm_medium f ON f.id = a.medium_id
    JOIN res_users ru ON ru.id = a.user_id
    JOIN res_partner rp ON ru.partner_id = rp.id
WHERE
    a.active
    AND a.create_date::date >= date('2020-04-01')
    AND rp.active
ORDER BY
    a.create_date DESC
