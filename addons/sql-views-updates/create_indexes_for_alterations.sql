/*
psql -U username -d myDataBase -a -f myFile
*/

-- Index: item_category_template_item_alteration_id_idx

-- DROP INDEX public.item_category_template_item_alteration_id_idx;

CREATE INDEX IF NOT EXISTS item_category_template_item_alteration_id_idx
    ON public.item_category_template USING btree
    (item_alteration_id ASC NULLS LAST)
    TABLESPACE pg_default;

-- Index: stock_production_lot_actual_barcode_ids_idx

-- DROP INDEX public.stock_production_lot_actual_barcode_ids_idx;

CREATE INDEX IF NOT EXISTS stock_production_lot_actual_barcode_ids_idx
    ON public.stock_production_lot USING btree
    (actual_barcode COLLATE pg_catalog."default" ASC NULLS LAST)
    TABLESPACE pg_default;

-- Index: item_alteration_serial_no_idx

-- DROP INDEX public.item_alteration_serial_no_idx;

CREATE INDEX IF NOT EXISTS item_alteration_serial_no_idx
    ON public.item_alteration USING btree
    (serial_no ASC NULLS LAST)
    TABLESPACE pg_default;

-- Index: stock_quant_location_id_idx

-- DROP INDEX public.stock_quant_location_id_idx;

CREATE INDEX IF NOT EXISTS stock_quant_location_id_idx
    ON public.stock_quant USING btree
    (location_id ASC NULLS LAST)
    TABLESPACE pg_default;

-- Index: stock_quant_lot_id_idx

-- DROP INDEX public.stock_quant_lot_id_idx;

CREATE INDEX IF NOT EXISTS stock_quant_lot_id_idx
    ON public.stock_quant USING btree
    (lot_id ASC NULLS LAST)
    TABLESPACE pg_default;

-- Index: wiz_add_product_product_id_idx

-- DROP INDEX public.wiz_add_product_product_id_idx;

CREATE INDEX IF NOT EXISTS wiz_add_product_product_id_idx
    ON public.wiz_add_product USING btree
    (product_id ASC NULLS LAST)
    TABLESPACE pg_default;

-- Index: conversion_non_alterable_category_template_conversion_non_alterable_id_idx

-- DROP INDEX public.conversion_non_alterable_category_template_conversion_non_alterable_id_idx;

CREATE INDEX IF NOT EXISTS conversion_non_alterable_category_template_conversion_non_alterable_id_idx
    ON public.conversion_non_alterable_category_template USING btree
    (conversion_non_alterable_id ASC NULLS LAST)
    TABLESPACE pg_default;


-- Index: conversion_non_alterable_serial_no_idx

-- DROP INDEX public.conversion_non_alterable_serial_no_idx;

CREATE INDEX IF NOT EXISTS conversion_non_alterable_serial_no_idx
    ON public.conversion_non_alterable USING btree
    (serial_no ASC NULLS LAST)
    TABLESPACE pg_default;

-- Index: non_item_category_template_non_item_alteration_id_idx

-- DROP INDEX public.non_item_category_template_non_item_alteration_id_idx;

CREATE INDEX IF NOT EXISTS non_item_category_template_non_item_alteration_id_idx
    ON public.non_item_category_template USING btree
    (non_item_alteration_id ASC NULLS LAST)
    TABLESPACE pg_default;

-- Index: non_item_alteration_serial_no_idx

-- DROP INDEX public.non_item_alteration_serial_no_idx;

CREATE INDEX IF NOT EXISTS non_item_alteration_serial_no_idx
    ON public.non_item_alteration USING btree
    (serial_no ASC NULLS LAST)
    TABLESPACE pg_default;
