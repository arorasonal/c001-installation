Customization Invoice
------------------------

Create Master of Account Invoice Item Mapping
----------------------------------

Create a new master of Account Invoice Item Mapping.Name is Account Invoice Item Mapping.Add following fields.
1. Sales Type -required
2. GST Type - required
3. Gst Taxes - required
4. Product - not required

In Invoice 
------------

1. In customer invoice when we click on validate button. In invoice line tax select according to master of taxes.
2. First we will check combination of sale type, gst tax and product.
3. If product will not match. so we will match accordingly to sale type and gst taxes.
4. Product Search based on sale type category in sale order, invoice and contract
