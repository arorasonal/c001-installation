# -*- coding: utf-8 -*-
{
    'name': "Mapping Invoice Item",

    'summary': """
        Mapping Invoice Item Line""",

    'description': """
         Mapping Invoice
    """,

    'author': "Developer",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/master/odoo/addons/base/module/module_data.xml
    # for the full list
    'category': 'Uncategorized',
    'version': '0.2',

    # any module necessary for this one to work correctly
    'depends': ['account','sale_order_type','invoice_cjpl'],

    # always loaded
    'data': [
        'views/master_invoice_mapping.xml',
        'security/ir.model.access.csv',
        'views/sale_order_type.xml',
        # 'views/account_invoice_view.xml',
    ],
    # only loaded in demonstration mode
}