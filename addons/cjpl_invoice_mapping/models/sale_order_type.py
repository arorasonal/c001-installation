# -*- coding: utf-8 -*-

from odoo import models, fields, api
from datetime import datetime, timedelta
from odoo.exceptions import ValidationError,UserError
from datetime import date

class SaleOrderType(models.Model):
    _inherit = 'sale.order.type'

    valid_product_category = fields.Many2many('product.category', 'categ_id', 'sale_type_id', string='Product Category')

class product_product(models.Model):
    _inherit = 'product.product'

    @api.model
    def name_search(self, name='', args=None, operator='ilike', limit=100):
        if self._context.get('sale_type_id') or self._context.get('sale_order_id'):
            product_categ = []
            if self._context.get('sale_order_id'):
                product_categ = self.env['sale.order'].browse(self._context.get('sale_order_id')).type_id.valid_product_category.ids
            else:
                product_categ = self.env['sale.order.type'].browse(self._context.get('sale_type_id')).valid_product_category.ids
            if product_categ:
                args = [('categ_id', 'in', product_categ)]
                return super(product_product, self).name_search(name=name, args=args, operator=operator, limit=limit)
            # else:
            #     categories = self.search(args, limit=limit)
            #     return categories.name_get()
        return super(product_product, self).name_search(name=name, args=args, operator=operator, limit=limit)

class SaleOrderLine(models.Model):
    _inherit = 'sale.order.line'

    sale_order_type = fields.Many2one('sale.order.type')

    @api.model
    def default_get(self, field_list):
        values ={}
        res = super(SaleOrderLine, self).default_get(field_list)
        context = self._context
        res.update({'sale_order_type':context.get('sale_type_id')})
        return res

    @api.multi
    @api.onchange('product_id')
    def product_id_change(self):
        result = {}
        res = super(SaleOrderLine, self).product_id_change()
        # if self._context.get('sale_type_id') or self._context.get('sale_order_id'):
        product_categ = self.sale_order_type.valid_product_category.ids
        print('product_categ',product_categ)
        domain = {'product_id': [('categ_id','in', product_categ)]}
        result['domain'] = domain
        
        return result
