# -*- coding: utf-8 -*-

from odoo import models, fields, api
from datetime import datetime, timedelta
from odoo.exceptions import ValidationError,UserError
from datetime import date

class AccountInvoiceMapping(models.Model):
    _name = 'account.invoice.item.mapping'
    _rec_name = "sale_type"

    sale_type = fields.Many2one('sale.order.type',string='Sales Type', required="1")
    tax_type = fields.Selection([
    	('local','Local'),('central','Central'),('sez','SEZ')
    	],'Local/Central Tax',default='local',required="1")
    gst_tax = fields.Many2one('account.tax',string='GST Tax', required="1",
    	domain=[('type_tax_use', '=', 'sale')])
    account_id = fields.Many2one('account.account',string='Account', required="1")
    # product_id = fields.Many2one('product.product',string='Product')
    product_category_id = fields.Many2one('product.category','Product Category')