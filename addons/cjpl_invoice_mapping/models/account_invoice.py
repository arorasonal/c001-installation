# -*- coding: utf-8 -*-

from odoo import models, fields, api
from datetime import datetime, timedelta
from odoo.exceptions import ValidationError,UserError
from datetime import date

class account_invoice(models.Model):
    _inherit = "account.invoice"

    @api.multi
    def action_invoice_open(self):
        for line in self.invoice_line_ids:
            master_account_code_prod_id = self.env['account.invoice.item.mapping'].search(
                                    [('sale_type', '=', self.sale_type_id.id),
                                     ('tax_type', '=', self.tax_type),
                                     ('gst_tax','in',line.invoice_line_tax_ids.ids),
                                     ('product_category_id', '=', line.product_id.categ_id.id)])
            master_account_code_id = self.env['account.invoice.item.mapping'].search(
                                    [('sale_type', '=', self.sale_type_id.id),
                                     ('tax_type', '=', self.tax_type),
                                     ('gst_tax','in',line.invoice_line_tax_ids.ids),
                                     ('product_category_id', '=', False)
                                     ])
            fpos = line.invoice_id.fiscal_position_id
            product = line.product_id
            company = line.invoice_id.company_id
            currency = line.invoice_id.currency_id
            account = line.get_invoice_line_account(type, product, fpos, company)
            if master_account_code_prod_id:
                line.write({'account_id': master_account_code_prod_id.account_id.id})
            elif not master_account_code_prod_id and master_account_code_id:
                line.write({'account_id': master_account_code_id.account_id.id})
        return super(account_invoice, self).action_invoice_open()
