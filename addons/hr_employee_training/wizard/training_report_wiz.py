# -*- coding: utf-8 -*-

from odoo import fields, models
from dateutil.parser import parse
import xlwt
from xlwt import easyxf
from cStringIO import StringIO


class employee_training_wiz(models.Model):
    _name = 'employee.training.wiz'

    company_id = fields.Many2one('res.company', 'Company', required=True,
                                 default=lambda self:
                                 self.env['res.company']._company_default_get()
                                 )
    date_start = fields.Date('Start Date')
    date_end = fields.Date('End Date')
    department_id = fields.Many2one(
        'hr.department', 'Department', required=True)
    filter_id = fields.Selection([('filter_no', 'No Filters'),
                                  ('filter_date', 'Date'),
                                  ('filter_title', 'Job Title'), ],
                                 "Filter by", required=True,
                                 default='filter_no')
    job_id = fields.Many2one('hr.job', 'Job Title')

    def print_training_report(self):
        import base64
        filename = 'training.xls'
        workbook = xlwt.Workbook()
        style = xlwt.XFStyle()
        tall_style = xlwt.easyxf('font:height 820;')
        font = xlwt.Font()
        font.name = 'Times New Roman'
        font.bold = True
        font.height = 250
        style.font = font
        style1_center = easyxf(
            'font: name Arial;'
            'alignment: horizontal  center;'
            'font:bold True,height 220'
        )
        style1_left = easyxf(
            'font: name Arial;'
            'alignment: horizontal  left;'
            'font:bold True,height 220'
        )
        style1_left1 = easyxf(
            'font: name Arial;'
            'alignment: horizontal  left;'
        )

        worksheet = workbook.add_sheet('Sheet 1')
        second_col = worksheet.col(2)
        third_col = worksheet.col(3)
        first_col = worksheet.col(1)
        zero_col = worksheet.col(0)
        new_col = 0
        new_row = 5
        i = 0
        second_col.width = 250 * 30
        third_col.width = 250 * 30
        first_col.width = 250 * 30
        zero_col.width = 250 * 30

        if self.department_id:
            training_ids = self.env['training.request'].search([
                ('department_id', '=', self.department_id.id)])

        worksheet.write_merge(0, 0, 0, 0, (str(
        )).upper(),
            style1_left
        )
        worksheet.write_merge(
            2, 2, 0, 8, (self.company_id.name),
            style1_center
        )
        worksheet.write(3, 0, '', style1_left)
        worksheet.write(3, 1, '', style1_left)
        worksheet.write(4, 0, 'Employee', style1_left)
        worksheet.write(4, 1, 'Employee Training no.', style1_left)
        worksheet.write(4, 2, 'Department', style1_left)
        worksheet.write(4, 3, 'Manager', style1_left)
        worksheet.write(4, 4, 'Employment Type', style1_left)
        worksheet.write(4, 5, 'Training Title', style1_left)
        worksheet.write(4, 6, 'Qualification Attained', style1_left)
        worksheet.write(4, 7, 'Training Date', style1_left)
        worksheet.write(4, 8, 'Training Duration', style1_left)
        worksheet.write(4, 9, 'Training Facilator', style1_left)
        # worksheet.write(4, 10, 'Training Cost', style1_left)
        # worksheet.write(4, 11, 'Bonding Document', style1_left)
        # worksheet.write(4, 12, 'Employment Status', style1_left)
        # worksheet.write(4, 13, 'Certification', style1_left)
        # worksheet.write(4, 14, 'Training/Development', style1_left)
        for rec in training_ids:
            worksheet.write(new_row, 0, rec.employee_id.name, style1_left1)
            worksheet.write(new_row, 1, rec.name or '', style1_left1)
            worksheet.write(new_row, 2, rec.department_id.name, style1_left1)
            worksheet.write(
                new_row, 3, rec.department_id.manager_id.name, style1_left1)
            worksheet.write(
                new_row, 4, rec.employee_id.employee_type.id, style1_left1)
            worksheet.write(new_row, 5, rec.title, style1_left1)
            worksheet.write(new_row, 7, rec.training_date, style1_left1)
            worksheet.write(new_row, 8, rec.duration, style1_left1)
            worksheet.write(new_row, 9, rec.facilitator.id, style1_left1)
            # worksheet.write(new_row, 10, rec.cost, style1_left1)
            # for line in rec.order_line:
            #     worksheet.write(new_row, 1, line.staff_no or '', style1_left1)
            #     worksheet.write(
            #         new_row, 6, line.qualification_attained or '', style1_left1)
            #     worksheet.write(
            #         new_row, 11, line.bonding_document_signed, style1_left1)
            #     worksheet.write(new_row, 12, line.status1 or '', style1_left1)
            #     worksheet.write(
            #         new_row, 13, line.certificate or '', style1_left1)
            #     worksheet.write(
            #         new_row, 14, line.training_development or '', style1_left1)
            new_row += 1

        fp = StringIO()
        workbook.save(fp)
        export_id = self.env['employeetraining.excel'].create(
            {
                'excel_file': base64.encodestring(fp.getvalue()),
                'file_name': filename,
            }
        )
        fp.close()

        return {
            'view_mode': 'form',
            'res_id': export_id.id,
            'res_model': 'employeetraining.excel',
            'view_type': 'form',
            'type': 'ir.actions.act_window',
            'target': 'new',
        }


class trainingreport_excel(models.TransientModel):
    _name = "employeetraining.excel"

    excel_file = fields.Binary('Training Report')
    file_name = fields.Char('Excel File', size=64)


trainingreport_excel()
