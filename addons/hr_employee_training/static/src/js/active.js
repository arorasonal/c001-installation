openerp.hcah_hr = function (instance) {
    
    var QWeb = instance.web.qweb;
    var _t = instance.web._t;
    var _lt = instance.web._lt;

    instance.hcah_hr.AttendanceSliderr = instance.web.Widget.extend({
        template: 'AttendanceSliderr',
        init: function (parent) {
            this._super(parent);
            this.set({"signed_in": false});
        },
        start: function() {
            var self = this;
            var tmp = function() {
                var $sign_in_out_iconn = this.$('#oe_attendance_signn_in_out_icon');
                $sign_in_out_iconn.toggleClass("fa-toggle-off", ! this.get("signed_in"));
                $sign_in_out_iconn.toggleClass("fa-toggle-on", this.get("signed_in"));
            };
            this.on("change:signed_in", this, tmp);
            _.bind(tmp, this)();
            this.$(".talesales_active").click(function(ev) {
                ev.preventDefault();
                self.do_update_active();

            });
            this.$el.tooltip({
                title: function() {
                    var res_users = new instance.web.DataSet(self, 'res.users');
                    if (self.get("signed_in")) {
                        return _.str.sprintf(_t("Click to Off"));
                    } 
                    else {
                        return _.str.sprintf(_t("Click to On"));
                    }
                },
            });
            return this.check_attendance();
        },

        do_update_active: function () {
            var self = this;
            var res_users = new instance.web.DataSet(self, 'res.users');
            res_users.call('active_change', [
                [self.session.uid]
            ]).done(function (result) {
                self.set({"signed_in": ! self.get("signed_in")});
            });
        },

        check_attendance: function () {
            var self = this;
            self.employee = false;
            this.$el.hide();
            var employee = new instance.web.DataSetSearch(self, 'res.users', self.session.user_context, [
                ['id', '=', self.session.uid]
            ]);
            return employee.read_slice(['id', 'tr_active']).then(function (res) {
                if (_.isEmpty(res) )
                    return;
                if (self.employee.tr_active === true){
                    return;
                }
                self.$el.show();
                self.employee = res[0];
                self.set({"signed_in": self.employee.tr_active !== false});
            });
        },
    });

    
    instance.web.UserMenu.include({
        do_update: function () {
            this._super();
            var self = this;
            this.update_promise.done(function () {
                if (!_.isUndefined(self.attendancesliderr)) {
                    return;
                }
                // check current user is an employee
                var Users = new instance.web.Model('res.users');
                Users.call('has_group', ['base.group_user']).done(function(is_employee) {
                    if (is_employee) {
                        self.attendancesliderr = new instance.hcah_hr.AttendanceSliderr(self);
                        self.attendancesliderr.prependTo(instance.webclient.$('.oe_systray'));
                    } else {
                        self.attendancesliderr = null;
                    }
                });
            });
        },
    });
};
