from odoo.exceptions import UserError, Warning
from dateutil import relativedelta
from datetime import datetime, date
from odoo.tools.translate import _
from odoo import api, fields, models
from odoo.tools.translate import _


class Training_Request(models.Model):
    _name = 'training.request'
    _inherit = 'mail.thread'
    _description = 'Training Request'
    _rec_name = "name"

    def _current_employee_get(self):
        emp_ids = self.env['hr.employee'].search(
            [('user_id', '=', self.env.uid)])
        return emp_ids and emp_ids[0] or False

    TRAINING_Selection = [
        ('internal', 'Internal'),
        ('external', 'External')
    ]
    CAPTURED_TRAINING_Selection = [
        ('yes', 'Yes'),
        ('no', 'No'),
        ('na', 'N/A')
    ]
    STATE_SELECTION = [
        ('draft', 'Draft'),
        ('awaiting_trainer_approval', 'Awaiting Trainer Approval'),
        ('awaiting_hod_approval', 'Awaiting Trainer HOD Approval'),
        ('awaiting_hr_approval', 'Awaiting HR Approval'),
        ('awaiting_ceo_approval', 'Awaiting CEO Approval'),
        ('approved', 'Awaiting Finance Approval'),
        ('done', 'Done'),
        ('refused', 'Refused'),
        ('in_progress', 'In Progress'),
        ('feedback', 'Feedback'),
        ('cancelled', 'Cancelled'),
        ('close', 'Closed'),
    ]

    employee_id = fields.Many2one('hr.employee', 'Requested By',
                                  readonly=True,
                                  required=True,
                                  default=_current_employee_get)
    date = fields.Date('Request Date')
    joining_date = fields.Date('Joining Date')
    manager_id = fields.Many2one('hr.employee', 'Responsible', readonly=True)
    user_id = fields.Many2one('res.users', "Employee", readonly=False)
    department_id = fields.Many2one('hr.department', 'Department',
                                    readonly=False)
    training_date = fields.Datetime('Training Date',
                                    readonly=False)
    training_end_date = fields.Datetime('Training End Date',
                                        readonly=False)
    type_id = fields.Many2one('training.type', 'Training Type',
                              index=True)
    type_mode = fields.Many2one('training.mode', 'Training Mode', index=True)
    name = fields.Char('Reference #')
    title = fields.Char('Training Title',
                        )
    duration = fields.Float('Training Duration (Days)',
                            required=True, compute="_total_days_month"
                            )
    training_hr = fields.Float('Training Duration (Hour)')
    venue = fields.Char('Venue')
    facilitator = fields.Many2one('hr.employee', 'Training Facilitator')
    sponsorship = fields.Char('Sponsorship (if any)', readonly=False
                              )
    highlight = fields.Text('Training Highlights', readonly=False
                            )
    justification = fields.Text('Training Justification', readonly=False
                                )
    hr_remarks = fields.Text('HR Remarks', readonly=False,
                             )
    state = fields.Selection(STATE_SELECTION, 'State', index=True,
                             readonly=False,
                             default="draft",
                             )
    order_line = fields.One2many('training.line', 'training_id',
                                 'Training Lines',
                                 )
    refused_by = fields.Many2one("res.users", "Decline By", readonly=True)
    feedback_lines = fields.One2many(
        'employee.feedback', 'feedback_id', 'Feedback')
    trainer_feedback_lines = fields.One2many(
        'trainer.feedback', 'feedback_id', 'Feedback')
    training_benifit_comp = fields.Text(
        "How would the training benefit the company",
        readonly=False,
    )
    job_id = fields.Many2one('hr.job', 'Designation')
    train_ids = fields.One2many(
        'training.training', 'train_id', 'Employee Training')
    cost_ids = fields.One2many(
        'estimate.cost', 'cost_emp_id', 'Cost Training')
    note_id = fields.Text("Reason for Refusal/Cancellation", states={
        'refused': [('readonly', True)]})
    total_cost_amount = fields.Float(
        'Actual Training Cost', default=0.0, compute='_compute_amount')
    total_amount = fields.Float(
        'Training Cost', default=0.0, compute='_compute_cost')
    current_user = fields.Boolean(
        copy=False, compute="check_current_user", default=False)
    current_trainer = fields.Boolean(
        copy=False, compute="check_current_trainer", default=False)
    act_training_emp = fields.Boolean(
        copy=False, default=False, compute="check_act_training_emp")
    current_emp = fields.Boolean(copy=False, compute="check_current_emp", default=False)


    @api.multi
    def check_current_user(self):
        user = self.env.uid
        for record in self:
            # if self.env.user.has_group('hr_employee_register.group_hr_department_head'):
            #     record.current_user = True
            if record.facilitator.department_id.manager_id.user_id.id == user:
                record.current_user = True
            else:
                record.current_user = False

    @api.multi
    def check_act_training_emp(self):
        user = self.env.uid
        for record in self:
            for line in record.cost_ids:
                if line.employee_id.user_id.id == user:
                    record.act_training_emp = True
                else:
                    record.act_training_emp = False

    @api.multi
    def check_current_trainer(self):
        user = self.env.uid
        for record in self:
            if record.facilitator.user_id.id == user:
                record.current_trainer = True
            else:
                record.current_trainer = False
    @api.multi
    def check_current_emp(self):
        user = self.env.uid
        for record in self:
            if record.employee_id.user_id.id == user:
                record.current_emp = True

    @api.depends('training_date', 'training_end_date')
    def _total_days_month(self):
        for sheet in self:
            if sheet.training_end_date:
                training_end_date = datetime.strptime(
                    str(sheet.training_end_date), "%Y-%m-%d %H:%M:%S")
                training_date = datetime.strptime(
                    str(sheet.training_date), "%Y-%m-%d %H:%M:%S")
                sheet.duration = (training_end_date -
                                  training_date).days + 1
        return sheet.duration

    # @api.depends('training_date', 'training_end_date')
    # def _total_hr_training_date(self):
    #     for record in self:
        #     if sheet.training_end_date:
        #         training_end_date = datetime.strptime(
        #             str(sheet.training_end_date), "%Y-%m-%d %H:%M:%S")
        #         training_date = datetime.strptime(
        #             str(sheet.training_date), "%Y-%m-%d %H:%M:%S")
        #         sheet.duration = (training_end_date -
        #                           training_date).days + 1
        # return sheet.duration

    @api.one
    @api.depends('cost_ids.amount')
    def _compute_amount(self):
        self.total_cost_amount = sum(line.amount for line in self.cost_ids)

    @api.one
    @api.depends('order_line.amount')
    def _compute_cost(self):
        self.total_amount = sum(
            line.amount for line in self.order_line)

    @api.multi
    @api.onchange('employee_id')
    def onchange_employee(self):
        result = {'value': {'department_id': False,
                            'job_id': False}}
        employee = self.env['hr.employee'].search([
            ('id', '=', self.employee_id.id)])
        if employee.department_id:
            result['value'] = {'department_id': employee.department_id.id,
                               'job_id': employee.job_id.id}
        return result

    @api.model
    def create(self, vals):
        """Create Method."""
        res = super(Training_Request, self).create(vals)
        if 'training_date' in vals:
            current_date = datetime.strptime(
                fields.Datetime.now(), "%Y-%m-%d %H:%M:%S").date()
            date = datetime.strptime(
                vals['training_date'], "%Y-%m-%d %H:%M:%S").date()
            if current_date >= date:
                difference = int((current_date - date).days)
                if difference >= 1:
                    raise UserError('You can not select Previous Date')
        res['department_id'] = res.employee_id.department_id.id
        res['job_id'] = res.employee_id.job_id.id
        return res

    @api.multi
    def write(self, vals):
        """Write Method."""
        employee = self.env['hr.employee'].search([
            ('id', '=', vals.get('employee_id'))])
        if employee:
            vals.update({'department_id': employee.department_id.id,
                         'job_id': employee.job_id.id,
                         })
        if 'training_date' in vals:
            current_date = datetime.strptime(
                fields.Datetime.now(), "%Y-%m-%d %H:%M:%S").date()
            date = datetime.strptime(
                vals['training_date'], "%Y-%m-%d %H:%M:%S").date()
            if current_date >= date:
                difference = int((current_date - date).days)
                if difference >= 1:
                    raise UserError('You can not select Previous Date')
        return super(Training_Request, self).write(vals)

    @api.multi
    def unlink(self):
        """Allows to delete hr draft"""
        for rec in self:
            if rec.state not in ['draft']:
                raise UserError(
                    'You can only delete in draft state')
        return super(Training_Request, self).unlink()

    @api.multi
    def button_submit_approval(self):
        current_date = fields.Datetime.now()
        if not self.name:
            seq = self.env['ir.sequence'].get('training.request')
            self.write({'name': seq})
        self.write({'state': 'awaiting_trainer_approval', 'date': current_date})
        return True

    @api.multi
    def button_trainer_approval(self):
        if self.state == 'awaiting_trainer_approval':
            template = self.env.ref(
                'hr_employee_training.submit_for_hod_Approval')
            users = self.env['res.users'].search([])
            mail = ''
            for user in users:
                if user.has_group('hr_employee_register.group_hr_department_head'):
                    mail += user.login + ','
            if template:
                for record in self:
                    if record.employee_id:
                        template.email_from = record.employee_id.work_email
                        template.email_to = record.department_id.manager_id.work_email or mail
                        template.send_mail(self.id, force_send=True)
            self.write({'state': 'awaiting_hod_approval'})
            return True

    @api.multi
    def button_trainer_hod(self):
        template = self.env.ref('hr_employee_training.submit_for_hr_Approval')
        users = self.env['res.users'].search([])
        mail = ''
        for user in users:
            if user.has_group('hr.group_hr_user'):
                mail += user.login + ','
        if template:
            for record in self:
                if record.employee_id:
                    template.email_from = record.employee_id.work_email
                    template.email_to = mail
                    template.send_mail(self.id, force_send=True)
        self.write({'state': 'awaiting_hr_approval'})
        return True

    @api.multi
    def button_hr_approval(self):
        template = self.env.ref(
            'hr_employee_training.submit_for_ceo/md_Approval')
        users = self.env['res.users'].search([])
        mail = ''
        for user in users:
            if user.has_group('hr_employee_register.group_base_ceo_md'):
                mail += user.login + ','
        if template:
            for record in self:
                if record.employee_id:
                    template.email_from = record.employee_id.work_email
                    template.email_to = mail
                    template.send_mail(self.id, force_send=True)
        self.write({'state': 'approved'})
        return True

    # @api.multi
    # def state_approved(self):
    #     self.write({'state': 'approved'})
    #     return True

    # @api.multi
    # def state_draft(self):
    #     self.write({'state': 'draft'})
    #     return True

    @api.multi
    def action_in_progress(self):
        self.write({'state': 'in_progress'})
        return True

    @api.multi
    def button_done(self):
        if not self.cost_ids:
            raise UserError(
                "Please enter the Actual Training Information")
        self.write({'state': 'done'})
        return True

    @api.multi
    def state_refused(self):
        user_id = self.env['res.users'].browse(self.env.uid).id
        for emp in self:
            if not emp.note_id:
                raise Warning(_('Please first write the Reason for Refusal'))
            else:
                self.write({'state': 'refused',
                            'refused_by': user_id})
        return True

    @api.multi
    def button_reset_to_draft_trainee(self):
        if self.state == 'awaiting_trainer_approval':
            self.write({'state': 'draft'})
        return True

    @api.multi
    def button_reset_to_draft_hod(self):
        if self.state == 'awaiting_hod_approval':
            self.write({'state': 'draft'})
        return True

    @api.multi
    def button_reset_to_draft_hr(self):
        if self.state == 'awaiting_hr_approval':
            self.write({'state': 'draft'})
        return True

    @api.multi
    def awaiting_finance_reset(self):
        if self.state == 'approved':
            self.write({'state': 'draft'})
        return True

    @api.multi
    def action_submit_feedback(self):
        lst = []
        for rec in self:
            for line in rec.cost_ids:
                lst.append((0, 0, {'employee_id': line.employee_id.id}))
        self.update({'trainer_feedback_lines': lst})
        self.update({'feedback_lines': lst})
        self.write({'state': 'feedback'})
        return True

    @api.multi
    def action_close(self):
        for line in self.trainer_feedback_lines:
            if not line.feedback:
                raise UserError(
                    "Please submit trainer feedback..")
        for line in self.feedback_lines:
            if not line.feedback:
                raise UserError(
                    "Please submit employee feedback..")
        if self.trainer_feedback_lines and self.feedback_lines:
            self.write({'state': 'close'})
        return True

    @api.multi
    def action_cancel(self):
        for emp in self:
            if not emp.note_id:
                raise Warning(_('Please first write Reason for Cancellation'))
            else:
                self.write({'state': 'cancelled'})
        return True

    @api.multi
    def button_progress_cancel(self):
        for emp in self:
            if not emp.note_id:
                raise Warning(_('Please first write Reason for Cancellation'))
            else:
                self.write({'state': 'cancelled'})
        return True

    @api.multi
    def button_finance_cancel(self):
        for emp in self:
            if not emp.note_id:
                raise Warning(_('Please first write Reason for Cancellation'))
            else:
                self.write({'state': 'cancelled'})
        return True
    

class Training_Lines(models.Model):
    _name = 'training.line'
    _description = 'Training Lines'

    employee_id = fields.Many2one('hr.employee', 'Employees')
    job_id = fields.Many2one('hr.job', 'Designation')
    department_id = fields.Many2one('hr.department', 'Department')
    training_id = fields.Many2one('training.request', 'Training Id')
    date_of_joining = fields.Date('Date of Joining')
    last_date = fields.Date('Last Training Date')
    cost_id = fields.Many2one('account.account', string='Cost Head')
    amount = fields.Float('Amount')    

    @api.multi
    def training_confirm_apagen(self):
        self.write({'status1': 'confirmed'})
        return True

    @api.multi
    @api.onchange('employee_id')
    def onchange_employee_id(self):
        lis = list()
        training_emp_id = self.env['estimate.cost'].search(
            [('employee_id', '=', self.employee_id.id)
             ])
        for training_emp in training_emp_id:
            lis.append(training_emp)
        if self.employee_id:
            self.department_id = self.employee_id.department_id.id
            self.job_id = self.employee_id.job_id.id
            self.date_of_joining = self.employee_id.date_of_joining
            if lis:
                for rec in lis[-1]:
                    self.last_date = rec.actual_date
            else:
                self.last_date = False

    @api.model
    def create(self, vals):
        """Create Method."""
        lis = list()
        training_emp_id = self.env['estimate.cost'].search(
            [('employee_id', '=', vals.get('employee_id'))])
        employee_ids = self.env['hr.employee'].search(
            [('id', '=', vals.get('employee_id'))])
        if training_emp_id:
            for rec in training_emp_id:
                lis.append(rec)
               
                if employee_ids:
                    vals.update({
                        'department_id': employee_ids.department_id.id,
                        'job_id': employee_ids.job_id.id,
                        'date_of_joining': employee_ids.date_of_joining,
                        'last_date': lis[-1].actual_date
                    })
        else:
            if employee_ids:
                vals.update({
                    'department_id': employee_ids.department_id.id,
                    'job_id': employee_ids.job_id.id,
                    'date_of_joining': employee_ids.date_of_joining,
                })
        return super(Training_Lines, self).create(vals)

    @api.multi
    def write(self, vals):
        """Write Method."""
        lis1 = list()
        # employee = self.env['hr.employee'].search([
        #     ('id', '=', vals.get('employee_id'))
        # ])
        training_emp_id = self.env['estimate.cost'].search(
            [('employee_id', '=', self.employee_id.id)
             ])
        for rec in training_emp_id:
            lis1.append(rec)
        # lis1.pop(-1)
        if self.training_id.employee_id:
            vals.update({
                'department_id': self.employee_id.department_id.id,
                'job_id': self.employee_id.job_id.id,
                'date_of_joining': self.employee_id.date_of_joining,
                'last_date': lis1[-1].actual_date
            })
        return super(Training_Lines, self).write(vals)


class employee_feedback(models.Model):
    _name = 'employee.feedback'

    feedback_id = fields.Many2one('training.request', 'Training Id')
    employee_id = fields.Many2one('hr.employee', 'Employees')
    feedback = fields.Char('Feedback')
    check_employee = fields.Boolean('Check Employee', compute="check_check_user" )

    @api.depends('check_employee')
    def check_check_user(self):
        user_id = self.env.user.id
        for rec in self:
            if (user_id == rec.employee_id.user_id.id):
                rec.check_employee = True

class trainer_feedback(models.Model):
    _name = 'trainer.feedback'

    feedback_id = fields.Many2one('training.request', 'Training Id')
    employee_id = fields.Many2one('hr.employee', 'Facilitator')
    feedback = fields.Char('Feedback')
    check_man = fields.Boolean('check manager', compute="check_manager" )

    @api.depends('feedback_id')
    def check_manager(self):
        user_id = self.env.uid
        for record in self:
            if (user_id == record.feedback_id.facilitator.user_id.id):
                record.check_man = True
            else:
                record.check_man = False


class Training(models.Model):
    _name = 'training.training'

    date_id = fields.Datetime('Date')
    description = fields.Char('Description')
    remarks = fields.Char('Remarks')
    train_id = fields.Many2one('training.request', 'Train Id')

    @api.model
    def create(self, vals):
        """Create Method."""
        res = super(Training, self).create(vals)
        if 'date_id' in vals:
            current_date = datetime.strptime(
                fields.Datetime.now(), "%Y-%m-%d %H:%M:%S").date()
            date = datetime.strptime(
                vals['date_id'], "%Y-%m-%d %H:%M:%S").date()
            if current_date >= date:
                difference = int((current_date - date).days)
                if difference >= 1:
                    raise UserError('You can not select Previous Date')
        return res

    @api.multi
    def write(self, vals):
        """Write Method."""
        if 'date_id' in vals:
            current_date = datetime.strptime(
                fields.Datetime.now(), "%Y-%m-%d %H:%M:%S").date()
            date = datetime.strptime(
                vals['date_id'], "%Y-%m-%d %H:%M:%S").date()
            if current_date >= date:
                difference = int((current_date - date).days)
                if difference >= 1:
                    raise UserError('You can not select Previous Date')
        return super(Training, self).write(vals)


class Estimate_Cost(models.Model):
    _name = 'estimate.cost'

    employee_id = fields.Many2one('hr.employee', 'Employees')
    job_id = fields.Many2one('hr.job', 'Designation')
    department_id = fields.Many2one('hr.department', 'Department')
    training_id = fields.Many2one('training.request', 'Training Id')
    date_of_joining = fields.Date('Date of Joining')
    last_date = fields.Date('Last Training Date')
    cost_id = fields.Many2one('account.account', string='Cost Head')
    amount = fields.Float('Amount')
    actual_date = fields.Date('actual Training Date')
    cost_emp_id = fields.Many2one('training.request', 'Employee Cost')

    @api.multi
    @api.onchange('employee_id')
    def onchange_employee_ids(self):
        lis = list()
        get_date = self.cost_emp_id.training_date
        self.actual_date = get_date
        training_emp_id = self.env['estimate.cost'].search(
            [('employee_id', '=', self.employee_id.id)
             ])
        for training_emp in training_emp_id:
            lis.append(training_emp)
        if self.employee_id:
            self.department_id = self.employee_id.department_id.id
            self.job_id = self.employee_id.job_id.id
            self.date_of_joining = self.employee_id.date_of_joining
            if lis:
                for rec in lis[-1]:
                    self.last_date = rec.actual_date
            else:
                self.last_date = False
   
    @api.model
    def create(self, vals):
        """Create Method."""
        lis1 = list()
        training_emp_id = self.env['estimate.cost'].search(
            [('employee_id', '=', vals.get('employee_id'))])
        employee_ids = self.env['hr.employee'].search(
            [('id', '=', vals.get('employee_id'))])
        if training_emp_id:
            for rec in training_emp_id:
                lis1.append(rec)
                if employee_ids:
                    vals.update({
                        'department_id': employee_ids.department_id.id,
                        'job_id': employee_ids.job_id.id,
                        'date_of_joining': employee_ids.date_of_joining,
                        'last_date': lis1[-1].actual_date
                    })

        else:
            if employee_ids:
                vals.update({
                    'department_id': employee_ids.department_id.id,
                    'job_id': employee_ids.job_id.id,
                    'date_of_joining': employee_ids.date_of_joining,
                })
        return super(Estimate_Cost, self).create(vals)

    @api.multi
    def write(self, vals):
        """Write Method."""
        lis1 = list()
        training_emp_id = self.env['estimate.cost'].search(
            [('employee_id', '=', self.employee_id.id)
             ])
        for rec in training_emp_id:
            lis1.append(rec)
        if self.cost_emp_id.employee_id:
            vals.update({
                'department_id': self.employee_id.department_id.id,
                'job_id': self.employee_id.job_id.id,
                'date_of_joining': self.employee_id.date_of_joining,
                'last_date': lis1[-1].actual_date
            })
        return super(Estimate_Cost, self).write(vals)


class Training_Type(models.Model):
    _name = 'training.type'

    name = fields.Char('Training Type')


class Training_Mode(models.Model):
    _name = 'training.mode'

    name = fields.Char('Training Mode')
