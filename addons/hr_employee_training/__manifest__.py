# -*- coding: utf-8 -*-

{
    'name': 'Employee Training',
    'version': '1.0',
    'category': 'HR',
    'sequence': 14,
    'summary': 'Employee Training',
    'description': """
    """,
    'author': 'Apagen Solutions Pvt. Ltd.',
    'website': 'https://www.apagen.com',
    'depends': [
                'hr',
                'base',
                'hr_employee_register',
                'hr_employee_contract',
    ],
    'data': [
        'training_request_view.xml',
        'training_request_sequence.xml',
        'wizard/training_report_wiz.xml',
        'employee_training_data.xml',
        'template_training_approvals.xml',
        'security/ir_rule_trining.xml',
        'security/ir.model.access.csv'
    ],
    'installable': True,
    'auto_install': False,
    'application': True,
}
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4: