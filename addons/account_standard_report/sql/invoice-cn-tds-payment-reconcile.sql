-- View: "jasper"."cjpl_js_invoice_cn_tds_payment_reconciliation"
-- DROP VIEW "jasper"."cjpl_js_invoice_cn_tds_payment_reconciliation";

-- CREATE VIEW "cjpl_js_invoice_cn_tds_payment_reconciliation" ---------
CREATE OR REPLACE VIEW "jasper"."cjpl_js_invoice_cn_tds_payment_reconciliation" AS
WITH account_report_standard_ledger_report_object
AS (
	SELECT DISTINCT NOW() AS create_date,
		aml.partner_id AS object_id,
		CASE 
			WHEN rep.ref IS NULL
				THEN rep.name
			ELSE rep.ref || ' ' || rep.name
			END AS name,
		NULL AS account_id,
		aml.partner_id AS partner_id,
		NULL AS journal_id,
		NULL AS analytic_account_id
	FROM account_move_line aml
	JOIN res_partner rep ON (rep.id = aml.partner_id)
	ORDER BY name
	),
rps
AS (
	SELECT id,
		coalesce(parent_id, id) AS parent_id
	FROM res_partner
	),
matching_in_futur_before_init (id)
AS (
	SELECT DISTINCT afr.id AS id
	FROM account_full_reconcile afr
	INNER JOIN account_move_line aml ON aml.full_reconcile_id = afr.id
	),
matching_in_futur_after_date_to (id)
AS (
	SELECT DISTINCT afr.id AS id
	FROM account_full_reconcile afr
	INNER JOIN account_move_line aml ON aml.full_reconcile_id = afr.id
	),
query_as_per_program
AS (
	SELECT NOW()::DATE AS create_date,
		aml.account_id,
		aml.analytic_account_id,
		'2_line' AS TYPE,
		'normal' AS type_view,
		aml.journal_id,
		aml.partner_id,
		aml.move_id,
		inv.id AS invoice_id,
		rpa.name AS invoice_salesperson,
		sot.name AS sale_order_type,
		m.name AS doc_no,
		j.name AS journal,
		j.type AS journal_type,
		rp.name AS customer_name,
		rpb.name AS customer_salesperson,
		crm_team.name AS customer_team,
		aml.id,
		aml.DATE AS doc_date,
		aml.date_maturity,
		coalesce(apt.name, '') AS payment_term,
		aml.debit,
		aml.credit,
		aml.balance,
		aml.amount_residual,
		coalesce(aml.full_reconcile_id, aml.id - 50000000)::INTEGER AS 
		full_reconcile_id,
		CASE 
			WHEN aml.full_reconcile_id IS NOT NULL
				AND NOT mifad.id IS NOT NULL
				THEN TRUE
			ELSE FALSE
			END AS reconciled,
		aml.amount_currency AS amount_currency,
		aml.currency_id AS currency_id
	FROM account_report_standard_ledger_report_object ro
	INNER JOIN account_move_line aml ON aml.partner_id = ro.object_id
	LEFT JOIN account_journal j ON (aml.journal_id = j.id)
	LEFT JOIN account_account acc ON (aml.account_id = acc.id)
	LEFT JOIN account_account_type acc_type ON (acc.user_type_id = acc_type.id
			)
	LEFT JOIN account_move m ON (aml.move_id = m.id)
	LEFT JOIN matching_in_futur_before_init mif ON (aml.full_reconcile_id = mif.id
			)
	LEFT JOIN matching_in_futur_after_date_to mifad ON (aml.full_reconcile_id = mifad.id
			)
	LEFT JOIN rps ON rps.id = aml.partner_id
	LEFT JOIN res_partner rp ON rp.id = rps.parent_id
	LEFT JOIN crm_team ON rp.team_id = crm_team.id
	LEFT JOIN account_invoice inv ON aml.invoice_id = inv.id
	LEFT JOIN account_payment_term apt ON apt.id = inv.payment_term_id
	LEFT JOIN sale_order_type sot ON sot.id = inv.sale_type_id
	LEFT JOIN res_users rua ON rua.id = inv.user_id
	LEFT JOIN res_partner rpa ON rpa.id = rua.partner_id
	LEFT JOIN res_users rub ON rub.id = rp.user_id
	LEFT JOIN res_partner rpb ON rpb.id = rub.partner_id
	WHERE m.STATE IN ('posted')
		AND (
			acc.type_third_parties IN ('customer')
			AND (
				aml.full_reconcile_id IS NULL
				OR mif.id IS NOT NULL
				)
			)
		AND aml.DATE >= '2019-12-01'::DATE
		AND NOT (
			FALSE
			AND acc.compacted = TRUE
			)
		AND (
			FALSE
			OR NOT (
				aml.full_reconcile_id IS NOT NULL
				AND NOT mifad.id IS NOT NULL
				)
			)
	),
compute_pending
AS (
	SELECT create_date,
		account_id,
		analytic_account_id,
		journal_id,
		partner_id,
		move_id,
		invoice_id,
		invoice_salesperson,
		sale_order_type,
		doc_no,
		journal,
		journal_type,
		customer_name,
		customer_salesperson,
		customer_team,
		doc_date,
		date_maturity,
		payment_term,
		round(debit, 2) AS debit,
		round(credit, 2) AS credit,
		round(balance, 2) AS balance,
		round(amount_residual, 2) AS amount_residual,
		CASE 
			WHEN round(balance, 2) <> round(amount_residual, 2)
				THEN round(amount_residual, 2)
			ELSE round(balance, 2)
			END AS pending_amount,
		full_reconcile_id,
		reconciled
	FROM query_as_per_program
	),
parse_amt
AS (
	SELECT RANK() OVER (
			PARTITION BY full_reconcile_id ORDER BY CASE 
					WHEN journal_type = 'sale'
						THEN 1
					WHEN journal_type = 'general'
						THEN 2
					WHEN journal_type = 'bank'
						THEN 3
					WHEN journal_type = 'cash'
						THEN 4
					ELSE 5
					END,
				doc_date
			) rnk,
		CASE 
			WHEN journal_type = 'bank'
				OR journal_type = 'cash'
				THEN debit - credit
			ELSE 0
			END AS bank_amt,
		CASE 
			WHEN journal_type = 'bank'
				OR journal_type = 'cash'
				THEN doc_no
			END AS bank_doc,
		CASE 
			WHEN journal_type = 'sale'
				AND (debit - credit) > 0
				THEN debit - credit
			ELSE 0
			END AS sale_amt,
		CASE 
			WHEN journal_type = 'sale'
				AND (debit - credit) < 0
				THEN debit - credit
			ELSE 0
			END AS cn_amt,
		CASE 
			WHEN journal_type = 'sale'
				AND (debit - credit) < 0
				THEN doc_no
			END AS cn_doc,
		CASE 
			WHEN journal_type = 'general'
				AND journal LIKE '%TDS%'
				THEN debit - credit
			ELSE 0
			END AS tds_amt,
		CASE 
			WHEN journal_type = 'general'
				AND journal LIKE '%TDS%'
				THEN doc_no
			END AS tds_doc,
		CASE 
			WHEN journal_type = 'general'
				AND journal LIKE '%Return%'
				THEN debit - credit
			ELSE 0
			END AS return_amt,
		CASE 
			WHEN journal_type = 'general'
				AND journal LIKE '%Return%'
				THEN doc_no
			END AS return_doc,
		CASE 
			WHEN journal_type = 'general'
				AND journal LIKE '%Opening%'
				THEN debit - credit
			ELSE 0
			END AS opening_amt,
		CASE 
			WHEN journal_type = 'general'
				AND journal LIKE '%Opening%'
				THEN doc_no
			END AS opening_doc,
		CASE 
			WHEN journal_type = 'general'
				AND journal NOT LIKE '%Opening%'
				AND journal NOT LIKE '%Return%'
				AND journal NOT LIKE '%TDS%'
				THEN debit - credit
			ELSE 0
			END AS journal_amt,
		CASE 
			WHEN journal_type = 'general'
				AND journal NOT LIKE '%Opening%'
				AND journal NOT LIKE '%Return%'
				AND journal NOT LIKE '%TDS%'
				THEN doc_no
			END AS journal_doc,
		*
	FROM compute_pending
	),
group_amt
AS (
	SELECT full_reconcile_id,
		sum(pending_amount) AS pending_amt,
		sum(sale_amt) AS sale_amt,
		sum(cn_amt) AS cn_amt,
		string_agg(cn_doc, ',') AS cn_doc,
		sum(bank_amt) AS bank_amt,
		string_agg(bank_doc, ',') AS bank_doc,
		sum(tds_amt) AS tds_amt,
		string_agg(tds_doc, ',') AS tds_doc,
		sum(opening_amt) AS opening_amt,
		string_agg(opening_doc, ',') AS opening_doc,
		sum(return_amt) AS return_amt,
		string_agg(return_doc, ',') AS return_doc,
		sum(journal_amt) AS journal_amt,
		string_agg(journal_doc, ',') AS journal_doc,
		string_agg(doc_no, ',') AS doc_no
	FROM parse_amt
	GROUP BY full_reconcile_id
	)
SELECT b.doc_no,
	b.doc_date,
	b.customer_name,
	b.customer_salesperson,
	b.customer_team,
	a.pending_amt,
	a.sale_amt,
	a.opening_amt,
	a.cn_amt,
	a.bank_amt,
	a.tds_amt,
	a.return_amt,
	a.journal_amt,
	a.cn_doc,
	a.bank_doc,
	a.tds_doc,
	a.return_doc,
	a.journal_doc
FROM group_amt a
JOIN parse_amt b ON a.full_reconcile_id = b.full_reconcile_id
WHERE b.rnk = 1
ORDER BY b.customer_name,
	b.doc_date,
	b.doc_no;

-- -------------------------------------------------------------

ALTER TABLE "jasper"."cjpl_js_invoice_cn_tds_payment_reconciliation"
    OWNER TO postgres;	


GRANT ALL ON TABLE jasper.cjpl_js_invoice_cn_tds_payment_reconciliation TO postgres;
GRANT ALL ON TABLE jasper.cjpl_js_invoice_cn_tds_payment_reconciliation TO maintainer WITH GRANT OPTION;
GRANT ALL ON TABLE jasper.cjpl_js_invoice_cn_tds_payment_reconciliation TO reporter;	


