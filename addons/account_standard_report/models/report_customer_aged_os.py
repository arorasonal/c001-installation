# -*- coding: utf-8 -*-

# -- 14-12-2020
# -- account standard report - aged outstanding - customers - posted - unreconciled
# -- report showed all bills and payments where partial adjustment were done showing full 
# --     value.
# -- account move lines has field amount_residual which shows amount remaining after 
# --     partial adjustment
# -- sql has been corrected to show amount residual wherever it is not zero

# -- 14-01-2021
# -- added invoice shipping & invoice partner ids and names 

# -- 14-05-2021
# -- opening balance if > 0 is aged
# -- opening balance if < 0 is shown on account

import logging

from odoo import api, fields, models
from odoo import tools, _
import time
import math
from time import gmtime, strftime
import odoo.addons.decimal_precision as dp
from datetime import datetime,timedelta,date
from odoo.tools import DEFAULT_SERVER_DATE_FORMAT,DEFAULT_SERVER_DATETIME_FORMAT
from odoo.exceptions import UserError, ValidationError
from odoo.modules.module import get_module_resource

class report_customer_aged_os(models.Model):
    _name="report.customer.aged.os"

    create_date  = fields.Date(string='Created in')
    account_id = fields.Many2one(comodel_name='account.account', string='Account')
    analytic_account_id = fields.Many2one(comodel_name='account.analytic.account', string='Account Analytic')
    journal_id = fields.Many2one(comodel_name='account.journal', string='Account Journal')
    partner_id = fields.Many2one(comodel_name='res.partner', string='Partner')
    move_id = fields.Many2one(comodel_name='account.move', string='Account Move')
    invoice_id = fields.Many2one(comodel_name='account.invoice', string='Invoice')
    partner_shipping_id = fields.Many2one(comodel_name='res.partner', string='Shipping Partner')
    partner_invoice_id = fields.Many2one(comodel_name='res.partner', string='Invoice Partner')

    invoice_salesperson = fields.Char(string='Invoice Salesperson')
    sale_order_type = fields.Char(string='Sale Order Type', index=True)
    doc_no = fields.Char(string='Document Number')
    journal = fields.Char(string='Journal')
    customer_name = fields.Char(string='Customer', index=True)
    customer_salesperson = fields.Char(string='Customer Salesperson', index=True)
    customer_team = fields.Char(string='Customer Team', index=True)

    customer_shipping_name = fields.Char(string='Shipping Customer')
    customer_invoice_name = fields.Char(string='Invoice Customer')

    doc_date = fields.Date(string = 'Date')
    date_maturity = fields.Date(string = 'Date Maturity')
    debit = fields.Float(string = 'Debit')
    credit = fields.Float(string = 'Credit')
    balance = fields.Float(string = 'Balance')
    full_reconcile_id = fields.Integer(string = 'Full Reconcile Id')
    reconciled = fields.Boolean(string = 'Reconciled')
    pending_amount = fields.Float(string = 'Actual Pending')

    open_balance = fields.Float(string = 'As On 01/12/19')
    on_account = fields.Float(string = 'On Account')
    not_due = fields.Float(string = 'Not Due')
    age_30_days = fields.Float(string = '(0 - 30 days)')
    age_45_days = fields.Float(string = '(30 - 45 days)')
    age_60_days = fields.Float(string = '(45 - 60 days)')
    age_90_days = fields.Float(string = '(60 - 90 days)')
    age_120_days = fields.Float(string = '(90 - 120 days)')
    older = fields.Float(string = '(> 120 days)')

    payment_term = fields.Char(string = 'Payment Term')
    
    @api.model_cr
    def cron_update_report_customer_aged_os(self):
        self._cr.execute("TRUNCATE TABLE report_customer_aged_os RESTART IDENTITY;")
        self._cr.execute("""
            WITH account_report_standard_ledger_report_object
            AS (
                SELECT DISTINCT NOW() AS create_date,
                    aml.partner_id AS object_id,
                    CASE 
                        WHEN rep.ref IS NULL
                            THEN rep.name
                        ELSE rep.ref || ' ' || rep.name
                        END AS name,
                    NULL AS account_id,
                    aml.partner_id AS partner_id,
                    NULL AS journal_id,
                    NULL AS analytic_account_id
                FROM account_move_line aml
                JOIN res_partner rep ON (rep.id = aml.partner_id)
                ORDER BY name
                ),
            rps
            AS (
                SELECT id,
                    coalesce(parent_id, id) AS parent_id
                FROM res_partner
                ),
            matching_in_futur_before_init (id)
            AS (
                SELECT DISTINCT afr.id AS id
                FROM account_full_reconcile afr
                INNER JOIN account_move_line aml ON aml.full_reconcile_id = afr.id
                ),
            matching_in_futur_after_date_to (id)
            AS (
                SELECT DISTINCT afr.id AS id
                FROM account_full_reconcile afr
                INNER JOIN account_move_line aml ON aml.full_reconcile_id = afr.id
                ),
            date_range
            AS (
                SELECT DATE (now()) AS date_current,
                    DATE (now()) - INTEGER '30' AS date_less_30_days,
                    DATE (now()) - INTEGER '45' AS date_less_45_days,
                    DATE (now()) - INTEGER '60' AS date_less_60_days,
                    DATE (now()) - INTEGER '90' AS date_less_90_days,
                    DATE (now()) - INTEGER '120' AS date_less_120_days,
                    DATE (now()) - INTEGER '150' AS date_older
                ),
            query_as_per_program
            AS (
                SELECT NOW()::DATE AS create_date,
                    aml.account_id,
                    aml.analytic_account_id,
                    '2_line' AS TYPE,
                    'normal' AS type_view,
                    aml.journal_id,
                    aml.partner_id,
                    aml.move_id,
                    inv.id AS invoice_id,
                    rpa.name AS invoice_salesperson,
                    sot.name AS sale_order_type,
                    m.name AS doc_no,
                    j.name AS journal,
                    rp.name AS customer_name,
                    rpb.name AS customer_salesperson,
                    crm_team.name AS customer_team,
                    aml.id,
                    aml.DATE AS doc_date,
                    aml.date_maturity,
                    coalesce(apt.name,'') AS payment_term,
                    aml.debit,
                    aml.credit,
                    aml.balance,
                    aml.amount_residual,
                    aml.full_reconcile_id,
                    CASE 
                        WHEN aml.full_reconcile_id IS NOT NULL
                            AND NOT mifad.id IS NOT NULL
                            THEN TRUE
                        ELSE FALSE
                        END AS reconciled,
                    aml.amount_currency AS amount_currency,
                    aml.currency_id AS currency_id
                FROM account_report_standard_ledger_report_object ro
                INNER JOIN account_move_line aml ON aml.partner_id = ro.object_id
                LEFT JOIN account_journal j ON (aml.journal_id = j.id)
                LEFT JOIN account_account acc ON (aml.account_id = acc.id)
                LEFT JOIN account_account_type acc_type ON (acc.user_type_id = acc_type.id)
                LEFT JOIN account_move m ON (aml.move_id = m.id)
                LEFT JOIN matching_in_futur_before_init mif ON (aml.full_reconcile_id = mif.id)
                LEFT JOIN matching_in_futur_after_date_to mifad ON (aml.full_reconcile_id = mifad.id)
                LEFT JOIN rps ON rps.id = aml.partner_id
                LEFT JOIN res_partner rp ON rp.id = rps.parent_id
                LEFT JOIN crm_team ON rp.team_id = crm_team.id
                LEFT JOIN account_invoice inv ON aml.invoice_id = inv.id
                LEFT JOIN account_payment_term apt ON apt.id = inv.payment_term_id
                LEFT JOIN sale_order_type sot ON sot.id = inv.sale_type_id
                LEFT JOIN res_users rua ON rua.id = inv.user_id
                LEFT JOIN res_partner rpa ON rpa.id = rua.partner_id
                LEFT JOIN res_users rub ON rub.id = rp.user_id
                LEFT JOIN res_partner rpb ON rpb.id = rub.partner_id
                WHERE m.STATE IN ('posted')
                    AND (
                        acc.type_third_parties IN ('customer')
                        AND (
                            aml.full_reconcile_id IS NULL
                            OR mif.id IS NOT NULL
                            )
                        )
                    AND aml.DATE >= '2019-12-01'::DATE
                    AND NOT (
                        FALSE
                        AND acc.compacted = TRUE
                        )
                    AND (
                        FALSE
                        OR NOT (
                            aml.full_reconcile_id IS NOT NULL
                            AND NOT mifad.id IS NOT NULL
                            )
                        )
                ),
            remove_fully_reconciled
            AS (
                SELECT create_date,
                    account_id,
                    analytic_account_id,
                    journal_id,
                    partner_id,
                    move_id,
                    invoice_id,
                    invoice_salesperson,
                    sale_order_type,
                    doc_no,
                    journal,
                    customer_name,
                    customer_salesperson,
                    customer_team,
                    doc_date,
                    date_maturity,
                    payment_term,
                    debit,
                    credit,
                    balance,
                    amount_residual,
                    full_reconcile_id,
                    reconciled
                FROM query_as_per_program
                WHERE full_reconcile_id IS NULL
                ),
            actual_pending
            AS (
                SELECT create_date,
                    account_id,
                    analytic_account_id,
                    journal_id,
                    partner_id,
                    move_id,
                    invoice_id,
                    invoice_salesperson,
                    sale_order_type,
                    doc_no,
                    journal,
                    customer_name,
                    customer_salesperson,
                    customer_team,
                    doc_date,
                    date_maturity,
                    payment_term,
                    debit,
                    credit,
                    balance,
                    amount_residual,
                    CASE 
                        WHEN round(balance, 2) <> round(amount_residual, 2)
                            THEN round(amount_residual, 2)
                        ELSE round(balance, 2)
                        END AS pending_amount,
                    full_reconcile_id,
                    reconciled
                FROM remove_fully_reconciled
                ),
            final_result
            AS (
                SELECT ap.create_date,
                    ap.account_id,
                    ap.analytic_account_id,
                    ap.journal_id,
                    ap.partner_id,
                    inv.partner_shipping_id,
                    inv.partner_invoice_id,
                    ap.move_id,
                    ap.invoice_id,
                    ap.invoice_salesperson,
                    ap.sale_order_type,
                    ap.doc_no,
                    ap.journal,
                    ap.customer_name,
                    ap.customer_salesperson,
                    ap.customer_team,
                    rpa.name AS customer_shipping_name,
                    rpb.name AS customer_invoice_name,
                    ap.doc_date,
                    ap.date_maturity,
                    ap.payment_term,
                    ap.debit,
                    ap.credit,
                    ap.balance,
                    ap.amount_residual,
                    ap.pending_amount,
                    CASE 
                        WHEN position('OPBAL' IN ap.doc_no) > 0
                            THEN ap.pending_amount
                        END AS open_balance,
                    CASE 
                        WHEN (ap.invoice_id IS NULL
                            AND position('OPBAL' IN ap.doc_no) = 0
                            AND ap.pending_amount < 0)
                            OR (position('OPBAL' IN ap.doc_no) > 0
                            AND ap.pending_amount < 0)
                            THEN ap.pending_amount
                        END AS on_account,
                    CASE 
                        WHEN ap.date_maturity > date_range.date_current
                            AND (ap.invoice_id IS NOT NULL OR ap.pending_amount > 0)
                            THEN ap.pending_amount
                        END AS not_due,
                    CASE 
                        WHEN ap.date_maturity > date_range.date_less_30_days
                            AND ap.date_maturity <= date_range.date_current
                            AND (ap.invoice_id IS NOT NULL OR ap.pending_amount > 0)
                            THEN ap.pending_amount
                        END AS age_30_days,
                    CASE 
                        WHEN ap.date_maturity > date_range.date_less_45_days
                            AND ap.date_maturity <= date_range.date_less_30_days
                            AND (ap.invoice_id IS NOT NULL OR ap.pending_amount > 0)
                            THEN ap.pending_amount
                        END AS age_45_days,
                    CASE 
                        WHEN ap.date_maturity > date_range.date_less_60_days
                            AND ap.date_maturity <= date_range.date_less_45_days
                            AND (ap.invoice_id IS NOT NULL OR ap.pending_amount > 0)
                            THEN ap.pending_amount
                        END AS age_60_days,
                    CASE 
                        WHEN ap.date_maturity > date_range.date_less_90_days
                            AND ap.date_maturity <= date_range.date_less_60_days
                            AND (ap.invoice_id IS NOT NULL OR ap.pending_amount > 0)
                            THEN ap.pending_amount
                        END AS age_90_days,
                    CASE 
                        WHEN ap.date_maturity > date_range.date_less_120_days
                            AND ap.date_maturity <= date_range.date_less_90_days
                            AND (ap.invoice_id IS NOT NULL OR ap.pending_amount > 0)
                            THEN ap.pending_amount
                        END AS age_120_days,
                    CASE 
                        WHEN ap.date_maturity <= date_range.date_less_120_days
                            AND (ap.invoice_id IS NOT NULL OR ap.pending_amount > 0)
                            THEN ap.pending_amount
                        END AS older,
                    ap.full_reconcile_id,
                    ap.reconciled
                FROM date_range,
                    actual_pending ap
                    LEFT JOIN account_invoice inv ON ap.invoice_id = inv.id
                    LEFT JOIN res_partner rpa ON rpa.id = inv.partner_shipping_id
                    LEFT JOIN res_partner rpb ON rpb.id = inv.partner_invoice_id
                WHERE ap.pending_amount <> 0
                )
            INSERT INTO report_customer_aged_os (
                create_date,
                account_id,
                analytic_account_id,
                journal_id,
                partner_id,
                partner_shipping_id,
                partner_invoice_id,
                move_id,
                invoice_id,
                invoice_salesperson,
                sale_order_type,
                doc_no,
                journal,
                customer_name,
                customer_salesperson,
                customer_team,
                customer_shipping_name,
                customer_invoice_name,
                doc_date,
                date_maturity,
                payment_term,
                debit,
                credit,
                balance,
                pending_amount,
                full_reconcile_id,
                reconciled,
                open_balance,
                on_account,
                not_due,
                age_30_days,
                age_45_days,
                age_60_days,
                age_90_days,
                age_120_days,
                older
                )
            SELECT create_date,
                account_id,
                analytic_account_id,
                journal_id,
                partner_id,
                partner_shipping_id,
                partner_invoice_id,
                move_id,
                invoice_id,
                invoice_salesperson,
                sale_order_type,
                doc_no,
                journal,
                customer_name,
                customer_salesperson,
                customer_team,
                customer_shipping_name,
                customer_invoice_name,
                doc_date,
                date_maturity,
                payment_term,
                debit,
                credit,
                balance,
                pending_amount,
                full_reconcile_id,
                reconciled,
                open_balance,
                on_account,
                not_due,
                age_30_days,
                age_45_days,
                age_60_days,
                age_90_days,
                age_120_days,
                older
            FROM final_result;
            """)
        return True

