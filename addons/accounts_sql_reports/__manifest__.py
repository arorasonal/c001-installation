# -*- coding: utf-8 -*-
# See LICENSE file for full copyright and licensing details.

{
    'name': 'Accounts Sql Reports',
    'version': '10.0.1.0.0',
    'category': 'Reports',
    'depends': ['account'],
    'license': 'AGPL-3',
    'author': 'Ravi Krishnan',
    'maintainer': 'Ravi Krishnan',
    'summary': 'Accounting Reports based on SQL Views',
    'data': [
    "views/menu.xml",  
		"views/inter_warehouse_transfers_view.xml",
             ],
    'installable': True,
    'auto_install': False,
}
