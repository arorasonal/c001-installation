# -*- coding: utf-8 -*-
# Copyright 2019 Ravi Krishnan
# See LICENSE file for full copyright and licensing details.

from odoo import api, fields, models, tools

class InterWarehouseTransfers(models.Model):
    _name = "inter.warehouse.transfers"
    _auto = False


    invoice_no = fields.Char(string='Invoice #')
    delivery_date = fields.Date(string='Delivery Date')
    from_warehouse = fields.Char(string="From Warehouse")
    from_gstin = fields.Char(string="From GSTIN")
    to_warehouse = fields.Char(string="To Warehouse")
    to_gstin = fields.Char(string="To GSTIN")
    product = fields.Char(string="Product")
    category = fields.Char(string="Category")
    hsn_no = fields.Char(string="HSN")
    qty = fields.Float(string="Product Qty")
    amt = fields.Float(string="Amount")
    gst = fields.Float(string="GST Amt.")
    tot_amt = fields.Float(string="Total Amt.")

# stock.picking will be associated with barcode.transfer if stock.pircking is created from barcode.transfer
# barcode.transfer may have barcode.serial.line with no tax info to compute gst
# hence final gst may be null -- also reason for "LEFT JOIN serial_wise_tax b ON a.bsl_id = b.bsl_id"
# what if stock.picking is made by return of stock.picking
# report is then completely wrong
# also cancelled stock.pickings are not reported

# report was based on BT transanctions. this was wrong since at the time of dispatch, less materials could have been sent 
# hence sql completely rewrite of pick_bs which checks stock_move and then gets data 
# stock_move has data of actual dispatch of items

    @api.model_cr
    def init(self):
        print "Connected"
        tools.drop_view_if_exists(self._cr, 'inter_warehouse_transfers')
        self._cr.execute("""
            CREATE OR REPLACE VIEW inter_warehouse_transfers AS (
                WITH pick_bs AS (
                    SELECT
                        a.id AS spo_id,
                        b.id btl_id,
                        a.transfer_line_id,
                        a.ordered_qty,
                        a.product_qty AS qty,
                        a.picking_id,
                        b.rate,
                        a.product_qty * b.rate AS amt,
                        b.product_id,
                        c.origin,
                        date(c.date_done) AS delivery_date,
                        c.name,
                        c.location_id,
                        c.location_dest_id
                    FROM
                        stock_pack_operation a
                        JOIN barcode_transfer_line b ON b.id = a.transfer_line_id
                        JOIN stock_picking c ON a.picking_id = c.id
                        JOIN stock_picking_type d ON c.picking_type_id = d.id
                    WHERE
                        coalesce(d.inter_warehouse_transfer, FALSE)
                        AND c.STATE = 'done'
                        AND c.date_done > ('now'::TEXT::DATE - '62 days'::interval)
                ),
                tax_rates AS (
                    SELECT
                        id,
                        amount
                    FROM
                        account_tax
                    WHERE
                        amount_type = 'percent'
                        AND active
                    UNION ALL
                    SELECT
                        a.id,
                        c.amount
                    FROM
                        account_tax a
                        JOIN account_tax_filiation_rel b ON a.id = b.parent_tax
                        JOIN account_tax c ON b.child_tax = c.id
                    WHERE
                        a.amount_type = 'group'
                        AND a.active
                ),
                serial_line_tax AS (
                    SELECT
                        a.btl_id,
                        round((a.amt * c.amount / 100)) AS gst
                    FROM
                        pick_bs a
                        JOIN account_tax_barcode_transfer_line_rel b ON a.btl_id = b.barcode_transfer_line_id
                        JOIN tax_rates c ON b.account_tax_id = c.id
                ),
                serial_wise_tax AS (
                    SELECT
                        btl_id,
                        sum(gst) AS gst
                    FROM
                        serial_line_tax
                    GROUP BY
                        btl_id
                ),
                LOCATION AS (
                    SELECT
                        a.id,
                        a.complete_name,
                        b.name AS warehouse,
                        c.gstin
                    FROM
                        stock_location a
                    LEFT JOIN stock_warehouse b ON a.wr_id = b.id
                    JOIN res_partner c ON b.partner_id = c.id
                )
                SELECT
                    row_number() OVER () AS id,
                        a.name AS invoice_no,
                        a.delivery_date,
                        l.warehouse AS from_warehouse,
                        l.gstin AS from_gstin,
                        m.warehouse AS to_warehouse,
                        m.gstin AS to_gstin,
                        t.product_challan AS product,
                        t1.name AS category,
                        t1.hsn_no,
                        a.qty,
                        a.amt,
                        coalesce(b.gst, 0) AS gst,
                    a.amt + coalesce(b.gst, 0) AS tot_amt
                FROM
                    pick_bs a
                    LEFT JOIN serial_wise_tax b ON a.btl_id = b.btl_id
                    JOIN product_product p ON a.product_id = p.id
                    JOIN product_template t ON p.product_tmpl_id = t.id
                    LEFT JOIN product_category t1 ON t1.id = t.categ_id
                    LEFT JOIN LOCATION l ON l.id = a.location_id
                    LEFT JOIN LOCATION m ON m.id = a.location_dest_id
            )""")


