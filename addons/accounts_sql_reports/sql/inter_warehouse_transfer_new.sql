WITH pick_bs AS (
    SELECT
        a.id AS spo_id,
        b.id btl_id,
        a.transfer_line_id,
        a.ordered_qty,
        a.product_qty AS qty,
        a.picking_id,
        b.rate,
        a.product_qty * b.rate AS amt,
        b.product_id,
        c.origin,
        date(c.date_done) AS delivery_date,
        c.name,
        c.location_id,
        c.location_dest_id
    FROM
        stock_pack_operation a
        JOIN barcode_transfer_line b ON b.id = a.transfer_line_id
        JOIN stock_picking c ON a.picking_id = c.id
        JOIN stock_picking_type d ON c.picking_type_id = d.id
    WHERE
        coalesce(d.inter_warehouse_transfer, FALSE)
        AND c.STATE = 'done'
        AND c.date_done > ('now'::TEXT::DATE - '62 days'::interval)
),
tax_rates AS (
    SELECT
        id,
        amount
    FROM
        account_tax
    WHERE
        amount_type = 'percent'
        AND active
    UNION ALL
    SELECT
        a.id,
        c.amount
    FROM
        account_tax a
        JOIN account_tax_filiation_rel b ON a.id = b.parent_tax
        JOIN account_tax c ON b.child_tax = c.id
    WHERE
        a.amount_type = 'group'
        AND a.active
),
serial_line_tax AS (
    SELECT
        a.btl_id,
        round((a.amt * c.amount / 100)) AS gst
    FROM
        pick_bs a
        JOIN account_tax_barcode_transfer_line_rel b ON a.btl_id = b.barcode_transfer_line_id
        JOIN tax_rates c ON b.account_tax_id = c.id
),
serial_wise_tax AS (
    SELECT
        btl_id,
        sum(gst) AS gst
    FROM
        serial_line_tax
    GROUP BY
        btl_id
),
LOCATION AS (
    SELECT
        a.id,
        a.complete_name,
        b.name AS warehouse,
        c.gstin
    FROM
        stock_location a
    LEFT JOIN stock_warehouse b ON a.wr_id = b.id
    JOIN res_partner c ON b.partner_id = c.id
)
SELECT
    row_number() OVER () AS id,
        a.name AS invoice_no,
        a.delivery_date,
        l.warehouse AS from_warehouse,
        l.gstin AS from_gstin,
        m.warehouse AS to_warehouse,
        m.gstin AS to_gstin,
        t.product_challan AS product,
        t1.name AS category,
        t1.hsn_no,
        a.qty,
        a.amt,
        coalesce(b.gst, 0) AS gst,
    a.amt + coalesce(b.gst, 0) AS tot_amt
FROM
    pick_bs a
    LEFT JOIN serial_wise_tax b ON a.btl_id = b.btl_id
    JOIN product_product p ON a.product_id = p.id
    JOIN product_template t ON p.product_tmpl_id = t.id
    LEFT JOIN product_category t1 ON t1.id = t.categ_id
    LEFT JOIN LOCATION l ON l.id = a.location_id
    LEFT JOIN LOCATION m ON m.id = a.location_dest_id
