WITH pick_bs AS (
    SELECT
        a.id,
        a.origin,
        date(a.date_done) AS delivery_date,
        a.name,
        a.location_id,
        a.location_dest_id,
        d.id AS bsl_id,
        d.product_id,
        d.qty,
        d.rate,
        d.qty * d.rate AS amt
    FROM
        stock_picking a
        JOIN stock_picking_type b ON a.picking_type_id = b.id
        JOIN barcode_transfer c ON a.id = c.picking_id
        JOIN barcode_serial_line d ON d.barcode_transfer_id = c.id
    WHERE
        coalesce(b.inter_warehouse_transfer, FALSE)
        AND a.STATE = 'done'
        AND a.date_done > ('now'::TEXT::DATE - '62 days'::interval)
),
tax_rates AS (
    SELECT
        id,
        amount
    FROM
        account_tax
    WHERE
        amount_type = 'percent'
        AND active
    UNION ALL
    SELECT
        a.id,
        c.amount
    FROM
        account_tax a
        JOIN account_tax_filiation_rel b ON a.id = b.parent_tax
        JOIN account_tax c ON b.child_tax = c.id
    WHERE
        a.amount_type = 'group'
        AND a.active
),
serial_line_tax AS (
    SELECT
        a.bsl_id,
        round((a.amt * c.amount / 100)) AS gst
    FROM
        pick_bs a
        JOIN account_tax_barcode_serial_line_rel b ON a.bsl_id = b.barcode_serial_line_id
        JOIN tax_rates c ON b.account_tax_id = c.id
),
serial_wise_tax AS (
    SELECT
        bsl_id,
        sum(gst) AS gst
    FROM
        serial_line_tax
    GROUP BY
        bsl_id
),
LOCATION AS (
    SELECT
        a.id,
        a.complete_name,
        b.name AS warehouse,
        c.gstin
    FROM
        stock_location a
    LEFT JOIN stock_warehouse b ON a.wr_id = b.id
    JOIN res_partner c ON b.partner_id = c.id
)
SELECT
    row_number() OVER () AS id,
        a.name AS invoice_no,
        a.delivery_date,
        l.warehouse AS from_warehouse,
        l.gstin AS from_gstin,
        m.warehouse AS to_warehouse,
        m.gstin AS to_gstin,
        t.product_challan AS product,
        t1.name AS category,
        t1.hsn_no,
        a.qty,
        a.amt,
        coalesce(b.gst, 0) AS gst,
    a.amt + coalesce(b.gst, 0) AS tot_amt
FROM
    pick_bs a
    LEFT JOIN serial_wise_tax b ON a.bsl_id = b.bsl_id
    JOIN product_product p ON a.product_id = p.id
    JOIN product_template t ON p.product_tmpl_id = t.id
    LEFT JOIN product_category t1 ON t1.id = t.categ_id
    LEFT JOIN LOCATION l ON l.id = a.location_id
    LEFT JOIN LOCATION m ON m.id = a.location_dest_id
