WITH all_partner AS (
    SELECT
        id,
        coalesce(parent_id, id) AS parent_id,
        name
    FROM
        res_partner
)
SELECT
    aml.id,
    aml.payment_id,
    aml.invoice_id,
    aml.journal_id,
    aml.user_type_id,
    aml.partner_id,
    aml.account_id,
    aml.debit,
    aml.credit,
    aml.name,
    aj.name AS journal,
    aa.name AS account,
    rpb.name AS customer,
    ai.number AS invoice_number,
    ai.date AS invoice_date,
    ap.name AS payment_voucher_no,
    ap.payment_date
FROM
    account_move_line aml
    JOIN account_journal aj ON aml.journal_id = aj.id
    JOIN account_account aa ON aa.id = aml.account_id
    JOIN all_partner rpa ON rpa.id = aml.partner_id
    JOIN all_partner rpb ON rpa.parent_id = rpb.id
    LEFT JOIN account_invoice ai ON ai.id = aml.invoice_id
    LEFT JOIN account_payment ap ON ap.id = aml.payment_id
WHERE
    aml.partner_id IS NOT NULL
    AND aml.create_date::date > date('2019-11-30')
    AND aml.account_id = 3
    AND aml.full_reconcile_id IS NULL
ORDER BY
    rpb.name,
    aml.id;

