BEGIN;

-- CREATE TABLE "sale_order" -----------------------------------
CREATE TABLE "public"."sale_order" ( 
	"id" Integer DEFAULT nextval('sale_order_id_seq'::regclass) NOT NULL,
	"origin" Character Varying,
	"create_date" Timestamp Without Time Zone,
	"write_uid" Integer,
	"team_id" Integer,
	"client_order_ref" Character Varying,
	"date_order" Timestamp Without Time Zone NOT NULL,
	"partner_id" Integer NOT NULL,
	"note" Text,
	"procurement_group_id" Integer,
	"amount_untaxed" Numeric,
	"message_last_post" Timestamp Without Time Zone,
	"company_id" Integer,
	"amount_tax" Numeric,
	"state" Character Varying,
	"pricelist_id" Integer,
	"project_id" Integer,
	"create_uid" Integer,
	"confirmation_date" Timestamp Without Time Zone,
	"validity_date" Date,
	"payment_term_id" Integer,
	"write_date" Timestamp Without Time Zone,
	"partner_invoice_id" Integer NOT NULL,
	"fiscal_position_id" Integer,
	"amount_total" Numeric,
	"invoice_status" Character Varying,
	"name" Character Varying NOT NULL,
	"partner_shipping_id" Integer NOT NULL,
	"user_id" Integer,
	"campaign_id" Integer,
	"opportunity_id" Integer,
	"medium_id" Integer,
	"source_id" Integer,
	"picking_policy" Character Varying NOT NULL,
	"incoterm" Integer,
	"warehouse_id" Integer NOT NULL,
	"rule_group" Character Varying,
	"ignore_exception" Boolean,
	"main_exception_id" Integer,
	"code" Boolean,
	"road_permit" Character Varying,
	"minimum_contract_period" Character Varying,
	"contract_ref" Integer,
	"sale_type_id" Boolean,
	"odoo8_id" Integer,
	"po_ref" Character Varying,
	"rental_end_date" Timestamp Without Time Zone,
	"sales_manager_approved" Boolean,
	"contract_id8" Integer,
	"ship_date" Date,
	"reason_cancel_id" Integer,
	"imported" Boolean,
	"coordinator_approve" Boolean,
	"time_check" Boolean,
	"rental_start_date" Timestamp Without Time Zone,
	"is_company" Boolean,
	"type_id" Integer,
	"transporter_id" Character Varying,
	"source" Integer,
	"sub_suply" Character Varying,
	"vehicle_no" Character Varying,
	"generate_e_way_bill" Boolean,
	"transportation_mode" Integer,
	"destination" Integer,
	"distance" Double Precision,
	"transporter" Character Varying,
	"e_way_bill" Character Varying,
	"supply_type" Character Varying,
	"partner_order_id" Integer,
	"account_staff_id" Integer,
	"sale_order_task_field_id" Integer,
	PRIMARY KEY ( "id" ) );
 ;
-- -------------------------------------------------------------

-- CHANGE "COMMENT" OF "FIELD "origin" -------------------------
COMMENT ON COLUMN "public"."sale_order"."origin" IS 'Source Document';
-- -------------------------------------------------------------

-- CHANGE "COMMENT" OF "FIELD "create_date" --------------------
COMMENT ON COLUMN "public"."sale_order"."create_date" IS 'Creation Date';
-- -------------------------------------------------------------

-- CHANGE "COMMENT" OF "FIELD "write_uid" ----------------------
COMMENT ON COLUMN "public"."sale_order"."write_uid" IS 'Last Updated by';
-- -------------------------------------------------------------

-- CHANGE "COMMENT" OF "FIELD "team_id" ------------------------
COMMENT ON COLUMN "public"."sale_order"."team_id" IS 'Sales Team';
-- -------------------------------------------------------------

-- CHANGE "COMMENT" OF "FIELD "client_order_ref" ---------------
COMMENT ON COLUMN "public"."sale_order"."client_order_ref" IS 'Customer Reference';
-- -------------------------------------------------------------

-- CHANGE "COMMENT" OF "FIELD "date_order" ---------------------
COMMENT ON COLUMN "public"."sale_order"."date_order" IS 'Order Date';
-- -------------------------------------------------------------

-- CHANGE "COMMENT" OF "FIELD "partner_id" ---------------------
COMMENT ON COLUMN "public"."sale_order"."partner_id" IS 'Customer';
-- -------------------------------------------------------------

-- CHANGE "COMMENT" OF "FIELD "note" ---------------------------
COMMENT ON COLUMN "public"."sale_order"."note" IS 'Terms and conditions';
-- -------------------------------------------------------------

-- CHANGE "COMMENT" OF "FIELD "procurement_group_id" -----------
COMMENT ON COLUMN "public"."sale_order"."procurement_group_id" IS 'Procurement Group';
-- -------------------------------------------------------------

-- CHANGE "COMMENT" OF "FIELD "amount_untaxed" -----------------
COMMENT ON COLUMN "public"."sale_order"."amount_untaxed" IS 'Untaxed Amount';
-- -------------------------------------------------------------

-- CHANGE "COMMENT" OF "FIELD "message_last_post" --------------
COMMENT ON COLUMN "public"."sale_order"."message_last_post" IS 'Last Message Date';
-- -------------------------------------------------------------

-- CHANGE "COMMENT" OF "FIELD "company_id" ---------------------
COMMENT ON COLUMN "public"."sale_order"."company_id" IS 'Company';
-- -------------------------------------------------------------

-- CHANGE "COMMENT" OF "FIELD "amount_tax" ---------------------
COMMENT ON COLUMN "public"."sale_order"."amount_tax" IS 'Taxes';
-- -------------------------------------------------------------

-- CHANGE "COMMENT" OF "FIELD "state" --------------------------
COMMENT ON COLUMN "public"."sale_order"."state" IS 'Status';
-- -------------------------------------------------------------

-- CHANGE "COMMENT" OF "FIELD "pricelist_id" -------------------
COMMENT ON COLUMN "public"."sale_order"."pricelist_id" IS 'Pricelist';
-- -------------------------------------------------------------

-- CHANGE "COMMENT" OF "FIELD "project_id" ---------------------
COMMENT ON COLUMN "public"."sale_order"."project_id" IS 'Analytic Account';
-- -------------------------------------------------------------

-- CHANGE "COMMENT" OF "FIELD "create_uid" ---------------------
COMMENT ON COLUMN "public"."sale_order"."create_uid" IS 'Created by';
-- -------------------------------------------------------------

-- CHANGE "COMMENT" OF "FIELD "confirmation_date" --------------
COMMENT ON COLUMN "public"."sale_order"."confirmation_date" IS 'Confirmation Date';
-- -------------------------------------------------------------

-- CHANGE "COMMENT" OF "FIELD "validity_date" ------------------
COMMENT ON COLUMN "public"."sale_order"."validity_date" IS 'Expiration Date';
-- -------------------------------------------------------------

-- CHANGE "COMMENT" OF "FIELD "payment_term_id" ----------------
COMMENT ON COLUMN "public"."sale_order"."payment_term_id" IS 'Payment Terms';
-- -------------------------------------------------------------

-- CHANGE "COMMENT" OF "FIELD "write_date" ---------------------
COMMENT ON COLUMN "public"."sale_order"."write_date" IS 'Last Updated on';
-- -------------------------------------------------------------

-- CHANGE "COMMENT" OF "FIELD "partner_invoice_id" -------------
COMMENT ON COLUMN "public"."sale_order"."partner_invoice_id" IS 'Invoice Address';
-- -------------------------------------------------------------

-- CHANGE "COMMENT" OF "FIELD "fiscal_position_id" -------------
COMMENT ON COLUMN "public"."sale_order"."fiscal_position_id" IS 'Fiscal Position';
-- -------------------------------------------------------------

-- CHANGE "COMMENT" OF "FIELD "amount_total" -------------------
COMMENT ON COLUMN "public"."sale_order"."amount_total" IS 'Total';
-- -------------------------------------------------------------

-- CHANGE "COMMENT" OF "FIELD "invoice_status" -----------------
COMMENT ON COLUMN "public"."sale_order"."invoice_status" IS 'Invoice Status';
-- -------------------------------------------------------------

-- CHANGE "COMMENT" OF "FIELD "name" ---------------------------
COMMENT ON COLUMN "public"."sale_order"."name" IS 'Order Reference';
-- -------------------------------------------------------------

-- CHANGE "COMMENT" OF "FIELD "partner_shipping_id" ------------
COMMENT ON COLUMN "public"."sale_order"."partner_shipping_id" IS 'Delivery Address';
-- -------------------------------------------------------------

-- CHANGE "COMMENT" OF "FIELD "user_id" ------------------------
COMMENT ON COLUMN "public"."sale_order"."user_id" IS 'Salesperson';
-- -------------------------------------------------------------

-- CHANGE "COMMENT" OF "FIELD "campaign_id" --------------------
COMMENT ON COLUMN "public"."sale_order"."campaign_id" IS 'Campaign';
-- -------------------------------------------------------------

-- CHANGE "COMMENT" OF "FIELD "opportunity_id" -----------------
COMMENT ON COLUMN "public"."sale_order"."opportunity_id" IS 'Opportunity';
-- -------------------------------------------------------------

-- CHANGE "COMMENT" OF "FIELD "medium_id" ----------------------
COMMENT ON COLUMN "public"."sale_order"."medium_id" IS 'Medium';
-- -------------------------------------------------------------

-- CHANGE "COMMENT" OF "FIELD "source_id" ----------------------
COMMENT ON COLUMN "public"."sale_order"."source_id" IS 'Source';
-- -------------------------------------------------------------

-- CHANGE "COMMENT" OF "FIELD "picking_policy" -----------------
COMMENT ON COLUMN "public"."sale_order"."picking_policy" IS 'Shipping Policy';
-- -------------------------------------------------------------

-- CHANGE "COMMENT" OF "FIELD "incoterm" -----------------------
COMMENT ON COLUMN "public"."sale_order"."incoterm" IS 'Incoterms';
-- -------------------------------------------------------------

-- CHANGE "COMMENT" OF "FIELD "warehouse_id" -------------------
COMMENT ON COLUMN "public"."sale_order"."warehouse_id" IS 'Warehouse';
-- -------------------------------------------------------------

-- CHANGE "COMMENT" OF "FIELD "rule_group" ---------------------
COMMENT ON COLUMN "public"."sale_order"."rule_group" IS 'Rule group';
-- -------------------------------------------------------------

-- CHANGE "COMMENT" OF "FIELD "ignore_exception" ---------------
COMMENT ON COLUMN "public"."sale_order"."ignore_exception" IS 'Ignore Exceptions';
-- -------------------------------------------------------------

-- CHANGE "COMMENT" OF "FIELD "main_exception_id" --------------
COMMENT ON COLUMN "public"."sale_order"."main_exception_id" IS 'Main Exception';
-- -------------------------------------------------------------

-- CHANGE "COMMENT" OF "FIELD "code" ---------------------------
COMMENT ON COLUMN "public"."sale_order"."code" IS 'Code';
-- -------------------------------------------------------------

-- CHANGE "COMMENT" OF "FIELD "road_permit" --------------------
COMMENT ON COLUMN "public"."sale_order"."road_permit" IS 'Road Permit Availability';
-- -------------------------------------------------------------

-- CHANGE "COMMENT" OF "FIELD "minimum_contract_period" --------
COMMENT ON COLUMN "public"."sale_order"."minimum_contract_period" IS 'Minimum Contract Period';
-- -------------------------------------------------------------

-- CHANGE "COMMENT" OF "FIELD "contract_ref" -------------------
COMMENT ON COLUMN "public"."sale_order"."contract_ref" IS 'Contract Reference';
-- -------------------------------------------------------------

-- CHANGE "COMMENT" OF "FIELD "sale_type_id" -------------------
COMMENT ON COLUMN "public"."sale_order"."sale_type_id" IS 'Sales Type';
-- -------------------------------------------------------------

-- CHANGE "COMMENT" OF "FIELD "odoo8_id" -----------------------
COMMENT ON COLUMN "public"."sale_order"."odoo8_id" IS 'Odoo8 Id';
-- -------------------------------------------------------------

-- CHANGE "COMMENT" OF "FIELD "po_ref" -------------------------
COMMENT ON COLUMN "public"."sale_order"."po_ref" IS 'Po Number';
-- -------------------------------------------------------------

-- CHANGE "COMMENT" OF "FIELD "rental_end_date" ----------------
COMMENT ON COLUMN "public"."sale_order"."rental_end_date" IS 'Rental End Date';
-- -------------------------------------------------------------

-- CHANGE "COMMENT" OF "FIELD "sales_manager_approved" ---------
COMMENT ON COLUMN "public"."sale_order"."sales_manager_approved" IS 'Sales Manager Approved';
-- -------------------------------------------------------------

-- CHANGE "COMMENT" OF "FIELD "contract_id8" -------------------
COMMENT ON COLUMN "public"."sale_order"."contract_id8" IS 'Contract Id8';
-- -------------------------------------------------------------

-- CHANGE "COMMENT" OF "FIELD "ship_date" ----------------------
COMMENT ON COLUMN "public"."sale_order"."ship_date" IS 'Ship Date';
-- -------------------------------------------------------------

-- CHANGE "COMMENT" OF "FIELD "reason_cancel_id" ---------------
COMMENT ON COLUMN "public"."sale_order"."reason_cancel_id" IS 'Reason for Cancel';
-- -------------------------------------------------------------

-- CHANGE "COMMENT" OF "FIELD "imported" -----------------------
COMMENT ON COLUMN "public"."sale_order"."imported" IS 'Imported';
-- -------------------------------------------------------------

-- CHANGE "COMMENT" OF "FIELD "coordinator_approve" ------------
COMMENT ON COLUMN "public"."sale_order"."coordinator_approve" IS 'Sales Approved';
-- -------------------------------------------------------------

-- CHANGE "COMMENT" OF "FIELD "time_check" ---------------------
COMMENT ON COLUMN "public"."sale_order"."time_check" IS 'Time Updated for Sales Order';
-- -------------------------------------------------------------

-- CHANGE "COMMENT" OF "FIELD "rental_start_date" --------------
COMMENT ON COLUMN "public"."sale_order"."rental_start_date" IS 'Rental Start Date';
-- -------------------------------------------------------------

-- CHANGE "COMMENT" OF "FIELD "is_company" ---------------------
COMMENT ON COLUMN "public"."sale_order"."is_company" IS 'Is company';
-- -------------------------------------------------------------

-- CHANGE "COMMENT" OF "FIELD "type_id" ------------------------
COMMENT ON COLUMN "public"."sale_order"."type_id" IS 'Type';
-- -------------------------------------------------------------

-- CHANGE "COMMENT" OF "FIELD "transporter_id" -----------------
COMMENT ON COLUMN "public"."sale_order"."transporter_id" IS 'Transporter Id';
-- -------------------------------------------------------------

-- CHANGE "COMMENT" OF "FIELD "source" -------------------------
COMMENT ON COLUMN "public"."sale_order"."source" IS 'Source';
-- -------------------------------------------------------------

-- CHANGE "COMMENT" OF "FIELD "sub_suply" ----------------------
COMMENT ON COLUMN "public"."sale_order"."sub_suply" IS 'Sub Supply Type';
-- -------------------------------------------------------------

-- CHANGE "COMMENT" OF "FIELD "vehicle_no" ---------------------
COMMENT ON COLUMN "public"."sale_order"."vehicle_no" IS 'Vehicle Number';
-- -------------------------------------------------------------

-- CHANGE "COMMENT" OF "FIELD "generate_e_way_bill" ------------
COMMENT ON COLUMN "public"."sale_order"."generate_e_way_bill" IS 'Generate E-Way Bill';
-- -------------------------------------------------------------

-- CHANGE "COMMENT" OF "FIELD "transportation_mode" ------------
COMMENT ON COLUMN "public"."sale_order"."transportation_mode" IS 'Transportation Mode';
-- -------------------------------------------------------------

-- CHANGE "COMMENT" OF "FIELD "destination" --------------------
COMMENT ON COLUMN "public"."sale_order"."destination" IS 'Destination';
-- -------------------------------------------------------------

-- CHANGE "COMMENT" OF "FIELD "distance" -----------------------
COMMENT ON COLUMN "public"."sale_order"."distance" IS 'Distance(KM)';
-- -------------------------------------------------------------

-- CHANGE "COMMENT" OF "FIELD "transporter" --------------------
COMMENT ON COLUMN "public"."sale_order"."transporter" IS 'Transporter';
-- -------------------------------------------------------------

-- CHANGE "COMMENT" OF "FIELD "e_way_bill" ---------------------
COMMENT ON COLUMN "public"."sale_order"."e_way_bill" IS 'E-Way Bill Number';
-- -------------------------------------------------------------

-- CHANGE "COMMENT" OF "FIELD "supply_type" --------------------
COMMENT ON COLUMN "public"."sale_order"."supply_type" IS 'Supply Type';
-- -------------------------------------------------------------

-- CHANGE "COMMENT" OF "FIELD "partner_order_id" ---------------
COMMENT ON COLUMN "public"."sale_order"."partner_order_id" IS 'Ordering Contact';
-- -------------------------------------------------------------

-- CHANGE "COMMENT" OF "FIELD "account_staff_id" ---------------
COMMENT ON COLUMN "public"."sale_order"."account_staff_id" IS 'Account Staff';
-- -------------------------------------------------------------

-- CHANGE "COMMENT" OF "FIELD "sale_order_task_field_id" -------
COMMENT ON COLUMN "public"."sale_order"."sale_order_task_field_id" IS 'Sale Order Task';
-- -------------------------------------------------------------

-- CHANGE "COMMENT" OF "TABLE "sale_order" ---------------------
COMMENT ON TABLE  "public"."sale_order" IS 'Sales Order';
-- -------------------------------------------------------------

-- CREATE INDEX "sale_order_confirmation_date_index" -----------
CREATE INDEX "sale_order_confirmation_date_index" ON "public"."sale_order" USING btree( "confirmation_date" Asc NULLS Last );
-- -------------------------------------------------------------

-- CREATE INDEX "sale_order_create_date_index" -----------------
CREATE INDEX "sale_order_create_date_index" ON "public"."sale_order" USING btree( "create_date" Asc NULLS Last );
-- -------------------------------------------------------------

-- CREATE INDEX "sale_order_date_order_index" ------------------
CREATE INDEX "sale_order_date_order_index" ON "public"."sale_order" USING btree( "date_order" Asc NULLS Last );
-- -------------------------------------------------------------

-- CREATE INDEX "sale_order_name_index" ------------------------
CREATE INDEX "sale_order_name_index" ON "public"."sale_order" USING btree( "name" Asc NULLS Last );
-- -------------------------------------------------------------

-- CREATE INDEX "sale_order_partner_id_index" ------------------
CREATE INDEX "sale_order_partner_id_index" ON "public"."sale_order" USING btree( "partner_id" Asc NULLS Last );
-- -------------------------------------------------------------

-- CREATE INDEX "sale_order_reason_cancel_id_index" ------------
CREATE INDEX "sale_order_reason_cancel_id_index" ON "public"."sale_order" USING btree( "reason_cancel_id" Asc NULLS Last );
-- -------------------------------------------------------------

-- CREATE INDEX "sale_order_state_index" -----------------------
CREATE INDEX "sale_order_state_index" ON "public"."sale_order" USING btree( "state" Asc NULLS Last );
-- -------------------------------------------------------------

-- CREATE INDEX "sale_order_user_id_index" ---------------------
CREATE INDEX "sale_order_user_id_index" ON "public"."sale_order" USING btree( "user_id" Asc NULLS Last );
-- -------------------------------------------------------------

COMMIT;

BEGIN;

-- CREATE LINK "sale_order_account_staff_id_fkey" --------------
ALTER TABLE "public"."sale_order"
	ADD CONSTRAINT "sale_order_account_staff_id_fkey" FOREIGN KEY ( "account_staff_id" )
	REFERENCES "public"."res_users" ( "id" ) MATCH SIMPLE
	ON DELETE Set NULL
	ON UPDATE No Action;
-- -------------------------------------------------------------

COMMIT;

BEGIN;

-- CREATE LINK "sale_order_campaign_id_fkey" -------------------
ALTER TABLE "public"."sale_order"
	ADD CONSTRAINT "sale_order_campaign_id_fkey" FOREIGN KEY ( "campaign_id" )
	REFERENCES "public"."utm_campaign" ( "id" ) MATCH SIMPLE
	ON DELETE Set NULL
	ON UPDATE No Action;
-- -------------------------------------------------------------

COMMIT;

BEGIN;

-- CREATE LINK "sale_order_company_id_fkey" --------------------
ALTER TABLE "public"."sale_order"
	ADD CONSTRAINT "sale_order_company_id_fkey" FOREIGN KEY ( "company_id" )
	REFERENCES "public"."res_company" ( "id" ) MATCH SIMPLE
	ON DELETE Set NULL
	ON UPDATE No Action;
-- -------------------------------------------------------------

COMMIT;

BEGIN;

-- CREATE LINK "sale_order_contract_ref_fkey" ------------------
ALTER TABLE "public"."sale_order"
	ADD CONSTRAINT "sale_order_contract_ref_fkey" FOREIGN KEY ( "contract_ref" )
	REFERENCES "public"."account_analytic_account" ( "id" ) MATCH SIMPLE
	ON DELETE Set NULL
	ON UPDATE No Action;
-- -------------------------------------------------------------

COMMIT;

BEGIN;

-- CREATE LINK "sale_order_create_uid_fkey" --------------------
ALTER TABLE "public"."sale_order"
	ADD CONSTRAINT "sale_order_create_uid_fkey" FOREIGN KEY ( "create_uid" )
	REFERENCES "public"."res_users" ( "id" ) MATCH SIMPLE
	ON DELETE Set NULL
	ON UPDATE No Action;
-- -------------------------------------------------------------

COMMIT;

BEGIN;

-- CREATE LINK "sale_order_destination_fkey" -------------------
ALTER TABLE "public"."sale_order"
	ADD CONSTRAINT "sale_order_destination_fkey" FOREIGN KEY ( "destination" )
	REFERENCES "public"."res_country_state" ( "id" ) MATCH SIMPLE
	ON DELETE Set NULL
	ON UPDATE No Action;
-- -------------------------------------------------------------

COMMIT;

BEGIN;

-- CREATE LINK "sale_order_fiscal_position_id_fkey" ------------
ALTER TABLE "public"."sale_order"
	ADD CONSTRAINT "sale_order_fiscal_position_id_fkey" FOREIGN KEY ( "fiscal_position_id" )
	REFERENCES "public"."account_fiscal_position" ( "id" ) MATCH SIMPLE
	ON DELETE Set NULL
	ON UPDATE No Action;
-- -------------------------------------------------------------

COMMIT;

BEGIN;

-- CREATE LINK "sale_order_incoterm_fkey" ----------------------
ALTER TABLE "public"."sale_order"
	ADD CONSTRAINT "sale_order_incoterm_fkey" FOREIGN KEY ( "incoterm" )
	REFERENCES "public"."stock_incoterms" ( "id" ) MATCH SIMPLE
	ON DELETE Set NULL
	ON UPDATE No Action;
-- -------------------------------------------------------------

COMMIT;

BEGIN;

-- CREATE LINK "sale_order_main_exception_id_fkey" -------------
ALTER TABLE "public"."sale_order"
	ADD CONSTRAINT "sale_order_main_exception_id_fkey" FOREIGN KEY ( "main_exception_id" )
	REFERENCES "public"."exception_rule" ( "id" ) MATCH SIMPLE
	ON DELETE Set NULL
	ON UPDATE No Action;
-- -------------------------------------------------------------

COMMIT;

BEGIN;

-- CREATE LINK "sale_order_medium_id_fkey" ---------------------
ALTER TABLE "public"."sale_order"
	ADD CONSTRAINT "sale_order_medium_id_fkey" FOREIGN KEY ( "medium_id" )
	REFERENCES "public"."utm_medium" ( "id" ) MATCH SIMPLE
	ON DELETE Set NULL
	ON UPDATE No Action;
-- -------------------------------------------------------------

COMMIT;

BEGIN;

-- CREATE LINK "sale_order_opportunity_id_fkey" ----------------
ALTER TABLE "public"."sale_order"
	ADD CONSTRAINT "sale_order_opportunity_id_fkey" FOREIGN KEY ( "opportunity_id" )
	REFERENCES "public"."crm_lead" ( "id" ) MATCH SIMPLE
	ON DELETE Set NULL
	ON UPDATE No Action;
-- -------------------------------------------------------------

COMMIT;

BEGIN;

-- CREATE LINK "sale_order_partner_id_fkey" --------------------
ALTER TABLE "public"."sale_order"
	ADD CONSTRAINT "sale_order_partner_id_fkey" FOREIGN KEY ( "partner_id" )
	REFERENCES "public"."res_partner" ( "id" ) MATCH SIMPLE
	ON DELETE Set NULL
	ON UPDATE No Action;
-- -------------------------------------------------------------

COMMIT;

BEGIN;

-- CREATE LINK "sale_order_partner_invoice_id_fkey" ------------
ALTER TABLE "public"."sale_order"
	ADD CONSTRAINT "sale_order_partner_invoice_id_fkey" FOREIGN KEY ( "partner_invoice_id" )
	REFERENCES "public"."res_partner" ( "id" ) MATCH SIMPLE
	ON DELETE Set NULL
	ON UPDATE No Action;
-- -------------------------------------------------------------

COMMIT;

BEGIN;

-- CREATE LINK "sale_order_partner_order_id_fkey" --------------
ALTER TABLE "public"."sale_order"
	ADD CONSTRAINT "sale_order_partner_order_id_fkey" FOREIGN KEY ( "partner_order_id" )
	REFERENCES "public"."res_partner" ( "id" ) MATCH SIMPLE
	ON DELETE Set NULL
	ON UPDATE No Action;
-- -------------------------------------------------------------

COMMIT;

BEGIN;

-- CREATE LINK "sale_order_partner_shipping_id_fkey" -----------
ALTER TABLE "public"."sale_order"
	ADD CONSTRAINT "sale_order_partner_shipping_id_fkey" FOREIGN KEY ( "partner_shipping_id" )
	REFERENCES "public"."res_partner" ( "id" ) MATCH SIMPLE
	ON DELETE Set NULL
	ON UPDATE No Action;
-- -------------------------------------------------------------

COMMIT;

BEGIN;

-- CREATE LINK "sale_order_payment_term_id_fkey" ---------------
ALTER TABLE "public"."sale_order"
	ADD CONSTRAINT "sale_order_payment_term_id_fkey" FOREIGN KEY ( "payment_term_id" )
	REFERENCES "public"."account_payment_term" ( "id" ) MATCH SIMPLE
	ON DELETE Set NULL
	ON UPDATE No Action;
-- -------------------------------------------------------------

COMMIT;

BEGIN;

-- CREATE LINK "sale_order_pricelist_id_fkey" ------------------
ALTER TABLE "public"."sale_order"
	ADD CONSTRAINT "sale_order_pricelist_id_fkey" FOREIGN KEY ( "pricelist_id" )
	REFERENCES "public"."product_pricelist" ( "id" ) MATCH SIMPLE
	ON DELETE Set NULL
	ON UPDATE No Action;
-- -------------------------------------------------------------

COMMIT;

BEGIN;

-- CREATE LINK "sale_order_procurement_group_id_fkey" ----------
ALTER TABLE "public"."sale_order"
	ADD CONSTRAINT "sale_order_procurement_group_id_fkey" FOREIGN KEY ( "procurement_group_id" )
	REFERENCES "public"."procurement_group" ( "id" ) MATCH SIMPLE
	ON DELETE Set NULL
	ON UPDATE No Action;
-- -------------------------------------------------------------

COMMIT;

BEGIN;

-- CREATE LINK "sale_order_project_id_fkey" --------------------
ALTER TABLE "public"."sale_order"
	ADD CONSTRAINT "sale_order_project_id_fkey" FOREIGN KEY ( "project_id" )
	REFERENCES "public"."account_analytic_account" ( "id" ) MATCH SIMPLE
	ON DELETE Set NULL
	ON UPDATE No Action;
-- -------------------------------------------------------------

COMMIT;

BEGIN;

-- CREATE LINK "sale_order_reason_cancel_id_fkey" --------------
ALTER TABLE "public"."sale_order"
	ADD CONSTRAINT "sale_order_reason_cancel_id_fkey" FOREIGN KEY ( "reason_cancel_id" )
	REFERENCES "public"."crm_lost_reason" ( "id" ) MATCH SIMPLE
	ON DELETE Set NULL
	ON UPDATE No Action;
-- -------------------------------------------------------------

COMMIT;

BEGIN;

-- CREATE LINK "sale_order_sale_order_task_field_id_fkey" ------
ALTER TABLE "public"."sale_order"
	ADD CONSTRAINT "sale_order_sale_order_task_field_id_fkey" FOREIGN KEY ( "sale_order_task_field_id" )
	REFERENCES "public"."project_task" ( "id" ) MATCH SIMPLE
	ON DELETE Set NULL
	ON UPDATE No Action;
-- -------------------------------------------------------------

COMMIT;

BEGIN;

-- CREATE LINK "sale_order_source_fkey" ------------------------
ALTER TABLE "public"."sale_order"
	ADD CONSTRAINT "sale_order_source_fkey" FOREIGN KEY ( "source" )
	REFERENCES "public"."res_country_state" ( "id" ) MATCH SIMPLE
	ON DELETE Set NULL
	ON UPDATE No Action;
-- -------------------------------------------------------------

COMMIT;

BEGIN;

-- CREATE LINK "sale_order_source_id_fkey" ---------------------
ALTER TABLE "public"."sale_order"
	ADD CONSTRAINT "sale_order_source_id_fkey" FOREIGN KEY ( "source_id" )
	REFERENCES "public"."utm_source" ( "id" ) MATCH SIMPLE
	ON DELETE Set NULL
	ON UPDATE No Action;
-- -------------------------------------------------------------

COMMIT;

BEGIN;

-- CREATE LINK "sale_order_team_id_fkey" -----------------------
ALTER TABLE "public"."sale_order"
	ADD CONSTRAINT "sale_order_team_id_fkey" FOREIGN KEY ( "team_id" )
	REFERENCES "public"."crm_team" ( "id" ) MATCH SIMPLE
	ON DELETE Set NULL
	ON UPDATE No Action;
-- -------------------------------------------------------------

COMMIT;

BEGIN;

-- CREATE LINK "sale_order_transportation_mode_fkey" -----------
ALTER TABLE "public"."sale_order"
	ADD CONSTRAINT "sale_order_transportation_mode_fkey" FOREIGN KEY ( "transportation_mode" )
	REFERENCES "public"."eway_bill_trans" ( "id" ) MATCH SIMPLE
	ON DELETE Set NULL
	ON UPDATE No Action;
-- -------------------------------------------------------------

COMMIT;

BEGIN;

-- CREATE LINK "sale_order_type_id_fkey" -----------------------
ALTER TABLE "public"."sale_order"
	ADD CONSTRAINT "sale_order_type_id_fkey" FOREIGN KEY ( "type_id" )
	REFERENCES "public"."sale_order_type" ( "id" ) MATCH SIMPLE
	ON DELETE Set NULL
	ON UPDATE No Action;
-- -------------------------------------------------------------

COMMIT;

BEGIN;

-- CREATE LINK "sale_order_user_id_fkey" -----------------------
ALTER TABLE "public"."sale_order"
	ADD CONSTRAINT "sale_order_user_id_fkey" FOREIGN KEY ( "user_id" )
	REFERENCES "public"."res_users" ( "id" ) MATCH SIMPLE
	ON DELETE Set NULL
	ON UPDATE No Action;
-- -------------------------------------------------------------

COMMIT;

BEGIN;

-- CREATE LINK "sale_order_warehouse_id_fkey" ------------------
ALTER TABLE "public"."sale_order"
	ADD CONSTRAINT "sale_order_warehouse_id_fkey" FOREIGN KEY ( "warehouse_id" )
	REFERENCES "public"."stock_warehouse" ( "id" ) MATCH SIMPLE
	ON DELETE Set NULL
	ON UPDATE No Action;
-- -------------------------------------------------------------

COMMIT;

BEGIN;

-- CREATE LINK "sale_order_write_uid_fkey" ---------------------
ALTER TABLE "public"."sale_order"
	ADD CONSTRAINT "sale_order_write_uid_fkey" FOREIGN KEY ( "write_uid" )
	REFERENCES "public"."res_users" ( "id" ) MATCH SIMPLE
	ON DELETE Set NULL
	ON UPDATE No Action;
-- -------------------------------------------------------------

COMMIT;
