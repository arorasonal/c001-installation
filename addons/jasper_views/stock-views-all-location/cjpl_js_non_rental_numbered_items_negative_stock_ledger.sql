-- View: jasper.cjpl_js_non_rental_numbered_items_negative_stock_ledger

DROP VIEW IF EXISTS jasper.cjpl_js_non_rental_numbered_items_negative_stock_ledger;

CREATE OR REPLACE VIEW jasper.cjpl_js_non_rental_numbered_items_negative_stock_ledger
 AS
 WITH rental_loc_name AS (
         SELECT stock_location.id,
            "substring"(stock_location.complete_name::text, "position"(stock_location.complete_name::text, '/'::text) + 1) AS location_name
           FROM stock_location
          WHERE stock_location.complete_name::text !~~ '%Rent%'::text AND stock_location.usage::text = 'internal'::text
        ), out_move AS (
         SELECT m.product_id,
            m.location_id AS loc_id,
            sum(0) AS in_qty,
            sum(m.product_qty) AS out_qty
           FROM stock_move m
          WHERE m.state::text ~~ 'done'::text AND (m.location_id IN ( SELECT rental_loc_name.id
                   FROM rental_loc_name))
          GROUP BY m.product_id, m.location_id
        ), in_move AS (
         SELECT m.product_id,
            m.location_dest_id AS loc_id,
            sum(m.product_qty) AS in_qty,
            sum(0) AS out_qty
           FROM stock_move m
          WHERE m.state::text ~~ 'done'::text AND (m.location_dest_id IN ( SELECT rental_loc_name.id
                   FROM rental_loc_name))
          GROUP BY m.product_id, m.location_dest_id
        ), all_move AS (
         SELECT a.product_id,
            a.loc_id,
            a.in_qty,
            a.out_qty
           FROM in_move a
        UNION
         SELECT b.product_id,
            b.loc_id,
            b.in_qty,
            b.out_qty
           FROM out_move b
        ), sum_move AS (
         SELECT all_move.product_id,
            all_move.loc_id,
            sum(all_move.in_qty) AS in_qty,
            sum(all_move.out_qty) AS out_qty
           FROM all_move
          GROUP BY all_move.product_id, all_move.loc_id
        ), negative_stock_id AS (
         SELECT a.loc_id,
            a.product_id
           FROM sum_move a
             JOIN product_product p_1 ON a.product_id = p_1.id
             JOIN product_template t_1 ON p_1.product_tmpl_id = t_1.id
             JOIN rental_loc_name l_1 ON a.loc_id = l_1.id
             JOIN product_category pc_1 ON t_1.categ_id = pc_1.id
          WHERE (a.in_qty - a.out_qty) < 0::numeric AND pc_1.numbered = true
        ), out_move_dtl AS (
         SELECT m.id,
            m.product_id,
            m.picking_id,
            m.date,
            m.origin,
            m.location_id AS loc_id,
            0 AS in_qty,
            m.product_qty AS out_qty
           FROM stock_move m
             JOIN negative_stock_id ON m.product_id = negative_stock_id.product_id AND m.location_id = negative_stock_id.loc_id
          WHERE m.state::text ~~ 'done'::text
        ), in_move_dtl AS (
         SELECT m.id,
            m.product_id,
            m.location_dest_id AS loc_id,
            m.picking_id,
            m.date,
            m.origin,
            m.product_qty AS in_qty,
            0 AS out_qty
           FROM stock_move m
             JOIN negative_stock_id ON m.product_id = negative_stock_id.product_id AND m.location_dest_id = negative_stock_id.loc_id
          WHERE m.state::text ~~ 'done'::text
        ), all_move_dtl AS (
         SELECT a.id,
            a.product_id,
            a.loc_id,
            a.date,
            a.origin,
            a.picking_id,
            a.in_qty,
            a.out_qty
           FROM in_move_dtl a
        UNION
         SELECT b.id,
            b.product_id,
            b.loc_id,
            b.date,
            b.origin,
            b.picking_id,
            b.in_qty,
            b.out_qty
           FROM out_move_dtl b
        )
 SELECT aml.id,
    aml.product_id,
    aml.loc_id,
    aml.date,
    aml.origin,
    aml.picking_id,
    aml.in_qty,
    aml.out_qty,
    sum(aml.in_qty - aml.out_qty) OVER (PARTITION BY aml.loc_id, aml.product_id ORDER BY aml.date) AS bal_qty,
    l.location_name AS location,
    pc.name AS category_name,
    t.name AS product_name,
    t.product_challan,
    sp.name
   FROM all_move_dtl aml
     LEFT JOIN stock_picking sp ON aml.picking_id = sp.id
     JOIN product_product p ON aml.product_id = p.id
     JOIN product_template t ON p.product_tmpl_id = t.id
     JOIN rental_loc_name l ON aml.loc_id = l.id
     JOIN product_category pc ON t.categ_id = pc.id
  ORDER BY aml.loc_id, aml.product_id, aml.date;

ALTER TABLE jasper.cjpl_js_non_rental_numbered_items_negative_stock_ledger
    OWNER TO postgres;


