-- View: jasper.cjpl_js_non_rental_numbered_items_negative_stock_summary

DROP VIEW IF EXISTS jasper.cjpl_js_rental_numbered_items_negative_stock_summary;

CREATE OR REPLACE VIEW jasper.cjpl_js_rental_numbered_items_negative_stock_summary
 AS
 WITH rental_loc_name AS (
         SELECT stock_location.id,
            "substring"(stock_location.complete_name::text, "position"(stock_location.complete_name::text, '/'::text) + 1) AS location_name
           FROM stock_location
          WHERE stock_location.complete_name::text ~~ '%Rent%'::text AND stock_location.usage::text = 'internal'::text
        ), out_move AS (
         SELECT m.product_id,
            m.location_id AS loc_id,
            sum(0) AS in_qty,
            sum(m.product_qty) AS out_qty
           FROM stock_move m
          WHERE m.state::text ~~ 'done'::text AND (m.location_id IN ( SELECT rental_loc_name.id
                   FROM rental_loc_name))
          GROUP BY m.product_id, m.location_id
        ), in_move AS (
         SELECT m.product_id,
            m.location_dest_id AS loc_id,
            sum(m.product_qty) AS in_qty,
            sum(0) AS out_qty
           FROM stock_move m
          WHERE m.state::text ~~ 'done'::text AND (m.location_dest_id IN ( SELECT rental_loc_name.id
                   FROM rental_loc_name))
          GROUP BY m.product_id, m.location_dest_id
        ), all_move AS (
         SELECT a_1.product_id,
            a_1.loc_id,
            a_1.in_qty,
            a_1.out_qty
           FROM in_move a_1
        UNION
         SELECT b.product_id,
            b.loc_id,
            b.in_qty,
            b.out_qty
           FROM out_move b
        ), sum_move AS (
         SELECT all_move.product_id,
            all_move.loc_id,
            sum(all_move.in_qty) AS in_qty,
            sum(all_move.out_qty) AS out_qty
           FROM all_move
          GROUP BY all_move.product_id, all_move.loc_id
        )
 SELECT a.loc_id,
    a.product_id,
    l.location_name AS location,
    pc.name AS category_name,
    t.name AS product_name,
    t.product_challan
   FROM sum_move a
     JOIN product_product p ON a.product_id = p.id
     JOIN product_template t ON p.product_tmpl_id = t.id
     JOIN rental_loc_name l ON a.loc_id = l.id
     JOIN product_category pc ON t.categ_id = pc.id
  WHERE (a.in_qty - a.out_qty) < 0::numeric AND pc.numbered = true;

ALTER TABLE jasper.cjpl_js_rental_numbered_items_negative_stock_summary
    OWNER TO postgres;


