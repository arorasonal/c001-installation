-- View: jasper.cjpl_js_hierarchical_sales_team

-- DROP VIEW jasper.cjpl_js_hierarchical_sales_team;

CREATE OR REPLACE VIEW jasper.cjpl_js_hierarchical_sales_team
 AS
 WITH RECURSIVE team_hierarchy AS (
         SELECT crm_team.id,
            crm_team.name,
            crm_team.parent_team,
            crm_team.user_id,
            0 AS level,
            'root'::character varying(100) AS path
           FROM crm_team
          WHERE crm_team.active AND crm_team.parent_team IS NULL AND crm_team.type_team::text = 'sale'::text
        UNION ALL
         SELECT e.id,
            e.name,
            e.parent_team,
            e.user_id,
            d.level + 1,
            (((d.path::text || '/'::text) || "right"('000000000'::text || e.id::character varying(10)::text, 10)))::character varying(100) AS path
           FROM crm_team e
             JOIN team_hierarchy d ON d.id = e.parent_team
          WHERE e.active AND e.type_team::text = 'sale'::text
        )
 SELECT ('|'::text || repeat('----'::text, th.level)) || th.name::text AS team_name,
    tln.name AS team_lead_name,
    tln.login AS team_lead_login,
    un.name AS member_name,
    un.login AS member_login
   FROM team_hierarchy th
     JOIN view_cjpl_user_name tln ON tln.id = th.user_id
     LEFT JOIN rel_team_users rel ON th.id = rel.team_id
     JOIN view_cjpl_user_name un ON un.id = rel.user_id
  WHERE tln.name::text <> un.name::text
  ORDER BY th.path;

ALTER TABLE jasper.cjpl_js_hierarchical_sales_team
    OWNER TO reporter;

GRANT ALL ON TABLE jasper.cjpl_js_hierarchical_sales_team TO postgres;
GRANT ALL ON TABLE jasper.cjpl_js_hierarchical_sales_team TO odoo;
GRANT ALL ON TABLE jasper.cjpl_js_hierarchical_sales_team TO maintainer;
GRANT ALL ON TABLE jasper.cjpl_js_hierarchical_sales_team TO reporter;

