-- View: public.cjpl_js_duplicate_contacts

DROP VIEW public.cjpl_js_duplicate_contacts;

CREATE OR REPLACE VIEW public.cjpl_js_duplicate_contacts
 AS
 WITH all_cap AS (
         SELECT upper(res_partner_1.name::text) AS up_name
           FROM res_partner res_partner_1
          WHERE res_partner_1.is_company AND res_partner_1.active
        ), dupl AS (
         SELECT all_cap.up_name
           FROM all_cap
          GROUP BY all_cap.up_name
         HAVING count(*) > 1
        )
 SELECT res_partner.id,
    res_partner.name,
    res_partner.active,
    res_partner.is_company,
    res_partner.street,
    res_partner.street2,
    res_partner.city,
    res_partner.zip,
    res_partner.customer,
    res_partner.type,
    res_partner.gstin,
    res_partner.accounts_approval,
    res_partner.core_code,
    res_partner.pan_no,
    res_partner.gst
   FROM res_partner
  WHERE (upper(res_partner.name::text) IN ( SELECT dupl.up_name
           FROM dupl)) AND res_partner.active AND res_partner.is_company
  ORDER BY res_partner.name, res_partner.id;

ALTER TABLE public.cjpl_js_duplicate_contacts
    OWNER TO postgres;


