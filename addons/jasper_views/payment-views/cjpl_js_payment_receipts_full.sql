-- View: public.cjpl_js_payments_receipts_full

-- DROP VIEW public.cjpl_js_payments_receipts_full;

CREATE OR REPLACE VIEW public.cjpl_js_payments_receipts_full
 AS
 WITH cust_sale_person AS (
         SELECT tbl1.partner_id,
            tbl1.users_id,
            tbl1.executive_name,
            tbl1.sale_team_id,
            tbl1.sale_team_name
           FROM ( SELECT row_number() OVER (PARTITION BY a.partner_id ORDER BY a.write_date DESC) AS rw_num,
                    a.partner_id,
                    a.user_id,
                    b.partner_id AS users_id,
                    c.display_name AS executive_name,
                    b.sale_team_id,
                    d.name AS sale_team_name
                   FROM saleperson_history a
                     JOIN res_users b ON a.user_id = b.id
                     JOIN res_partner c ON b.partner_id = c.id
                     JOIN crm_team d ON b.sale_team_id = d.id) tbl1
          WHERE tbl1.rw_num = 1
        ), diff_amount AS (
         SELECT row_number() OVER (PARTITION BY tbl2.name ORDER BY tbl2.name) AS rw_nm,
            tbl2.name,
            sum(tbl2.tot_amt) AS tot_amt,
            sum(tbl2.line_amt) AS line_amt,
            sum(tbl2.tot_amt)::double precision - sum(tbl2.line_amt) AS amt_diff
           FROM ( SELECT ap.name,
                    ap.amount AS tot_amt,
                    0 AS line_amt
                   FROM account_payment ap
                  WHERE ap.name::text <> 'Draft Payment'::text
                UNION ALL
                 SELECT ap.name,
                    0 AS tot_amt,
                        CASE
                            WHEN apl.type::text = 'cr'::text THEN apl.amount
                            ELSE - apl.amount
                        END AS line_amt
                   FROM account_payment ap
                     LEFT JOIN account_payment_lines apl ON apl.payment_id = ap.id AND apl.amount > 0::double precision
                  WHERE ap.name::text <> 'Draft Payment'::text) tbl2
          GROUP BY tbl2.name
         HAVING sum(tbl2.line_amt) > 0::double precision AND (sum(tbl2.tot_amt)::double precision - sum(tbl2.line_amt)) > 0::double precision
        )
 SELECT COALESCE(str1.voucher_number, ''::character varying) AS voucher_number,
    COALESCE(str1.payment_date, '1900-01-01'::date) AS payment_date,
    COALESCE(str1.bank_id, ''::character varying) AS bank_id,
    COALESCE(str1.bank_name, ''::character varying) AS bank_name,
    COALESCE(str1.intrument_type, ''::character varying) AS intrument_type,
    COALESCE(str1.cheque_date, '1900-01-01'::date) AS cheque_date,
    COALESCE(str1.payment_type, ''::character varying) AS payment_type,
    COALESCE(str1.description, ''::character varying) AS description,
    COALESCE(str1.remarks, ''::character varying) AS remarks,
    COALESCE(str1.invoice_number, ''::character varying) AS invoice_number,
    COALESCE(str1.parent_id, 0) AS parent_id,
    COALESCE(str1.customer_name, ''::character varying) AS customer_name,
    COALESCE(str1.date_invoice, '1900-01-01'::date) AS date_invoice,
    COALESCE(str1.excutive_name, ''::character varying) AS excutive_name,
    COALESCE(str1.sale_team_name, ''::character varying) AS sale_team_name,
    COALESCE(str1.invoice_type, ''::character varying) AS invoice_type,
    COALESCE(str1.invoice_status, ''::character varying) AS invoice_status,
    COALESCE(str1.amount, 0::double precision) AS payment_amount,
    COALESCE(str1.payment_difference_handling, ''::character varying) AS payment_difference_handling,
    COALESCE(str1.tds_amount, 0::double precision) AS tds_amount,
    COALESCE(str1.total, 0::double precision) AS allocation,
    COALESCE(cda.amt_diff, 0::double precision) AS payment_difference,
    COALESCE(str1.total + COALESCE(cda.amt_diff, 0::double precision), 0::double precision) AS total_amount
   FROM ( SELECT ap.name AS voucher_number,
            ap.payment_date,
            ap.move_name AS bank_id,
            aj.name AS bank_name,
            it.name AS intrument_type,
            ap.check_date AS cheque_date,
            pt.name AS payment_type,
            ap.description,
            ap.remarks,
            COALESCE(ai.number, aj1.name) AS invoice_number,
            COALESCE(c1.parent_id, ap.partner_id) AS parent_id,
            COALESCE(c2.name, c3.name) AS customer_name,
            ai.date_invoice,
            COALESCE(c.name, ccsp.executive_name) AS excutive_name,
            COALESCE(e.name, ccsp.sale_team_name) AS sale_team_name,
            d.name AS invoice_type,
            ai.state AS invoice_status,
            COALESCE(
                CASE
                    WHEN apl.type::text = 'cr'::text THEN apl.amount
                    ELSE - apl.amount
                END, ap.amount::double precision) AS amount,
            ap.payment_difference_handling,
            apl.tds_amount,
            COALESCE(
                CASE
                    WHEN apl.type::text = 'cr'::text THEN apl.amount
                    ELSE - apl.amount
                END, ap.amount::double precision) + COALESCE(apl.tds_amount, 0::double precision) AS total,
            row_number() OVER (PARTITION BY ap.name ORDER BY ap.name) AS rw_nm
           FROM account_payment ap
             LEFT JOIN account_payment_lines apl ON apl.payment_id = ap.id AND apl.amount > 0::double precision
             LEFT JOIN account_move_line aml ON apl.move_line_id = aml.id
             LEFT JOIN account_invoice ai ON ai.id = aml.invoice_id AND aml.reconciled = true
             LEFT JOIN instrument_type it ON ap.instrument_type = it.id
             LEFT JOIN payment_type pt ON ap.acc_payment_type = pt.id
             LEFT JOIN account_journal aj ON aj.id = ap.journal_id
             LEFT JOIN account_journal aj1 ON aj1.id = aml.journal_id
             LEFT JOIN res_users b ON ai.user_id = b.id
             LEFT JOIN res_partner c ON b.partner_id = c.id
             LEFT JOIN res_partner c1 ON ai.partner_id = c1.id
             LEFT JOIN res_partner c2 ON c1.parent_id = c2.id
             LEFT JOIN res_partner c3 ON ap.partner_id = c3.id
             LEFT JOIN cust_sale_person ccsp ON ap.partner_id = ccsp.partner_id
             LEFT JOIN sale_order_type d ON ai.sale_type_id = d.id
             LEFT JOIN crm_team e ON ai.team_id = e.id
          WHERE  ap.state::text <> 'draft'::text) str1
     LEFT JOIN diff_amount cda ON str1.voucher_number::text = cda.name::text AND str1.rw_nm = cda.rw_nm;

ALTER TABLE public.cjpl_js_payments_receipts_full
    OWNER TO postgres;


