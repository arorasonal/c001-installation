-- View: jasper.cjpl_js_outstanding_amt_party_ledger_entries

DROP VIEW IF EXISTS jasper.cjpl_js_outstanding_amt_party_ledger_entries;

CREATE OR REPLACE VIEW jasper.cjpl_js_outstanding_amt_party_ledger_entries
 AS
 WITH usr_dtl AS (
         SELECT COALESCE(a_1.parent_id, a_1.id) AS parent_id,
            b_1.name AS parent_name,
            a_1.id,
            a_1.name,
            b_1.user_id,
            COALESCE(a_1.user_id, b_1.user_id) AS usr_id,
            c.partner_id AS executive_id,
            d_1.name AS executive_name,
            e_1.name AS team_name,
            e1.name AS parent_team_name
           FROM res_partner a_1
             JOIN res_partner b_1 ON b_1.id = COALESCE(a_1.parent_id, a_1.id)
             LEFT JOIN res_users c ON a_1.user_id = c.id
             LEFT JOIN res_partner d_1 ON c.partner_id = d_1.id
             LEFT JOIN crm_team e_1 ON e_1.id = c.sale_team_id
             LEFT JOIN crm_team e1 ON e_1.parent_team = e1.id
        ), tds_amt AS (
         SELECT am_1.id,
            aml_1.debit
           FROM account_move am_1
             JOIN account_move_line aml_1 ON am_1.id = aml_1.move_id
             JOIN account_account aa_1 ON aml_1.account_id = aa_1.id
             LEFT JOIN res_partner rp_1 ON aml_1.partner_id = rp_1.id
             LEFT JOIN res_partner rp_par_1 ON rp_1.parent_id = rp_par_1.id
          WHERE 1 = 1 AND aa_1.id = 12
        )
 SELECT am.id AS am_id,
    am.journal_id,
    aml.id AS aml_id,
    aml.move_id,
    aml.invoice_id,
    aml.partner_id,
    am.name AS journal_ref_name,
    aj.name AS journal_name,
    rp.name AS party_name,
    aml.amount_residual AS on_account_outstanding_amt,
    round(aml.debit, 2) AS debit_amt,
        CASE
            WHEN aml.credit = tamt.debit THEN 0::numeric
            ELSE round(aml.credit, 2)
        END AS credit_amt,
        CASE
            WHEN aml.credit = tamt.debit THEN round(tamt.debit, 2)
            ELSE 0::numeric
        END AS tds_amt,
    aml.date AS payment_rec_dt,
    aa.name AS account_name,
    aml.name AS ref_name,
    ap.name AS payment_voucher_no,
    pt.name AS payment_type_name,
    udtl.executive_name AS excutive_name,
    udtl.team_name,
    udtl.parent_team_name
   FROM account_move am
     JOIN account_move_line aml ON am.id = aml.move_id
     LEFT JOIN account_payment ap ON aml.payment_id = ap.id
     LEFT JOIN payment_type pt ON pt.id = ap.acc_payment_type
     JOIN account_account aa ON aml.account_id = aa.id
     JOIN account_journal aj ON aml.journal_id = aj.id
     LEFT JOIN tds_amt tamt ON aml.move_id = tamt.id AND aml.credit::integer = tamt.debit::integer
     LEFT JOIN res_partner rp ON aml.partner_id = rp.id
     LEFT JOIN res_partner rp_par ON rp.parent_id = rp_par.id
     LEFT JOIN usr_dtl udtl ON aml.partner_id = udtl.id
  WHERE 1 = 1 AND aml.date >= '2019-12-01'::date AND aa.name::text = 'Sundry Debtors'::text
  ORDER BY rp.name, aml.date;

ALTER TABLE jasper.cjpl_js_outstanding_amt_party_ledger_entries
    OWNER TO postgres;


