-- View: public.cjpl_js_payments_received_bank_sheet

-- DROP VIEW public.cjpl_js_payments_received_bank_sheet;

CREATE OR REPLACE VIEW public.cjpl_js_payments_received_bank_sheet
 AS
 WITH payment_details AS (
         SELECT apl.payment_id,
            (COALESCE(am.name, ''::character varying)::text || ' Amt adj='::text) || btrim(to_char(apl.amount, '999999999D99'::text)) AS reference
           FROM account_payment_lines apl
             JOIN account_move_line aml ON aml.id = apl.move_line_id
             LEFT JOIN account_move am ON am.id = aml.move_id
          WHERE apl.tds_amount <> 0::double precision OR apl.amount <> 0::double precision
        ), payment_adjusted AS (
         SELECT payment_details.payment_id,
            string_agg(payment_details.reference, ','::text) AS adjustment_dtl
           FROM payment_details
          GROUP BY payment_details.payment_id
        )
 SELECT a.name AS voucher_no,
    a.payment_date,
    e.name AS payment_type,
    b.name AS journal_name,
    c.name AS partner,
    a.move_name AS journal_entry_no,
    a.amount,
    d.name AS instrument,
    a.instrument AS instrument_no,
    a.check_date AS instrument_date,
    a.description AS bank_name,
    a.remarks,
    f.adjustment_dtl
   FROM account_payment a
     JOIN account_journal b ON a.journal_id = b.id
     JOIN res_partner c ON a.partner_id = c.id
     JOIN instrument_type d ON d.id = a.instrument_type
     JOIN payment_type e ON e.id = a.acc_payment_type
     LEFT JOIN payment_adjusted f ON f.payment_id = a.id
  WHERE a.state::text = 'posted'::text;

ALTER TABLE public.cjpl_js_payments_received_bank_sheet
    OWNER TO postgres;


