SELECT
    coalesce(d.name, '') || ' ' || coalesce(b.name, '') AS reference,
    a.move_line_id,
    b.name,
    b.move_id,
    d.name,
    a.tds_amount,
    a.amount
FROM
    account_payment_lines a
    JOIN account_move_line b ON b.id = a.move_line_id
    LEFT JOIN account_move d ON d.id = b.move_id
WHERE
    a.tds_amount <> 0
    AND a.amount <> 0


