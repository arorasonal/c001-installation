-- View: "jasper"."cjpl_js_payments_not_fully_reconciled"

-- DROP VIEW "jasper"."cjpl_js_payments_not_fully_reconciled";

-- CREATE VIEW "cjpl_js_payments_not_fully_reconciled" ---------
CREATE OR REPLACE VIEW "jasper"."cjpl_js_payments_not_fully_reconciled" AS  WITH get_residual AS (
         SELECT b.amount_residual,
            b.debit,
            b.credit,
            b.reconciled,
            b.balance_cash_basis,
            b.name AS narr,
            c.name AS account,
            e.name AS payment_voucher,
            e.amount
           FROM ((account_move_line b
             JOIN account_account c ON ((b.account_id = c.id)))
             JOIN account_payment e ON ((e.id = b.payment_id)))
          WHERE (((c.name)::text = 'Sundry Debtors'::text) AND (abs(b.amount_residual) > (1)::numeric) AND (b.invoice_id IS NULL))
        ), each_payment_residual AS (
         SELECT get_residual.payment_voucher,
            sum(round(get_residual.amount_residual, 2)) AS unadj_amt
           FROM get_residual
          GROUP BY get_residual.payment_voucher
        )
 SELECT a.name AS voucher_number,
    a.payment_date,
    aj.name AS journal,
    rp.name AS customer,
    pt.name AS payment_type,
    round(a.amount, 2) AS voucher_amt,
    abs(epr.unadj_amt) AS unadjusted_amt,
    a.journal_id,
    a.partner_id,
    a.acc_payment_type
   FROM ((((account_payment a
     JOIN each_payment_residual epr ON (((a.name)::text = (epr.payment_voucher)::text)))
     JOIN res_partner rp ON ((a.partner_id = rp.id)))
     JOIN account_journal aj ON ((aj.id = a.journal_id)))
     JOIN payment_type pt ON ((pt.id = a.acc_payment_type)))
  ORDER BY rp.name, epr.payment_voucher;
-- -------------------------------------------------------------

ALTER TABLE "jasper"."cjpl_js_payments_not_fully_reconciled"
    OWNER TO postgres;