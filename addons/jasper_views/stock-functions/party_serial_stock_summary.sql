-- FUNCTION: public.party_serial_stock_summary(integer, character varying)

DROP FUNCTION IF EXISTS public.party_serial_stock_summary(integer, character varying);

CREATE OR REPLACE FUNCTION public.party_serial_stock_summary(
	p_partner_id integer,
	p_end_date character varying)
    RETURNS TABLE(r_id bigint, r_company_name character varying, r_company_partner_name character varying, r_serial_number character varying, r_category_name character varying, r_product_name character varying, r_product_challan character varying, r_doc_no character varying, r_move_date date, r_so_no character varying, r_product_qty integer) 
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
BEGIN
    RETURN QUERY WITH move_lines AS (
        SELECT
            sp.id,
            sp.name AS doc_no,
            date(sp.date_done) AS move_date,
            sp.origin,
            sp.partner_id,
            COALESCE(rp.parent_id, rp.id) AS parent_id,
            sp.picking_type_id,
            sp.transaction_type_id,
            sp.order_id,
            sp.location_id,
            sla.name AS from_location,
            sp.location_dest_id,
            slb.name AS to_location,
            spt.name AS movement_name,
            spt.code AS movement_type,
            sm.product_id,
            sm.product_qty,
            CASE WHEN sla.name::text = 'Customers'::text THEN
                '-1'::integer::numeric * sm.product_qty
            WHEN sla.name::text = 'Rental Out Stock'::text THEN
                '-1'::integer::numeric * sm.product_qty
            WHEN sla.name::text = 'Repair & Warrnty Replacement'::text THEN
                '-1'::integer::numeric * sm.product_qty
            WHEN slb.name::text = 'Customers'::text THEN
                1::numeric * sm.product_qty
            WHEN slb.name::text = 'Rental Out Stock'::text THEN
                1::numeric * sm.product_qty
            WHEN slb.name::text = 'Repair & Warrnty Replacement'::text THEN
                1::numeric * sm.product_qty
            WHEN spt.name::text = 'Delivery Note - Repair & Warranty Replacement'::text THEN
                1::numeric * sm.product_qty
            WHEN spt.name::text = 'Delivery Orders - Demo'::text THEN
                1::numeric * sm.product_qty
            WHEN spt.name::text = 'Delivery Orders - for View Purpose'::text THEN
                1::numeric * sm.product_qty
            WHEN spt.name::text = 'Delivery Orders - Rental'::text THEN
                1::numeric * sm.product_qty
            WHEN spt.name::text = 'Delivery Orders - Repair'::text THEN
                1::numeric * sm.product_qty
            WHEN spt.name::text = 'Delivery Orders - Replacement'::text THEN
                1::numeric * sm.product_qty
            WHEN spt.name::text = 'Delivery Orders - Sale'::text THEN
                1::numeric * sm.product_qty
            WHEN spt.name::text = 'Delivery Orders Purpose - RI'::text THEN
                1::numeric * sm.product_qty
            WHEN spt.name::text = 'Receipts - against Purchase'::text THEN
                '-1'::integer::numeric * sm.product_qty
            WHEN spt.name::text = 'Returns from Customer'::text THEN
                '-1'::integer::numeric * sm.product_qty
            WHEN spt.name::text = 'Returns from Supplier'::text THEN
                '-1'::integer::numeric * sm.product_qty
            WHEN spt.name::text = 'Returns to Supplier'::text THEN
                1::numeric * sm.product_qty
            WHEN spt.code::text = 'outgoing'::text THEN
                sm.product_qty
            WHEN spt.code::text = 'incoming'::text THEN
                - sm.product_qty
            ELSE
                NULL::numeric
            END AS move_qty,
            COALESCE(sm.serial_id, 0) AS serial_id,
            lot.name AS serial_number,
            row_number() OVER () AS distinct_key,
            ((((COALESCE(rp.parent_id, rp.id)::text || '-'::text) || lot.name::text) || '-'::text) || to_char(sp.date_done, ('yyyy-mm-dd hh24:mi:ss'::text) || '-'::text)) || row_number() OVER ()::text AS partition_key
        FROM
            stock_picking sp
            JOIN stock_picking_type spt ON sp.picking_type_id = spt.id
            JOIN stock_location sla ON sla.id = sp.location_id
            JOIN stock_location slb ON slb.id = sp.location_dest_id
            JOIN stock_move sm ON sm.picking_id = sp.id
            JOIN res_partner rp ON sp.partner_id = rp.id
            JOIN stock_production_lot lot ON lot.id = sm.serial_id
        WHERE
            sm.serial_id IS NOT NULL
            AND sp.partner_id IS NOT NULL
            AND sp.state::text = 'done'::text
            AND (spt.code::text = ANY (ARRAY['incoming'::character varying::text,
                    'outgoing'::character varying::text]))
            AND (rp.id = p_partner_id
                OR rp.parent_id = p_partner_id)
            AND sp.date_done <= date(p_end_date)
),
move_lines_sum AS (
    SELECT
        move_lines.parent_id,
        move_lines.serial_number,
        sum(move_lines.move_qty) AS balance_qty
    FROM
        move_lines
    GROUP BY
        move_lines.parent_id,
        move_lines.serial_number
    HAVING
        sum(move_lines.move_qty) <> 0::numeric
),
move_lines_rank AS (
    SELECT
        m.id,
        m.doc_no,
        m.move_date,
        m.origin,
        m.partner_id,
        m.parent_id,
        m.picking_type_id,
        m.transaction_type_id,
        m.order_id,
        m.location_id,
        m.from_location,
        m.location_dest_id,
        m.to_location,
        m.movement_name,
        m.movement_type,
        m.product_id,
        m.product_qty,
        m.move_qty,
        m.serial_id,
        m.serial_number,
        m.distinct_key,
        m.partition_key,
        row_number() OVER (PARTITION BY m.parent_id,
            m.serial_number ORDER BY m.partition_key DESC) AS rn
    FROM
        move_lines m
)
SELECT
    row_number() OVER () AS id,
        pa.name AS company_name,
        pb.name AS company_partner_name,
        mlr.serial_number,
        pc.name AS category_name,
        t.name AS product_name,
        t.product_challan,
        mlr.doc_no,
        mlr.move_date,
        COALESCE(sale_order.name, ''::character varying) AS so_no,
    mls.balance_qty::integer AS product_qty
FROM
    move_lines_rank mlr
    JOIN move_lines_sum mls ON mls.parent_id = mlr.parent_id
        AND mls.serial_number::text = mlr.serial_number::text
    JOIN res_partner pa ON pa.id = mlr.parent_id
    JOIN res_partner pb ON pb.id = mlr.partner_id
    JOIN product_product p ON mlr.product_id = p.id
    JOIN product_template t ON p.product_tmpl_id = t.id
    JOIN product_category pc ON pc.id = t.categ_id
    LEFT JOIN sale_order ON mlr.order_id = sale_order.id
    LEFT JOIN transaction_type tt ON tt.id = mlr.transaction_type_id
WHERE
    mlr.rn = 1
ORDER BY
    pa.name,
    pc.name,
    t.name,
    mlr.serial_number;
END;
$BODY$;

ALTER FUNCTION public.party_serial_stock_summary(integer, character varying)
    OWNER TO postgres;

