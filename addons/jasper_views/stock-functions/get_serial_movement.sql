-- FUNCTION: cjpl_get_serial_movement(character varying)

DROP FUNCTION IF EXISTS cjpl_get_serial_movement(character varying);

CREATE OR REPLACE FUNCTION cjpl_get_serial_movement(
	p_serial_no character varying)
    RETURNS TABLE(r_move_id integer, r_serial_id integer, r_location_id integer, r_location_dest_id integer, r_product_id integer, r_doc_name character varying, r_from_loc character varying, r_to_loc character varying, r_category_name character varying, r_product_challan character varying, r_product_name character varying, r_move_date date, r_write_date timestamp without time zone, r_product_qty integer) 
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
BEGIN
    RETURN QUERY WITH reqd_serial_id AS (
        SELECT
            id
        FROM
            stock_production_lot
        WHERE
            name = p_serial_no
),
reqd_product_id AS (
    SELECT
        product_id
    FROM
        stock_production_lot
    WHERE
        name = p_serial_no
),
reqd_alteration AS (
    SELECT
        ia.id,
        ia.name,
        ia.serial_no,
        ia.product_id,
        ia.write_date,
        ia.validate_date AS move_date
    FROM
        item_alteration ia
        JOIN reqd_serial_id rqi ON rqi.id = ia.serial_no
UNION
SELECT
    cna.id,
    cna.name,
    cna.serial_no,
    cna.product_id,
    cna.write_date,
    cna.validate_date AS move_date
FROM
    conversion_non_alterable cna
    JOIN reqd_serial_id rqi ON rqi.id = cna.serial_no
UNION
SELECT
    nia.id,
    nia.name,
    nia.serial_no,
    nia.product_id,
    nia.write_date,
    nia.validate_date AS move_date
FROM
    non_item_alteration nia
    JOIN reqd_serial_id rqi ON rqi.id = nia.serial_no
),
item_stk_move AS (
    SELECT
        ma.id,
        spk.name AS doc_name,
        ma.product_id,
        ma.serial_id,
        ma.location_id,
        ma.location_dest_id,
        ma.product_qty,
        spk.write_date,
        date(spk.date_done) AS move_date
FROM
    stock_move ma
    JOIN reqd_serial_id rqi ON rqi.id = ma.serial_id
    JOIN stock_picking spk ON ma.picking_id = spk.id
    WHERE
        ma.state::text = 'done'::text
        AND ma.picking_id IS NOT NULL
),
item_alt_move AS (
    SELECT
        ma.id,
        ma.origin AS doc_name,
        ma.product_id,
        reqd_alteration.serial_no AS serial_id,
        ma.location_id,
        ma.location_dest_id,
        ma.product_qty,
        reqd_alteration.write_date,
        reqd_alteration.move_date
    FROM
        stock_move ma
        JOIN reqd_alteration ON ma.origin::text = reqd_alteration.name::text
        JOIN reqd_product_id ON ma.product_id = reqd_product_id.product_id
    WHERE
        ma.state::text = 'done'::text
        AND ma.picking_id IS NULL
),
item_all_move AS (
    SELECT
        *
    FROM
        item_stk_move ism
    UNION
    SELECT
        *
    FROM
        item_alt_move iam
)
SELECT
    g.id as move_id, 
    g.serial_id,
    g.location_id,
    g.location_dest_id,
    g.product_id,
    g.doc_name,
    la.name AS from_loc,
    lb.name AS to_loc,
    pc.name AS category_name,
    t.product_challan,
    t.name AS product_name,
    date(g.move_date) as move_date,
    g.write_date,
    g.product_qty::integer AS product_qty
FROM
    item_all_move g
    JOIN product_product p ON g.product_id = p.id
    JOIN product_template t ON p.product_tmpl_id = t.id
    JOIN stock_location la ON g.location_id = la.id
    JOIN stock_location lb ON g.location_dest_id = lb.id
    JOIN product_category pc ON t.categ_id = pc.id
ORDER BY
    g.write_date,
    g.product_id;
END;
$BODY$;

ALTER FUNCTION cjpl_get_serial_movement(character varying)
    OWNER TO postgres;

