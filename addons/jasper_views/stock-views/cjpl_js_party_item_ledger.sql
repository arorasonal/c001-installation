-- View: public."cjpl_js_party_item_ledger"

DROP VIEW IF EXISTS public."cjpl_js_party_item_ledger";
DROP VIEW IF EXISTS public."cjpl_js_party_item_ledger.sql";


CREATE OR REPLACE VIEW public."cjpl_js_party_item_ledger"
 AS
 WITH move_lines AS (
         SELECT sp.id,
            sp.name,
            date(sp.date_done) AS move_date,
            sp.origin,
            sp.partner_id,
            sp.picking_type_id,
            sp.order_id,
            sp.location_id,
            sla.name AS from_location,
            sp.location_dest_id,
            slb.name AS to_location,
            spt.name AS movement_name,
            spt.code AS movement_type,
            sm.product_id,
            sm.product_qty,
            -- stock movement multiplier is first computed based location 
            -- if locations do not fall in locations displayed then - 
            --    next its based on delivery document name == picking_type_id 
            --    if delivery document name is not amongst those displayed here 
            --       its based on whether stock picking type is outgoing or incoming 
            -- need to put stock movement multiplier info on
            --    (a) location
            --    (b) stock picking type
            -- so that this sql is not dependent on name of location or stock picking type
                CASE
                    WHEN sla.name::text = 'Customers'::text THEN '-1'::integer::numeric * sm.product_qty
                    WHEN sla.name::text = 'Rental Out Stock'::text THEN '-1'::integer::numeric * sm.product_qty
                    WHEN sla.name::text = 'Repair & Warrnty Replacement'::text THEN '-1'::integer::numeric * sm.product_qty
                    WHEN slb.name::text = 'Customers'::text THEN 1::integer::numeric * sm.product_qty
                    WHEN slb.name::text = 'Rental Out Stock'::text THEN 1::integer::numeric * sm.product_qty
                    WHEN slb.name::text = 'Repair & Warrnty Replacement'::text THEN 1::integer::numeric * sm.product_qty
                    WHEN spt.name::text = 'Delivery Note - Repair & Warranty Replacement'::text THEN 1::numeric * sm.product_qty
                    WHEN spt.name::text = 'Delivery Orders - Demo'::text THEN 1::numeric * sm.product_qty
                    WHEN spt.name::text = 'Delivery Orders - for View Purpose'::text THEN 1::numeric * sm.product_qty
                    WHEN spt.name::text = 'Delivery Orders - Rental'::text THEN 1::numeric * sm.product_qty
                    WHEN spt.name::text = 'Delivery Orders - Repair'::text THEN 1::numeric * sm.product_qty
                    WHEN spt.name::text = 'Delivery Orders - Replacement'::text THEN 1::numeric * sm.product_qty
                    WHEN spt.name::text = 'Delivery Orders - Sale'::text THEN 1::numeric * sm.product_qty
                    WHEN spt.name::text = 'Delivery Orders Purpose - RI'::text THEN 1::numeric * sm.product_qty
                    WHEN spt.name::text = 'Receipts - against Purchase'::text THEN '-1'::integer::numeric * sm.product_qty
                    WHEN spt.name::text = 'Returns from Customer'::text THEN '-1'::integer::numeric * sm.product_qty
                    WHEN spt.name::text = 'Returns from Supplier'::text THEN '-1'::integer::numeric * sm.product_qty
                    WHEN spt.name::text = 'Returns to Supplier'::text THEN 1::numeric * sm.product_qty
                    WHEN spt.code::text = 'outgoing'::text THEN sm.product_qty
                    WHEN spt.code::text = 'incoming'::text THEN - sm.product_qty
                    ELSE NULL::numeric
                END AS move_qty,
            COALESCE(sm.serial_id, 0) AS serial_id,
            row_number() OVER () AS distinct_key,
            (((((((sp.partner_id::text || '-'::text) || sm.product_id::text) || '-'::text) || COALESCE(sm.serial_id, 0)::text) || '-'::text) || to_char(sp.date_done, ('yyyy-mm-dd hh24:mi:ss'::text))) || '-'::text) || row_number() OVER ()::text AS sort_key
           FROM stock_picking sp
             JOIN stock_picking_type spt ON sp.picking_type_id = spt.id
             JOIN stock_location sla ON sla.id = sp.location_id
             JOIN stock_location slb ON slb.id = sp.location_dest_id
             JOIN stock_move sm ON sm.picking_id = sp.id
          WHERE sp.partner_id IS NOT NULL AND sp.state::text = 'done'::text AND (spt.code::text = ANY (ARRAY['incoming'::character varying, 'outgoing'::character varying]::text[]))
        ), partner_is_not_company_with_no_parent AS (
         SELECT a.id,
            a.name AS company_partner_name,
            a.name AS company_name,
            a.parent_id,
            a.is_company
           FROM res_partner a
          WHERE NOT a.is_company AND a.parent_id IS NULL
        ), partner_is_a_company AS (
         SELECT a.id,
            a.name AS company_partner_name,
            a.name AS company_name,
            a.parent_id,
            a.is_company
           FROM res_partner a
          WHERE a.is_company
        ), partner_is_not_company_but_has_parent AS (
         SELECT a.id,
            a.name AS company_partner_name,
            COALESCE(b.company_name, a.name) AS company_name,
            a.parent_id,
            a.is_company
           FROM res_partner a
             LEFT JOIN partner_is_a_company b ON a.parent_id = b.id
          WHERE NOT a.is_company AND a.parent_id IS NOT NULL
        ), partner_all_companies AS (
         SELECT a.id,
            a.company_partner_name,
            a.company_name,
            a.parent_id,
            a.is_company
           FROM partner_is_a_company a
        UNION
         SELECT b.id,
            b.company_partner_name,
            b.company_name,
            b.parent_id,
            b.is_company
           FROM partner_is_not_company_but_has_parent b
        UNION
         SELECT c.id,
            c.company_partner_name,
            c.company_name,
            c.parent_id,
            c.is_company
           FROM partner_is_not_company_with_no_parent c
        )
 SELECT ml.partner_id,
    ml.product_id,
    ml.serial_id,
    ml.distinct_key,
    ml.sort_key,
    pa.company_name,
    pa.company_partner_name,
    pc.name AS category_name,
    t.name AS product_name,
    t.product_challan,
    ml.name AS doc_no,
    ml.movement_name AS doc_name,
    ml.move_date,
    COALESCE(ml.origin, ''::character varying) AS org_doc,
    COALESCE(sale_order.name, ''::character varying) AS so_no,
    ml.from_location,
    ml.to_location,
    COALESCE(lot.name, ''::character varying) AS serial_number,
    ml.move_qty,
    sum(ml.move_qty) OVER (PARTITION BY ml.partner_id, ml.product_id, ml.serial_id ORDER BY ml.move_date, ml.distinct_key) AS running_stock
   FROM move_lines ml
     JOIN partner_all_companies pa ON pa.id = ml.partner_id
     LEFT JOIN stock_production_lot lot ON lot.id = ml.serial_id
     JOIN product_product p ON ml.product_id = p.id
     JOIN product_template t ON p.product_tmpl_id = t.id
     JOIN product_category pc ON pc.id = t.categ_id
     LEFT JOIN sale_order ON ml.order_id = sale_order.id
  ORDER BY pa.company_name, pa.company_partner_name, pc.name, t.product_challan, lot.name, ml.move_date, ml.distinct_key;

ALTER TABLE public."cjpl_js_party_item_ledger"
    OWNER TO postgres;


