-- View: jasper.cjpl_js_erroneous_negative_stock

DROP VIEW IF EXISTS jasper.cjpl_js_erroneous_negative_stock;

CREATE OR REPLACE VIEW jasper.cjpl_js_erroneous_negative_stock
 AS
 WITH internal_loc_name AS (
         SELECT stock_location.id,
            stock_location.allow_negative_stock,
            "substring"(stock_location.complete_name::text, "position"(stock_location.complete_name::text, '/'::text) + 1) AS location_name
           FROM stock_location
          WHERE stock_location.usage::text = 'internal'::text
        ), out_move AS (
         SELECT m.product_id,
            m.location_id AS loc_id,
            sum(0) AS in_qty,
            sum(m.product_qty) AS out_qty
           FROM stock_move m
          WHERE m.state::text ~~ 'done'::text AND (m.location_id IN ( SELECT internal_loc_name.id
                   FROM internal_loc_name))
          GROUP BY m.product_id, m.location_id
        ), in_move AS (
         SELECT m.product_id,
            m.location_dest_id AS loc_id,
            sum(m.product_qty) AS in_qty,
            sum(0) AS out_qty
           FROM stock_move m
          WHERE m.state::text ~~ 'done'::text AND (m.location_dest_id IN ( SELECT internal_loc_name.id
                   FROM internal_loc_name))
          GROUP BY m.product_id, m.location_dest_id
        ), all_move AS (
         SELECT a.product_id,
            a.loc_id,
            a.in_qty,
            a.out_qty
           FROM in_move a
        UNION
         SELECT b.product_id,
            b.loc_id,
            b.in_qty,
            b.out_qty
           FROM out_move b
        ), sum_move AS (
         SELECT all_move.product_id,
            all_move.loc_id,
            sum(all_move.in_qty) AS in_qty,
            sum(all_move.out_qty) AS out_qty
           FROM all_move
          GROUP BY all_move.product_id, all_move.loc_id
        ), neg_stock AS (
         SELECT a.product_id,
            a.loc_id,
            a.in_qty - a.out_qty AS stock_in_hand
           FROM sum_move a
             JOIN internal_loc_name l_1 ON a.loc_id = l_1.id
          WHERE (a.in_qty - a.out_qty) < 0::numeric AND COALESCE(l_1.allow_negative_stock, false)
        ), item_stk_move AS (
         SELECT ma.picking_id,
            ma.product_id,
            ma.location_id AS loc_id,
            sum(0) AS in_qty,
            sum(ma.product_qty) AS out_qty
           FROM stock_move ma
             JOIN neg_stock ns ON ma.product_id = ns.product_id AND ma.location_id = ns.loc_id
          WHERE ma.state::text ~~ 'done'::text AND ma.picking_id IS NOT NULL
          GROUP BY ma.picking_id, ma.product_id, ma.location_id
        UNION
         SELECT mb.picking_id,
            mb.product_id,
            mb.location_dest_id AS loc_id,
            sum(mb.product_qty) AS in_qty,
            sum(0) AS out_qty
           FROM stock_move mb
             JOIN neg_stock nt ON mb.product_id = nt.product_id AND mb.location_dest_id = nt.loc_id
          WHERE mb.state::text ~~ 'done'::text AND mb.picking_id IS NOT NULL
          GROUP BY mb.picking_id, mb.product_id, mb.location_dest_id
        ), item_alt_move AS (
         SELECT ma.origin,
            ma.product_id,
            ma.location_id AS loc_id,
            sum(0) AS in_qty,
            sum(ma.product_qty) AS out_qty
           FROM stock_move ma
             JOIN neg_stock ns ON ma.product_id = ns.product_id AND ma.location_id = ns.loc_id
          WHERE ma.state::text ~~ 'done'::text AND ma.picking_id IS NULL
          GROUP BY ma.origin, ma.product_id, ma.location_id
        UNION
         SELECT mb.origin,
            mb.product_id,
            mb.location_dest_id AS loc_id,
            sum(mb.product_qty) AS in_qty,
            sum(0) AS out_qty
           FROM stock_move mb
             JOIN neg_stock nt ON mb.product_id = nt.product_id AND mb.location_dest_id = nt.loc_id
          WHERE mb.state::text ~~ 'done'::text AND mb.picking_id IS NULL
          GROUP BY mb.origin, mb.product_id, mb.location_dest_id
        ), item_all_move AS (
         SELECT ism.loc_id,
            ism.product_id,
            ism.in_qty,
            ism.out_qty,
            spk.name,
            spk.write_date,
            date(spk.date_done) AS move_date
           FROM item_stk_move ism
             JOIN stock_picking spk ON ism.picking_id = spk.id
        UNION
         SELECT iam.loc_id,
            iam.product_id,
            iam.in_qty,
            iam.out_qty,
            iam.origin AS name,
            COALESCE(ialt.write_date, COALESCE(cna.write_date, nia.write_date)) AS write_date,
            date(COALESCE(ialt.validate_date, COALESCE(cna.validate_date, nia.validate_date))) AS move_date
           FROM item_alt_move iam
             LEFT JOIN item_alteration ialt ON ialt.name::text = iam.origin::text
             LEFT JOIN conversion_non_alterable cna ON cna.name::text = iam.origin::text
             LEFT JOIN non_item_alteration nia ON nia.name::text = iam.origin::text
        )
 SELECT g.loc_id,
    l.location_name AS location,
    g.product_id,
    pc.name AS category_name,
    t.product_challan,
    t.name AS product_name,
    g.name AS document,
    g.move_date,
    g.write_date,
    g.in_qty,
    g.out_qty,
    sum(g.in_qty - g.out_qty) OVER (PARTITION BY g.loc_id, g.product_id ORDER BY g.write_date , g.name) AS bal_qty
   FROM item_all_move g
     JOIN product_product p ON g.product_id = p.id
     JOIN product_template t ON p.product_tmpl_id = t.id
     JOIN internal_loc_name l ON g.loc_id = l.id
     JOIN product_category pc ON t.categ_id = pc.id
  ORDER BY g.loc_id, g.product_id, g.write_date, g.name;

ALTER TABLE jasper.cjpl_js_erroneous_negative_stock
    OWNER TO postgres;


