-- View: jasper.cjpl_js_party_non_serial_stock_ledger

DROP VIEW IF EXISTS jasper.cjpl_js_party_non_serial_stock_ledger;

CREATE OR REPLACE VIEW "jasper"."cjpl_js_party_non_serial_stock_ledger" AS  WITH move_lines AS (
         SELECT sp.id,
            sp.name,
            date(sp.date_done) AS move_date,
            sp.origin,
            sp.partner_id,
            COALESCE(rp.parent_id, rp.id) AS parent_id,
            sp.picking_type_id,
            sp.order_id,
            sp.location_id,
            sla.name AS from_location,
            sp.location_dest_id,
            slb.name AS to_location,
            spt.name AS movement_name,
            spt.code AS movement_type,
            sm.product_id,
            sm.product_qty,
                CASE
                    WHEN ((sla.name)::text = 'Customers'::text) THEN (('-1'::integer)::numeric * sm.product_qty)
                    WHEN ((sla.name)::text = 'Rental Out Stock'::text) THEN (('-1'::integer)::numeric * sm.product_qty)
                    WHEN ((sla.name)::text = 'Repair & Warrnty Replacement'::text) THEN (('-1'::integer)::numeric * sm.product_qty)
                    WHEN ((slb.name)::text = 'Customers'::text) THEN ((1)::numeric * sm.product_qty)
                    WHEN ((slb.name)::text = 'Rental Out Stock'::text) THEN ((1)::numeric * sm.product_qty)
                    WHEN ((slb.name)::text = 'Repair & Warrnty Replacement'::text) THEN ((1)::numeric * sm.product_qty)
                    WHEN ((spt.name)::text = 'Delivery Note - Repair & Warranty Replacement'::text) THEN ((1)::numeric * sm.product_qty)
                    WHEN ((spt.name)::text = 'Delivery Orders - Demo'::text) THEN ((1)::numeric * sm.product_qty)
                    WHEN ((spt.name)::text = 'Delivery Orders - for View Purpose'::text) THEN ((1)::numeric * sm.product_qty)
                    WHEN ((spt.name)::text = 'Delivery Orders - Rental'::text) THEN ((1)::numeric * sm.product_qty)
                    WHEN ((spt.name)::text = 'Delivery Orders - Repair'::text) THEN ((1)::numeric * sm.product_qty)
                    WHEN ((spt.name)::text = 'Delivery Orders - Replacement'::text) THEN ((1)::numeric * sm.product_qty)
                    WHEN ((spt.name)::text = 'Delivery Orders - Sale'::text) THEN ((1)::numeric * sm.product_qty)
                    WHEN ((spt.name)::text = 'Delivery Orders Purpose - RI'::text) THEN ((1)::numeric * sm.product_qty)
                    WHEN ((spt.name)::text = 'Receipts - against Purchase'::text) THEN (('-1'::integer)::numeric * sm.product_qty)
                    WHEN ((spt.name)::text = 'Returns from Customer'::text) THEN (('-1'::integer)::numeric * sm.product_qty)
                    WHEN ((spt.name)::text = 'Returns from Supplier'::text) THEN (('-1'::integer)::numeric * sm.product_qty)
                    WHEN ((spt.name)::text = 'Returns to Supplier'::text) THEN ((1)::numeric * sm.product_qty)
                    WHEN ((spt.code)::text = 'outgoing'::text) THEN sm.product_qty
                    WHEN ((spt.code)::text = 'incoming'::text) THEN (- sm.product_qty)
                    ELSE NULL::numeric
                END AS move_qty,
            row_number() OVER () AS distinct_key,
            ((((((((COALESCE(rp.parent_id, rp.id))::text || '-'::text) || (sm.product_id)::text) || '-'::text) || '-'::text) || to_char(sp.date_done, ('yyyy-mm-dd hh24:mi:ss'::text)) ) || '-'::text) || (row_number() OVER ())::text) AS sort_key
           FROM (((((stock_picking sp
             JOIN stock_picking_type spt ON ((sp.picking_type_id = spt.id)))
             JOIN stock_location sla ON ((sla.id = sp.location_id)))
             JOIN stock_location slb ON ((slb.id = sp.location_dest_id)))
             JOIN stock_move sm ON ((sm.picking_id = sp.id)))
             JOIN res_partner rp ON ((sp.partner_id = rp.id)))
          WHERE ((sp.partner_id IS NOT NULL) AND (sm.serial_id IS NULL) AND ((sp.state)::text = 'done'::text) AND ((spt.code)::text = ANY (ARRAY[('incoming'::character varying)::text, ('outgoing'::character varying)::text])))
        )
 SELECT ml.parent_id,
    ml.product_id,
    ml.distinct_key,
    ml.sort_key,
    pa.name AS company_name,
    pc.name AS category_name,
    t.name AS product_name,
    t.product_challan,
    ml.name AS doc_no,
    ml.movement_name AS doc_name,
    ml.move_date,
    COALESCE(ml.origin, ''::character varying) AS org_doc,
    COALESCE(sale_order.name, ''::character varying) AS so_no,
    ml.from_location,
    ml.to_location,
    ml.move_qty,
    sum(ml.move_qty) OVER (PARTITION BY ml.parent_id, ml.product_id ORDER BY ml.move_date, ml.distinct_key) AS running_stock
   FROM (((((move_lines ml
     JOIN res_partner pa ON ((pa.id = ml.parent_id)))
     JOIN product_product p ON ((ml.product_id = p.id)))
     JOIN product_template t ON ((p.product_tmpl_id = t.id)))
     JOIN product_category pc ON ((pc.id = t.categ_id)))
     LEFT JOIN sale_order ON ((ml.order_id = sale_order.id)))
  ORDER BY pa.name, pc.name, t.product_challan, ml.move_date, ml.distinct_key;;
-- -------------------------------------------------------------

-- CHANGE "COMMENT" OF "VIEW "cjpl_js_party_non_serial_stock_ledger" 
COMMENT ON VIEW "jasper"."cjpl_js_party_non_serial_stock_ledger" IS '        -- stock movement multiplier is first computed based location
        -- if locations do not fall in locations displayed then -
        --    next its based on delivery document name == picking_type_id
        --    if delivery document name is not amongst those displayed here
        --       its based on whether stock picking type is outgoing or incoming
        -- need to put stock movement multiplier info on
        --    (a) location
        --    (b) stock picking type
        -- so that this sql is not dependent on name of location or stock picking type
';



