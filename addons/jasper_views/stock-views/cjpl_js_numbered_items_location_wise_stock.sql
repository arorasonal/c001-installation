-- View: jasper.cjpl_js_numbered_items_location_wise_stock

DROP VIEW IF EXISTS jasper.cjpl_js_numbered_items_location_wise_stock;

CREATE OR REPLACE VIEW jasper.cjpl_js_numbered_items_location_wise_stock
 AS
 WITH serial_last_move AS (
         SELECT DISTINCT ON (a_1.name) row_number() OVER () AS id,
            a_1.product_id,
            a_1.id AS lot_id,
            a_1.name AS serial_number,
            d.location_dest_id AS location_id
           FROM stock_production_lot a_1
             JOIN stock_move d ON a_1.id = d.serial_id
          WHERE d.location_dest_id <> 18 AND d.state::text = 'done'::text AND a_1.product_id = d.product_id
          ORDER BY a_1.name, d.date DESC NULLS LAST
        )
 SELECT a.id,
    a.product_id,
    t.name AS product_name,
    t.product_challan,
    pc.name AS category_name,
    a.lot_id,
    a.serial_number,
    a.location_id,
    sl.name AS location_name
   FROM serial_last_move a
     JOIN stock_location sl ON a.location_id = sl.id
     JOIN product_product p ON a.product_id = p.id
     JOIN product_template t ON p.product_tmpl_id = t.id
     JOIN product_category pc ON t.categ_id = pc.id
  ORDER BY sl.name, pc.name, t.product_challan, a.serial_number;

ALTER TABLE jasper.cjpl_js_numbered_items_location_wise_stock
    OWNER TO postgres;


