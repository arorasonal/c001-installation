-- View: public.cjpl_js_serial_movement

DROP VIEW IF EXISTS public."cjpl_js_serial_movement";

CREATE OR REPLACE VIEW public.cjpl_js_serial_movement
 AS
 WITH partner_is_not_company_with_no_parent AS (
         SELECT a_1.id,
            a_1.name AS company_partner_name,
            a_1.name AS company_name,
            a_1.parent_id,
            a_1.is_company
           FROM res_partner a_1
          WHERE NOT a_1.is_company AND a_1.parent_id IS NULL
        ), partner_is_a_company AS (
         SELECT a_1.id,
            a_1.name AS company_partner_name,
            a_1.name AS company_name,
            a_1.parent_id,
            a_1.is_company
           FROM res_partner a_1
          WHERE a_1.is_company
        ), partner_is_not_company_but_has_parent AS (
         SELECT a_1.id,
            a_1.name AS company_partner_name,
            COALESCE(b_1.company_name, a_1.name) AS company_name,
            a_1.parent_id,
            a_1.is_company
           FROM res_partner a_1
             LEFT JOIN partner_is_a_company b_1 ON a_1.parent_id = b_1.id
          WHERE NOT a_1.is_company AND a_1.parent_id IS NOT NULL
        ), partner_all_companies AS (
         SELECT a_1.id,
            a_1.company_partner_name,
            a_1.company_name,
            a_1.parent_id,
            a_1.is_company
           FROM partner_is_a_company a_1
        UNION
         SELECT b_1.id,
            b_1.company_partner_name,
            b_1.company_name,
            b_1.parent_id,
            b_1.is_company
           FROM partner_is_not_company_but_has_parent b_1
        UNION
         SELECT c_1.id,
            c_1.company_partner_name,
            c_1.company_name,
            c_1.parent_id,
            c_1.is_company
           FROM partner_is_not_company_with_no_parent c_1
        )
 SELECT a.id,
    a.picking_id,
    a.create_date AS stock_move_create,
    a.write_date AS stock_move_write,
    a.location_id,
    sla.name AS from_location,
    a.location_dest_id,
    slb.name AS to_location,
    b.name AS doc_name,
    b.date_done,
    c.name AS doc_type,
    a.serial_id,
    lot.name AS serial_number,
    lot.product_id,
    COALESCE(b.partner_id, 0) AS partner_id,
    t.name AS product_name,
    t.product_challan,
    pc.name AS category_name,
    COALESCE(pa.company_name, ''::character varying) AS company_name,
    COALESCE(pa.company_partner_name, ''::character varying) AS partner_name
   FROM stock_move a
     LEFT JOIN stock_picking b ON b.id = a.picking_id
     LEFT JOIN stock_picking_type c ON b.picking_type_id = c.id
     JOIN stock_location sla ON sla.id = a.location_id
     JOIN stock_location slb ON slb.id = a.location_dest_id
     JOIN stock_production_lot lot ON a.serial_id = lot.id
     JOIN product_product p ON lot.product_id = p.id
     JOIN product_template t ON p.product_tmpl_id = t.id
     JOIN product_category pc ON pc.id = t.categ_id
     LEFT JOIN partner_all_companies pa ON pa.id = b.partner_id
  WHERE a.serial_id IS NOT NULL and a.state = 'done'
  ORDER BY a.serial_id, a.write_date;

ALTER TABLE public.cjpl_js_serial_movement
    OWNER TO postgres;


