-- View: public.cjpl_js_parent_party_item_summary

DROP VIEW IF EXISTS public.cjpl_js_parent_party_item_summary;

CREATE OR REPLACE VIEW public.cjpl_js_parent_party_item_summary
 AS
 WITH move_lines AS (
         SELECT sp.id,
            sp.name,
            date(sp.date_done) AS move_date,
            sp.origin,
            sp.partner_id,
            COALESCE(rp.parent_id, rp.id) AS parent_id,
            sp.picking_type_id,
            sp.order_id,
            sp.location_id,
            sla.name AS from_location,
            sp.location_dest_id,
            slb.name AS to_location,
            spt.name AS movement_name,
            spt.code AS movement_type,
            sm.product_id,
            sm.product_qty,
            -- stock movement multiplier is first computed based location 
            -- if locations do not fall in locations displayed then - 
            --    next its based on delivery document name == picking_type_id 
            --    if delivery document name is not amongst those displayed here 
            --       its based on whether stock picking type is outgoing or incoming 
            -- need to put stock movement multiplier info on
            --    (a) location
            --    (b) stock picking type
            -- so that this sql is not dependent on name of location or stock picking type
                CASE
                    WHEN sla.name::text = 'Customers'::text THEN '-1'::integer::numeric * sm.product_qty
                    WHEN sla.name::text = 'Rental Out Stock'::text THEN '-1'::integer::numeric * sm.product_qty
                    WHEN sla.name::text = 'Repair & Warrnty Replacement'::text THEN '-1'::integer::numeric * sm.product_qty
                    WHEN slb.name::text = 'Customers'::text THEN 1::numeric * sm.product_qty
                    WHEN slb.name::text = 'Rental Out Stock'::text THEN 1::numeric * sm.product_qty
                    WHEN slb.name::text = 'Repair & Warrnty Replacement'::text THEN 1::numeric * sm.product_qty
                    WHEN spt.name::text = 'Delivery Note - Repair & Warranty Replacement'::text THEN 1::numeric * sm.product_qty
                    WHEN spt.name::text = 'Delivery Orders - Demo'::text THEN 1::numeric * sm.product_qty
                    WHEN spt.name::text = 'Delivery Orders - for View Purpose'::text THEN 1::numeric * sm.product_qty
                    WHEN spt.name::text = 'Delivery Orders - Rental'::text THEN 1::numeric * sm.product_qty
                    WHEN spt.name::text = 'Delivery Orders - Repair'::text THEN 1::numeric * sm.product_qty
                    WHEN spt.name::text = 'Delivery Orders - Replacement'::text THEN 1::numeric * sm.product_qty
                    WHEN spt.name::text = 'Delivery Orders - Sale'::text THEN 1::numeric * sm.product_qty
                    WHEN spt.name::text = 'Delivery Orders Purpose - RI'::text THEN 1::numeric * sm.product_qty
                    WHEN spt.name::text = 'Receipts - against Purchase'::text THEN '-1'::integer::numeric * sm.product_qty
                    WHEN spt.name::text = 'Returns from Customer'::text THEN '-1'::integer::numeric * sm.product_qty
                    WHEN spt.name::text = 'Returns from Supplier'::text THEN '-1'::integer::numeric * sm.product_qty
                    WHEN spt.name::text = 'Returns to Supplier'::text THEN 1::numeric * sm.product_qty
                    WHEN spt.code::text = 'outgoing'::text THEN sm.product_qty
                    WHEN spt.code::text = 'incoming'::text THEN - sm.product_qty
                    ELSE NULL::numeric
                END AS move_qty,
            COALESCE(sm.serial_id, 0) AS serial_id,
            row_number() OVER () AS distinct_key,
            (((((((COALESCE(rp.parent_id, rp.id)::text || '-'::text) || sm.product_id::text) || '-'::text) || COALESCE(sm.serial_id, 0)::text) || '-'::text) || to_char(sp.date_done, ('yyyy-mm-dd hh24:mi:ss'::text))) || '-'::text) || row_number() OVER ()::text AS sort_key
           FROM stock_picking sp
             JOIN stock_picking_type spt ON sp.picking_type_id = spt.id
             JOIN stock_location sla ON sla.id = sp.location_id
             JOIN stock_location slb ON slb.id = sp.location_dest_id
             JOIN stock_move sm ON sm.picking_id = sp.id
             JOIN res_partner rp ON sp.partner_id = rp.id
          WHERE sp.partner_id IS NOT NULL AND sp.state::text = 'done'::text AND (spt.code::text = ANY (ARRAY['incoming'::character varying::text, 'outgoing'::character varying::text]))
        ), move_lines_sum AS (
         SELECT move_lines.parent_id,
            move_lines.product_id,
            move_lines.serial_id,
            sum(move_lines.move_qty) AS balance_qty
           FROM move_lines
          GROUP BY move_lines.parent_id, move_lines.product_id, move_lines.serial_id
         HAVING sum(move_lines.move_qty) <> 0::numeric
        )
 SELECT ml.parent_id,
    ml.product_id,
    ml.serial_id,
    pa.name AS company_name,
    pc.name AS category_name,
    t.name AS product_name,
    t.product_challan,
    COALESCE(lot.name, ''::character varying) AS serial_number,
    ml.balance_qty
   FROM move_lines_sum ml
     JOIN res_partner pa ON pa.id = ml.parent_id
     LEFT JOIN stock_production_lot lot ON lot.id = ml.serial_id
     JOIN product_product p ON ml.product_id = p.id
     JOIN product_template t ON p.product_tmpl_id = t.id
     JOIN product_category pc ON pc.id = t.categ_id
  ORDER BY pa.name, pc.name, t.product_challan, lot.name;

ALTER TABLE public.cjpl_js_parent_party_item_summary
    OWNER TO postgres;


