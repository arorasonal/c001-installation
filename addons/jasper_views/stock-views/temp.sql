WITH serial_last_move AS (
    SELECT DISTINCT ON (a.name)
        row_number() OVER () AS id,
        a.product_id,
        a.id AS lot_id,
        a.name AS serial_number,
        d.location_dest_id AS location_id
    FROM
        stock_production_lot a
        JOIN stock_move d ON a.id = d.serial_id
    WHERE
        d.location_dest_id <> 18
        AND d.STATE::text = 'done'::text
        AND a.product_id = d.product_id
    ORDER BY
        a.name,
        d.DATE DESC NULLS LAST
)
SELECT
    a.id,
    a.product_id,
    t.name AS product_name,
    t.product_challan,
    pc.name AS category_name,
    a.lot_id,
    a.serial_number,
    a.location_id,
    sl.name AS location_name
FROM
    serial_last_move a
    JOIN stock_location sl ON a.location_id = sl.id
    JOIN product_product p ON a.product_id = p.id
    JOIN product_template t ON p.product_tmpl_id = t.id
    JOIN product_category pc ON t.categ_id = pc.id
ORDER BY
    sl.name,
    pc.name,
    t.product_challan,
    a.serial_number
