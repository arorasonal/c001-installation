-- View: public.cjpl_js_lot_serial_location_with_components
DROP VIEW IF EXISTS public.cjpl_js_lot_serial_location_with_components;

CREATE OR REPLACE VIEW public.cjpl_js_lot_serial_location_with_components AS
WITH lot_info AS (
    SELECT
        a.product_id,
        a.id AS lot_id,
        d.date AS move_date,
        e.date AS picking_date,
        b.location_id,
        e.partner_id
    FROM
        stock_production_lot a
        JOIN stock_quant b ON a.id = b.lot_id
        JOIN stock_quant_move_rel c ON c.quant_id = b.id
        JOIN stock_move d ON d.id = c.move_id
        LEFT JOIN stock_picking e ON e.id = d.picking_id
    WHERE
        b.location_id <> 18
        AND d.state::text = 'done'::text
        AND b.location_id = d.location_dest_id
        AND a.product_id = d.product_id
),
lot_location AS (
    SELECT DISTINCT ON (lot_info.lot_id)
        row_number() OVER () AS id,
        lot_info.lot_id,
        lot_info.product_id,
        lot_info.location_id,
        lot_info.partner_id
    FROM
        lot_info
    ORDER BY
        lot_info.lot_id,
        lot_info.picking_date DESC NULLS LAST
),
product_seq_component_details AS (
    SELECT
        pt.id AS pt_id,
        pt.add_product_id,
        pc.can_altered,
        ctl.seq_no,
        pta.name AS component
    FROM
        product_template pt
        JOIN product_category pc ON pt.categ_id = pc.id
        JOIN wiz_add_product wap ON wap.id = pt.add_product_id
        JOIN category_template_line ctl ON ctl.wiz_add_product_id = wap.id
        JOIN product_product pp_1 ON pp_1.id = ctl.product_id
        JOIN product_template pta ON pp_1.product_tmpl_id = pta.id
    WHERE
        pc.can_altered = TRUE
),
product_seq_component AS (
    SELECT
        product_seq_component_details.pt_id,
        CASE WHEN product_seq_component_details.seq_no = 10 THEN
            product_seq_component_details.component
        ELSE
            NULL::character varying
        END AS seq_010,
        CASE WHEN product_seq_component_details.seq_no = 20 THEN
            product_seq_component_details.component
        ELSE
            NULL::character varying
        END AS seq_020,
        CASE WHEN product_seq_component_details.seq_no = 30 THEN
            product_seq_component_details.component
        ELSE
            NULL::character varying
        END AS seq_030,
        CASE WHEN product_seq_component_details.seq_no = 40 THEN
            product_seq_component_details.component
        ELSE
            NULL::character varying
        END AS seq_040,
        CASE WHEN product_seq_component_details.seq_no = 50 THEN
            product_seq_component_details.component
        ELSE
            NULL::character varying
        END AS seq_050,
        CASE WHEN product_seq_component_details.seq_no = 60 THEN
            product_seq_component_details.component
        ELSE
            NULL::character varying
        END AS seq_060,
        CASE WHEN product_seq_component_details.seq_no = 70 THEN
            product_seq_component_details.component
        ELSE
            NULL::character varying
        END AS seq_070,
        CASE WHEN product_seq_component_details.seq_no = 80 THEN
            product_seq_component_details.component
        ELSE
            NULL::character varying
        END AS seq_080,
        CASE WHEN product_seq_component_details.seq_no = 90 THEN
            product_seq_component_details.component
        ELSE
            NULL::character varying
        END AS seq_090,
        CASE WHEN product_seq_component_details.seq_no = 100 THEN
            product_seq_component_details.component
        ELSE
            NULL::character varying
        END AS seq_100,
        CASE WHEN product_seq_component_details.seq_no = 110 THEN
            product_seq_component_details.component
        ELSE
            NULL::character varying
        END AS seq_110,
        CASE WHEN product_seq_component_details.seq_no = 120 THEN
            product_seq_component_details.component
        ELSE
            NULL::character varying
        END AS seq_120,
        CASE WHEN product_seq_component_details.seq_no = 130 THEN
            product_seq_component_details.component
        ELSE
            NULL::character varying
        END AS seq_130,
        CASE WHEN product_seq_component_details.seq_no = 140 THEN
            product_seq_component_details.component
        ELSE
            NULL::character varying
        END AS seq_140,
        CASE WHEN product_seq_component_details.seq_no = 150 THEN
            product_seq_component_details.component
        ELSE
            NULL::character varying
        END AS seq_150
    FROM
        product_seq_component_details
),
product_with_component AS (
    SELECT
        product_seq_component.pt_id,
        COALESCE(string_agg(product_seq_component.seq_010::text, ', '::text), ''::text) AS seq_010,
        COALESCE(string_agg(product_seq_component.seq_020::text, ', '::text), ''::text) AS seq_020,
        COALESCE(string_agg(product_seq_component.seq_030::text, ', '::text), ''::text) AS seq_030,
        COALESCE(string_agg(product_seq_component.seq_040::text, ', '::text), ''::text) AS seq_040,
        COALESCE(string_agg(product_seq_component.seq_050::text, ', '::text), ''::text) AS seq_050,
        COALESCE(string_agg(product_seq_component.seq_060::text, ', '::text), ''::text) AS seq_060,
        COALESCE(string_agg(product_seq_component.seq_070::text, ', '::text), ''::text) AS seq_070,
        COALESCE(string_agg(product_seq_component.seq_080::text, ', '::text), ''::text) AS seq_080,
        COALESCE(string_agg(product_seq_component.seq_090::text, ', '::text), ''::text) AS seq_090,
        COALESCE(string_agg(product_seq_component.seq_100::text, ', '::text), ''::text) AS seq_100,
        COALESCE(string_agg(product_seq_component.seq_110::text, ', '::text), ''::text) AS seq_110,
        COALESCE(string_agg(product_seq_component.seq_120::text, ', '::text), ''::text) AS seq_120,
        COALESCE(string_agg(product_seq_component.seq_130::text, ', '::text), ''::text) AS seq_130,
        COALESCE(string_agg(product_seq_component.seq_140::text, ', '::text), ''::text) AS seq_140,
        COALESCE(string_agg(product_seq_component.seq_150::text, ', '::text), ''::text) AS seq_150
    FROM
        product_seq_component
    GROUP BY
        product_seq_component.pt_id
),
partner_is_not_company_with_no_parent AS (
    SELECT
        a.id,
        a.name AS company_partner_name,
        a.name AS company_name,
        a.parent_id,
        a.is_company
    FROM
        res_partner a
    WHERE
        NOT a.is_company
        AND a.parent_id IS NULL
),
partner_is_a_company AS (
    SELECT
        a.id,
        a.name AS company_partner_name,
        a.name AS company_name,
        a.parent_id,
        a.is_company
    FROM
        res_partner a
    WHERE
        a.is_company
),
partner_is_not_company_but_has_parent AS (
    SELECT
        a.id,
        a.name AS company_partner_name,
        COALESCE(b.company_name, a.name) AS company_name,
        a.parent_id,
        a.is_company
    FROM
        res_partner a
    LEFT JOIN partner_is_a_company b ON a.parent_id = b.id
WHERE
    NOT a.is_company
    AND a.parent_id IS NOT NULL
),
partner_all_companies AS (
    SELECT
        a.id,
        a.company_partner_name,
        a.company_name,
        a.parent_id,
        a.is_company
    FROM
        partner_is_a_company a
    UNION
    SELECT
        b.id,
        b.company_partner_name,
        b.company_name,
        b.parent_id,
        b.is_company
    FROM
        partner_is_not_company_but_has_parent b
    UNION
    SELECT
        c.id,
        c.company_partner_name,
        c.company_name,
        c.parent_id,
        c.is_company
    FROM
        partner_is_not_company_with_no_parent c
)
SELECT
    ll.lot_id,
    ll.product_id,
    ll.location_id,
    ll.partner_id,
    pp.id AS pp_id,
    pat.id AS pat_id,
    pwc.pt_id,
    sw.name AS warehouse,
    sl.name AS LOCATION,
    spl.name AS serial_number,
    pat.product_challan,
    pat.name AS product_name,
    pcy.name AS category_name,
    pa.company_name,
    pa.company_partner_name,
    pwc.seq_010,
    pwc.seq_020,
    pwc.seq_030,
    pwc.seq_040,
    pwc.seq_050,
    pwc.seq_060,
    pwc.seq_070,
    pwc.seq_080,
    pwc.seq_090,
    pwc.seq_100,
    pwc.seq_110,
    pwc.seq_120,
    pwc.seq_130,
    pwc.seq_140,
    pwc.seq_150
FROM
    lot_location ll
    JOIN product_product pp ON pp.id = ll.product_id
    JOIN product_template pat ON pp.product_tmpl_id = pat.id
    LEFT JOIN product_with_component pwc ON pat.id = pwc.pt_id
    LEFT JOIN product_category pcy ON pcy.id = pat.categ_id
    JOIN stock_location sl ON ll.location_id = sl.id
    LEFT JOIN stock_warehouse sw ON sl.wr_id = sw.id
    LEFT JOIN partner_all_companies pa ON pa.id = ll.partner_id
    JOIN stock_production_lot spl ON ll.lot_id = spl.id
ORDER BY
    ll.location_id,
    pcy.name,
    pat.product_challan;

ALTER TABLE public.cjpl_js_lot_serial_location_with_components OWNER TO postgres;

