-- View: jasper.cjpl_js_negative_location_stock_serial_data

DROP VIEW IF EXISTS jasper.cjpl_js_erroneous_negative_stock;
DROP VIEW IF EXISTS jasper.cjpl_js_negative_location_stock_serial_data;
DROP VIEW IF EXISTS jasper.cjpl_js_negative_stock_location_serial_data;

CREATE OR REPLACE VIEW jasper.cjpl_js_negative_stock_location_serial_data
 AS
 WITH internal_loc_name AS (
         SELECT stock_location.id,
            stock_location.allow_negative_stock,
            "substring"(stock_location.complete_name::text, "position"(stock_location.complete_name::text, '/'::text) + 1) AS location_name
           FROM stock_location
          WHERE stock_location.usage::text = 'internal'::text
        ), out_move AS (
         SELECT m.product_id,
            m.location_id AS loc_id,
            sum(0) AS in_qty,
            sum(m.product_qty) AS out_qty
           FROM stock_move m
          WHERE m.state::text ~~ 'done'::text AND (m.location_id IN ( SELECT internal_loc_name.id
                   FROM internal_loc_name))
          GROUP BY m.product_id, m.location_id
        ), in_move AS (
         SELECT m.product_id,
            m.location_dest_id AS loc_id,
            sum(m.product_qty) AS in_qty,
            sum(0) AS out_qty
           FROM stock_move m
          WHERE m.state::text ~~ 'done'::text AND (m.location_dest_id IN ( SELECT internal_loc_name.id
                   FROM internal_loc_name))
          GROUP BY m.product_id, m.location_dest_id
        ), all_move AS (
         SELECT a.product_id,
            a.loc_id,
            a.in_qty,
            a.out_qty
           FROM in_move a
        UNION
         SELECT b.product_id,
            b.loc_id,
            b.in_qty,
            b.out_qty
           FROM out_move b
        ), sum_move AS (
         SELECT all_move.product_id,
            all_move.loc_id,
            sum(all_move.in_qty) AS in_qty,
            sum(all_move.out_qty) AS out_qty
           FROM all_move
          GROUP BY all_move.product_id, all_move.loc_id
        ), neg_stock AS (
         SELECT a.product_id,
            a.loc_id,
            a.in_qty - a.out_qty AS stock_in_hand
           FROM sum_move a
             JOIN internal_loc_name l_1 ON a.loc_id = l_1.id
          WHERE (a.in_qty - a.out_qty) < 0::numeric AND COALESCE(l_1.allow_negative_stock, false)
        ), item_stk_move AS (
         SELECT ma.picking_id,
            ma.product_id,
            ma.location_id AS loc_id,
            ma.serial_id,
            0 AS in_qty,
            ma.product_qty AS out_qty
           FROM stock_move ma
             JOIN neg_stock ns ON ma.product_id = ns.product_id AND ma.location_id = ns.loc_id
          WHERE ma.state::text ~~ 'done'::text AND ma.picking_id IS NOT NULL
        UNION
         SELECT mb.picking_id,
            mb.product_id,
            mb.location_dest_id AS loc_id,
            mb.serial_id,
            mb.product_qty AS in_qty,
            0 AS out_qty
           FROM stock_move mb
             JOIN neg_stock nt ON mb.product_id = nt.product_id AND mb.location_dest_id = nt.loc_id
          WHERE mb.state::text ~~ 'done'::text AND mb.picking_id IS NOT NULL
        ), item_alt_move AS (
         SELECT ma.origin,
            ma.product_id,
            ma.location_id AS loc_id,
            0 AS in_qty,
            ma.product_qty AS out_qty
           FROM stock_move ma
             JOIN neg_stock ns ON ma.product_id = ns.product_id AND ma.location_id = ns.loc_id
          WHERE ma.state::text ~~ 'done'::text AND ma.picking_id IS NULL
        UNION
         SELECT mb.origin,
            mb.product_id,
            mb.location_dest_id AS loc_id,
            mb.product_qty AS in_qty,
            0 AS out_qty
           FROM stock_move mb
             JOIN neg_stock nt ON mb.product_id = nt.product_id AND mb.location_dest_id = nt.loc_id
          WHERE mb.state::text ~~ 'done'::text AND mb.picking_id IS NULL
        ), item_all_move AS (
         SELECT ism.loc_id,
            ism.product_id,
            ism.serial_id,
            ism.in_qty,
            ism.out_qty,
            spk.name,
            spk.write_date,
            date(spk.date_done) AS move_date
           FROM item_stk_move ism
             JOIN stock_picking spk ON ism.picking_id = spk.id
        UNION
         SELECT iam.loc_id,
            iam.product_id,
            COALESCE(ialt.serial_no, COALESCE(cna.serial_no, nia.serial_no)) AS serial_id,
            iam.in_qty,
            iam.out_qty,
            iam.origin AS name,
            COALESCE(ialt.write_date, COALESCE(cna.write_date, nia.write_date)) AS write_date,
            date(COALESCE(ialt.validate_date, COALESCE(cna.validate_date, nia.validate_date))) AS move_date
           FROM item_alt_move iam
             LEFT JOIN item_alteration ialt ON ialt.name::text = iam.origin::text
             LEFT JOIN conversion_non_alterable cna ON cna.name::text = iam.origin::text
             LEFT JOIN non_item_alteration nia ON nia.name::text = iam.origin::text
        ), non_zero_stock AS (
         SELECT item_all_move.loc_id,
            item_all_move.product_id,
            item_all_move.serial_id,
            sum(item_all_move.in_qty - item_all_move.out_qty) AS sum
           FROM item_all_move
          WHERE item_all_move.serial_id IS NOT NULL
          GROUP BY item_all_move.loc_id, item_all_move.product_id, item_all_move.serial_id
         HAVING sum(item_all_move.in_qty - item_all_move.out_qty) <> 0::numeric
        ), all_the_moves AS (
         SELECT iam1.loc_id,
            iam1.product_id,
            iam1.serial_id,
            iam1.in_qty,
            iam1.out_qty,
            iam1.name,
            iam1.write_date,
            iam1.move_date
           FROM item_all_move iam1
             JOIN non_zero_stock nzs ON iam1.loc_id = nzs.loc_id AND iam1.product_id = nzs.product_id AND iam1.serial_id = nzs.serial_id
          WHERE iam1.serial_id IS NOT NULL
        UNION
         SELECT iam2.loc_id,
            iam2.product_id,
            iam2.serial_id,
            iam2.in_qty,
            iam2.out_qty,
            iam2.name,
            iam2.write_date,
            iam2.move_date
           FROM item_all_move iam2
          WHERE iam2.serial_id IS NULL
        )
 SELECT g.loc_id,
    l.location_name AS location,
    g.serial_id,
    spl.name AS serial_number,
    g.product_id,
    pc.name AS category_name,
    t.product_challan,
    t.name AS product_name,
    g.name AS document,
    g.move_date,
    g.write_date,
    g.in_qty,
    g.out_qty,
    sum(g.in_qty - g.out_qty) OVER (PARTITION BY g.loc_id, g.product_id, g.serial_id ORDER BY g.write_date, g.name) AS bal_qty
   FROM all_the_moves g
     JOIN product_product p ON g.product_id = p.id
     JOIN product_template t ON p.product_tmpl_id = t.id
     JOIN internal_loc_name l ON g.loc_id = l.id
     JOIN product_category pc ON t.categ_id = pc.id
     LEFT JOIN stock_production_lot spl ON spl.id = g.serial_id
  ORDER BY g.loc_id, g.product_id, g.serial_id, g.write_date, g.name;

ALTER TABLE jasper.cjpl_js_negative_stock_location_serial_data
    OWNER TO postgres;


