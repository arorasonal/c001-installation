WITH move_lines AS (
    SELECT
        sp.id,
        sp.name,
        date(sp.date_done) AS move_date,
        sp.origin,
        sp.partner_id,
        COALESCE(rp.parent_id, rp.id) AS parent_id,
        sp.picking_type_id,
        sp.order_id,
        sp.location_id,
        sla.name AS from_location,
        sp.location_dest_id,
        slb.name AS to_location,
        spt.name AS movement_name,
        spt.code AS movement_type,
        sm.product_id,
        sm.product_qty,
        -- stock movement multiplier is first computed based location
        -- if locations do not fall in locations displayed then -
        --    next its based on delivery document name == picking_type_id
        --    if delivery document name is not amongst those displayed here
        --       its based on whether stock picking type is outgoing or incoming
        -- need to put stock movement multiplier info on
        --    (a) location
        --    (b) stock picking type
        -- so that this sql is not dependent on name of location or stock picking type
        CASE WHEN sla.name::text = 'Customers'::text THEN
            '-1'::integer::numeric * sm.product_qty
        WHEN sla.name::text = 'Rental Out Stock'::text THEN
            '-1'::integer::numeric * sm.product_qty
        WHEN sla.name::text = 'Repair & Warrnty Replacement'::text THEN
            '-1'::integer::numeric * sm.product_qty
        WHEN slb.name::text = 'Customers'::text THEN
            1::numeric * sm.product_qty
        WHEN slb.name::text = 'Rental Out Stock'::text THEN
            1::numeric * sm.product_qty
        WHEN slb.name::text = 'Repair & Warrnty Replacement'::text THEN
            1::numeric * sm.product_qty
        WHEN spt.name::text = 'Delivery Note - Repair & Warranty Replacement'::text THEN
            1::numeric * sm.product_qty
        WHEN spt.name::text = 'Delivery Orders - Demo'::text THEN
            1::numeric * sm.product_qty
        WHEN spt.name::text = 'Delivery Orders - for View Purpose'::text THEN
            1::numeric * sm.product_qty
        WHEN spt.name::text = 'Delivery Orders - Rental'::text THEN
            1::numeric * sm.product_qty
        WHEN spt.name::text = 'Delivery Orders - Repair'::text THEN
            1::numeric * sm.product_qty
        WHEN spt.name::text = 'Delivery Orders - Replacement'::text THEN
            1::numeric * sm.product_qty
        WHEN spt.name::text = 'Delivery Orders - Sale'::text THEN
            1::numeric * sm.product_qty
        WHEN spt.name::text = 'Delivery Orders Purpose - RI'::text THEN
            1::numeric * sm.product_qty
        WHEN spt.name::text = 'Receipts - against Purchase'::text THEN
            '-1'::integer::numeric * sm.product_qty
        WHEN spt.name::text = 'Returns from Customer'::text THEN
            '-1'::integer::numeric * sm.product_qty
        WHEN spt.name::text = 'Returns from Supplier'::text THEN
            '-1'::integer::numeric * sm.product_qty
        WHEN spt.name::text = 'Returns to Supplier'::text THEN
            1::numeric * sm.product_qty
        WHEN spt.code::text = 'outgoing'::text THEN
            sm.product_qty
        WHEN spt.code::text = 'incoming'::text THEN
            - sm.product_qty
        ELSE
            NULL::numeric
        END AS move_qty,
        COALESCE(sm.serial_id, 0) AS serial_id,
        row_number() OVER () AS distinct_key,
        (((((((COALESCE(rp.parent_id, rp.id)::text || '-'::text) || sm.product_id::text) || '-'::text) || COALESCE(sm.serial_id, 0)::text) || '-'::text) || to_char(sp.date_done, ('yyyy-mm-dd hh24:mi:ss'::text) )) || '-'::text) || row_number() OVER ()::text AS sort_key
    FROM
        stock_picking sp
        JOIN stock_picking_type spt ON sp.picking_type_id = spt.id
        JOIN stock_location sla ON sla.id = sp.location_id
        JOIN stock_location slb ON slb.id = sp.location_dest_id
        JOIN stock_move sm ON sm.picking_id = sp.id
        JOIN res_partner rp ON sp.partner_id = rp.id
    WHERE
        sp.partner_id IS NOT NULL
        AND sm.serial_id IS NOT NULL
        AND sp.state::text = 'done'::text
        AND (spt.code::text = ANY (ARRAY['incoming'::character varying::text,
                'outgoing'::character varying::text]))
),
move_lines_sum AS (
    SELECT
        move_lines.parent_id,
        move_lines.product_id,
        move_lines.serial_id,
        sum(move_lines.move_qty) AS balance_qty
    FROM
        move_lines
    GROUP BY
        move_lines.parent_id,
        move_lines.product_id,
        move_lines.serial_id
    HAVING
        sum(move_lines.move_qty) < 0::numeric
),
neg_bal_serial AS (
    SELECT DISTINCT
        serial_id
    FROM
        move_lines_sum
),
partner_is_not_company_with_no_parent AS (
    SELECT
        a_1.id,
        a_1.name AS company_partner_name,
        a_1.name AS company_name,
        a_1.parent_id,
        a_1.is_company
    FROM
        res_partner a_1
    WHERE
        NOT a_1.is_company
        AND a_1.parent_id IS NULL
),
partner_is_a_company AS (
    SELECT
        a_1.id,
        a_1.name AS company_partner_name,
        a_1.name AS company_name,
        a_1.parent_id,
        a_1.is_company
    FROM
        res_partner a_1
    WHERE
        a_1.is_company
),
partner_is_not_company_but_has_parent AS (
    SELECT
        a_1.id,
        a_1.name AS company_partner_name,
        COALESCE(b_1.company_name, a_1.name) AS company_name,
        a_1.parent_id,
        a_1.is_company
    FROM
        res_partner a_1
    LEFT JOIN partner_is_a_company b_1 ON a_1.parent_id = b_1.id
WHERE
    NOT a_1.is_company
    AND a_1.parent_id IS NOT NULL
),
partner_all_companies AS (
    SELECT
        a_1.id,
        a_1.company_partner_name,
        a_1.company_name,
        a_1.parent_id,
        a_1.is_company
    FROM
        partner_is_a_company a_1
    UNION
    SELECT
        b_1.id,
        b_1.company_partner_name,
        b_1.company_name,
        b_1.parent_id,
        b_1.is_company
    FROM
        partner_is_not_company_but_has_parent b_1
    UNION
    SELECT
        c_1.id,
        c_1.company_partner_name,
        c_1.company_name,
        c_1.parent_id,
        c_1.is_company
    FROM
        partner_is_not_company_with_no_parent c_1
)
SELECT
    a.id,
    a.picking_id,
    a.create_date AS stock_move_create,
    a.write_date AS stock_move_write,
    a.location_id,
    sla.name AS from_location,
    a.location_dest_id,
    slb.name AS to_location,
    b.name AS doc_name,
    b.date_done,
    c.name AS doc_type,
    a.serial_id,
    lot.name AS serial_number,
    lot.product_id,
    COALESCE(b.partner_id, 0) AS partner_id,
    t.name AS product_name,
    t.product_challan,
    pc.name AS category_name,
    COALESCE(pa.company_name, ''::character varying) AS company_name,
    COALESCE(pa.company_partner_name, ''::character varying) AS partner_name
FROM
    stock_move a
    LEFT JOIN stock_picking b ON b.id = a.picking_id
    LEFT JOIN stock_picking_type c ON b.picking_type_id = c.id
    JOIN stock_location sla ON sla.id = a.location_id
    JOIN stock_location slb ON slb.id = a.location_dest_id
    JOIN stock_production_lot lot ON a.serial_id = lot.id
    JOIN product_product p ON lot.product_id = p.id
    JOIN product_template t ON p.product_tmpl_id = t.id
    JOIN product_category pc ON pc.id = t.categ_id
    JOIN neg_bal_serial nbs ON nbs.serial_id = a.serial_id
    LEFT JOIN partner_all_companies pa ON pa.id = b.partner_id
WHERE
    a.serial_id IS NOT NULL
    AND a.state = 'done'
ORDER BY
    a.serial_id,
    a.write_date;

