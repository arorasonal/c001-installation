-- View: public.cjpl_js_sale_team_users

-- DROP VIEW public.cjpl_js_sale_team_users;

CREATE OR REPLACE VIEW "public"."cjpl_js_sale_team_users" AS
SELECT
    b.login AS user_login,
    d.name AS user_name,
    c.name AS team_name
FROM (((user_sales_team_rel a
            JOIN res_users b ON ((b.id = a.res_user_rel)))
        JOIN crm_team c ON ((c.id = a.sales_team_rel)))
    JOIN res_partner d ON ((d.id = b.partner_id)))
ORDER BY
    c.name,
    d.name;

ALTER TABLE public.cjpl_js_sale_team_users
    OWNER TO postgres;