-- View: public.cjpl_js_inter_warehouse_transfers

DROP VIEW public.cjpl_js_inter_warehouse_transfers;

CREATE OR REPLACE VIEW public.cjpl_js_inter_warehouse_transfers
 AS
 WITH pick_bs AS (
         SELECT a_1.id AS spo_id,
            b_1.id AS btl_id,
            a_1.transfer_line_id,
            a_1.ordered_qty,
            a_1.product_qty AS qty,
            a_1.picking_id,
            b_1.rate,
            a_1.product_qty * b_1.rate AS amt,
            b_1.product_id,
            c.origin,
            date(c.date_done) AS delivery_date,
            c.name,
            c.location_id,
            c.location_dest_id
           FROM stock_pack_operation a_1
             JOIN barcode_transfer_line b_1 ON b_1.id = a_1.transfer_line_id
             JOIN stock_picking c ON a_1.picking_id = c.id
             JOIN stock_picking_type d ON c.picking_type_id = d.id
          WHERE COALESCE(d.inter_warehouse_transfer, false) AND c.state::text = 'done'::text 
        ), tax_rates AS (
         SELECT account_tax.id,
            account_tax.amount
           FROM account_tax
          WHERE account_tax.amount_type::text = 'percent'::text AND account_tax.active
        UNION ALL
         SELECT a_1.id,
            c.amount
           FROM account_tax a_1
             JOIN account_tax_filiation_rel b_1 ON a_1.id = b_1.parent_tax
             JOIN account_tax c ON b_1.child_tax = c.id
          WHERE a_1.amount_type::text = 'group'::text AND a_1.active
        ), serial_line_tax AS (
         SELECT a_1.btl_id,
            round(a_1.amt * c.amount / 100::numeric) AS gst
           FROM pick_bs a_1
             JOIN account_tax_barcode_transfer_line_rel b_1 ON a_1.btl_id = b_1.barcode_transfer_line_id
             JOIN tax_rates c ON b_1.account_tax_id = c.id
        ), serial_wise_tax AS (
         SELECT serial_line_tax.btl_id,
            sum(serial_line_tax.gst) AS gst
           FROM serial_line_tax
          GROUP BY serial_line_tax.btl_id
        ), location AS (
         SELECT a_1.id,
            a_1.complete_name,
            b_1.name AS warehouse,
            c.gstin
           FROM stock_location a_1
             LEFT JOIN stock_warehouse b_1 ON a_1.wr_id = b_1.id
             JOIN res_partner c ON b_1.partner_id = c.id
        )
 SELECT row_number() OVER () AS id,
    a.name AS invoice_no,
    a.delivery_date,
    l.warehouse AS from_warehouse,
    l.gstin AS from_gstin,
    m.warehouse AS to_warehouse,
    m.gstin AS to_gstin,
    t.product_challan AS product,
    t1.name AS category,
    t1.hsn_no,
    a.qty,
    a.amt,
    COALESCE(b.gst, 0::numeric) AS gst,
    a.amt + COALESCE(b.gst, 0::numeric) AS tot_amt
   FROM pick_bs a
     LEFT JOIN serial_wise_tax b ON a.btl_id = b.btl_id
     JOIN product_product p ON a.product_id = p.id
     JOIN product_template t ON p.product_tmpl_id = t.id
     LEFT JOIN product_category t1 ON t1.id = t.categ_id
     LEFT JOIN location l ON l.id = a.location_id
     LEFT JOIN location m ON m.id = a.location_dest_id;

ALTER TABLE public.cjpl_js_inter_warehouse_transfers
    OWNER TO postgres;


