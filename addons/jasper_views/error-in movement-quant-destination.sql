SELECT
    sm.id,
    sm.origin,
    sm.location_id,
    sm.location_dest_id,
    sm.name,
    sq.lot_id,
    sq.location_id
FROM
    stock_move sm
    JOIN stock_quant_move_rel sl ON sm.id = sl.move_id
    JOIN stock_quant sq ON sq.id = sl.quant_id
WHERE
    sm.location_dest_id <> sq.location_id
    AND sq.location_id <> sm.location_id
    AND sm.origin IS NOT NULL
    AND sq.lot_id IS NOT NULL
    AND
    LEFT (sm.origin, 2) <> 'PO'
ORDER BY
    sm.origin
