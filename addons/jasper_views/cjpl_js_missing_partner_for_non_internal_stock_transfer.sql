-- View: public.cjpl_js_missing_partner_non_internal_stock_transfer

DROP VIEW IF EXISTS public.cjpl_js_missing_partner_non_internal_stock_transfer;

CREATE OR REPLACE VIEW public.cjpl_js_missing_partner_non_internal_stock_transfer
 AS
 SELECT sp.id,
    sp.location_id,
    sp.location_dest_id,
    sp.origin,
    sp.date_done,
    sla.name AS from_location,
    slb.name AS to_location,
    sp.name AS doc_no,
    spt.code,
    spt.name AS doc_type
   FROM stock_picking sp
     JOIN stock_picking_type spt ON sp.picking_type_id = spt.id
     JOIN stock_location sla ON sla.id = sp.location_id
     JOIN stock_location slb ON slb.id = sp.location_dest_id
  WHERE sp.state::text = 'done'::text AND sp.partner_id IS NULL AND spt.code::text <> 'internal'::text
  ORDER BY spt.name, sp.date_done, sp.name;

ALTER TABLE public.cjpl_js_missing_partner_non_internal_stock_transfer
    OWNER TO postgres;


