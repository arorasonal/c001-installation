WITH move_lines AS (
    SELECT
        sp.id,
        sp.name AS doc_no,
        date(sp.date_done) AS move_date,
        sp.origin,
        sp.partner_id,
        COALESCE(rp.parent_id, rp.id) AS parent_id,
        sp.picking_type_id,
        sp.order_id,
        sp.location_id,
        sla.name AS from_location,
        sp.location_dest_id,
        slb.name AS to_location,
        spt.name AS movement_name,
        spt.code AS movement_type,
        sm.product_id,
        sm.product_qty,
        -- stock movement multiplier is first computed based location
        -- if locations do not fall in locations displayed then -
        --    next its based on delivery document name == picking_type_id
        --    if delivery document name is not amongst those displayed here
        --       its based on whether stock picking type is outgoing or incoming
        -- need to put stock movement multiplier info on
        --    (a) location
        --    (b) stock picking type
        -- so that this sql is not dependent on name of location or stock picking type
        CASE WHEN sla.name::text = 'Customers'::text THEN
            '-1'::integer::numeric * sm.product_qty
        WHEN sla.name::text = 'Rental Out Stock'::text THEN
            '-1'::integer::numeric * sm.product_qty
        WHEN sla.name::text = 'Repair & Warrnty Replacement'::text THEN
            '-1'::integer::numeric * sm.product_qty
        WHEN slb.name::text = 'Customers'::text THEN
            1::numeric * sm.product_qty
        WHEN slb.name::text = 'Rental Out Stock'::text THEN
            1::numeric * sm.product_qty
        WHEN slb.name::text = 'Repair & Warrnty Replacement'::text THEN
            1::numeric * sm.product_qty
        WHEN spt.name::text = 'Delivery Note - Repair & Warranty Replacement'::text THEN
            1::numeric * sm.product_qty
        WHEN spt.name::text = 'Delivery Orders - Demo'::text THEN
            1::numeric * sm.product_qty
        WHEN spt.name::text = 'Delivery Orders - for View Purpose'::text THEN
            1::numeric * sm.product_qty
        WHEN spt.name::text = 'Delivery Orders - Rental'::text THEN
            1::numeric * sm.product_qty
        WHEN spt.name::text = 'Delivery Orders - Repair'::text THEN
            1::numeric * sm.product_qty
        WHEN spt.name::text = 'Delivery Orders - Replacement'::text THEN
            1::numeric * sm.product_qty
        WHEN spt.name::text = 'Delivery Orders - Sale'::text THEN
            1::numeric * sm.product_qty
        WHEN spt.name::text = 'Delivery Orders Purpose - RI'::text THEN
            1::numeric * sm.product_qty
        WHEN spt.name::text = 'Receipts - against Purchase'::text THEN
            '-1'::integer::numeric * sm.product_qty
        WHEN spt.name::text = 'Returns from Customer'::text THEN
            '-1'::integer::numeric * sm.product_qty
        WHEN spt.name::text = 'Returns from Supplier'::text THEN
            '-1'::integer::numeric * sm.product_qty
        WHEN spt.name::text = 'Returns to Supplier'::text THEN
            1::numeric * sm.product_qty
        WHEN spt.code::text = 'outgoing'::text THEN
            sm.product_qty
        WHEN spt.code::text = 'incoming'::text THEN
            - sm.product_qty
        ELSE
            NULL::numeric
        END AS move_qty,
        COALESCE(sm.serial_id, 0) AS serial_id,
        lot.name AS serial_number,
        row_number() OVER () AS distinct_key,
        COALESCE(rp.parent_id, rp.id)::text || '-'::text || lot.name || '-'::text || to_char(date(sp.date_done)::timestamp WITH time zone, 'yyyy-mm-dd'::text || '-'::text) || row_number() OVER ()::text AS partition_key
    FROM
        stock_picking sp
        JOIN stock_picking_type spt ON sp.picking_type_id = spt.id
        JOIN stock_location sla ON sla.id = sp.location_id
        JOIN stock_location slb ON slb.id = sp.location_dest_id
        JOIN stock_move sm ON sm.picking_id = sp.id
        JOIN res_partner rp ON sp.partner_id = rp.id
        JOIN stock_production_lot lot ON lot.id = sm.serial_id
    WHERE
        sm.serial_id IS NOT NULL
        AND sp.partner_id IS NOT NULL
        AND sp.state::text = 'done'::text
        AND (spt.code::text = ANY (ARRAY['incoming'::character varying::text,
                'outgoing'::character varying::text]))
),
move_lines_sum AS (
    SELECT
        parent_id,
        serial_number,
        sum(move_qty) AS balance_qty
    FROM
        move_lines
    GROUP BY
        parent_id,
        serial_number
    HAVING
        sum(move_qty) <> 0
),
move_lines_rank AS (
    SELECT
        m.*,
        ROW_NUMBER() OVER (PARTITION BY parent_id,
            serial_number ORDER BY partition_key DESC) AS rn
    FROM
        move_lines AS m
)
SELECT
    mlr.id,
    mlr.parent_id,
    mlr.partner_id,
    mlr.picking_type_id,
    mlr.order_id,
    mlr.location_id,
    mlr.location_dest_id,
    mlr.product_id,
    mlr.serial_id,
    pa.name AS company_name,
    pb.name AS company_partner_name,
    mlr.serial_number,
    pc.name AS category_name,
    t.name AS product_name,
    t.product_challan,
    mlr.doc_no,
    mlr.move_date,
    COALESCE(mlr.origin, ''::character varying) AS org_doc,
    COALESCE(sale_order.name, ''::character varying) AS so_no,
    mlr.from_location,
    mlr.to_location,
    mlr.movement_name,
    mlr.movement_type,
    mlr.product_qty,
    mls.balance_qty
FROM
    move_lines_rank mlr
    JOIN move_lines_sum mls ON mls.parent_id = mlr.parent_id
        AND mls.serial_number = mlr.serial_number
    JOIN res_partner pa ON pa.id = mlr.parent_id
    JOIN res_partner pb ON pb.id = mlr.partner_id
    JOIN product_product p ON mlr.product_id = p.id
    JOIN product_template t ON p.product_tmpl_id = t.id
    JOIN product_category pc ON pc.id = t.categ_id
    LEFT JOIN sale_order ON mlr.order_id = sale_order.id
WHERE
    mlr.rn = 1
ORDER BY
    pa.name,
    mlr.serial_number;

;

