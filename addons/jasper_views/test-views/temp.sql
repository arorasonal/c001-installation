SELECT
   a.id,
   a.lot_id,
   a.location_id,
   c.name AS LOCATION,
   a.product_id,
   a.write_date,
   b.name AS serial_number,
   t.name AS product,
   t.product_challan
FROM
   stock_quant a
   JOIN stock_production_lot b ON b.id = a.lot_id
   JOIN stock_location c ON a.location_id = c.id
   JOIN product_product p ON a.product_id = p.id
   JOIN product_template t ON p.product_tmpl_id = t.id
WHERE
   a.location_id <> 18
ORDER BY
   b.name,
   a.write_date

/*
   b.name IN ('CJDKE05372JD', 'CJDKE02653GC', 'CJDKE08163HH', 'CJDKE10278LJ', 'CJLPE01003DB', 'CJLPE10141EJ', 'CJLPE10147EJ', 'CJLPE10148EJ', 'CJLPE10460FJ', 'CJLPE10497FJ', 'CJSRE00108HI')
*/
