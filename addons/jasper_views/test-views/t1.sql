SELECT
    pt.add_product_id,
    pc.name,
    pc.can_altered,
    wap.id,
    wap.name,
    ctl.seq_no,
    ctl.product_id
FROM
    product_template pt
    JOIN product_category pc ON pt.categ_id = pc.id
    JOIN wiz_add_product wap ON wap.id = pt.add_product_id
    JOIN category_template_line ctl ON ctl.wiz_add_product_id = wap.id
    JOIN product_product pp ON pp.id = ctl.product_id
WHERE
    pc.can_altered = TRUE
