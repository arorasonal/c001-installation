WITH move_lines AS (
    SELECT
        sp.id,
        sp.name AS doc_no,
        date(sp.date_done) AS move_date,
        sp.origin,
        sp.partner_id,
        COALESCE(rp.parent_id, rp.id) AS parent_id,
        sp.picking_type_id,
        sp.transaction_type_id,
        sp.order_id,
        sp.location_id,
        sla.name AS from_location,
        sp.location_dest_id,
        slb.name AS to_location,
        spt.name AS movement_name,
        spt.code AS movement_type,
        sm.product_id,
        sm.product_qty,
        CASE WHEN ((sla.name)::text = 'Customers'::text) THEN
        (('-1'::integer)::numeric * sm.product_qty)
        WHEN ((sla.name)::text = 'Rental Out Stock'::text) THEN
        (('-1'::integer)::numeric * sm.product_qty)
        WHEN ((sla.name)::text = 'Repair & Warrnty Replacement'::text) THEN
        (('-1'::integer)::numeric * sm.product_qty)
        WHEN ((slb.name)::text = 'Customers'::text) THEN
        ((1)::numeric * sm.product_qty)
        WHEN ((slb.name)::text = 'Rental Out Stock'::text) THEN
        ((1)::numeric * sm.product_qty)
        WHEN ((slb.name)::text = 'Repair & Warrnty Replacement'::text) THEN
        ((1)::numeric * sm.product_qty)
        WHEN ((spt.name)::text = 'Delivery Note - Repair & Warranty Replacement'::text) THEN
        ((1)::numeric * sm.product_qty)
        WHEN ((spt.name)::text = 'Delivery Orders - Demo'::text) THEN
        ((1)::numeric * sm.product_qty)
        WHEN ((spt.name)::text = 'Delivery Orders - for View Purpose'::text) THEN
        ((1)::numeric * sm.product_qty)
        WHEN ((spt.name)::text = 'Delivery Orders - Rental'::text) THEN
        ((1)::numeric * sm.product_qty)
        WHEN ((spt.name)::text = 'Delivery Orders - Repair'::text) THEN
        ((1)::numeric * sm.product_qty)
        WHEN ((spt.name)::text = 'Delivery Orders - Replacement'::text) THEN
        ((1)::numeric * sm.product_qty)
        WHEN ((spt.name)::text = 'Delivery Orders - Sale'::text) THEN
        ((1)::numeric * sm.product_qty)
        WHEN ((spt.name)::text = 'Delivery Orders Purpose - RI'::text) THEN
        ((1)::numeric * sm.product_qty)
        WHEN ((spt.name)::text = 'Receipts - against Purchase'::text) THEN
        (('-1'::integer)::numeric * sm.product_qty)
        WHEN ((spt.name)::text = 'Returns from Customer'::text) THEN
        (('-1'::integer)::numeric * sm.product_qty)
        WHEN ((spt.name)::text = 'Returns from Supplier'::text) THEN
        (('-1'::integer)::numeric * sm.product_qty)
        WHEN ((spt.name)::text = 'Returns to Supplier'::text) THEN
        ((1)::numeric * sm.product_qty)
        WHEN ((spt.code)::text = 'outgoing'::text) THEN
            sm.product_qty
        WHEN ((spt.code)::text = 'incoming'::text) THEN
        (- sm.product_qty)
    ELSE
        NULL::numeric
        END AS move_qty,
        COALESCE(sm.serial_id, 0) AS serial_id,
        lot_1.name AS serial_number,
        row_number() OVER () AS distinct_key,
        ((((((COALESCE(rp.parent_id, rp.id))::text || '-'::text) || (lot_1.name)::text) || '-'::text) || to_char(sp.date_done, ('yyyy-mm-dd hh24:mi:ss'::text) || '-'::text)) || (row_number() OVER ())::text) AS partition_key
    FROM ((((((stock_picking sp
                            JOIN stock_picking_type spt ON ((sp.picking_type_id = spt.id)))
                        JOIN stock_location sla ON ((sla.id = sp.location_id)))
                    JOIN stock_location slb ON ((slb.id = sp.location_dest_id)))
                JOIN stock_move sm ON ((sm.picking_id = sp.id)))
            JOIN res_partner rp ON ((sp.partner_id = rp.id)))
        JOIN stock_production_lot lot_1 ON ((lot_1.id = sm.serial_id)))
    WHERE ((sm.serial_id IS NOT NULL)
        AND (sp.partner_id IS NOT NULL)
        AND ((sp.state)::text = 'done'::text)
        AND ((spt.code)::text = ANY (ARRAY[('incoming'::character varying)::text,
                ('outgoing'::character varying)::text])))
),
move_lines_sum AS (
    SELECT
        move_lines.parent_id,
        move_lines.serial_number,
        sum(move_lines.move_qty) AS balance_qty
    FROM
        move_lines
    GROUP BY
        move_lines.parent_id,
        move_lines.serial_number
    HAVING (sum(move_lines.move_qty) <> (0)::numeric)
),
move_lines_rank AS (
    SELECT
        m.id,
        m.doc_no,
        m.move_date,
        m.origin,
        m.partner_id,
        m.parent_id,
        m.picking_type_id,
        m.transaction_type_id,
        m.order_id,
        m.location_id,
        m.from_location,
        m.location_dest_id,
        m.to_location,
        m.movement_name,
        m.movement_type,
        m.product_id,
        m.product_qty,
        m.move_qty,
        m.serial_id,
        m.serial_number,
        m.distinct_key,
        m.partition_key,
        row_number() OVER (PARTITION BY m.parent_id,
            m.serial_number ORDER BY m.partition_key DESC) AS rn
    FROM
        move_lines m
),
party_serial_stock_summary AS (
    SELECT
        mlr.id,
        mlr.parent_id,
        mlr.partner_id,
        mlr.picking_type_id,
        mlr.order_id,
        mlr.location_id,
        mlr.location_dest_id,
        mlr.product_id,
        mlr.serial_id,
        pa.name AS company_name,
        pb.name AS company_partner_name,
        mlr.serial_number,
        lot.alpha_serial_no AS alpha_no,
        pc.name AS category_name,
        t.name AS product_name,
        t.product_challan,
        mlr.doc_no,
        mlr.move_date,
        COALESCE(mlr.origin, ''::character varying) AS org_doc,
        COALESCE(sale_order.name, ''::character varying) AS so_no,
        mlr.from_location,
        mlr.to_location,
        mlr.movement_name,
        tt.name AS transaction_type,
        mlr.movement_type,
        mlr.product_qty,
        mls.balance_qty
    FROM (((((((((move_lines_rank mlr
                                        JOIN move_lines_sum mls ON (((mls.parent_id = mlr.parent_id)
                                                    AND ((mls.serial_number)::text = (mlr.serial_number)::text))))
                                    JOIN res_partner pa ON ((pa.id = mlr.parent_id)))
                                JOIN res_partner pb ON ((pb.id = mlr.partner_id)))
                            JOIN product_product p ON ((mlr.product_id = p.id)))
                        JOIN product_template t ON ((p.product_tmpl_id = t.id)))
                    JOIN product_category pc ON ((pc.id = t.categ_id)))
                JOIN stock_production_lot lot ON ((lot.id = mlr.serial_id)))
            LEFT JOIN sale_order ON ((mlr.order_id = sale_order.id)))
        LEFT JOIN transaction_type tt ON ((tt.id = mlr.transaction_type_id)))
    WHERE (mlr.rn = 1)
),
all_quant_location AS (
    SELECT
        a.id,
        a.qty,
        a.lot_id,
        a.product_id,
        a.location_id,
        a.write_date,
        b.name AS location_name,
        b.complete_name AS location_complete_name,
        b.usage,
        spl.name AS serial_number,
        pc.name AS category_name,
        t.name AS product_name,
        t.product_challan,
        sw.name AS warehouse_name,
        row_number() OVER (PARTITION BY spl.name ORDER BY a.write_date DESC) AS rn
    FROM
        stock_quant a
        JOIN stock_location b ON a.location_id = b.id
        JOIN stock_production_lot spl ON a.lot_id = spl.id
        JOIN product_product p ON a.product_id = p.id
        JOIN product_template t ON p.product_tmpl_id = t.id
        JOIN product_category pc ON pc.id = t.categ_id
        LEFT JOIN stock_warehouse sw ON sw.id = b.wr_id
    WHERE
        a.lot_id IS NOT NULL
),
all_quant_final_location AS (
    SELECT
        *
    FROM
        all_quant_location
    WHERE
        rn = 1
)
SELECT
    pssy.parent_id,
    pssy.partner_id,
    pssy.location_id,
    pssy.location_dest_id AS party_location_id,
    pssy.product_id AS party_product_id,
    pssy.serial_id AS party_lot_id,
    pssy.company_name,
    pssy.serial_number,
    pssy.from_location,
    pssy.to_location,
    pssy.product_qty,
    pssy.balance_qty,
    qfl.location_id AS quant_location_id,
    qfl.product_id AS quant_product_id,
    qfl.lot_id AS quant_lot_id
FROM
    party_serial_stock_summary pssy
    LEFT JOIN all_quant_final_location qfl ON pssy.serial_number = qfl.serial_number
WHERE
    pssy.location_dest_id <> qfl.location_id
    OR pssy.product_id <> qfl.product_id
ORDER BY
    pssy.company_name,
    pssy.serial_number;

