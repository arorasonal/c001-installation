select am.id , am.ref, am.journal_id , aj.name as journal_name , am.date , am.name ,  aml.partner_id, rpa.name as customer, 
aml.debit, aml.account_id , aml.invoice_id , ai.number as invoice_number  from account_move am 
join account_move_line aml on am.id = aml.move_id 
left join res_partner rp on rp.id = am.partner_id 
left join res_partner rpa on rpa.id = aml.partner_id 
left join account_invoice ai on ai.id = aml.invoice_id
join account_journal aj on aj.id = am.journal_id
where am.date > '2019-11-30' and am.state = 'posted' 
and aml.debit > 0 


def _credit_debit_get(self):
        tables, where_clause, where_params = self.env['account.move.line'].with_context(company_id=self.env.user.company_id.id)._query_get()
        where_params = [tuple(self.ids)] + where_params
        if where_clause:
            where_clause = 'AND ' + where_clause
        self._cr.execute("""SELECT account_move_line.partner_id, act.type, SUM(account_move_line.amount_residual)
                      FROM account_move_line
                      LEFT JOIN account_account a ON (account_move_line.account_id=a.id)
                      LEFT JOIN account_account_type act ON (a.user_type_id=act.id)
                      WHERE act.type IN ('receivable','payable')
                      AND account_move_line.partner_id IN %s
                      AND account_move_line.reconciled IS FALSE
                      """ + where_clause + """
                      GROUP BY account_move_line.partner_id, act.type
                      """, where_params)
        for pid, type, val in self._cr.fetchall():
            partner = self.browse(pid)
            if type == 'receivable':
                partner.credit = val
            elif type == 'payable':
                partner.debit = -val