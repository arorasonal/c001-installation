WITH yy
AS (
	SELECT pp.id,
		pp.product_tmpl_id,
		pt.active
	FROM product_product pp
	JOIN product_template pt ON pp.product_tmpl_id = pt.id
	)
SELECT *
FROM "jasper"."cjpl_js_party_non_serial_stock_summary"
WHERE location_dest_id = 367
	AND balance_qty > 0
	AND product_id IN (
		SELECT id
		FROM yy
		WHERE active
		)