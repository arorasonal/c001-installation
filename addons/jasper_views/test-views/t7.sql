select a.id , a.origin, a.product_id , a.location_id , b.name as from_loc , a.location_dest_id , c.name as to_loc , d.serial_no , e.name , e.product_id 
 from stock_move a 
 join stock_location b on a.location_id = b.id
 join stock_location c on a.location_dest_id = c.id
 join item_alteration d on d.name = a.origin
 join stock_production_lot e on e.id = d.serial_no 
 where a.origin in ('ALT-046424','ALT-046425') 
 order by a.id


select id , create_date , lot_id , location_id , write_date , product_id  
from stock_quant 
where lot_id in (select id from stock_production_lot where name = 'CJLPE10460FJ')
order by write_date desc