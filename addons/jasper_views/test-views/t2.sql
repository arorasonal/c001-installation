WITH product_seq_component_details AS (
    SELECT
        pt.id AS pt_id,
        pt.add_product_id,
        pc.can_altered,
        ctl.seq_no,
        pta.name AS component
    FROM
        product_template pt
        JOIN product_category pc ON pt.categ_id = pc.id
        JOIN wiz_add_product wap ON wap.id = pt.add_product_id
        JOIN category_template_line ctl ON ctl.wiz_add_product_id = wap.id
        JOIN product_product pp ON pp.id = ctl.product_id
        JOIN product_template pta ON pp.product_tmpl_id = pta.id
    WHERE
        pc.can_altered = TRUE
),
product_seq_component AS (
    SELECT
        pt_id,
        CASE WHEN seq_no = 10 THEN
            component
        ELSE
            NULL
        END AS seq_010,
        CASE WHEN seq_no = 20 THEN
            component
        ELSE
            NULL
        END AS seq_020,
        CASE WHEN seq_no = 30 THEN
            component
        ELSE
            NULL
        END AS seq_030,
        CASE WHEN seq_no = 40 THEN
            component
        ELSE
            NULL
        END AS seq_040,
        CASE WHEN seq_no = 50 THEN
            component
        ELSE
            NULL
        END AS seq_050,
        CASE WHEN seq_no = 60 THEN
            component
        ELSE
            NULL
        END AS seq_060,
        CASE WHEN seq_no = 70 THEN
            component
        ELSE
            NULL
        END AS seq_070,
        CASE WHEN seq_no = 80 THEN
            component
        ELSE
            NULL
        END AS seq_080,
        CASE WHEN seq_no = 90 THEN
            component
        ELSE
            NULL
        END AS seq_090,
        CASE WHEN seq_no = 100 THEN
            component
        ELSE
            NULL
        END AS seq_100,
        CASE WHEN seq_no = 110 THEN
            component
        ELSE
            NULL
        END AS seq_110,
        CASE WHEN seq_no = 120 THEN
            component
        ELSE
            NULL
        END AS seq_120,
        CASE WHEN seq_no = 130 THEN
            component
        ELSE
            NULL
        END AS seq_130,
        CASE WHEN seq_no = 140 THEN
            component
        ELSE
            NULL
        END AS seq_140,
        CASE WHEN seq_no = 150 THEN
            component
        ELSE
            NULL
        END AS seq_150
    FROM
        product_seq_component_details
),
product_with_component AS (
    SELECT
        pt_id,
        coalesce(string_agg(seq_010, ', '), '') AS seq_010,
        coalesce(string_agg(seq_020, ', '), '') AS seq_020,
        coalesce(string_agg(seq_030, ', '), '') AS seq_030,
        coalesce(string_agg(seq_040, ', '), '') AS seq_040,
        coalesce(string_agg(seq_050, ', '), '') AS seq_050,
        coalesce(string_agg(seq_060, ', '), '') AS seq_060,
        coalesce(string_agg(seq_070, ', '), '') AS seq_070,
        coalesce(string_agg(seq_080, ', '), '') AS seq_080,
        coalesce(string_agg(seq_090, ', '), '') AS seq_090,
        coalesce(string_agg(seq_100, ', '), '') AS seq_100,
        coalesce(string_agg(seq_110, ', '), '') AS seq_110,
        coalesce(string_agg(seq_120, ', '), '') AS seq_120,
        coalesce(string_agg(seq_130, ', '), '') AS seq_130,
        coalesce(string_agg(seq_140, ', '), '') AS seq_140,
        coalesce(string_agg(seq_150, ', '), '') AS seq_150
    FROM
        product_seq_component
    GROUP BY
        pt_id
)
SELECT
    pcy.name AS category_name,
    pat.product_challan,
    pat.name AS product_name,
    pat.id,
    pwc.*
FROM
    product_with_component pwc
    JOIN product_template pat ON pwc.pt_id = pat.id
    JOIN product_category pcy ON pcy.id = pat.categ_id
ORDER BY
    pcy.name,
    pat.product_challan
