SELECT
    a.id,
    a.qty,
    a.lot_id,
    a.product_id,
    a.location_id,
    a.write_date,
    b.name AS location_name,
    b.complete_name AS location_complete_name,
    b.usage,
    spl.name AS serial_number,
    pc.name AS category_name,
    t.name AS product_name,
    t.product_challan,
    sw.name AS warehouse_name
FROM
    stock_quant a
    JOIN stock_location b ON a.location_id = b.id
    JOIN stock_production_lot spl ON a.lot_id = spl.id
    JOIN product_product p ON a.product_id = p.id
    JOIN product_template t ON p.product_tmpl_id = t.id
    JOIN product_category pc ON pc.id = t.categ_id
    LEFT JOIN stock_warehouse sw ON sw.id = b.wr_id
WHERE
    a.lot_id IS NOT NULL
ORDER BY
    spl.name,
    a.write_date DESC
