-- View: public.cjpl_js_product_with_component

DROP VIEW IF EXISTS public.cjpl_js_product_with_component;

CREATE OR REPLACE VIEW public.cjpl_js_product_with_component
 AS
 WITH product_seq_component_details AS (
         SELECT pt.id AS pt_id,
            pt.add_product_id,
            pc.can_altered,
            ctl.seq_no,
            pta.name AS component
           FROM product_template pt
             JOIN product_category pc ON pt.categ_id = pc.id
             JOIN wiz_add_product wap ON wap.id = pt.add_product_id
             JOIN category_template_line ctl ON ctl.wiz_add_product_id = wap.id
             JOIN product_product pp ON pp.id = ctl.product_id
             JOIN product_template pta ON pp.product_tmpl_id = pta.id
          WHERE pc.can_altered = true
        ), product_seq_component AS (
         SELECT product_seq_component_details.pt_id,
                CASE
                    WHEN product_seq_component_details.seq_no = 10 THEN product_seq_component_details.component
                    ELSE NULL::character varying
                END AS seq_010,
                CASE
                    WHEN product_seq_component_details.seq_no = 20 THEN product_seq_component_details.component
                    ELSE NULL::character varying
                END AS seq_020,
                CASE
                    WHEN product_seq_component_details.seq_no = 30 THEN product_seq_component_details.component
                    ELSE NULL::character varying
                END AS seq_030,
                CASE
                    WHEN product_seq_component_details.seq_no = 40 THEN product_seq_component_details.component
                    ELSE NULL::character varying
                END AS seq_040,
                CASE
                    WHEN product_seq_component_details.seq_no = 50 THEN product_seq_component_details.component
                    ELSE NULL::character varying
                END AS seq_050,
                CASE
                    WHEN product_seq_component_details.seq_no = 60 THEN product_seq_component_details.component
                    ELSE NULL::character varying
                END AS seq_060,
                CASE
                    WHEN product_seq_component_details.seq_no = 70 THEN product_seq_component_details.component
                    ELSE NULL::character varying
                END AS seq_070,
                CASE
                    WHEN product_seq_component_details.seq_no = 80 THEN product_seq_component_details.component
                    ELSE NULL::character varying
                END AS seq_080,
                CASE
                    WHEN product_seq_component_details.seq_no = 90 THEN product_seq_component_details.component
                    ELSE NULL::character varying
                END AS seq_090,
                CASE
                    WHEN product_seq_component_details.seq_no = 100 THEN product_seq_component_details.component
                    ELSE NULL::character varying
                END AS seq_100,
                CASE
                    WHEN product_seq_component_details.seq_no = 110 THEN product_seq_component_details.component
                    ELSE NULL::character varying
                END AS seq_110,
                CASE
                    WHEN product_seq_component_details.seq_no = 120 THEN product_seq_component_details.component
                    ELSE NULL::character varying
                END AS seq_120,
                CASE
                    WHEN product_seq_component_details.seq_no = 130 THEN product_seq_component_details.component
                    ELSE NULL::character varying
                END AS seq_130,
                CASE
                    WHEN product_seq_component_details.seq_no = 140 THEN product_seq_component_details.component
                    ELSE NULL::character varying
                END AS seq_140,
                CASE
                    WHEN product_seq_component_details.seq_no = 150 THEN product_seq_component_details.component
                    ELSE NULL::character varying
                END AS seq_150
           FROM product_seq_component_details
        ), product_with_component AS (
         SELECT product_seq_component.pt_id,
            COALESCE(string_agg(product_seq_component.seq_010::text, ', '::text), ''::text) AS seq_010,
            COALESCE(string_agg(product_seq_component.seq_020::text, ', '::text), ''::text) AS seq_020,
            COALESCE(string_agg(product_seq_component.seq_030::text, ', '::text), ''::text) AS seq_030,
            COALESCE(string_agg(product_seq_component.seq_040::text, ', '::text), ''::text) AS seq_040,
            COALESCE(string_agg(product_seq_component.seq_050::text, ', '::text), ''::text) AS seq_050,
            COALESCE(string_agg(product_seq_component.seq_060::text, ', '::text), ''::text) AS seq_060,
            COALESCE(string_agg(product_seq_component.seq_070::text, ', '::text), ''::text) AS seq_070,
            COALESCE(string_agg(product_seq_component.seq_080::text, ', '::text), ''::text) AS seq_080,
            COALESCE(string_agg(product_seq_component.seq_090::text, ', '::text), ''::text) AS seq_090,
            COALESCE(string_agg(product_seq_component.seq_100::text, ', '::text), ''::text) AS seq_100,
            COALESCE(string_agg(product_seq_component.seq_110::text, ', '::text), ''::text) AS seq_110,
            COALESCE(string_agg(product_seq_component.seq_120::text, ', '::text), ''::text) AS seq_120,
            COALESCE(string_agg(product_seq_component.seq_130::text, ', '::text), ''::text) AS seq_130,
            COALESCE(string_agg(product_seq_component.seq_140::text, ', '::text), ''::text) AS seq_140,
            COALESCE(string_agg(product_seq_component.seq_150::text, ', '::text), ''::text) AS seq_150
           FROM product_seq_component
          GROUP BY product_seq_component.pt_id
        )
 SELECT pcy.name AS category_name,
    pat.product_challan,
    pat.name AS product_name,
    pat.id,
    pwc.pt_id,
    pwc.seq_010,
    pwc.seq_020,
    pwc.seq_030,
    pwc.seq_040,
    pwc.seq_050,
    pwc.seq_060,
    pwc.seq_070,
    pwc.seq_080,
    pwc.seq_090,
    pwc.seq_100,
    pwc.seq_110,
    pwc.seq_120,
    pwc.seq_130,
    pwc.seq_140,
    pwc.seq_150
   FROM product_with_component pwc
     JOIN product_template pat ON pwc.pt_id = pat.id
     JOIN product_category pcy ON pcy.id = pat.categ_id
  ORDER BY pcy.name, pat.product_challan;

ALTER TABLE public.cjpl_js_product_with_component
    OWNER TO postgres;


