from datetime import datetime, timedelta
import math
import logging
import copy
import calendar
from openerp.exceptions import Warning
from openerp import tools
from odoo import api, fields, models, _
from openerp.tools.translate import _
from dateutil.relativedelta import relativedelta
from dateutil.rrule import rrule, MONTHLY, DAILY
from odoo.exceptions import UserError, AccessError, ValidationError
from openerp.tools import DEFAULT_SERVER_DATETIME_FORMAT
from dateutil.relativedelta import relativedelta
from odoo.tools import float_compare
from calendar import monthrange

_logger = logging.getLogger(__name__)


class account_fiscalyear(models.Model):
    _name = "account.fiscalyear"
    _description = "Fiscal Year"

    name = fields.Char('Fiscal Year', required=True)
    date_start = fields.Date('Start Date', required=True)
    date_stop = fields.Date('End Date', required=True)
    check_current_yr = fields.Boolean('Check Current Year', default=False)
    check_previous_yr = fields.Boolean('Check previous Year', default=False)
    check_scheduler = fields.Boolean('Check Scheduler', default=False)
    # state = fields.Selection([('draft', 'Open'), ('done', 'Closed')],
    # 'Status')

    # @api.onchange('date_start', 'date_stop')
    # def onchange_dates(self):
    #     if self.date_start >= self.date_stop:
    #         raise UserError("Warning! "
    #             "The 'Start Date' must be anterior to the 'End Date'")

    @api.multi
    def carry_forward_earned(self):
        # Date_Format = '%Y-%m-%d'
        current_date = datetime.now().date()
        DATE_FORMAT = "%Y-%m-%d"

        holiday_obj = self.env['hr.holidays']
        previous_year = current_date - relativedelta(years=1)
        year_ids = self.search(
            [
                ('date_start', '<=', previous_year),
                ('date_stop', '>=', previous_year)],
            limit=1)
        current_year_ids = self.search(
            [('date_start', '<=', current_date),
             ('date_stop', '>=', current_date)],
            limit=1)
        if year_ids:
            # if current_date >= datetime.strptime(self.date_start,
            # "%Y-%m-%d").date() and current_date <=
            # datetime.strptime(self.date_stop, "%Y-%m-%d").date():
            for emp in self.env['hr.employee'].search([]):
                earned_leave_data = []
                remaing_earned_leave = 0
                holidays_rec = holiday_obj.search([('employee_id', '=', emp.id),
                                                   ('holiday_status_id.name',
                                                    '=', 'Earned Leave'),
                                                   ('type', '=', 'remove'),
                                                   ('state', '=', 'validate')])
                if holidays_rec:
                    last_record = holidays_rec[0]
                    remaing_earned_leave = last_record.current_remaining_leave

                    if remaing_earned_leave > 60:
                        remaing_earned_leave = 60
                else:
                    if emp.leave_structure_id.active_:
                        joining = emp.date_of_joining
                        earned_count = 0
                        d1 = datetime.strptime(
                            str(current_date), DATE_FORMAT)
                        for rec in emp.leave_structure_id.structure_line_ids:
                            if rec.leave_type.name == 'Earned Leave':
                                earned_count = rec.monthly_annual
                        # if datetime.strptime(joining, DATE_FORMAT).day <= 15:
                        #     d2 = datetime.strptime(
                        #         joining, DATE_FORMAT).month
                        # else:
                        #     d2 = datetime.strptime(
                        #         joining, DATE_FORMAT).month + 1
                        # leave_diff = (d1.month - d2) + 1
                        remaing_earned_leave = earned_count
                        # count = count + 1
                earned_leave_data.append({
                    'fiscal_id': year_ids.id,
                    'carry_over': remaing_earned_leave,
                    'check_carry': True
                })
                emp.earned_leave_year = earned_leave_data
            self.check_previous_yr = True
            # else:
            #     raise UserError("Warning! "
            #                     "You can not carry forward the leaves before/after the financial year starts")

    @api.multi
    def carry_forward_scheduler(self):
        current_date = datetime.now().date()
        year_ids = self.env['account.fiscalyear'].search(
            [('date_start', '<=', current_date),
             ('date_stop', '>=', current_date)],
            limit=1)
        year_ids.carry_forward_earned()
        year_ids.check_scheduler = True

    @api.constrains('date_start', 'date_stop')
    def check_fiscal(self):
        obj = self.env['account.fiscalyear'].search([('id', '!=', self.id)])
        for record in obj:
            if self.date_start >= record.date_start and self.date_stop <= record.date_stop:
                raise UserError("Warning! "
                                "Duplicate Fiscal year can not be created for overlapping date range.")


class HrHolidaysStatus(models.Model):
    _inherit = "hr.holidays.status"
    _description = "Leave Type"

    @api.multi
    def name_get(self):
        if not self.env.context.get('employee_id', False):
            return super(HrHolidaysStatus, self).name_get()
        res = []
        for record in self:
            name = record.name
            if not record.limit:
                name = name
            res.append((record.id, name))
        return res

    leave_num = fields.Char('Leaves Number', size=64)
    monthly_accrual = fields.Boolean('Monthly Accrual')
    monthly_accrued_leaves = fields.Float('Monthly Accrued Leaves')
    code = fields.Char('Leaves Code', size=64)
    active_ = fields.Boolean('Active', default=True)


class HrHolidays(models.Model):
    _name = "hr.holidays"
    _inherit = "hr.holidays"
    _order = 'date_from desc'

    @api.constrains('number_of_days_temp', 'date_from')
    def _check_number_of_days_leave(self):
        lt = []
        val = 0
        if self.type == 'remove':
            date1 = self.date_from
            a = datetime.strptime(str(date1), '%Y-%m-%d')
            first_date = a.replace(day=1)
            date2 = calendar.monthrange(first_date.year, first_date.month)[1]
            last_date = a.replace(day=date2)
            hr_holidays_ids = self.search([
                ('employee_id', '=', self.employee_id.id),
                ('holiday_status_id', '=', 'Casual Leave'),
                ('state', 'in', ('validate', 'confirm', 'validate1')),
                ('type', '=', 'remove'),
                ('date_from', '>=', first_date),
                ('date_from', '<=', last_date)
            ])
            date_from = datetime.strptime(self.date_from, "%Y-%m-%d")
            joining_date = datetime.strptime(
                self.employee_id.date_of_joining, "%Y-%m-%d"
            )
            if self.holiday_status_id.name == 'Earned Leave':
                for dt in rrule(MONTHLY, dtstart=joining_date, until=date_from):
                    lt.append(dt)
                if len(lt) < 6 and self.new_casual_leave <= 1:
                    raise ValidationError(
                        "Earned Leave Should be taken after 6 months of Joining date"
                    )
            for rec in hr_holidays_ids:
                val += rec.number_of_days_temp
            if hr_holidays_ids:
                if self.holiday_status_id.name == 'Casual Leave':
                    if val >= 1:
                        raise ValidationError(
                            "You can not apply Casual leave more than 1 leave"
                        )
                    if val < 1 and self.full_half_day == 'full':
                        raise ValidationError(
                            "You can not apply Casual leave for full day leave if you already have a half day leave in  a Month"
                        )
                    
        if self.holiday_status_id.name == 'Sick Leave':
            if self.ir_attachment_medical.id == False:
                if self.number_of_days_temp > 3:
                    raise ValidationError(
                        "Please attached Medical certificate"
                    )
        if self.holiday_status_id.name == 'Casual Leave':
            if self.number_of_days_temp > 1:
                raise ValidationError(
                    "You can not apply Casual leave more than 1 leave"
                )

    @api.multi
    def check_cancel(self):
        ''' Check Box for Cancel Button '''
        current_date = datetime.now().date()
        check = {}
        for rec in self:
            date_from = datetime.strptime(rec.date_from, "%Y-%m-%d").date()
            if rec.state in ('confirm', 'validate'):
                if current_date < date_from:
                    check[rec.id] = True
                else:
                    check[rec.id] = False
            else:
                check[rec.id] = False
        return check

    @api.constrains('number_of_days_temp')
    def _check_holidays(self):
        if self.type == 'add' and self.number_of_days_temp <= 0:
            raise UserError("Warning! "
                            "Duration should be greater than zero")

    @api.multi
    def check_modify(self):
        ''' Check Box for Modification Button '''
        current_date = datetime.now().date()
        check_modify = {}
        for rec in self:
            date_from = datetime.strptime(rec.date_from, "%Y-%m-%d").date()
            if rec.state == 'validate':
                if current_date > date_from:
                    check_modify[rec.id] = True
                else:
                    check_modify[rec.id] = False
            else:
                check_modify[rec.id] = False
        return check_modify

    @api.multi
    def check_employee(self):
        ''' Employee Leave Checker '''
        # if not isinstance(id, int):
        #     raise ValidationError(_('This method can only except a single'
        #                             ' integer.'))
        self.ensure_one()
        user_list = [1]
        user_list.append(self.employee_id.user_id.id)
        if self.env.uid in user_list:
            return True
        return False

    @api.multi
    def _get_check(self):
        ''' Check Two fields '''
        res = {}
        for data in self:
            if data.state == 'confirm':
                if data.employee_id.user_id.id == self:
                    res[data.id] = False
                elif data.employee_id.parent_id.user_id.id == self:
                    res[data.id] = True
            else:
                res[data.id] = False
        return res

    fiscal_year = fields.Many2one('account.fiscalyear', 'Year')
    state = fields.Selection([
        ('draft', 'To Submit'),
        ('modify', 'Under Modification'),
        ('confirm', 'Awaiting Manager Approval'),
        ('validate1', 'Awaiting HOD Approval'),
        ('validate', 'Approved'),
        ('refuse', 'Refused'),
        ('reject', 'Rejected'),
        ('lapsed', 'Lapsed'),
        ('cancel', 'Cancelled'), ],
        'Status', readonly=True,
        copy=False, default='draft')
    current_user = fields.Boolean(copy=False, default=True)
    employee_manager = fields.Boolean(
        copy=False, compute="check_employee_manager")
    user_submit = fields.Boolean(copy=False, compute="check_employee_manager")
    employee_type = fields.Selection([
        ('old', 'By Calendar'),
        ('new', 'By Joining Date')],
        'Allocation Type')
    remaining_leave = fields.Float(
        "Estimated Leave Balance (On Applied Dates)",
        store=True)
    utilize_leave = fields.Float("Leaves Utilized", store=True)
    approval_note_id = fields.Text("Approval Remarks",)
    reason_note_id = fields.Text("Reason For Rejection",)
    leave_bal = fields.Float(
        "Estimated Leaves Accrued (On Applied Dates)")
    remaining_visibility = fields.Boolean("Utilized Leaves", default=False)
    duration_edit = fields.Boolean("Duration", default=False)
    check_user = fields.Boolean(string="Is User")
    matenity = fields.Boolean("Is Maternity")
    sick = fields.Boolean("Is Sick", default=False)
    short = fields.Boolean("Is Short", default=False)
    restrict = fields.Boolean("Is Restricted", default=False)
    ir_attachment_medical = fields.Many2many(
        'ir.attachment', 'rel_medical_attachment',
        'medical_id', 'attachment_medical_id', 'Medical Certificate')
    manager_id = fields.Many2one(
        relation='hr.employee', related='employee_id.parent_id', string='Manager')
    date_from = fields.Date(
        'Start Date', readonly=True,
        states={'draft': [('readonly', False)],
                'confirm': [('readonly', False)]},
        index=True, copy=False)
    date_to = fields.Date(
        'End Date', readonly=True,
        states={'draft': [('readonly', False)],
                'confirm': [('readonly', False)]},
        copy=False)
    full_half_day = fields.Selection(
        [('full', 'Full Day'),
         ('half', 'Half Day'),
         ('quarter', 'Quarter Day')], 'Day Status')
    half_day = fields.Selection([
        ('f_half', '1st Half'),
        ('s_half', '2nd Half')],
        '1st Half/2nd Half', default='f_half')
    check_half = fields.Boolean(string="half")
    current_leave_bal = fields.Float(
        "Leaves Accrued (As of Today)")
    current_utilize_leave = fields.Float(
        "Leaves Utilized (As of Today)")
    current_remaining_leave = fields.Float(
        "Leaves Balance (As of Today)")
    modify = fields.Boolean('Modify')
    off_emails = fields.Char('Emails')
    holiday_status_id = fields.Many2one("hr.holidays.status", string="Leave Type", required=True,
                                        states={'draft': [('readonly', False)], 'confirm': [('readonly', False)]})
    check_earned_year = fields.Date(
        'Check Earned Leave', default=datetime.now())
    check_earned = fields.Boolean(default=False)
    estimated_leave_bal = fields.Float()
    estimated_remainig_leave = fields.Float()
    allocated_date = fields.Date('Allocated Date')
    reference = fields.Char(string='Reference #')
    new_casual_leave = fields.Float("New Casual Leave")
    check_manager = fields.Boolean("Check Manager", compute="check_emp_manager")

    @api.multi
    def check_emp_manager(self):
        for id in self:
            if not self.check_employee():
                id.check_manager = True
            else:
                id.check_manager = False

    @api.multi
    def check_employee_manager(self):
        user = self.env.uid
        for record in self:
            if self.env.user.has_group('hr_payroll.group_hr_payroll_manager') or self.env.user.has_group('hr_payroll.group_hr_payroll_user'):
                record.employee_manager = True
            elif self.env.user.has_group('base_hr.group_hr_manager_apagen1') and (self.employee_id.parent_id.user_id.id == user):
                record.employee_manager = True
            else:
                record.employee_manager = False
            # if record.employee_id.user_id.id == user:
            #     record.user_submit = True
            # else:
            #     record.user_submit = False

    @api.onchange('employee_id')
    def onchange_employee(self):
        ''' Onchange For EMployee '''
        result = {'value': {'department_id': False}}
        lis = []
        user = self.env.uid

        emp_contract = self.env['hr.contract'].search([
            ('employee_id', '=', self.employee_id.id),
            ('status1', 'not in', ['close_contract', 'terminated'])
        ])

        if not emp_contract:
            raise UserError(
                'There is No Any contract created for this employee.')

        if not self.employee_id.employee_code:
            raise UserError(
                'You can not Apply Leave for this Employee. Employee Code is not created for this Employee.')

        for record in self:
            if self.env.user.has_group('hr_payroll.group_hr_payroll_manager') or self.env.user.has_group('hr_payroll.group_hr_payroll_user') or self.env.user.has_group('hr.group_hr_user'):
                record.current_user = True
            else:
                record.current_user = False
            # if record.employee_id.user_id.id == user:
            #     record.user_submit = True
            # else:
            #     record.user_submit = False

        # if self.employee_id.leave_structure_id.active_:
        #     for rec in self.employee_id.leave_structure_id.structure_line_ids:
        #         if rec.active_ and self.type == 'remove':
        #             lis.append(rec.leave_type.id)
        #         elif self.type == 'add' and rec.active_ and rec.leave_type.name == 'Compensatory Leave':
        #             lis.append(rec.leave_type.id)

        if self.employee_id:
            employee = self.env['hr.employee']
            result['value'] = {'department_id': self.employee_id.department_id.id,
                               'manager_id': self.employee_id.parent_id.id,
                               'holiday_status_id': '',
                               'full_half_day': '',
                               'date_from': '',
                               'date_to': '',
                               'number_of_duration': '',
                               'leave_bal': '',
                               'remaining_leave': ''
                               }
        return {'result': result}

    @api.onchange('full_half_day')
    def onchange_full_half_day(self):
        '''Onchange for Full And Half'''
        if self.full_half_day == 'quarter' and self.holiday_status_id.code not in ['short_leave', 'on_duty_leave']:
            raise UserError(
                'Quarter Day is only applicable for the "Short Leave".')
        elif self.full_half_day == 'half' and self.holiday_status_id.name == 'Compensatory Leave':
            raise UserError(
                'You can apply only Full Day, Compensatory Leave.')
        val = {}
        if self.full_half_day:
            val['date_from'] = ''
            val['date_to'] = ''
            val['number_of_duration'] = ''
            val['leave_bal'] = ''
            val['remaining_leave'] = ''
        return {'value': val}

    @api.onchange('holiday_status_id', 'employee_id')
    def onchange_holiday_status_id(self):
        ''' Onchange Holiday Status '''
        val = {}
        lis = []
        holiday_status_obj = self.env['hr.holidays.status']
        self.cal_casual_leave(self.employee_id)
        if self.employee_id.leave_structure_id.active_:
            for rec in self.employee_id.leave_structure_id.structure_line_ids:
                if rec.active_ and self.type == 'remove':
                    lis.append(rec.leave_type.id)
                elif self.type == 'add' and rec.active_ and rec.leave_type.name == 'Compensatory Leave':
                    lis.append(rec.leave_type.id)

        if self.holiday_status_id.id and self.type == 'remove':
            code = self.holiday_status_id.code
            if code == 'short_leave':
                self.short = True
            else:
                self.short = False
            if code == 'restricted_leave':
                self.restrict = True
            else:
                self.restrict = False
            apply_method_name = 'apply_{code}'.format(code=code)
            if hasattr(self, apply_method_name):
                apply_method = getattr(self, apply_method_name)
                return apply_method(self.employee_id)
            else:
                leave_type = self.holiday_status_id.name
                raise AccessError(
                    _('Apply method for Leave Type is not implemented.'))
            val['full_half_day'] = ''
            val['date_from'] = ''
            val['date_to'] = ''
            val['number_of_days_temp'] = ''
        return {'value': val, 'domain': {
                'holiday_status_id': [
                    ('id', 'in', lis)
                ]
                }}

    @api.multi
    def get_holidays_list(self, date_from, date_to,
                          employee_id):
        '''Sunday and holiday list'''
        holiday_count = 0
        DATE_FORMAT = "%Y-%m-%d"
        from_date = datetime.strptime(date_from, DATE_FORMAT)
        to_date = datetime.strptime(date_to, DATE_FORMAT)
        interval = to_date - from_date
        dt_range = []
        employee = self.env['hr.employee']
        hr_holiday_obj = self.env['hr.holidays.calendar']
        fiscal_year = self.env['account.fiscalyear']
        partner_obj = employee.browse(employee_id)
        [dt_range.append((from_date + timedelta(days=x)).strftime(DATE_FORMAT)
                         ) for x in range(int(interval.days + 1))]
        for dt in dt_range:
            year_id = fiscal_year.search([
                ('date_start', '<=', date_from),
                ('date_stop', '>=', date_to)],
                limit=1)
            if year_id:
                holiday_id = hr_holiday_obj.search(
                    [('fiscal_id', '=', year_id.id),
                     ('state_id', '=', self.employee_id.location.state_id.id)])
                if holiday_id:
                    for period in holiday_id.period_ids:
                        if period.holiday_type == 'gazetted':
                            pfrom = datetime.strptime(
                                period.date_start, DATE_FORMAT)
                            pto = datetime.strptime(
                                period.date_end, DATE_FORMAT)
                            holiday_period = pto - pfrom
                            for x in range(int(holiday_period.days + 1)):
                                holiday_date = (
                                    pfrom + timedelta(days=x)).strftime(
                                    DATE_FORMAT)
                                if dt == holiday_date:
                                    holiday_count += 1
                else:
                    raise UserError(
                        'There is no Holiday Calendar defined for your state, Please contact your Admin')
        return holiday_count

    def _get_number_of_days(self, date_from, date_to, employee_id):
        """ Returns a float equals to the timedelta between two dates given as string."""
        from_dt = fields.Datetime.from_string(date_from)
        to_dt = fields.Datetime.from_string(date_to)
        week_off_day = 0
        resource_day = []
        resource_calendar = self.env['hr.contract'].search([
            ('employee_id', '=', self.employee_id.id),
            ('status1', 'not in', ['close_contract', 'terminated'])
        ])
        if resource_calendar.working_hours:
            for calendar in resource_calendar.working_hours.attendance_ids:
                resource_day.append(int(calendar.dayofweek))
            for dt in rrule(DAILY, dtstart=from_dt, until=to_dt):
                if dt.weekday() not in resource_day:
                    continue
                else:
                    week_off_day += 1
        if week_off_day > 0:
            holiday_count = self.get_holidays_list(
                date_from, date_to, employee_id)
            second_sat = self.calculate_sat_holiday(date_from, date_to)
            week_off_day = week_off_day - holiday_count - second_sat
        return week_off_day

    @api.onchange('date_from')
    def onchange_date_from_main(self):
        res = self.onchange_date_from()
        return res

    def onchange_date_from(self):
        """
        If there are no date set for date_to, automatically set one 8
        hours later than the date_from.
        Also update the number_of_days.
        """
        # date_to has to be greater than date_from
        DATE_FORMAT = "%Y-%m-%d"
        result = {'value': {}}
        if self.date_from:
            current_date = datetime.now().date()
            start_date = datetime.strptime(
                str(self.date_from), '%Y-%m-%d').month
            end_date = datetime.strptime(
                str(current_date), '%Y-%m-%d').month
            if(self.holiday_status_id.code == 'on_duty_leave' 
                or self.holiday_status_id.code == 'short_leave' or self.holiday_status_id.code == 'restricted_leave'
                or self.holiday_status_id.code == 'compensatory_leave'):
                # <commented by omendra>
                # if start_date > end_date and self.current_remaining_leave <= 0:
                if start_date > end_date:
                    self.date_from = False
                    return {
                        'warning': {
                        'title': "Error",
                        'message': 'You can not apply a leave for future months!',
                        }
                    }
            if(self.holiday_status_id.code == 'casual_leave'):
                if start_date > end_date:
                    self.date_from = False
                    return {
                        'warning': {
                        'title': "Error",
                        'message': 'You can not apply a leave for future months!',
                        }
                    }
            if(self.holiday_status_id.code == 'casual_leave'):
                if start_date < end_date:
                    self.date_from = False
                    return {
                        'warning': {
                        'title': "Error",
                        'message': 'You can not apply a leave for past months!',
                        }
                    }
        if (self.holiday_status_id.code == 'restricted_leave' and self.date_from) and self.full_half_day == 'full':
            self.check_joining_date()
            result['value']['date_to'] = self.date_from
        elif self.full_half_day == 'full' and self.date_from:
            self.check_joining_date()
            result['value']['date_to'] = ''
        elif (self.full_half_day == 'half' or self.full_half_day == 'quarter') and self.date_from:
            result['value']['date_to'] = self.date_from
        employee = self.env["hr.employee"]
        fiscal_year = self.env['account.fiscalyear']
        holiday_status = self.env['hr.holidays.status']
        holiday_type = holiday_status.browse(self.holiday_status_id)
        end_date = self.date_from
        year_id = fiscal_year.search([
            ('date_start', '<=', end_date),
            ('date_stop', '>=', end_date)])
        year = year_id
        year_id_from = fiscal_year.search([
            ('date_start', '<=', self.date_to),
            ('date_stop', '>=', self.date_to)])
        year_from = year_id_from
        current_date = datetime.now().date()
        current_year_id = fiscal_year.search([
            ('date_start', '<=', current_date),
            ('date_stop', '>=', current_date)])
        current_year = current_year_id
        if (self.date_from):
            date_from = datetime.strptime(str(self.date_from), DATE_FORMAT)
            # if date_from.weekday() == 6:
            #     raise UserError(
            #         'It is Sunday, already a holiday. Change your Start Date.')
        code = self.holiday_status_id.code
        get_utilize_leave = 'get_utilize_{code}'.format(code=code)
        get_utilize_leave_modify = 'get_utilize_modify_{code}'.format(
            code=code)
        get_leaves = 'get_{code}'.format(code=code)
        get_duration_leave = 'get_duration_{code}'.format(code=code)
        # No date_to set so far: automatically compute one 3 months later
        if self.date_from and not self.date_to:
            self.check_joining_date()
            if self.holiday_status_id.name == "Maternity Leave":
                date_mate = datetime.strptime(
                    date_from, DATE_FORMAT).date() + timedelta(
                    hours=4344, minutes=00, seconds=00)
                result['value']['date_to'] = date_mate
            else:
                result['value']['date_to'] = str(self.date_from)
            if hasattr(self, get_leaves):
                get_leaves = getattr(self, get_leaves)
                if self.employee_id.date_of_joining:
                    join_day = datetime.strptime(
                        self.employee_id.date_of_joining, DATE_FORMAT)
                else:
                    raise UserError(_('Your Joining Date is'
                                      'not configured in the system.'
                                      'Please contact HR Ops team'))
                # Case Date of joining
                if year:
                    if (year.date_start <= self.employee_id.date_of_joining and
                            year.date_stop >= self.employee_id.date_of_joining):
                        start_date = self.employee_id.date_of_joining
                        employee_id = self.employee_id.id
                        end_date = self.date_from
                        res_get = get_leaves(self.employee_id, end_date,
                                             start_date=start_date)
                        res = 0.0
                        if join_day.day > 15:
                            if code == 'earned_leave':
                                res = res_get - 2
                        else:
                            res = res_get
                    else:
                        employee_id = self.employee_id.id
                        start_date = year.date_start
                        res = get_leaves(employee_id,
                                         end_date,
                                         start_date=start_date)
                else:
                    raise UserError(_('No fiscal year available in'
                                      ' system please contact your HR.'))
            # Utilize Method
            if hasattr(self, get_utilize_leave):
                get_utilize_leave = getattr(self, get_utilize_leave)
                end_date = self.date_from
                start_date = self.date_to
                res_utilize = get_utilize_leave(self.employee_id,
                                                end_date, start_date)
                result['value']['utilize_leave'] = res_utilize
                result['value']['leave_bal'] = res
                result['value']['remaining_leave'] = res - res_utilize
        # Compute and update the number of days
        if (self.date_to and self.date_from) and (self.date_from <= self.date_to):
            self.check_joining_date()
            end_date = self.date_to
            if self.holiday_status_id.name == "Maternity Leave":
                date_mate = datetime.strptime(
                    self.date_from, DATE_FORMAT).date() + timedelta(
                    hours=4344, minutes=00, seconds=00)
                result['value']['date_to'] = date_mate
            if hasattr(self, get_leaves):
                if year_from.name != year.name:
                    result['value']['leave_bal'] = ''
                    result['value']['utilize_leave'] = ''
                    raise UserError(_(
                        'Warning!' 'You select different'
                        ' Fiscal Year Date!'))
                get_leaves = getattr(self, get_leaves)
                if self.employee_id.date_of_joining:
                    join_day = datetime.strptime(
                        self.employee_id.date_of_joining, DATE_FORMAT)
                else:
                    raise UserError(_('Your Joining Date is'
                                      'not configured in the system.'
                                      'Please contact HR Ops team'))
                # Case Date of joining
                if year:
                    if (year.date_start <= self.employee_id.date_of_joining and
                            year.date_stop >= self.employee_id.date_of_joining):
                        start_date = self.employee_id.date_of_joining
                        employee_id = self.employee_id.id
                        res_get = get_leaves(self.employee_id, end_date,
                                             start_date=start_date)
                        res = 0.0
                        if join_day.day > 15:
                            if code == 'earned_leave':
                                res = res_get - 2
                                result['value']['leave_bal'] = res
                            else:
                                result['value']['leave_bal'] = res_get
                                res = res_get
                                self.leave_bal = res
                    else:
                        start_date = year.date_start
                        res = get_leaves(self.employee_id,
                                         end_date,
                                         start_date=start_date)
                        result['value']['leave_bal'] = res
                else:
                    raise UserError(_('No fiscal year available in'
                                      ' system please contact your HR.'))
            # Utilize Method
            if hasattr(self, get_utilize_leave):
                get_utilize_leave = getattr(self, get_utilize_leave)
                end_date = datetime.strptime(
                    self.date_from, '%Y-%m-%d').replace(day=1)
                start_date = self.date_to
                res_utilize = get_utilize_leave(self.employee_id,
                                                end_date, start_date)
                result['value']['utilize_leave'] = res_utilize
                self.utilize_leave = res_utilize
                result['value']['remaining_leave'] = res - res_utilize
                self.remaining_leave = res - res_utilize
            if hasattr(self, get_duration_leave):
                get_duration_leave = getattr(self, get_duration_leave)
                full_half_day = self.full_half_day
                date_to = self.date_to
                date_from = self.date_from
                employee_id = self.employee_id.id
                duration = get_duration_leave(employee_id, date_from, date_to,
                                              full_half_day)
                result['value']['number_of_days_temp'] = duration
            else:
                date_to = datetime.strptime(self.date_to, DATE_FORMAT)
                date_from = datetime.strptime(self.date_from, DATE_FORMAT)
                days_diff = (date_to - date_from)
                no_of_days_diff = days_diff.days + 1
                result['value']['number_of_days_temp'] = no_of_days_diff
            if self.holiday_status_id.name == 'Sick Leave' and result['value']['number_of_days_temp'] >= 2:
                self.sick = True
            else:
                self.sick = False
        else:
            result['value']['number_of_days_temp'] = 0
        self.estimated_leave_bal = self.leave_bal
        self.estimated_remainig_leave = self.remaining_leave
        return result

    @api.onchange('date_to', 'date_from')
    def onchange_date_to_main(self):
        res = self.onchange_date_to()
        return res

    def onchange_date_to(self):
        """
        Update the number_of_days. onchange for date to
        """
        DATE_FORMAT = "%Y-%m-%d"
        employee = self.env["hr.employee"]
        holiday_status = self.env['hr.holidays.status']
        holiday_type = holiday_status.browse(holiday_status)
        fiscal_year = self.env['account.fiscalyear']
        end_date = self.date_to
        date_from = self.date_from
        year_id_to = fiscal_year.search([
            ('date_start', '<=', end_date),
            ('date_stop', '>=', end_date)])
        year = year_id_to
        year_id_from = fiscal_year.search([
            ('date_start', '<=', date_from),
            ('date_stop', '>=', date_from)])
        year_from = year_id_from
        current_date = datetime.now().date()
        current_year_id = fiscal_year.search([
            ('date_start', '<=', current_date),
            ('date_stop', '>=', current_date)])
        current_year = current_year_id
        # date_to has to be greater than date_from
        result = {'value': {}}
        # Compute and update the number of days
        if (self.date_to and self.date_from) and (self.date_from <= self.date_to):
            self.check_joining_date()
            code = self.holiday_status_id.code
            get_leaves = 'get_{code}'.format(code=code)
            get_duration_leave = 'get_duration_{code}'.format(code=code)
            get_utilize_leave = 'get_utilize_{code}'.format(code=code)
            get_utilize_leave_modify = 'get_utilize_modify_{code}'.format(
                code=code)
            if hasattr(self, get_leaves):
                if year_from.name != year.name:
                    result['value']['leave_bal'] = ''
                    result['value']['utilize_leave'] = ''
                    raise UserError(_(
                        'You select different'
                        ' Fiscal Year Date!'))
                get_leaves = getattr(self, get_leaves)
                if self.employee_id.date_of_joining:
                    join_day = datetime.strptime(
                        self.employee_id.date_of_joining, DATE_FORMAT)
                else:
                    raise UserError(_('Your Joining Date is '
                                      'not configured in the system.'
                                      'Please contact HR Ops team'))
                # Case Date of joiningresult['value']['utilize_leave'] = ''
                if year:
                    if (year.date_start <= self.employee_id.date_of_joining and
                            year.date_stop >= self.employee_id.date_of_joining):
                        start_date = self.employee_id.date_of_joining
                        end_date = self.date_to
                        employee_id = self.employee_id.id
                        res_get = get_leaves(self.employee_id, end_date,
                                             start_date=start_date)
                        res = 0.0
                        if join_day.day > 15:
                            if code == 'earned_leave':
                                res = res_get - 2
                                result['value']['leave_bal'] = res
                                self.leave_bal = res
                            else:
                                res = res_get
                                result['value']['leave_bal'] = res
                                self.leave_bal = res
                        else:
                            res = res_get
                            result['value']['leave_bal'] = res
                            self.leave_bal = res
                    else:
                        start_date = self.employee_id.date_of_joining
                        end_date = self.date_to
                        employee_id = self.employee_id
                        start_date = year.date_start
                        res = get_leaves(employee_id,
                                         end_date,
                                         start_date=start_date)
                        result['value']['leave_bal'] = res
                        self.leave_bal = res
                else:
                    raise UserError(_('No fiscal year available in'
                                      ' system please contact your HR.'))
            # Utilize Method
            if hasattr(self, get_utilize_leave):
                get_utilize_leave = getattr(self, get_utilize_leave)
                end_date = self.date_from
                start_date = self.date_to
                employee_id = self.employee_id
                res_utilize = get_utilize_leave(employee_id,
                                                end_date, start_date)
                result['value']['utilize_leave'] = res_utilize
                self.utilize_leave = res_utilize
                result['value']['remaining_leave'] = res - res_utilize
                self.remaining_leave = res - res_utilize

            if hasattr(self, get_duration_leave):
                get_duration_leave = getattr(self, get_duration_leave)
                employee_id = self.employee_id.id
                date_from = self.date_from
                date_to = self.date_to
                full_half_day = self.full_half_day
                duration = get_duration_leave(employee_id, date_from, date_to,
                                              full_half_day)
                result['value']['number_of_days_temp'] = duration
                self.number_of_days_temp = duration
            else:
                from_dt = datetime.strptime(self.date_from, DATE_FORMAT)
                to_dt = datetime.strptime(self.date_to, DATE_FORMAT)
                days_diff = (to_dt - from_dt)
                no_of_days_diff = days_diff.days + 1
                result['value']['number_of_days_temp'] = no_of_days_diff
                self.number_of_days_temp = no_of_days_diff
            if self.holiday_status_id.name == 'Sick Leave' and self.number_of_days_temp >= 2:
                self.sick = True
            else:
                self.sick = False
        else:
            result['value']['number_of_days_temp'] = 0
            self.number_of_days_temp = 0
        self.estimated_leave_bal = self.leave_bal
        self.estimated_remainig_leave = self.remaining_leave
        return result

    @api.multi
    def check_joining_date(self):
        if self.employee_id.date_of_joining:
            if self.employee_id.date_of_joining > self.date_from:
                raise UserError(
                    _("Warning" "The leave can be requested only after Joining Date"))
            else:
                return True
        else:
            raise UserError(_('Your Joining Date is'
                              ' not configured in the system.'
                              ' Please contact HR Ops team'))
        return True

    @api.multi
    def check_leave_overlap(self):
        """ Leave request not a same leave type """
        for holiday in self:
            domain = [
                ('employee_id', '=', holiday.employee_id.id),
                # ('id', '!=', self._origin.id),
                ('state', '=', 'draft'),
                ('holiday_status_id', '=', holiday.holiday_status_id.id),
                ('type', '=', holiday.type)
            ]
            holidays = self.search_count(domain)
            if holidays > 1:
                raise UserError("You cannot have 2 leave requests"
                                " applied for the same leave type on 'To Submit' state."
                                " Please cancel the previous leave request in 'To Submit'"
                                " for this leave type before creating a new request"
                                " you can also 'Cancel' this request and modify the"
                                " previous request")
            else:
                return True
        return True

    @api.multi
    def check_saturday_holiday(self,
                               start_date=None,
                               end_date=None):
        """ Raise warning on 2nd and fourth saturday between date_from and date_to"""
        list_user = []
        DATE_FORMAT = "%Y-%m-%d"
        start_date = datetime.strptime(str(start_date), DATE_FORMAT)
        end_date = datetime.strptime(str(end_date), DATE_FORMAT)
        users = self.env['res.users'].search([])
        for user in users:
            if user.has_group('hr.group_hr_user'):
                user1 = user
                list_user.append(user1.id)
        if start_date == end_date:
            if start_date.weekday() == 5:
                if ((start_date.day - 1) // 7 == 1 and self.employee_id.user_id.id not in list_user):
                    raise UserError("It's already a holiday")
            elif (start_date.weekday() == 6 and self.employee_id.user_id.id not in list_user):
                raise UserError("It's already a holiday")
        return False

    @api.multi
    def calculate_sat_holiday(self, date_from, date_to):
        """ Calculate 2nd and fourth saturday between date_from and date_to"""
        DATE_FORMAT = "%Y-%m-%d"
        count = 0
        start_date = datetime.strptime(str(date_from), DATE_FORMAT)
        end_date = datetime.strptime(str(date_to), DATE_FORMAT)
        for dt in rrule(DAILY, dtstart=start_date, until=end_date):
            if dt.weekday() == 5:
                if (dt.day - 1) // 7 == 1:
                    count += 1
        return count

    @api.multi
    def check_date_overlap(self):
        """ Leave request not a same day """
        for holiday in self:
            domain = [
                ('date_from', '<=', holiday.date_to),
                ('date_to', '>=', holiday.date_from),
                ('employee_id', '=', holiday.employee_id.id),
                # ('id', '!=', self._origin.id),
                ('state', 'not in', ['cancel', 'refuse']),
                ('type', '=', holiday.type),
            ]
            holidays = self.search_count(domain)
            if holidays > 1:
                raise UserError('You cannot have 2 leave requests'
                                ' applied for the same day.Please check your'
                                ' existing leave requests.')
            else:
                return True
        return True

    @api.multi
    def check_gazetted_holiday(self, employee_id, to_date, from_date):
        ''' Check Gazetted Holiday'''
        calendar = self.env['hr.holidays.calendar']
        period_obj = self.env['holiday.calendar.period']
        fiscal_year = self.env['account.fiscalyear']
        DATE_FORMAT = "%Y-%m-%d"
        start_date = datetime.strptime(str(to_date), DATE_FORMAT)
        end_date = datetime.strptime(str(from_date), DATE_FORMAT)
        interval = end_date - start_date
        # service_area = employee_id.office_location.id
        if interval.days <= 0:
            # dt_range = []
            # [dt_range.append(
            #     (start_date + timedelta(days=x)).strftime(DATE_FORMAT))
            #  for x in range(int(interval.days + 1))]
            # for dt in dt_range:
            year_id = fiscal_year.search(
                [('date_start', '<=', start_date),
                 ('date_stop', '>=', end_date)],
                limit=1)
            holiday_id = calendar.search([
                ('fiscal_id', '=', year_id.id),
                ('state_id', '=', employee_id.location.state_id.id),
            ], limit=1)
            if holiday_id:
                for period in holiday_id.period_ids:
                    holiday_period_ids = period_obj.search(
                        [
                            ('date_start', '=', start_date),
                            ('holiday_id', '=', holiday_id.id),
                            ('holiday_type', '=', 'gazetted')])
                    if holiday_period_ids:
                        raise ValidationError(
                            'The Dates selected for leave '
                            'Its already a Gazetted Holiday.'
                            'You cannot apply leaves on a gazetted'
                            ' holiday. Kindly change dates or'
                            ' cancel the request.')


    @api.multi
    def check_restricted_holiday(self, employee_id, to_date, from_date):
        ''' Check Gazetted Holiday'''
        calendar = self.env['hr.holidays.calendar']
        period_obj = self.env['holiday.calendar.period']
        fiscal_year = self.env['account.fiscalyear']
        DATE_FORMAT = "%Y-%m-%d"
        start_date = datetime.strptime(str(to_date), DATE_FORMAT)
        end_date = datetime.strptime(str(from_date), DATE_FORMAT)
        interval = end_date - start_date
        # service_area = employee_id.office_location.id
        if interval.days <= 0:
            # dt_range = []
            # [dt_range.append(
            #     (start_date + timedelta(days=x)).strftime(DATE_FORMAT))
            #  for x in range(int(interval.days + 1))]
            # for dt in dt_range:
            year_id = fiscal_year.search(
                [('date_start', '<=', start_date),
                 ('date_stop', '>=', end_date)],
                limit=1)
            holiday_id = calendar.search([
                ('fiscal_id', '=', year_id.id),
                ('state_id', '=', employee_id.location.state_id.id),
            ], limit=1)
            if holiday_id:
                for period in holiday_id.period_ids:
                    holiday_period_ids = period_obj.search(
                        [
                            ('date_start', '=', start_date),
                            ('holiday_id', '=', holiday_id.id),
                            ('holiday_type', '=', 'restricted')])
                    if not holiday_period_ids:
                        raise ValidationError(
                            'The Dates selected for leave is'
                            'not a Restricted Holiday.'
                            ' Kindly change dates or'
                            ' cancel the request.')

    @api.multi
    def check_duration(self):
        '''Check Duration '''
        for holiday in self:
            total = holiday.number_of_days_temp + holiday.utilize_leave
            if total > holiday.leave_bal:
                raise UserError('Your leave duration exceeds'
                                ' the maximum possible number of leaves'
                                ' allowed. Please alter your Start Date/End Date')
        return True
    @api.multi
    def get_duration_on_duty_leave(self, employee_id, date_from,
                                   date_to, full_half_day):
        '''Number of days calculate for onduty Leave'''
        if not full_half_day:
            raise UserError(
                'Please Select Full and Half Day.')
        if full_half_day == 'half':
            diff_day = self._get_number_of_days(
                date_from, date_to, employee_id)
            if round(math.floor(diff_day)) == 0.0:
                number_of_days_temp = round(math.floor(diff_day))
            else:
                number_of_days_temp = round(math.floor(diff_day)) - 0.5
        elif full_half_day == 'full':
            diff_day = self._get_number_of_days(
                date_from, date_to, employee_id)
            number_of_days_temp = round(math.floor(diff_day))
        elif full_half_day == 'quarter':
            diff_day = self._get_number_of_days(
                date_from, date_to, employee_id)
            if round(math.floor(diff_day)) == 0.0:
                number_of_days_temp = round(math.floor(diff_day))
            else:
                number_of_days_temp = round(math.floor(diff_day)) - 0.75
        return number_of_days_temp

    @api.multi
    def apply_on_duty_leave(self, employee_id):
        ''' Apply On Duty '''
        val = {}
        val['full_half_day'] = ''
        val['date_from'] = ''
        val['date_to'] = ''
        val['number_of_duration'] = ''
        val['leave_bal'] = ''
        val['utilize_leave'] = ''
        val['remaining_leave'] = ''
        val['current_leave_bal'] = ''
        val['current_utilize_leave'] = ''
        val['current_remaining_leave'] = ''
        val['remaining_visibility'] = False
        val['matenity'] = False
        val['duration_edit'] = True
        val['full_half_day'] = ''
        return {'value': val}

    @api.multi
    def calculate_on_duty_leave(self):
        ''' Calculate On duty '''
        hr_holidays = self
        # self.check_joining_date()
        self.check_gazetted_holiday(hr_holidays.employee_id, hr_holidays.date_to,
                                    hr_holidays.date_from)
        # self.check_saturday_holiday(hr_holidays.date_from,
        # hr_holidays.date_to)
        self.check_date_overlap()
        self.check_leave_overlap()
        return True

    @api.multi
    def get_duration_leave_wo_pay(self, employee_id, date_from,
                                  date_to, full_half_day):
        ''' Number of days calculate for Without pay Leave '''
        if not full_half_day:
            raise UserError(
                'Please Select Full and Half Day.')
        if full_half_day == 'half':
            diff_day = self._get_number_of_days(
                date_from, date_to, employee_id)
            if round(math.floor(diff_day)) == 0.0:
                number_of_days_temp = round(math.floor(diff_day))
            else:
                number_of_days_temp = round(math.floor(diff_day)) - 0.5
        elif full_half_day == 'full':
            diff_day = self._get_number_of_days(
                date_from, date_to, employee_id)
            number_of_days_temp = round(math.floor(diff_day))
        return number_of_days_temp

    @api.multi
    def apply_leave_wo_pay(self, employee_id):
        ''' Apply Leave Without Pay leave '''
        val = {}
        val['full_half_day'] = ''
        val['date_from'] = ''
        val['date_to'] = ''
        val['number_of_duration'] = ''
        val['leave_bal'] = ''
        val['utilize_leave'] = ''
        val['remaining_leave'] = ''
        val['current_leave_bal'] = ''
        val['current_utilize_leave'] = ''
        val['current_remaining_leave'] = ''
        val['remaining_visibility'] = False
        val['matenity'] = False
        val['duration_edit'] = True
        val['full_half_day'] = ''
        return {'value': val}

    @api.multi
    def calculate_leave_wo_pay(self):
        ''' Leave without pay Check Leave .'''
        DATETIME_FORMAT = "%Y-%m-%d"
        hr_holidays = self
        self.check_gazetted_holiday(
            hr_holidays.employee_id, hr_holidays.date_to, hr_holidays.date_from)
        # self.check_saturday_holiday(hr_holidays.date_from,
        # hr_holidays.date_to)
        self.check_date_overlap()
        self.check_leave_overlap()
        # self.check_duration()
        return True

    @api.multi
    def apply_maternity_leave(self, employee_id):
        ''' Onchange Maternity Leave Check Leave. '''
        val = {}
        if self.employee_id.gender == "male":
            raise UserError(
                _('Warning! Maternity leave can only be applied by female employees.'))
        val['full_half_day'] = ''
        val['date_from'] = ''
        val['date_to'] = ''
        val['number_of_duration'] = ''
        val['leave_bal'] = ''
        val['utilize_leave'] = ''
        val['remaining_leave'] = ''
        val['remaining_visibility'] = False
        val['matenity'] = True
        val['duration_edit'] = False
        val['full_half_day'] = ''
        return {'value': val}

    @api.multi
    def calculate_maternity_leave(self):
        ''' Maternity Leave Check Leave '''
        DATE_FORMAT = "%Y-%m-%d"
        # hr_holidays = self.browse()
        start_date = datetime.strptime(str(self.date_from), DATE_FORMAT)
        record_ids = self.search_read(
            [('type', '=', 'remove'),
             ('holiday_status_id', '=', 'Maternity Leave'),
             ('employee_id', '=', self.employee_id.id)],
            ['date_to', 'date_from'])
        if len(record_ids) > 2:
            raise UserError(
                'Maternity Leave can only be applied two times by'
                ' an Employee.'
            )
        date = record_ids[0]['date_from']
        end_date = datetime.strptime(date, DATE_FORMAT)
        diffrence = relativedelta(start_date, end_date)
        year = int(diffrence.years)
        if len(record_ids) > 1:
            if year < 1:
                raise UserError(
                    'Maternity Leave can only be applied once in a '
                    ' financial year. You have already applied for this financial year.')
        return True

    @api.multi
    def get_duration_earned_leave(self, employee_id,
                                  date_from, date_to, full_half_day):
        ''' Number of days calculate for Earned Leave '''
        if not full_half_day:
            raise UserError(
                'Please Select Full and Half Day.')
        if full_half_day == 'half':
            diff_day = self._get_number_of_days(
                date_from, date_to, employee_id)
            if round(math.floor(diff_day)) == 0.0:
                number_of_days_temp = round(math.floor(diff_day))
            else:
                number_of_days_temp = round(math.floor(diff_day)) - 0.5
        elif full_half_day == 'full':
            diff_day = self._get_number_of_days(
                date_from, date_to, employee_id)
            number_of_days_temp = round(math.floor(diff_day))
        return number_of_days_temp

    @api.multi
    def get_duration_casual_leave(self, employee_id,
                                  date_from, date_to, full_half_day):
        ''' Number of days calculate for Casual Leave '''
        if not full_half_day:
            raise UserError(
                'Please Select Full and Half Day.')
        if full_half_day == 'half':
            diff_day = self._get_number_of_days(
                date_from, date_to, employee_id)
            if round(math.floor(diff_day)) == 0.0:
                number_of_days_temp = round(math.floor(diff_day))
            else:
                number_of_days_temp = round(math.floor(diff_day)) - 0.5
        elif full_half_day == 'full':
            diff_day = self._get_number_of_days(
                date_from, date_to, employee_id)
            number_of_days_temp = round(math.floor(diff_day))
        return number_of_days_temp

    @api.multi
    def get_duration_sick_leave(self, employee_id, date_from,
                                date_to, full_half_day):
        ''' Number of days calculate for Sick Leave '''
        if not full_half_day:
            raise UserError(
                'Please Select Full and Half Day.')
        diff_day = self._get_number_of_days(
            date_from, date_to, employee_id)
        number_of_days_temp = round(math.floor(diff_day))
        return number_of_days_temp

    @api.multi
    def apply_earned_leave(self, employee_id):
        ''' Apply Earned Leave '''
        current_date = datetime.now().date()
        val = {}
        res_cur = 0
        holiday_status_obj = self.env['hr.holidays.status']
        holiday_status_id = holiday_status_obj.search(
            [('name', '=', 'Earned Leave')])
        rec_id = holiday_status_obj.browse(holiday_status_id)
        employee = self.env["hr.employee"].browse(employee_id)
        fiscal_year = self.env['account.fiscalyear']
        year_id = fiscal_year.search([
            ('date_start', '<=', current_date),
            ('date_stop', '>=', current_date)])
        year = year_id
        code = self.holiday_status_id.code
        # No date_to set so far: automatically compute one 3 months later
        get_utilize_leave = 'get_utilize_{code}'.format(code=code)
        get_leaves = 'get_{code}'.format(code=code)
        if hasattr(self, get_leaves):
            get_leaves = getattr(self, get_leaves)
            if self.employee_id.date_of_joining:
                join_day = datetime.strptime(
                    self.employee_id.date_of_joining, '%Y-%m-%d')
            else:
                raise ValidationError(_('Your Joining Date is'
                                        'not configured in the system.'
                                        'Please contact HR Ops team'))
            # Case Date of joining
            if year:
                end_date = str(current_date)
                if (year.date_start <= self.employee_id.date_of_joining and
                        year.date_stop >= self.employee_id.date_of_joining):
                    start_date_current = self.employee_id.date_of_joining
                    res_cur = get_leaves(employee_id,
                                         end_date,
                                         start_date=start_date_current)
                else:
                    start_date_current = year.date_start
                    res_cur = get_leaves(employee_id,
                                         end_date,
                                         start_date=start_date_current)
                val['current_leave_bal'] = res_cur
            else:
                raise ValidationError(_('No fiscal year available in'
                                        ' system please contact your HR.'))
        # Utilize Method
        if hasattr(self, get_utilize_leave):
            get_utilize_leave = getattr(self, get_utilize_leave)
            end_date_current = str(current_date)
            start_date_current = str(current_date)
            res_utilize = get_utilize_leave(employee_id,
                                            end_date_current,
                                            start_date=start_date_current)
            val['current_utilize_leave'] = res_utilize
            val['current_remaining_leave'] = res_cur - res_utilize
        val['full_half_day'] = 'full'
        val['date_from'] = ''
        val['date_to'] = ''
        val['number_of_duration'] = ''
        val['leave_bal'] = ''
        val['utilize_leave'] = ''
        val['remaining_leave'] = ''
        val['remaining_visibility'] = True
        val['matenity'] = False
        val['duration_edit'] = True
        return {'value': val}

    @api.multi
    def apply_casual_leave(self, employee_id):
        ''' Apply Casual Leave '''
        current_date = datetime.now().date()
        val = {}
        res_cur = 0
        holiday_status_obj = self.env['hr.holidays.status']
        holiday_status_id = holiday_status_obj.search(
            [('name', '=', 'Casual Leave')])
        rec_id = holiday_status_obj.browse(holiday_status_id)
        employee = self.env["hr.employee"].browse(employee_id)
        fiscal_year = self.env['account.fiscalyear']
        year_id = fiscal_year.search([
            ('date_start', '<=', current_date),
            ('date_stop', '>=', current_date)])
        year = year_id
        code = self.holiday_status_id.code
        # No date_to set so far: automatically compute one 3 months later
        get_utilize_leave = 'get_utilize_{code}'.format(code=code)
        get_leaves = 'get_{code}'.format(code=code)
        if hasattr(self, get_leaves):
            get_leaves = getattr(self, get_leaves)
            if self.employee_id.date_of_joining:
                join_day = datetime.strptime(
                    self.employee_id.date_of_joining, '%Y-%m-%d')
            else:
                raise ValidationError(_('Your Joining Date is'
                                        'not configured in the system.'
                                        'Please contact HR Ops team'))
            # Case Date of joining
            if year:
                end_date = str(current_date)
                if (year.date_start <= self.employee_id.date_of_joining and
                        year.date_stop >= self.employee_id.date_of_joining):
                    start_date_current = self.employee_id.date_of_joining
                    res_cur = get_leaves(employee_id,
                                         end_date,
                                         start_date=start_date_current)
                    # if datetime.strptime(start_date_current, '%Y-%m-%d').day <= 20 and res_cur > 0:
                    #     res_cur += 1
                else:
                    start_date_current = year.date_start
                    res_cur = get_leaves(employee_id,
                                         end_date,
                                         start_date=start_date_current)
                val['current_leave_bal'] = res_cur
            else:
                raise ValidationError(_('No fiscal year available in'
                                        ' system please contact your HR.'))
        # Utilize Method
        if hasattr(self, get_utilize_leave):
            get_utilize_leave = getattr(self, get_utilize_leave)
            end_date_current = str(current_date)
            start_date_current = str(current_date)
            res_utilize = get_utilize_leave(employee_id,
                                            end_date_current,
                                            start_date=start_date_current)
            val['current_utilize_leave'] = res_utilize
            val['current_remaining_leave'] = res_cur - res_utilize
        val['full_half_day'] = 'full'
        val['date_from'] = ''
        val['date_to'] = ''
        val['number_of_duration'] = ''
        val['leave_bal'] = ''
        val['utilize_leave'] = ''
        val['remaining_leave'] = ''
        val['remaining_visibility'] = True
        val['matenity'] = False
        val['duration_edit'] = True
        return {'value': val}

    @api.multi
    def apply_sick_leave(self, employee_id):
        ''' Apply Sick Leave '''
        current_date = datetime.now().date()
        val = {}
        res_cur = 0
        holiday_status_obj = self.env['hr.holidays.status']
        holiday_status_id = holiday_status_obj.search(
            [('name', '=', 'Sick Leave')])
        rec_id = holiday_status_obj.browse(holiday_status_id)
        employee = self.env["hr.employee"].browse(employee_id)
        fiscal_year = self.env['account.fiscalyear']
        year_id = fiscal_year.search([
            ('date_start', '<=', current_date),
            ('date_stop', '>=', current_date)])
        year = year_id
        code = self.holiday_status_id.code
        # No date_to set so far: automatically compute one 3 months later
        get_utilize_leave = 'get_utilize_{code}'.format(code=code)
        get_leaves = 'get_{code}'.format(code=code)
        if hasattr(self, get_leaves):
            get_leaves = getattr(self, get_leaves)
            if self.employee_id.date_of_joining:
                join_day = datetime.strptime(
                    self.employee_id.date_of_joining, '%Y-%m-%d')
            else:
                raise ValidationError(_('Your Joining Date is'
                                        'not configured in the system.'
                                        'Please contact HR Ops team'))
            # Case Date of joining
            if year:
                end_date = str(current_date)
                start_date_current = year.date_start
                res_cur = get_leaves(employee_id,
                                     end_date,
                                     start_date=start_date_current)
                val['current_leave_bal'] = res_cur
            else:
                raise ValidationError(_('No fiscal year available in'
                                        ' system please contact your HR.'))
        # Utilize Method
        if hasattr(self, get_utilize_leave):
            get_utilize_leave = getattr(self, get_utilize_leave)
            end_date_current = str(current_date)
            start_date_current = str(current_date)
            res_utilize = get_utilize_leave(employee_id,
                                            end_date_current,
                                            start_date=start_date_current)
            val['current_utilize_leave'] = res_utilize
            val['current_remaining_leave'] = res_cur - res_utilize
        val['full_half_day'] = 'full'
        val['date_from'] = ''
        val['date_to'] = ''
        val['number_of_duration'] = ''
        val['leave_bal'] = ''
        val['utilize_leave'] = ''
        val['remaining_leave'] = ''
        val['remaining_visibility'] = True
        val['matenity'] = False
        val['duration_edit'] = True
        return {'value': val}

    @api.multi
    def cal_casual_leave(self, employee_id):
        res_cur = 0
        res_ear = 0
        current_date = datetime.now().date()
        fiscal_year = self.env['account.fiscalyear']
        year_id = fiscal_year.search([
            ('date_start', '<=', current_date),
            ('date_stop', '>=', current_date)])
        year = year_id
        get_utilize_leave = 'get_utilize_{code}'.format(code='casual_leave')
        get_leaves = 'get_{code}'.format(code='casual_leave')
        #if hasattr(self, get_leaves):
        end_date = str(current_date)
        if year:
            if (year.date_start <= employee_id.date_of_joining and
                    year.date_stop >= employee_id.date_of_joining):
                start_date_current = employee_id.date_of_joining
                res_cur = self.get_casual_leave(employee_id, end_date, start_date=start_date_current)
                res_ear = self.get_earned_leave(employee_id, end_date, start_date=start_date_current)
            else:
                start_date_current = year.date_start
                res_cur = self.get_casual_leave(employee_id, end_date, start_date=start_date_current)
                res_ear = self.get_earned_leave(employee_id, end_date, start_date=start_date_current)
        get_utilize_leave = getattr(self, get_utilize_leave)
        end_date_current = str(current_date)
        start_date_current = str(current_date)
        res_utilize = get_utilize_leave(employee_id,
                                        end_date_current,
                                        start_date=start_date_current)
        self.new_casual_leave = res_cur - res_utilize

    @api.multi
    def get_earned_leave(self, employee_id, end_date, start_date):
        list_att = []
        list_att1 = []
        list1=[]
        list2 = []
        list3 = []
        list_in = []
        DATE_FORMAT = "%Y-%m-%d"
        resource_day = []
        earned_count = 0
        leave_count = 0
        current_date = datetime.now().date()
        start_date = datetime.strptime(
            start_date, '%Y-%m-%d')
        end_date = datetime.strptime(
            end_date, '%Y-%m-%d')
        year_id = self.env['account.fiscalyear'].search([
            ('date_start', '<=', current_date),
            ('date_stop', '>=', current_date)])
        joining_date = datetime.strptime(
            self.employee_id.date_of_joining, '%Y-%m-%d')
        hr_holiday_obj = self.env['hr.holidays.calendar']
        earned_count = end_date.month - start_date.month
        for obj in self:
            contract_id = self.env['hr.contract'].search([
                    ('employee_id', '=', obj.employee_id.id),
                    ('status1', 'not in', ['close_contract', 'terminated'])
                ])
            if year_id:
                holiday_id = hr_holiday_obj.search(
                    [('fiscal_id', '=', year_id.id),
                     ('state_id', '=', obj.employee_id.location.state_id.id)])
                for holidays in holiday_id.period_ids:
                    date_start = datetime.strptime(
                        holidays.date_start, DATE_FORMAT)
                    date_to = datetime.strptime(holidays.date_end, DATE_FORMAT)
                    for dt in rrule(DAILY, dtstart=date_start, until=date_to):
                        date1 = dt.strftime("%Y-%m-%d")
                        month = date1.split('-')[1]
                        list3.append(month)
                dic_holiday = dict((x,list3.count(x)) for x in set(list3))
            if contract_id:
                resource_calendar = contract_id.working_hours
                for calendar1 in resource_calendar.attendance_ids:
                    resource_day.append(int(calendar1.dayofweek))
                for dt in rrule(DAILY, dtstart=start_date, until=end_date):
                    if dt.weekday() not in resource_day:
                        dt1 = dt.strftime("%Y-%m-%d")
                        month = dt1.split('-')[1]
                        list2.append(month)
                dic1 = dict((x,list2.count(x)) for x in set(list2))
            for key, values in dic1.items():
                if key in dic_holiday:
                    dic1.update({key: dic_holiday[key] + dic1[key]}) 
        data = self._cr.execute(
            ''' select
                employee_id,
                check_in,
                check_out
            from
                hr_attendance
            where
                employee_id = %s and
                (check_in)::date between '%s' and '%s'
                order by check_in
            ''' % (self.employee_id.id, start_date.date(), end_date.date()))
        recs = self._cr.fetchall()
        for rec_att in recs:
            month = rec_att[1].split('-')[1]
            list_att.append(month)
        dic = dict((x,list_att.count(x)) for x in set(list_att))
        for key, values in dic1.items():
            if key in dic:
                dic.update({key: dic1[key] + dic[key] + 1})
        values_days = dic.values()
        for lis_rec in values_days:
            if lis_rec >= 11:
                a = lis_rec
                list1.append(a)
        length = len(list1)
        earned_count = length
        current_date = datetime.now().date()
        date1 = current_date
        a = datetime.strptime(str(date1), '%Y-%m-%d')
        first_date = a.replace(day=1)
        date2 = calendar.monthrange(first_date.year, first_date.month)[1]
        last_date = a.replace(day=date2)
        hr_holidays_ids = self.search([
            ('employee_id', '=', self.employee_id.id),
            ('holiday_status_id', '=', 'Casual Leave'),
            ('state', 'in', ('validate', 'confirm', 'validate1')),
            ('type', '=', 'remove'),
            ('date_from', '>=', first_date),
            ('date_from', '<=', last_date)
        ])
        if self.employee_id.leave_structure_id.active_:
            for rec in self.employee_id.leave_structure_id.structure_line_ids:
                if rec.leave_type.name == 'Earned Leave' and rec.monthly_annual > 0:
                    leave_count = (rec.monthly_annual * (earned_count + 1))
        if not hr_holidays_ids:
            leave_count = leave_count + (self.new_casual_leave  - 1)
        if hr_holidays_ids:
            leave_count = leave_count + self.new_casual_leave
        if end_date.month > current_date.month:
            end_date1 = datetime.strptime(str(end_date),'%Y-%m-%d %H:%M:%S')
            delta = end_date1.date() - current_date
            for i in range(delta.days + 1):
                date_greater = current_date + timedelta(days=i)
                d1 = date_greater.strftime("%Y-%m-%d")
                month = d1.split('-')[1]
                list_att1.append(month)
            dic_inc = dict((x,list_att1.count(x)) for x in set(list_att1))
            value_month = dic_inc.keys()
            for rec in value_month:
                if int(rec) > current_date.month:
                    b = rec
                    list_in.append(b)
            length = len(list_in)
            earned_count1 = length + earned_count
            for rec in self.employee_id.leave_structure_id.structure_line_ids:
                if rec.leave_type.name == 'Earned Leave' and rec.monthly_annual > 0:
                    leave_count = (rec.monthly_annual * (earned_count1+ 1))
        if employee_id.earned_leave_year:
            for carry in employee_id.earned_leave_year[-1]:
                leave_count = carry.carry_over + leave_count
        if joining_date >= datetime.strptime(
                year_id.date_start, '%Y-%m-%d') and joining_date.day > 20 and leave_count > 0:
            leave_count -= 2
        return leave_count

    @api.multi
    def get_casual_leave(self, employee_id, end_date, start_date):
        casual_count = 0
        leave_count = 0
        list_att = []
        list_att1 = []
        list_casual= []
        list_cas_inc = []
        current_date = datetime.now().date()
        start_date = datetime.strptime(
            start_date, '%Y-%m-%d')
        # end_date = datetime.strptime(
        #     end_date, '%Y-%m-%d')
        pre_end_date = datetime.strptime(
            end_date, '%Y-%m-%d').replace(day=1)
        end_date = pre_end_date - timedelta(days=1)
        year_id = self.env['account.fiscalyear'].search([
            ('date_start', '<=', current_date),
            ('date_stop', '>=', current_date)])
        joining_date = datetime.strptime(
            self.employee_id.date_of_joining, '%Y-%m-%d')
        # casual_count = end_date - start_date
        data = self._cr.execute(
            ''' select
                employee_id,
                check_in,
                check_out
            from
                hr_attendance
            where
                employee_id = %s and
                (check_in)::date between '%s' and '%s'
                order by check_in
            ''' % (self.employee_id.id, start_date.date(), end_date.date()))
        recs = self._cr.fetchall()
        for rec_att in recs:
            month = rec_att[1].split('-')[1]
            list_att.append(month)
        dic = dict((x,list_att.count(x)) for x in set(list_att))
        values_days = dic.values()
        for lis_rec in values_days:
            a = lis_rec
            list_casual.append(a)
        length = len(list_casual)
        casual_count = length
        if self.employee_id.leave_structure_id.active_:
            for rec in self.employee_id.leave_structure_id.structure_line_ids:
                if rec.leave_type.name == 'Casual Leave' and rec.monthly_annual > 0:
                    leave_count = (rec.monthly_annual  * (casual_count + 1))
        if end_date.month > current_date.month:
            end_date1 = datetime.strptime(str(end_date),'%Y-%m-%d %H:%M:%S')
            delta = end_date1.date() - current_date
            for i in range(delta.days + 1):
                date_greater = current_date + timedelta(days=i)
                d1 = date_greater.strftime("%Y-%m-%d")
                month = d1.split('-')[1]
                list_att1.append(month)
            dic_inc = dict((x,list_att1.count(x)) for x in set(list_att1))
            value_month = dic_inc.keys()
            for rec in value_month:
                if int(rec) > current_date.month:
                    b = rec
                    list_cas_inc.append(b)
            length = len(list_cas_inc)
            casual_count1 = length + casual_count
            for rec in self.employee_id.leave_structure_id.structure_line_ids:
                if rec.leave_type.name == 'Earned Leave' and rec.monthly_annual > 0:
                    leave_count = (rec.monthly_annual * (casual_count1+ 1))
        if joining_date >= datetime.strptime(
                year_id.date_start, '%Y-%m-%d') and joining_date.day > 20 and leave_count > 0:
            leave_count -= 1

        return leave_count

    @api.multi
    def get_sick_leave(self, employee_id, end_date, start_date):
        sick_count = 0
        if self.employee_id.leave_structure_id.active_:
            for rec in self.employee_id.leave_structure_id.structure_line_ids:
                if rec.leave_type.name == 'Sick Leave':
                    sick_count = rec.monthly_annual
        return sick_count

    @api.multi
    def get_utilize_earned_leave(self, employee_id, end_date, start_date):
        ''' Count Number of Earned utilize leave '''
        total_number = 0.0
        fiscal_year = self.env['account.fiscalyear']
        year_ids = fiscal_year.search([('date_start', '<=', end_date),
                                       ('date_stop', '>=', end_date)])
        hr_holidays_ids = self.search([
            ('employee_id', '=', employee_id.id),
            ('holiday_status_id', '=', 'Earned Leave'),
            ('state', 'in', ('validate', 'confirm', 'validate1', 'lapsed')),
            ('type', '=', 'remove')
        ])
        for hr_holidays_id in hr_holidays_ids:
            if (year_ids.date_start <= hr_holidays_id.date_from and
                    year_ids.date_stop >= hr_holidays_id.date_from):
                total_number += hr_holidays_id.number_of_days_temp
        return total_number

    @api.multi
    def get_utilize_casual_leave(self, employee_id, end_date, start_date):
        ''' Count Number of Casual utilize leave '''
        total_number = 0.0
        fiscal_year = self.env['account.fiscalyear']
        year_ids = fiscal_year.search([('date_start', '<=', end_date),
                                       ('date_stop', '>=', end_date)])
        hr_holidays_ids = self.search([
            ('employee_id', '=', employee_id.id),
            ('holiday_status_id', '=', 'Casual Leave'),
            ('state', 'in', ('validate', 'confirm', 'validate1', 'lapsed')),
            ('type', '=', 'remove')
        ])
        for hr_holidays_id in hr_holidays_ids:
            if (year_ids.date_start <= hr_holidays_id.date_from and
                    year_ids.date_stop >= hr_holidays_id.date_from):
                total_number += hr_holidays_id.number_of_days_temp
        return total_number

    @api.multi
    def get_utilize_sick_leave(self, employee_id,
                               end_date, start_date):
        ''' Count Number of Earned utilize leave '''
        total_number = 0.0
        fiscal_year = self.env['account.fiscalyear']
        year_ids = fiscal_year.search([('date_start', '<=', end_date),
                                       ('date_stop', '>=', end_date)])
        hr_holidays_ids = self.search([
            ('employee_id', '=', employee_id.id),
            ('holiday_status_id', '=', 'Sick Leave'),
            ('state', 'in', ('validate', 'confirm', 'validate1')),
            ('type', '=', 'remove')
        ])
        for hr_holidays_id in hr_holidays_ids:
            if (year_ids.date_start <= hr_holidays_id.date_from and
                    year_ids.date_stop >= hr_holidays_id.date_from):
                total_number += hr_holidays_id.number_of_days_temp
        return total_number

    @api.multi
    def calculate_earned_leave(self):
        ''' Calculate Earned Leave .'''
        self.check_duration()
        self.check_gazetted_holiday(
            self.employee_id,
            self.date_to,
            self.date_from)
        self.check_date_overlap()
        self.check_leave_overlap()
        return True

    @api.multi
    def calculate_casual_leave(self):
        ''' Calculate casual Leave .'''
        self.check_duration()
        self.check_gazetted_holiday(
            self.employee_id,
            self.date_to,
            self.date_from)
        self.check_date_overlap()
        self.check_leave_overlap()
        return True

    @api.multi
    def calculate_sick_leave(self):
        ''' Calculate Sick Leave .'''
        self.check_duration()
        # self.check_saturday_holiday(self.date_from, self.date_to)
        self.check_gazetted_holiday(
            self.employee_id,
            self.date_to,
            self.date_from)
        self.check_date_overlap()
        self.check_leave_overlap()
        return True

    @api.multi
    def get_duration_compensatory_leave(self, employee_id, date_from,
                                        date_to, full_half_day):
        ''' Number of days calculate for Without pay Leave '''
        if not full_half_day:
            raise UserError(
                'Please Select Full and Half Day.')
        diff_day = self._get_number_of_days(
            date_from, date_to, employee_id)
        number_of_days_temp = round(math.floor(diff_day))
        return number_of_days_temp

    @api.multi
    def apply_compensatory_leave(self, employee_id):
        ''' Apply Sick Leave '''
        current_date = datetime.now().date()
        val = {}
        code = self.holiday_status_id.code
        # No date_to set so far: automatically compute one 3 months later
        get_utilize_leave = 'get_utilize_{code}'.format(code=code)
        get_leaves = 'get_{code}'.format(code=code)
        if hasattr(self, get_leaves):
            get_leaves = getattr(self, get_leaves)
            # Case Date of joining
            current_date = datetime.now().date()
            end_date = str(current_date)
            start_date = str(current_date)
            res_cur = get_leaves(employee_id, end_date, start_date)
            val['current_leave_bal'] = res_cur
        # Utilize Method
        if hasattr(self, get_utilize_leave):
            get_utilize_leave = getattr(self, get_utilize_leave)
            end_date_current = str(current_date)
            start_date_current = str(current_date)
            res_utilize = get_utilize_leave(employee_id,
                                            end_date_current,
                                            start_date=start_date_current)
            val['current_utilize_leave'] = res_utilize
            val['current_remaining_leave'] = res_cur - res_utilize
        val['full_half_day'] = 'full'
        val['date_from'] = ''
        val['date_to'] = ''
        val['number_of_duration'] = ''
        val['leave_bal'] = ''
        val['utilize_leave'] = ''
        val['remaining_leave'] = ''
        val['remaining_visibility'] = True
        val['matenity'] = False
        val['duration_edit'] = True
        return {'value': val}

    @api.multi
    def get_compensatory_leave(self, employee_id, end_date,
                               start_date):
        sick_count = 0
        employee = employee_id.id
        current_date = datetime.now().date()
        fiscal_year = self.env['account.fiscalyear']
        year_ids = fiscal_year.search([('date_start', '<=', current_date),
                                       ('date_stop', '>=', current_date)])
        query_leave = """
            SELECT
               sum(leave.number_of_days_temp)
            FROM
                hr_holidays leave,
                hr_holidays_status holiday_status
            WHERE
                leave.holiday_status_id = holiday_status.id AND
                holiday_status.code = 'compensatory_leave' AND
                leave.type = 'add' AND
                leave.state = 'validate' AND
                leave.employee_id = {} AND
                leave.allocated_date >= '{}' AND
                leave.allocated_date <= '{}' AND
                leave.allocated_date >=  NOW() - INTERVAL '90 days'
        """.format(employee, year_ids.date_start, year_ids.date_stop)
        self._cr.execute(query_leave)
        leave_count = self._cr.fetchall()
        sick_count = leave_count[0][0]
        if sick_count == None:
            sick_count = 0
        return sick_count

    @api.multi
    def get_utilize_compensatory_leave(self, employee_id,
                                       end_date, start_date):
        ''' Count Number of Earned utilize leave '''
        total_number = 0.0
        fiscal_year = self.env['account.fiscalyear']
        year_ids = fiscal_year.search([('date_start', '<=', end_date),
                                       ('date_stop', '>=', end_date)])
        hr_holidays_ids = self.search([('employee_id', '=', employee_id.id),
                                       ('holiday_status_id', '=',
                                        'Compensatory Leave'),
                                       ('state', 'in', ('confirm',
                                                        'validate', 'validate1')),
                                       ('type', '=', 'remove')])
        for hr_holidays_id in hr_holidays_ids:
            if (year_ids.date_start <= hr_holidays_id.date_from and
                    year_ids.date_stop >= hr_holidays_id.date_from):
                total_number += hr_holidays_id.number_of_days_temp
        return total_number

    @api.multi
    def calculate_compensatory_leave(self):
        ''' Calculate Sick Leave .'''
        self.check_duration()
        # self.check_saturday_holiday(self.date_from, self.date_to)
        self.check_gazetted_holiday(
            self.employee_id,
            self.date_to,
            self.date_from)
        self.check_date_overlap()
        self.check_leave_overlap()
        return True
    # ==============================================================================

    @api.multi
    def get_duration_joining_leave(self, employee_id, date_from,
                                   date_to, full_half_day):
        ''' Number of days calculate for Without pay Leave '''
        if not full_half_day:
            raise UserError(
                'Please Select Full and Half Day.')
        diff_day = self._get_number_of_days(
            date_from, date_to, employee_id)
        number_of_days_temp = round(math.floor(diff_day))
        return number_of_days_temp

    @api.multi
    def apply_joining_leave(self, employee_id):
        ''' Apply joining Leave '''
        current_date = datetime.now().date()
        val = {}
        code = self.holiday_status_id.code
        # No date_to set so far: automatically compute one 3 months later
        get_utilize_leave = 'get_utilize_{code}'.format(code=code)
        get_leaves = 'get_{code}'.format(code=code)
        if hasattr(self, get_leaves):
            get_leaves = getattr(self, get_leaves)
            # Case Date of joining
            current_date = datetime.now().date()
            end_date = str(current_date)
            start_date = str(current_date)
            res_cur = get_leaves(employee_id, end_date, start_date)
            val['current_leave_bal'] = res_cur
        # Utilize Method
        if hasattr(self, get_utilize_leave):
            get_utilize_leave = getattr(self, get_utilize_leave)
            end_date_current = str(current_date)
            start_date_current = str(current_date)
            res_utilize = get_utilize_leave(employee_id,
                                            end_date_current,
                                            start_date=start_date_current)
            val['current_utilize_leave'] = res_utilize
            val['current_remaining_leave'] = res_cur - res_utilize
        val['full_half_day'] = 'full'
        val['date_from'] = ''
        val['date_to'] = ''
        val['number_of_duration'] = ''
        val['leave_bal'] = ''
        val['utilize_leave'] = ''
        val['remaining_leave'] = ''
        val['remaining_visibility'] = True
        val['matenity'] = False
        val['duration_edit'] = True
        return {'value': val}

    @api.multi
    def get_joining_leave(self, employee_id, end_date,
                          start_date):
        sick_count = 0
        employee = employee_id.id
        current_date = datetime.now().date()
        fiscal_year = self.env['account.fiscalyear']
        year_ids = fiscal_year.search([('date_start', '<=', current_date),
                                       ('date_stop', '>=', current_date)])
        query_leave = """
            SELECT
               sum(leave.number_of_days_temp)
            FROM
                hr_holidays leave,
                hr_holidays_status holiday_status
            WHERE
                leave.holiday_status_id = holiday_status.id AND
                holiday_status.code = 'joining_leave' AND
                leave.type = 'add' AND
                leave.state = 'validate' AND
                leave.employee_id = {} AND
                leave.allocated_date >= '{}' AND
                leave.allocated_date <= '{}' AND
                leave.allocated_date >=  NOW() - INTERVAL '90 days'
        """.format(employee, year_ids.date_start, year_ids.date_stop)
        self._cr.execute(query_leave)
        leave_count = self._cr.fetchall()
        sick_count = leave_count[0][0]
        if sick_count == None:
            sick_count = 0
        return sick_count

    @api.multi
    def get_utilize_joining_leave(self, employee_id,
                                  end_date, start_date):
        ''' Count Number of joining utilize leave '''
        total_number = 0.0
        fiscal_year = self.env['account.fiscalyear']
        year_ids = fiscal_year.search([('date_start', '<=', end_date),
                                       ('date_stop', '>=', end_date)])
        hr_holidays_ids = self.search([('employee_id', '=', employee_id.id),
                                       ('holiday_status_id', '=',
                                        'Joining Leave'),
                                       ('state', 'in', ('validate', 'validate1')),
                                       ('type', '=', 'remove')])
        for hr_holidays_id in hr_holidays_ids:
            if (year_ids.date_start <= hr_holidays_id.date_from and
                    year_ids.date_stop >= hr_holidays_id.date_from):
                total_number += hr_holidays_id.number_of_days_temp
        return total_number

    @api.multi
    def calculate_joining_leave(self):
        ''' Calculate joining Leave .'''
        self.check_duration()
        # self.check_saturday_holiday(self.date_from, self.date_to)
        self.check_gazetted_holiday(
            self.employee_id,
            self.date_to,
            self.date_from)
        self.check_date_overlap()
        self.check_leave_overlap()
        return True

    @api.multi
    def get_duration_short_leave(self, employee_id, date_from,
                                 date_to, full_half_day):
        ''' Number of days calculate for Short Leave '''
        if not full_half_day:
            raise UserError(
                'Please Select Day Status.')
        if full_half_day == 'quarter':
            diff_day = self._get_number_of_days(
                date_from, date_to, employee_id)
            if round(math.floor(diff_day)) == 0.0:
                number_of_days_temp = round(math.floor(diff_day))
            else:
                number_of_days_temp = round(math.floor(diff_day))
        if full_half_day == 'half':
            diff_day = self._get_number_of_days(
                date_from, date_to, employee_id)
            if round(math.floor(diff_day)) == 0.0:
                number_of_days_temp = round(math.floor(diff_day))
            else:
                number_of_days_temp = round(math.floor(diff_day)) - 0.5
        elif full_half_day == 'full':
            diff_day = self._get_number_of_days(
                date_from, date_to, employee_id)
            number_of_days_temp = round(math.floor(diff_day))
        return number_of_days_temp

    @api.multi
    def get_short_leave(self, employee_id, end_date,
                        start_date=None):

        leave_count = 0
        if self.employee_id.leave_structure_id.active_:
            for rec in self.employee_id.leave_structure_id.structure_line_ids:
                if rec.leave_type.name == 'Short Leave':
                    leave_count = rec.monthly_annual

        return leave_count

    @api.multi
    def get_utilize_short_leave(self, employee_id,
                                end_date, start_date):
        ''' Count Number of Earned utilize leave '''
        fst_month_date = datetime.strptime(
            start_date, '%Y-%m-%d').replace(day=1)
        total_number = 0.0
        fiscal_year = self.env['account.fiscalyear']
        year_ids = fiscal_year.search([('date_start', '<=', end_date),
                                       ('date_stop', '>=', end_date)])
        hr_holidays_ids = self.search([('employee_id', '=', employee_id.id),
                                       ('holiday_status_id', '=', 'Short Leave'),
                                       ('state', 'in', ('validate',
                                                        'confirm', 'validate1')),
                                       ('type', '=', 'remove'),
                                       ('date_from', '>=', fst_month_date),
                                       ('date_to', '<=', end_date)])
        for hr_holidays_id in hr_holidays_ids:
            if (year_ids.date_start <= hr_holidays_id.date_from and
                    year_ids.date_stop >= hr_holidays_id.date_from):
                total_number += hr_holidays_id.number_of_days_temp
        return total_number

    @api.multi
    def get_utilize_modify_short_leave(self, employee_id,
                                       end_date, start_date):
        total_number = 0.0
        fiscal_year = self.env['account.fiscalyear']
        year_ids = fiscal_year.search(
            [('date_start', '<=', end_date),
             ('date_stop', '>=', end_date)],
            limit=1)
        hr_holidays_ids = self.search(
            [('employee_id', '=', employee_id),
             ('holiday_status_id', '=', 'Casual Leave'),
             ('type', '=', 'remove'),
             ('state', 'in', ('validate', 'confirm', 'modify', 'validate1')),
             ('date_from', '<', end_date),
             ('date_to', '<', start_date)])
        for hr_holidays_id in self.browse(hr_holidays_ids):
            if (year_ids.date_start <= hr_holidays_id.date_from and
                    year_ids.date_stop >= hr_holidays_id.date_from):
                total_number += hr_holidays_id.number_of_days_temp
        return total_number

    @api.multi
    def apply_short_leave(self, employee_id):
        ''' Apply Short Leave '''
        current_date = datetime.now().date()
        val = {}
        holiday_status_obj = self.env['hr.holidays.status']
        holiday_status_id = holiday_status_obj.search(
            [('name', '=', 'Short Leave')])
        rec_id = holiday_status_obj.browse(holiday_status_id)
        employee = self.env["hr.employee"].browse(employee_id)
        fiscal_year = self.env['account.fiscalyear']
        year_id = fiscal_year.search([
            ('date_start', '<=', current_date),
            ('date_stop', '>=', current_date)])
        year = fiscal_year.browse(year_id)
        code = self.holiday_status_id.code
        # No date_to set so far: automatically compute one 3 months later
        get_utilize_leave = 'get_utilize_{code}'.format(code=code)
        get_leaves = 'get_{code}'.format(code=code)
        if hasattr(self, get_leaves):
            get_leaves = getattr(self, get_leaves)
            if self.employee_id.date_of_joining:
                join_day = datetime.strptime(
                    self.employee_id.date_of_joining, '%Y-%m-%d')
            else:
                raise UserError('Your Joining Date is'
                                'not configured in the system.'
                                'Please contact HR Ops team')
            # Case Date of joining
            if year:
                if (fiscal_year.date_start <= self.employee_id.date_of_joining and
                        fiscal_year.date_stop >= self.employee_id.date_of_joining):
                    end_date = str(current_date)
                    start_date_current = self.employee_id.date_of_joining
                    res_cur_get = get_leaves(employee_id,
                                             end_date,
                                             start_date=start_date_current)
                    res_cur = 0.0
                    if join_day.day > 15:
                        res_cur = res_cur_get - 2
                        val['current_leave_bal'] = res_cur
                    else:
                        res_cur = res_cur_get
                        val['current_leave_bal'] = res_cur
                else:
                    end_date = str(current_date)
                    start_date_current = fiscal_year.date_start
                    res_cur = get_leaves(employee_id,
                                         end_date,
                                         start_date=start_date_current)
                    val['current_leave_bal'] = res_cur
            else:
                raise UserError('No fiscal year available in'
                                ' system please contact your HR.')
        # Utilize Method
        if hasattr(self, get_utilize_leave):
            get_utilize_leave = getattr(self, get_utilize_leave)
            end_date_current = str(current_date)
            start_date_current = str(current_date)
            res_utilize = get_utilize_leave(employee_id,
                                            end_date_current,
                                            start_date=start_date_current)
            val['current_utilize_leave'] = res_utilize
            val['current_remaining_leave'] = res_cur - res_utilize
        val['full_half_day'] = 'quarter'
        val['date_from'] = ''
        val['date_to'] = ''
        val['number_of_duration'] = ''
        val['leave_bal'] = ''
        val['utilize_leave'] = ''
        val['remaining_leave'] = ''
        val['remaining_visibility'] = False
        val['matenity'] = False
        val['duration_edit'] = True
        return {'value': val}

    @api.multi
    def calculate_short_leave(self):
        ''' Calculate Earned Leave Check Leave '''
        DATETIME_FORMAT = "%Y-%m-%d"
        join_date = self.employee_id.date_of_joining
        start_date = datetime.strptime(str(self.date_from), DATETIME_FORMAT)
        joining_date = datetime.strptime(str(join_date), DATETIME_FORMAT)
        self.check_duration()
        # self.check_saturday_holiday(self.date_from, self.date_to)
        self.check_gazetted_holiday(self.employee_id, self.date_to,
                                    self.date_from)
        self.check_date_overlap()
        self.check_leave_overlap()
        return True

    @api.multi
    def get_restricted_leave(self, employee_id, end_date, start_date):
        leave_count = 0
        current_date = datetime.now().date()
        year_id = self.env['account.fiscalyear'].search([
            ('date_start', '<=', current_date),
            ('date_stop', '>=', current_date)])
        joining_date = datetime.strptime(
            self.employee_id.date_of_joining, '%Y-%m-%d')
        if self.employee_id.leave_structure_id.active_:
            for rec in self.employee_id.leave_structure_id.structure_line_ids:
                if rec.leave_type.name == 'Restricted Leave':
                    leave_count = rec.monthly_annual
        if joining_date >= datetime.strptime(
                year_id.date_start, '%Y-%m-%d') and joining_date.day > 20 and leave_count > 0:
            leave_count -= 2
        return leave_count

    @api.multi
    def get_utilize_restricted_leave(self, employee_id, end_date, start_date):
        ''' Count Number of Casual utilize leave '''
        total_number = 0.0
        fiscal_year = self.env['account.fiscalyear']
        year_ids = fiscal_year.search([('date_start', '<=', end_date),
                                       ('date_stop', '>=', end_date)])
        hr_holidays_ids = self.search([
            ('employee_id', '=', employee_id.id),
            ('holiday_status_id', '=', 'Restricted Leave'),
            ('state', 'in', ('validate', 'confirm')),
            ('type', '=', 'remove')
        ])
        for hr_holidays_id in hr_holidays_ids:
            if (year_ids.date_start <= hr_holidays_id.date_from and
                    year_ids.date_stop >= hr_holidays_id.date_from):
                total_number += hr_holidays_id.number_of_days_temp
        return total_number

    @api.multi
    def get_duration_restricted_leave(self, employee_id,
                                      date_from, date_to, full_half_day):
        ''' Number of days calculate for Casual Leave '''
        if not full_half_day:
            raise UserError(
                'Please Select Full and Half Day.')
        if full_half_day == 'full':
            diff_day = self._get_number_of_days(
                date_from, date_to, employee_id)
            number_of_days_temp = round(math.floor(diff_day))
        return number_of_days_temp

    @api.multi
    def apply_restricted_leave(self, employee_id):
        ''' Apply Casual Leave '''
        current_date = datetime.now().date()
        val = {}
        holiday_status_obj = self.env['hr.holidays.status']
        holiday_status_id = holiday_status_obj.search(
            [('name', '=', 'Restricted Leave')])
        rec_id = holiday_status_obj.browse(holiday_status_id)
        employee = self.env["hr.employee"].browse(employee_id)
        fiscal_year = self.env['account.fiscalyear']
        year_id = fiscal_year.search([
            ('date_start', '<=', current_date),
            ('date_stop', '>=', current_date)])
        year = fiscal_year.browse(year_id)
        code = self.holiday_status_id.code
        # No date_to set so far: automatically compute one 3 months later
        get_utilize_leave = 'get_utilize_{code}'.format(code=code)
        get_leaves = 'get_{code}'.format(code=code)
        if hasattr(self, get_leaves):
            get_leaves = getattr(self, get_leaves)
            if self.employee_id.date_of_joining:
                join_day = datetime.strptime(
                    self.employee_id.date_of_joining, '%Y-%m-%d')
            else:
                raise UserError('Your Joining Date is'
                                'not configured in the system.'
                                'Please contact HR Ops team')
            # Case Date of joining
            if year:
                if (fiscal_year.date_start <= self.employee_id.date_of_joining and
                        fiscal_year.date_stop >= self.employee_id.date_of_joining):
                    end_date = str(current_date)
                    start_date_current = self.employee_id.date_of_joining
                    res_cur_get = get_leaves(employee_id,
                                             end_date,
                                             start_date=start_date_current)
                    res_cur = 0.0
                    if join_day.day > 15:
                        res_cur = res_cur_get - 2
                        val['current_leave_bal'] = res_cur
                    else:
                        res_cur = res_cur_get
                        val['current_leave_bal'] = res_cur
                else:
                    end_date = str(current_date)
                    start_date_current = fiscal_year.date_start
                    res_cur = get_leaves(employee_id,
                                         end_date,
                                         start_date=start_date_current)
                    val['current_leave_bal'] = res_cur
            else:
                raise UserError('No fiscal year available in'
                                ' system please contact your HR.')
        # Utilize Method
        if hasattr(self, get_utilize_leave):
            get_utilize_leave = getattr(self, get_utilize_leave)
            end_date_current = str(current_date)
            start_date_current = str(current_date)
            res_utilize = get_utilize_leave(employee_id,
                                            end_date_current,
                                            start_date=start_date_current)
            val['current_utilize_leave'] = res_utilize
            val['current_remaining_leave'] = res_cur - res_utilize
        val['full_half_day'] = 'full'
        val['date_from'] = ''
        val['date_to'] = ''
        val['number_of_duration'] = ''
        val['leave_bal'] = ''
        val['utilize_leave'] = ''
        val['remaining_leave'] = ''
        val['remaining_visibility'] = False
        val['matenity'] = False
        val['duration_edit'] = True
        return {'value': val}

    @api.multi
    def calculate_restricted_leave(self):
        ''' Calculate casual Leave .'''
        self.check_duration()
        self.check_gazetted_holiday(
            self.employee_id,
            self.date_to,
            self.date_from)
        self.check_restricted_holiday(
            self.employee_id,
            self.date_to,
            self.date_from)
        self.check_date_overlap()
        self.check_leave_overlap()
        self.check_end_date()
        return True

    @api.multi
    def check_end_date(self):
        '''Check Duration '''
        for holiday in self:
            if holiday.number_of_days_temp > 1:
                raise UserError('Your leave duration exceeds'
                                ' the maximum possible number of leaves allowed')
        return True

    @api.multi
    def validate(self):
        ''' Validate all case '''
        if (self.date_from and self.date_to) and (self.date_from > self.date_to):
            raise UserError(
                _('The start date must be anterior to the end date.'))
        code = self.holiday_status_id.code
        calculate_method_name = 'calculate_{code}'.format(code=code)
        if code == 'earned_leave':
            resource_calendar = self.env['hr.contract'].search([
                ('employee_id', '=', self.employee_id.id),
                ('status1', '=', 'confirmed')
            ])
            if not resource_calendar:
                raise UserError(
                    "You can apply Earned Leave, only after your Contract confirmation.")
        if self.date_from and self.date_to:
            self.check_saturday_holiday(self.date_from, self.date_to)
        if self.date_from or self.date_to:
            if hasattr(self, calculate_method_name):
                calculate_method = getattr(self, calculate_method_name)
                return calculate_method()
            else:
                leave_type = self.holiday_status_id.name
                raise UserError(
                    "Apply method for Leave is not implemented")
        return True

    def _check_state_access_right(self, vals):
        return True

    @api.model
    def create(self, vals):
        ''' Craete Leave Record '''
        if not self.reference:
            vals['reference'] = self.env['ir.sequence'].get('hr.holidays')
        res = super(HrHolidays, self).create(vals)
        res.write({'state': 'draft'})
        return res

    @api.multi
    def write(self, vals):
        ''' write Leave Record '''
        if self.holiday_status_id.id:
            non_allocated_leave = ['maternity_leave', 'leave_wo_pay']
            non_short_leave = ['short_leave']
            non_restricted_leave = ['restricted_leave']
            code = self.holiday_status_id.code
            if code not in non_allocated_leave:
                apply_method_name = 'apply_{code}'.format(code=code)
                if hasattr(self, apply_method_name):
                    apply_method = getattr(self, apply_method_name)
                    get_apply = apply_method(self.employee_id)
                    vals.update({'current_leave_bal': get_apply['value']['current_leave_bal'],
                                 'current_utilize_leave': get_apply['value']['current_utilize_leave'],
                                 'current_remaining_leave': get_apply['value']['current_remaining_leave'],
                                 # 'leave_bal': self.estimated_leave_bal,
                                 # 'remaining_leave': self.estimated_remainig_leave,
                                 })
            if code in non_short_leave:
                apply_method_name = 'apply_{code}'.format(code=code)
                if hasattr(self, apply_method_name):
                    apply_method = getattr(self, apply_method_name)
                    get_apply = apply_method(self.employee_id)
                    vals.update({'current_leave_bal': get_apply['value']['current_leave_bal'],
                                 'current_utilize_leave': get_apply['value']['current_utilize_leave'],
                                 'current_remaining_leave': get_apply['value']['current_remaining_leave'],
                                 # 'leave_bal': self.estimated_leave_bal,
                                 # 'remaining_leave': self.estimated_remainig_leave,
                                 'full_half_day': 'quarter'
                                 })
            if code in non_restricted_leave:
                apply_method_name = 'apply_{code}'.format(code=code)
                if hasattr(self, apply_method_name):
                    apply_method = getattr(self, apply_method_name)
                    get_apply = apply_method(self.employee_id)
                    vals.update({'current_leave_bal': get_apply['value']['current_leave_bal'],
                                 'current_utilize_leave': get_apply['value']['current_utilize_leave'],
                                 'current_remaining_leave': get_apply['value']['current_remaining_leave'],
                                 # 'leave_bal': self.estimated_leave_bal,
                                 # 'remaining_leave': self.estimated_remainig_leave,
                                 'full_half_day': 'full'
                                 })
        res = super(HrHolidays, self).write(vals)
        if self.employee_id.earned_leave_year:
            if self.date_from != 0 and self.date_to != 0:
                check_carry_forward = self.employee_id.earned_leave_year[-1]
                check_carry_forward.check_carry = False
        self.validate()
        return res

    @api.multi
    def send_to_manager(self):
        ''' Leave submit '''
        if self.type == "remove":
            for id in self:
                if not self.check_employee():
                    raise UserError(_(
                        'The leave request can not be submitted '
                        'by any user other than Employee who raised the Leave Request'))
            template_id = self.env.ref(
                'hr_leaves.employee_leave_approval_process')
            if template_id:
                template_id.send_mail(self.id, force_send=True)
            return self.write({'state': 'confirm'})
        else:
            return self.write({'state': 'confirm'})

    @api.multi
    def check_holidays(self):
        return True

    @api.multi
    def holidays_validate(self):
        '''
        To update the utilized leaves in employee record after final approval
        '''
        # if self.type == 'remove':
        #     template_id = self.env.ref(
        #         'hr_leaves.employee_leave_approval_process_manager')
        #     if template_id:
        #         template_id.send_mail(self.id, force_send=True)
        return self.write({'state': 'validate1'})

    @api.multi
    def holidays_validate1(self):
        '''
        To update the utilized leaves in employee record after final approval
        '''
        if self.type == 'remove':
            template_id = self.env.ref(
                'hr_leaves.employee_leave_approval_process_hod')
            if template_id:
                template_id.send_mail(self.id, force_send=True)
        self.write({'state': 'validate'})

    @api.multi
    def holidays_allocation_validate(self):

        return self.write({'state': 'validate1'})

    @api.multi
    def holidays_allocation_validate1(self):

        return self.write({'state': 'validate', 'allocated_date': datetime.now().date()})

    @api.multi
    def unlink(self):
        """Allows to delete hr holiday lines in draft,cancel states"""
        for rec in self:
            user_list = [1]
            user_list.append(rec.employee_id.user_id.id)
            if rec.state in ['confirm', 'validate', 'cancel', 'refuse']:
                raise UserError(
                    'Cannot delete a leaves which is'
                    ' in state \'%s\'.' % (rec.state))
            for id in self:
                if not self.check_employee():
                    raise UserError(
                        'The leave request can not be deleted'
                        'by any user other than Employee')

        return super(HrHolidays, self).unlink()

    # Cancel Leave
    @api.multi
    def holidays_cancel(self):
        for record in self:
            if record.type == 'remove':
                user_list = [1]
                user_list.append(record.employee_id.user_id.id)
                if self.env.user.id not in user_list:
                    raise UserError(
                        'Only Employees can cancel their leave'
                        ' request.')
        self.write({
            'state': 'cancel',
        })
        return True

    @api.multi
    def holidays_modify(self):
        ''' Modify Leave '''
        for record in self:
            user_list = [1]
            user_list.append(record.employee_id.user_id.id)
            if self.env.uid not in user_list:
                raise UserError('Warning!',
                                'Only Employees can modify their leave'
                                ' request.')
        # email_obj = self.env['hr.email.configuration']
        # ir_model_data = self.env['ir.model.data']
        # template_obj = self.env['email.template']
        # # Email Code
        # email_ids = email_obj.search()
        # email = email_obj.browse(email_ids)
        # if email:
        #     email_from = str(email.email_from)
        # else:
        #     raise ValidationError('Please configure email at Email',
        #                           'Configuration')
        # template_id = ir_model_data.get_object_reference(
        #     'hcah_leave_management',
        #     'employee_leave_request_modify')[1]
        # if template_id:
        #     mail_id = template_obj.send_mail(
        #         template_id, force_send=True)


    # Reset Leave
    @api.multi
    def holidays_reset(self):
        to_unlink = []
        for record in self:
            user_list = [1]
            user_list.append(record.employee_id.parent_id.user_id.id)
            if record.type == 'remove':
                template_id = self.env.ref(
                    'hr_leaves.employee_leave_reset_to_draft')
                if template_id:
                    template_id.send_mail(self.id, force_send=True)
            for record2 in record.linked_request_ids:
                self.holidays_reset([record2.id])
                to_unlink.append(record2.id)
        self.write({'state': 'draft'})
        if to_unlink:
            self.unlink(to_unlink)
        return True

    # Refuse Holidays
    @api.multi
    def holidays_refuse(self):
        for holiday in self:
            if holiday.type == 'remove':
                template_id = self.env.ref(
                    'hr_leaves.employee_leave_allocation_refused')
                if template_id:
                    template_id.send_mail(self.id, force_send=True)
            if holiday.reason_note_id is False:
                raise UserError(
                    'Kindly enter reasons for rejecting the leave to proceed for refusal')

        return self.write({'state': 'refuse'})

    # @api.multi
    # def earned_leave(self):
    #     """Scheduler for earned leave carry over."""
    #     employee_obj = self.env['hr.employee']
    #     emp_ids = employee_obj.search(
    #         [('active', '=', True)],
    #     )
    #     for employee in employee_obj.browse(
    #             emp_ids):
    #         employee_obj.button_allocation_leave(
    #             employee.id)
    #     return True


class holiday_calendar_period(models.Model):
    _name = "holiday.calendar.period"
    _description = "Holidays Calendar Period"

    name = fields.Char('Holiday Name')
    date_start = fields.Date('Start Date')
    date_end = fields.Date('End Date')
    holiday_id = fields.Many2one('hr.holidays.calendar', 'Holiday')
    holiday_type = fields.Selection(
        [('gazetted', 'Gazetted Holiday'),
         ('restricted', 'Restricted Holiday')], default='gazetted', string='Holiday Type')

    @api.onchange('date_start')
    def onchange_holiday_date_start(self):
        fiscal_id = self.env.context.get("fiscal_id")
        if (fiscal_id):
            calendar_obj = self.env['account.fiscalyear'].browse(fiscal_id)
            if self.date_start and (self.date_start < calendar_obj.date_start or self.date_start > calendar_obj.date_stop):
                raise UserError("Warning! "
                                "You can not select start date outside the slected fiscal year!")
        else:
            raise UserError("Warning! "
                            "First select fiscal year!")

    @api.onchange('date_end')
    def onchange_holiday_end_date(self):
        fiscal_id = self.env.context.get("fiscal_id")
        if (fiscal_id):
            calendar_obj = self.env['account.fiscalyear'].browse(fiscal_id)
            if self.date_end and (self.date_end < calendar_obj.date_start or self.date_end > calendar_obj.date_stop):
                raise UserError("Warning! "
                                "You can not select End date outside the slected fiscal year!")
            # if self.date_end < self.date_start:
            #     raise UserError("Warning! "
            #                     "The 'Start Date' must be anterior to the 'End Date'")
        else:
            raise UserError("Warning! "
                            "First select fiscal year!")


class holiday_calendar_year(models.Model):
    _name = "holiday.calendar.year"
    _description = "Calendar Year"

    name = fields.Char('Calendar Year')
    start_date = fields.Date('Start Date')
    end_date = fields.Date('End Date')
    active = fields.Boolean('Active', default=False)
    # 'employee_id': fields.Many2one('hr.employee', 'Employee'),

    # _sql_constraints = [
    #     ('name_uniq', 'unique(name)',
    #      'There is already Calendar Year defined for this year!'),
    # ]

    @api.onchange('start_date')
    def onchange_start_date(self):
        if self.start_date:
            start_date = datetime.strptime(self.start_date, '%Y-%m-%d')
            start_year = start_date.year
            start_day = start_date.day
            end_date = start_date + \
                relativedelta(year=start_year + 1) - timedelta(days=1)
            return {'value': {'end_date': end_date}}


class HREmployee(models.Model):
    _inherit = "hr.employee"

    earned_leave_year = fields.One2many(
        'earned.leave.year', 'employee_id', 'Earned Leave', index=True)
    # journal_id = fields.Many2one('account.journal', 'Journal')
    date_of_joining = fields.Date('Date of Joining')
    leave_policy_ids = fields.One2many(
        'leave.policy',
        'hr_leave_id',
        string="Leave Policy")
    pass_date = fields.Date("DATE")


class EarnedLeavesYear(models.Model):
    _name = "earned.leave.year"

    fiscal_id = fields.Many2one('account.fiscalyear', 'Calendar Year')
    carry_over = fields.Float('Carry Over')
    check_carry = fields.Boolean('Check Carry', default=False)
    employee_id = fields.Many2one('hr.employee', 'Employee')
    leave_encashment_id = fields.Float('Leave Encashment')


class HrHolidaysCalendar(models.Model):
    _name = "hr.holidays.calendar"
    _description = "Holidays Calendar"

    @api.multi
    def _default_country(self):
        return self.env.context.get('default_country_id') or self.env['res.country'].search([('code', '=', 'IN')], limit=1)

    name = fields.Char('Description')
    year_id = fields.Many2one('holiday.calendar.year', 'Calendar Year')
    start_date = fields.Date('Start Date')
    end_date = fields.Date('End Date')
    company_id = fields.Many2one('res.company', 'Company')
    period_ids = fields.One2many(
        'holiday.calendar.period', 'holiday_id', 'Holiday Period')
    country_id = fields.Many2one(
        'res.country', 'Country', default=_default_country)
    state_id = fields.Many2one('res.country.state', 'State')
    fiscal_id = fields.Many2one('account.fiscalyear', 'Fiscal Year')

    # _sql_constraints = [
    #     ('fiscal_id', 'state_id', 'unique(year_id, state_id)',
    #      'There is already Holiday Calendar defined for this Company!'),
    # ]

    # _sql_constraints = [('name_uniq', 'UNIQUE (state_id)',
    #                      'There is already Holiday Calendar defined for this Company!')]

    # @api.constrains('dob')
    #  def _check_fiscal(self):
    #     for record in self:

    #         if self. <= d_date:

    #             raise ValidationError("Your DOB is should be less then today date")

    @api.constrains('fiscal_id', 'state_id')
    def check_calendar_duplication(self):
        obj = self.env['hr.holidays.calendar'].search([('id', '!=', self.id)])
        for record in obj:
            if (self.company_id.id == record.company_id.id
                    and self.fiscal_id.id == record.fiscal_id.id
                    and self.state_id.id == record.state_id.id):
                raise UserError("Warning! "
                                "There is already Holiday Calendar defined for the same Company, Fiscal year and State.")


class LeavePolicy(models.Model):
    _name = "leave.policy"

    leave_type = fields.Many2one("leave.type", 'Leave Type')
    hr_leave_id = fields.Many2one("hr.employee", 'Employee')
    check_box = fields.Boolean(string=" ")
    accrued = fields.Float('Accrued')


class FiscalYear(models.Model):
    _name = 'period.fiscalyear'

    date_start = fields.Date()
    date_stop = fields.Date()
    code = fields.Char()
