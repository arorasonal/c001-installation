from odoo import fields, models
from odoo.tools.translate import _
from odoo.exceptions import UserError, ValidationError
import base64


class LeaveImport(models.TransientModel):
    """Import Contract."""

    _name = 'leave.import.wiz'

    file_leave_import = fields.Binary('Upload File', required=True)

    def import_leaves(self):
        x = base64.decodestring(self.file_leave_import).split('\n')
        length = len(base64.decodestring(self.file_leave_import).split('\n'))
        f = []
        total_length = length - 1
        count = 0
        for data in range(1, total_length):
            try:
                count += 1
                f = x[data]
                v = f.split(",")
                emp_obj = self.env['hr.employee'].search(
                    [('old_employee_code', '=', v[0].strip())])
                if emp_obj:
                    leave_type = self.env['hr.holidays.status'].search([
                        ('name', '=', v[3].strip())])
                    if leave_type.name == "Maternity Leave":
                        duration_edit = False
                    else:
                        duration_edit = True
                    if leave_type.name in ['Maternity Leave', 'On Duty Leave', 'Unpaid Leave', 'Short Leave']:
                        remaining_visibility = False
                    else:
                        remaining_visibility = True
                    data = {
                        'employee_id': emp_obj.id,
                        'full_half_day': v[7].strip(),
                        'name': v[8].strip(),
                        'date_from': v[4].strip(),
                        'date_to': v[5].strip(),
                        'holiday_status_id': leave_type.id,
                        'remaining_visibility': remaining_visibility,
                        'duration_edit': duration_edit,
                        'state': 'validate'
                    }
                    leave_new_id = self.env['hr.holidays'].new(data)
                    leave_data = leave_new_id.onchange_date_to()
                    leave_types = ['leave_bal', 'number_of_days_temp', 'remaining_leave',
                                   'utilize_leave', 'estimated_leave_bal', 'estimated_remainig_leave']
                    leave_record = list(
                        filter(lambda x: x in leave_data['value'], leave_types))
                    for rec in leave_record:
                        data.update({rec: leave_data['value'][rec]})
                    leave_new_id.onchange_employee()
                    leave_new_id.onchange_holiday_status_id()
                    leave_new_id.create(data)
                else:
                    print "Employee Not Found", v[0].strip()

            except UserError, e:
                e = e.message + e[0] + ' ' + 'for %s' % emp_obj.name
                raise UserError(e)

            except ValidationError, e:
                e = e.message + e[0] + ' ' + 'for %s' % emp_obj.name
                raise UserError(e)

            # except Exception, e:
            #     raise UserError(
            #         _('Please check .csv file at row %s.') % (count + 1))
