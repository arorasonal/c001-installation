# -*- coding: utf-8 -*-
from odoo import api, fields, models, _
from datetime import datetime, timedelta


class LeaveWiz(models.Model):

    _name = 'leave.wiz'

    date_start = fields.Date('Start Date', required=True)
    date_end = fields.Date('End Date', required=True)

    def get_holidays_list(self, cr, uid, ids, employee_id, context=None):
        """ Holiday calendar. """
        current_wiz = self.browse(cr, uid, ids, context=context)
        holiday_count = 0
        date_format = "%Y-%m-%d"
        start_date = current_wiz.date_start
        end_date = current_wiz.date_end
        date_from = datetime.strptime(start_date, date_format)
        date_to = datetime.strptime(end_date, date_format)
        interval = date_to - date_from
        dt_range = []
        employee_obj = self.pool.get('hr.employee')
        fiscal_year = self.pool.get('account.fiscalyear')
        hr_holiday_obj = self.pool.get('hr.holidays.calendar')
        [dt_range.append(
            (date_from + timedelta(days=x)).strftime('%Y-%m-%d'))
            for x in range(int(interval.days + 1))]
        for dt in dt_range:
            year_id = fiscal_year.search(cr, uid, [
                ('date_start', '<=', date_from),
                ('date_stop', '>=', date_to)],
                context=context, limit=1)
            if year_id:
                emp_ids = employee_obj.search_read(
                    cr, uid, [('id', '=', employee_id)],
                    fields=['office_location'], context=context)
                for office_location in emp_ids:
                    if office_location['office_location']:
                        holiday_id = hr_holiday_obj.search(
                            cr, uid, [
                                ('fiscal_id', 'in', year_id),
                                ('service_area_id', '=', office_location['office_location'][0])
                            ], context=context)
                        if holiday_id:
                            holiday = hr_holiday_obj.browse(
                                cr, uid, holiday_id[0], context=context)
                            for period in holiday.period_ids:
                                if period.holiday_type == 'gazetted':
                                    pfrom = datetime.strptime(
                                        period.date_start, date_format)
                                    pto = datetime.strptime(
                                        period.date_end, date_format)
                                    holiday_period = pto - pfrom
                                    for x in range(int(holiday_period.days + 1)):
                                        holiday_date = (
                                            pfrom + timedelta(days=x)).strftime('%Y-%m-%d')
                                        if dt == holiday_date:
                                            holiday_count += 1
        return holiday_count

    def print_leave(self, cr, uid, ids, context=None):
        """Print Leave Report."""
        if context is None:
            context = {}
        employee_obj = self.pool.get('hr.employee')
        holiday_obj = self.pool.get('hr.holidays')
        resourcecal_obj = self.pool.get('resource.calendar')
        time_check_start = datetime.now()
        fiscal_year = self.pool.get('account.fiscalyear')

        data = self.browse(cr, uid, ids, context=context)
        start_date = data.date_start
        end_date = data.date_end
        datetime_format = "%Y-%m-%d"
        from_dt = datetime.strptime(start_date, datetime_format)
        to_date = datetime.strptime(end_date, datetime_format)
        timedelta = to_date - from_dt
        diff_day = timedelta.days + float(timedelta.seconds) / 86400
        calendar = resourcecal_obj.browse(
            cr, uid, resourcecal_obj.search(cr, uid, [])[0])
        from_dt = resourcecal_obj.get_next_day(
            cr, uid, calendar.id,
            from_dt, context) if str(from_dt.weekday()) not in [
            calendar_attendance.dayofweek
            for calendar_attendance in
                calendar.attendance_ids] else from_dt
        to_date = resourcecal_obj.get_previous_day(
            cr, uid, calendar.id, to_date,
            context) if str(to_date.weekday()) not in [
            calendar_attendance.dayofweek
            for calendar_attendance in
                calendar.attendance_ids] else to_date
        hours_start_end = resourcecal_obj.get_working_hours(
            cr, uid, calendar.id, from_dt, to_date,
            compute_leaves=True)
        working_hours_on_day = resourcecal_obj.working_hours_on_day(
            cr, uid, calendar, from_dt)
        employee_ids = employee_obj.search(
            cr, uid, ['|', ('active', '!=', True), ('active', '=', True)],
            context=context) or []
        all_emp_id = employee_obj.browse(
            cr, uid, employee_ids, context=context)
        emp_list = []
        for all_employee_id in all_emp_id:
            if not all_employee_id.active:
                if (
                    all_employee_id.last_working >= start_date and
                    all_employee_id.last_working <= end_date
                ):
                    emp_list.append(employee_ids)
            else:
                emp_list.append(employee_ids)
        emp_set = set(emp_list[0])
        emp_list_set = list(emp_set)
        for employee_id in emp_list_set:
            holidays = self.get_holidays_list(
                cr, uid, ids, employee_id, context=context)
            days = (hours_start_end) / (working_hours_on_day or 9)
            total = days - holidays
            leave_vals = {
                'allocate_cl': 0,
            }
            leave_earned_vals = {
                'allocate_el': 0,
            }

            year_id = fiscal_year.search(
                cr, uid, [
                    ('date_start', '<=', start_date),
                    ('date_stop', '>=', start_date)],
                context=context, limit=1)
            year = fiscal_year.browse(cr, uid, year_id, context=context)
            if year:
                if (start_date <= all_employee_id.date_of_joining and
                        end_date >= all_employee_id.date_of_joining):
                    res = holiday_obj.get_casual_leave(
                        cr, uid, employee_id, end_date,
                        start_date=start_date, context=context)
                    res1 = holiday_obj.get_earned_leave(
                        cr, uid, employee_id, end_date,
                        start_date=start_date, context=context)
                    if join_day.day > 15:
                        leave_vals['allocate_cl'] = res - 1
                        leave_earned_vals['allocate_el'] = res1 - 2
                    else:
                        leave_vals['allocate_cl'] = res
                        leave_earned_vals['allocate_el'] = res1
                else:
                    res = holiday_obj.get_casual_leave(
                        cr, uid, employee_id, end_date,
                        start_date=start_date, context=context)
                    res1 = holiday_obj.get_earned_leave(
                        cr, uid, employee_id, end_date,
                        start_date=start_date, context=context)
                    leave_vals['allocate_cl'] = res
                    leave_earned_vals['allocate_el'] = res1
            # Leave Request
            remove_query = """
            select distinct
                hh.number_of_days_temp,
                hh.leave_balanc
            from
                hr_holidays hh,
                hr_employee he,
                hr_holidays_status hrs
            where
                hh.employee_id = he.id and
                hh.state = 'validate' and
                hh.type = 'remove'"""

            remove_query += ' AND ' + "hh.date_from >= '{date}'".format(date=start_date)
            remove_query += ' AND ' + "hh.date_to <= '{date}'".format(date=end_date)
            remove_query += ' AND hh.employee_id = ' + str(employee_id)

            leave_take_query = leave_earned_take_query = leave_onpay_query = leave_onduty_query = leave_comp_off_query = remove_query
            # Casual Leave Take
            leave_take_query += ' AND hh.holiday_status_id = ' + str(353)
            cr.execute(leave_take_query)
            leave_take_data = cr.fetchall()
            # Structure = (0:taken)
            leave_take_vals = {
                'taken': 0,
            }

            for leave_take_datum in leave_take_data:
                leave_take_vals['taken'] += leave_take_datum[0]
            balance = leave_vals['allocate_cl'] - leave_take_vals['taken']

            # Earned Leave Take
            leave_earned_take_query += ' AND hh.holiday_status_id = ' + str(361)
            cr.execute(leave_earned_take_query)
            leave_earned_take_data = cr.fetchall()
            # Structure = (0:taken)
            leave_earned_take_vals = {
                'taken_el': 0,
            }

            for leave_earned_take_datum in leave_earned_take_data:
                leave_earned_take_vals['taken_el'] += leave_earned_take_datum[0]
            balance_el = (
                leave_earned_vals['allocate_el'] -
                leave_earned_take_vals['taken_el']
            )
            # On Pay Leave Take
            leave_onpay_query += ' AND hh.holiday_status_id = ' + str(355)
            cr.execute(leave_onpay_query)
            leave_onpay_data = cr.fetchall()
            # Structure = (0:allocate_cl)
            leave_onpay_vals = {
                'wo_pay_leaves': 0,
            }
            for leave_onpay_datum in leave_onpay_data:
                leave_onpay_vals['wo_pay_leaves'] += leave_onpay_datum[0]

            # On Duty Leave Take
            leave_onduty_query += ' AND hh.holiday_status_id = ' + str(354)
            cr.execute(leave_onduty_query)
            leave_onduty_data = cr.fetchall()
            # Structure = (0:allocate_cl)
            leave_onduty_vals = {
                'on_duty_leaves': 0,
            }
            for leave_onduty_datum in leave_onduty_data:
                leave_onduty_vals['on_duty_leaves'] += leave_onduty_datum[0]

            # Compensatory Leave Take
            leave_comp_off_query += ' AND hh.holiday_status_id = ' + str(362)
            cr.execute(leave_comp_off_query)
            leave_comp_off_data = cr.fetchall()
            # Structure = (0:allocate_cl)
            leave_comp_off_vals = {
                'comp_off': 0,
            }
            for leave_comp_off_datum in leave_comp_off_data:
                leave_comp_off_vals['comp_off'] += leave_comp_off_datum[0]
            # Vals Update Value
            cal_res = {
                'allocate_cl': leave_vals['allocate_cl'],
                'taken': leave_take_vals['taken'],
                'cl_balance': balance,
                'allocate_el': leave_earned_vals['allocate_el'],
                'taken_el': leave_earned_take_vals['taken_el'],
                'el_balance': balance_el,
                'wo_pay_leaves': leave_onpay_vals['wo_pay_leaves'],
                'on_duty_leaves': leave_onduty_vals['on_duty_leaves'],
                'comp_off': leave_comp_off_vals['comp_off'],
                'start_date': start_date,
                'end_date': end_date,
                'standard_paid_days': diff_day + 1,
                'actual_paid_days': total
            }
            employee_obj.write(
                cr, uid, employee_id, cal_res, context=context)
        datas = {
            'ids': emp_list_set,
            'model': 'hr.employee'
        }
        print "Total Time Taken: " + str(datetime.now() - time_check_start), "Leave Report"
        return{'type': 'ir.actions.report.xml',
               'report_name': 'leave.data.xls',
               'datas': datas}


class HREmployee(models.Model):
    _inherit = "hr.employee"

    allocate_cl = fields.Integer('CL Allotted')
    taken = fields.Integer('CL Taken')
    cl_balance = fields.Float('CL Balance')
    allocate_el = fields.Integer('EL Allotted')
    taken_el = fields.Integer('EL Taken')
    el_balance = fields.Float('EL Balance')
    wo_pay_leaves = fields.Integer('W/O pay leave')
    on_duty_leaves = fields.Integer('On Duty leave')
    comp_off = fields.Integer('Comp Off')
    start_date = fields.Date('Start date')
    end_date = fields.Date('End date')
    standard_paid_days = fields.Integer('Standard Days')
    actual_paid_days = fields.Integer('Actual Days')