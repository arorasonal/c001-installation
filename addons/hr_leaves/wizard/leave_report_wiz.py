from odoo import api, fields, models, _
import openerp.tools
from openerp.tools.translate import _
from cStringIO import StringIO
import base64
import xlwt
from openerp.exceptions import except_orm, Warning, RedirectWarning


class LeaveReportWiz(models.Model):
    _name = 'leave.report.wiz'

    company_id = fields.Many2one('res.company', 'Company', default=lambda self: self.env['res.company']._company_default_get('leave.report.wiz'))
    date_from = fields.Date('Date From')
    date_to = fields.Date('Date To')

    def print_leave_report(self, cr, uid, ids, context=None):
        if context is None:
            context = {}

        ir_model_data = self.pool.get('ir.model.data')
        template_id = ir_model_data.get_object_reference(cr, uid, 'hcah_leave_management', 'view_hr_holidays_tree1')[1]
        datas = {}
        payslip_obj = self.pool.get('hr.employee')
        data = self.read(cr, uid, ids, context=context)[0]
        for rec in self.browse(cr, uid, ids, context=context):
            company_id = rec.company_id.id 
            date_from = rec.date_from
            date_to = rec.date_to
        domain = [
            ('employee_id.company_id', '=', company_id),('date_from', '>=', date_from),('date_to', '<=', date_to)
        ]
        payslip_ids = payslip_obj.search(cr, uid, domain, context=context) or []
        if payslip_ids:
            for winner_obj_id in payslip_obj.browse(cr, uid, payslip_ids, context=context):   
                payslip_obj.write(cr, uid, winner_obj_id.id, {'report_date_from': date_from, 'report_date_to': date_to, 'report_company' : company_id}, context=context)
        if payslip_ids == [] :
            raise osv.except_osv(" Warning", 'No Records to Print Muster Roll Report Plz Try to select other company or year and months.')
        else:
            datas = {
                 'ids': payslip_ids,
                 'model': 'hr.employee',
    #             'form': data
            }
        return {
            'name': _('Annual Leave Report'),
            'view_type': 'form',
            'view_mode': 'tree',
            'res_model': 'hr.employee',
            'type': 'ir.actions.act_window',
            'view_id': template_id,
            'domain': [('id', '=', payslip_ids)],
        }

    def print_leave_pdf_report(self, cr, uid, ids, context=None):
        if context is None:
            context= {}
        leave_obj = self.pool.get('hr.employee')
        final_ids = []
        emp_obj = self.pool.get('hr.employee')
        for data in self.browse(cr, uid, ids, context=context):
            date_from = data.date_from
            date_to = data.date_to
            company_id = data.company_id.id
        domain = [
            ('employee_id.company_id', '=', company_id)
        ]        
        payslip_ids = leave_obj.search(cr, uid, domain, context=context) or []
        if payslip_ids:
            for winner_obj_id in leave_obj.browse(cr, uid, payslip_ids, context=context):   
                leave_obj.write(cr, uid, winner_obj_id.id, {'report_date_from': date_from, 'report_date_to': date_to, 'report_company' : company_id}, context=context)

        if payslip_ids == []:
            raise osv.except_osv(" Warning", 'No Records to Print Leave Report Plz Try to select other company or year and months.')
        else:
            datas = {
                 'ids': payslip_ids,
                 'model': 'hr.employee',
    #             'form': data
            }

            return {
                'type': 'ir.actions.report.xml',
                'report_name': 'report_leave_report_pdf',
               'datas': datas,
        }
