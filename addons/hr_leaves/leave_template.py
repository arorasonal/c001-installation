from odoo import api, fields, models
from datetime import datetime, timedelta
from odoo.exceptions import Warning, UserError, ValidationError


class LeaveStructure(models.Model):

    _name = 'leave.structure'

    @api.model
    def _get_leave_type(self):
        lis = []
        leave_type_ids = self.env['hr.holidays.status'].search(
            [('active_', '=', True)])
        for rec in leave_type_ids:
            lis.append({
                'leave_type': rec,
                'active_': True,
                'duration': True if (rec.name == 'Earned Leave' or rec.name == 'Casual Leave' or rec.name == 'Sick Leave') else False,
                'monthly_annual': 0.0
            })
        return lis

    name = fields.Char(
        'Name')
    project_id = fields.Many2one(
        'account.analytic.account', 'Project/Analytic Account')
    active_ = fields.Boolean('Active', default=True)
    structure_line_ids = fields.One2many(
        'leave.structure.line',
        'leave_structure_id', default=_get_leave_type)
    structure_default = fields.Boolean("Make Default",
                                       help="If default is selected then, default structure will be selected in employee form automatically, while creation of employee")

    # @api.constrains('project_id', 'active_')
    # def check_leave_structure(self):
    #     obj = self.env['leave.structure'].search([('id', '!=', self.id)])
    #     for record in obj:
    #         if self.project_id.id == record.project_id.id and self.active_ == True:
    #             raise UserError("Warning! "
    #                             "One active leave structure for this project is already exist.")

    @api.multi
    def allocate_leave_structure(self):

        if self.project_id.project_ids:
            employee_obj = self.env['hr.employee'].search([
                ('project_id', '=', self.project_id.id)])
            for rec in employee_obj:
                rec.leave_structure_id = self

    @api.model
    def create(self, vals):
        ''' Monthly/Annual Accural of Earned leave and Sick leave
        should be greatter than zero '''

        holiday_obj = self.env['hr.holidays.status']
        for rec in vals['structure_line_ids']:
            leave_type_id = rec[2]['leave_type']
            holiday_record = holiday_obj.browse([leave_type_id])
            if holiday_record.name == 'Earned Leave' or holiday_record.name == 'Sick Leave':
                if rec[2]['monthly_annual'] <= 0 and rec[2]['active_'] == True:
                    raise UserError(
                        'Monthly/Annual Accural for "Earned" and "Sick" Leave type should be greatter than zero.')
        return super(LeaveStructure, self).create(vals)

    @api.multi
    def write(self, vals):
        ''' Monthly/Annual Accural of Earned leave and Sick leave
        should be greatter than zero '''
        for rec in self.structure_line_ids:
            if rec.leave_type.name == 'Earned Leave' or rec.leave_type.name == 'Sick Leave':
                if rec.monthly_annual <= 0 and rec.active_ == True:
                    raise UserError(
                        'Monthly/Annual Accural for "Earned" and "Sick" Leave type should be greatter than zero.')
        return super(LeaveStructure, self).write(vals)


class LeaveTemplateLine(models.Model):

    _name = 'leave.structure.line'

    active_ = fields.Boolean('Active', default=True)
    duration = fields.Boolean()
    leave_structure_id = fields.Many2one(
        'leave.structure')
    leave_type = fields.Many2one('hr.holidays.status',
                                 'Leave Type')
    monthly_annual = fields.Float('Monthly/Annual Accrual')


class Project(models.Model):

    _inherit = 'project.project'

    leave_structure_id = fields.Many2one(
        'leave.structure',
        'Leave Structure')


class HrEmployee(models.Model):

    _inherit = 'hr.employee'

    leave_structure_id = fields.Many2one(
        'leave.structure',
        string='Leave Structure', Readonly="True")

    @api.model
    def create(self, vals):

        res = super(HrEmployee, self).create(vals)
        leave_structure = self.env["leave.structure"].search(
            [('structure_default', '=', True)], limit=1)
        if leave_structure:
            res.leave_structure_id = leave_structure.id

        return res
