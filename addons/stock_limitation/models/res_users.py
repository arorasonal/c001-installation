from odoo import models, fields, api, _

class Users(models.Model):
    _inherit = 'res.users'

    location_ids = fields.Many2many('stock.location','stock_location_users_rel','user_id','slid','Accepted Location')
