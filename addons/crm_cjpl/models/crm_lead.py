from datetime import datetime, timedelta, date
from operator import itemgetter
from odoo import SUPERUSER_ID
from odoo import tools
from odoo import fields, models, api
from odoo.tools.translate import _
from odoo.tools import email_re, email_split
from odoo.exceptions import UserError, ValidationError, Warning


class crm_lead(models.Model):
    _inherit = "crm.lead"

    user_id = fields.Many2one('res.users', string='Salesperson', index=True,
                              track_visibility='onchange', default=lambda self: self.env.user)
    lost_reason = fields.Many2one(
        'crm.lost.reason', string='Reason for Lost', 
        index=True, track_visibility='onchange', domain=[('show_opportunity', '=', True)])
    lost_date = fields.Date('Lost Date')
    planned_cost = fields.Float("Planned Cost")
    payment_mode = fields.Many2one('crm.payment.mode', string="Payment Mode")
    imported = fields.Boolean('Imported')
    odoo8_id = fields.Integer('Odoo8 Id')
    sale_order_id8 = fields.Integer('Sale Order id8')
    reason_for_lost = fields.Char('Reason for lost')

    # @api.multi
    # def action_cancel(self):
    #     res={}
    #     for lead in self:
    #         if lead.reason_cancel_id:
    #             self.write(lead.id,{'stage_id':3,'lost_date':date.today().strftime('%Y-%m-%d')})

    # @api.onchange('stage_id')
    # def _onchange_stage_id(self):
    #     if not self.lost_reason:
    #         raise ValidationError(_("Please fill the Reason for Lost first."))
    #     values = self._onchange_stage_id_values(self.stage_id.id)
    #     self.update(values)

    @api.multi
    def action_set_won(self):
        """ Won semantic: probability = 100 (active untouched) """
        for lead in self:
            stage_id = lead._stage_find(domain=[('probability', '=', 100.0), ('on_change', '=', True)])
            lead.write({'stage_id': stage_id.id, 'probability': 100})

        return True

    @api.multi
    def action_set_lost(self):
        for lead in self:
            stage_id = lead._stage_find(
                domain=[('probability', '=', 0), ('on_change', '=', True)])
            lead.write({'stage_id': stage_id.id, 'probability': 0,
                        'active': False, 'lost_date': date.today().strftime('%Y-%m-%d')})
        return True

    @api.multi
    def case_mark_lost(self):
        """ Mark the case as lost: state=cancel and probability=0
        """
        stages_leads = {}
        for lead in self:
            stage_id = self.stage_find([lead], lead.team_id.id or False, [(
                'probability', '=', 0.0), ('on_change', '=', True), ('sequence', '>', 1)])
            if stage_id:
                if stages_leads.get(stage_id):
                    stages_leads[stage_id].append(lead.id)
                else:
                    stages_leads[stage_id] = [lead.id]
            else:
                raise UserError(
                    _('To relieve your sales pipe and group all Lost opportunities, configure one of your sales stage as follow:\n'
                        'probability = 0 %, select "Change Probability Automatically".\n'
                        'Create a specific stage or edit an existing one by editing columns of your opportunity pipe.'))
        for stage_id, lead_ids in stages_leads.items():
            if lead.lost_reason:

                self.write(lead_ids, {
                           'stage_id': stage_id, 'lost_date': date.today().strftime('%Y-%m-%d')})
            else:
                raise UserError(
                    _('Please fill the Reason for Lost first.'))
        return True

    @api.onchange('stage_id')
    def _onchange_stage_id(self):
        # if not self.lost_reason:
        #     print('###################')
        #     raise ValidationError(_("Please fill the Reason for Lost first."))
        values = self._onchange_stage_id_values(self.stage_id.id)
        self.update(values)

    @api.model
    def cron_lead_opportunity_alert(self):
        for val in self.env['crm.lead'].search([]):
            obj = self.env['mail.activity'].search(
                [('lead_opportunity', '=', val.id)])
            if not obj:
                if (datetime.strptime(val.date_open, "%Y-%m-%d %H:%M:%S")) < (datetime.now() - timedelta(days=5)):
                    template_id = self.env.ref(
                        'base_crm.crm_lead_activity_alert_template')
                    user = self.env['hr.employee'].search(
                        [('id', '=', self.user_id.id)])
                    if user:
                        template.email_from = user.user_id.login
                        if template_id:
                            template_id.send_mail(
                                val.id, force_send=True)
        return True


class MergeOpportunity(models.TransientModel):
    _inherit = 'crm.merge.opportunity'

    user_id = fields.Many2one(
        'res.users', 'Sales Executive', index=True, required=True)
    team_id = fields.Many2one(
        'crm.team', 'Sales Team', required=True,
        oldname='section_id', index=True)


class Lead2OpportunityMassConvert(models.TransientModel):
    _inherit = 'crm.lead2opportunity.partner.mass'

    user_ids = fields.Many2many(
        'res.users', string='Sales Executive', required=True)
    team_id = fields.Many2one(
        'crm.team', 'Sales Team',
        index=True, oldname='section_id', required=True)
    name = fields.Selection([
        ('convert', 'Convert to opportunity'),
        ('merge', 'Merge with existing opportunities')
    ], 'Conversion Action', required=True, default="convert")


class Partner(models.Model):
    _inherit = "res.partner"

    user_id = fields.Many2one(
        'res.users', string='Sales Executive',
        help='The internal user that is in charge of communicating with this contact if any.')


class Meeting(models.Model):
    _inherit = "calendar.event"

    meeting_agenda = fields.Text('Meeting Agenda', size=4)
    meeting_minutes = fields.Text('Minutes of the Meeting')
    customer_id = fields.Many2one('res.partner', "Customer")
    privacy = fields.Selection(
        [('public', 'Everyone'),
         ('private', 'Only me'),
         ('confidential', 'Only internal users')],
        'Privacy', default='private', states={'done': [('readonly', True)]}, oldname="class")


class reason_cancel(models.Model):
    _name = "reason.cancel"

    name = fields.Char('Name')
    active = fields.Boolean('Active')

class CrmStage(models.Model):
    _inherit = "crm.stage"
  
    types = fields.Selection(
        [('lead', 'Lead'),
         ('opportunity', 'Opportunity'),
         ('both', 'Both')],
        'Type', required=True,  help="This field is used to distinguish stages related to Leads from stages related to Opportunities, or to specify stages available for both types.")