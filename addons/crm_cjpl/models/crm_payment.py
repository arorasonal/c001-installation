
from datetime import datetime,date
from operator import itemgetter
from odoo import SUPERUSER_ID
from odoo import tools
from odoo import fields, models, api
from odoo.tools.translate import _
from odoo.tools import email_re, email_split
from odoo.exceptions import UserError, ValidationError, Warning


class CrmPaymentMode(models.Model):
    """ Payment Mode for Fund """
    _name = "crm.payment.mode"
    _description = "CRM Payment Mode"
    
    name = fields.Char('Name', required=True)
    team_id = fields.Many2one('crm.team', 'Sales Team')

              
   