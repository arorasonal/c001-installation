from odoo import api, fields, models
from datetime import datetime, timedelta
import odoo.addons.decimal_precision as dp


class CrmTeam(models.Model):
    _inherit = "crm.team"

    name = fields.Char('Sales Team', required=True, translate=True)
    user_id = fields.Many2one('res.users', string='Sales Team Leader')
    quotation_address_id = fields.Many2one('res.partner', string='Quotation From Address')
    member_ids = fields.Many2many(
        'res.users', 'rel_team_users', 'team_id', 'user_id', string='Channel Members')


    @api.multi
    def write(self, vals):
        if vals.get('user_id'):
            teams = []
            sale_team = self.env['crm.team'].browse(self.id)
            if sale_team:
                teams.append(sale_team.id)
                teams.extend(self.get_team(sale_team, teams))
                print "#####", vals.get('user_id'), self.user_id
                self.env['res.users'].browse(vals.get('user_id')).update(
                    {'sales_team_ids': [(6, 0, list(set(teams)))]})
                self.user_id.update(
                    {'sales_team_ids': [(6, 0, [])]})
        return super(CrmTeam, self).write(vals)

    def get_team(self, sale_team, teams):
        sale_teams = self.env['crm.team'].search(
            [('parent_team', '=', sale_team.id)])
        if sale_teams:
            for team in sale_teams:
                teams.append(team.id)
                child_team = self.env['crm.team'].search(
                    [('parent_team', '=', team.id)])
                if child_team:
                    self.get_team(team, teams)
        return teams


class User(models.Model):
    _inherit = "res.users"

    sales_team_ids = fields.Many2many(
        'crm.team', 'user_sales_team_rel', 'res_user_rel', 'sales_team_rel', 'Teams Under')
