{

    'name': 'CRM CJPL',
    'version': '1.0',
    'author': 'Apagen Solutions Pvt. Ltd.',
    'website': 'https://www.apagen.com',
    'depends': ['base', 'crm', 'sales_team', 'base', 'sale_crm', 'base_exception', 'analytic'],
    'category': ' ',

    'description': '',
    'data': [
        'security/crm_security.xml',
        'security/ir.model.access.csv',
        'data/ir_cron_view.xml',
        'data/security_rule.xml',
        'view/crm_lead_view.xml',
        # 'view/reason_cancel_form.xml',
        'view/crm_payment_view.xml',
        'view/email_view.xml',
        'view/res_users_view.xml',
        'view/crm_team.xml',
    ],
    'demo_xml': [],
    'js': [],
    'css': [],

    'installable': True,
    'auto-installable': False,
    'active': False,
}
