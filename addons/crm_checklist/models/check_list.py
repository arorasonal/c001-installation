#coding: utf-8
from openerp import models, fields, api, _

class crm_check_list(models.Model):
    _name = "crm.check.list"

    name = fields.Char(string="What should be done on this stage", required=True, )
    crm_stage_st_id = fields.Many2one(
        "crm.stage",
        string="CRM Stage",
        required=True,
    )
    group_ids = fields.Many2many(
        "res.groups",
        "res_groups_crm_check_list_rel_table",
        "res_groups_id",
        "crm_check_list_id",
        string="User groups",
        help="Leave it empty if any user may confirm this checklist item,"
    )
    sequence = fields.Integer(string="Sequence")

    _order = "sequence, id"