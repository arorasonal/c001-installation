# -*- coding: utf-8 -*-
{
    "name": "CRM Check List and Approval Process",
    "version": "10.0.1.1.1",
    "category": "Sales",
    "author": "faOtools",
    "website": "https://faotools.com/apps/10.0/crm-check-list-and-approval-process-247",
    "license": "Other proprietary",
    "application": True,
    "installable": True,
    "auto_install": False,
    "depends": [
        "crm"
    ],
    "data": [
        "views/crm_stage.xml",
        "views/crm_lead.xml",
        "data/data.xml",
        "security/ir.model.access.csv"
    ],
    "qweb": [
        
    ],
    "js": [
        
    ],
    "demo": [
        
    ],
    "external_dependencies": {},
    "summary": "The tool to make sure required jobs are carefully done on this pipeline stage",
    "description": """

For the full details look at static/description/index.html

* Features * 

- Check lists are assigned per each opportunity stage. Set of check list points is up to you

- As soon as an opportunity is moved to a certain stage, its check list is updated to a topical one from this stage. For instance, actions for 'new' and 'proposition' stages must be different, don't they?

- To move a CRM lead forward in your funnel, a check list should be fully confirmed. By 'moving forward' any change of stage with lesser sequence to a bigger sequence is implied

- Confirmation might assume involvement of different user roles. Certain check list points might be approved only by top-level employees, for example. In such a case just assign a user group for this check list item 

- For some situations you do not need a check list fulfilment even a new stage is further. For example, for the 'Cancelled' stage. In such a case just mark this stage as 'No need for checklist'
 
* Extra Notes *



#odootools_proprietary

    """,
    "images": [
        "static/description/main.png"
    ],
    "price": "28.0",
    "currency": "EUR",
}