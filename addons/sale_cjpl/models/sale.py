import time

from odoo import api, models, fields
from odoo.exceptions import UserError
from odoo.tools.safe_eval import safe_eval
from odoo.tools.translate import _
from lxml import etree


class SaleOrder(models.Model):
    _inherit = 'sale.order'
    # _inherit = ["sale.order", "ir.needaction_mixin"]

    @api.model
    def default_get(self, field_list):
        values ={}
        res = super(SaleOrder, self).default_get(field_list)
        context = self._context
        if 'default_medium_id' in context and context.get('default_medium_id') not in [False,'', None]:
            values.update({'medium_id':context.get('default_medium_id')})
        if 'default_source_id' in context and context.get('default_source_id') not in [False,'', None]:
            values.update({'source_id':context.get('default_source_id')})
        if 'default_campaign_id' in context and context.get('default_campaign_id') not in [False,'', None]:
            values.update({'campaign_id':context.get('default_campaign_id')})
        if 'default_team_id' in context and context.get('default_team_id') not in [False,'', None]:
            print('default_team_id')
            values.update({'team_id':context.get('default_team_id')})
        if values:
            values.update({'check_opp_status':True})
            res.update(values)
        return res

    pricelist_id = fields.Many2one('product.pricelist', string='Pricelist', required=False,
                                   readonly=True, states={'draft': [('readonly', False)], 'sent': [('readonly', False)]},
                                   help="Pricelist for current sales order.")
    currency_id = fields.Many2one("res.currency", related='pricelist_id.currency_id',
                                  string="Currency", readonly=True, required=False)
    rental_start_date = fields.Datetime(string='Rental Start Date')
    rental_end_date = fields.Datetime(string='Rental End Date')
    minimum_contract_period = fields.Char(string='Minimum Contract Period')
    road_permit = fields.Selection([
        ('blank', 'Blank'),
        ('yes', 'Yes'),
        ('no', 'No')], string="Road Permit Availability")
    coordinator_approve = fields.Boolean(string='Sales Approved')
    sales_manager_approved = fields.Boolean(string='Sales Manager Approved')
    specl_discount = fields.Float('Specl. Discount')
    time_check = fields.Boolean(string='Time Updated for Sales Order')
    po_ref = fields.Char(string='Po Number')
    sale_type_id = fields.Boolean(string='Sales Type')
    check_opp_status = fields.Boolean(string='Check Opportunity Status')
    contract_ref = fields.Many2one(
        'account.analytic.account', string="Contract Reference")
    reason_cancel_id = fields.Many2one('crm.lost.reason', string='Reason for Cancel',
                                       index=True, domain=[('show_quotation', '=', True)],track_visibility='onchange', states={'cancel': [('readonly', True)]})
    ship_date = fields.Date('Ship Date', track_visibility='onchange')
    product_ids = fields.Many2many(
        'product.product', 'sale_tab', 'current_id', 'sale_id', 'Product', domain="[('imported', '=', True)]")
    code = fields.Boolean('Code')
    name = fields.Char(string='Order Reference', required=True, copy=False, readonly=True, states={
                       'draft': [('readonly', False)]}, index=True, default=lambda self: _('New'))
    state = fields.Selection([
        ('draft', 'Quotation'),
        ('sent', 'Quotation Sent'),
        ('sale', 'Sales Order'),
        ('done', 'Locked'),
        ('cancel', 'Cancelled'),
        ('waiting_date', 'Waiting Schedule'),
        ('manual', 'Sale to Invoice'),
        ('shipping_except', 'Shipping Exception'),
        ('invoice_except', 'Invoice Exception'),
    ], string='Status', readonly=True, copy=False, index=True, track_visibility='onchange', default='draft')

    # for importing data from odoo8
    imported = fields.Boolean("Imported")
    odoo8_id = fields.Integer("Odoo8 Id")
    contract_id8 = fields.Integer("Contract Id8")
    is_company = fields.Boolean()

    @api.multi
    def write(self, vals):
        res = super(SaleOrder, self).write(vals)
        return res

    @api.multi
    @api.onchange('partner_id')
    def onchange_partner_id(self):
        if self.partner_id.company_type == 'company':
            self.is_company = True
        else:
            self.is_company = False
        return super(SaleOrder, self).onchange_partner_id()

    @api.multi
    def action_confirm(self):
        case_obj = self.env['crm.lead']
        for order in self:
            if order.opportunity_id:
                self.opportunity_id.action_set_won()
                print "========", self.opportunity_id
        return super(SaleOrder, self).action_confirm()

    @api.onchange('product_ids')
    def onchange_product_product(self):
        result = {}
        order_list = []
        product_list = []
        if self.product_ids:
            for val in self.product_ids:
                product_list.append(val)

        if product_list:
            for val in product_list:
                order_list.append((0, 0, {'product_id': val.id, 'price_unit': val.list_price, 'state': 'draft',
                                          'product_uom_qty': 1.0, 'delay': 7.0, 'name': val.name, 'product_uom': val.uom_id, 'tax_id': [(6, 0, val.taxes_id.ids)], }))
        self.order_line = order_list

    @api.depends('order_line.price_total')
    def _amount_all(self):
        """
        Compute the total amounts of the SO.
        changed pricelist_id to company_id or not using pricelist
        """
        for order in self:
            amount_untaxed = amount_tax = 0.0
            for line in order.order_line:
                amount_untaxed += line.price_subtotal
                # FORWARDPORT UP TO 10.0
                if order.company_id.tax_calculation_rounding_method == 'round_globally':
                    price = line.price_unit * \
                        (1 - (line.discount or 0.0) / 100.0)
                    taxes = line.tax_id.compute_all(
                        price, line.order_id.currency_id, line.product_uom_qty, product=line.product_id, partner=order.partner_shipping_id)
                    amount_tax += sum(t.get('amount', 0.0)
                                      for t in taxes.get('taxes', []))
                else:
                    amount_tax += line.price_tax
            order.update({
                'amount_untaxed': order.company_id.currency_id.round(amount_untaxed),
                'amount_tax': order.company_id.currency_id.round(amount_tax),
                'amount_total': amount_untaxed + amount_tax,
            })

    @api.multi
    def action_button_confirm(self):
        self.ensure_one()
        if self.detect_exceptions():
            return self._popup_exceptions()

        elif self.origin:
            ref = self.origin
            lenth = len(ref)
            reference = ref[13:lenth]
            crm_obj = self.env['crm.lead'].search([('id', '=', reference)])
            crm_obj.stage_id = 6
            return super(SaleOrder, self).action_button_confirm()
        else:
            return super(SaleOrder, self).action_button_confirm()

    @api.multi
    def action_cancel(self):
        sale_order_line_obj = self.env['sale.order.line']
        account_invoice_obj = self.env['account.invoice']
        for sale in self:
            tasks = self.env['project.task'].search([('sale_order_id','=',sale.id)])
            if len(tasks) > 0:
                if tasks.filtered(lambda r: r.stage_id.stage_cancel == False):
                    raise UserError(("Please cancel the task after that we will cancel Sale Order"))
            picking = self.env['stock.picking'].search([('group_id.name','=',self.name)])
            if picking:
                for val in picking:
                    if val.state != 'cancel':
                        raise UserError(_('There is already a delivery order %s for this SO .')%(val.name))
            for inv in sale.invoice_ids:
                if inv.state not in ('draft', 'cancel'):
                    raise UserError(
                        ('Cannot cancel this sales order! First cancel all invoices attached to this sales order.'))
                inv.signal_workflow('invoice_cancel')
            line_ids = [l.id for l in sale.order_line if l.state != 'cancel']

            if sale.contract_ref:
                contract_obj = self.env['account.analytic.account'].browse(
                    sale.contract_ref.id)
                if contract_obj.state != 'cancelled':
                    raise UserError(
                        _('First cancel contract created from this sale order.'))
            sale_order_line_obj.button_cancel(line_ids)
            if sale.project_id:
                print" dcd ", sale.project_id
                contract_obj = self.env[
                    'account.analytic.account'].browse(sale.project_id.id)
                if contract_obj.state != 'cancelled':
                    raise UserError(
                        _('Please cancel the contract  first.'))
            if sale.reason_cancel_id:
                if sale.opportunity_id:
                    self.opportunity_id.lost_reason = sale.reason_cancel_id.id
                    self.opportunity_id.action_set_lost()
                self.write({'state': 'cancel'})
            else:
                raise UserError(
                    _('Please fill the Reason for cancel first.'))


#         self.write(cr, uid, ids, {'state': 'cancel'})
        return True

    @api.multi
    def _prepare_invoice(self):
        print('_prepare_invoice+++++++++++++++++1111')
        """Prepare the dict of values to create the new invoice for a
           sales order. This method may be overridden to implement custom
           invoice generation (making sure to call super() to establish
           a clean extension chain).

           :param browse_record order: sale.order record to invoice
           :param list(int) line: list of invoice line IDs that must be
                                  attached to the invoice
           :return: dict of value to create() the invoice
        """
        self.ensure_one()
        journal_id = self.env['account.invoice'].default_get(['journal_id'])[
            'journal_id']
        if not journal_id:
            raise UserError(
                _('Please define an accounting sale journal for this company.'))
        invoice_vals = {
            'name': self.client_order_ref or '',
            'origin': self.name,
            'type': 'out_invoice',
            'address_shipping_id': self.partner_shipping_id.id,
            'reference': self.client_order_ref or self.name,
            'account_id': self.partner_invoice_id.property_account_receivable_id.id,
            'partner_id': self.partner_invoice_id.id,
            'journal_id': journal_id,
            # 'invoice_line': [(6, 0, lines)],
            'currency_id': self.company_id.currency_id.id,
            'comment': self.note,
            'payment_term_id': self.payment_term_id.id,
            'fiscal_position_id': self.fiscal_position_id.id or self.partner_invoice_id.property_account_position_id.id,
            'company_id': self.company_id.id,
            'user_id': self.user_id and self.user_id.id,
            'team_id': self.team_id.id,
            'sale_type': self.type_id.id,
            'account_staff_id':self.account_staff_id.id,
            # 'sale_order':self.name,
        }

        return invoice_vals

    @api.multi
    def action_button_contract(self):
        sale = self
        contract_obj = self.env['account.analytic.account']
        contract_obj_line = self.env['account.analytic.invoice.line']
        order_list = []
        res = {}
        sac = ' '
        if sale.type_id.id != 1:
            sac = sale.type_id.sac
            # for line in sale.order_line:
            #     if line.product_id:
            #         order_list.append((0, False, {
            #             'product_id': line.product_id.id,
            #             'name': line.name,
            #             'hsn': line.order_id.type_id.sac,
            #             'quantity': line.product_uom_qty,
            #             'uom_id': line.product_uom.id,
            #             'price_unit': line.price_unit,
            #             'discount': line.discount,
            #             'price_subtotal': line.price_subtotal,

            #         }))
            contract_id = contract_obj.create({'name': sale.name, 'state': 'open', 'type': 'contract',
                                               'partner_id': sale.partner_id.id,
                                               'partner_order_id': sale.partner_order_id.id,
                                               'partner_shipping_id': sale.partner_shipping_id.id,
                                               'partner_invoice_id': sale.partner_invoice_id.id,
                                               'po_ref': sale.client_order_ref,
                                               'code': sale.name,
                                               'saleorder_number': sale.id,
                                               'manager_id': sale.user_id.id,
                                               'date_start': sale.rental_start_date,
                                               'recurring_invoices': True,
                                               'journal_id':sale.invoice_journal_id.id,
                                               'team_id':sale.team_id.id,
                                               'type_id':sale.type_id.id,
                                               'account_staff_id':sale.account_staff_id.id,
                                               }, )
            self.write({'code': True, 'contract_ref': contract_id.id})

            for line in sale.order_line:
                if line.product_id:
                    contract_invoice_line = contract_obj_line.create({
                        'product_id': line.product_id.id,
                        'name': line.name,
                        'quantity': line.product_uom_qty,
                        'uom_id': line.product_uom.id,
                        'price_unit': line.price_unit,
                        'discount': line.discount,
                        'price_subtotal': line.price_subtotal,
                        'analytic_account_id': contract_id.id
                    })
            return True


class sale_order_line(models.Model):
    _inherit = 'sale.order.line'

    product_id = fields.Many2one('product.product', string='Product',
                                 domain="[('sale_ok', '=', True), ('imported', '=', True)]", change_default=True, ondelete='restrict', required=False)
    imported = fields.Boolean("Imported")
    odoo8_id = fields.Integer()

    # def _prepare_order_line_invoice_line(self, account_id=False):
    #     """Prepare the dict of values to create the new invoice line for a
    #        sales order line. This method may be overridden to implement custom
    #        invoice generation (making sure to call super() to establish
    #        a clean extension chain).

    #        :param browse_record line: sale.order.line record to invoice
    #        :param int account_id: optional ID of a G/L account to force
    #            (this is used for returning products including service)
    #        :return: dict of values to create() the invoice line
    #     """
    #     res = {}
    #     hsn = ' '
    #     if line.order_id.type_id.id == 1:
    #         hsn = line.product_id.categ_id.hsc
    #     else:
    #         hsn = order_id.type_id.sac
    #     if not line.invoiced:
    #         if not account_id:
    #             if line.product_id:
    #                 account_id = line.product_id.property_account_income.id
    #                 if not account_id:
    #                     account_id = line.product_id.categ_id.property_account_income_categ.id
    #                 if not account_id:
    #                     raise UserError(_('Error!'),
    #                                     _('Please define income account for this product: "%s" (id:%d).') %
    #                                     (line.product_id.name, line.product_id.id,))
    #             else:
    #                 prop = self.env['ir.property'].get(
    #                     'property_account_income_categ', 'product.category')
    #                 account_id = prop and prop.id or False
    #         uosqty = self._get_line_qty()
    #         uom_id = self._get_line_uom()
    #         pu = 0.0
    #         if uosqty:
    #             pu = round(line.price_unit * line.product_uom_qty / uosqty,
    #                        self.env['decimal.precision'].precision_get('Product Price'))
    #         fpos = line.order_id.fiscal_position or False
    #         account_id = self.env[
    #             'account.fiscal.position'].map_account(fpos, account_id)
    #         if not account_id:
    #             raise UserError(
    #                 _('There is no Fiscal Position defined or Income category account defined for default properties of Product categories.'))
    #         res = {
    #             'name': line.name,
    #             'sequence': line.sequence,
    #             'hsn': hsn,
    #             'origin': line.order_id.name,
    #             'account_id': account_id,
    #             'price_unit': pu,
    #             'quantity': uosqty,
    #             'discount': line.discount,
    #             'uom_id': uom_id,
    #             'product_id': line.product_id.id or False,
    #             'invoice_line_tax_id': [(6, 0, [x.id for x in line.tax_id])],
    #             'account_analytic_id': line.order_id.project_id and line.order_id.project_id.id or False,
    #         }

    #     return res

    @api.multi
    def button_cancel(self, line_ids):
        if self.state == 'sale':
            self.write({'state': 'cancel'})
