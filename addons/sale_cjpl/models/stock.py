from odoo import fields, models, _


class stock_picking(models.Model):

    _inherit = "stock.picking"

    source_loc = fields.Many2one('stock.location', 'Source Location')
