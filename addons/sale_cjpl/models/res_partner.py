# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import api, fields, models, _
from odoo.exceptions import UserError, ValidationError


class Partner(models.Model):
    _inherit = 'res.partner'

    @api.multi
    def write(self, vals):
        if not self.user_has_groups('sale_cjpl.group_allow_customer_change'):
            print('res+++++++++', self)
            sale_obj = self.env['sale.order']
            invoice_obj = self.env['account.invoice']
            contract_obj = self.env['account.analytic.account']
            picking_obj = self.env['stock.picking']
        #create env of every obj
            flag = False
            sale_order = sale_obj.search(['|',('partner_id', 'in', self.ids),
                ('partner_id.parent_id', 'in', self.ids),
                ('state','in',['sale','done'])], limit=1)
            inv_order = invoice_obj.search(['|',('partner_id', 'in', self.ids),
                ('partner_id.parent_id', 'in', self.ids),('state','=','done'),
                ('type', 'in', ['out_invoice', 'out_refund'])], limit=1)
            contract_order = contract_obj.search(['|',('partner_id', 'in', self.ids),
                ('partner_id.parent_id', 'in', self.ids)], limit=1)
            stock_order = picking_obj.search(['|',('partner_id', 'in', self.ids),
                ('partner_id.parent_id', 'in', self.ids),('state','=','done')], limit=1)
            print('flag+++++++++++++++++',sale_order,inv_order,contract_order,stock_order)
            if sale_order:
                flag = True
            if inv_order:
                flag = True
            if contract_order:
                flag = True
            if stock_order:
                flag = True
            lst = ['gstn','name','state_id', 'city', 'country_id', 'street', 'street2']
            if flag:
                if 'child_ids' in vals:
                    if vals['child_ids'][0][0] == 1:
                        change_vals = vals['child_ids'][0][2].keys()
                        for l in lst:
                            if l in lst:
                                raise UserError(_("You can not Change Address"))
                key_lst = vals.keys()
                for l in key_lst:
                    if l in lst:
                        raise UserError(_("You can not Change Address"))
        result = super(Partner, self).write(vals)
        return result