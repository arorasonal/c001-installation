from odoo import api, fields, models, _


class bulk(models.Model):
    _name = "bulk"

    @api.multi
    def create_stock_move(self):
        picking_obj = self.env('stock.picking')
        stock_obj = self.env('stock.move')
        if context is None:
            context = {}
        res = {}
        location_list = []
        location_list1 = []
        s = ''
        for list_obj in self:

            if list_obj.bulk_lines:
                for val in list_obj.bulk_lines:
                    if val.qty > 0:
                        location_list.append(val.dest_loc.id)
                        s = val
                    else:
                        location_list = []
                        s = ' '
                        break
                if s != ' ':
                    location_list1 = list(set(location_list))
            if s != ' ':
                for val2 in location_list1:

                    pick_id = picking_obj.create(cr, uid, {'picking_type_id': list_obj.picking_type_id.id,
                                                           'source_loc': list_obj.source_location.id})
                    for val in list_obj.bulk_lines:

                        if val.dest_loc.id == val2:
                            ss = stock_obj.create(cr, uid, {'picking_id': pick_id, 'product_id': val.product_id.id, 'product_uom_qty': val.qty,
                                                            'location_id': list_obj.source_location.id, 'location_dest_id': val.dest_loc.id,
                                                            'picking_type_id': list_obj.picking_type_id.id, 'product_uom': val.uom_id.id,
                                                            'name': val.product_id.name})

        self.write({'code': True})
#             picking_obj.create(cr,uid,{'move_lines':ss,'picking_type_id':list_obj.picking_type_id.id})
        return res

    picking_type_id = fields.Many2one('stock.picking.type', 'Picking Type')
    date = fields.Date('Date')
    source_location = fields.Many2one('stock.location', 'Source Location')
    bulk_lines = fields.One2many('bulk.line', 'bulk_id', 'Stock Lines')
    code = fields.Boolean('Code')


class bulk_line(models.Model):
    _name = "bulk.line"
    product_id = fields.Many2one('product.product', 'Product')
    qty = fields.Integer('Qty')
    uom_id = fields.Many2one('product.uom', 'Uom')
    dest_loc = fields.Many2one('stock.location', 'Destination Location')
    bulk_id = fields.Many2one('bulk', 'Bulk Lines')
    code = fields.Boolean('Code')

    # _defaults = {
    #     'uom_id': 1,
    # }
