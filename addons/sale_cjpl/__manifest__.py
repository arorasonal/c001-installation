{

    'name': 'Sale Cjpl',
    'version': '1.0',
    'author': 'Apagen Solutions Pvt. Ltd.',
    'website': 'https://www.apagen.com',
    'depends': ['sale', 'sales_team', 'sale_crm'],
    'category': ' ',

    'description': '',
    'data': [
        'security/security_view.xml',
        'security/sale_security_rule.xml',
        'report/external_layout.xml',
        'report/sale_report.xml',
        'view/sale_view.xml',
        'report/sale_report_template.xml',
        # 'view/stock_view.xml',
        #'wizard/stock_transfer_details.xml',
        'wizard/bulk_view.xml',
        'wizard/stock_return_picking_view.xml',
    ],
    'demo_xml': [],
    'js': [],
    'css': [],

    'installable': True,
    'auto-installable': False,
    'active': False,
}
