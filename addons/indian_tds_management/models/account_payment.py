from odoo import models, fields, api, _


class account_payment(models.Model):
    _inherit = "account.payment"

    apply_tds = fields.Boolean(
        'Apply TDS')
    account_tax_id = fields.Many2one(
        'account.tax', domain=([('tax_group_id.name', '=', 'TDS')]), string="Account Tax Id")
    tds_amount = fields.Monetary('Actual TDS')
    expected_tds_amount = fields.Monetary("Expected TDS")
    amount_to_be_paid = fields.Float('Amount to be Paid')
    total_untaxed_amount = fields.Float()
    total_tax_amount = fields.Float()
    total_invoice_amount = fields.Float()

    @api.onchange('account_tax_id')
    def update_tds(self):
        for rec in self:
            if rec.apply_tds and rec.account_tax_id:
                rec.tds_amount = (rec.total_untaxed_amount *
                                  rec.account_tax_id.amount) / 100
                rec.expected_tds_amount = rec.tds_amount
                rec.amount = rec.amount_to_be_paid - rec.tds_amount
            else:
                rec.tds_amount = 0.0
                rec.expected_tds_amount = 0.0

    @api.onchange('tds_amount')
    def onchange_tds_amount(self):
        self.amount = self.amount_to_be_paid - self.tds_amount

    @api.one
    @api.depends('invoice_ids', 'amount', 'payment_date', 'currency_id')
    def _compute_payment_difference(self):
        res = super(account_payment, self)._compute_payment_difference()
        if self.apply_tds:
            if len(self.invoice_ids) == 0:
                return
            if self.invoice_ids[0].type in ['out_invoice', 'in_refund']:
                self.payment_difference = self._compute_total_invoices_amount() - self.amount - self.tds_amount
            else:
                self.payment_difference = self.amount + self.tds_amount - self._compute_total_invoices_amount()
        return res

    @api.onchange('apply_tds')
    def onchange_apply_tds(self):
        if not self.apply_tds:
            invoice_defaults = self.resolve_2many_commands(
                'invoice_ids', self.invoice_ids.ids)
            if invoice_defaults and len(invoice_defaults) == 1:
                invoice = invoice_defaults[0]
                self.amount = invoice.get('residual')

    @api.onchange('partner_id')
    def onchnage_partner_id(self):
        for rec in self:
            if not self._context.get('apply_tds'):
                partner_lst = []
                if rec.partner_id:
                    partner_lst.append(rec.partner_id.id)
                    partner_lst.append(rec.partner_id.child_ids.ids)
                    new_partner_lst = []
                    for i in partner_lst:
                        if isinstance(i, list):
                            new_partner_lst += i
                        else:
                            new_partner_lst.append(i)
                    total_untaxed_amount = 0.0
                    total_tax_amount = 0.0
                    amount_to_be_paid = 0.0
                    total_invoice_amount = 0.0
                    self._cr.execute('''SELECT amount_tax, residual, amount_untaxed, amount_total
                        FROM account_invoice WHERE partner_id in %s and state='open'
                        ''', [(tuple(new_partner_lst))])
                    for result in self._cr.dictfetchall():
                        total_untaxed_amount += result['amount_untaxed'] if result['amount_untaxed'] else 0
                        total_tax_amount += result['amount_tax'] if result['amount_tax'] else 0
                        amount_to_be_paid += result['residual'] if result['residual'] else 0
                        total_invoice_amount += result['amount_total'] if result['amount_total'] else 0
                    rec.total_untaxed_amount = total_untaxed_amount
                    rec.total_tax_amount = total_tax_amount
                    rec.amount_to_be_paid = amount_to_be_paid
                    rec.total_invoice_amount = total_invoice_amount

    @api.model
    def default_get(self, fields):
        rec = super(account_payment, self).default_get(fields)
        invoice_defaults = self.resolve_2many_commands(
            'invoice_ids', rec.get('invoice_ids'))
        if invoice_defaults and len(invoice_defaults) == 1:
            invoice = invoice_defaults[0]
            rec['amount_to_be_paid'] = invoice.get('residual')
            rec['total_tax_amount'] = invoice.get('amount_tax')
            rec['total_untaxed_amount'] = invoice.get('amount_untaxed')
        return rec

    @api.onchange('amount', 'currency_id')
    def _onchange_amount(self):
        res = super(account_payment, self)._onchange_amount()
        for invoice in self.invoice_ids:
            total_invoice_amount = invoice.amount_untaxed + \
                invoice.amount_tax - invoice.tds_amount
            payment_amount = self.amount
            percentage = (payment_amount / total_invoice_amount) * 100
            tds_amount = (invoice.amount_untaxed *
                          self.account_tax_id.amount) / 100
            self.tds_amount = (percentage / 100) * tds_amount
        return res

    @api.model
    def tds_move_line_get(self, move):
        res = []
        for tds in self:
            if tds.apply_tds and tds.payment_type == 'outbound':
                res.append({
                    'type': 'tds',
                    'name': 'TDS',
                    'journal_id': tds.journal_id.id,
                    # 'invoice_id': tds.invoice_ids.ids,
                    'move_id': move.id,
                    'debit': 0,
                    'credit': tds.tds_amount,
                    'partner_id': tds.partner_id.id,
                    'quantity': 1,
                    'account_id': tds.account_tax_id.account_id.id,
                    'payment_id': tds.id
                })
            elif tds.apply_tds and tds.payment_type == 'inbound':
                res.append({
                    'type': 'tds',
                    'name': 'TDS',
                    'journal_id': tds.journal_id.id,
                    # 'invoice_id': tds.invoice_ids.ids,
                    'move_id': move.id,
                    'debit': tds.tds_amount,
                    'credit': 0,
                    'partner_id': tds.partner_id.id,
                    'quantity': 1,
                    'account_id': tds.account_tax_id.account_id.id,
                    'payment_id': tds.id
                })
        return res

    # def _create_payment_entry(self, amount):
    #     """ Create a journal entry corresponding to a payment, if the payment references invoice(s) they are reconciled.
    #         Return the journal entry.
    #     """
    #     aml_obj = self.env['account.move.line'].with_context(check_move_validity=False)
    #     invoice_currency = False
    #     if self.invoice_ids and all([x.currency_id == self.invoice_ids[0].currency_id for x in self.invoice_ids]):
    #         #if all the invoices selected share the same currency, record the paiement in that currency too
    #         invoice_currency = self.invoice_ids[0].currency_id
    #     debit, credit, amount_currency, currency_id = aml_obj.with_context(date=self.payment_date).compute_amount_fields(amount, self.currency_id, self.company_id.currency_id, invoice_currency)

    #     move = self.env['account.move'].create(self._get_move_vals())

    #     # Write line corresponding to invoice payment
    #     if self.payment_type == 'outbound' and self.apply_tds:
    #         counterpart_aml_dict = self._get_shared_move_line_vals((debit + self.tds_amount), credit, amount_currency, move.id, False)
    #     elif self.payment_type == 'inbound' and self.apply_tds:
    #         counterpart_aml_dict = self._get_shared_move_line_vals(debit, self.amount_to_be_paid, amount_currency, move.id, False)
    #     else:
    #         counterpart_aml_dict = self._get_shared_move_line_vals(debit, credit, amount_currency, move.id, False)
    #     counterpart_aml_dict.update(self._get_counterpart_move_line_vals(self.invoice_ids))
    #     counterpart_aml_dict.update({'currency_id': currency_id})
    #     counterpart_aml = aml_obj.create(counterpart_aml_dict)
    #     if self.apply_tds:
    #         tds_move_line = self.tds_move_line_get(move)
    #         aml_obj.create(tds_move_line[0])

    #     # Reconcile with the invoices
    #     if self.payment_difference_handling == 'reconcile' and self.payment_difference:
    #         writeoff_line = self._get_shared_move_line_vals(0, 0, 0, move.id, False)
    #         amount_currency_wo, currency_id = aml_obj.with_context(date=self.payment_date).compute_amount_fields(self.payment_difference, self.currency_id, self.company_id.currency_id, invoice_currency)[2:]
    #         # the writeoff debit and credit must be computed from the invoice residual in company currency
    #         # minus the payment amount in company currency, and not from the payment difference in the payment currency
    #         # to avoid loss of precision during the currency rate computations. See revision 20935462a0cabeb45480ce70114ff2f4e91eaf79 for a detailed example.
    #         total_residual_company_signed = sum(invoice.residual_company_signed for invoice in self.invoice_ids)
    #         total_payment_company_signed = self.currency_id.with_context(date=self.payment_date).compute(self.amount, self.company_id.currency_id)
    #         if self.invoice_ids[0].type in ['in_invoice', 'out_refund']:
    #             amount_wo = total_payment_company_signed - total_residual_company_signed
    #         else:
    #             amount_wo = total_residual_company_signed - total_payment_company_signed
    #         # Align the sign of the secondary currency writeoff amount with the sign of the writeoff
    #         # amount in the company currency
    #         if amount_wo > 0:
    #             debit_wo = amount_wo
    #             credit_wo = 0.0
    #             amount_currency_wo = abs(amount_currency_wo)
    #         else:
    #             debit_wo = 0.0
    #             credit_wo = -amount_wo
    #             amount_currency_wo = -abs(amount_currency_wo)
    #         writeoff_line['name'] = self.writeoff_label
    #         writeoff_line['account_id'] = self.writeoff_account_id.id
    #         writeoff_line['debit'] = debit_wo
    #         writeoff_line['credit'] = credit_wo
    #         writeoff_line['amount_currency'] = amount_currency_wo
    #         writeoff_line['currency_id'] = currency_id
    #         writeoff_line = aml_obj.create(writeoff_line)
    #         if counterpart_aml['debit'] or writeoff_line['credit']:
    #             counterpart_aml['debit'] += credit_wo - debit_wo
    #         if counterpart_aml['credit'] or writeoff_line['debit']:
    #             counterpart_aml['credit'] += debit_wo - credit_wo
    #         counterpart_aml['amount_currency'] -= amount_currency_wo

    #     # Write counterpart lines
    #     if not self.currency_id.is_zero(self.amount):
    #         if not self.currency_id != self.company_id.currency_id:
    #             amount_currency = 0
    #         liquidity_aml_dict = self._get_shared_move_line_vals(credit, debit, -amount_currency, move.id, False)
    #         liquidity_aml_dict.update(self._get_liquidity_move_line_vals(-amount))
    #         aml_obj.create(liquidity_aml_dict)

    #     # validate the payment
    #     move.post()

    #     # reconcile the invoice receivable/payable line(s) with the payment
    #     self.invoice_ids.register_payment(counterpart_aml)

    #     return move
