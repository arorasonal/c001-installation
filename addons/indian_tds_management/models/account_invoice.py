from odoo import api, fields, models, _
from odoo.exceptions import UserError


class AccountInvoice(models.Model):
    _inherit = "account.invoice"

    apply_tds = fields.Boolean(
        'Apply TDS')
    account_tax_id = fields.Many2one(
        'account.tax', domain=([('tax_group_id.name', '=', 'TDS')]))
    tds_amount = fields.Monetary(
        'TDS Amount')
    analytic_account_id = fields.Many2one('account.analytic.account')

    @api.onchange('account_tax_id')
    def onchange_account_tax_id(self):
        self.tds_amount = (self.amount_untaxed *
                           self.account_tax_id.amount) / 100

    def update_tds(self):
        for rec in self:
            if rec.apply_tds and rec.account_tax_id:
                rec.amount_total = rec.amount_untaxed + rec.amount_tax - rec.tds_amount
                # rec.residual = rec.rounded_total

    @api.multi
    def action_invoice_open(self):
        # lots of duplicate calls to action_invoice_open, so we remove those
        # already open
        res = super(AccountInvoice, self).action_invoice_open()
        self.update_tds()
        return res

    def _compute_amount(self):
        for res in self:
            round_curr = res.currency_id.round
            res.amount_untaxed = sum(
                line.price_subtotal for line in res.invoice_line_ids)
            res.amount_tax = sum(round_curr(line.amount)
                                 for line in res.tax_line_ids)

            res.rounded_total = round(
                res.amount_untaxed + res.amount_tax - res.tds_amount)
            res.amount_total = res.amount_untaxed + res.amount_tax
            res.round_off_value = res.rounded_total - \
                (res.amount_untaxed + res.amount_tax - res.tds_amount)
            amount_total_company_signed = res.amount_total
            amount_untaxed_signed = res.amount_untaxed
            if res.currency_id and res.company_id and res.currency_id != res.company_id.currency_id:
                currency_id = res.currency_id.with_context(
                    date=res.date_invoice)
                amount_total_company_signed = currency_id.compute(
                    res.amount_total, res.company_id.currency_id)
                amount_untaxed_signed = currency_id.compute(
                    res.amount_untaxed, res.company_id.currency_id)
            sign = res.type in ['in_refund', 'out_refund'] and -1 or 1
            res.amount_total_company_signed = amount_total_company_signed * sign
            res.amount_total_signed = res.amount_total * sign
            res.amount_untaxed_signed = amount_untaxed_signed * sign

    @api.model
    def tds_move_line_get(self):
        res = []
        for tds in self:
            if tds.apply_tds:
                res.append({
                    'type': 'tds',
                    'name': 'TDS',
                    'price_unit': tds.tds_amount,
                    'quantity': 1,
                    'price': -tds.tds_amount,
                    'account_id': tds.account_tax_id.account_id.id,
                    'account_analytic_id': tds.analytic_account_id.id,
                    'invoice_id': self.id,
                })
        return res

    @api.multi
    def action_move_create(self):
        """ Creates invoice related analytics and financial move lines """
        account_move = self.env['account.move']

        for inv in self:
            if not inv.journal_id.sequence_id:
                raise UserError(
                    _('Please define sequence on the journal related to this invoice.'))
            if not inv.invoice_line_ids:
                raise UserError(_('Please create some invoice lines.'))
            if inv.move_id:
                continue

            ctx = dict(self._context, lang=inv.partner_id.lang)

            if not inv.date_invoice:
                inv.with_context(ctx).write(
                    {'date_invoice': fields.Date.context_today(self)})
            if not inv.date_due:
                inv.with_context(ctx).write({'date_due': inv.date_invoice})
            company_currency = inv.company_id.currency_id

            # create move lines (one per invoice line + eventual taxes and
            # analytic lines)
            iml = inv.invoice_line_move_line_get()
            iml += inv.tax_line_move_line_get()
            iml += inv.tds_move_line_get()

            diff_currency = inv.currency_id != company_currency
            # create one move line for the total and possibly adjust the other
            # lines amount
            total, total_currency, iml = inv.with_context(
                ctx).compute_invoice_totals(company_currency, iml)

            name = inv.name or '/'
            if inv.payment_term_id:
                totlines = inv.with_context(ctx).payment_term_id.with_context(
                    currency_id=company_currency.id).compute(total, inv.date_invoice)[0]
                res_amount_currency = total_currency
                # ctx['date'] = inv._get_currency_rate_date()
                for i, t in enumerate(totlines):
                    if inv.currency_id != company_currency:
                        amount_currency = company_currency.with_context(
                            ctx).compute(t[1], inv.currency_id)
                    else:
                        amount_currency = False

                    # last line: add the diff
                    res_amount_currency -= amount_currency or 0
                    if i + 1 == len(totlines):
                        amount_currency += res_amount_currency
                    iml.append({
                        'type': 'dest',
                        'name': name,
                        'price': t[1],
                        'account_id': inv.account_id.id,
                        'date_maturity': t[0],
                        'amount_currency': diff_currency and amount_currency,
                        'currency_id': diff_currency and inv.currency_id.id,
                        'invoice_id': inv.id
                    })
            else:
                iml.append({
                    'type': 'dest',
                    'name': name,
                    'price': total,
                    'account_id': inv.account_id.id,
                    'date_maturity': inv.date_due,
                    'amount_currency': diff_currency and total_currency,
                    'currency_id': diff_currency and inv.currency_id.id,
                    'invoice_id': inv.id
                })

            part = self.env['res.partner']._find_accounting_partner(
                inv.partner_id)
            line = [(0, 0, self.line_get_convert(l, part.id)) for l in iml]
            line = inv.group_lines(iml, line)

            journal = inv.journal_id.with_context(ctx)
            line = inv.finalize_invoice_move_lines(line)

            date = inv.date or inv.date_invoice
            move_vals = {
                'ref': inv.reference,
                'line_ids': line,
                'journal_id': journal.id,
                'date': date,
                'narration': inv.comment,
            }
            ctx['company_id'] = inv.company_id.id
            ctx['invoice'] = inv
            ctx_nolang = ctx.copy()
            ctx_nolang.pop('lang', None)
            move = account_move.with_context(ctx_nolang).create(move_vals)
            # Pass invoice in context in method post: used if you want to get the same
            # account move reference when creating the same invoice after a
            # cancelled one:
            move.post()
            # make the invoice point to that move
            vals = {
                'move_id': move.id,
                'date': date,
                'move_name': move.name,
            }
            inv.with_context(ctx).write(vals)
        return True


class TDSRateChart(models.Model):
    _name = 'tds.rate.chart'

    section = fields.Char()
    nature_of_payment = fields.Char()
    limit_in = fields.Char()
    tds_rate_individual = fields.Float()
    domestic_company = fields.Float()
    start_date = fields.Date()
    end_date = fields.Date()
