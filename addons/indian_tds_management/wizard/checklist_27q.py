# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import models, fields
from dateutil.parser import parse
import xlwt
from xlwt import easyxf
from cStringIO import StringIO


class Wizard27QChecklist(models.TransientModel):
    _name = 'wizard.checklist27q'

    start_date = fields.Date('Start Date')
    end_date = fields.Date('End Date')
    employee_id = fields.Many2one('hr.employee', 'Employee')
    company_id = fields.Many2one('res.company', string='Company', default=lambda self: self.env.user.company_id)

    def print_report(self):
        import base64
        filename = '27Q Checklist Report.xls'
        workbook = xlwt.Workbook()
        style1_center = easyxf(
            'font: name Arial;'
            'alignment: horizontal  center;'
            'font:bold True'
        )
        style1_left = easyxf(
            'borders: top_color black, bottom_color black, right_color black, left_color black,\
                              left thin, right thin, top thin, bottom thin;'
            'font: name Arial;'
            'alignment: vertical  center;'
            'font:bold True,'
        )
        style1_left1 = easyxf(
            'font: name Arial;'
            'alignment: horizontal  left;'
        )

        worksheet = workbook.add_sheet('Sheet 1')
        zero_col = worksheet.col(0)
        first_col = worksheet.col(1)
        second_col = worksheet.col(2)
        third_col = worksheet.col(3)
        fourth_col = worksheet.col(4)
        fifth_col = worksheet.col(5)
        sixth_col = worksheet.col(6)
        seventh_col = worksheet.col(7)
        eleventh_col = worksheet.col(11)
        twelveth_col = worksheet.col(12)
        zero_col.width = 250 * 20
        first_col.width = 250 * 20
        second_col.width = 250 * 20
        third_col.width = 250 * 20
        fourth_col.width = 250 * 20
        fifth_col.width = 250 * 20
        sixth_col.width = 250 * 15
        seventh_col.width = 250 * 25
        eleventh_col.width = 250 * 25
        twelveth_col.width = 250 * 20
        worksheet.row(4).height_mismatch = True
        worksheet.row(4).height = 20 * 25

        dd1 = parse(self.start_date).strftime('%d/%m/%Y')
        dd2 = parse(self.end_date).strftime('%d/%m/%Y')
        worksheet.write_merge(
            1, 1, 0, 13, (self.company_id.name),
            style1_center
        )
        worksheet.write_merge(
            2, 2, 0, 13, ('TDS Entries Checklist for the period of %s To %s' % (dd1, dd2)), style1_center
        )
        worksheet.write_merge(
            3, 3, 0, 13, ('GSTIN/UIN: %s' % ('TIN')), style1_center
        )
        row = 4
        worksheet.write(row, 0, 'Serial No.', style1_left)
        worksheet.write(row, 1, 'Deduction\nDate', style1_left)
        worksheet.write(row, 2, 'Name', style1_left)
        worksheet.write(row, 3, 'Nature', style1_left)
        worksheet.write(row, 4, 'Amount Paid', style1_left)
        worksheet.write(row, 5, 'TDS Amount', style1_left)
        worksheet.write(row, 6, 'Surcharge', style1_left)
        worksheet.write(row, 7, 'Cess & Higher\nCess', style1_left)
        worksheet.write(row, 8, 'Total', style1_left)
        worksheet.write(row, 9, 'Rate(%)', style1_left)
        worksheet.write(row, 10, 'Deposited', style1_left)
        worksheet.write(row, 11, 'Balance to be\nDeposited', style1_left)
        worksheet.write(row, 12, 'Challan Date', style1_left)
        worksheet.write(row, 13, 'Challan No', style1_left)

        worksheet.write(10, 3, 'Total', style1_left)

        fp = StringIO()
        workbook.save(fp)
        export_id = self.env['checklist27q.excel'].create(
            {
                'excel_file': base64.encodestring(fp.getvalue()),
                'file_name': filename,
            }
        )
        fp.close()

        return {
            'view_mode': 'form',
            'res_id': export_id.id,
            'res_model': 'checklist27q.excel',
            'view_type': 'form',
            'type': 'ir.actions.act_window',
            'target': 'new',
        }


class Checklist27QReport(models.TransientModel):
    _name = "checklist27q.excel"

    excel_file = fields.Binary('Report')
    file_name = fields.Char('Excel File', size=64)

Checklist27QReport()
