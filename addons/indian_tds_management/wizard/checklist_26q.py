# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import models, fields
from dateutil.parser import parse
import xlwt
from xlwt import easyxf
from cStringIO import StringIO


class Wizard26QChecklist(models.TransientModel):
    _name = 'wizard.checklist26q'

    start_date = fields.Date('Start Date')
    end_date = fields.Date('End Date')
    company_id = fields.Many2one(
        'res.company', string='Company',
        default=lambda self: self.env.user.company_id)

    def print_report(self):
        import base64
        filename = '26Q Checklist Report.xls'
        workbook = xlwt.Workbook()
        style1_center = easyxf(
            'font: name Arial;'
            'alignment: horizontal center;'
            'font:bold True'
        )
        style1_left = easyxf(
            'borders: top_color black,bottom_color black,right_color black,\
                left_color black,left thin,right thin,top thin, bottom thin;'
            'font: name Arial;'
            'alignment: vertical center, horizontal center;'
            'font:bold True,'
        )
        style1_left1 = easyxf(
            'font: name Arial;'
            'alignment: horizontal center;'
        )

        worksheet = workbook.add_sheet('Sheet 1')
        zero_col = worksheet.col(0)
        first_col = worksheet.col(1)
        second_col = worksheet.col(2)
        third_col = worksheet.col(3)
        fourth_col = worksheet.col(4)
        fifth_col = worksheet.col(5)
        sixth_col = worksheet.col(6)
        seventh_col = worksheet.col(7)
        eleventh_col = worksheet.col(11)
        twelveth_col = worksheet.col(12)
        zero_col.width = 250 * 10
        first_col.width = 250 * 15
        second_col.width = 250 * 20
        third_col.width = 250 * 12
        fourth_col.width = 250 * 15
        fifth_col.width = 250 * 15
        sixth_col.width = 250 * 15
        seventh_col.width = 250 * 20
        eleventh_col.width = 250 * 20
        twelveth_col.width = 250 * 15
        worksheet.row(4).height_mismatch = True
        worksheet.row(4).height = 20 * 25

        dd1 = parse(self.start_date).strftime('%d/%m/%Y')
        dd2 = parse(self.end_date).strftime('%d/%m/%Y')
        payment_ids = self.env['account.payment'].search([
            ('payment_date', '>=', self.start_date),
            ('payment_date', '<=', self.end_date),
            ('partner_type', '=', 'supplier'),
            ('state', '=', 'posted'),
        ])
        worksheet.write_merge(
            1, 1, 0, 13, (self.company_id.name),
            style1_center
        )
        worksheet.write_merge(
            2, 2, 0, 13, ('TDS Entries Checklist for the period of %s To %s' %
                          (dd1, dd2)), style1_center
        )
        worksheet.write_merge(
            3, 3, 0, 13, ('GSTIN/UIN: %s' % (self.company_id.vat)), style1_center
        )
        row = 4
        worksheet.write(row, 0, 'Serial\nNo.', style1_left)
        worksheet.write(row, 1, 'Deduction\nDate', style1_left)
        worksheet.write(row, 2, 'Name', style1_left)
        worksheet.write(row, 3, 'Nature', style1_left)
        worksheet.write(row, 4, 'Amount Paid', style1_left)
        worksheet.write(row, 5, 'TDS Amount', style1_left)
        worksheet.write(row, 6, 'Surcharge', style1_left)
        worksheet.write(row, 7, 'Cess & Higher\nCess', style1_left)
        worksheet.write(row, 8, 'Total', style1_left)
        worksheet.write(row, 9, 'Rate(%)', style1_left)
        worksheet.write(row, 10, 'Deposited', style1_left)
        worksheet.write(row, 11, 'Balance to be\nDeposited', style1_left)
        worksheet.write(row, 12, 'Challan Date', style1_left)
        worksheet.write(row, 13, 'Challan No', style1_left)
        i = 1
        new_row = 5
        total_amnt_paid = 0
        total_tds_amnt = 0
        total_total = 0
        total_deposited = 0
        for payment in payment_ids:
            worksheet.write(new_row, 0, i, style1_left1)
            worksheet.write(new_row, 1, payment.payment_date, style1_left1)
            worksheet.write(new_row, 2, payment.partner_id.name, style1_left1)
            worksheet.write(new_row, 3, '', style1_left1)
            worksheet.write(new_row, 4, payment.amount, style1_left1)
            worksheet.write(new_row, 5, payment.tds_amount, style1_left1)
            worksheet.write(new_row, 6, '', style1_left1)
            worksheet.write(new_row, 7, '', style1_left1)
            worksheet.write(new_row, 8, payment.tds_amount, style1_left1)
            worksheet.write(new_row, 9, '', style1_left1)
            worksheet.write(new_row, 10, payment.tds_amount, style1_left1)
            worksheet.write(new_row, 11, '', style1_left1)
            worksheet.write(new_row, 12, '', style1_left1)
            worksheet.write(new_row, 13, '', style1_left1)
            total_amnt_paid += payment.amount
            total_tds_amnt += payment.tds_amount
            total_total += payment.tds_amount
            total_deposited += payment.tds_amount
            i += 1
            new_row += 1
        worksheet.write(new_row, 3, 'Total', style1_left)
        worksheet.write(new_row, 4, total_amnt_paid, style1_left)
        worksheet.write(new_row, 5, total_tds_amnt, style1_left)
        worksheet.write(new_row, 6, '', style1_left)
        worksheet.write(new_row, 7, '', style1_left)
        worksheet.write(new_row, 8, total_total, style1_left)
        worksheet.write(new_row, 9, '', style1_left)
        worksheet.write(new_row, 10, total_deposited, style1_left)
        worksheet.write(new_row, 11, '', style1_left)
        worksheet.write(new_row, 12, '', style1_left)
        worksheet.write(new_row, 13, '', style1_left)

        fp = StringIO()
        workbook.save(fp)
        export_id = self.env['checklist26q.excel'].create(
            {
                'excel_file': base64.encodestring(fp.getvalue()),
                'file_name': filename,
            }
        )
        fp.close()

        return {
            'view_mode': 'form',
            'res_id': export_id.id,
            'res_model': 'checklist26q.excel',
            'view_type': 'form',
            'type': 'ir.actions.act_window',
            'target': 'new',
        }


class Checklist26QReport(models.TransientModel):
    _name = "checklist26q.excel"

    excel_file = fields.Binary('Report')
    file_name = fields.Char('Excel File', size=64)

Checklist26QReport()
