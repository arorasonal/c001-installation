# -*- coding: utf-8 -*-
{
    'name': 'Indian TDS Management',
    'version': '1.1',
    'summary': 'Indian TDS Management',
    'author': 'Apagen Solutions Pvt. Ltd.',
    'website': 'https://www.apagen.com',
    'sequence': 30,
    'description': """
To calculate taxes with TDS
    """,
    'category': 'Accounting',
    'website': 'https://www.apagen.com',
    'depends': ['account'],
    'data': [
        'data/tax_groups.xml',
        'views/account_invoice_view.xml',
        'views/account_payment_view.xml',
        'wizard/checklist_26q.xml',
        'wizard/checklist_27q.xml',
    ],
    'installable': True,
    'application': False,
    'auto_install': False,
}
