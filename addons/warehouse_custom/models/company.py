import os
import re

from odoo import api, fields, models, tools, _
from odoo.exceptions import ValidationError


class Company(models.Model):
    _inherit = "res.company"
    
    code = fields.Char('Value for Serial  No.',track_visibility="onchange")
    sez_declaration = fields.Char('SEZ Declaration',track_visibility="onchange")
