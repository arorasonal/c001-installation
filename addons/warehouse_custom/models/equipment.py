import re

from odoo import api, fields, models, tools, _
from odoo.exceptions import ValidationError
from odoo.osv import expression
from odoo.exceptions import UserError, ValidationError
import odoo.addons.decimal_precision as dp

class equipment(models.Model):
    _name="equipment"
    _inherit = ['mail.thread', 'ir.needaction_mixin']
    
    @api.model
    def create(self,vals):
        if vals.get('code'):
            s = len(vals['code'])
            if s != 3:
                raise UserError(_('Value for Sl No. must be of 3 digits.'))
        if vals.get('sequence_id'):
            eq = self.env['equipment'].search([('sequence_id','=',vals['sequence_id'])])
            if eq:
                raise UserError(_('You cannot use same reference sequence.'))
        equipment_id =  super(equipment,self).create(vals)
        return equipment_id
    
    @api.multi
    def write(self,vals):
        spec = self.env['equipment'].browse(self.id)
        if 'code' in vals and vals['code']:
            s = len(vals['code'])
            if s != 3:
                raise UserError(_('Value for Sl No. must be of 3 digits.'))
        if 'sequence_id' in vals and vals['sequence_id']:
            eq = self.env['equipment'].search([('sequence_id','=',vals['sequence_id'])])
            if eq:
                raise UserError(_('You cannot use same reference sequence.'))
        equipment_id =  super(equipment,self).write(vals)
        return equipment_id
    
    name = fields.Char('Equipment Name',track_visibility="onchange")
    code = fields.Char('Value for Serial No.',track_visibility="onchange")
    sequence_id = fields.Many2one('ir.sequence','Reference Sequence',track_visibility="onchange")
    
    _sql_constraints=[('unique_name','unique(name)','Name must be unique !')]