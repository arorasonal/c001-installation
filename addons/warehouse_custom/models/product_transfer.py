import logging

from odoo import api, fields, models
from odoo import tools, _
import time
import math
from time import gmtime, strftime
import odoo.addons.decimal_precision as dp
from datetime import datetime,timedelta,date
from odoo.tools import DEFAULT_SERVER_DATE_FORMAT,DEFAULT_SERVER_DATETIME_FORMAT
from odoo.exceptions import UserError, ValidationError
from odoo.modules.module import get_module_resource
class product_transfer(models.Model):
    _name="product.transfer"
    _inherit = ['mail.thread', 'ir.needaction_mixin']
    
    @api.model
    def create(self, vals):
        order = ' '
        seq = self.env['ir.sequence'].next_by_code('product_transfer')
        vals['name'] =  str(seq)
        product_transfer_id = super(product_transfer, self).create(vals)
        return product_transfer_id
    
    def action_mark(self):
        if self.state =="draft":
            self.write({'state':'mark'})
    
    def action_validate(self):
        p = []
        s = ''
        if self.product_serial_lines:
            for val in self.product_serial_lines:
                quant = self.env['stock.quant'].search([('location_id','=',self.source_location_id.id),c('lot_id','=',val.serial_id.id),('product_id','=',val.product_id.id)])
                if quant:
                    for val1 in quant:
                        if val1.location_id != self.destination_location_id:
                            val1.write({'location_id':self.destination_location_id.id})
                    s = self.env['stock.move'].create({'product_id':val.product_id.id,'product_uom_qty':val.serial_id.product_qty,
                                                   'product_uom':val.product_id.uom_id.id,
                                                   'name':val.product_id.name,'origin':self.name,
                                                   'location_id':self.source_location_id.id,'location_dest_id':self.destination_location_id.id})
                    stock_obj = self.env['stock.move'].browse(s.id)
                    p.append(val1.id)
                    s = stock_obj.write({'quant_ids':[(6,0,p)],'state':'done'})
    
    source_location_id = fields.Many2one('stock.location','Source Location')
    destination_location_id = fields.Many2one('stock.location','Destination Location')
    name = fields.Char('Name')
    order_id = fields.Many2one('sale.order','SO#',track_visibility="onchange")
    transaction_type_id = fields.Many2one('transaction.type','Transaction Type',track_visibility="onchange")
    state = fields.Selection([('draft','Draft'),('mark','Mark As Todo'),('validate','Validate')],'State',default="draft")
    date = fields.Date('Date',default=lambda *a: datetime.now(),copy=False,track_visibility="onchange")
    product_transfer_lines = fields.One2many('product.transfer.line','product_transfer_id','Product Tramsfer Lines')
    product_serial_lines = fields.One2many('product.serial.line','product_transfer_id','Product Serial Lines')
    
class product_transfer_line(models.Model):
    _name="product.transfer.line"
    _inherit = ['mail.thread', 'ir.needaction_mixin']
    
    @api.onchange('product_id')
    def _onchange_product_id(self):
        if self.product_id.tracking == 'serial':
            self.serial = True
     
    @api.onchange('barcode')
    def _onchange_barcode(self):
        if self.barcode:
            product = self.env['product.product'].search([('barcode','=',self.barcode)])
            if product:
                for val in product:
                    product_obj = self.env['product.product'].browse(val.id)
                    self.product_id = product_obj.id
                    if product_obj.tracking == 'serial':
#                         if product_obj.product_tmpl_id.categ_id == self.category_id
                        
                        self.serial = True
#                         self.short_name1 = product_obj.short_name_product
#             else:
#                 raise UserError(_('The Product does not belong to this category or may not exist.'))       
    
    product_transfer_id = fields.Many2one('product.transfer','Product Transfer')
    barcode = fields.Char('Barcode')
    product_id = fields.Many2one('product.product','Product')
    qty  = fields.Float('Qty')
    serial = fields.Boolean('Serial')
    
class product_serial_line(models.Model):
    _name="product.serial.line"
    
    product_transfer_id = fields.Many2one('product.transfer','Product Transfer')
    product_id = fields.Many2one('product.product','Product')
    serial_id = fields.Many2one('stock.production.lot','Serial No')
    
    
