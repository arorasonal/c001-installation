import itertools
import psycopg2

import odoo.addons.decimal_precision as dp

from odoo import api, fields, models, tools, _
from odoo.exceptions import ValidationError, except_orm


class ProductTemplate(models.Model):
    _inherit = "product.template"
    
    long_name = fields.Char('Detailed Specification',track_visibility="onchange")
    short_name = fields.Char('Short Name',track_visibility="onchange")
    current_name = fields.Char('Current Name',track_visibility="onchange")
    hsn_no = fields.Char('HSN NO',track_visibility="onchange")
    short_name_product = fields.Char('Short Name for Component in Bom',track_visibility="onchange")
    equipment_id = fields.Many2one('equipment','Equipment',track_visibility="onchange")
    short_alterable = fields.Char('Short name for Alterable Product')
    add_product_id = fields.Many2one('wiz.add.product','Product')
    product_alteration = fields.Char('Product Alteration')
    product_challan = fields.Char('Product for Printing in Challan')