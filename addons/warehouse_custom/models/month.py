import re

from odoo import api, fields, models, tools, _
from odoo.exceptions import ValidationError
from odoo.osv import expression
from odoo.exceptions import UserError, ValidationError
import odoo.addons.decimal_precision as dp

class month(models.Model):
    _name="month"
    _inherit = ['mail.thread', 'ir.needaction_mixin']
        
    name = fields.Char('Month Name',track_visibility="onchange")
    code = fields.Char('Value for Serial No.',track_visibility="onchange")
  
    
    _sql_constraints=[('unique_name','unique(name)','Name must be unique !')]