from datetime import datetime
from dateutil import relativedelta
import time

from odoo import api, fields, models, _
from odoo.addons import decimal_precision as dp
from odoo.addons.procurement.models import procurement
from odoo.exceptions import UserError
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT
from odoo.tools.float_utils import float_compare, float_round, float_is_zero


class StockPicking(models.Model):
    _inherit = "stock.picking"
    
    serial_number_lines = fields.One2many('serial.number.data','picking_id','Serial Number Data')
    
    def get_serial_data(self):
        res={}
        serial_list=[]
        if self.pack_operation_product_ids:
            for val in self.pack_operation_product_ids:
                if val.product_id.tracking ==  'serial':
                    serial_list.append({'product_id':val.product_id,
                                        'product_qty':val.serial_qty,
                                        'from_loc':val.from_loc,'to_loc':val.to_loc,
                                       })
        self.update({'serial_number_lines':serial_list,'serial_code':True})
        
    def _create_lots_for_picking(self):
        Lot = self.env['stock.production.lot']
        for pack_op_lot in self.mapped('pack_operation_ids').mapped('pack_lot_ids'):
            if not pack_op_lot.lot_id:
                lot = Lot.create({'name': pack_op_lot.lot_name, 'product_id': pack_op_lot.operation_id.product_id.id,
                                  'picking_no':pack_op_lot.operation_id.picking_id.name})
                pack_op_lot.write({'lot_id': lot.id})
        # TDE FIXME: this should not be done here
        self.mapped('pack_operation_ids').mapped('pack_lot_ids').filtered(lambda op_lot: op_lot.qty == 0.0).unlink()
    create_lots_for_picking = _create_lots_for_picking
    
    def generate_sequence(self):
        res={}
        company_code = ' '
        equipment_code = ' '
        if self.serial_number_lines:
            for val in self.serial_number_lines:
                seq_list = []
                t = 1
#                 s = 0
                while( t <= val.product_qty):
                    if not val.month_id:
                        raise UserError(_('Please fill the month first.'))
                    if not val.year_id:
                        raise UserError(_('Please fill the year first.'))
                    seq1 = self.env['ir.sequence'].browse(val.product_id.equipment_id.sequence_id.id).next_by_id()
                    seq = str(self.company_id.code or ' ')  + str(val.product_id.equipment_id.code or ' ') +str(seq1)+ str(val.month_id.code or ' ')+str(val.year_id.code or ' ')
                    seq_list.append(seq)
                    t  = t + 1
#                     if s == 0:
#                         s = 1
#                     elif s == 1:
#                         s = 0
                else:
                    stock_pack = self.env['stock.pack.operation'].search([('picking_id','=',self.id),('product_id','=',val.product_id.id)])
                    if stock_pack:
                        for each in seq_list:
                            self.env['stock.pack.operation.lot'].create({'operation_id':stock_pack.id,'lot_name':each})
                        for pack in stock_pack:
                            if pack.product_id.tracking != 'none':
                                pack.write({'qty_done': sum(pack.pack_lot_ids.mapped('qty'))})
        self.update({'sequence_code':True})
        
    
    @api.depends('picking_type_id')
    def _get_serial(self):
        for each in self:
            if each.picking_type_id:
                each.update({'serial':each.picking_type_id.serial})
                
    @api.depends('picking_type_id')
    def _get_attach_serial(self):
        for each in self:
            if each.picking_type_id:
                each.update({'attach_serial':each.picking_type_id.attach_serial})
              
        
    serial_code = fields.Boolean('Code',copy=False)
    sequence_code = fields.Boolean('Seq Code',copy=False)
    barcode_transfer_id = fields.Many2one('barcode.transfer','Product Transfer#')
    serial = fields.Boolean(compute='_get_serial',method=True,string="Serial No Data",type="Boolean",track_visibility="always")
    attach_serial = fields.Boolean(compute='_get_attach_serial',method=True,string="Attach Serial No.s",type="Boolean",track_visibility="always")
    
class serial_number_data(models.Model):
    _name="serial.number.data"
    
    picking_id = fields.Many2one('stock.picking','Stock Picking')
    year_id = fields.Many2one('year','Year')
    month_id = fields.Many2one('month','Month')
    from_loc = fields.Char('From')
    to_loc = fields.Char('To')
    product_id = fields.Many2one('product.product', 'Product', ondelete="cascade")
    product_qty = fields.Float('To Do', default=0.0, digits=dp.get_precision('Product Unit of Measure'), required=True)
