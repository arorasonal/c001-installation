# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import api, fields, models, _
from odoo.exceptions import UserError


class ProductionLot(models.Model):
    _inherit = 'stock.production.lot'
    
     
    picking_no = fields.Char('SEQ')
    alpha_serial_no = fields.Char('Alpha Serial No')
    oem_serial_no = fields.Char('OEM Serial No')
    oem_warranty_date = fields.Date('OEM Warranty Date')
    alteration_timestamp = fields.Datetime('Alteration Timestamp')
