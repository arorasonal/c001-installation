import re

from odoo import api, fields, models, tools, _
from odoo.exceptions import ValidationError
from odoo.osv import expression
from odoo.exceptions import UserError, ValidationError
import odoo.addons.decimal_precision as dp

class product_component_short_name(models.Model):
    _name="product.component.short.name"
    _inherit = ['mail.thread', 'ir.needaction_mixin']
        
    name = fields.Char('Name',track_visibility="onchange")
    category_id = fields.Many2one('product.category','Category',track_visibility="onchange")
  
    
    _sql_constraints=[('unique_name','unique(name,category_id)','(Name + Category)must be unique !')]