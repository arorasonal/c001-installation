import logging

from odoo import api, fields, models
from odoo import tools, _
import time
import math
import odoo.addons.decimal_precision as dp
from datetime import datetime,timedelta
from odoo.tools import DEFAULT_SERVER_DATE_FORMAT,DEFAULT_SERVER_DATETIME_FORMAT
from odoo.exceptions import UserError, ValidationError
from odoo.modules.module import get_module_resource


class wiz_add_product(models.Model):
    _name="wiz.add.product"
    _inherit = ['mail.thread', 'ir.needaction_mixin']

# This function is for creating Add Product Record
    @api.model
    def create(self,vals):
        prefix =  ''
        short_name = ''
        long_name = ''
        long_name1 = ''
        category_id = ''
        category_id1 = ''
        categ_list = []
        categ_list1 = []
        seq_list = []
        seq_list1 = []
        category_list1 = []
        category_list = []
        template_list = []
        pro = []
        pro_seq_list = []
        pro_seq_list1 = []
        pro_seq_list2 = []
        prod = []
        done = []
        seq = self.env['ir.sequence'].next_by_code('add_product')
        vals['name'] = str(seq)
        product_id =  super(wiz_add_product,self).create(vals)
        product_obj = self.env['wiz.add.product'].browse(product_id.id)
        if vals.get('category_id'):
            category_id = vals.get('category_id')
        prefix = ''
        while(category_id != ''):
            category_obj = self.env['product.category'].browse(category_id)
            if category_obj.parent_id:
                category_id = category_obj.parent_id.id
		if category_obj.prefix:
		        if prefix == '':
		            prefix = category_obj.prefix
		        else:
		            prefix = category_obj.prefix + prefix
		else:
			prefix = prefix
            else:
		if category_obj.prefix:
		        category_id = ''
		        if prefix == '':
		            prefix = category_obj.prefix
		        else:
		            prefix = category_obj.prefix + prefix
		else:
			prefix = prefix
        if prefix == '':
            short_name = ''
        else:
            short_name = prefix
        if vals.get('add_product_lines'):
            product_list = self.env['add.product.line'].search([('wiz_add_product_id','=',product_obj.id)])
            for each1 in product_list:
                seq_list.append(each1.seq_no)
            if seq_list:
                seq_list1 = list(set(seq_list))
            if seq_list1:
                seq_list2 = seq_list1.sort()
            if seq_list1:
                for each43 in seq_list1:
                    product_list = self.env['add.product.line'].search([('wiz_add_product_id','=',product_obj.id),('seq_no','=',each43)])
                    if product_list:
                        for each1 in product_list:
                            if each1.short_name:
                                if short_name == '':
                                    short_name = each1.short_name
                                else:
                                    short_name = short_name + each1.short_name
                            else:
                                short_name = short_name
                            if each1.valid_value:
                                if long_name == '':
                                    long_name = each1.valid_value.name
                                else:
                                    long_name = long_name + '-' + each1.valid_value.name
                            else:
                                long_name = long_name
            product_obj.write({'short_name':short_name,'long_name':long_name})
        if vals.get('category_id1'):
            category_id = vals.get('category_id1')
        prefix = ''
        while(category_id != ''):
            category_obj = self.env['product.category'].browse(category_id)
            if category_obj.parent_id:
                category_id = category_obj.parent_id.id
                if category_obj.prefix:
                    if prefix == '':
                        prefix = category_obj.prefix
                    else:
                        prefix = category_obj.prefix + prefix
                else:
                    prefix = prefix
            else:
                if category_obj.prefix:
                    category_id = ''
                    if prefix == '':
                        prefix = category_obj.prefix
                    else:
                        prefix = category_obj.prefix + prefix
                else:
                    prefix = prefix
        if prefix == '':
            short_name = ' '
        else:
            short_name = prefix
        if vals.get('category_template_lines'):
            product_list = self.env['category.template.line'].search([('wiz_add_product_id','=',product_obj.id)])
            for each1 in product_list:
                seq_list.append(each1.seq_no)
            if seq_list:
                seq_list1 = list(set(seq_list))
            if seq_list1:
                seq_list2 = seq_list1.sort()
            if seq_list1:
                for each43 in seq_list1:
                    product_list = self.env['category.template.line'].search([('wiz_add_product_id','=',product_obj.id),('seq_no','=',each43)])
                    if product_list:
                        for each1 in product_list:
                            if each1.id not in done:
                                new = ''
                                new1 = ''
                                new2 = ''
                                pro = self.env['category.template.line'].search([('wiz_add_product_id','=',product_obj.id),('category_id','=',each1.category_id.id)])
                                if len(pro) > 1:
                                    for val in pro:
                                        pro_seq_list.append(val.seq_no)
                                    if pro_seq_list:
                                        pro_seq_list1 = list(set(pro_seq_list))
                                    if pro_seq_list1:
                                        pro_seq_list2 = pro_seq_list1.sort()
                                    if pro_seq_list1:
                                        for val in pro_seq_list1:
                                            prod_list = self.env['category.template.line'].search([('wiz_add_product_id','=',product_obj.id),('seq_no','=',val),('category_id','=',each1.category_id.id)])
                                            if prod_list:
                                                for each in prod_list:
                                                    if each.id not in done:
                                                        if each.qty > 1:
                                                            if not each.short_name:
                                                                new  = short_name
                                                                if each.product_id:
                                                                    long_name = str(long_name) + '[' + str(each.product_id.id) + ']'
                                                                    long_name1 = str(long_name1) + '-' +str(each.product_id.name)
                                                                else:
                                                                    long_name = long_name
                                                                    long_name1 = long_name1
                                                                done.append(each.id)
                                                            else:
                                                                if not new:
                                                                    if each.part_name == True:
                                                                        new = str(each.short_name) + '*' + str(each.qty)
                                                                        done.append(each.id)
                                                                    else:
                                                                        done.append(each.id)
                                                                else:
                                                                    if each.part_name == True:
                                                                        new = str(new) + '+' + str(each.short_name) + '*' + str(each.qty)
                                                                        done.append(each.id)
                                                                    else:
                                                                        done.append(each.id)
                                                                if not new1:
                                                                    if each.product_id:
                                                                        new1 = '[' + str(each.product_id.id) +']' + str(each.short_name)  + '*' + str(each.qty)
                                                                    else:
                                                                        new1 = str(each.short_name)  + '*' + str(each.qty)
                                                                else:
                                                                    if each.product_id:
                                                                        new1 = str(new1) + '+' + '[' + str(each.product_id.id) + ']' + str(each.short_name)  + '*' + str(each.qty)
                                                                    else:
                                                                        new1 = str(new1) + '+' + str(each.short_name)  + '*' + str(each.qty)
                                                                if not new2:
                                                                    if each.product_id:
                                                                        new2 = str(each.product_id.name)
                                                                    else:
                                                                        new2 = str(new2)
                                                                else:
                                                                    if each.product_id:
                                                                        new2 = str(new2) + '-' + str(each.product_id.name)
                                                                    else:
                                                                        new2  = str(new2)
                                                        else:
                                                            if not each.short_name:
                                                                new  = short_name
                                                                if each.product_id:
                                                                    long_name = str(long_name) + '[' + str(each.product_id.id) + ']'
                                                                    long_name1 = str(long_name1) + '-' + str(each.product_id.name)
                                                                else:
                                                                    long_name = long_name
                                                                    long_name1 = long_name1
                                                                done.append(each.id)
                                                            else:
                                                                if not new:
                                                                    if each.part_name == True:
                                                                        new = str(each.short_name) 
                                                                        done.append(each.id)
                                                                else:
                                                                    if each.part_name == True:
                                                                        new = str(new) + '+' + str(each.short_name)
                                                                        done.append(each.id)
                                                                if not new1:
                                                                    if each.product_id:
                                                                        new1 = '[' + str(each.product_id.id) + ']' + str(each.short_name) 
                                                                    else:
                                                                        new1 = str(each.short_name)  
                                                                else:
                                                                    if each.product_id:
                                                                        new1 = str(new1) + '+' + '[' + str(each.product_id.id) + ']' + str(each.short_name)
                                                                    else:
                                                                        new1 = str(new1) + '+' +  str(each.short_name)
                                                                if not new2:
                                                                    if each.product_id:
                                                                        new2 = str(each.product_id.name)
                                                                    else:
                                                                        new2 = str(new2)
                                                                else:
                                                                    if each.product_id:
                                                                        new2 = str(new2) + '-' + str(each.product_id.name)
                                                                    else:
                                                                        new2 = str(new2)
                                    short_name = str(short_name) + str(new)
                                    if not long_name:
                                        long_name = str(new1)
                                    else:
                                        long_name  = str(long_name) +'-'+ str(new1)  
                                    if not long_name1:
                                        long_name1 = str(new2)
                                    else:
                                        long_name1 = str(long_name1) + '-' + str(new2)
                                else:
                                    if each1.qty > 1:
                                        if each1.short_name:
                                            if short_name == '':
                                                if each1.part_name == True:
                                                    short_name = str(each1.short_name)  + '*' +  str(each1.qty)
                                                    done.append(each1.id)
                                                else:
                                                    done.append(each1.id)
                                            else:
                                                if each1.part_name == True:
                                                    short_name = str(short_name) + str(each1.short_name) + '*' + str(each1.qty)
                                                    done.append(each1.id)
                                                else:
                                                    done.append(each1.id)
                                                
                                            if long_name == '':
                                                if each1.product_id:
                                                    long_name = '[' + str(each1.product_id.id) + ']' + str(each1.short_name) + '*' + str(each1.qty)
                                                else:
                                                    long_name =  str(each1.short_name) + '*' + str(each1.qty)
                                            else:
                                                if each1.product_id:
                                                    long_name = str(long_name) +'-'+  '[' + str(each1.product_id.id) + ']' + str(each1.short_name)  + '*' + str(each1.qty)
                                                else:
                                                    long_name = str(long_name) +'-'+  str(each1.short_name)  + '*' + str(each1.qty)
                                            if long_name1 == '':
                                                if each1.product_id:
                                                    long_name1 = str(each1.product_id.name)
                                                else:
                                                    long_name1 = str(long_name1)
                                            else:
                                                if each1.product_id:
                                                    long_name1 = str(long_name1) + '-' + str(each1.product_id.name)
                                                else:
                                                    long_name1 = str(long_name1)
                                        else:
                                            short_name = short_name
                                            if each1.product_id:
                                                long_name = str(long_name) + '[' + str(each1.product_id.id) + ']'
                                                long_name1 = str(long_name1) + '-' + str(each1.product_id.name)
                                            else:
                                                long_name = long_name
                                                long_name1 = long_name1
                                            done.append(each1.id)
                                    else:
                                        if each1.short_name:
                                            if short_name == '':
                                                if each1.part_name == True:
                                                    short_name = each1.short_name
                                                    done.append(each1.id)
                                                else:
                                                    done.append(each1.id)
                                            else:
                                                if each1    .part_name == True:
                                                    short_name = short_name + each1.short_name
                                                    done.append(each1.id)
                                                else:
                                                    done.append(each1.id)
                                            if long_name == '':
                                                if each1.product_id:
                                                    long_name = '[' + str(each1.product_id.id) + ']' + str(each1.short_name)
                                                else:
                                                    long_name =  str(each1.short_name)
                                            else:
                                                if each1.product_id:
                                                    long_name = str(long_name) + '-'+ '[' + str(each1.product_id.id) + ']' + str(each1.short_name)
                                                else:
                                                    long_name = str(long_name) + '-'+ str(each1.short_name)
                                            if long_name1 == '':
                                                if each1.product_id:
                                                    long_name1  = str(each1.product_id.name)
                                                else:
                                                    long_name1  = str(long_name1)
                                            else:
                                                if each1.product_id:
                                                    long_name1 = str(long_name1) + '-' + str(each1.product_id.name)
                                                else:
                                                    long_name1 = str(long_name1) 
                                        else:
                                            short_name = short_name
                                            if each1.product_id:
                                                long_name = str(long_name) + '[' + str(each1.product_id.id) + ']'
                                                long_name1 = str(long_name1) + '-' + str(each1.product_id.name)
                                            else:
                                                long_name = long_name
                                                long_name1 = long_name1
                                            done.append(each1.id)
            product_obj.update({'short_name':short_name,'long_name':long_name,'long_name1':long_name1})
        return product_id
 # This function is for modifying Add Product Record    
    @api.multi
    def write(self,vals):
        short_name = ''
        long_name = ''
        long_name1 = ''
        category_id = ''
        prefix = ''
        categ_list = []
        catreg_list1 = []
        seq_list1 = []
        seq_list= []
        seq_list2 = []
        pro = []
        pro_seq_list = []
        pro_seq_list1 = []
        pro_seq_list2 = []
        prod = []
        done = []
        product_obj = self.env['wiz.add.product'].browse(self.id)
        product_id =  super(wiz_add_product,self).write(vals)
        if product_obj.category_id:
             category_id = product_obj.category_id.id
        prefix = ''
        while(category_id != ''):
            category_obj = self.env['product.category'].browse(category_id)
            if category_obj.parent_id:
                category_id = category_obj.parent_id.id
		if category_obj.prefix:
		        category_id = category_obj.parent_id.id
		        if prefix == '':
		            prefix = category_obj.prefix
		        else:
		            prefix = category_obj.prefix + prefix
		else:
			prefix= prefix
            else:
		if category_obj.prefix:
		        category_id = ''
		        if prefix == '':
		            prefix = category_obj.prefix
		        else:
		            prefix = category_obj.prefix + prefix
		else:
			prefix  = prefix
        if prefix == '':
            short_name = ''
        else:
            short_name = prefix
        if 'add_product_lines' in vals and vals['add_product_lines']:
            product_list = self.env['add.product.line'].search([('wiz_add_product_id','=',product_obj.id)])
            for each1 in product_list:
                seq_list.append(each1.seq_no)
            if seq_list:
                seq_list1 = list(set(seq_list))
            if seq_list1:
                seq_list2 = seq_list1.sort()
            if seq_list1:
                for each43 in seq_list1:
                    product_list = self.env['add.product.line'].search([('wiz_add_product_id','=',product_obj.id),('seq_no','=',each43)])
                    if product_list:
                        for each1 in product_list:
                            if each1.short_name:
                                if short_name == '':
                                    short_name = each1.short_name
                                else:
                                    short_name = short_name + each1.short_name
                            else:
                                short_name = short_name
                            if each1.valid_value:
                                if long_name == '':
                                    long_name = each1.valid_value.name
                                else:
                                    long_name = long_name + '-' + each1.valid_value.name
                            else:
                                long_name = long_name
            product_obj.write({'short_name':short_name,'long_name':long_name})
        if product_obj.category_id1:
            category_id = product_obj.category_id1.id
        prefix = ''
        while(category_id != ''):
            category_obj = self.env['product.category'].browse(category_id)
            if category_obj.parent_id:
                category_id = category_obj.parent_id.id
                if category_obj.prefix:
                    category_id = category_obj.parent_id.id
                    if prefix == '':
                        prefix = category_obj.prefix
                    else:
                        prefix = category_obj.prefix + prefix
                else:
                    prefix= prefix
            else:
                if category_obj.prefix:
                    category_id = ''
                    if prefix == '':
                        prefix = category_obj.prefix
                    else:
                        prefix = category_obj.prefix + prefix
                else:
                    prefix  = prefix
        if prefix == '':
            short_name = ''
        else:
            short_name = prefix
        if 'category_template_lines' in vals and vals['category_template_lines']:
            product_list = self.env['category.template.line'].search([('wiz_add_product_id','=',product_obj.id)])
            for each1 in product_list:
                seq_list.append(each1.seq_no)
            if seq_list:
                seq_list1 = list(set(seq_list))
            if seq_list1:
                seq_list2 = seq_list1.sort()
            if seq_list1:
                for each43 in seq_list1:
                    product_list = self.env['category.template.line'].search([('wiz_add_product_id','=',product_obj.id),('seq_no','=',each43)])
                    if product_list:
                        for each1 in product_list:
                            if each1.id not in done:
                                new = ''
                                new1 = ''
                                new2 = ''
                                pro = self.env['category.template.line'].search([('wiz_add_product_id','=',product_obj.id),('category_id','=',each1.category_id.id)])
                                if len(pro) > 1:
                                    for val in pro:
                                        pro_seq_list.append(val.seq_no)
                                    if pro_seq_list:
                                        pro_seq_list1 = list(set(pro_seq_list))
                                    if pro_seq_list1:
                                        pro_seq_list2 = pro_seq_list1.sort()
                                    if pro_seq_list1:
                                        for val in pro_seq_list1:
                                            prod_list = self.env['category.template.line'].search([('wiz_add_product_id','=',product_obj.id),('seq_no','=',val),('category_id','=',each1.category_id.id)])
                                            if prod_list:
                                                for each in prod_list:
                                                    if each.id not in done:
                                                        if each.qty > 1:
                                                            if not each.short_name:
                                                                new  = short_name
                                                                if each.product_id:
                                                                    long_name = str(long_name) + '[' + str(each.product_id.id) + ']'
                                                                    long_name1 = str(long_name1) + '-' + str(each.product_id.name)
                                                                else:
                                                                    long_name = long_name
                                                                    long_name1 = long_name1
                                                                done.append(each.id)
                                                            else:
                                                                if not new:
                                                                    if each.part_name == True:
                                                                        new = str(each.short_name) + '*' + str(each.qty)
                                                                        done.append(each.id)
                                                                    else:
                                                                        done.append(each.id)
                                                                else:
                                                                    if each.part_name == True:
                                                                        new = str(new) + '+' + str(each.short_name) + '*' + str(each.qty)
                                                                        done.append(each.id)
                                                                    else:
                                                                        done.append(each.id)
                                                                if not new1:
                                                                    if each.product_id:
                                                                        new1 = '[' + str(each.product_id.id) + ']' +str(each.short_name)  + '*' + str(each.qty)
                                                                    else:
                                                                        new1 = str(each.short_name)  + '*' + str(each.qty)
                                                                else:
                                                                    if each.product_id:
                                                                        new1 = str(new1) + '+' + '[' + str(eah.product_id.id) + ']' + str(each.short_name)  + '*' + str(each.qty)
                                                                    else:
                                                                        new1 = str(new1) + '+' + str(each.short_name)  + '*' + str(each.qty)
                                                                if not new2:
                                                                    if each.product_id:
                                                                        new2 = str(each.product_id.name)
                                                                    else:
                                                                        new2 = str(new2)
                                                                else:
                                                                    if each.product_id:
                                                                        new2 = str(new2) + '-' + str(each.product_id.name)
                                                                    else:
                                                                        new2 = str(new2) 
                                                        else:
                                                            if not each.short_name:
                                                                new  = short_name
                                                                if each.product_id:
                                                                    long_name = str(long_name) + '[' + str(each.product_id.id) + ']'
                                                                    long_name1 = str(long_name1) + '-' + str(each.product_id.name)
                                                                else:
                                                                    long_name = long_name
                                                                    long_name1 = long_name1
                                                                done.append(each.id)
                                                            else:
                                                                if not new:
                                                                    if each.part_name == True:
                                                                        new = str(each.short_name) 
                                                                        done.append(each.id)
                                                                    else:
                                                                        done.append(each.id)
                                                                else:
                                                                    if each.part_name == True:
                                                                        new = str(new) + '+' + str(each.short_name) 
                                                                        done.append(each.id)
                                                                    else:
                                                                        done.append(each.id)
                                                                if not new1:
                                                                    if each.product_id:
                                                                        new1 = '[' + str(each.product_id.id) + ']' + str(each.short_name) 
                                                                    else:
                                                                        new1 = str(each.short_name) 
                                                                else:
                                                                    if each.product_id:
                                                                        new1 = str(new1) + '+' + '[' + str(each.product_id.id) + ']' + str(each.short_name)  
                                                                    else:
                                                                        new1 = str(new1) + '+' + str(each.short_name)  
                                                                if not new2:
                                                                    if each.product_id:
                                                                        new2 = str(each.product_id.name)
                                                                    else:
                                                                        new2 = str(new2)
                                                                else:
                                                                    if each.product_id:
                                                                        new2 = str(new2) + '-' + str(each.product_id.name)
                                                                    else:
                                                                        new2 = str(new2) 
                                    short_name = str(short_name) + str(new)   
                                    if not long_name:
                                        long_name = str(new1)
                                    else:
                                        long_name  = str(long_name) +'-'+ str(new1)  
                                    if not long_name1:
                                        long_name1 = str(new2)
                                    else:
                                        long_name1 = str(long_name1) + '-' + str(new2)      
                                else:
                                    if each1.qty > 1:
                                        if each1.short_name:
                                            if short_name == '':
                                                if each1.part_name == True:
                                                    short_name = str(each1.short_name)  + '*' +  str(each1.qty)
                                                    done.append(each1.id)
                                                else:
                                                    done.append(each1.id)
                                            else:
                                                if each1.part_name == True:
                                                    short_name = str(short_name) + str(each1.short_name) + '*' + str(each1.qty)
                                                    done.append(each1.id)
                                                else:
                                                    done.append(each1.id)
                                            if long_name == '':
                                                if each1.product_id:
                                                    long_name = '[' + str(each1.product_id.id) + ']' + str(each1.short_name) + '*' + str(each1.qty)
                                                else:
                                                    long_name = str(each1.short_name) + '*' + str(each1.qty)
                                            else:
                                                if each1.product_id:
                                                    long_name = str(long_name) + '-' + '[' + str(each1.product_id.id) + ']' + str(each1.short_name)  + '*' + str(each1.qty)
                                                else:
                                                    long_name = str(long_name) + '-' + str(each1.short_name)  + '*' + str(each1.qty)
                                            if long_name1 == '':
                                                if each1.product_id:
                                                    long_name1 = str(each1.product_id.name)
                                                else:
                                                    long_name1 = str(long_name1)
                                            else:
                                                if each1.product_id:
                                                    long_name1 = str(long_name1) + '-' + str(each1.product_id.name)
                                                else:
                                                    long_name1 = str(long_name1) 
                                        else:
                                            short_name = short_name
                                            if each1.product_id:
                                                long_name = str(long_name) + '[' + str(each1.product_id.id) + ']'
                                                long_name1 = str(long_name1) + '-' + str(each1.product_id.name)
                                            else:
                                                long_name = long_name
                                                long_name1 = long_name1
                                            done.append(each1.id)
                                    else:
                                        if each1.short_name:
                                            if short_name == '':
                                                if each1.part_name == True:
                                                    short_name = each1.short_name
                                                    done.append(each1.id)
                                                else:
                                                    done.append(each1.id)
                                            else:
                                                if each1.part_name == True:
                                                    short_name = short_name + each1.short_name
                                                    done.append(each1.id)
                                                else:
                                                    done.append(each1.id)
                                            if long_name == '':
                                                if each1.product_id:
                                                    long_name = '[' + str(each1.product_id.id) + ']' + str(each1.short_name)
                                                else:
                                                    long_name =  str(each1.short_name) 
                                            else:
                                                if each1.product_id:
                                                    long_name = str(long_name) + '-' + '[' + str(each1.product_id.id) + ']' + str(each1.short_name)
                                                else:
                                                    long_name = str(long_name) + '-' + str(each1.short_name)
                                            if long_name1 == '':
                                                if each1.product_id:
                                                    long_name1 = str(each1.product_id.name)
                                                else:
                                                    long_name1 = str(long_name1)
                                            else:
                                                if each1.product_id:
                                                    long_name1 = str(long_name1) + '-' + str(each1.product_id.name)
                                                else:
                                                    long_name1 = str(long_name1)
                                        else:
                                            short_name = short_name
                                            if each1.product_id:
                                                long_name = str(long_name) + '[' + str(each1.product_id.id) + ']'
                                                long_name1 = str(long_name1) + '-' + str(each1.product_id.name)
                                            else:
                                                long_name = long_name
                                                long_name1 = long_name1
                                            done.append(each1.id)
            product_obj.write({'short_name':short_name,'long_name':long_name,'long_name1':long_name1})
#             self.action_calculate_price()
        return product_id
    # This function is for rename the products
    @api.multi
    def rename(self):
        short_name = ''
        long_name = ''
        long_name1 = ''
        category_id = ''
        prefix = ''
        categ_list = []
        catreg_list1 = []
        product_data =  {}
        prod_id = ''
        pro = []
        pro_seq_list = []
        pro_seq_list1 = []
        pro_seq_list2 = []
        prod = []
        done = []
        for item in self:
            short_name = ''
            long_name = ''
            long_name1 = ''
            category_id = ''
            prefix = ''
            categ_list = []
            catreg_list1 = []
            product_data =  {}
            prod_id = ''
            seq_list = []
            seq_list1 = []
            seq_list2 = []
            product_obj = self.env['wiz.add.product'].browse(item.id)
            if product_obj.category_id:
                 category_id = product_obj.category_id.id
            prefix = ''
            while(category_id != ''):
                category_obj = self.env['product.category'].browse(category_id)
                if category_obj.parent_id:
                    category_id = category_obj.parent_id.id
                    if category_obj.prefix:
                        category_id = category_obj.parent_id.id
                        if prefix == '':
                            prefix = category_obj.prefix
                        else:
                            prefix = category_obj.prefix + prefix
                    else:
                        prefix= prefix
                else:
                    if category_obj.prefix:
                        category_id = ''
                        if prefix == '':
                            prefix = category_obj.prefix
                        else:
                            prefix = category_obj.prefix + prefix
                    else:
                        prefix  = prefix
            if prefix == '':
                short_name = ''
            else:
                short_name = prefix
            if product_obj.add_product_lines:
                product_list = self.env['add.product.line'].search([('wiz_add_product_id','=',product_obj.id)])
                for each1 in product_list:
                    seq_list.append(each1.seq_no)
                if seq_list:
                    seq_list1 = list(set(seq_list))
                if seq_list1:
                    seq_list2 = seq_list1.sort()
                if seq_list1:
                    for each43 in seq_list1:
                        product_list = self.env['add.product.line'].search([('wiz_add_product_id','=',product_obj.id),('seq_no','=',each43)])
                        if product_list:
                            for each1 in product_list:
                                if each1.short_name:
                                    if short_name == '':
                                        short_name = each1.short_name
                                    else:
                                        short_name = short_name + each1.short_name
                                else:
                                    short_name = short_name
                                if each1.valid_value:
                                    if long_name == '':
                                        long_name = each1.valid_value.name
                                    else:
                                        long_name = long_name + '-' + each1.valid_value.name
                                else:
                                    long_name = long_name
                product_obj.write({'short_name':short_name,'long_name':long_name})
            if product_obj.category_id1:
                category_id = product_obj.category_id1.id
            prefix = ''
            while(category_id != ''):
                category_obj = self.env['product.category'].browse(category_id)
                if category_obj.parent_id:
                    category_id = category_obj.parent_id.id
                    if category_obj.prefix:
                        category_id = category_obj.parent_id.id
                        if prefix == '':
                            prefix = category_obj.prefix
                        else:
                            prefix = category_obj.prefix + prefix
                    else:
                        prefix= prefix
                else:
                    if category_obj.prefix:
                        category_id = ''
                        if prefix == '':
                            prefix = category_obj.prefix
                        else:
                            prefix = category_obj.prefix + prefix
                    else:
                        prefix  = prefix
            if prefix == '':
                short_name = ''
            else:
                short_name = prefix
            if product_obj.category_template_lines:
                product_list = self.env['category.template.line'].search([('wiz_add_product_id','=',product_obj.id)])
                for each1 in product_list:
                    seq_list.append(each1.seq_no)
                if seq_list:
                    seq_list1 = list(set(seq_list))
                if seq_list1:
                    seq_list2 = seq_list1.sort()
                if seq_list1:
                    for each43 in seq_list1:
                        product_list = self.env['category.template.line'].search([('wiz_add_product_id','=',product_obj.id),('seq_no','=',each43)])
                        if product_list:
                            for each1 in product_list:
                                if each1.id not in done:
                                    new = ''
                                    new1 = ''
                                    new2 = ''
                                    pro = self.env['category.template.line'].search([('wiz_add_product_id','=',product_obj.id),('category_id','=',each1.category_id.id)])
                                    if len(pro) > 1:
                                        for val in pro:
                                            pro_seq_list.append(val.seq_no)
                                        if pro_seq_list:
                                            pro_seq_list1 = list(set(pro_seq_list))
                                        if pro_seq_list1:
                                            pro_seq_list2 = pro_seq_list1.sort()
                                        if pro_seq_list1:
                                            for val in pro_seq_list1:
                                                prod_list = self.env['category.template.line'].search([('wiz_add_product_id','=',product_obj.id),('seq_no','=',val),('category_id','=',each1.category_id.id)])
                                                if prod_list:
                                                    for each in prod_list:
                                                        if each.id not in done:
                                                            if each.qty > 1:
                                                                if not each.short_name:
                                                                    new  = short_name
                                                                    if each.product_id:
                                                                        long_name = str(long_name) + '[' + str(each.product_id.id) + ']'
                                                                        long_name1 = str(long_name1) + '-' + str(each.product_id.name)
                                                                    else:
                                                                        long_name = long_name
                                                                        long_name1 = long_name1
                                                                    done.append(each.id)
                                                                else:
                                                                    if not new:
                                                                        if each.part_name == True:
                                                                            new = str(each.short_name) + '*' + str(each.qty)
                                                                            done.append(each.id)
                                                                        else:
                                                                            done.append(each.id)
                                                                    else:
                                                                        if each.part_name == True:
                                                                            new = str(new) + '+' + str(each.short_name) + '*' + str(each.qty)
                                                                            done.append(each.id)
                                                                        else:
                                                                            done.append(each.id)
                                                                    if not new1:
                                                                        if each.product_id:
                                                                            new1 = '[' + str(each.product_id.id) + ']' +str(each.short_name)  + '*' + str(each.qty)
                                                                        else:
                                                                            new1 = str(each.short_name)  + '*' + str(each.qty)
                                                                    else:
                                                                        if each.product_id:
                                                                            new1 = str(new1) + '+' + '[' + str(each.product_id.id) + ']' + str(each.short_name)  + '*' + str(each.qty)
                                                                        else:
                                                                            new1 = str(new1) + '+'  + str(each.short_name)  + '*' + str(each.qty)
                                                                    if not new2:
                                                                        if each.product_id:
                                                                            new2 = str(each.product_id.name)
                                                                        else:
                                                                            new2 = str(new2)
                                                                    else:
                                                                        if each.product_id:
                                                                            new2 = str(new2) + '-' + str(each.product_id.name)
                                                                        else:
                                                                            new2 = str(new2) 
                                                            else:
                                                                if not each.short_name:
                                                                    new  = short_name
                                                                    if each.product_id:
                                                                        long_name = str(long_name) + '[' + str(each.product_id.id) + ']'
                                                                        long_name1 = str(long_name1) + '-' + str(each.product_id.name)
                                                                    else:
                                                                        long_name = long_name
                                                                        long_name1 = long_name1
                                                                    done.append(each.id)
                                                                else:
                                                                    if not new:
                                                                        if each.part_name == True:
                                                                            new = str(each.short_name) 
                                                                            done.append(each.id)
                                                                        else:
                                                                            done.append(each.id)
                                                                    else:
                                                                        if each.part_name == True:
                                                                            new = str(new) + '+' + str(each.short_name)
                                                                            done.append(each.id)
                                                                        else:
                                                                            done.append(each.id)
                                                                    if not new1:
                                                                        if each.product_id:
                                                                            new1 = ' [' + str(each.product_id.id) + ']' + str(each.short_name)  
                                                                        else:
                                                                            new1 = str(each.short_name)  
                                                                    else:
                                                                        if each.product_id:
                                                                            new1 = str(new1) + '+' + '[' + str(each.product_id.id)  + ']' + str(each.short_name) 
                                                                        else:
                                                                            new1 = str(new1) + '+' + str(each.short_name) 
                                                                    if not new2:
                                                                        if each.product_id:
                                                                            new2 = str(each.product_id.name)
                                                                        else:
                                                                            new2 = str(new2)
                                                                    else:
                                                                        if each.product_id:
                                                                            new2 = str(new2) +'-' + str(each.product_id.name)
                                                                        else:
                                                                            new2 = str(new2) 
                                        short_name = str(short_name) + str(new)
                                        if not long_name:
                                            long_name = str(new1)
                                        else:
                                            long_name  = str(long_name) +'-'+ str(new1)  
                                        if not long_name1:
                                            long_name1 = str(new2)
                                        else:
                                            long_name1 = str(long_name1) + '-' + str(new2) 
                                    else:
                                        if each1.qty > 1:
                                            if each1.short_name:
                                                if short_name == '':
                                                    if each1.part_name == True:
                                                        short_name = str(each1.short_name)  + '*' +  str(each1.qty)
                                                        done.append(each1.id)
                                                    else:
                                                        done.append(each1.id)
                                                else:
                                                    if each1.part_name == True:
                                                        short_name = str(short_name) + str(each1.short_name) + '*' + str(each1.qty)
                                                        done.append(each1.id)
                                                    else:
                                                        done.append(each1.id)
                                                if long_name == '':
                                                    if each1.product_id:
                                                        long_name =  '[ '+ str(eah1.product_id.id) + ']' + str(each1.short_name) + '*' + str(each1.qty)
                                                    else:
                                                        long_name = str(each1.short_name) + '*' + str(each1.qty)
                                                else:
                                                    if each1.product_id:
                                                        long_name = str(long_name) + '-' + '[' + str(each1.product_id.id) +'] ' + str(each1.short_name)  + '*' + str(each1.qty)
                                                    else:
                                                        long_name = str(long_name) + '-' +  str(each1.short_name)  + '*' + str(each1.qty)
                                                if long_name1:
                                                    if each1.product_id:
                                                        long_name1 = str(each1.product_id.name)
                                                    else:
                                                        long_name1 = str(long_name1)
                                                else:
                                                    if each1.product_id:
                                                        long_name1 = str(long_name1) + '-' + str(each1.product_id.name)
                                                    else:
                                                        long_name1 = str(long_name1) 
                                            else:
                                                short_name = short_name
                                                if each1.product_id:
                                                    long_name = str(long_name) + '[' + str(each1.product_id.id) + ']'
                                                    long_name1 = str(long_name1) + '-' + str(each1.product_id.name)
                                                else:
                                                    long_name = long_name
                                                    long_name1 = long_name1
                                                done.append(each1.id)
                                        else:
                                            if each1.short_name:
                                                if short_name == '':
                                                    if each1.part_name == True:
                                                        short_name = each1.short_name
                                                        done.append(each1.id)
                                                    else:
                                                        done.append(each1.id)
                                                else:
                                                    if each1.part_name == True:
                                                        short_name = short_name + each1.short_name
                                                        done.append(each1.id)
                                                    else:
                                                        done.append(each1.id)
                                                if long_name == '':
                                                    if each1.product_id:
                                                        long_name = '[' + str(each1.product_id.id) + ']' +  str(each1.short_name) 
                                                    else:
                                                        long_name = str(each1.short_name) 
                                                else:
                                                    if each1.product_id:
                                                        long_name = str(long_name) + '-' + '[' + str(each1.product_id.id) + ']' +  str(each1.short_name) 
                                                    else:
                                                        long_name = str(long_name) + '-' +   str(each1.short_name) 
                                                if long_name1 == '':
                                                    if each1.product_id:
                                                        long_name1 = str(each1.product_id.name)
                                                    else:
                                                        long_name1 = str(long_name1)
                                                else:
                                                    if each1.product_id:
                                                        long_name1 = str(long_name1) + '-' + str(each1.product_id.name)
                                                    else:
                                                        long_name1 = str(long_name1) 
                                            else:
                                                short_name = short_name
                                                if each1.product_id:
                                                    long_name = str(long_name) + '[' + str(each1.product_id.id) + ']'
                                                    long_name1 = str(long_name1) + '-' + str(each1.product_id.name)
                                                else:
                                                    long_name = long_name
                                                    long_name1 = long_name1
                                                done.append(each1.id)
                product_obj.write({'short_name':short_name,'long_name':long_name,'long_name1':long_name1})
            prod1 = ' '
            product_data = {}
#             if item.product_id:
#                 prod1 = self.env['product.template'].browse(item.product_id.product_tmpl_id.id)
#                 if prod1:
#                     if prod1.short_name == ' ' and prod1.hsn == ' ':
#                         short_name = item.short_name
#                         hsn = item.hsn_no
#                     elif prod1.short_name == ' ':
#                         short_name = item.short_name
#                         hsn = prod1.hsn_no
#                     elif prod1.hsn_no == ' ':
#                         hsn = item.hsn_no
#                         short_name = prod1.short_name
#                     else:
#                         short_name = prod1.short_name
#                         hsn = prod1.hsn_no
#                     if prod1.equipment_id:
#                         equipment = prod1.equipment_id.id
#                     else:
#                         equipment = item.equipment_id.id
#                     if product_obj.category_id:
#                         if product_obj.category_id.numbered == True:
#                             prod1.update({'name':item.short_name,
#                                           'product_alteration':item.long_name,
#                                                     'long_name':item.long_name1,
#                                                     'short_name':short_name,
#                                                     'categ_id':item.category_id.id,
#                                                     'tracking':'serial',
#                                                     'type':'product',
#                                                     'equipment_id':equipment,
#                                                     'short_name_product':item.short_name_product,
#                                                     'current_name':item.current_name,
#                                                     'hsn_no':hsn})
#                                
#                         else:
#                             prod1.update({'name':item.short_name,
#                                             'product_alteration':item.long_name,
#                                                     'long_name':item.long_name1,
#                                             'short_name':short_name,
#                                             'categ_id':item.category_id.id,
#                                             'tracking':'none',
#                                             'type':'product',
#                                             'equipment_id':equipment,
#                                             'short_name_product':item.short_name_product,
#                                             'current_name':item.current_name,
#                                             'hsn_no':hsn})
#                     elif product_obj.category_id1:
#                         if product_obj.category_id1.numbered == True:
#                             prod1.update({
#                                             'name':item.long_name,
#                                             'product_alteration':item.long_name,
#                                                     'long_name':item.long_name1,
#                                             'short_name':short_name,
#                                             'categ_id':item.category_id1.id,
#                                             'tracking':'serial',
#                                             'type':'product',
#                                             'equipment_id':equipment,
#                                             'short_name_product':item.short_name_product,
#                                             'current_name':item.current_name,
#                                             'hsn_no':hsn
#                                             })
#                         else:
#                             prod1.update({
#                                             'name':item.long_name,
#                                            'product_alteration':item.long_name,
#                                                     'long_name':item.long_name1,
#                                             'short_name':short_name,
#                                             'categ_id':item.category_id1.id,
#                                             'tracking':'none',
#                                             'type':'product',
#                                             'equipment_id':equipment,
#                                             'short_name_product':item.short_name_product,
#                                             'current_name':item.current_name,
#                                             'hsn_no':hsn
#                                             })
#             else:
            if item.short_name:
                prod1 = self.env['product.template'].search([('name','=',item.short_name)])
                prod_id1 = prod1
                print"bdfbdfv",prod_id1
                if prod1:
                    raise UserError(_('Product with the same name already exist'))
                elif item.product_id:
                    prod1 = self.env['product.template'].browse(item.product_id.product_tmpl_id.id)
                    prod_id1 = prod1
                    if prod1:
                        if prod1.short_name == ' ' and prod1.hsn == ' ':
                            short_name = item.short_name
                            hsn = item.hsn_no
                        elif prod1.short_name == ' ':
                            short_name = item.short_name
                            hsn = prod1.hsn_no
                        elif prod1.hsn_no == ' ':
                            hsn = item.hsn_no
                            short_name = prod1.short_name
                        else:
                            short_name = prod1.short_name
                            hsn = prod1.hsn_no
                        if prod1.equipment_id:
                            equipment = prod1.equipment_id.id
                        else:
                            equipment = item.equipment_id.id
                        if product_obj.category_id:
                            if product_obj.category_id.numbered == True:
                                prod1.update({'name':item.short_name,
                                              'product_alteration':item.long_name,
                                                        'long_name':item.long_name1,
                                                        'short_name':short_name,
                                                        'categ_id':item.category_id.id,
                                                        'tracking':'serial',
                                                        'type':'product',
                                                        'equipment_id':equipment,
                                                        'short_name_product':item.short_name_product,
                                                        'current_name':item.current_name,
                                                        'hsn_no':hsn})
                                    
                            else:
                                prod1.update({'name':item.short_name,
                                                'product_alteration':item.long_name,
                                                        'long_name':item.long_name1,
                                                'short_name':short_name,
                                                'categ_id':item.category_id.id,
                                                'tracking':'none',
                                                'type':'product',
                                                'equipment_id':equipment,
                                                'short_name_product':item.short_name_product,
                                                'current_name':item.current_name,
                                                'hsn_no':hsn})
#                         elif product_obj.category_id1:
#                             if product_obj.category_id1.numbered == True:
#                                 prod1.update({
#                                                 'name':item.long_name,
#                                                 'product_alteration':item.long_name,
#                                                         'long_name':item.long_name1,
#                                                 'short_name':short_name,
#                                                 'categ_id':item.category_id1.id,
#                                                 'tracking':'serial',
#                                                 'type':'product',
#                                                 'equipment_id':equipment,
#                                                 'short_name_product':item.short_name_product,
#                                                 'current_name':item.current_name,
#                                                 'hsn_no':hsn
#                                                 })
#                             else:
#                                 prod1.update({
#                                                 'name':item.long_name,
#                                                'product_alteration':item.long_name,
#                                                         'long_name':item.long_name1,
#                                                 'short_name':short_name,
#                                                 'categ_id':item.category_id1.id,
#                                                 'tracking':'none',
#                                                 'type':'product',
#                                                 'equipment_id':equipment,
#                                                 'short_name_product':item.short_name_product,
#                                                 'current_name':item.current_name,
#                                                 'hsn_no':hsn
#                                                 })
#                 elif product_obj.category_id:
#                     if product_obj.category_id.numbered == True:
#                         product_data = {
#                                                 'name':item.short_name,
#                                                'product_alteration':item.long_name,
#                                                     'long_name':item.long_name1,
#                                                 'short_name':item.short_name,
#                                                 'categ_id':item.category_id.id,
#                                                 'tracking':'serial',
#                                                 'type':'product',
#                                                 'equipment_id':item.equipment_id.id,
#                                                 'short_name_product':item.short_name_product,
#                                                 'current_name':item.current_name
#                                                 }
#                     else:
#                         product_data = {
#                                                 'name':item.short_name,
#                                                 'product_alteration':item.long_name,
#                                                     'long_name':item.long_name1,
#                                                 'short_name':item.short_name,
#                                                 'categ_id':item.category_id.id,
#                                                 'tracking':'none',
#                                                 'type':'product',
#                                                 'equipment_id':item.equipment_id.id,
#                                                 'short_name_product':item.short_name_product,
#                                                 'current_name':item.current_name
#                                                 }
#                 prod_id1 = self.env['product.template'].create(product_data)
            if item.long_name:
                prod1 = self.env['product.template'].search([('name','=',item.long_name)])
                prod_id1 = prod1
                print"bdfbdfv",prod_id1
                if prod1:
                    raise UserError(_('Product with the same name already exist'))
                else:
                    if item.product_id:
                        prod1 = self.env['product.template'].browse(item.product_id.product_tmpl_id.id)
                        prod_id1 = prod1
                        if prod1:
                            if prod1.short_name == ' ' and prod1.hsn == ' ':
                                short_name = item.short_name
                                hsn = item.hsn_no
                            elif prod1.short_name == ' ':
                                short_name = item.short_name
                                hsn = prod1.hsn_no
                            elif prod1.hsn_no == ' ':
                                hsn = item.hsn_no
                                short_name = prod1.short_name
                            else:
                                short_name = prod1.short_name
                                hsn = prod1.hsn_no
                            if prod1.equipment_id:
                                equipment = prod1.equipment_id.id
                            else:
                                equipment = item.equipment_id.id
#                             if product_obj.category_id:
#                                 if product_obj.category_id.numbered == True:
#                                     prod1.update({'name':item.short_name,
#                                                   'product_alteration':item.long_name,
#                                                             'long_name':item.long_name1,
#                                                             'short_name':short_name,
#                                                             'categ_id':item.category_id.id,
#                                                             'tracking':'serial',
#                                                             'type':'product',
#                                                             'equipment_id':equipment,
#                                                             'short_name_product':item.short_name_product,
#                                                             'current_name':item.current_name,
#                                                             'hsn_no':hsn})
#                                         
#                                 else:
#                                     prod1.update({'name':item.short_name,
#                                                     'product_alteration':item.long_name,
#                                                             'long_name':item.long_name1,
#                                                     'short_name':short_name,
#                                                     'categ_id':item.category_id.id,
#                                                     'tracking':'none',
#                                                     'type':'product',
#                                                     'equipment_id':equipment,
#                                                     'short_name_product':item.short_name_product,
#                                                     'current_name':item.current_name,
#                                                     'hsn_no':hsn})
                            if product_obj.category_id1:
                                if product_obj.category_id1.numbered == True:
                                    prod1.update({
                                                    'name':item.long_name,
                                                    'product_alteration':item.long_name,
                                                            'long_name':item.long_name1,
                                                    'short_name':short_name,
                                                    'categ_id':item.category_id1.id,
                                                    'tracking':'serial',
                                                    'type':'product',
                                                    'equipment_id':equipment,
                                                    'short_name_product':item.short_name_product,
                                                    'current_name':item.current_name,
                                                    'hsn_no':hsn
                                                    })
                                else:
                                    prod1.update({
                                                    'name':item.long_name,
                                                   'product_alteration':item.long_name,
                                                            'long_name':item.long_name1,
                                                    'short_name':short_name,
                                                    'categ_id':item.category_id1.id,
                                                    'tracking':'none',
                                                    'type':'product',
                                                    'equipment_id':equipment,
                                                    'short_name_product':item.short_name_product,
                                                    'current_name':item.current_name,
                                                    'hsn_no':hsn
                                                    })
                     
        
#             if item.product_id:
#                 prod_id = item.product_id
#             else:
            pro = self.env['product.product'].search([('product_tmpl_id','=',prod_id1.id)])
            if pro:
                for val in pro:
                    prod_id = val
            if product_obj.category_id.attribute_lines:
                for each2 in product_obj.category_id.attribute_lines:
                    value_list = []
                    attribute_list = self.env['product.attribute.value'].search([('attribute_id','=',each2.attribute_id.id)])
                    attr = self.env['product.attribute.line'].search([('attribute_id','=',each2.attribute_id.id),('product_tmpl_id','=',prod_id.product_tmpl_id.id)])
                    if attr:
                        for each1 in attribute_list:
                            value_list.append(each1.id)
                            attr.write({'value_ids':[(6,0,value_list)]})
                            prod_id.product_tmpl_id.create_variant_ids()
                    else:
                        for each1 in attribute_list:
                            value_list.append(each1.id)
                        self.env['product.attribute.line'].create({'product_tmpl_id':prod_id.product_tmpl_id.id,'attribute_id':each2.attribute_id.id,'value_ids':[(6,0,value_list)]})
                        prod_id.product_tmpl_id.create_variant_ids()
            elif product_obj.category_id1.attribute_lines:
                for each2 in product_obj.category_id1.attribute_lines:
                    value_list = []
                    attribute_list = self.env['product.attribute.value'].search([('attribute_id','=',each2.attribute_id.id)])
                    for each1 in attribute_list:
                        value_list.append(each1.id)
                    self.env['product.attribute.line'].create({'product_tmpl_id':prod_id.product_tmpl_id.id,'attribute_id':each2.attribute_id.id,'value_ids':[(6,0,value_list)]})
                    prod_id.product_tmpl_id.create_variant_ids()
            product_obj1 = ' '
            if prod_id:       
                item.write({'state':'product_created','product_id':prod_id.id})
                item.action_calculate_price()  
        
#     def refresh_category(self,ids,context=None):
#         if context is None:
#             context={}
#         if self.name == 'non-altered':
#             if self.category_id:
#                 specification = self.env['specification.line'].search([('category_id1','=',self.category_id.id)])
#                 if specification:
#                     for val in specification:
#                         if self.add_product_lines:
#                             for val2 in self.add_product_lines:
#                                 pro = self.env['add.product.line'].search([('wiz_add_product_id','=',self.id),('specification','=',val.name)])
#                                 if not pro:
#                                     self.env['add.product.line'].create({'wiz_add_product_id':self.id,'specification':val.name,
#                                                                          'seq_no':val.seq_no})
#         elif self.name == 'altered':
#             if self.category_id1:
#                 category = self.env['category.template'].search([('category_id1','=',self.category_id1.id)])
#                 if category:
#                     for val in category:
#                         if self.category_template_lines:
#                             for val2 in self.category_template_lines:
#                                 cate = self.env['category.template.line'].search([('wiz_add_product_id','=',self.id),('category_id','=',val.category_id.id)])
#                                 if not cate:
#                                     self.env['category.template.line'].create({'wiz_add_product_id':self.id,'category_id':val.category_id.id,
#                                                                                'seq_no':val.seq_no,'part_name':val.part_name,'mandatory':val.mandatory})
#                                     
#         return True
                                    
                            
                        
    # This function is for Creating the Product.
    
    def create_product(self,ids,context=None):
        if context is None:
            context = {}
        product_data = {}
        product_obj = self.env['wiz.add.product'].browse(self.id)
        prod_obj = self.env['product.template']
        for each in self:
            if each.short_name:
                prod1 = self.env['product.template'].search([('name','=',each.short_name)])
                if prod1:
                    raise UserError(_('Product with the same name already exist'))
                elif product_obj.category_id:
                    if product_obj.category_id.numbered == True:
                        product_data = {
                                        'name':each.short_name,
                                        'product_alteration':each.long_name,
                                        'long_name':each.long_name1,
                                        'short_name':each.short_name,
                                        'categ_id':each.category_id.id,
                                        'tracking':'serial',
                                        'hsn_no':each.hsn_no,
                                        'type':'product',
                                        'short_name_product':each.short_name_product,
                                        'current_name':each.current_name,
                                        'equipment_id':each.equipment_id.id,
                                        }
                    else:
                        product_data = {
                                        'name':each.short_name,
                                        'product_alteration':each.long_name,
                                        'long_name':each.long_name1,
                                        'short_name':each.short_name,
                                        'categ_id':each.category_id.id,
                                        'tracking':'none',
                                        'type':'product',
                                        'hsn_no':each.hsn_no,
                                        'short_name_product':each.short_name_product,
                                        'current_name':each.current_name,
                                        'equipment_id':each.equipment_id.id,
                                        }
                elif product_obj.category_id1:
                    if product_obj.category_id1.numbered == True:
                        product_data = {
                                        'name':each.long_name,
                                        'product_alteration':each.long_name,
                                        'long_name':each.long_name1,
                                        'short_name':each.short_name,
                                        'categ_id':each.category_id1.id,
                                        'tracking':'serial',
                                        'type':'product',
                                        'hsn_no':each.hsn_no,
                                        'short_name_product':each.short_name_product,
                                        'current_name':each.current_name,
                                        'equipment_id':each.equipment_id.id,
                                        'short_alterable':each.short_name,
                                        }
                    else:
                        product_data = {
                                        'name':each.long_name,
                                        'product_alteration':each.long_name,
                                        'long_name':each.long_name1,
                                        'short_name':each.short_name,
                                        'categ_id':each.category_id1.id,
                                        'tracking':'none',
                                        'type':'product',
                                        'hsn_no':each.hsn_no,
                                        'short_name_product':each.short_name_product,
                                        'current_name':each.current_name,
                                        'equipment_id':each.equipment_id.id,
                                        'short_alterable':each.short_name,
                                        }

        prod_id = prod_obj.create(product_data)
        value_list = [ ]
        if prod1:
            for each in prod1:
                prod1_obj = self.env['product.product'].browse(each.id)
        if product_obj.category_id.attribute_lines:
            for each in product_obj.category_id.attribute_lines:
                value_list = []
                attribute_list = self.env['product.attribute.value'].search([('attribute_id','=',each.attribute_id.id)])
                for each1 in attribute_list:
                    value_list.append(each1.id)
                self.env['product.attribute.line'].create({'product_tmpl_id':prod_id.id,'attribute_id':each.attribute_id.id,'value_ids':[(6,0,value_list)]})
                prod_id.create_variant_ids()
        elif product_obj.category_id1.attribute_lines:
            for each in product_obj.category_id1.attribute_lines:
                value_list = []
                attribute_list = self.env['product.attribute.value'].search([('attribute_id','=',each.attribute_id.id)])
                for each1 in attribute_list:
                    value_list.append(each1.id)
                self.env['product.attribute.line'].create({'product_tmpl_id':prod_id.id,'attribute_id':each.attribute_id.id,'value_ids':[(6,0,value_list)]})
                prod_id.create_variant_ids()
        if prod_id:
            pro = self.env['product.product'].search([('product_tmpl_id','=',prod_id.id)])
            self.write({'state':'product_created','product_id':pro.id})  
            self.action_calculate_price()
        return prod_id
              
  # This function is for onchange on Non-Alterable Product Category.            
    @api.onchange('category_id')
    def _onchange_category_id(self):
        bom_list = []
        if self.category_id:
            if self.name1 == 'non-altered':
                if self.category_id.numbered == True:
                    self.numbered = True
                if self.category_id.specification_lines:
                    for val in self.category_id.specification_lines:
                         bom_list.append((0,False,{'category_id':self.category_id,'seq_no':val.seq_no,'specification':val.name}))
                self.add_product_lines = bom_list
                self.product_category = self.category_id.id
                self.hsn_no = self.category_id.hsn_no
                self.equipment_id = self.category_id.equipment_id.id
                
# This function is for onchange on Alterable Product Category. 
    @api.onchange('category_id1')
    def _onchange_category_id1(self):
        bom_list = []
        if self.category_id1:
            if self.name1 == 'altered':
                if self.category_id1.numbered == True:
                    self.numbered = True
                if self.category_id1.category_template_lines:
                    for val in self.category_id1.category_template_lines:
                         bom_list.append((0,False,{'name':val.id,'seq_no':val.seq_no,'part_name':val.part_name,
							'seq_no1':val.seq_no,
                                                   'mandatory':val.mandatory,'category_id':val.name.id,'qty':1}))
                self.category_template_lines = bom_list
                self.product_category = self.category_id1.id
                self.hsn_no = self.category_id1.hsn_no
                self.equipment_id = self.category_id1.equipment_id.id
         
#     @api.one
#     def copy(self, default=None):
#         if default is None:
#             default = {}
#         default['name'] = self.env['ir.sequence'].get('add_product')
#         return super(wiz_add_product, self).copy(default)       
                 
    
#     name2 = fields.Char('Name2')

    @api.onchange('product_component_short_name_id')
    def _onchange_product_component_short_name(self):
        if self.product_component_short_name_id:
            self.short_name_product = self.product_component_short_name_id.name

    name = fields.Char('Name')
    item_alteration_id = fields.Many2one('item.alteration','Item Alteration')
    non_item_alteration_id = fields.Many2one('non.item.alteration','Item Alteration')
    name1 = fields.Selection([('altered','Altered'),('non-altered','Non-Altered')],'Category Type',default='non-altered',track_visibility="onchange")
    category_id = fields.Many2one('product.category','Non-Alterable Product Category',track_visibility="onchange")
    numbered = fields.Boolean('Numbered',track_visibility="onchange")
    category_id1 = fields.Many2one('product.category','Alterable Product Category',track_visibility="onchange") 
    product_category = fields.Many2one('product.category','Product Category',track_visibility="onchange")
    hsn_no = fields.Char('HSN No',track_visibility="onchange")
    conversion_id = fields.Many2one('conversion.non.alterable','Conversion of Non Alterable to Alterable')
    long_name1 = fields.Char('Long Name',copy=False,track_visibility="onchange")
    equipment_id = fields.Many2one('equipment','Equipment',track_visibility="onchange")
    add_product_lines  = fields.One2many('add.product.line','wiz_add_product_id','Add Product',copy=True,track_visibility="onchange")
    short_name = fields.Char('Short Name',copy=False,track_visibility="onchange")
    long_name = fields.Char('Product for Alteration Purpose',copy=False,track_visibility="onchange")
    current_name = fields.Char('Current Name',copy=False,track_visibility="onchange")
    short_name_product = fields.Char('Short Name for Component in Bom',copy=False,track_visibility="onchange")
    date = fields.Date('Date',default=lambda *a: datetime.now(),copy=False,track_visibility="onchange")
    created_by = fields.Many2one('res.users','Created By',readonly=True,copy=False,default=lambda self: self.env.user,track_visibility="onchange")
    state = fields.Selection([('draft','Draft'),('product_created','Product Created')],'State',default="draft",track_visibility="onchange")
    category_template_lines = fields.One2many('category.template.line','wiz_add_product_id','Add Category',copy=True,track_visibility="onchange")
    product_id = fields.Many2one('product.product','Product',copy=False)
    product_component_short_name_id = fields.Many2one('product.component.short.name','Product Component Short Name',track_visibility="onchange")




class add_product_line(models.Model):
    _name="add.product.line"
    _inherit = ['mail.thread', 'ir.needaction_mixin']
    
    @api.model
    def write(self,values):
        values.update({'modifier': self.env.user.id,'modifier_date':datetime.now().date()})
        super(add_product_line,self).write(values)
    
   # This function is for onchange on valid value 
    @api.onchange('valid_value')
    def _onchange_valid_value(self):
        short_name = ''
        if self.valid_value:
            valid = self.env['valid.specification.value'].browse(self.valid_value.id)
            self.short_name = valid.short_name
            
    wiz_add_product_id = fields.Many2one('wiz.add.product','Add Product',track_viisbility="onchange")
    seq_no = fields.Integer('Seq No',track_visibility="onchange")
    specification_id = fields.Many2one('specification.line','Specifications',track_visibility="onchange")
    specification = fields.Char('Specification',track_visibility="onchange")
    short_name = fields.Char('Short Name',track_visibility="onchange")
    category_id = fields.Many2one('product.category','Product Category',track_visibility="onchange")
    valid_value = fields.Many2one('valid.specification.value','Valid Value',track_visibility="onchange")
    modifier = fields.Many2one('res.users', "Modifier",track_visibility="onchange")
    modifier_date = fields.Date('Modified Date',track_visibility="onchange")

class category_template_line(models.Model):
    _name="category.template.line"
    _inherit = ['mail.thread', 'ir.needaction_mixin']
    
    @api.model
    def write(self,values):
        values.update({'modifier': self.env.user.id,'modifier_date':datetime.now().date()})
        super(category_template_line,self).write(values)
          
    # This function is for onchange on Product.
    @api.onchange('product_id')
    def _onchange_product_id(self):
        bom_list = []
        if self.product_id:
            self.short_name = self.product_id.short_name_product
            self.short_name1 = self.product_id.short_name_product
            
     # This function is for onchange on Category       
    @api.onchange('name')
    def _onchange_name(self):
        if self.name:
            self.category_id = self.name.name.id   
            self.seq_no = self.name.seq_no
            self.part_name = self.name.part_name
            self.mandatory = self.name.mandatory
            self.seq_no1 = self.name.seq_no
     # This function is for onchange on Barcode       
    @api.onchange('barcode')
    def _onchange_barcode(self):
        if self.barcode:
            product = self.env['product.product'].search([('barcode','=',self.barcode),('product_tmpl_id.categ_id','=',self.category_id.id)])
            if product:
                for val in product:
                    product_obj = self.env['product.product'].browse(val.id)
                    if product_obj:
#                         if product_obj.product_tmpl_id.categ_id == self.category_id
                        self.product_id = product_obj.id
                        self.short_name = product_obj.short_name_product
                        self.short_name1 = product_obj.short_name_product
            else:
                raise UserError(_('The Product does not belong to this category or may not exist.'))
                        

    wiz_add_product_id = fields.Many2one('wiz.add.product','Add Product',track_visibility="onchange")
    name = fields.Many2one('category.template','Category')
    barcode = fields.Char('Barcode')
    category_id = fields.Many2one('product.category','Category Name',track_visibility="onchange")
    seq_no = fields.Integer('Actual Sequence No',track_visibility="onchange")
    seq_no1 = fields.Integer('Sequence No',track_viisbility="onchange")
    short_name = fields.Char('Actual Short Name for Product',track_visibility="onchange")
    short_name1 = fields.Char('Short Name for Product',track_visibility="onchange")
    part_name = fields.Boolean('Part of Name',track_visibility="onchange")
    mandatory = fields.Boolean('Mandatory',track_visibility="onchange")
    qty = fields.Integer('Qty',track_visibility="onchange")
    product_id = fields.Many2one('product.product','Product',track_visibility="onchange")
    modifier = fields.Many2one('res.users', "Modifier",track_visibility="onchange")
    modifier_date = fields.Date('Modified Date',track_visibility="onchange")
