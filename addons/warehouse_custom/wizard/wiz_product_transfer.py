import logging

from odoo import api, fields, models
from odoo import tools, _
import time
import math
import odoo.addons.decimal_precision as dp
from datetime import datetime,timedelta
from odoo.tools import DEFAULT_SERVER_DATE_FORMAT,DEFAULT_SERVER_DATETIME_FORMAT
from odoo.exceptions import UserError, ValidationError
from odoo.modules.module import get_module_resource

class wiz_product_transfer(models.TransientModel):
    _name ="wiz.product.transfer"
    
    @api.model
    def default_get(self, fields):
        res = super(wiz_product_transfer, self).default_get(fields)
        serial_list = []
        product_transfer_id = self.env['product.transfer.line'].browse(self._context.get('active_id'))
        quant = self.env['stock.quant'].search([('product_id','=',product_transfer_id.product_id.id),('location_id','=',product_transfer_id.product_transfer_id.source_location_id.id)])
        if quant:
            for val in quant:
                serial_list.append((0,False,{'serial_id':val.lot_id.id}))
        res = {'source_location_id':product_transfer_id.product_transfer_id.source_location_id.id,
               'product_id':product_transfer_id.product_id.id,
               'product_transfer_id':product_transfer_id.product_transfer_id.id,
               'qty':product_transfer_id.qty,'wiz_product_transfer_lines':serial_list,
               'destination_location_id':product_transfer_id.product_transfer_id.destination_location_id.id}
        return res
    
    @api.multi
    def create_line(self):
        serial_list = []
        q = 0
        for wizard in self:
            product_obj = self.env['product.transfer'].browse(wizard.product_transfer_id.id)
            if wizard.wiz_product_transfer_lines:
                for val in wizard.wiz_product_transfer_lines:
                    if val.select == True:
                        q = q + 1
            if q == wizard.qty:
                if product_obj.product_serial_lines:
                    for val3 in product_obj.product_serial_lines:
                        if val3.product_id.id == wizard.product_id.id:
                            val3.unlink()
                for val in wizard.wiz_product_transfer_lines:
                    if val.select == True:
                        serial_list.append((0,False,{'serial_id':val.serial_id.id,
                                                 'product_id':wizard.product_id.id}))
                product_obj.write({'product_serial_lines':serial_list})
                
                    
            else:
                raise UserError(_('You can not transfer these products.'))
                     
    
    source_location_id = fields.Many2one('stock.location','Source Location')
    destination_location_id = fields.Many2one('stock.location','Destination Location')
    product_id = fields.Many2one('product.product','Product')
    qty = fields.Float('Qty')
    product_transfer_id = fields.Many2one('product.transfer','Product Transfer')
    wiz_product_transfer_lines = fields.One2many('wiz.product.transfer.line','wiz_product_transfer_id','Product Transfer Lines')
    
class wiz_product_transfer_line(models.TransientModel):
    _name="wiz.product.transfer.line"
    
    select = fields.Boolean('Select')
    wiz_product_transfer_id = fields.Many2one('wiz.product.transfer','Product Transfer Wizard')
    serial_id = fields.Many2one('stock.production.lot','Serial No')
    