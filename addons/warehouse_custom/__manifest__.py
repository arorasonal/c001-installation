{
    "name": "Warehouse Custom",
    "version": "1.0",
    'website': 'http://www.diracerp.in',
    'images': [ ],
    "depends": ['web','base','product','stock'],
    "author": "Dirac ERP",
    "category": "Stock",
#     'sequence': 16,
    "description": """
    This module is for CJPL warehouse functionalities. 
    """,
    'data': [
             'security/security_view.xml',
             ##### VIEW
             'view/sequence_view.xml',
            'view/product_category_view.xml',
            'view/valid_specification_value_view.xml',
            'wizard/wiz_add_product_view.xml',
            'view/product_view.xml',
            'view/equipment_view.xml',
            'view/month_view.xml',
            'view/year_view.xml',
            'view/company_view.xml',
            'view/stock_view.xml',
            'view/item_alteration_view.xml',
            'view/stock_production_lot_view.xml',
            'wizard/wiz_product_transfer_view.xml',
            'view/product_transfer_view.xml',
            'view/non_item_alteration_view.xml',
            'view/stock_location_function_type_view.xml',
            'view/product_component_short_name_view.xml',
           
            #REPORT 
            # 'report/challan_report_view.xml',
            'report/challan_eway_summary_report_view.xml',
            'report/product_label1_report_view.xml',
            
            # REPORT TEMPLATES
            #'view/challan_report_template.xml',
            'view/challan_eway_summary_report_template.xml',
            'view/product_product_label1_template.xml',
            'view/product_template_label1_template.xml',

            
                    ],
    'demo_xml': [],
     'js': [
            'static/src/js/view_list.js'
#            'static/src/js/view_list.js'
#     'static/src/js/subst.js'
    ],
    'css': [
#             'static/src/css/sample_kanban.css'
            ],
    'pre_init_hook': 'pre_init_hook',
    'post_init_hook': 'post_init_hook',
    'installable': True,
    'active': False,
    'auto_install': False,
#    'certificate': 'certificate',
}
