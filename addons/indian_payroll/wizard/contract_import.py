from odoo import fields, models
from odoo.tools.translate import _
from odoo.exceptions import UserError, ValidationError
import base64


class ContractImport(models.TransientModel):
    """Import Contract."""

    _name = 'contract.import'

    file_contract_import = fields.Binary('Upload File', required=True)

    def import_contract(self):
        """From this method we fetch record from csv file and create records in contract.""" 
        if self.file_contract_import:
            x = base64.decodestring(self.file_contract_import).split('\n')
            length = len(base64.decodestring(
                self.file_contract_import).split('\n'))
            f = []
            total_length = length - 1
            count = 0
            # Seperating the headers row from csv file.
            for record in range(0, 1):
                f = x[record]
                k = f.split(",")
                # Fetching other rows and create records.
                for data in range(1, total_length):
                    try:
                        count += 1
                        f = x[data]
                        v = f.split(",")
                        emp_obj = self.env['hr.employee'].search(
                            [('employee_code', '=', v[0].strip())])
                        if emp_obj:
                            contract_obj = self.env['hr.contract']
                            structure = self.env['hr.payroll.structure'].search([
                                ('name', '=', v[4].strip())])
                            # analytic_account_id = self.env['account.analytic.account'].search([
                            #     ('name', '=', v[3].strip())])
                            # tds_rule = self.env['tds.rule'].search([('name', '=', v[7].strip())])
                            data = {
                                'employee_id': emp_obj.id,
                                'wage': v[6].strip(),
                                'struct_id': structure.id,
                                'leave_encashment': v[7].strip(),
                                'bonus': v[8].strip(),
                                # 'type_id': v[2].strip(),
                                # 'analytic_account_id': analytic_account_id.id,
                                'esic_applicable': v[2].strip(),
                                'epf_applicable': v[3].strip(),
                                'pay_day_calculation': v[9].strip(),
                                'pay_days': v[10].strip(),
                                # 'tds_rule': tds_rule.id,
                                'city_percentage': v[11].strip(),
                                'state': 'open'
                            }
                            contract = contract_obj.create(data)
                            contract.onchange_employee()
                            row = 12
                            for rule in k:
                                allw_lines = []
                                ded_lines = []
                                rule_name = rule
                                salary_rule_id = self.env['hr.salary.rule'].search([('name', '=', rule_name)])
                                for rule in salary_rule_id:
                                    if rule.category_id.code == "ALW" or rule.category_id.code == "ALWT":
                                        allw_lines.append((0, 0, {
                                            'salary_rule_id': salary_rule_id.id,
                                            'amount': v[row].strip()
                                        }))
                                        row += 1
                                    if rule.category_id.code == 'DED':
                                        ded_lines.append((0, 0, {
                                            'salary_rule_id_ded': salary_rule_id.id,
                                            'amount': v[row]
                                        }))
                                        row += 1
                                contract.write({
                                    'salary_allowance_id': allw_lines,
                                    'salary_deduction_id': ded_lines
                                })
                    except ValidationError:
                        raise ValidationError(_('You can not have 2 Contract that overlaps on same Period!(%s)') % emp_obj.name)
                    except Exception:
                        raise UserError(
                            _('Please check .csv file at row %s.') % (count + 1))
