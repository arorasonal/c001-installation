from odoo import models, api


class MultiHRPayslipApproveWiz(models.TransientModel):
    _name = 'multi.hr.payslip.approve.wiz'

    @api.multi
    def approve_multi_hr_payslip(self):
        hr_payslip_ids = self.env['hr.payslip'].browse(self._context.get('active_ids'))
        for payslip in hr_payslip_ids:
            if payslip.state == 'awaiting_approval':
                payslip.action_payslip_done()


class MultiHRPayslipAwaitingWiz(models.TransientModel):
    _name = 'multi.hr.payslip.awaiting.wiz'

    @api.multi
    def awaiting_multi_hr_payslip(self):
        hr_payslip_ids = self.env['hr.payslip'].browse(self._context.get('active_ids'))
        for payslip in hr_payslip_ids:
            if payslip.state == 'draft':
                payslip.action_submit_for_approve()
