from odoo import fields, models
from odoo.tools.translate import _
from odoo.exceptions import UserError, ValidationError
import base64


class InvestmentImport(models.TransientModel):
    """Import Investment."""

    _name = 'investment.import'

    file_investment_import = fields.Binary('Upload File', required=True)

    def import_investment(self):
        x = base64.decodestring(self.file_investment_import).split('\n')
        length = len(base64.decodestring(
            self.file_investment_import).split('\n'))
        f = []
        total_length = length - 1
        count = 0
        for data in range(1, total_length):
            sec_c_data = []
            sec_d_data = []
            sec_ccg_data = []
            others = []
            months = []
            all_month = ['April', 'May', 'June', 'July', 'August', 'September',
                         'October', 'November', 'December', 'January', 'February', 'March']
            try:
                count += 1
                f = x[data]
                v = f.split(",")
                emp_obj = self.env['hr.employee'].search(
                    [('employee_code', '=', v[0].strip())])
                financial_year = self.env['account.fiscalyear'].search(
                    [('name', '=', v[2].strip())])
                if emp_obj and financial_year:
                    if emp_obj.date_of_joining >= financial_year.date_start and emp_obj.date_of_joining <= financial_year.date_stop:
                        is_previous_employment = True
                    data = {
                        'employee_id': emp_obj.id,
                        'financial_year': financial_year.id,
                        'is_previous_employment': is_previous_employment,
                        'home_loan_interest': v[32]
                    }
                    investment_new_id = self.env['investment.form'].create(data)
                    for sec_c in range(3, 19):
                        sec_c_data.append(v[sec_c])
                    for sec_d in range(19, 22):
                        sec_d_data.append(v[sec_d])
                    for sec_ccg in range(22, 23):
                        sec_ccg_data.append(v[sec_ccg])
                    for rec in range(23, 32):
                        others.append(v[rec])
                    for month in range(34, 46):
                        months.append(v[month])
                    investment_sec_c = self.env['declaration.line'].search([
                        ('investment_form_sec_c_id', '=', investment_new_id.id)
                    ])
                    investment_sec_d = self.env['declaration.line'].search([
                        ('investment_form_sec_d_id', '=', investment_new_id.id)
                    ])
                    investment_sec_ccg = self.env['declaration.line'].search([
                        ('investment_form_sec_ccg_id', '=', investment_new_id.id)
                    ])
                    other_investment = self.env['declaration.line'].search([
                        ('investment_form_sec_other_sec_id', '=', investment_new_id.id)
                    ])
                    for cnt, record in enumerate(investment_sec_c):
                        record.write({
                            'proposed_amount': sec_c_data[cnt]
                        })
                    for cnt, record in enumerate(investment_sec_d):
                        record.write({
                            'proposed_amount': sec_d_data[cnt]
                        })
                    for cnt, record in enumerate(investment_sec_ccg):
                        record.write({
                            'proposed_amount': sec_ccg_data[cnt]
                        })
                    for cnt, record in enumerate(other_investment):
                        record.write({
                            'proposed_amount': others[cnt]
                        })
                    for cnt, record in enumerate(all_month):
                        a = 33 + cnt
                        self.env['investment.month'].create({
                            'months': all_month[cnt],
                            'amount': v[a],
                            'investment_id': investment_new_id.id
                        })
                    for cnt, record in enumerate(all_month):
                        a = 45 + cnt
                        self.env['investment.month'].create({
                            'months': all_month[cnt],
                            'amount': v[a],
                            'investment_tds_deduction_id': investment_new_id.id
                        })
            except ValidationError, e:
                e = e.message + e[0] + ' ' + 'for %s' % emp_obj.name
                raise UserError(e)

            except Exception:
                raise UserError(
                    _('Please check .csv file at row %s.') % (count + 1))
