from odoo import api, models


class PayslipReport(models.AbstractModel):
    _name = 'report.hr_payroll.report_payslip'

    @api.model
    def get_report_values(self, docids, data):
        late_comming = 0
        half_days = 0
        lwp = 0
        paid_leave = 0
        payslip = self.env['hr.payslip'].browse(docids)
        attendance_ids = self.env['hr.attendance.sheet'].search([
            ('employee_id', '=', payslip.employee_id.id),
            ('date_from', '>=', payslip.date_from),
            ('date_to', '<=', payslip.date_to)
        ])
        for attendance in attendance_ids:
            late_comming += attendance.late_not

        holiday_ids = self.env['hr.holidays'].search([
            ('employee_id', '=', payslip.employee_id.id),
            ('date_from', '>=', payslip.date_from),
            ('date_to', '<=', payslip.date_to)
        ])
        holiday_status_id = self.env['hr.holidays.status'].search([
            ('name', '=', 'Unpaid')])
        for holiday in holiday_ids:
            if holiday.full_half_day == 'half':
                half_days += 1
            if holiday.holiday_status_id == holiday_status_id.id:
                lwp += holiday.number_of_days_temp
            else:
                paid_leave += holiday.number_of_days_temp
        return {
            'doc_ids': payslip,
            'doc_model': 'hr.payslip',
            'docs': payslip,
            'data': data,
            'late_comming': late_comming,
            'half_days': half_days,
            'lwp': lwp,
            'paid_leave': paid_leave
        }
