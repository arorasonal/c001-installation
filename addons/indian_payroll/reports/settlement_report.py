from odoo import api, models
from datetime import datetime
import time
from dateutil.rrule import rrule, MONTHLY


class SettlementReport(models.AbstractModel):
    _name = 'report.indian_payroll.report_final_settlement'

    @api.model
    def get_report_values(self, docids, data):
        months = []
        payslip_dict = {}
        hra_dict = {}
        rule_id = []
        paid_days = []
        next_year = 0
        rent_recieved = []
        allowances = {}
        rule_alw = []
        settlement_alw = {}
        city_percent = 0
        yearly_total = {}
        scnd_lst_payslip = {}
        till_last_month = {}
        tds_amount = {}
        now = datetime.now()
        if now.month in (1, 2, 3):
            year = now.year - 1
            next_year = now.year
        else:
            year = now.year
            next_year = year + 1
        date_start = datetime.strptime(time.strftime('%Y-04-01'), "%Y-%m-%d").date()
        date_end = datetime.strptime(time.strftime('%Y-03-31'), "%Y-%m-%d").date()
        start_date_year = date_start.replace(year=year)
        end_date_year = date_end.replace(year=next_year)
        all_months = [dt.strftime("%b-%y") for dt in rrule(MONTHLY, dtstart=start_date_year, until=end_date_year)]
        start_date = start_date_year.strftime("%B-%Y")
        end_date = end_date_year.strftime("%B-%Y")
        payslip_obj = self.env['hr.payslip']
        payslip_ids = payslip_obj.browse(docids)
        payslips = payslip_obj.search([
            ('employee_id', '=', payslip_ids.employee_id.id),
            ('date_from', '>=', start_date_year),
            ('date_to', '<=', end_date_year)
        ])
        for cnt, rec in enumerate(payslips):
            for line in rec.line_ids:
                if line.name not in rule_id:
                    if not line.salary_rule_id.category_id.code == 'COMP':
                        rule_id.append(line.name)
                if (line.category_id.code == 'ALW' or line.category_id.code == 'BASIC') and line.name not in rule_alw:
                    rule_alw.append(line.name)
                allowances.setdefault(line.name, []).append(line.total)
                payslip_dict.setdefault(line.name, []).append(line.total)
                hra_dict.setdefault(line.name, []).append(line.total)
                if (cnt == (len(payslips) - 1)) and line.name == 'House Rent Allowance':
                    for length in range(12 - len(payslips)):
                        hra_dict.setdefault(line.name, []).append(0.0)
                month = datetime.strptime(rec.date_from, "%Y-%m-%d").strftime('%b-%y')
                if line.name == 'TDS':
                    tds_amount.setdefault(month, []).append(line.total)
            months.append(str(month))
            paid_days.append(rec.worked_days_line_ids.number_of_days)
            city_percent = rec.contract_id.wage * rec.contract_id.city_percentage / 100
            if rec.payslip_type == 'settlement':
                for line in rec.line_ids:
                    if (line.category_id.code == 'ALW' or line.category_id.code == 'BASIC'):
                        settlement_alw.setdefault(line.name, []).append(line.total)
            else:
                for line in rec.line_ids:
                    till_last_month.setdefault(line.name, []).append(line.total)
        for line in allowances:
            temp_total = settlement_alw.get(line, [0.0])[0] + \
                sum(till_last_month.get(line, [0.0]))
            yearly_total.setdefault(line, []).append(temp_total)

        for payslip in payslips[-2]:
            for line in payslip.line_ids:
                scnd_lst_payslip.setdefault(line.name, []).append(line.total)

        investment_obj = self.env['investment.form']
        investment = investment_obj.search([
            ('employee_id', '=', payslip_ids.employee_id.id)
        ])
        sec_c_lst = []
        sec_c_dict = {}
        income_tax = []
        for sections in investment.declaration_line_id_sec_c:
            sec_c_lst.append(sections.scheme.name)
            sec_c_dict.setdefault(sections.scheme.name, []).append(sections.proposed_amount)

        for rec in investment.investment_months:
            rent_recieved.append(rec.amount)
        for tax in investment.tds_deductions_ids:
            income_tax.append(tax.amount)

        investment_conf = self.env['config.investment.section'].search([('name', '=', 'Section 80C')])
        qualify_sec_c_amount = investment_conf.max_exemption

        return {
            'doc_ids': payslip_ids,
            'doc_model': 'hr.payslip',
            'docs': payslip_ids,
            'data': data,
            'months': months,
            'payslip': payslip_dict,
            'hra': hra_dict,
            'rule_id': rule_id,
            'paid_days': paid_days,
            'all_months': all_months,
            'rent_recieved': rent_recieved,
            'allowances': allowances,
            'till_last_month': till_last_month,
            'rule_alw': rule_alw,
            'settlement_alw': settlement_alw,
            'yearly_total': yearly_total,
            'start_date': start_date,
            'end_date': end_date,
            'sec_c_dict': sec_c_dict,
            'sec_c_lst': sec_c_lst,
            'qualify_sec_c_amount': qualify_sec_c_amount,
            'city_percent': city_percent,
            'income_tax': income_tax,
            'scnd_lst_payslip': scnd_lst_payslip,
            'tds_amount': tds_amount
        }
