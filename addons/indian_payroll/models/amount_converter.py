from odoo import api, models, fields


class HrPayslip(models.Model):
    _inherit = 'hr.payslip'

    @api.multi
    def _get_currency_id(self):
        currency_id = self.env.user.company_id.currency_id.id
        return currency_id

    currency_id = fields.Many2one('res.currency', default=_get_currency_id)

    @api.multi
    def amount_to_text(self, total_cost, currency='INR'):
        a = "Only"
        cost = 0.00
        for rec in self:
            if total_cost:
                cost = self.currency_id.amount_to_text(total_cost) + " " + a
        return cost.upper()
