from odoo import models, fields, api, tools, _
from datetime import date, timedelta
import time
import babel
import calendar
from time import strptime
from datetime import datetime, timedelta
from dateutil.rrule import rrule, MONTHLY
from dateutil import relativedelta
from odoo.exceptions import ValidationError, Warning, UserError


class HrPayslip(models.Model):
    _name = 'hr.payslip'
    _inherit = ['mail.thread', 'hr.payslip']

    name = fields.Char(string='Payslip Name',
                       states={'draft': [('readonly', False)]})
    number = fields.Char(string='Reference', copy=False,
                         states={'draft': [('readonly', False)]})
    employee_id = fields.Many2one('hr.employee', string='Employee', required=True,
                                  states={'draft': [('readonly', False)]})
    tds_rule = fields.Many2one('tds.rule', 'TDS Rule',
                               readonly=True)
    professional_tax_rule_id = fields.Many2one('professional.tax.rule')
    state = fields.Selection([
        ('draft', 'Draft'),
        ('awaiting_approval', 'Awaiting Approval'),
        ('verify', 'Waiting'),
        ('done', 'Done'),
        ('cancel', 'Rejected'),
    ], string='Status', index=True, readonly=True, copy=False, default='draft',
        help="""* When the payslip is created the status is \'Draft\'
                \n* If the payslip is under verification, the status is \'Waiting\'.
                \n* If the payslip is confirmed then status is set to \'Done\'.
                \n* When user cancel payslip the status is \'Rejected\'.""")
    journal_id = fields.Many2one('account.journal', default=False, domain=[
                                 ('type', '=', 'general')])
    department_id = fields.Many2one('hr.department', string="Department")
    analytic_account_id = fields.Many2one(
        'account.analytic.account', 'Analytic Account')
    payslip_type = fields.Selection([
        ('payslip', 'Payslip'),
        ('settlement', 'Settlement')
    ], string="Type")
    settlement_date = fields.Date(default=datetime.now())
    leave_encashment_days = fields.Float()
    leave_encash_days = fields.Float()
    notice_days = fields.Float()
    buy_notice = fields.Boolean()
    no_of_days = fields.Integer()
    notice_amount = fields.Float()
    amount_to_pay = fields.Float()
    fiscal_year = fields.Many2one('account.fiscal.year')
    remaining_months_gross = fields.Float()
    total_sda_exempt = fields.Float()

    @api.constrains('date_from', 'date_to')
    def check_payslip(self):
        for rec in self:
            paid_payslips = self.env['hr.payslip'].search([
                ('employee_id', '=', rec.employee_id.id),
                ('id', '!=', rec.id),
                ('state', '!=', 'cancel'),
            ])
        for pp in paid_payslips:
            if (pp.date_from >= rec.date_from and pp.date_to <= rec.date_to) or (pp.date_from <= rec.date_from and pp.date_to >= rec.date_to) or \
                    (pp.date_from <= rec.date_from and pp.date_to >= rec.date_from) or (pp.date_from >= rec.date_from and pp.date_from <= rec.date_to):
                raise ValidationError(
                    _('You can not create multiple payslips of employee :- . %s' % rec.employee_id.name))
            # elif rec.worked_days_line_ids[0].number_of_days < 1:
            #     raise ValidationError(
            #         'You can not create payslip because this month attendance is not present')
        # return True

    def cal_leave_days_encashment(self):
        earned_leave = 0
        leave_encashment_days = 0
        current_date = datetime.now()

        leave_structure_obj = self.env['leave.structure'].search(
            [('project_id', '=', self.analytic_account_id.id)])
        for record in leave_structure_obj:
            for leave in record.structure_line_ids:
                if leave.leave_type.name == 'Earned Leave':
                    earned_leave = leave.monthly_annual
        if current_date.month in (1, 2, 3):
            year = current_date.year - 1
            next_year = current_date.year
        else:
            year = current_date.year
            next_year = year + 1

        fiscal_start_date = datetime.strptime(
            time.strftime('%Y-04-01'), "%Y-%m-%d").date()
        fiscal_end_date = datetime.strptime(
            time.strftime('%Y-03-31'), "%Y-%m-%d").date()
        fiscal_start_year = fiscal_start_date.replace(year=year)
        fiscal_next_year = fiscal_end_date.replace(year=next_year)
        all_payslips = self.search([
            ('employee_id', '=', self.employee_id.id),
            ('date_from', '>=', fiscal_start_year),
            ('date_to', '<=', fiscal_next_year)
        ])
        earned_leave = earned_leave * (len(all_payslips) + 1)
        holiday_obj = self.env['hr.holidays']
        DATE_FORMAT = "%Y-%m-%d"
        for obj in self:
            from_date = datetime.strptime(obj.date_from, DATE_FORMAT).date()
            to_date = datetime.strptime(obj.date_to, DATE_FORMAT).date()
            leaves = 0
            lwp_holidays = holiday_obj.search([
                ('employee_id', '=', obj.employee_id.id),
                '|',
                ('state', '=', 'validate'),
                ('state', '=', 'confirm'),
                ('holiday_status_id.name', '=', 'Earned Leave')
            ])
            for holiday in lwp_holidays:
                date_start = datetime.strptime(
                    holiday.date_from, DATE_FORMAT).date()
                date_to = datetime.strptime(
                    holiday.date_to, DATE_FORMAT).date()
                if (date_start < from_date) and (date_to >= from_date):
                    leaves = holiday.number_of_days_temp - \
                        (from_date - date_start).days + leaves
                elif (date_start >= from_date) and (date_to <= to_date):
                    leaves = holiday.number_of_days_temp + leaves
                elif (date_start <= to_date) and (date_to > to_date):
                    leaves = holiday.number_of_days_temp + \
                        (to_date - date_to).days + leaves
        if self.employee_id.earned_leave_year:
            for rec in self.employee_id.earned_leave_year:
                carry_stop_date = datetime.strptime(
                    rec.fiscal_id.date_stop, "%Y-%m-%d")
                carry_start_date = datetime.strptime(
                    rec.fiscal_id.date_start, "%Y-%m-%d")
                previous_year = current_date - \
                    relativedelta.relativedelta(years=1)
                if previous_year >= carry_start_date and previous_year <= carry_stop_date:
                    leave_encashment_days = rec.carry_over + earned_leave - leaves
        else:
            leave_encashment_days = earned_leave - leaves
        return leave_encashment_days

    @api.model
    def get_worked_day_lines(self, contract_ids, date_from, date_to):
        """
        @param contract_ids: list of contract id
        @return: returns a list of dict containing the input that should be applied for the given contract between date_from and date_to
        """

        def was_on_leave_interval(employee_id, date_from, date_to):
            date_from = fields.Datetime.to_string(date_from)
            date_to = fields.Datetime.to_string(date_to)
            return self.env['hr.holidays'].search([
                ('state', '=', 'validate'),
                ('employee_id', '=', employee_id),
                ('type', '=', 'remove'),
                ('date_from', '<=', date_from),
                ('date_to', '>=', date_to)
            ], limit=1)

        res = []
        # fill only if the contract as a working schedule linked
        uom_day = self.env.ref('product.product_uom_day',
                               raise_if_not_found=False)
        for contract in self.env['hr.contract'].browse(contract_ids).filtered(lambda contract: contract.working_hours):
            uom_hour = contract.employee_id.resource_id.calendar_id.uom_id or self.env.ref(
                'product.product_uom_hour', raise_if_not_found=False)
            interval_data = []
            holidays = self.env['hr.holidays']
            attendances = {
                'name': _("Normal Working Days paid at 100%"),
                'sequence': 1,
                'code': 'WORK100',
                'number_of_days': 0.0,
                'number_of_hours': 0.0,
                'contract_id': contract.id,
            }
            attendances1 = {
                'name': _("Remaining Earned Leave"),
                'sequence': 3,
                'code': 'LA100',
                'number_of_days': 0.0,
                'number_of_hours': 0.0,
                'contract_id': contract.id,
            }
            leaves = {}
            day_from = fields.Datetime.from_string(date_from)
            day_to = fields.Datetime.from_string(date_to)
            nb_of_days = (day_to - day_from).days + 1

            # Gather all intervals and holidays
            for day in range(0, nb_of_days):
                working_intervals_on_day = contract.working_hours.get_working_intervals_of_day(
                    start_dt=day_from + timedelta(days=day))
                for interval in working_intervals_on_day:
                    interval_data.append((interval, was_on_leave_interval(
                        contract.employee_id.id, interval[0], interval[1])))

            # Extract information from previous data. A working interval is considered:
            # - as a leave if a hr.holiday completely covers the period
            # - as a working period instead

            # for interval, holiday in interval_data:
            #     holidays |= holiday
            #     hours = (interval[1] - interval[0]).total_seconds() / 3600.0
            #     if holiday:
            #         # if he was on leave, fill the leaves dict
            #         if holiday.holiday_status_id.name in leaves:
            #             leaves[holiday.holiday_status_id.name][
            #                 'number_of_hours'] += hours
            #         else:
            #             leaves[holiday.holiday_status_id.name] = {
            #                 'name': holiday.holiday_status_id.name,
            #                 'sequence': 5,
            #                 'code': holiday.holiday_status_id.name,
            #                 'number_of_days': 0.0,
            #                 'number_of_hours': hours,
            #                 'contract_id': contract.id,
            #             }
            #     else:
            #         # add the input vals to tmp (increment if existing)
            #         attendances['number_of_hours'] += hours

            # Clean-up the results
            for data in [attendances]:
                line_obj = self.env['monthly.attendance.line'].search([
                    ('employee_id', '=', self.employee_id.id),
                    ('hr_attendance_id.date_from', '>=', self.date_from),
                    ('hr_attendance_id.date_to', '<=', self.date_to)])
                for rec in line_obj:
                    present_days = rec.paid_days
                    data['number_of_days'] = present_days
                    data['number_of_hours'] = present_days * 8
                res.append(data)
            leaves = [value for key, value in leaves.items()]
            for data in leaves:
                data['number_of_days'] = uom_hour._compute_quantity(data['number_of_hours'], uom_day)\
                    if uom_day and uom_hour\
                    else data['number_of_hours'] / 8.0
            for data in [attendances1]:
                data['number_of_days'] = 0.0
                res.append(data)
        return res

    @api.onchange('employee_id', 'date_from', 'date_to')
    def onchange_employee(self):
        if self.payslip_type == 'payslip':
            self.env.context = self.with_context(
                attendance_flag='payroll').env.context
            super(HrPayslip, self).onchange_employee()
            self.department_id = self.contract_id.department_id
            self.tds_rule = self.contract_id.tds_rule
            self.analytic_account_id = self.contract_id.analytic_account_id
            self.journal_id = self.contract_id.journal_id
        else:
            self.env.context = self.with_context(
                attendance_flag='payroll').env.context
            super(HrPayslip, self).onchange_employee()
            self.department_id = self.contract_id.department_id
            self.analytic_account_id = self.contract_id.analytic_account_id
            self.tds_rule = self.contract_id.tds_rule
            self.journal_id = self.contract_id.journal_id
        #     ttyme = datetime.fromtimestamp(time.mktime(time.strptime(self.date_from, "%Y-%m-%d")))
        #     locale = self.env.context.get('lang') or 'en_US'
        #     self.name = _('Settlement Slip of %s for %s') % (self.employee_id.name, tools.ustr(
        # babel.dates.format_date(date=ttyme, format='MMMM-y', locale=locale)))
            if self.employee_id:
                self.leave_encashment_days = self.cal_leave_days_encashment()
                self.leave_encash_days = self.cal_leave_days_encashment()

    @api.model
    def get_contract(self, employee, date_from, date_to):
        clause_1 = ['&', ('date_end', '<=', date_to),
                    ('date_end', '>=', date_from)]
        clause_2 = ['&', ('date_start', '<=', date_to),
                    ('date_start', '>=', date_from)]
        clause_3 = ['&', ('date_start', '<=', date_from), '|',
                    ('date_end', '=', False), ('date_end', '>=', date_to)]
        clause_final = [('employee_id', '=', employee.id), ('state', '=', 'open'), '|',
                        '|'] + clause_1 + clause_2 + clause_3
        return self.env['hr.contract'].search(clause_final).ids

    @api.multi
    def compute_sheet(self):
        payslip_run = self.env['hr.payslip.run'].search([])
        professional_tax_rule_id = self.env['professional.tax.rule'].search([
            ('state_id', '=', self.employee_id.location.state_id.id),
            ('start_date', '<=', self.date_from),
            ('end_date', '>=', self.date_to)
        ])
        self.professional_tax_rule_id = professional_tax_rule_id.id
        for rec_id in payslip_run:
            for employee in rec_id.payroll_batche.batch_employee_id:
                investment_id = self.env['investment.form'].search([
                    ('employee_id', '=', employee.id),
                    ('start_date', '<=', self.date_from),
                    ('end_date', '>=', self.date_to),
                    ('state', '!=', 'closed')
                ])
                if not investment_id:
                    raise UserError(
                        'Please Create Investment Before Computing Payslip: %s' % employee.name)
        return super(HrPayslip, self).compute_sheet()

    @api.constrains('date_from', 'date_to')
    def check_date(self):
        previous_id = self.env['hr.payslip'].search([
            ('employee_id', '=', self.employee_id.id),
            ('date_from', '<=', self.date_from),
            ('date_to', '>=', self.date_to),
        ])
        if not previous_id:
            raise UserError(
                'You can not create duplicate payslip.')

    @api.model
    def create(self, vals):
        contract = self.env['hr.contract'].search([
            ('employee_id', '=', vals.get('employee_id')),
            ('status1', 'not in', ['close_contract', 'terminated'])])
        vals['struct_id'] = contract.struct_id.id
        vals['contract_id'] = contract.id
        vals['department_id'] = contract.department_id.id
        vals['journal_id'] = contract.journal_id.id
        vals['tds_rule'] = contract.tds_rule.id
        vals['analytic_account_id'] = contract.analytic_account_id.id
        if 'payslip_type' in vals and vals['payslip_type'] == 'settlement':
            vals['leave_encashment_days'] = vals['leave_encash_days']
        else:
            vals['payslip_type'] = "payslip"
        return super(HrPayslip, self).create(vals)

    @api.multi
    def write(self, vals):
        for record in self:
            contract = self.env['hr.contract'].search([
                ('employee_id', '=', vals.get('employee_id'))])
            if contract:
                vals['struct_id'] = contract.struct_id.id
                vals['contract_id'] = contract.id
                vals['department_id'] = contract.department_id.id
                vals['tds_rule'] = contract.tds_rule.id
                vals['analytic_account_id'] = contract.analytic_account_id.id
                vals['journal_id'] = contract.journal_id.id
            if record.payslip_type == 'settlement':
                if 'leave_encash_days' in vals:
                    vals['leave_encashment_days'] = vals['leave_encash_days']
        return super(HrPayslip, self).write(vals)

    @api.multi
    def onchange_employee_id(self, date_from, date_to, employee_id=False, contract_id=False):
        self.env.context = self.with_context(
            attendance_flag='payroll').env.context
        return super(HrPayslip, self).onchange_employee_id(date_from, date_to, employee_id, contract_id)

    @api.multi
    def action_submit_for_approve(self):
        for rec in self:
            if rec.state == 'draft':
                self.write({'state': 'awaiting_approval'})
            if not rec.line_ids:
                raise UserError(
                    "Please Compute the Payslip before submitting it for Approval.")
        return True

    @api.multi
    def action_print_payslip(self):
        return self.env['report'].get_action(self, 'indian_payroll.action_report_payslip_form')

    @api.multi
    def action_print_payslip_details(self):
        return self.env['report'].get_action(self, 'indian_payroll.report_payslipdetails_form')

    @api.multi
    def action_print_final_settlement(self):
        return self.env['report'].get_action(self, 'indian_payroll.report_final_settlement_form')

    @api.multi
    def action_send_payslip_mail(self):
        template = self.env.ref('indian_payroll.payslip_mail_template')
        if template:
            template.send_mail(self.id, force_send=True)


class HrContract(models.Model):
    _inherit = 'hr.contract'

    @api.multi
    def _epf_applicable(self):
        current_date = str(date.today())
        financial_year = self.env['account.fiscal.year'].search([
            ('date_start', '<=', current_date),
            ('date_stop', '>=', current_date)], limit=1)
        if self.wage <= financial_year.epf_employee_limit:
            self.epf_applicable = True
        else:
            False

    @api.multi
    def _esic_applicable(self):
        current_date = str(date.today())
        financial_year = self.env['account.fiscal.year'].search([
            ('date_start', '<=', current_date),
            ('date_stop', '>=', current_date)], limit=1)
        if self.gross_sal <= financial_year.esci_employee_limit:
            self.esic_applicable = True
        else:
            False

    @api.multi
    def _bonous_applicable(self):
        current_date = str(date.today())
        financial_year = self.env['account.fiscal.year'].search([
            ('date_start', '<=', current_date),
            ('date_stop', '>=', current_date)], limit=1)
        if self.gross_sal <= financial_year.esci_employee_limit:
            self.bonus_applicable = True
        else:
            False

    tds_rule = fields.Many2one('tds.rule', "Tax Slab")
    leave_encashment = fields.Float("Leave Encashment(%)")
    bonus = fields.Float("Bonus(%)")
    esic_applicable = fields.Boolean(
        'ESIC Applicable', compute="_esic_applicable")
    esic_applicable_check = fields.Boolean(
        'ESIC Applicable Check')
    epf_applicable = fields.Boolean(
        'EPF Applicable', compute="_epf_applicable")
    epf_applicable_check = fields.Boolean(
        'EPF Applicable Check')
    epf_applicable_pure = fields.Boolean(
        'EPF Applicable on Limit')
    epf_applicable_actual = fields.Boolean(
        'EPF Applicable on Basic')
    leave_encashment_applicable = fields.Boolean('Leave Encashment Applicable')
    bonus_applicable = fields.Boolean(
        'Bonus Applicable', compute='_bonous_applicable')
    bonus_applicable_on_limit = fields.Boolean(
        'Bonus Applicable on Limit')
    bonus_applicable_on_basic = fields.Boolean(
        'Bonus Applicable on Basic')
    city_percentage = fields.Float('City (%)')
    pay_day_calculation = fields.Selection([
        ('fixed', 'Fixed'),
        ('days_in_month', 'No. of Days in Month')
    ], default='fixed', string="Pay Days Calculation")
    pay_days = fields.Integer(string="Pay Days")
    journal_id = fields.Many2one('account.journal',
                                 default=lambda self: self.env['account.journal'].search([('name', '=', 'Salary Journal')], limit=1))
    sda_exemption_applicable = fields.Boolean(
        string="SDA Exemption Applicable")
    exempt_amount = fields.Integer()
    wage = fields.Float(
        'Basic Salary',
        digits=(16, 2),
        required=True,
        help="Basic Salary of the employee"
    )
    first_half = fields.Boolean(
        'First Half')
    second_half = fields.Boolean(
        'Second Half')

    @api.multi
    def get_esic_applicable(self, gross, payslip):
        esic = 0
        financial_year = self.env['account.fiscal.year'].search([
            ('id', '!=', self.id)])
        start_date = datetime.strptime(
            financial_year.date_start, "%Y-%m-%d")
        end_date = datetime.strptime(
            financial_year.date_stop, "%Y-%m-%d")
        all_months = [dt.strftime("%B") for dt in rrule(
            MONTHLY, dtstart=start_date, until=end_date)]
        first_half_months = []
        second_half_months = []
        first_half_months.append(all_months[0:6])
        second_half_months.append(all_months[7:13])
        if payslip.contract_id.esic_applicable:
            if gross <= financial_year.esci_employee_limit:
                esic = gross * financial_year.esci_employee
                return esic
            else:
                return False
        else:
            return False

    @api.onchange('employee_id')
    def onchange_employee(self):
        age = 0
        if self.employee_id and self.employee_id.gender:
            today = datetime.now()
            born = datetime.strptime(
                self.employee_id.birthday, "%Y-%m-%d")
            age = today.year - born.year - \
                ((today.month, today.day) < (born.month, born.day))
            age_limit = self.env['tds.rule'].search(
                ['|', ('tax_slab_type.age_end', '>=', age), ('tax_slab_type.gender', '=', self.employee_id.gender)])
            for rec in age_limit:
                self.tds_rule = rec.id

    @api.onchange('struct_id')
    def onchange_structure(self):
        self.leave_encashment = self.struct_id.leave_encashment
        self.bonus = self.struct_id.bonus

    @api.model
    def create(self, vals):
        age = 0
        closed_contract = []
        if vals.get('employee_id'):
            employee = self.env['hr.employee'].search(
                [('id', '=', vals.get('employee_id'))])
            amount = 0
            if 'salary_allowance_id' in vals:
                for rec in vals['salary_allowance_id']:
                    amount += rec[2]['amount']
            gross = vals['wage'] + amount
            if gross >= 21000:
                today = date.today()
                financial_year = self.env['account.fiscal.year'].search([
                    ('date_start', '<=', today), ('date_stop', '>=', today)])
                start_date = datetime.strptime(
                    financial_year.date_start, "%Y-%m-%d")
                end_date = datetime.strptime(
                    financial_year.date_stop, "%Y-%m-%d")
                first_half_months = ''
                second_half_months = ''
                for fis_yr in financial_year:
                    all_months = [dt.strftime("%B") for dt in rrule(
                        MONTHLY, dtstart=start_date, until=end_date)]
                    first_half_months = all_months[0:6]
                    second_half_months = all_months[7:13]
                    contract = self.env['hr.contract'].search(
                        [('employee_id', '=', vals.get('employee_id')), ('status1', '=', 'close_contract')])
                    for record in contract:
                        closed_contract.append(record.id)
                        latest_closed_contract = closed_contract[-1]
                        record.doc_con_ref = latest_closed_contract
                        gross = record.doc_con_ref.gross_sal
                        date_end_dd = record.doc_con_ref.date_end
                        date_end_dd = datetime.strptime(
                            record.doc_con_ref.date_end, "%Y-%m-%d")
                        current_months = date_end_dd.strftime("%B")
                        if current_months in first_half_months:
                            vals['first_half'] = True
                        else:
                            vals['second_half'] = True
            if employee and employee.birthday:
                today = datetime.now()
                born = datetime.strptime(employee.birthday, "%Y-%m-%d")
                age = today.year - born.year - \
                    ((today.month, today.day) < (born.month, born.day))
                if vals.get(age) and vals.get(self.employee_id.gender):
                    age_limit = self.env['tds.rule'].search(
                        ['|', ('tax_slab_type.age_end', '>=', vals.get(
                            age)), ('tax_slab_type.gender', '=', vals.get(self.employee_id.gender))])
                    for rec in age_limit:
                        vals['tds_rule'] = rec.id
        return super(HrContract, self).create(vals)

    @api.multi
    def write(self, vals):
        age = 0
        if vals.get('employee_id'):
            employee = self.env['hr.employee'].search(
                [('id', '=', vals.get('employee_id'))])
            if employee and employee.birthday:
                today = datetime.now()
                born = datetime.strptime(employee.birthday, "%Y-%m-%d")
                age = today.year - born.year - \
                    ((today.month, today.day) < (born.month, born.day))
                age_limit = self.env['tds.rule'].search(
                    ['|', ('tax_slab_type.age_end', '>=', age), ('tax_slab_type.gender', '=', self.employee_id.gender)])
                for rec in age_limit:
                    self.tds_rule = rec.id
        return super(HrContract, self).write(vals)


class hr_payroll_run(models.Model):
    _name = 'hr.payslip.run'
    _inherit = ['mail.thread', 'hr.payslip.run']
    _rec_name = 'payroll_batche'

    @api.multi
    def _active(self):
        for rec in self:
            if rec.slip_ids:
                self.fetch_active = True
            else:
                self.fetch_active = False

    name = fields.Char(string='Ref #', required=False, copy=False)
    state = fields.Selection([
        ('draft', 'Draft'),
        ('compute', 'Compute'),
        ('payslip_generated', 'Payslip Generated')
    ], 'Status', index=True, readonly=True, copy=False,
        track_visibility='onchange')
    payroll_batche = fields.Many2one('payroll.batches', 'Payroll Batches')
    company_id = fields.Many2one(
        'res.company', 'Company', default=lambda self: self.env.user.company_id
    )
    fetch_active = fields.Boolean("Active", compute="_active")

    @api.model
    def create(self, vals):
        vals['name'] = self.env['ir.sequence'].next_by_code('hr.payslip.run')
        result = super(hr_payroll_run, self).create(vals)
        return result

    @api.multi
    def fetch_payroll_batch(self):
        payslip_obj = self.env['hr.payslip']
        contract_obj = self.env['hr.contract']
        for rec in self:
            rec.slip_ids.unlink()
            # Validate that payslip batch is selected
            if not rec.payroll_batche:
                raise ValidationError("Please select a Payroll Batch!")

            date_from = rec.date_start
            date_to = rec.date_end
            for employee in rec.payroll_batche.batch_employee_id:
                contract = contract_obj.search([
                    ('employee_id', '=', employee.id),
                    ('status1', 'not in', ['close_contract', 'terminated'])
                ])
                if contract:
                    payslip = payslip_obj.onchange_employee_id(
                        date_from, date_to, employee_id=employee.id,
                        contract_id=contract.id)
                else:
                    raise ValidationError(
                        'Please create the contract of employees first: %s' % employee.name)
                payslip_data = {
                    'employee_id': employee.id,
                    'name': payslip['value']['name'],
                    'contract_id': payslip['value']['contract_id'],
                    'struct_id': payslip['value']['struct_id'],
                    'payslip_run_id': rec.id,
                    'company_id': payslip['value']['company_id'],
                    'date_from': rec.date_start,
                    'date_to': rec.date_end,
                    'journal_id': rec.journal_id.id,
                    'input_line_ids': [(0, 0, x) for x in payslip['value'].get('input_line_ids')],
                    'worked_days_line_ids': [(0, 0, x) for x in payslip['value'].get('worked_days_line_ids')],
                    'line_ids': payslip['value']['line_ids'],
                }
                payslip_obj.create(payslip_data)

    @api.multi
    def compute_payslip_run(self):
        for record_id in self:
            if record_id.slip_ids:
                for line_id in record_id.slip_ids:
                    if line_id:
                        line_id.compute_sheet()
        return self.write({'state': 'draft'})

    @api.multi
    def action_generate_payslips(self):
        for record in self.mapped('slip_ids'):
            record.compute_sheet()
        self.write({'state': 'payslip_generated'})


class InvestmentForm(models.Model):
    _name = 'investment.form'
    _inherit = 'mail.thread'
    _rec_name = 'employee_id'

    @api.multi
    def _get_head_investment_sec_80_c(self):
        val = []
        for record in self.env['investment.schemes.conf'].search([('section', '=', 'Section 80C')]):
            sec_c_lines = self.env['declaration.line'].create({
                'scheme': record.id,
                'investment_form_sec_c_id': self.id
            })
            val.append(sec_c_lines.id)
        return val

    @api.multi
    def _get_head_investment_sec_80_d(self):
        val = []
        for record in self.env['investment.schemes.conf'].search([('section', '=', 'Section 80D')]):
            sec_d_lines = self.env['declaration.line'].create({
                'scheme': record.id,
                'investment_form_sec_d_id': self.id
            })
            val.append(sec_d_lines.id)
        return val

    @api.multi
    def _get_head_investment_sec_80_ccg(self):
        val = []
        for record in self.env['investment.schemes.conf'].search([('section', '=', 'Section 80CCG')]):
            sec_ccg_lines = self.env['declaration.line'].create({
                'scheme': record.id,
                'investment_form_sec_ccg_id': self.id
            })
            val.append(sec_ccg_lines.id)
        return val

    @api.multi
    def _get_head_investment_other_sec(self):
        val = []
        for record in self.env['investment.schemes.conf'].search([('section', '=', 'Other Sections')]):
            other_sec_lines = self.env['declaration.line'].create({
                'scheme': record.id,
                'investment_form_sec_other_sec_id': self.id
            })
            val.append(other_sec_lines.id)
        return val

    @api.multi
    def _get_exemption(self):
        dict1 = ['Conveyance', 'House Rent Allowance',
                 'Leave Encashment', 'Leave Amount', 'Standard Allowance', 'Medical Allowance', 'Professional Tax']
        dict2 = []
        for record in dict1:
            obj = self.env['exemption.exemption'].create({
                'description': record,
            })
            dict2.append(obj.id)
        return dict2

    name = fields.Char(string='Reference #', copy=False)
    employee_id = fields.Many2one(
        'hr.employee', 'Employee',
        default=lambda self: self.env['hr.employee'].search([('user_id', '=', self.env.uid)], limit=1))
    gender = fields.Selection('Gender', related='employee_id.gender')
    pan_no = fields.Char('PAN', related='employee_id.identification_id')
    date_of_joining = fields.Date(
        'Date of Joining', related='employee_id.date_of_joining')
    dob = fields.Date('Date of Birth', related='employee_id.birthday')
    location = fields.Many2one('hr.location', related='employee_id.location')
    financial_year = fields.Many2one('account.fiscal.year', 'Financial Year')
    start_date = fields.Date('Start Date')
    end_date = fields.Date(string='End Date')
    declaration_line_id_sec_c = fields.One2many(
        'declaration.line', 'investment_form_sec_c_id', default=_get_head_investment_sec_80_c)
    declaration_line_id_sec_d = fields.One2many(
        'declaration.line', 'investment_form_sec_d_id', default=_get_head_investment_sec_80_d)
    declaration_line_id_sec_ccg = fields.One2many(
        'declaration.line', 'investment_form_sec_ccg_id', default=_get_head_investment_sec_80_ccg)
    declaration_line_id_other_sec = fields.One2many(
        'declaration.line', 'investment_form_sec_other_sec_id', default=_get_head_investment_other_sec)

    exemptions_ids = fields.One2many(
        'exemption.exemption', 'investment_exemption_id', default=_get_exemption)
    prev_employment_ids = fields.One2many(
        'previous.employment', 'investment_id')
    home_loan_interest = fields.Float(string="Interest Paid on Home Loan")
    income_other_sources = fields.One2many(
        'declaration.line', 'investment_form_other_sources')
    investment_months = fields.One2many('investment.month', 'investment_id')
    tds_deductions_ids = fields.One2many(
        'investment.month', 'investment_tds_deduction_id')
    contract_ids = fields.One2many('employee.contract', 'investment_id')
    total_income_salary = fields.Float()
    gross_total_income = fields.Float()
    deductions = fields.Float("Total Chapter VIA Investment")
    gross_taxable_income = fields.Float()
    total_exemption = fields.Float()
    net_tds = fields.Float("Net TDS")
    is_previous_employment = fields.Boolean()
    attachment = fields.Binary('Attachments')
    state = fields.Selection([
        ('draft', 'Draft'),
        ('awaiting_approval', 'Awaiting Approval'),
        ('approved', 'Approved'),
        ('awaiting_final_declaration', 'Awaiting Final Declaration'),
        ('closed', 'Closed')], 'State', default='draft', track_visibility='onchange')

    @api.multi
    def check_employee(self):
        for investment in self:
            record = self.search([
                ('employee_id', '=', investment.employee_id.id),
                ('start_date', '=', investment.start_date),
                ('state', '!=', 'closed')
            ])
            if record:
                record_lst = record.ids
                record_lst.remove(self.id)
                if record_lst:
                    return False
        return True

    @api.multi
    def check_date(self):
        lst = []
        for rec in self:
            if len(rec.prev_employment_ids) == 1:
                for line in rec.prev_employment_ids:
                    if line.start_date >= rec.start_date and line.end_date <= rec.end_date:
                        return True
                    else:
                        return False
            elif len(rec.prev_employment_ids) > 1:
                previous_employer_ids = self.env['previous.employment'].search([
                    ('investment_id', '=', self.id)
                ])
                for obj in previous_employer_ids:
                    lst.append(obj)
                for record in lst:
                    if lst[-1].start_date >= record.start_date and lst[-1].end_date <= record.end_date and \
                            lst[-1].start_date >= rec.start_date and lst[-1].end_date <= rec.end_date:
                        return False
                    elif lst[-1].start_date > record.start_date and lst[-1].end_date > record.end_date and \
                            lst[-1].start_date >= rec.start_date and lst[-1].end_date <= rec.end_date:
                        if lst[-1].start_date >= record.start_date and lst[-1].start_date <= record.end_date:
                            return False
                        else:
                            return True
                    else:
                        return False
            return True

    _constraints = [
        (check_employee, 'You can not declare two investment in a year.', []),
        (check_date, 'Start Date and End Date can not overlap and can not out of Fiscal Year.', [])
    ]

    @api.onchange('financial_year')
    def onchange_finacial_year(self):
        self.start_date = self.financial_year.date_start
        self.end_date = self.financial_year.date_stop
        self.is_previous_employment = False
        if self.start_date and self.end_date:
            start_date = datetime.strptime(self.start_date, "%Y-%m-%d")
            end_date = datetime.strptime(self.end_date, "%Y-%m-%d")
            all_months = [dt.strftime("%B") for dt in rrule(
                MONTHLY, dtstart=start_date, until=end_date)]
            total_months = []
            for month in all_months:
                total_months.append({
                    'months': month
                })
            self.investment_months = total_months
            self.tds_deductions_ids = total_months
        if self.date_of_joining >= self.start_date and self.date_of_joining <= self.end_date:
            self.is_previous_employment = True

    @api.onchange('employee_id')
    def onchange_employee_id(self):
        values = []
        for rec in self:
            rec.is_previous_employment = False
            if rec.date_of_joining >= rec.start_date and rec.date_of_joining <= rec.end_date:
                rec.is_previous_employment = True
        contract_ids = self.env['hr.contract'].search([
            ('employee_id', '=', self.employee_id.id)
        ])
        for contract in contract_ids:
            values.append((0, 0, {
                'contract_id': contract.id,
                'state': contract.status1
            }))
        self.contract_ids = values

    @api.model
    def create(self, vals):
        total_months = []
        invest_months = []
        values = []
        vals['name'] = self.env['ir.sequence'].next_by_code('investment.form')
        financial_year = self.env['account.fiscal.year'].search([
            ('id', '=', vals.get('financial_year'))])
        vals['start_date'] = financial_year.date_start
        vals['end_date'] = financial_year.date_stop
        if vals.get('start_date') and vals.get('end_date'):
            start_date = datetime.strptime(vals.get('start_date'), "%Y-%m-%d")
            end_date = datetime.strptime(vals.get('end_date'), "%Y-%m-%d")
            all_months = [dt.strftime("%B") for dt in rrule(
                MONTHLY, dtstart=start_date, until=end_date)]
            if 'investment_months' in vals:
                for count, rec in enumerate(vals['investment_months']):
                    total_months.append((0, 0, {
                        'months': all_months[count],
                        'investment_tds_deduction_id': self.id
                    }))
                    invest_months.append((0, 0, {
                        'months': all_months[count],
                        'amount': rec[2]['amount'],
                        'investment_id': self.id

                    }))
                vals['investment_months'] = invest_months
            vals['tds_deductions_ids'] = total_months
        contract_ids = self.env['hr.contract'].search([
            ('employee_id', '=', vals.get('employee_id'))
        ])
        for contract in contract_ids:
            values.append((0, 0, {
                'contract_id': contract.id,
                'state': contract.status1
            }))
        vals['contract_ids'] = values
        contract = self.env['hr.contract'].search([
            ('employee_id', '=', vals.get('employee_id')),
            ('state', '!=', 'close')
        ])
        result = super(InvestmentForm, self).create(vals)
        exempt_amount = result._get_amount_exemption(contract)
        if exempt_amount:
            for rec in result.exemptions_ids:
                if rec.description == 'Standard Allowance':
                    rec.amount = exempt_amount[0]
                if rec.description == 'Leave Encashment':
                    rec.amount = exempt_amount[1]
                if rec.description == 'House Rent Allowance':
                    rec.amount = exempt_amount[2]
        return result

    @api.multi
    def _get_amount_exemption(self, contract):
        sda = 0
        enchash = 0
        hra = 0
        sum = 0
        condition1 = 0
        actual_rent_paid = 0
        # Leave Encashment
        leave_encashment = 0
        if contract.leave_encashment_applicable:
            leave_encashment_percent = contract.leave_encashment
            leave_encashment = (contract.wage * leave_encashment_percent / 100)
        if contract.joining_date >= self.start_date and contract.joining_date <= self.end_date:
            joining_date = datetime.strptime(contract.joining_date, "%Y-%m-%d")
            date_to = datetime.strptime(self.end_date, "%Y-%m-%d")
            difference = relativedelta.relativedelta(
                date_to, joining_date)
            months = difference.months
            days = calendar.monthrange(
                joining_date.year, joining_date.month)[1]
            # Leave Encashment
            leave_encashment_day = (
                leave_encashment / days) * (days - joining_date.day + 1)
            if leave_encashment <= 300000:
                final_leave_encashment = (
                    leave_encashment * months) + leave_encashment_day
            elif leave_encashment > 300000:
                leave_encashment_day = (300000 / days) * \
                    (days - joining_date.day + 1)
                enchash = (300000 * months) + leave_encashment_day
            enchash = round(final_leave_encashment)
            # HRA
            for loop in contract.salary_allowance_id:
                skip_months = []
                if loop.salary_rule_id.code == 'HRA':
                    # Condition 1
                    hra_day = (loop.amount / days) * \
                        (days - joining_date.day + 1)
                    hra = hra_day + (loop.amount * months)
                    # Condition 2
                    basic_day = (contract.wage / days) * \
                        (days - joining_date.day + 1)
                    condition1 = ((basic_day + (contract.wage * months))
                                  * contract.city_percentage / 100)
                    # Condition 3
                    date_start = datetime.strptime(self.start_date, "%Y-%m-%d")
                    for number in range(date_start.month, joining_date.month):
                        skip_months.append(calendar.month_name[number])
                    for record in self.investment_months:
                        if record.months in skip_months:
                            continue
                        else:
                            sum += record.amount
                    basic_salary = (
                        (basic_day + (contract.wage * months)) * 10 / 100)
                    actual_rent_paid = sum - basic_salary
                previous_company_data = self.calculate_previous_employer(
                    contract)
                if contract.sda_exemption_applicable:
                    # sda_day = (loop.amount / days) * (difference.days + 1)
                    # sda = (sda_day + (loop.amount * months)) +
                    # previous_company_data[2]
                    sda = contract.exempt_amount - previous_company_data[2]
                    if sda >= contract.tds_rule.sda_exemption_limit:
                        sda = contract.tds_rule.sda_exemption_limit
                    hra = min(hra, condition1, actual_rent_paid)
                    if hra < 0:
                        hra = 0
        else:
            for loop in contract.salary_allowance_id:
                # Standard Allowance
                if contract.sda_exemption_applicable:
                    # sda = loop.amount * 12
                    sda = contract.exempt_amount
                    if sda >= contract.tds_rule.sda_exemption_limit:
                        sda = contract.tds_rule.sda_exemption_limit
                # HRA
                if loop.salary_rule_id.code == 'HRA':
                    # condition 1
                    hra = loop.amount * 12
            if leave_encashment > 300000:
                enchash = 3600000
            elif leave_encashment <= 300000:
                enchash = (round(leave_encashment) * 12)
            # condition 2
            condition1 = ((contract.wage * 12) *
                          contract.city_percentage / 100)

            # condition 3
            for record in self.investment_months:
                sum += record.amount
            basic_salary = ((contract.wage * 12) * 10 / 100)
            actual_rent_paid = sum - basic_salary
            # Minimum amoung 3 conditions
            hra = min(hra, condition1, actual_rent_paid)
            if hra < 0:
                hra = 0
        return sda, enchash, hra

    @api.multi
    def write(self, vals):
        values = []
        total_months = []
        invest_months = []
        if 'financial_year' in vals:
            financial_year = self.env['account.fiscal.year'].search([
                ('id', '=', vals.get('financial_year'))])
            vals['start_date'] = financial_year.date_start
            vals['end_date'] = financial_year.date_stop
        if 'employee_id' in vals or 'contract_ids' in vals:
            self.contract_ids.unlink()
            contract_ids = self.env['hr.contract'].search([
                '|',
                ('employee_id', '=', vals.get('employee_id')),
                ('employee_id', '=', self.employee_id.id)
            ])
            for contract in contract_ids:
                values.append((0, 0, {
                    'contract_id': contract.id,
                    'state': contract.status1,
                }))
            vals['contract_ids'] = values
        if vals.get('start_date') and vals.get('end_date'):
            self.tds_deductions_ids.unlink()
            self.investment_months.unlink()
            start_date = datetime.strptime(vals.get('start_date'), "%Y-%m-%d")
            end_date = datetime.strptime(vals.get('end_date'), "%Y-%m-%d")
            all_months = [dt.strftime("%B") for dt in rrule(
                MONTHLY, dtstart=start_date, until=end_date)]
            for month in all_months:
                total_months.append((0, 0, {
                    'months': month,
                    'investment_tds_deduction_id': self.id
                }))
                invest_months.append((0, 0, {
                    'months': month,
                    'investment_id': self.id

                }))
            vals['tds_deductions_ids'] = total_months
            vals['investment_months'] = invest_months
        if vals.get('employee_id') and self.employee_id:
            emp_contract = self.env['hr.contract'].search([
                ('employee_id', '=', vals.get('employee_id')),
                ('status1', 'not in', ['close_contract', 'terminated'])
            ])
        else:
            emp_contract = self.env['hr.contract'].search([
                ('employee_id', '=', self.employee_id.id),
                ('status1', 'not in', ['close_contract', 'terminated'])
            ])
        exempt_amount = self._get_amount_exemption(emp_contract)
        if exempt_amount:
            for rec in self.exemptions_ids:
                if rec.description == 'Standard Allowance':
                    rec.amount = exempt_amount[0]
                if rec.description == 'Leave Encashment':
                    rec.amount = exempt_amount[1]
                if rec.description == 'House Rent Allowance':
                    rec.amount = exempt_amount[2]
        res = super(InvestmentForm, self).write(vals)
        if (('exemptions_ids' in vals) or ('declaration_line_id_sec_c' in vals) or
                ('declaration_line_id_sec_d' in vals) or ('financial_year' in vals) or
                ('declaration_line_id_sec_ccg' in vals) or ('investment_months' in vals) or
                ('declaration_line_id_other_sec' in vals) or ('income_other_sources' in vals) or
                ('prev_employment_ids' in vals) or ('prev_employment_ids' in vals) or
                ('home_loan_interest' in vals)):
            self.calculate_taxable_value()
        return res

    def investment_plans(self, scheme, field, invest_amount):
        sec_total = 0
        section_obj = self.env['config.investment.section']
        section_id = section_obj.search(
            [('name', '=', scheme), ('fiscal_year', '=', self.financial_year.id)])
        for record in field:
            if record.actual_amount:
                if record.scheme.exemption_applicable:
                    if record.actual_amount > record.scheme.exemption_limit:
                        invest_amount += record.scheme.exemption_limit
                    else:
                        invest_amount += record.actual_amount
                else:
                    invest_amount += record.actual_amount
            else:
                if record.scheme.exemption_applicable:
                    if record.proposed_amount > record.scheme.exemption_limit:
                        invest_amount += record.scheme.exemption_limit
                    else:
                        invest_amount += record.proposed_amount
                else:
                    invest_amount += record.proposed_amount
                if invest_amount >= section_id.max_exemption:
                    sec_total = section_id.max_exemption
                else:
                    sec_total = invest_amount
        return sec_total

    @api.multi
    def calculate_taxable_value(self):
        for order in self:
            total = 0
            sec_c_amount = 0
            sec_d_amount = 0
            sec_ccg_amount = 0
            sec_other_amount = 0
            contract_obj = self.env['hr.contract'].search([
                ('employee_id', '=', order.employee_id.id),
                ('state', '!=', 'close')
            ])

            # Bonus
            bonus = 0
            if contract_obj.bonus_applicable:
                bonus_percent = contract_obj.bonus
                bonus = (contract_obj.wage * bonus_percent / 100)
            # Leave Encashment
            leave_encashment = 0
            if contract_obj.leave_encashment_applicable:
                leave_encashment_percent = contract_obj.leave_encashment
                leave_encashment = (contract_obj.wage *
                                    leave_encashment_percent / 100)

            # Calculating months
            # total_income_salary
            remaining_gross = 0
            if contract_obj.joining_date >= order.start_date and contract_obj.joining_date <= order.end_date:
                joining_date = datetime.strptime(
                    contract_obj.joining_date, "%Y-%m-%d")
                date_to = datetime.strptime(order.end_date, "%Y-%m-%d")
                difference = relativedelta.relativedelta(date_to, joining_date)
                months = difference.months
                days = calendar.monthrange(
                    joining_date.year, joining_date.month)[1]
                salary_day = (contract_obj.gross_sal / days) * \
                    (days - joining_date.day + 1)
                bonus_day = (bonus / days) * (days - joining_date.day + 1)
                leave_encashment_day = (
                    leave_encashment / days) * (days - joining_date.day + 1)
                for line in contract_obj.salary_allowance_id:
                    if line.salary_rule_id.category_id.code == 'ALW':
                        remaining_gross += line.amount
                gross_salary = (
                    (contract_obj.wage + remaining_gross) * months) + salary_day
                final_bonus = (bonus * months) + bonus_day
                final_leave_encashment = (
                    leave_encashment * months) + leave_encashment_day
                order.total_income_salary = round(
                    gross_salary) + round(final_bonus) + round(final_leave_encashment)
                monthly_basic = (contract_obj.wage / days) * \
                    (difference.days + 1)
                if monthly_basic >= 15000:
                    epf = 1800
                else:
                    epf = monthly_basic * 0.12
                if contract_obj.wage >= 15000:
                    remaining_epf = 1800
                else:
                    remaining_epf = contract_obj.wage * 0.12
                remaining_mnt_epf = remaining_epf * months
                total_epf = remaining_mnt_epf + epf
                for line in order.declaration_line_id_sec_c:
                    if "Provident Fund (PF)" in line.scheme.name:
                        line.proposed_amount = total_epf
            else:
                for line in contract_obj.salary_allowance_id:
                    if line.salary_rule_id.category_id.code == 'ALW':
                        remaining_gross += line.amount
                order.total_income_salary = (((contract_obj.wage + remaining_gross) * 12) +
                                             (round(bonus) * 12) + (round(leave_encashment) * 12))
                if contract_obj.wage >= 15000:
                    epf = 1800
                else:
                    epf = contract_obj.wage * 0.12
                epf = round(epf * 12)
                for line in order.declaration_line_id_sec_c:
                    if "Provident Fund (PF)" in line.scheme.name:
                        line.proposed_amount = epf

            # gross_total_income
            for record in order.income_other_sources:
                if record.actual_amount:
                    total += record.actual_amount
                else:
                    total += record.proposed_amount
            order.gross_total_income = (order.total_income_salary + total)

            # Exemption
            sum = 0
            for rec in order.exemptions_ids:
                sum += rec.amount
            order.total_exemption = sum

            # deductions
            sec_total = order.investment_plans(
                'Section 80C', order.declaration_line_id_sec_c, sec_c_amount)

            sec_total1 = order.investment_plans(
                'Section 80D', order.declaration_line_id_sec_d, sec_d_amount)
            sec_total2 = order.investment_plans(
                'Section 80CCG', order.declaration_line_id_sec_ccg, sec_ccg_amount)

            sec_total3 = order.investment_plans(
                'Other Sections', order.declaration_line_id_other_sec, sec_other_amount)

            order.deductions = sec_total + sec_total1 + \
                sec_total2 + sec_total3 + order.home_loan_interest
            previous_taxable_salary = self.calculate_previous_employer(
                contract_obj)
            order.gross_taxable_income = round(
                order.gross_total_income + previous_taxable_salary[0] - order.deductions - order.total_exemption)
        return True

    @api.multi
    def action_calculate_tds(self):
        for record in self:
            contract_obj = self.env['hr.contract'].search([
                ('employee_id', '=', record.employee_id.id),
                ('status1', 'not in', ['close_contract', 'terminated'])
            ])
            if contract_obj:
                tax = 0
                for line in contract_obj.tds_rule.rule_lines:
                    if (record.gross_taxable_income > line.from_amount and
                            record.gross_taxable_income < line.to_amount or line.to_amount == -1):
                        tax = tax + ((record.gross_taxable_income -
                                      line.from_amount) * line.percentage / 100)
                        break
                    else:
                        tax = tax + \
                            ((line.to_amount - line.from_amount)
                             * line.percentage / 100)
                cess = tax * contract_obj.tds_rule.cess / 100
                tds = cess + tax
                previous_taxable_salary = self.calculate_previous_employer(
                    contract_obj)
                record.net_tds = float(round(tds) + previous_taxable_salary[1])
                # Rebate
                if record.gross_taxable_income < 350000:
                    record.net_tds -= contract_obj.tds_rule.rebate_amount
                    if record.net_tds < 0:
                        record.net_tds = 0
        return True

    @api.multi
    def calculate_previous_employer(self, contract):
        previous_taxable_salary = 0
        taxable_salary = 0
        total_exemptions = 0
        hra = 0
        sda = 0
        tds = 0
        other = 0
        if contract.joining_date >= self.start_date and contract.joining_date <= self.end_date:
            for line in self.prev_employment_ids:
                taxable_salary += line.amount_paid
                hra += line.hra_exemption
                sda += line.sda_exemption
                other += line.other_exemption
                tds += line.tds_paid
            total_exemptions = hra + sda
            previous_taxable_salary = taxable_salary - total_exemptions
        return previous_taxable_salary, tds, sda

    @api.multi
    def unlink(self):
        for rec in self:
            if rec.state not in ['draft']:
                raise Warning(
                    _('You can delete a Investment Declaration only in draft state.'))
            else:
                for record in rec.exemptions_ids:
                    record.unlink()
        return super(InvestmentForm, self).unlink()

    @api.multi
    def action_awaiting(self):
        self.write({'state': 'awaiting_approval'})
        return True

    @api.multi
    def action_approve(self):
        self.write({'state': 'approved'})
        return True

    @api.multi
    def action_final_investment(self):
        self.write({'state': 'awaiting_final_declaration'})
        return True

    @api.multi
    def action_submit(self):
        self.write({'state': 'closed'})
        return True

    @api.multi
    def action_reset_to_draft(self):
        self.write({'state': 'draft'})
        return True


class InvestmentMonths(models.Model):
    _name = 'investment.month'

    investment_id = fields.Many2one('investment.form')
    investment_tds_deduction_id = fields.Many2one('investment.form')
    months = fields.Char()
    amount = fields.Float()
    attachment = fields.Binary('Attachments')


class HrPayrollStructure(models.Model):
    _inherit = 'hr.payroll.structure'

    leave_encashment = fields.Float("Leave Encashment(%)")
    bonus = fields.Float("Bonus(%)")

    @api.multi
    def write(self, vals):
        if 'rule_ids' in vals:
            rule = list()
            salary_rule_list = []
            contracts = self.env['hr.contract'].search(
                [('struct_id', '=', self.id)])
            allowance_obj = self.env['allowance.allowance']
            deduction_obj = self.env['deduction.deduction']
            for rec in self.rule_ids:
                rule.append(rec.id)
            salary_rule_list.extend(vals['rule_ids'][0][-1])
            rule_create_list = list((set(salary_rule_list) - set(rule)))
            rule_delete_list = list((set(rule) - set(salary_rule_list)))
            for value in rule_create_list:
                salary_rule = self.env['hr.salary.rule'].browse(value)
                for contract in contracts:
                    if salary_rule.category_id.name == 'Allowance':
                        allowance_obj.create({
                            'salary_rule_id': salary_rule.id,
                            'amount': 0.0,
                            'hr_contract_id': contract.id
                        })
                    else:
                        deduction_obj.create({
                            'salary_rule_id_ded': salary_rule.id,
                            'amount': 0.0,
                            'hr_contract_id': contract.id
                        })
            for record in rule_delete_list:
                salary_rule = self.env['hr.salary.rule'].browse(record)
                for contract in contracts:
                    if salary_rule.category_id.name == 'Allowance':
                        allowance_obj.search([
                            ('salary_rule_id', '=', salary_rule.id),
                            ('hr_contract_id', '=', contract.id)]).unlink()
                    else:
                        deduction_obj.search([
                            ('salary_rule_id_ded', '=', salary_rule.id),
                            ('hr_contract_id', '=', contract.id)]).unlink()
        return super(HrPayrollStructure, self).write(vals)


class HrSalaryRule(models.Model):
    _inherit = 'hr.salary.rule'

    # We are using this field only for representation. This field is not used
    # in any TDS calculation.
    tds_deductable = fields.Boolean("TDS Deductable")


class DeclarationLine(models.Model):
    """Declaration Line."""

    _name = "declaration.line"

    investment_form_sec_c_id = fields.Many2one('investment.form')
    investment_form_sec_d_id = fields.Many2one('investment.form')
    investment_form_sec_ccg_id = fields.Many2one('investment.form')
    investment_form_sec_other_sec_id = fields.Many2one('investment.form')
    investment_form_other_sources = fields.Many2one('investment.form')
    scheme = fields.Many2one('investment.schemes.conf', store=True)
    scheme_new = fields.Char('Income Source')
    actual_amount = fields.Float('Actual Amount')
    proposed_amount = fields.Float('Proposed Amount')
    remarks = fields.Char('Remarks')
    attachment = fields.Binary('Attachments')


class ConfigInvestment(models.Model):

    _name = "config.investment.section"

    name = fields.Char()
    max_exemption = fields.Float("Max Exemption")
    fiscal_year = fields.Many2one('account.fiscal.year')


class InvestmentSchemesConf(models.Model):

    _name = "investment.schemes.conf"

    name = fields.Char()
    section = fields.Many2one('config.investment.section')
    exemption_applicable = fields.Boolean("Exemption Applicable")
    exemption_limit = fields.Integer()


class PayrollBatche(models.Model):
    _name = 'payroll.batches'

    state = fields.Selection([
        ('draft', 'Draft'),
        ('awaiting_approval', 'Awaiting Approval'),
        ('approve', 'Approved'),
    ], string='Status', index=True, readonly=True, copy=False, default='draft')

    name = fields.Char('Name')
    batch_employee_id = fields.Many2many(
        'hr.employee',
        'rel_payroll_batch_employee',
        'batch_id',
        'batch_employee_id')

    @api.multi
    def submit_button(self):
        if self.state == 'draft':
            self.write({'state': 'awaiting_approval'})

    @api.multi
    def approve_button(self):
        for rec in self.batch_employee_id:
            employee_batches = self.env['hr.employee'].search(
                [('id', '=', rec.id)])
            for record in employee_batches:
                employee_batches.write({
                    'payroll_batche': self.id
                })
        if self.state == 'awaiting_approval':
            self.write({'state': 'approve'})

    @api.multi
    def unlink(self):
        """Payroll Batches to delete hr draft"""
        for rec in self:
            if rec.state == 'approve':
                raise UserError(
                    'You can only delete on Draft state')
        return super(PayrollBatche, self).unlink()


class BatchEmployee(models.Model):
    _name = 'batch.employee'

    payroll_batch_id = fields.Many2one('payroll.batches')
    employee_id = fields.Many2one('hr.employee', 'Employee')


class TDSSalbs(models.Model):
    _name = 'tds.slabs'

    tds_rule = fields.Many2one('tds.rule')
    from_amount = fields.Integer("From Amount")
    to_amount = fields.Integer("To Amount")
    percentage = fields.Float("Percentage")


class TDSRule(models.Model):
    _name = 'tds.rule'

    name = fields.Char(string='Rule Name')
    fiscal_year_id = fields.Many2one('account.fiscal.year', "Fiscal Year")
    tax_slab_type = fields.Many2one('tax.slab', "Tax Slab Type")
    cess = fields.Integer(string='Cess %')
    rebate_amount = fields.Integer("Rebate Amount")
    sda_exemption_limit = fields.Integer("Sda Exemption Limit")
    rule_lines = fields.One2many('tds.slabs', 'tds_rule')

    @api.constrains('fiscal_year_id', 'tax_slab_type')
    def _check_date_overlap(self):
        for record in self:
            date_ids = self.search([
                ('fiscal_year_id', '=', self.fiscal_year_id.id),
                ('tax_slab_type', '=', self.tax_slab_type.id)
            ])
            for line in date_ids:
                if record != line:
                    raise UserError(
                        _('Tax Slab is already created for this fiscal year with this category.'))


class Employee(models.Model):
    _inherit = "hr.employee"

    @api.multi
    def get_amount(self, basic, allowance, deduction, payslip):
        tax = 0
        sec_c_amount = 0
        sec_d_amount = 0
        sec_ccg_amount = 0
        sec_other_amount = 0
        total_investment_ded = 0
        self.ensure_one()
        investment_obj = self.env['investment.form']
        investment_id = investment_obj.search([
            ('employee_id.id', '=', payslip.employee_id),
            ('start_date', '<=', payslip.date_from),
            ('end_date', '>=', payslip.date_to),
            ('state', '!=', 'closed')
            # ('state', '=', 'approved')
        ])
        previous_payslip = []
        previous_payslip_date = datetime.strptime(
            payslip.date_from, "%Y-%m-%d") - relativedelta.relativedelta(months=1)
        fiscal_start_date = datetime.strptime(
            investment_id.start_date, "%Y-%m-%d")
        fiscal_end_date = datetime.strptime(investment_id.end_date, "%Y-%m-%d")
        if previous_payslip_date >= fiscal_start_date and previous_payslip_date <= fiscal_end_date:
            previous_payslip = self.env['hr.payslip'].search([
                ('employee_id', '=', payslip.employee_id),
                ('date_from', '=', previous_payslip_date)
            ], limit=1)
        current_payslip = self.env['hr.payslip'].search([
            ('employee_id', '=', payslip.employee_id),
            ('date_from', '=', payslip.date_from),
            ('date_to', '=', payslip.date_to)
        ], limit=1)
        work_days = 0
        payslip_month = datetime.strptime(
            payslip.date_from, "%Y-%m-%d").strftime("%B")
        total_exemption = self.calculate_live_exemption(
            payslip.contract_id, payslip, investment_id, basic, previous_payslip, current_payslip)
        previous_company_data = investment_id.calculate_previous_employer(
            payslip.contract_id)
        net_amount = round(basic) + allowance - total_exemption[0]
        if previous_payslip:
            net_amount = net_amount + previous_payslip.remaining_months_gross
        # Working days
        for rec in payslip.worked_days_line_ids:
            work_days += rec.number_of_days
        current_payslip.remaining_months_gross = net_amount
        if previous_company_data[0]:
            net_amount = net_amount + previous_company_data[0]
        # Bonus
        bonus = 0
        if payslip.contract_id.bonus_applicable:
            bonus_percent = payslip.contract_id.bonus
            bonus = (payslip.contract_id.wage * bonus_percent / 100)
        # Leave Encashment
        leave_encashment = 0
        if payslip.contract_id.leave_encashment_applicable:
            leave_encashment_percent = payslip.contract_id.leave_encashment
            leave_encashment = (payslip.contract_id.wage *
                                leave_encashment_percent / 100)
        current_month = datetime.strptime(payslip.date_from, "%Y-%m-%d").month
        if current_month in (1, 2, 3):
            remaining_months = 3 - current_month
        else:
            remaining_months = (13 - current_month) + 2
        remaining_gross = 0
        for line in payslip.contract_id.salary_allowance_id:
            if line.salary_rule_id.category_id.code == 'ALW':
                remaining_gross += line.amount
        total_remaining_gross = payslip.contract_id.wage + \
            bonus + leave_encashment + remaining_gross
        remaining_net_amount = (
            total_remaining_gross * remaining_months) - total_exemption[1]
        amount = round(remaining_net_amount + net_amount)
        # gross_total_income
        total = 0
        for record in investment_id.income_other_sources:
            if record.actual_amount:
                total += record.actual_amount
            else:
                total += record.proposed_amount
        amount = amount + total
        sec_total = investment_id.investment_plans(
            'Section 80C', investment_id.declaration_line_id_sec_c, sec_c_amount)

        sec_total1 = investment_id.investment_plans(
            'Section 80D', investment_id.declaration_line_id_sec_d, sec_d_amount)

        sec_total2 = investment_id.investment_plans(
            'Section 80CCG', investment_id.declaration_line_id_sec_ccg, sec_ccg_amount)

        sec_total3 = investment_id.investment_plans(
            'Other Sections', investment_id.declaration_line_id_other_sec, sec_other_amount)
        total_investment_ded = sec_total + sec_total1 + sec_total2 + sec_total3
        amount = amount - total_investment_ded - investment_id.home_loan_interest
        for line in payslip.tds_rule.rule_lines:
            if amount > line.from_amount and amount < line.to_amount or line.to_amount == -1:
                tax = tax + ((amount - line.from_amount)
                             * line.percentage / 100)
                break
            else:
                tax = tax + ((line.to_amount - line.from_amount)
                             * line.percentage / 100)
        cess = tax * payslip.tds_rule.cess / 100
        tds = cess + tax
        annual_tds = float(round(tds))
        # Rebate
        if amount < 350000:
            annual_tds -= payslip.tds_rule.rebate_amount
            if annual_tds < 0:
                annual_tds = 0
        already_deducted_tds = 0.00
        for record in investment_id.tds_deductions_ids:
            if record.months == payslip_month:
                break
            else:
                already_deducted_tds += record.amount
        # Calculated the Net TDS to be deducted in remaining year
        if previous_company_data[1]:
            tds_to_be_deducted = annual_tds - \
                already_deducted_tds - previous_company_data[1]
        else:
            tds_to_be_deducted = annual_tds - already_deducted_tds

        # Calculate remaining months in current FY
        current_month = datetime.strptime(payslip.date_from, "%Y-%m-%d").month
        if current_month in (1, 2, 3):
            remaining_months = 3 - current_month
        else:
            remaining_months = (13 - current_month) + 2
        day_from = datetime.strptime(payslip.date_from, "%Y-%m-%d")
        day_to = datetime.strptime(payslip.date_to, "%Y-%m-%d")
        if payslip.contract_id.pay_day_calculation == 'days_in_month':
            total_worked_days = relativedelta.relativedelta(
                day_to, day_from).days + 1
        else:
            total_worked_days = payslip.contract_id.pay_days
        if total_worked_days == 31:
            total_days = 31 * remaining_months
        elif total_worked_days == 30:
            total_days = 30 * remaining_months
        else:
            total_days = total_worked_days * remaining_months
        daily_tds = tds_to_be_deducted / (total_days + work_days)
        monthly_tds = round(daily_tds * work_days)
        for line in investment_id.tds_deductions_ids:
            if line.months == payslip_month:
                line.amount = monthly_tds
        return monthly_tds

    def calculate_live_exemption(self, contract, payslip, investment_id, basic, previous_payslip, current_payslip):
        work_days = 0
        total_exemption = 0
        payslip_month = datetime.strptime(
            payslip.date_from, "%Y-%m-%d").strftime("%B")
        for rec in payslip.worked_days_line_ids:
            work_days += rec.number_of_days
        current_month = datetime.strptime(payslip.date_from, "%Y-%m-%d").month
        if current_month in (1, 2, 3):
            remaining_months = 3 - current_month
        else:
            remaining_months = (13 - current_month) + 2
        for allowance in contract.salary_allowance_id:
            if allowance.salary_rule_id.code == 'HRA':
                total_rent_paid = 0
                hra_amount = allowance.amount
                # Condition1
                hra = self.get_hra_with_workdays(
                    hra_amount, work_days, payslip)
                # Condition2
                condition1 = (
                    (basic) * payslip.contract_id.city_percentage / 100)
                # Condition3
                for rent_paid in investment_id.investment_months:
                    if strptime(payslip_month, '%B').tm_mon == 12:
                        month = 1
                    else:
                        month = strptime(payslip_month, '%B').tm_mon + 1
                    if strptime(payslip_month, '%B').tm_mon == strptime(rent_paid.months, '%B').tm_mon:
                        total_rent_paid = rent_paid.amount
                    if month == strptime(rent_paid.months, '%B').tm_mon:
                        remaining_rent = rent_paid.amount
                hra_rent_paid = total_rent_paid - ((basic) * 0.10)
                hra_exempt = min(hra, condition1, hra_rent_paid)
                if hra_exempt < 0:
                    hra_exempt = 0
                remaining_hra_amount = hra_amount * remaining_months
                remaining_hra_cn1 = (
                    (contract.wage * remaining_months) * contract.city_percentage / 100)
                hra_rentpaid = remaining_rent * remaining_months - \
                    ((contract.wage * remaining_months) * 0.10)
                remaining_hra_rent_paid = min(
                    remaining_hra_amount, remaining_hra_cn1, hra_rentpaid)
                if remaining_hra_rent_paid < 0:
                    remaining_hra_rent_paid = 0
        if contract.sda_exemption_applicable:
            previous_payslip_sda = 0
            sda_amount = contract.exempt_amount
            monthly_sda_amount = sda_amount / 12
            sda = self.get_sda_with_workdays(
                monthly_sda_amount, work_days, payslip)
            remaining_sda = sda_amount * remaining_months
            previous_company_sda = investment_id.calculate_previous_employer(
                contract)
            if previous_payslip:
                previous_payslip_sda = previous_payslip.total_sda_exempt
            total_sda = sda + remaining_sda + \
                previous_company_sda[2] + previous_payslip_sda
            if total_sda >= 40000:
                total_sda = 40000
            actual_sda = total_sda - \
                previous_company_sda[2] - previous_payslip_sda
            day_from = datetime.strptime(payslip.date_from, "%Y-%m-%d")
            day_to = datetime.strptime(payslip.date_to, "%Y-%m-%d")
            if payslip.contract_id.pay_day_calculation == 'days_in_month':
                total_worked_days = relativedelta.relativedelta(
                    day_to, day_from).days + 1
            else:
                total_worked_days = payslip.contract_id.pay_days
            if total_worked_days == 31:
                total_days = 31 * remaining_months
            elif total_worked_days == 30:
                total_days = 30 * remaining_months
            else:
                total_days = total_worked_days * remaining_months
            total_days = total_days + work_days
            daily_sda = actual_sda / total_days
            sda = round(daily_sda * work_days)
            current_payslip.total_sda_exempt = sda + previous_payslip_sda
            remaining_sda = (actual_sda - sda)
            leave_encashment = 0
            if contract.leave_encashment_applicable:
                leave_encashment = basic * contract.leave_encashment / 100
            remaining_leave_encash = leave_encashment * remaining_months
        total_exemption = hra_exempt + sda + leave_encashment
        remaining_exempt = remaining_hra_rent_paid + \
            remaining_sda + remaining_leave_encash
        return total_exemption, remaining_exempt

    def yearly_or_from_joining_date(self, payslip, investment_obj):
        joining_date = datetime.strptime(
            payslip.contract_id.joining_date, "%Y-%m-%d")
        date_to = datetime.strptime(investment_obj.end_date, "%Y-%m-%d")
        difference = relativedelta.relativedelta(date_to, joining_date)
        months = difference.months
        return months

    @api.multi
    def get_professional_tax(self, basic, allowance, allowance_alwt, payslip):
        gross_sal = basic + allowance + allowance_alwt
        professional_tax = 0
        for line in payslip.professional_tax_rule_id.pt_rule_lines:
            if gross_sal >= line.from_amount and gross_sal <= line.to_amount:
                professional_tax = line.amount
            elif gross_sal >= line.from_amount and line.to_amount == -1:
                professional_tax = line.amount
        return professional_tax

    @api.multi
    def get_employee_labour_fund(self, gross, payslip):
        return payslip.professional_tax_rule_id.employee_contribution

    @api.multi
    def get_employer_labour_fund(self, gross, payslip):
        return payslip.professional_tax_rule_id.employer_contribution

    @api.multi
    def get_esic_applicable(self, gross, payslip):
        esic = 0
        financial_year = self.env['account.fiscal.year'].search([
            ('id', '!=', self.id)])
        start_date = datetime.strptime(
            financial_year.date_start, "%Y-%m-%d")
        end_date = datetime.strptime(
            financial_year.date_stop, "%Y-%m-%d")
        all_months = [dt.strftime("%B") for dt in rrule(
            MONTHLY, dtstart=start_date, until=end_date)]
        fi_half_months = ''
        se_half_months = []
        fi_half_months = all_months[0:6]
        se_half_months = all_months[7:13]
        if payslip.contract_id.esic_applicable:
            if gross <= financial_year.esci_employee_limit:
                esic = gross * financial_year.esci_employee
                return esic / 100
            else:
                return False
        elif not payslip.contract_id.esic_applicable:
            date_from = datetime.strptime(
                payslip.date_from, "%Y-%m-%d")
            current_st_months = date_from.strftime("%B")
            months_data = ''
            months_data = current_st_months[0:9]
            if payslip.contract_id.first_half == True:
                if months_data in fi_half_months:
                    if payslip.contract_id.doc_con_ref.gross_sal <= financial_year.esci_employee_limit:
                        esic = payslip.contract_id.doc_con_ref.gross_sal * financial_year.esci_employee
                        return esic / 100
                    else:
                        return False
            elif payslip.contract_id.second_half == True:
                if months_data in se_half_months:
                    if payslip.contract_id.doc_con_ref.gross_sal <= financial_year.esci_employee_limit:
                        esic = payslip.contract_id.doc_con_ref.gross_sal * financial_year.esci_employee
                        return esic / 100
                    else:
                        return False
                return False
            return False
        return False

    @api.multi
    def get_emic_applicable(self, gross, payslip):
        esic = 0
        financial_year = self.env['account.fiscal.year'].search([
            ('id', '!=', self.id)])
        start_date = datetime.strptime(
            financial_year.date_start, "%Y-%m-%d")
        end_date = datetime.strptime(
            financial_year.date_stop, "%Y-%m-%d")
        all_months = [dt.strftime("%B") for dt in rrule(
            MONTHLY, dtstart=start_date, until=end_date)]
        fi_half_months = ''
        se_half_months = []
        fi_half_months = all_months[0:6]
        se_half_months = all_months[7:13]
        if payslip.contract_id.esic_applicable:
            if gross <= financial_year.esci_employee_limit:
                esic = gross * financial_year.esci_employer
                return esic / 100
            else:
                return False
        elif not payslip.contract_id.esic_applicable:
            date_from = datetime.strptime(
                payslip.date_from, "%Y-%m-%d")
            current_st_months = date_from.strftime("%B")
            months_data = ''
            months_data = current_st_months[0:9]
            if payslip.contract_id.first_half == True:
                if months_data in fi_half_months:
                    if payslip.contract_id.doc_con_ref.gross_sal <= financial_year.esci_employee_limit:
                        esic = payslip.contract_id.doc_con_ref.gross_sal * financial_year.esci_employer
                        return esic / 100
                    else:
                        return False
            elif payslip.contract_id.second_half == True:
                if months_data in se_half_months:
                    if payslip.contract_id.doc_con_ref.gross_sal <= financial_year.esci_employee_limit:
                        esic = payslip.contract_id.doc_con_ref.gross_sal * financial_year.esci_employer
                        return esic / 100
                    else:
                        return False
                return False
            return False
        return False

    @api.multi
    def get_epf_applicable(self, basic, payslip):
        epf = 0
        arrears = 0
        financial_year = self.env['account.fiscal.year'].search([
            ('id', '!=', self.id)])
        if payslip.contract_id.epf_applicable:
            if basic <= financial_year.epf_employee_limit:
                epf = financial_year.epf_employee_limit * financial_year.epf_employee
            for rec in payslip.input_line_ids:
                if rec.code == 'EPFAAR':
                    arrears += rec.amount
            if (basic + arrears) <= financial_year.epf_employee_limit:
                epf = (basic + arrears) * financial_year.epf_employee
                # return epf / 100
        elif payslip.contract_id.epf_applicable_check:
            if payslip.contract_id.epf_applicable_pure:
                if basic > financial_year.epf_employee_limit:
                    epf = financial_year.epf_employee_limit * financial_year.epf_employee
                    # return epf / 100
            elif payslip.contract_id.epf_applicable_actual:
                if basic > financial_year.epf_employee_limit:
                    epf = basic * financial_year.epf_employee
                    # return epf / 100
        return epf / 100

    @api.multi
    def get_empf_applicable(self, basic, payslip):
        epf = 0
        arrears = 0
        financial_year = self.env['account.fiscal.year'].search([
            ('id', '!=', self.id)])
        if payslip.contract_id.epf_applicable:
            for rec in payslip.input_line_ids:
                if rec.code == 'EPFAAR':
                    arrears += rec.amount
            if (basic + arrears) <= financial_year.epf_employee_limit:
                epf = (basic + arrears) * financial_year.epf_employer
                return epf / 100
            else:
                return False
        elif payslip.contract_id.epf_applicable_check:
            if payslip.contract_id.epf_applicable_pure:
                if basic > financial_year.epf_employee_limit:
                    epf = financial_year.epf_employee_limit * financial_year.epf_employer
                    return epf / 100
                else:
                    return False
            elif payslip.contract_id.epf_applicable_actual:
                if basic > financial_year.epf_employee_limit:
                    epf = basic * financial_year.epf_employer
                    return epf / 100
                else:
                    return False
            else:
                return False
        else:
            return False

    def salary_rule_caculation(self, rule, work_days, payslip):
        day_from = datetime.strptime(payslip.date_from, "%Y-%m-%d")
        day_to = datetime.strptime(payslip.date_to, "%Y-%m-%d")
        if payslip.contract_id.pay_day_calculation == 'days_in_month':
            total_worked_days = relativedelta.relativedelta(
                day_to, day_from).days + 1
            basic_with_work_days = (work_days * rule) / total_worked_days
        else:
            total_worked_days = payslip.contract_id.pay_days
            basic_with_work_days = (work_days * rule) / total_worked_days
        return basic_with_work_days

    @api.multi
    def get_leave_encashment(self, basic, work_days, payslip):
        prorated_leave = 0
        employee = self.env['hr.employee'].search(
            [('id', '=', payslip.employee_id)])
        if payslip.contract_id.leave_encashment_applicable and payslip.payslip_type == 'payslip':
            leave_encashment_percent = payslip.contract_id.leave_encashment
            leave_encashment = basic * leave_encashment_percent / 100
            prorated_leave = self.salary_rule_caculation(
                leave_encashment, work_days, payslip)
        elif payslip.payslip_type == 'settlement' and employee.last_working:
            basic_yearly = round(basic) * 12
            daily_basic = round(basic_yearly) / 365
            prorated_leave = round(daily_basic) * payslip.leave_encashment_days
        return round(prorated_leave)

    @api.multi
    def get_gratuity(self, basic, work_days, payslip):
        gratuity_payout = 0
        if payslip.payslip_type == 'settlement':
            employee = self.env['hr.employee'].search(
                [('id', '=', payslip.employee_id)])
            day_from = datetime.strptime(payslip.date_from, "%Y-%m-%d")
            day_to = datetime.strptime(employee.date_of_joining, "%Y-%m-%d")
            years_of_service = relativedelta.relativedelta(
                day_from, day_to).years
            if years_of_service >= 5:
                gratuity_payout = (basic * years_of_service * 15) / 26
        return round(gratuity_payout)

    @api.multi
    def get_net_salary(self, basic, alw, alwt, ded, payslip):
        current_payslip = self.env['hr.payslip'].search(
            [('id', '=', payslip.id)])
        # loan = current_payslip.check_insallments_to_pay()
        # print "============", loan
        if payslip.payslip_type == "settlement":
            net_salary = round(basic + alw + alwt - ded) - round(payslip.notice_amount)
            current_payslip.amount_to_pay = 0.0
            if net_salary < 0:
                current_payslip.amount_to_pay = abs(net_salary)
                net_salary = 0
        else:
            net_salary = (basic + alw + alwt - ded)
        return net_salary

    @api.multi
    def get_bonus(self, basic, work_days, payslip):
        prorated_bonus = 0
        financial_year = self.env['account.fiscal.year'].search([
            ('id', '!=', self.id)])
        if payslip.contract_id.bonus_applicable:
            if payslip.contract_id.bonus_applicable_on_limit:
                if basic > financial_year.bonus_avalibility:
                    bonus = (financial_year.bonus_avalibility *
                             financial_year.max_bonus) / 100
            elif payslip.contract_id.bonus_applicable_on_basic:
                if basic < financial_year.bonus_avalibility:
                    bonus = (basic * financial_year.min_bonus) / 100
            prorated_bonus = self.salary_rule_caculation(
                bonus, work_days, payslip)
        return round(prorated_bonus)

    @api.multi
    def get_basic_with_workdays(self, basic, work_days, payslip):
        basic_with_work_days = self.salary_rule_caculation(
            basic, work_days, payslip)
        return round(basic_with_work_days)

    @api.multi
    def get_basic_with_leave_amount(self, basic, work_days, payslip):
        basic_with_work_days = self.salary_rule_caculation(
            basic, work_days, payslip)
        return round(basic_with_work_days)

    @api.multi
    def get_hra_with_workdays(self, hra, work_days, payslip):
        basic_with_work_days = self.salary_rule_caculation(
            hra, work_days, payslip)
        return basic_with_work_days

    @api.multi
    def get_sda_with_workdays(self, sda, work_days, payslip):
        basic_with_work_days = self.salary_rule_caculation(
            sda, work_days, payslip)
        return round(basic_with_work_days)

    @api.multi
    def get_mobile_with_workdays(self, mobile, work_days, payslip):
        basic_with_work_days = self.salary_rule_caculation(
            mobile, work_days, payslip)
        return basic_with_work_days

    @api.multi
    def get_special_with_workdays(self, special, work_days, payslip):
        basic_with_work_days = self.salary_rule_caculation(
            special, work_days, payslip)
        return basic_with_work_days

    @api.multi
    def get_other_allowance_with_workdays(self, other_allowance, work_days, payslip):
        basic_with_work_days = self.salary_rule_caculation(
            other_allowance, work_days, payslip)
        return basic_with_work_days

    @api.multi
    def get_food_with_workdays(self, food, work_days, payslip):
        basic_with_work_days = self.salary_rule_caculation(
            food, work_days, payslip)
        return basic_with_work_days

    @api.multi
    def get_flexi_attire_with_workdays(self, attire, work_days, payslip):
        basic_with_work_days = self.salary_rule_caculation(
            attire, work_days, payslip)
        return basic_with_work_days

    @api.multi
    def get_flexi_business_with_workdays(self, business, work_days, payslip):
        basic_with_work_days = self.salary_rule_caculation(
            business, work_days, payslip)
        return basic_with_work_days

    @api.multi
    def get_flexi_conveyance_with_workdays(self, conveyance, work_days, payslip):
        basic_with_work_days = self.salary_rule_caculation(
            conveyance, work_days, payslip)
        return basic_with_work_days

    @api.multi
    def get_flexi_driver_with_workdays(self, driver, work_days, payslip):
        basic_with_work_days = self.salary_rule_caculation(
            driver, work_days, payslip)
        return basic_with_work_days

    @api.multi
    def get_flexi_lta_with_workdays(self, lta, work_days, payslip):
        basic_with_work_days = self.salary_rule_caculation(
            lta, work_days, payslip)
        return basic_with_work_days

    @api.multi
    def get_flexi_medical_with_workdays(self, medical, work_days, payslip):
        basic_with_work_days = self.salary_rule_caculation(
            medical, work_days, payslip)
        return basic_with_work_days

    @api.multi
    def get_flexi_telephone_with_workdays(self, telephone, work_days, payslip):
        basic_with_work_days = self.salary_rule_caculation(
            telephone, work_days, payslip)
        return basic_with_work_days


class HrPayslipWorkedDays(models.Model):
    _inherit = 'hr.payslip.worked_days'

    @api.onchange('number_of_days')
    def onchange_number_of_days(self):
        self.number_of_hours = self.number_of_days * 8


class Exemption(models.Model):
    _name = 'exemption.exemption'

    description = fields.Char()
    amount = fields.Float()
    investment_exemption_id = fields.Many2one('investment.form')


class PreviousEmployment(models.Model):
    _name = 'previous.employment'

    previous_employer = fields.Char()
    pan_of_ded = fields.Char('PAN')
    tan_of_ded = fields.Char('TAN')
    start_date = fields.Date()
    end_date = fields.Date()
    amount_paid = fields.Float('Amount Paid (Gross)')
    hra_exemption = fields.Float(string="HRA Exemption")
    sda_exemption = fields.Float(string="SDA Exemption")
    other_exemption = fields.Float(string="Other")
    tds_paid = fields.Float(string="TDS Paid")
    investment_id = fields.Many2one('investment.form')


class EmployeeContract(models.Model):
    _name = 'employee.contract'

    investment_id = fields.Many2one('investment.form')
    contract_id = fields.Many2one('hr.contract')
    state = fields.Selection([
        ('on_probation', "On Probation"),
        ('prob_extended', "Probation Extended"),
        ('prob_lapsed', "Probation Lapsed"),
        ('confirmed', "Confirmed"),
        ('close_contract', "Closed"),
        ('terminated', "Terminated")], "Status", default='on_probation',
    )


class ProfessionalTaxSlab(models.Model):
    _name = 'professional.tax.slab'

    professional_tax_rule_id = fields.Many2one('professional.tax.rule')
    from_amount = fields.Integer("From Amount")
    to_amount = fields.Integer("To Amount")
    amount = fields.Float("Amount")


class ProfessionalTaxRule(models.Model):
    _name = 'professional.tax.rule'
    _rec_name = 'state_id'

    name = fields.Char()
    state_id = fields.Many2one('res.country.state', "State")
    state_code = fields.Char(related="state_id.code")
    tin_no = fields.Char(
        related="state_id.tin_prefix_2_digit", string="Tin No")
    financial_year = fields.Many2one('account.fiscal.year', "Financial Year")
    period = fields.Selection([
        ('monthly', 'Monthly'),
        ('half_yearly', 'Half Yearly'),
        ('yearly', 'Yearly')
    ])
    country_id = fields.Many2one('res.country', related="state_id.country_id")
    start_date = fields.Date("Start Date", readonly=True)
    end_date = fields.Date("End Date", readonly=True)
    pt_rule_lines = fields.One2many(
        'professional.tax.slab', 'professional_tax_rule_id')
    employee_contribution = fields.Float(
        "Employee Contribution")
    employer_contribution = fields.Float("Employer Contribution")
    total_contribution = fields.Float(
        "Total Contribution", compute="_get_total_contribution")

    @api.depends('employee_contribution', 'employer_contribution')
    def _get_total_contribution(self):
        self.total_contribution = self.employee_contribution + self.employer_contribution

    @api.onchange('financial_year')
    def onchange_finacial_year(self):
        self.start_date = self.financial_year.date_start
        self.end_date = self.financial_year.date_stop

    @api.model
    def create(self, vals):
        financial_year = self.env['account.fiscal.year'].search([
            ('id', '=', vals.get('financial_year'))])
        vals['start_date'] = financial_year.date_start
        vals['end_date'] = financial_year.date_stop
        return super(ProfessionalTaxRule, self).create(vals)

    @api.multi
    def write(self, vals):
        if 'financial_year' in vals:
            financial_year = self.env['account.fiscal.year'].search([
                ('id', '=', vals.get('financial_year'))])
            vals['start_date'] = financial_year.date_start
            vals['end_date'] = financial_year.date_stop
            return super(ProfessionalTaxRule, self).write(vals)


class LabourWelfareFund(models.Model):
    _name = 'labour.welfare.fund'

    state_id = fields.Many2one('res.country.state')
    financial_year = fields.Many2one('account.fiscalyear')
    start_date = fields.Date(related='financial_year.date_start')
    end_date = fields.Date(related='financial_year.date_stop')
    employee_contribution = fields.Float()
    employer_contribution = fields.Float()
    total_contribution = fields.Float(compute="_get_total_contribution")
    period = fields.Selection([
        ('monthly', 'Monthly'),
        ('half_yearly', 'Half Yearly'),
        ('yearly', 'Yearly')
    ])
    due_date = fields.Date()
    last_date = fields.Date()

    @api.depends('employee_contribution', 'employer_contribution')
    def _get_total_contribution(self):
        self.total_contribution = self.employee_contribution + self.employer_contribution


class TaxSlabType(models.Model):
    _name = 'tax.slab'

    name = fields.Char()
    age_start = fields.Float("Age Start")
    age_end = fields.Float("Age End")
    gender = fields.Selection([
        ('male', 'Male'),
        ('female', 'Female')])


class account_fiscalyear(models.Model):
    _name = "account.fiscal.year"
    _description = "Fiscal Year"

    name = fields.Char('Fiscal Year', required=True)
    date_start = fields.Date('Start Date', required=True)
    date_stop = fields.Date('End Date', required=True)
    epf_employee = fields.Float("EPF Employee(%)")
    epf_employer = fields.Float("EPF Employer(%)", compute='_compute_epf')
    esci_employee = fields.Float("ESIC Employee(%)")
    esci_employer = fields.Float("ESIC Employer(%)")
    epf_employee_limit = fields.Float("EPF Employee Limit")
    esci_employee_limit = fields.Float("ESIC Employee Limit")
    min_bonus = fields.Float("Minimum Bonus")
    max_bonus = fields.Float("Maximum Bonus")
    bonus_avalibility = fields.Float("Bonus Values")
    epf = fields.Float("EPF")
    eps = fields.Float("EPS")
    edli = fields.Float("(EDLI Charges")
    epf_admin = fields.Float("EPF Admin charges")
    edli_admin = fields.Float("EDLI Admin charges")
    salary_allowed = fields.Integer("Salary Allowed")

    @api.multi
    def _compute_epf(self):
        self.epf_employer = self.epf + self.eps + \
            self.edli + self.epf_admin + self.edli_admin

    @api.constrains('date_start', 'date_stop')
    def check_fiscal(self):
        obj = self.env['account.fiscal.year'].search([('id', '!=', self.id)])
        for record in obj:
            if self.date_start >= record.date_start and self.date_stop <= record.date_stop:
                raise UserError("Warning! "
                                "Duplicate Fiscal year can not be created for overlapping date range.")
