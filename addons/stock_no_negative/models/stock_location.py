from odoo import api, fields, models, _


class StockLocation(models.Model):
    _inherit = "stock.location"

    allow_negative_stock = fields.Boolean(
        string='Disallow Negative Stock',
        help="Allow negative stock levels for the stockable products "
             "attached to this category. The options doesn't apply to products "
             "attached to sub-categories of this category.")