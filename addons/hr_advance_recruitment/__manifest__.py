{
    'name': 'Advance Recruitment',
    'version': '1.0',
    'sequence': 21,
    'summary': 'Jobs,Recruitment',
    'description': """
====================================

This is inherited HR Recruitment module customised for specific usage
    """,
    'depends': [
        'hr_employee_register',
        'hr_recruitment',
        'hr_recruitment_request',
        'hr',
        'base',
        'base_hr',
    ],
    'data': [
        'security/ir.model.access.csv',
        'security/recruitment_security.xml',
        'views/recruitment_view.xml',
        'views/recruitment_template.xml',
    ],
    'category': 'Human Resources',
    'author': 'Apagen Solutions Pvt. Ltd.',
    'website': 'http://www.apagen.com',
    'installable': True,
    'application': True,
    'auto_install': False,
}
