import datetime
import re
from odoo import api, fields, models
from datetime import timedelta
from odoo.tools.translate import _
from email.utils import formataddr
from odoo.exceptions import Warning, RedirectWarning
from odoo.exceptions import UserError, ValidationError


class HrApplicant(models.Model):
    _inherit = "hr.applicant"
    _description = "Applicant"

    @api.model
    def _get_traits(self):
        interview_obj = self.env['traits.config']
        interview_ids = interview_obj.search([])
        val = []
        for record in interview_ids:
            val.append({
                'traits': record.id,
                'factors': record.factors
            })
        return val

    def _default_stage_id(self):
        ids = self.env['hr.recruitment.stage'].search([
            ('name', '=', 'Profile Screening'),
        ]).ids
        if ids:
            return ids[0]
        return False

    stage_id = fields.Many2one(
        'hr.recruitment.stage',
        'Stage',
        track_visibility='onchange',
        copy=False, index=True,
        group_expand='_read_group_stage_ids',
        default=_default_stage_id)
    stage_check = fields.Char(related='stage_id.name')
    course_ids = fields.Many2one('hr.recruitment.course', 'Course')
    reference_id = fields.Many2one('hr.recruitment.request', "Reference")
    candidate_location = fields.Char("Candidate Location")
    candidate_loc = fields.Text("Candidate Location")
    work_location = fields.Many2one(
        'hr.location', "Work Location")
    ready_to_relocate = fields.Selection(
        [('yes', 'Yes'),
         ('no', 'No')], string="Ready to Relocate")
    partner_mobile = fields.Char('Mobile No')
    applied_company = fields.Many2one('res.company', 'Applied Company')
    department_id = fields.Many2one('hr.department', string="Department")
    sub_dept_id = fields.Many2one('hr.sub.department', 'Sub Department')
    reason_for_change = fields.Char("Reason For Change")
    stage_name = fields.Char("Stage name")
    hiring_source = fields.Many2one('source.type', string="Hiring Source")
    contact_id = fields.Char()
    recruiter_feedback = fields.Selection(
        [('short', 'Shortlisted'),
         ('reject', 'Rejected'),
         ('hold', 'Hold')], string="Recruiter Feedback")
    cv_upload = fields.Binary("Upload CV")
    total_work_experience = fields.Float("Total Work Experience (Year)")
    notice_period = fields.Integer("Notice Period (Days)")
    availability = fields.Date("Availability")
    # salary_expected = fields.Float('Current CTC',
    #                                help="Salary Expected by Applicant")
    # salary_proposed = fields.Float('Expected CTC',
    # help="Salary Proposed by the Organisation")
    salary_expected = fields.Float('Expected CTC',
                                   help="Salary Expected by Applicant")
    salary_proposed = fields.Float('Proposed CTC',
                                   help="Salary Proposed by the Organisation")
    salary_current = fields.Float('Current CTC',
                                  help="Current Salary of Applicant")
    hiring_manager = fields.Many2one('res.users', "Hiring Manager")
    hiring_manager_feedback = fields.Selection(
        [('short', 'Shortlisted'),
         ('reject', 'Rejected'),
         ('hold', 'Hold')], string="Hiring Manager Feedback")
    # band = fields.Many2one('hr.band', "Band")
    # band_designation = fields.Many2one('hr.band.designation',
    #                                    "Band Designation")
    # designation = fields.Many2one('hr.designation', "Designation")
    date_of_offer = fields.Date("Date of Offer")
    date_of_join_expected = fields.Date("Expected Date of Joining")
    document_check_ids = fields.One2many(
        'document.attached',
        'documents_attach_id',
        "Document Attachments")
    annual_fixed_ctc = fields.Float("Annual Fixed CTC")
    annual_variable_pay = fields.Float("Annual Variable Pay")
    total_ctc = fields.Float("Total Annual CTC")
    skip_level_1 = fields.Selection(
        [('short', 'Shortlisted'),
         ('reject', 'Rejected'),
         ('hold', 'Hold')], "Skip Level 1 Feedback")
    skip_level_2 = fields.Selection(
        [('short', 'Shortlisted'),
         ('reject', 'Rejected'),
         ('hold', 'Hold')], "Skip Level 2 Feedback")
    level_manager_1 = fields.Many2one('res.users', "Skip Level 1 Recruiter")
    level_manager_2 = fields.Many2one('res.users', "Skip Level 2 Recruiter")
    annouced_bouns = fields.Float("Annual Assured Bonus")
    date_1 = fields.Datetime("First Schedule Date")
    date_2 = fields.Datetime("Second Schedule Date")
    # role_id = fields.Many2one('hr.role', string="Role")
    # source_id = fields.Many2one('res.partner', 'Hiring Source')
    source_id = fields.Char('Hiring Source')
    # service_area_id = fields.Char("service area")
    notice_period = fields.Integer("Notice Period (Days)")
    available_date = fields.Date("Available Date")
    partner_mobile = fields.Char('Mobile', size=10)
    feedback_ids = fields.One2many(
        'hr.feedback', 'applicant_id', "Feedback")
    employee_id1 = fields.Many2one(
        'hr.employee', 'Responsible',
        default=lambda self: self.env['hr.employee'].search([
            ('user_id', '=', self.env.uid)], limit=1))
    business_partner = fields.Many2one('res.users', 'HR Business Partner')
    application_id = fields.Many2one('hr.applicant', "Application")
    # source_id = fields.Many2one('hr.source', "Hiring Source")
    work_email = fields.Char("Work Email")
    hiring_source = fields.Many2one('res.partner', "Hiring Source Detail")
    gender = fields.Selection(
        [('male', 'Male'),
         ('female', 'Female')], string="Gender")
    f_name = fields.Char("Father's Name")
    date_of_joining = fields.Date()
    # band = fields.Many2one('hr.band', "Band")
    # band_designation = fields.Many2one(
    #     'hr.band.designation', "Band Designation")

    marital = fields.Selection(
        [
            ('single', 'Single'),
            ('married', 'Married'),
            ('widower', 'Widower'),
            ('divorced', 'Divorced')
        ], 'Marital Status')
    date_of_birth = fields.Date("Date of Birth")
    # service_area_id = fields.Many2one(
    #     'hr.employee.service.area', 'Service Area')
    # cost_center = fields.Many2one('hr.cost.center', "Cost Center")
    blood_group = fields.Selection(
        [
            ('O+', 'O+'),
            ('O-', 'O-'),
            ('A-', 'A-'),
            ('A+', 'A+'),
            ('B-', 'B-'),
            ('B+', 'B+'),
            ('AB-', 'AB-'),
            ('AB+', 'AB+'),
            ('not_available', 'Not Available')
        ], "Blood Group")
    # business_unit = fields.Many2one('business.unit', 'Business Unit')
    business_partner = fields.Many2one('res.users', 'HR Business Partner')
    identification_id = fields.Char('PAN Card Number', required=False, size=10)
    pff_no = fields.Char("PF No")
    uan_no = fields.Char("UAN No")
    esic_no = fields.Char("ESIC No")
    joining_date = fields.Datetime("Date of Joining")
    application_date = fields.Date(
        'Application Date', default=fields.Datetime.now)
    present_company = fields.Char('Present Company')
    # industry = fields.Char('Industry')
    industry_id = fields.Many2one('industry.config')
    project_id = fields.Many2one('project.project', 'Project')
    # analytic_account_id = fields.Many2one('account.analytic.account', 'Project/Analytic Account')
    shortlisting_date = fields.Datetime('Shortlisting Date')
    date_action = fields.Datetime('Date Action')
    date_action_mail = fields.Datetime('Date Action Mail')
    interview_venue = fields.Char("Interview Venue")
    title_action = fields.Char('Next Action')
    postal_address = fields.Char('Postal Address')
    facebook_id = fields.Char('Facebook')
    twitter_id = fields.Char('Twitter')
    website_id = fields.Char('Website')
    other_id = fields.Char('Othet Social Id')
    applicant_qualifiaction_ids = fields.One2many(
        'applicant.qualifiaction', 'hr_applicant_id', 'Qualifaction ID')
    applicant_computerskills_ids = fields.One2many(
        'applicant.computerskills', 'hr_applicant_skill_id', 'Skill ID')
    education_certification_ids = fields.One2many(
        'education.certification', 'hr_applicant_id')
    language_known_ids = fields.One2many(
        'language.known', 'hr_applicant_lang_id', string='Language Known')
    applicant_employeehistory_ids = fields.One2many(
        'applicant.employeehistory', 'hr_applicant_history_id', 'Skill ID')
    domain_experience_ids = fields.One2many(
        'domain.experience', 'hr_applicant_id')
    interview_assessment_id = fields.One2many(
        'interview.assessment', 'hr_applicant_id', 'Assessment', default=_get_traits)
    rating_scale = fields.Selection([
        ('excellent', 'Excellent'),
        ('very_good', 'Very Good'),
        ('good', 'Good'),
        ('average', 'Average')
    ], string='Rating Scale')
    assessment_remark = fields.Text('Assesment Remarks')
    employee_type = fields.Many2one('emp.type', 'Employee Type')
    interviewer = fields.Many2one('hr.employee', 'Interviewer')

    @api.onchange('date_action')
    def onchange_date_action(self):
        if self.date_action:
            self.date_action_mail = datetime.datetime.strptime(
                self.date_action, '%Y-%m-%d %H:%M:%S') + timedelta(
                hours=5, minutes=30)

    @api.multi
    def ValidateEmail(self, email_from):
        if email_from:
            return True
            # if re.match("^.+\\@(\\[?)[a-zA-Z0-9\\-\\.]+\\.([a-zA-Z]{2,3}|[0-9]{1,3})(\\]?)$", email_from) != None:
            #     return True
            # else:
            #     raise UserError('Invalid Email',
            #                     'Please enter a valid email address')

    @api.model
    def create(self, data):
        res = super(HrApplicant, self).create(data)
        template_id = self.env.ref(
            'hr_advance_recruitment.process_screening_id')
        if template_id:
            template_id.send_mail(res.id, force_send=True)
        return res

    def write(self, data):
        res = super(HrApplicant, self).write(data)
        if data.get('stage_id') and data.get('last_stage_id'):
            rec = self.stage_id
            if ((self.last_stage_id.sequence < self.stage_id.sequence) and (rec.sequence == 1)):
                template_id = self.env.ref(
                    'hr_advance_recruitment.process_screening_id')
                if template_id:
                    template_id.send_mail(self.id, force_send=True)
            elif ((self.last_stage_id.sequence < self.stage_id.sequence) and (rec.sequence == 2)):
                template_id = self.env.ref(
                    'hr_advance_recruitment.tech_round_id')
                if template_id:
                    template_id.send_mail(self.id, force_send=True)
            elif ((self.last_stage_id.sequence < self.stage_id.sequence) and (rec.sequence == 3)):
                template_id = self.env.ref(
                    'hr_advance_recruitment.applicant_interest_id')
                if template_id:
                    template_id.send_mail(self.id, force_send=True)
            elif ((self.last_stage_id.sequence < self.stage_id.sequence) and (rec.sequence == 4)):
                template_id = self.env.ref(
                    'hr_advance_recruitment.result_emloyee')
                if template_id:
                    template_id.send_mail(self.id, force_send=True)
            elif ((self.last_stage_id.sequence < self.stage_id.sequence) and (rec.sequence == 5)):
                template_id = self.env.ref(
                    'hr_advance_recruitment.result_emloyee_rejected')
                if template_id:
                    template_id.send_mail(self.id, force_send=True)
            self.stage_name = rec.name
        return res

    @api.multi
    @api.onchange('stage_id')
    def onchange_stage_id(self):
        res = {}
        if not self.stage_id:
            return {'value': res}
        stage = self.env['hr.recruitment.stage'].browse(self.stage_id).id
        res['stage_name'] = stage.name
        return {'value': res}

    @api.multi
    @api.onchange('annual_fixed_ctc', 'annual_variable_pay')
    def onchange_annual_fixed_ctc(self):
        res = {}
        if self.annual_fixed_ctc:
            res['total_ctc'] = self.annual_fixed_ctc + self.annual_variable_pay
        return {'value': res}

    @api.multi
    @api.onchange('annual_fixed_ctc', 'annual_variable_pay')
    def onchange_annual_variable_pay(self):
        res = {}
        if self.annual_variable_pay:
            res['total_ctc'] = self.annual_fixed_ctc + self.annual_variable_pay
        return {'value': res}

    @api.model
    def message_get_reply_to(self, res_ids, default=None):
        applicant = self.sudo().browse(res_ids)
        res = {}
        for rec in applicant:
            res[rec.id] = formataddr(
                (rec.employee_id1.user_id.name, rec.employee_id1.user_id.login))
        return res

    @api.multi
    def create_employee_from_applicant(self):
        """ Create an hr.employee from the hr.applicants """
        qualification_val = []
        certificate_val = []
        computer_val = []
        language_val = []
        history_val = []
        domain_val = []
        employee = False
        for applicant in self:
            contact_name = False
            if applicant.partner_id:
                address_id = applicant.partner_id.address_get(['contact'])[
                    'contact']
                contact_name = applicant.partner_id.name_get()[0][1]
            else:
                new_partner_id = self.env['res.partner'].create({
                    'is_company': False,
                    'name': applicant.partner_name,
                    'email': applicant.email_from,
                    'phone': applicant.partner_phone,
                    'mobile': applicant.partner_mobile
                })
                address_id = new_partner_id.address_get(['contact'])['contact']
            if applicant.job_id and (applicant.partner_name or contact_name):
                applicant.job_id.write(
                    {'no_of_hired_employee': applicant.job_id.no_of_hired_employee + 1})
                for qualifiaction in applicant.applicant_qualifiaction_ids:
                    qualification_val.append(({
                        'qualification': qualifiaction.qualification,
                        'name': qualifiaction.name,
                        'course_studied': qualifiaction.course_studied,
                        'certificate': qualifiaction.certificate,
                        'period_from': qualifiaction.period_from,
                        'period_to': qualifiaction.period_to,
                    }))
                for computer in applicant.applicant_computerskills_ids:
                    computer_val.append(({
                        'skill': computer.skill,
                        'skill_description': computer.skill_description,
                        'aquired_date': computer.aquired_date,
                        'certificate': computer.certificate,
                        'level': computer.level,
                    }))
                for certificate in applicant.education_certification_ids:
                    certificate_val.append(({
                        'certification': certificate.certification,
                        'name': certificate.name,
                        'certi_description': certificate.certi_description,
                        'attachment': certificate.attachment,
                        'period_from': certificate.period_from,
                        'period_to': certificate.period_to,
                    }))
                for language in applicant.language_known_ids:
                    language_val.append(({
                        'language_known': language.language_known,
                        'is_read': language.is_read,
                        'is_write': language.is_write,
                        'is_speak': language.is_speak,
                    }))
                for history in applicant.applicant_employeehistory_ids:
                    history_val.append(({
                        'company_id': history.company_id,
                        'start_date': history.start_date,
                        'end_date': history.end_date,
                        'salary': history.salary,
                        'position': history.position,
                        'reason': history.reason,
                    }))
                for domain in applicant.domain_experience_ids:
                    domain_val.append(({
                        'name': domain.name,
                        'experience': domain.experience,
                    }))
                employee = self.env['hr.employee'].create({
                    'birthday': applicant.date_of_birth,
                    'gender': applicant.gender,
                    'fam_father': applicant.f_name,
                    'mobile_phone': applicant.partner_mobile,
                    'personal_email': applicant.email_from,
                    'contact_address': applicant.contact_id,
                    'marital': applicant.marital,
                    'location': applicant.work_location.id,
                    # 'location' : applicant.work_location.service.area,
                    'name': applicant.partner_name or contact_name,
                    'job_id': applicant.job_id.id,
                    'address_home_id': address_id,
                    'blood_group': applicant.blood_group,
                    'employee_type': applicant.employee_type.id,
                    # 'project_id': applicant.analytic_account_id.id,
                    'department_id': applicant.department_id.id or False,
                    'company_id': applicant.applied_company.id or False,
                    'sub_department': applicant.sub_dept_id.id or False,
                    'address_id': applicant.company_id and
                    applicant.company_id.partner_id and
                    applicant.company_id.partner_id.id or False,
                    # 'work_email': applicant.department_id and
                    # applicant.department_id.company_id and
                    # applicant.department_id.company_id.email or False,
                    # 'work_phone': applicant.department_id and
                    # applicant.department_id.company_id and
                    # applicant.department_id.company_id.phone or False
                })
                applicant.write({'emp_id': employee.id, 'stage_id': 8})
                applicant.job_id.message_post(
                    body=_(
                        'New Employee %s Hired') % applicant.partner_name if applicant.partner_name else applicant.name,
                    subtype="hr_recruitment.mt_job_applicant_hired")
                employee._broadcast_welcome()
                employee.applicant_qualifiaction_ids = qualification_val
                employee.applicant_computerskills_ids = computer_val
                employee.education_certification_ids = certificate_val
                employee.language_known_ids = language_val
                employee.applicant_employeehistory_ids = history_val
                employee.domain_experience_ids = domain_val
            else:
                raise UserError(
                    _('You must define an Applied Job and a Contact Name for this applicant.'))

        employee_action = self.env.ref('hr.open_view_employee_list')
        dict_act_window = employee_action.read([])[0]
        if employee:
            dict_act_window['res_id'] = employee.id
        dict_act_window['view_mode'] = 'form,tree'
        return dict_act_window


class HrRecruitmentStage(models.Model):
    """ Stage of HR Recruitment """
    _inherit = "hr.recruitment.stage"

    group_ids = fields.Many2many(
        'res.users',
        'rel_parent_user_id',
        'rel_child_user_id',
        'new_user_group',
        "Group")


class HrDocumentAttached(models.Model):
    _name = "document.attached"
    _description = "Document Attachments"

    documents_attach_id = fields.Many2one('hr.applicant', "Attachments")
    name = fields.Char('Document Name')
    attachment = fields.Binary('Attachemnts')
    check = fields.Boolean('Check')


class HrFeedback(models.Model):
    _name = "hr.feedback"
    _description = "Feedback"

    applicant_id = fields.Many2one('hr.applicant', "Feedback")
    name = fields.Many2one(
        'res.users', 'Name',
        default=lambda self: self.env.user)
    date = fields.Date('Date', default=fields.Datetime.now)
    description = fields.Char('Description')


class HrRecruitmentCourse(models.Model):
    _name = "hr.recruitment.course"
    _description = "Course of Recruitment"

    name = fields.Char('Name')


class ApplicantQualifiaction(models.Model):

    _name = 'applicant.qualifiaction'

    qualification = fields.Many2one('hr.recruitment.degree', "Qualification")
    name = fields.Char('Name of Institution')
    period_attended = fields.Char('Period Attended (From-To)')
    course_studied = fields.Char('Course Studied')
    certificate = fields.Many2many(
        'ir.attachment', 'rel_qualification_certificated',
        'certificate', 'attachment_certificate_idd', 'Attachment')
    hr_applicant_id = fields.Many2one('hr.applicant')
    period_from = fields.Date("Period Attended From")
    period_to = fields.Date('Period Attended To')

    @api.onchange('period_to')
    def onchange_period_to(self):
        if (self.period_from and self.period_to) and (self.period_from > self.period_to):
            raise UserError(
                _('The Period Attended From must be anterior to the Period Attended To.'))


class EducationCertifiaction(models.Model):

    _name = 'education.certification'

    certification = fields.Char('Certification')
    name = fields.Char('Name of Institution')
    period_attended = fields.Char('Period Attended (From-To)')
    certi_description = fields.Char('Certificate Description')
    attachment = fields.Many2many(
        'ir.attachment', 'rel_qualification_certificate',
        'certificate', 'attachment_certificate', 'Attachment')
    period_from = fields.Date("Period Attended From")
    period_to = fields.Date('Period Attended To')
    hr_applicant_id = fields.Many2one('hr.applicant')

    @api.onchange('period_to')
    def onchange_period_to(self):
        if (self.period_from and self.period_to) and (self.period_from > self.period_to):
            raise UserError(
                _('The Period Attended From must be anterior to the Period Attended To.'))


class ApplicantComputerSkills(models.Model):

    _name = 'applicant.computerskills'

    skill = fields.Char('Skill')
    skill_description = fields.Char('Skill Description')
    aquired_date = fields.Date('Skill Aquired Date')
    certificate = fields.Many2many(
        'ir.attachment', 'rel_certificate_atts',
        'certificate', 'attachment_certificate_ids', 'Attachment')
    level = fields.Selection([
        ('low', 'Low'),
        ('medium', 'Medium'),
        ('high', 'High')], 'Level of Proficiency')
    hr_applicant_skill_id = fields.Many2one('hr.applicant')


class LanguageKnown(models.Model):

    _name = 'language.known'

    hr_applicant_lang_id = fields.Many2one('hr.applicant')
    language_known = fields.Many2one(
        'language.config', string="Language Known")
    is_read = fields.Boolean('Read')
    is_write = fields.Boolean('Write')
    is_speak = fields.Boolean('Speak')


class ApplicantEmployeehistory(models.Model):

    _name = 'applicant.employeehistory'

    company_id = fields.Char('Company')
    start_date = fields.Date('Start Date')
    end_date = fields.Date('End Date')
    salary = fields.Float('Last Salary Drawn')
    position = fields.Char('Designation')
    reason = fields.Char('Reasons for Leaving')
    hr_applicant_history_id = fields.Many2one('hr.applicant')

    @api.onchange('end_date')
    def onchange_period_to(self):
        if (self.start_date and self.end_date) and (self.start_date > self.end_date):
            raise UserError(
                _('The Start Date must be anterior to the End Date.'))


class DomainExperience(models.Model):

    _name = 'domain.experience'

    hr_applicant_id = fields.Many2one('hr.applicant')
    name = fields.Many2one('domain.config', 'Domain')
    experience = fields.Float('Experience (Years)')


# class DomainConfig(models.Model):
#     _name = 'domain.config'

#     name = fields.Char('Domain')


class TraitsConfig(models.Model):
    _name = 'traits.config'
    _rec_name = 'traits'

    traits = fields.Char('Trait')
    factors = fields.Char('Factor')


class IndustryConfig(models.Model):
    _name = 'industry.config'
    _rec_name = 'industry'

    industry = fields.Char('Industry')


class InterviewAssessment(models.Model):

    _name = 'interview.assessment'

    hr_applicant_id = fields.Many2one('hr.applicant')
    # traits_config_id = fields.Many2one('traits.config')
    traits = fields.Many2one('traits.config', string='Trait')
    factors = fields.Char('Factor')
    tech_remarks = fields.Char('Remarks (Technical)')
    hr_remarks = fields.Char('Remarks (HR)')
    dept_remarks = fields.Char('Remarks (Department)')
    mgmt_remarks = fields.Char('Remarks (Management)')
    final_remarks = fields.Char('Remarks (Final Interview)')


class HrRecruitment(models.Model):
    _inherit = "hr.recruitment.request"
