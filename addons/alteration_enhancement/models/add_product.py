import logging

from odoo import api, fields, models
from odoo import tools, _
import time
import math
import odoo.addons.decimal_precision as dp
from datetime import datetime,timedelta
from odoo.tools import DEFAULT_SERVER_DATE_FORMAT,DEFAULT_SERVER_DATETIME_FORMAT
from odoo.exceptions import UserError, ValidationError
from odoo.modules.module import get_module_resource


class wiz_add_product(models.Model):
    _inherit="wiz.add.product"
    
    def action_calculate_price(self):
        cost = 0.0
        if self.category_template_lines:
            for each in self.category_template_lines:
                cost = cost + (each.product_id.standard_price * each.qty)
            product_obj = self.env['product.product'].browse(self.product_id.id)
            product_obj.update({'standard_price':cost})
            
    @api.model
    def calculate_price(self):
        wiz_obj = self.env['wiz.add.product'].search([('state','=','product_created'),('name1','=','altered')])
        # print"wiz_objjj",wiz_obj
        for val in wiz_obj:
            cost = 0.0
            add_obj  = self.env['wiz.add.product'].browse(val.id)
            # print"dvjbdj",add_obj.name
            if add_obj.category_template_lines:
                for each in add_obj.category_template_lines:
                    cost = cost + (each.product_id.standard_price * each.qty)
                if add_obj.product_id:
                    product_obj = self.env['product.product'].browse(add_obj.product_id.id)
                    product_obj.update({'standard_price':cost})
        return True
        
        
        
        
       
