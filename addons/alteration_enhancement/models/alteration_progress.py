import logging

from odoo import api, fields, models
from odoo import tools, _
import time
import math
import odoo.addons.decimal_precision as dp
from datetime import datetime,timedelta
from odoo.tools import DEFAULT_SERVER_DATE_FORMAT,DEFAULT_SERVER_DATETIME_FORMAT
from odoo.exceptions import UserError, ValidationError
from odoo.modules.module import get_module_resource


class alteration_progress(models.Model):
    _name="alteration.progress"
    _inherit = ['mail.thread', 'ir.needaction_mixin']
    
    transfer_progress = fields.Boolean('Transfer in Progress')
    alteration_id = fields.Many2one('item.alteration','Alteration#')
    conversion_id = fields.Many2one('conversion.non.alterable','Conversion#')
