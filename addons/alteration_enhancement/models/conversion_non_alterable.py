import logging

from odoo import api, fields, models
from odoo import tools, _
import time
import math
from collections import Counter
from time import gmtime, strftime
import odoo.addons.decimal_precision as dp
from datetime import datetime,timedelta,date
from odoo.tools import DEFAULT_SERVER_DATE_FORMAT,DEFAULT_SERVER_DATETIME_FORMAT
from odoo.exceptions import UserError, ValidationError
from odoo.modules.module import get_module_resource

class conversion_non_alterable(models.Model):
    _name="conversion.non.alterable"
    _inherit = ['mail.thread', 'ir.needaction_mixin']
    _order="date DESC"
    
    
    @api.model
    def create(self, vals):
        order = ' '
        categ_list = []
        product_list = []
        seq = self.env['ir.sequence'].next_by_code('conversion_non_alterable')
        vals['name'] = str(seq)
        conversion_non_alterable_id = super(conversion_non_alterable, self).create(vals)
        item_obj = self.env['conversion.non.alterable'].browse(conversion_non_alterable_id.id)
        if item_obj.serial_no:
            if item_obj.serial_no.quant_ids.location_id.id != item_obj.source_location_id.id:
                raise UserError(_('Location is changed for the serial No'))
        item_line = self.env['conversion.non.alterable.category.template'].search([('conversion_non_alterable_id','=',item_obj.id)])
        if item_line:
            for val in item_line:
                categ_list.append(val.name.id)
                product_list.append(val.product_id.id)
        if categ_list:
            for val1 in categ_list:
                if product_list:
                    for val2 in product_list:
                        item_line1  =self.env['conversion.non.alterable.category.template'].search([('conversion_non_alterable_id','=',item_obj.id),('name','=',val1),('product_id','=',val2)])
                        if len(item_line1) > 1:
                            raise UserError(_('Category with the same Product can not be repeat'))
        return conversion_non_alterable_id
     # This function is for modifying Item Alteration Record.
    @api.multi
    def write(self,vals):
        product_list = []
        categ_list = []
        item_obj = self.env['conversion.non.alterable'].browse(self.id)
        item_id =  super(conversion_non_alterable,self).write(vals)
        if item_obj.serial_no:
            if item_obj.serial_no.quant_ids.location_id.id != item_obj.source_location_id.id:
                raise UserError(_('Location is changed for the serial No'))
        item_line = self.env['conversion.non.alterable.category.template'].search([('conversion_non_alterable_id','=',item_obj.id)])
        if item_line:
            for val in item_line:
                categ_list.append(val.name.id)
                product_list.append(val.product_id.id)
        if categ_list:
            for val1 in categ_list:
                if product_list:
                    for val2 in product_list:
                        item_line1  =self.env['conversion.non.alterable.category.template'].search([('conversion_non_alterable_id','=',item_obj.id),('name','=',val1),('product_id','=',val2)])
                        if len(item_line1) > 1:
                            raise UserError(_('Category with the same Product can not be repeat'))
        return item_id
    
    
    # This function is for onchange on Barcode
    @api.onchange('barcode')
    def _onchange_barcode(self):
        result={}
        location_ids = []
        product_ids = []
        category_ids = []
#         bom_list = []
        add_product_obj = ' '
        if self.barcode:
            stock = self.env['stock.production.lot'].search([('actual_barcode','=',self.barcode),('quant_ids.location_id','!=','Virtual Locations/Alteration')])
            if stock:
                for val in stock:
                    serial_obj = self.env['stock.production.lot'].browse(val.id)
                    alter = self.env['conversion.non.alterable'].search([('serial_no','=',serial_obj.id)])
                    if alter:
                        for val1 in alter:
                            if val1.state =='draft':
                                raise UserError(_('There is already an alteration %s in progress for product %s .')%(val1.name,serial_obj.product_id.name))
                            elif val1.state =='mark':
                                raise UserError(_('There is already an alteration %s in progress for product %s .')%(val1.name,serial_obj.product_id.name))
                            elif val1.state =='validate':
                                raise UserError(_('There is already an alteration %s in progress for product %s .')%(val1.name,serial_obj.product_id.name))
#                             if val1.state != 'transfer' or val1.state !='cancel':
#                                 raise UserError(_('There is already an conversion %s in progress for product %s .')%(val1.name,serial_obj.product_id.name))
                    if serial_obj.product_id.categ_id.can_altered == False:
                        if serial_obj.product_id.categ_id.numbered == True:
                            product_ids.append(serial_obj.product_id.id)
                            self.product_id = serial_obj.product_id.id 
                            category_ids.append(serial_obj.product_id.categ_id.id)
                            self.product_category_id = serial_obj.product_id.categ_id.id 
                            quant = self.env['stock.quant'].search([('lot_id','=',serial_obj.id)])
                            if quant:
                                for val in quant:
                                    location_ids.append(val.location_id.id)
                                    self.source_location_id = val.location_id.id 
                            add_product = self.env['wiz.add.product'].search([('product_id','=',serial_obj.product_id.id)])
                            if add_product:
                                for val1 in add_product:
                                    add_product_obj = self.env['wiz.add.product'].browse(val1.id)
        #                             if val1.category_template_lines:
        #                                 for val2 in val1.category_template_lines:
        #                                     bom_list.append((0,False,{'name':val2.name.id,'qty':val2.qty,'category_id':val2.category_id.id,
        #                                                                    'seq_no':val2.seq_no,'product_id':val2.product_id.id,
        #                                                                    'part_name':val2.part_name,'mandatory':val2.mandatory,
        #                                                                    'short_name':val2.short_name,
        #                                                                    'seq_no1':val2.seq_no,'barcode':val2.barcode,
        #                                                                    'short_name1':val2.short_name}))
                            else:
                                raise UserError(_('There is no Add Product record for this Product.'))
        #                     self.conversion_non_alterable_lines = bom_list
                            result.update({'value':{'add_product_id':add_product_obj.id,'serial_no':serial_obj.id},
                                       'domain':{'product_id':[('id','in',product_ids)],
                                                 'product_category_id':[('id','in',category_ids)],
                                                 'source_location_id':[('id','in',location_ids)]}
                                       })
                        else:
                            raise UserError(_('Category must be Numbered111.'))
                    else:
                        raise UserError(_('Category must be Non-Altered2222.'))
        return result  
        
    @api.onchange('new_product_category_id')
    def _onchange_new_product_category_id(self):
        bom_list = []
        if self.new_product_category_id:
            if self.new_product_category_id.category_template_lines:
                for val in self.new_product_category_id.category_template_lines:
                    bom_list.append((0,False,{'name':val.id,'seq_no':val.seq_no,'part_name':val.part_name,
                            'seq_no1':val.seq_no,
                                                   'mandatory':val.mandatory,'category_id':val.name.id,'qty':1}))
        self.conversion_non_alterable_lines = bom_list
        
    # This function is for onchange on Serial No
    @api.onchange('serial_no')
    def _onchange_serial_no(self):
        result={}
        location_ids = []
        product_ids = []
        category_ids = []
#         bom_list = []
        add_product_obj = ' '
        if self.serial_no:
            alter = self.env['conversion.non.alterable'].search([('serial_no','=',self.serial_no.id)])
            if alter:
                for val1 in alter:
                    if val1.state =='draft':
                        raise UserError(_('There is already an alteration %s in progress for product %s .')%(val1.name,self.serial_no.product_id.name))
                    elif val1.state =='mark':
                        raise UserError(_('There is already an alteration %s in progress for product %s .')%(val1.name,self.serial_no.product_id.name))
                    elif val1.state =='validate':
                        raise UserError(_('There is already an alteration %s in progress for product %s .')%(val1.name,self.serial_no.product_id.name))
#                     if val1.state != 'transfer' or val1.state !='cancel':
#                         raise UserError(_('There is already an conversion %s in progress for product %s .')%(val1.name,serial_obj.product_id.name))
            if self.serial_no.product_id.categ_id.can_altered == False:
                if self.serial_no.product_id.categ_id.numbered == True:
                    product_ids.append(self.serial_no.product_id.id)
                    self.product_id = self.serial_no.product_id.id 
                    category_ids.append(self.serial_no.product_id.categ_id.id)
                    self.product_category_id = self.serial_no.product_id.categ_id.id 
                    quant = self.env['stock.quant'].search([('lot_id','=',self.serial_no.id)])
                    if quant:
                        for val in quant:
                            location_ids.append(val.location_id.id)
                            self.source_location_id = val.location_id.id 
                    add_product = self.env['wiz.add.product'].search([('product_id','=',self.serial_no.product_id.id)])
                    if add_product:
                        for val1 in add_product:
                            add_product_obj = self.env['wiz.add.product'].browse(val1.id)
                    
        #                     if val1.category_template_lines:
        #                         for val2 in val1.category_template_lines:
        #                             bom_list.append((0,False,{'name':val2.name.id,'qty':val2.qty,'category_id':val2.category_id.id,
        #                                                            'seq_no':val2.seq_no,'product_id':val2.product_id.id,
        #                                                            'part_name':val2.part_name,'mandatory':val2.mandatory,
        #                                                            'short_name':val2.short_name,
        #                                                            'seq_no1':val2.seq_no,'barcode':val2.barcode,
        #                                                            'short_name1':val2.short_name}))
                    else:
                        raise UserError(_('There is no Add Product record for this Product.'))
                    result.update({'value':{'add_product_id':add_product_obj.id,},
                       'domain':{'product_id':[('id','in',product_ids)],
                                 'product_category_id':[('id','in',category_ids)],
                                 'source_location_id':[('id','in',location_ids)]}
                       })
                else:
                    raise UserError(_('Category must be Numbered333.'))
            else:
                raise UserError(_('Category Must be Non-Altered.4444'))
#             self.conversion_non_alterable_lines = bom_list
            
        return result 
    
     
#     self.add_product_id = add_product_obj
    # This function is for Mark As TODO Button
    def action_mark(self):
        if self.state == 'draft':
            self.write({'state':'mark','alteration_timestamp': strftime("%Y-%m-%d %H:%M:%S", gmtime())})
        stock_obj = self.env['stock.production.lot'].browse(self.serial_no.id)
        stock_obj.write({'alteration_timestamp':self.alteration_timestamp})
        
    def action_approved(self):
        for each in self:
            each.write({'state':'approved'})
            
     # This fucntion is for Validate Button.       
    def action_validate(self):
        if self.state == 'mark':
            if self.alteration_timestamp == self.serial_no.alteration_timestamp:
                self.write({'state':'validate','code1':True,'validate_by':self.env.user.id,'validate_date':datetime.today().strftime('%Y-%m-%d %H:%M:%S'),})
            else:
                raise UserError(_('There is another Alteration for this Serial No.'))
            
    
            
           
    def action_transfer(self):
        product_list = []     
        seq_list = []
        seq_list1 = []
        seq_list2 = []   
        pro_seq_list = []
        pro_seq_list1 = []
        pro_seq_list2 = []
        done = []
        long_name = ''
        category_id = ''
        long_name1 = ''
        short_name = ''
        if self.new_product_category_id:
            category_id = self.new_product_category_id.id
        prefix = ''
        if self.serial_no:
            if self.serial_no.quant_ids.location_id.id != self.source_location_id.id:
                raise UserError(_('Location is changed for the serial No'))
        base_obj = self.env['alteration.progress'].browse(1)
        if base_obj.transfer_progress == False:
            base_obj.update({'transfer_progress':True,'conversion_id':self.id})
            if self.alteration_timestamp == self.serial_no.alteration_timestamp:
                while(category_id != ''):
                    category_obj = self.env['product.category'].browse(category_id)
                    if category_obj.parent_id:
                        category_id = category_obj.parent_id.id
                        if category_obj.prefix:
                            if prefix == '':
                                prefix = category_obj.prefix
                            else:
                                prefix = category_obj.prefix + prefix
                        else:
                            prefix = prefix
                    else:
                        if category_obj.prefix:
                            category_id = ''
                            if prefix == '':
                                prefix = category_obj.prefix
                            else:
                                prefix = category_obj.prefix + prefix
                        else:
                            prefix = prefix
                if prefix == '':
                    short_name = ' '
                else:
                    short_name = prefix
                if self.conversion_non_alterable_lines:
                    product_list = self.env['conversion.non.alterable.category.template'].search([('conversion_non_alterable_id','=',self.id)])
                    for each1 in product_list:
                        seq_list.append(each1.seq_no)
                    if seq_list:
                        seq_list1 = list(set(seq_list))
                    if seq_list1:
                        seq_list2 = seq_list1.sort()
                    if seq_list1:
                        for each43 in seq_list1:
                            product_list = self.env['conversion.non.alterable.category.template'].search([('conversion_non_alterable_id','=',self.id),('seq_no','=',each43)])
                            if product_list:
                                for each1 in product_list:
                                    if each1.id not in done:
                                        new = ''
                                        new1 = ''
                                        new2 = ''
                                        pro = self.env['conversion.non.alterable.category.template'].search([('conversion_non_alterable_id','=',self.id),('category_id','=',each1.category_id.id)])
                                        if len(pro) > 1:
                                            for val in pro:
                                                pro_seq_list.append(val.seq_no)
                                            if pro_seq_list:
                                                pro_seq_list1 = list(set(pro_seq_list))
                                            if pro_seq_list1:
                                                pro_seq_list2 = pro_seq_list1.sort()
                                            if pro_seq_list1:
                                                for val in pro_seq_list1:
                                                    prod_list = self.env['conversion.non.alterable.category.template'].search([('conversion_non_alterable_id','=',self.id),('seq_no','=',val),('category_id','=',each1.category_id.id)])
                                                    if prod_list:
                                                        for each in prod_list:
                                                            if each.id not in done:
                                                                if each.qty > 1:
        #                                                             each.action_qty()
                                                                    if not each.short_name:
                                                                        new  = short_name
                                                                        if each.product_id:
                                                                            long_name = str(long_name) + '[' + str(each.product_id.id) + ']'
                                                                            long_name1 = str(long_name1) + '-' + str(each.product_id.name)
                                                                        else:
                                                                            long_name = long_name
                                                                            long_name1 = long_name1
                                                                        done.append(each.id)
                                                                    else:
                                                                        if not new:
                                                                            if each.part_name == True:
                                                                                new = str(each.short_name) + '*' + str(each.qty)
                                                                                done.append(each.id)
                                                                            else:
                                                                                done.append(each.id)
                                                                        else:
                                                                            if each.part_name == True:
                                                                                new = str(new) + '+' + str(each.short_name) + '*' + str(each.qty)
                                                                                done.append(each.id)
                                                                            else:
                                                                                done.append(each.id)
                                                                        if not new1:
                                                                            if each.product_id:
                                                                                new1 = '[' + str(each.product_id.id) + ']' + str(each.short_name)  + '*' + str(each.qty)
                                                                            else:
                                                                                new1 = str(each.short_name)  + '*' + str(each.qty)
                                                                        else:
                                                                            if each.product_id:
                                                                                new1 = str(new1) + '+' + '[' + str(each.product_id.id) + ']' + str(each.short_name)  + '*' + str(each.qty)
                                                                            else:
                                                                                new1 = str(new1) + '+' + str(each.short_name)  + '*' + str(each.qty)
                                                                        if not new2:
                                                                            if each.product_id:
                                                                                new2 = str(each.product_id.name)
                                                                            else:
                                                                                new2 = str(new2)
                                                                        else:
                                                                            if each.product_id:
                                                                                new2 = str(new2)+ '-' + str(each.product_id.name)
                                                                            else:
                                                                                new2 = str(new2)
                                                                elif each.qty == 1:
        #                                                             each.action_qty_1()
                                                                    if not each.short_name:
                                                                        new  = short_name
                                                                        if each.product_id:
                                                                            long_name = str(long_name) + '[' + str(each.product_id.id) + ']'
                                                                            long_name1 = str(long_name1) + '-' + str(each.product_id.name)
                                                                        else:
                                                                            long_name = long_name
                                                                            long_name1 = long_name1
                                                                        done.append(each.id)
                                                                    else:
                                                                        if not new:
                                                                            if each.part_name == True:
                                                                                new = str(each.short_name) 
                                                                                done.append(each.id)
                                                                        else:
                                                                            if each.part_name == True:
                                                                                new = str(new) + '+' + str(each.short_name)
                                                                                done.append(each.id)
                                                                        if not new1:
                                                                            if each.product_id:
                                                                                new1 = '[' + str(each.product_id.id) + ']' + str(each.short_name)
                                                                            else:
                                                                                new1 = str(each.short_name)  
                                                                        else:
                                                                            if each.product_id:
                                                                                new1 = str(new1) + '+' + '[' + str(each.product_id.id) + ']' + str(each.short_name)
                                                                            else:
                                                                                new1 = str(new1) + '+' + str(each.short_name)
                                                                        if not new2:
                                                                            if each.product_id:
                                                                                new2 = str(each.product_id.name)
                                                                            else:
                                                                                new2 = str(new2)
                                                                        else:
                                                                            if each.product_id:
                                                                                new2 = str(new2) + '-' + str(each.product_id.name)
                                                                            else:
                                                                                new2 = str(new2)
                                            short_name = str(short_name) + str(new)
                                            if long_name == '':
                                                long_name = str(new1)
                                            else:
                                                long_name  = str(long_name) +'-'+ str(new1)  
                                            if long_name1 == '':
                                                long_name1 = str(new2)
                                            else:
                                                long_name1 = str(long_name1) + '-' + str(new2)
                                        else:
                                            if each1.qty > 1:
    #                                             each1.action_qty()
                                                if each1.short_name:
                                                    if short_name == '':
                                                        if each1.part_name == True:
                                                            short_name = str(each1.short_name)  + '*' +  str(each1.qty)
                                                            done.append(each1.id)
                                                        else:
                                                            done.append(each1.id)
                                                    else:
                                                        if each1.part_name == True:
                                                            short_name = str(short_name) + str(each1.short_name) + '*' + str(each1.qty)
                                                            done.append(each1.id)
                                                        else:
                                                            done.append(each1.id)
                                                             
                                                    if long_name == '':
                                                        if each1.product_id:
                                                            long_name = '[' + str(each1.product_id.id) + ']' + str(each1.short_name) + '*' + str(each1.qty)
                                                        else:
                                                            long_name = str(each1.short_name) + '*' + str(each1.qty)
                                                    else:
                                                        if each1.product_id:
                                                            long_name = str(long_name) +'-'+ '[' + str(each1.product_id.id) + ']' +  str(each1.short_name)  + '*' + str(each1.qty)
                                                        else:
                                                            long_name = str(long_name) +'-'+  str(each1.short_name)  + '*' + str(each1.qty)
                                                    if long_name1 == '':
                                                        if each1.product_id:
                                                            long_name1 = str(each1.product_id.name)
                                                        else:
                                                            long_name1 = str(long_name1)
                                                    else:
                                                        if each1.product_id:
                                                            long_name1 = str(long_name1) + '-' + str(each1.product_id.name)
                                                        else:
                                                            long_name1 = str(long_name1)
                                                else:
                                                    short_name = short_name
                                                    if each1.product_id:
                                                        long_name = str(long_name) + '[' + str(each1.product_id.id) + ']'
                                                        long_name1 = str(long_name1) + '-' + str(each1.product_id.name)
                                                    else:
                                                        long_name = long_name
                                                        long_name1 = long_name1
                                                    done.append(each1.id)
                                            elif each1.qty == 1:
    #                                             each1.action_qty_1()
                                                if each1.short_name:
                                                    if short_name == '':
                                                        if each1.part_name == True:
                                                            short_name = each1.short_name
                                                            done.append(each1.id)
                                                        else:
                                                            done.append(each1.id)
                                                    else:
                                                        if each1.part_name == True:
                                                            short_name = short_name + each1.short_name
                                                            done.append(each1.id)
                                                        else:
                                                            done.append(each1.id)
                                                    if long_name == '':
                                                        if each1.product_id:
                                                            long_name = '[' + str(each1.product_id.id) + ']' + str(each1.short_name)
                                                        else:
                                                            long_name =  str(each1.short_name)
                                                    else:
                                                        if each1.product_id:
                                                            long_name = str(long_name) + '-'+  '[' + str(each1.product_id.id) +']' + str(each1.short_name)
                                                        else:
                                                            long_name = str(long_name) + '-'+  str(each1.short_name)
                                                    if long_name1 == '':
                                                        if each1.product_id:
                                                            long_name1 = str(each1.product_id.name)
                                                        else:
                                                            long_name1 = str(long_name1)
                                                    else:
                                                        if each1.product_id:
                                                            long_name1 = str(long_name1) + '-' + str(each1.product_id.name)
                                                        else:
                                                            long_name1 = str(long_name1) 
                                                else:
                                                    short_name = short_name
                                                    if each1.product_id:
                                                        long_name = str(long_name) + '[' + str(each1.product_id.id) + ']'
                                                        long_name1 = str(long_name1) + '-' + str(each1.product_id.name)
                                                    else:
                                                        long_name = long_name
                                                        long_name1 = long_name1
                                                    done.append(each1.id)
                product_data = {}
                pro = []
                prod_obj = self.env['product.template']
                p1 = []
                prod1 = ''
                for each in self:
                    if long_name:
                        prod1 = self.env['product.template'].search([('name','=',long_name)])
                        if prod1:
                            prod_id1 = prod1
                            pro = self.env['product.product'].search([('product_tmpl_id','=',prod_id1.id)])
                            if pro.id == self.product_id.id:
                                base_obj.write({'transfer_progress':False,'conversion_id':''})
                                raise UserError(_('There is no Conversion takes place.Product Already Exist.'))
                            else:
                                self.write({'state':'transfer','new_product_id':pro.id,'long_name':long_name,
                                            'long_name1':long_name1,
                                            'short_name':short_name})
                        elif self.new_product_category_id:
                            if self.new_product_category_id.numbered == True:
                                product_data = {
                                                'name':long_name,
                                                    'product_alteration':long_name,
                                                    'long_name':long_name1,
                                                    'short_name':short_name,
                                                    'categ_id':self.new_product_category_id.id,
                                                    'tracking':'serial',
                                                    'type':'product',
                                                    'hsn_no':self.product_id.hsn_no,
                                                    'short_name_product':self.short_name_product,
                                                    'current_name':self.current_name,
                                                    'equipment_id':self.product_id.equipment_id.id,
                                                    'short_alterable':short_name,
                                                    }
                            else:
                                product_data = {
                                                    'name':long_name,
                                                    'product_alteration':long_name,
                                                    'long_name':long_name1,
                                                    'short_name':short_name,
                                                    'categ_id':self.new_product_category_id.id,
                                                    'tracking':'none',
                                                    'type':'product',
                                                    'hsn_no':self.product_id.hsn_no,
                                                    'short_name_product':self.short_name_product,
                                                    'current_name':self.current_name,
                                                  'equipment_id':self.product_id.equipment_id.id,
                                                    'short_alterable':short_name,
                                                    }
                            prod_id = prod_obj.create(product_data)
                            value_list = [ ]
                            location_id = ''
                            location_dest_id= ' '
                            if prod1:
                                for each in prod1:
                                    prod1_obj = self.env['product.product'].browse(each.id)
                            if self.new_product_category_id.attribute_lines:
                                for each in self.product_category_id.attribute_lines:
                                    value_list = []
                                    attribute_list = self.env['product.attribute.value'].search([('attribute_id','=',each.attribute_id.id)])
                                    for each1 in attribute_list:
                                        value_list.append(each1.id)
                                    self.env['product.attribute.line'].create({'product_tmpl_id':prod_id.id,'attribute_id':each.attribute_id.id,'value_ids':[(6,0,value_list)]})
                                    prod_id.create_variant_ids()
                            if prod_id:   
                                pro = self.env['product.product'].search([('product_tmpl_id','=',prod_id.id)])
                                self.write({'state':'transfer','new_product_id':pro.id,'long_name':long_name1,
                                            'short_name':short_name,'product_alteration':long_name})
                            if self.new_product_category_id:
                                template_list = []
                                if self.conversion_non_alterable_lines:
                                    for val3 in self.conversion_non_alterable_lines:
                                        template_list.append((0,False,{'name':val3.name.id,'category_id':val3.category_id.id,
                                                                           'part_name':val3.part_name,'mandatory':val3.mandatory,
                                                                           'seq_no':val3.seq_no,'short_name':val3.short_name,
                                                                           'barcode':val3.barcode,'qty':val3.qty,
                                                                           'product_id':val3.product_id.id}))
                                add_id = self.env['wiz.add.product'].create({'name':'altered','category_id1':self.new_product_category_id.id,
                                                                        'category_template_lines':template_list,'name1':'altered',
                                                                        'product_category':self.new_product_category_id.id,
                                                                        'conversion_id':self.id,'long_name':self.product_alteration,
                                                                        'long_name1':self.long_name,
                                                                       'short_name_product':self.short_name_product,
                                                                       'product_id':pro.id,'hsn_no':self.product_id.hsn_no,
                                                                       'equipment_id':self.product_id.equipment_id.id,
                                                                        'current_name':self.current_name,'numbered':self.new_product_category_id.numbered})
                            prod_id.write({'add_product_id':add_id.id})
                            add_obj = self.env['wiz.add.product'].browse(add_id.id)
                            add_obj.action_calculate_price()
                quant_list = []
                serial_obj = ''
                p = []
                p1 = []
                p2 = []
                p3 = []
                if self.serial_no:
                    serial_obj = self.env['stock.production.lot'].browse(self.serial_no.id)
                    loc = self.env['stock.location'].search([('usage','=','view'),('name','=','Physical Locations')])
                    if loc:
                        loc1 = self.env['stock.location'].search([('usage','=','view'),('location_id','=',loc.id),('name','=','WH')])
                        if loc1:
                            loc2 = self.env['stock.location'].search([('location_id','=',loc1.id),('usage','=','internal'),('name','=','Stock')])
                            if loc2:
                                location_id = loc2
                    loc3 = self.env['stock.location'].search([('usage','=','view'),('name','=','Virtual Locations')])
                    if loc3:
                        loc4 = self.env['stock.location'].search([('location_id','=',loc3.id),('usage','=','production'),('name','=','Alteration')])
                        if loc4:
                            location_dest_id = loc4
                    if self.new_product_id:
                        lot  = self.env['stock.production.lot'].search([('name','=',self.serial_no.name),('product_id','=',self.new_product_id.id)])
                        if lot:
                            for val43 in lot:
                                quant = self.env['stock.quant'].search([('lot_id','=',val43.id)])
                                if quant:
                                    for val in quant:
                                        if val.location_id != self.source_location_id:
                                            val.write({'location_id':self.source_location_id.id})
                                s = self.env['stock.move'].create({'product_id':self.new_product_id.id,'product_uom_qty':self.serial_no.product_qty,
                                                                       'product_uom':self.new_product_id.uom_id.id,
                                                                       'name':self.new_product_id.name,'origin':self.name,
                                                                       'location_dest_id':self.source_location_id.id,'location_id':location_dest_id.id})
                                stock_obj = self.env['stock.move'].browse(s.id)
                                p1.append(val.id)
                                s = stock_obj.write({'quant_ids':[(6,0,p)],'state':'done'})
                                picking_id = ''
                                picking = self.env['stock.picking.type'].search([('name','=','Alterations')])
                                for each in picking:
                                    picking_id = each
                                quant = self.env['stock.quant'].search([('lot_id','=',self.serial_no.id)])
                                if quant:
                                    for val1 in quant:
                                        if val1.location_id != location_dest_id:
                                            val1.write({'location_id':location_dest_id.id})
                                s1 = self.env['stock.move'].create({'product_id':self.product_id.id,'product_uom_qty':self.serial_no.product_qty,
                                                                       'product_uom':self.product_id.uom_id.id,'picking_type_id':picking_id.id,
                                                                       'name':self.product_id.name,'origin':self.name,
                                                                       'location_id':self.source_location_id.id,'location_dest_id':location_dest_id.id})
                                stock_obj = self.env['stock.move'].browse(s1.id)
                                p1.append(val1.id)
                                s1 = stock_obj.write({'quant_ids':[(6,0,p1)],'state':'done'})
                                self.write({'state':'transfer'})
                        else:
                            lot_id = self.env['stock.production.lot'].create({'name':self.serial_no.name,'product_qty':self.serial_no.product_qty,
                                                                              'alpha_serial_no':self.serial_no.alpha_serial_no,
                                                                              'oem_serial_no':self.serial_no.oem_serial_no,
                                                                              'oem_warranty_date':self.serial_no.oem_warranty_date,
                                                                              'ref':self.serial_no.ref,
                                                                     'product_id':pro.id})
                            s1 =  self.env['stock.move'].create({'product_id':self.new_product_id.id,'product_uom_qty':self.serial_no.product_qty,
                                                                 'product_uom':self.new_product_id.uom_id.id,
                                                               'name':self.new_product_id.name,'origin':self.name,
                                                               'location_id':location_dest_id.id,'location_dest_id':self.source_location_id.id})
                            stock_obj = self.env['stock.move'].browse(s1.id)
                            s1  = self.env['stock.quant'].create({'product_id':self.new_product_id.id,'lot_id':lot_id.id,
                                                                               'qty':(self.serial_no.product_qty),'location_id':self.source_location_id.id,
                                                                               }) 
                            quant_obj = self.env['stock.quant'].browse(s1.id)
                            p1.append(quant_obj.id)
                            s1 = stock_obj.write({'quant_ids':[(6,0,p1)],'state':'done'})
                            picking_id = ''
                            if self.product_id:
                                picking = self.env['stock.picking.type'].search([('name','=','Alterations')])
                                for each in picking:
                                    picking_id = each
                                quant = self.env['stock.quant'].search([('lot_id','=',self.serial_no.id)])
                                if quant:
                                    for val in quant:
                                        if val.location_id != location_dest_id:
                                            val.write({'location_id':location_dest_id.id})
                                s = self.env['stock.move'].create({'product_id':self.product_id.id,'product_uom_qty':self.serial_no.product_qty,
                                                                   'product_uom':self.product_id.uom_id.id,'picking_type_id':picking_id.id,
                                                                   'name':self.product_id.name,'origin':self.name,
                                                                   'location_id':self.source_location_id.id,'location_dest_id':location_dest_id.id})
                                stock_obj = self.env['stock.move'].browse(s.id)
                                p.append(val.id)
                                s = stock_obj.write({'quant_ids':[(6,0,p)],'state':'done'})
                else:
                    base_obj.write({'transfer_progress':False,'conversion_id':''})
                    raise UserError(_('There is another Conversion in progress for this Serial No. Please Cancel that conversion first.'))
            else:
                base_obj.write({'transfer_progress':False,'conversion_id':''})
                raise UserError(_('There is no Conversion take place.'))
        else:
            raise UserError(_('The Conversion No %s is in progress.')%(base_obj.conversion_id.name))
        base_obj.write({'transfer_progress':False,'conversion_id':''})            
       
        return True
     
     # This function is for Cancel Button
    def action_cancel(self):
        if self.state == 'transfer':
             raise UserError(_('You can not cancel it because it is already transfer.'))
        else:
            self.write({'state':'cancel'})
    
    
    name = fields.Char('Name')
    job_description = fields.Char('Job Description')
    barcode = fields.Char('Barcode')
    date = fields.Date('Date',default=lambda *a: datetime.now(),copy=False,track_visibility="onchange")
    serial_no = fields.Many2one('stock.production.lot','Serial Number')
    product_id = fields.Many2one('product.product','Product')
    add_product_id = fields.Many2one('wiz.add.product','Add Product')
    source_location_id = fields.Many2one('stock.location','Source Location')
#     item_location_id = fields.Many2one('stock.location','Item Location')
    validate_by = fields.Many2one('res.users','Validate By')
    validate_date = fields.Datetime('Validate Date')
    long_name = fields.Char('Long Name')
    short_name = fields.Char('Short Name')
    product_alteration = fields.Char('Product for Alteration Purpose')
    new_product_id = fields.Many2one('product.product','New Product')
    short_name_product = fields.Char('Short Name for Component in Bom')
    current_name = fields.Char('Current Name')
    code = fields.Boolean('Code')
    complaint_no = fields.Char('Complaint No')
    code1 = fields.Boolean('Code1')
    alteration_timestamp = fields.Datetime('Alteration Timestamp')
#     item_add_lines = fields.One2many('item.add','item_alteration_id','Item Add',copy=True)
    product_category_id = fields.Many2one('product.category','Product Category')
    new_product_category_id = fields.Many2one('product.category','New Product Category')
    state = fields.Selection([('draft','Draft'),('mark','Mark as Todo'),('validate','Validate'),('approved','Approved'),('transfer','Transfer'),('cancel','Cancel')],'State',default="draft",track_visibility="onchange")
#     item_remove_lines = fields.One2many('item.remove','item_alteration_id','Item Remove',copy=True)
    item_category_template_lines = fields.One2many('item.category.template','item_alteration_id','Category Template Lines',copy=True)
    conversion_non_alterable_lines = fields.One2many('conversion.non.alterable.category.template','conversion_non_alterable_id','Conversion of Non Alterable Lines')
    
class conversion_non_alterable_category_template(models.Model):
    _name="conversion.non.alterable.category.template"
    
    def action_qty(self):
        new = ''
        new1 = ''
        new2 = ''
        for each in self:
            if each.qty > 1:
                if not each.short_name:
                    new  = short_name
                    if each.product_id:
                        long_name = str(long_name) + '[' + str(each.product_id.id) + ']'
                        long_name1 = str(long_name1) + '-' + str(each.product_id.name)
                    else:
                        long_name = long_name
                        long_name1 = long_name1
                    done.append(each.id)
                else:
                    if not new:
                        if each.part_name == True:
                            new = str(each.short_name) + '*' + str(each.qty)
                            done.append(each.id)
                        else:
                            done.append(each.id)
                    else:
                        if each.part_name == True:
                            new = str(new) + '+' + str(each.short_name) + '*' + str(each.qty)
                            done.append(each.id)
                        else:
                            done.append(each.id)
                    if not new1:
                        if each.product_id:
                            new1 = '[' + str(each.product_id.id) + ']' + str(each.short_name)  + '*' + str(each.qty)
                        else:
                            new1 = str(each.short_name)  + '*' + str(each.qty)
                    else:
                        if each.product_id:
                            new1 = str(new1) + '+' + '[' + str(each.product_id.id) + ']' + str(each.short_name)  + '*' + str(each.qty)
                        else:
                            new1 = str(new1) + '+' + str(each.short_name)  + '*' + str(each.qty)
                    if not new2:
                        if each.product_id:
                            new2 = str(each.product_id.name)
                        else:
                            new2 = str(new2)
                    else:
                        if each.product_id:
                            new2 = str(new2)+ '-' + str(each.product_id.name)
                        else:
                            new2 = str(new2)
        return True
    
    def action_qty_1(self):
        new = ''
        new1 = ''
        new2 = ''
        for each in self:
            if not each.short_name:
                new  = short_name
                if each.product_id:
                    long_name = str(long_name) + '[' + str(each.product_id.id) + ']'
                    long_name1 = str(long_name1) + '-' + str(each.product_id.name)
                else:
                    long_name = long_name
                    long_name1 = long_name1
                done.append(each.id)
            else:
                if not new:
                    if each.part_name == True:
                        new = str(each.short_name) 
                        done.append(each.id)
                else:
                    if each.part_name == True:
                        new = str(new) + '+' + str(each.short_name)
                        done.append(each.id)
                if not new1:
                    if each.product_id:
                        new1 = '[' + str(each.product_id.id) + ']' + str(each.short_name)
                    else:
                        new1 = str(each.short_name)  
                else:
                    if each.product_id:
                        new1 = str(new1) + '+' + '[' + str(each.product_id.id) + ']' + str(each.short_name)
                    else:
                        new1 = str(new1) + '+' + str(each.short_name)
                if not new2:
                    if each.product_id:
                        new2 = str(each.product_id.name)
                    else:
                        new2 = str(new2)
                else:
                    if each.product_id:
                        new2 = str(new2) + '-' + str(each.product_id.name)
                    else:
                        new2 = str(new2)
        return True
    
    # This fucntion is for onchange on Product
    @api.onchange('product_id')
    def _onchange_product_id(self):
        bom_list = []
        if self.product_id:
            self.short_name = self.product_id.short_name_product
            self.short_name1 = self.product_id.short_name_product
            
      # This fucntion is for onchange on Category      
    @api.onchange('name')
    def _onchange_name(self):
        if self.name:
            self.category_id = self.name.name.id   
            self.seq_no = self.name.seq_no
            self.part_name = self.name.part_name
            self.mandatory = self.name.mandatory
            self.seq_no1 = self.name.seq_no
            
            
    @api.onchange('barcode')
    def _onchange_barcode(self):
        if self.barcode:
            product = self.env['product.product'].search([('barcode','=',self.barcode),('product_tmpl_id.categ_id','=',self.category_id.id)])
            if product:
                for val in product:
                    product_obj = self.env['product.product'].browse(val.id)
                    if product_obj:
#                         if product_obj.product_tmpl_id.categ_id == self.category_id
                        self.product_id = product_obj.id
                        self.short_name = product_obj.short_name_product
                        self.short_name1 = product_obj.short_name_product
            else:
                raise UserError(_('The Product does not belong to this category or may not exist.'))

    conversion_non_alterable_id = fields.Many2one('conversion.non.alterable','Conversion of Non Alterable ')
#     item_alteration_id = fields.Many2one('item.alteration','Item Alteration')
    name = fields.Many2one('category.template','Category')
    barcode = fields.Char('Barcode')
    category_id = fields.Many2one('product.category','Category',track_visibility="onchange")
    seq_no = fields.Integer('Sequence No',track_visibility="onchange")
    part_name = fields.Boolean('Part of Name',track_visibility="onchange")
    mandatory = fields.Boolean('Mandatory',track_visibility="onchange")
    short_name = fields.Char('Short Name for Product',track_visibility="onchange")
    qty = fields.Integer('Qty',default=1,track_visibility="onchange")
    product_id = fields.Many2one('product.product','Product',track_visibility="onchange")
    seq_no1 = fields.Integer('Sequence No',track_visibility="onchange")
    short_name1 = fields.Char('Short Name for Product',track_visibility="onchange")    
