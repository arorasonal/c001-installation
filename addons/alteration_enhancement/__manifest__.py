{
    "name": "Alteration Enhancement",
    "version": "1.0",
    'website': 'http://www.diracerp.in',
    'images': [ ],
    "depends": ['web','base','product','stock','warehouse_extension'],
    "author": "Dirac ERP",
    "category": "Stock",
#     'sequence': 16,
    "description": """
    This module is for enhancement of Alteration in CJPL. 
    """,
    'data': [
             'view/add_product_view.xml',
             'security/ir.model.access.csv',
             'security/security_view.xml',
             'view/sequence_view.xml',
             'view/conversion_non_alterable_view.xml',
             'report/conversion_label_report_view.xml',
             'view/conversion_label_report_template.xml',
             'view/alteration_progress_view.xml',
             'report/add_product_label_report_view.xml',
             'view/add_product_label_template.xml',
             'report/alteration_report_view.xml',
             'view/alteration_report_template_view.xml',
             'data/alteration_progress_data.xml',
                    ],
    'demo_xml': [],
     'js': [
            'static/src/js/view_list.js'
#            'static/src/js/view_list.js'
#     'static/src/js/subst.js'
    ],
    'css': [
#             'static/src/css/sample_kanban.css'
            ],
#     'pre_init_hook': 'pre_init_hook',
#     'post_init_hook': 'post_init_hook',
    'installable': True,
    'active': False,
    'auto_install': False,
#    'certificate': 'certificate',
}
