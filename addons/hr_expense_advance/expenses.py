import time
from odoo import api, fields, models, _
from odoo.exceptions import UserError
import odoo.addons.decimal_precision as dp


class HrExpenseSheet(models.Model):
    _inherit = "hr.expense.sheet"

    @api.multi
    def _links_get(self):
        link_obj = self.env['res.request.link']
        return [(r.object, r.name) for r in link_obj.search([])]

    state = fields.Selection(
        [('to_hod_approve', 'Awaiting HOD Approval'),
         ('to_hr_approve', 'Awaiting HR Approval'),
         ('to_finance_approve', 'Awaiting Finance Approval'),
         ('approve', 'Approved'),
         ('post', 'Posted'),
         ('done', 'Paid'),
         ('cancel', 'Refused')
         ], string='Status', index=True, readonly=True,
        track_visibility='onchange', copy=False,
        default='to_hod_approve', required=True,
        help='Expense Report State')
    transport_mode = fields.Many2one(
        'transport.mode', string="Mode of Transport")
    reference = fields.Char("Reference")
    dist_from = fields.Char("Traveled From")
    dist_to = fields.Char("Traveled To")
    purpose = fields.Reference(
        string='Purpose', selection=_links_get,
        ondelete="set null")
    partner_id = fields.Many2one('res.partner', 'Customer Name')
    employee_id = fields.Many2one(
        'hr.employee', string="Employee", required=True, readonly=False,
        states={'to_hod_approve': [('readonly', False)]},
        default=lambda self: self.env['hr.employee'].search([('user_id', '=', self.env.uid)]))
    account_id = fields.Many2one('account.account', string="Account's Head")
    employee_code = fields.Char(
        'Employee Code', related='employee_id.employee_code')
    department_id = fields.Many2one(
        'hr.department', store=True)
    manager_id = fields.Many2one(
        'hr.employee', string="Manager", store=True)
    current_user = fields.Boolean(
        copy=False, compute="check_current_user", default=False)
    refuse = fields.Text(copy=False)
    payment_mode = fields.Selection([
        ("own_account", "Employee (to reimburse)"),
        ("company_account", "Company")],
        related='expense_line_ids.payment_mode',
        default='own_account', readonly=False,
        string="Payment By")
    date_from = fields.Datetime("Date From")
    date_to = fields.Datetime("Time To")
    attachment = fields.Binary("Attachment")
    reference_no = fields.Char()

    @api.multi
    def unlink(self):
        """Allows to delete hr draft"""
        for rec in self:
            if rec.state not in ['to_hod_approve']:
                raise UserError(
                    'You can only delete in draft state')
        return super(HrExpenseSheet, self).unlink()

    @api.model
    def create(self, vals):
        a = self._create_set_followers(vals)
        employee = self.env['hr.employee'].search(
            [('id', '=', vals.get('employee_id'))])
        vals.update({
            'department_id': employee.department_id.id,
            'manager_id': employee.parent_id.id,
        })
        sequence = self.env['ir.sequence'].next_by_code(
            'hr.expense.sheet') or _('New')
        vals['reference_no'] = sequence
        return super(HrExpenseSheet, self).create(vals)

    @api.multi
    def write(self, vals):
        employee = self.env['hr.employee'].search(
            [('id', '=', vals.get('employee_id'))])
        if 'employee_id' in vals:
            vals.update({
                'department_id': employee.department_id.id,
                'manager_id': employee.parent_id.id,
            })
        return super(HrExpenseSheet, self).write(vals)

    @api.multi
    @api.onchange('employee_id')
    def onchange_employee(self):
        if self.employee_id:
            employee = self.env['hr.employee'].browse(self.employee_id).id
            self.department_id = self.employee_id.department_id
            self.manager_id = self.employee_id.parent_id

    @api.multi
    def button_finance_refuse(self):
        for value in self:
            if value.refuse is False:
                raise UserError('Please enter reason for refusal !!!')
            self.write({'state': 'cancel'})

    @api.multi
    def button_reset_to_draft(self):
        self.write({'state': 'to_hod_approve'})

    # @api.multi
    # def button_reset_to_draft_hod(self):
    #     self.write({'state': 'draft'})        
    @api.multi
    def name_get(self):
        result = {}
        list_name = []
        for employee in self:
            if employee.employee_code:
                result[employee.id] = '[%s] %s' % (
                    employee.employee_code, employee.name)
            else:
                result[employee.id] = employee.name

        return result.items()

    @api.multi
    def check_current_user(self):
        user = self.env.uid
        for record in self:
            if record.department_id.manager_id.user_id.id == user:
                record.current_user = True
            else:
                record.current_user = False

    @api.multi
    def approve_expense_sheets(self):
        if not self.user_has_groups('hr_expense.group_hr_expense_user'):
            raise UserError(_("Only HR Officers can approve expenses"))
        self.write({'state': 'to_finance_approve',
                    'responsible_id': self.env.user.id})

    # @api.multi
    # def submitted_expense(self):
    #     for order in self:
    #         if order.state == 'to_hod_approve':
    #             self.write({'state': 'to_hr_approve'})
        # if not self.reference:
        #     seq = self.env['ir.sequence'].get('hr.expense.sheet')
        # self.write({'state': 'to_hod_approve'})

    @api.multi
    def button_hr_approval(self):
        for order in self:
            if order.state == 'to_hr_approve':
                self.write({'state': 'to_finance_approve'})
            elif order.state == 'to_finance_approve':
                self.write({'state': 'approve'})

    @api.multi
    def button_hod_approval(self):
        for order in self:
            if order.state == 'to_hod_approve':
                self.write({'state': 'to_hr_approve'})

    @api.multi
    def button_finance_approval(self):
        for order in self:
            if order.state == 'to_finance_approve':
                self.write({'state': 'approve'})


class HrExpense(models.Model):
    _inherit = "hr.expense"

    @api.multi
    def _links_get(self):
        link_obj = self.env['res.request.link']
        return [(r.object, r.name) for r in link_obj.search([])]

    name = fields.Char(string='Details/Status', readonly=True, required=True,
                       states={'draft': [('readonly', False)], 'refused': [('readonly', False)]})
    date = fields.Datetime(readonly=True, states={'draft': [('readonly', False)], 'refused': [
        ('readonly', False)]}, default=fields.Date.context_today, string="Start Date")
    date_end = fields.Datetime(readonly=True, states={'draft': [('readonly', False)], 'refused': [
        ('readonly', False)]}, default=fields.Date.context_today, string="End Date")
    product_id = fields.Many2one('product.product', string='Product', readonly=True, states={'draft': [(
        'readonly', False)], 'refused': [('readonly', False)]}, domain=[('can_be_expensed', '=', True)], required=False)
    contact_id = fields.Many2one('res.partner', string="Contact Person", states={'draft': [('readonly', False)], 'refused': [
        ('readonly', False)]})
    quantity = fields.Float(string='Total Lms.', required=True, readonly=True, states={'draft': [('readonly', False)], 'refused': [
                            ('readonly', False)]}, digits=dp.get_precision('Product Unit of Measure'), default=1)
    total_amount = fields.Float(string='Amount', store=True,
                                compute='_compute_amount', digits=dp.get_precision('Account'))
    order_no = fields.Char("Order No")
    transaction_type = fields.Char("Trans.Type")
    report_no = fields.Char("Report No")
    transport_mode = fields.Many2one(
        'transport.mode', string="Mode of Transport")
    reference = fields.Char("Reference")
    dist_from = fields.Char("Traveled From")
    dist_to = fields.Char("Traveled To")
    purpose = fields.Reference(
        string='Purpose/Trans.ID', selection=_links_get,
        ondelete="set null")
    partner_id = fields.Many2one('res.partner', 'Customer Name')
    attachment = fields.Binary("Attachment")

    @api.multi
    @api.onchange('partner_id')
    def onchange_partner_id(self):
        if self.partner_id:
            partner = self.env['res.partner'].search(
                [('id', '=', self.partner_id.id)])
            for child in partner.child_ids:
                if child.type == 'contact':
                    self.contact_id = child.id

    @api.multi
    def submit_expenses(self):
        if any(expense.state != 'draft' for expense in self):
            raise UserError(_("You cannot report twice the same line!"))
        if len(self.mapped('employee_id')) != 1:
            raise UserError(
                _("You cannot report expenses for different employees in the same report!"))
        return {
            'type': 'ir.actions.act_window',
            'view_mode': 'form',
            'res_model': 'hr.expense.sheet',
            'target': 'current',
            'context': {
                'default_expense_line_ids': [line.id for line in self],
                'default_employee_id': self[0].employee_id.id,
                'default_name': self[0].name if len(self.ids) == 1 else '',
                # 'default_purpose': self[0].purpose.id if self.purpose else'',
                'default_partner_id': self[0].partner_id.id,
                'default_attachment': self[0].attachment,
                'default_report_no': self[0].report_no,
                'default_date_end': self[0].date_end,
                'default_transport_mode': self[0].transport_mode.id,
                'default_dist_from': self[0].dist_from,
                'default_dist_to': self[0].dist_to,
                'default_account_id': self[0].account_id.id
            }
        }


class TransportMode(models.Model):
    _name = 'transport.mode'

    name = fields.Char("Name")
