{
    'name': 'Employee Expense Reimbursement',
    'version': '1.0',
    'category': 'Human Resources',
    'description': """

    """,
    "author": "Apagen Solutions Pvt. Ltd.",
    'website': 'http://www.apagen.com',
    'depends': [
        'base',
        'hr_expense',
        'hr_travel_request',
        'hr_employee_register'],
    'data': [
        'security/ir_rule.xml',
        'security/ir.model.access.csv',
        'data/expense_data.xml',
        'hr_expense_sequence.xml',
        'expenses_view.xml',

    ],
    'installable': True,
    'auto_install': False,
    'application': True,
}
