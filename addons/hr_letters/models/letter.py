from odoo import api, fields, models, _
from datetime import datetime
from odoo.exceptions import UserError


class HrEmployee(models.Model):
    _inherit = 'hr.employee'

    hr_letter_id = fields.One2many(
        'hr.letter', 'employee_id', 'PIP Warning',
        domain=[('state', '=', 'confirm')])

    @api.multi
    def print_letter(self):
        self.write({'status1': 'confirmed'})
        return True


class CompanyLetter(models.Model):
    _name = 'company.letter'

    employee_id = fields.Many2one("hr.employee", "Name")
    department_id = fields.Many2one("hr.department", "Department")
    job_title = fields.Many2one("hr.job", "Job Title")
    date = fields.Date("Date")
    letter = fields.Char("Letter")
    letter_regarding_action = fields.Char("Letter Regarding Action")
    show_cause_letter_date = fields.Date("Show Cause Letter Date")
    last_rply_date = fields.Date("Last Reply Date")
    emloyee_required_to_do = fields.Char("Employee Required To Do")
    action_taken = fields.Char("Action Taken")
    descriprion = fields.Text("Description")


class PipFeedback(models.Model):
    _name = 'pip.feedback'

    employee_id = fields.Many2one("hr.employee", "Name")
    letter = fields.Char("Letter")
    date = fields.Date("Date")


class PipWarning(models.Model):
    _name = 'pip.warning'

    employee_id = fields.Many2one("hr.employee", "Name")
    Warning_letter = fields.Char("Warning Letter Regarding")
    date = fields.Date("Date")


class EmployeePip(models.Model):
    _name = 'employee.pip'

    employee_id = fields.Many2one("hr.employee", "Name")
    performance_feedback_letter = fields.Char("Performance Feedback Letter")
    date = fields.Date("Date")
    close_business_date = fields.Date("Close of business date")


class HrLetter(models.Model):
    _name = 'hr.letter'
    _inherit = 'mail.thread'
    _rec_name = 'reference'


    @api.depends('salary_breakup_ids.total_gross_sal')
    def _amount_all(self):
        amount = 0.0
        for order in self.salary_breakup_ids:
            amount += order.total_gross_sal
        self.update({'gross_salary': amount})

    reference = fields.Char('Reference #', index=True)
    employee_id = fields.Many2one("hr.employee", "Employee Name")
    candidate_name = fields.Char("Candidate Name")
    address = fields.Char()
    district = fields.Char()
    gross_salary_words = fields.Char()
    state_id = fields.Char("State")
    department_id = fields.Many2one(
        "hr.department", "Department", related='employee_id.department_id', store=True)
    job_id = fields.Many2one(
        "hr.job", "Designation", related='employee_id.job_id', store=True)
    staff_no = fields.Char("Staff No")
    descriprion = fields.Text("Description")
    letter_type = fields.Selection(
        [('offer_letter', 'Offer Letter'),
         ('appraisal_letter', 'Appraisal Letter'),
         ('probation_period_letter', 'Probation Period Completion'),
         ('re_designation', 'Re-Designation'),
         ('pip_warning_letter', 'PIP Warning Letter'),
         ('appointment_letter', 'Appointment Letter'),
         ('notice_period_letter', 'Notice Period Letter'),
         ('relieving_cum_exp_letter', 'Relieving Cum Experience Letter'),
         ('transfer_letter', 'Transfer Letter'),
         ('warning_letter', 'Warning Letter'),
         ], "Letter Type")
    created_by = fields.Many2one(
        "res.users",
        'Created By',
        readonly=True,
        default=lambda self: self.env.user)
    manager_id = fields.Many2one(
        "hr.employee", 'Manager')
    subject = fields.Char("Subject")
    effective_date = fields.Date("Effective From")
    date_till = fields.Date("Effective To")
    pip_from_day = fields.Integer(compute='_get_pip_from_day', string="Day")
    pip_from_symbol = fields.Char(compute='_get_pip_from_symbol', string="Day")
    pip_till_day = fields.Integer(compute='_get_pip_till_day', string="Day")
    pip_till_symbol = fields.Char(compute='_get_pip_till_symbol', string="Day")
    submission_date_day = fields.Integer(
        compute='_get_submission_date_day', string="Day")
    submission_date_symbol = fields.Char(
        compute='_get_submission_date_symbol', string="Day")
    submission_date = fields.Date("Return Signed Copy By")
    hearing_date = fields.Date("Hearing Date")
    hearing_date_date_day = fields.Integer(
        compute='_get_hearing_date_day', string="Day")
    hearing_date_symbol = fields.Char(
        compute='_get_hearing_date_symbol', string="Day")
    final_dues = fields.Text("Final Dues")
    cause_date = fields.Date("Show Cause Letter Date")
    reply_date = fields.Date("Employee Reply Date")
    employee_required_to = fields.Char("Employee Required To")
    action_taken = fields.Char("Action Taken")
    warning_letter_no = fields.Char("Warning Letter No")
    bulletin_id = fields.One2many('bulletin', 'hr_letter_id', "Details")
    cc_id = fields.One2many('cc', 'hr_letter_id', "Cc")
    state = fields.Selection(
        [('draft', "Draft"),
         ('confirm', 'Confirmed')], "State", track_visibility='always', default='draft')
    creation_date = fields.Date("Creation Date", readonly=True)
    day = fields.Integer(compute='_get_day', string="Day")
    day_symbol = fields.Char(compute='_get_date_symbol', string="Day")
    performance_hearing_date = fields.Date("Performance Hearing Date")
    overall_performance = fields.Char("Overall Performance")
    agreement_date = fields.Date("Agreement Date")
    contract_start_date = fields.Date("Contract Period Start Date")
    contract_end_date = fields.Date("Contract Period End Date")
    basic_salary = fields.Float("Basic Salary")
    milage_allowance = fields.Float("Mileage Allowance")
    po_box_no = fields.Char("PO. Box No.")
    city = fields.Char("City")
    new_salary = fields.Float("New Salary")
    new_job_position = fields.Char("New Job Position")
    gross_salary = fields.Float("Gross Salary", compute='_amount_all')
    business_closing_date = fields.Date("Business Closing Date")
    business_closing_day = fields.Integer(
        compute='_get_business_closing_day', string="Day")
    business_closing_symbol = fields.Char(
        compute='_get_business_closing_symbol', string="Day")
    overall_apraisal_score = fields.Char("Overall Apraisal Score")
    adress_hearing = fields.Char("Address")
    hearing_time = fields.Char("Time")
    performance_hearing_on = fields.Date("Performance Hearing On")
    confirmation_date = fields.Date("Effective Date")
    cause_hearing_date = fields.Date("Cause Letter Date")
    cause_hearing_response_date = fields.Date("Subsequent Response Date")
    cause_hearing_response_date = fields.Date("Subsequent Response Date")
    datas = fields.Binary(string='Upload File', nodrop=True, attachment=True)
    datas_fname = fields.Char('File Name')
    store_fname = fields.Char('Stored Filename')
    db_datas = fields.Binary('Database Data', attachment=True)
    show_cause_letter_date = fields.Date("Show Cause Letter Date")
    response_letter_date = fields.Date("Response Date")
    description_pip = fields.Text("Description")
    description_increment = fields.Text("Description")
    description_warning = fields.Text("Description")
    description_restructuring = fields.Text("Description")
    description_probation = fields.Text("Description")
    description_pip_termination = fields.Text("Description")
    description_extended = fields.Text("Description")
    capacity = fields.Char("Capacity")
    capacity_manager = fields.Char("Capacity")
    telephone_allowance = fields.Float("Telephone Allowance")
    disciplinary_hearing_on = fields.Date("Disciplinary Hearing On")
    description_hearing = fields.Text("Description")
    description_show_cause = fields.Text("Description")
    name_upper_case = fields.Char(compute='_get_uppercase_name', string="Name")
    # project_id = fields.Many2one('project.project', 'Project')
    closure_date = fields.Date('Closure Date')
    location_id = fields.Many2one("hr.location","Location")
    joining_date = fields.Date('Date of Joining')
    transfer_date = fields.Date('Transfer Date')
    letter_issue_date = fields.Date('Letter Issue Date')
    salary_breakup_ids = fields.One2many('salary.breakup', 'hr_salary_id', 'Salary Breakup')
    benefit_id = fields.Text('Other Benefits')
    company_id = fields.Many2one('res.company')
    father_name = fields.Char("Father Name")
    note = fields.Text()
    note_warning = fields.Text()
    days = fields.Char()
    notes = fields.Text("Confirmation")
    bond_info = fields.Text("Bond Information")
    esic_id = fields.Boolean('ESIC Applicable')
    epf_id = fields.Boolean('EPF Applicable')
    contract_id = fields.Many2one(
        "hr.contract", "Contract", store=True)
    title = fields.Selection([
                        ('mr', 'Mr'),
                        ('mrs', 'Mrs'),
                        ('miss', 'Miss'),
                    ], "Title")
    sub_department_id = fields.Many2one('hr.sub.department', 'Sub Department')
    notice_note = fields.Text('Information')
    probation_completion_date = fields.Date('Probation Completion Date')
    review_date = fields.Date('Review Date')
    improvement_date = fields.Date('Improvement Date')
    final_review_date = fields.Date('Final Review Date')

    @api.model
    def create(self, vals):
        vals['reference'] = self.env['ir.sequence'].next_by_code('hr.letter')
        employee = self.env['hr.employee'].search(
            [('id', '=', vals.get('employee_id'))])
        contract_obj = self.env['hr.contract'].search(
            [('employee_id', '=', vals.get('employee_id'))])
        vals.update({
            'contract_id': contract_obj.id,
        })
        return super(HrLetter, self).create(vals)

    @api.multi
    def write(self, vals):
        employee = self.env['hr.employee'].search(
            [('id', '=', vals.get('employee_id'))])
        contract_obj = self.env['hr.contract'].search(
            [('employee_id', '=', vals.get('employee_id'))])
        if 'employee_id' in vals:
            vals.update({
                'contract_id': contract_obj.id,
            })
        return super(HrLetter, self).write(vals)

    @api.multi
    def unlink(self):
        for rec in self:
            if rec.state not in ['draft']:
                raise UserError(
                    'You can only delete in Draft state')
        return super(HrLetter, self).unlink()

    @api.onchange('employee_id')
    def onchange_employee_id(self):
        """Onchange Employee Method."""
        if self.employee_id:
            employee = self.env['hr.employee'].browse(self.employee_id).id
            contract = self.env['hr.contract'].search(
                [('employee_id', '=', employee.id or False)])
            print"[[[[[[[[[[[[[[[[[[[[[[[[", self.employee_id.sub_department.id
            self.manager_id = self.employee_id.parent_id.id
            self.sub_department_id = self.employee_id.sub_department.id
            self.contract_id = contract.id

    @api.onchange('gross_salary')
    def onchange_gross_salary(self):
        val = self.env.user.currency_id.amount_to_text(self.gross_salary)
        self.gross_salary_words = val

    @api.multi
    def _get_day(self):
        res = {}
        for record in self:
            if record.creation_date:
                date_obj = datetime.strptime(record.creation_date, "%Y-%m-%d")
                day = date_obj.day
                res[record.id] = day
        return res

    @api.multi
    def _get_date_symbol(self):
        res = {}
        day = 0
        for record in self:
            rec = int(record.day)
            if rec:
                if rec == 1 or rec == 21 or rec == 31:
                    day = 'st'
                elif rec == 2 or rec == 22:
                    day = 'nd'
                elif rec == 3 or rec == 23:
                    day = 'rd'
                else:
                    day = 'th'
            res[record.id] = day
        return res

    @api.multi
    def _get_uppercase_name(self):
        res = {}
        for record in self:
            if record.employee_id:
                name_upper = record.employee_id.name
                # surname_upper = record.employee_id.surname
                # res[record.id] = name_upper.upper() + ' ' + \
                #     surname_upper.upper()
                res[record.id] = name_upper.upper()
        return res

    @api.multi
    def _get_business_closing_day(self):
        res = {}
        for record in self:
            if record.business_closing_date:
                date_obj = datetime.strptime(
                    record.business_closing_date, "%Y-%m-%d")
                day = date_obj.day
                res[record.id] = day
        return res

    @api.multi
    def _get_business_closing_symbol(self):
        res = {}
        day = 0
        for record in self:
            rec = int(record.business_closing_day)
            if rec:
                if rec == 1 or rec == 21 or rec == 31:
                    day = 'st'
                elif rec == 2 or rec == 22:
                    day = 'nd'
                elif rec == 3 or rec == 23:
                    day = 'rd'
                else:
                    day = 'th'
            res[record.id] = day
        return res

    @api.multi
    def _get_pip_from_day(self):
        res = {}
        for record in self:
            if record.effective_date:
                date_obj = datetime.strptime(record.effective_date, "%Y-%m-%d")
                day = date_obj.day
                res[record.id] = day
        return res

    @api.multi
    def _get_pip_from_symbol(self):
        res = {}
        day = 0
        for record in self:
            rec = int(record.pip_from_day)
            if rec:
                if rec == 1 or rec == 21 or rec == 31:
                    day = 'st'
                elif rec == 2 or rec == 22:
                    day = 'nd'
                elif rec == 3 or rec == 23:
                    day = 'rd'
                else:
                    day = 'th'
            res[record.id] = day
        return res

    @api.multi
    def _get_pip_till_day(self):
        res = {}
        for record in self:
            if record.date_till:
                date_obj = datetime.strptime(record.date_till, "%Y-%m-%d")
                day = date_obj.day
                res[record.id] = day
        return res

    @api.multi
    def _get_pip_till_symbol(self):
        res = {}
        day = 0
        for record in self:
            rec = int(record.pip_till_day)
            if rec:
                if rec == 1 or rec == 21 or rec == 31:
                    day = 'st'
                elif rec == 2 or rec == 22:
                    day = 'nd'
                elif rec == 3 or rec == 23:
                    day = 'rd'
                else:
                    day = 'th'
            res[record.id] = day
        return res

    @api.multi
    def _get_submission_date_day(self):
        res = {}
        for record in self:
            if record.submission_date:
                date_obj = datetime.strptime(
                    record.submission_date, "%Y-%m-%d")
                day = date_obj.day
                res[record.id] = day
        return res

    @api.multi
    def _get_submission_date_symbol(self):
        res = {}
        day = 0
        for record in self:
            rec = int(record.submission_date_day)
            if rec:
                if rec == 1 or rec == 21 or rec == 31:
                    day = 'st'
                elif rec == 2 or rec == 22:
                    day = 'nd'
                elif rec == 3 or rec == 23:
                    day = 'rd'
                else:
                    day = 'th'
            res[record.id] = day
        return res

    @api.multi
    def _get_hearing_date_day(self):
        res = {}
        for record in self:
            if record.hearing_date:
                date_obj = datetime.strptime(record.hearing_date, "%Y-%m-%d")
                day = date_obj.day
                res[record.id] = day
        return res

    @api.multi
    def _get_hearing_date_symbol(self):
        res = {}
        day = 0
        for record in self:
            rec = int(record.hearing_date_date_day)
            if rec:
                if rec == 1 or rec == 21 or rec == 31:
                    day = 'st'
                elif rec == 2 or rec == 22:
                    day = 'nd'
                elif rec == 3 or rec == 23:
                    day = 'rd'
                else:
                    day = 'th'
            res[record.id] = day
        return res

    # @api.onchange('employee_id')
    # def onchange_employee_id(self):
    #     emp_read = self.env['hr.employee'].browse(self.employee_id)
    #     res = {
    #         'department_id': emp_read.department_id,
    #         'manager_id': emp_read.parent_id,
    #         'staff_no': emp_read.staff_no,
    #         'job_id': emp_read.job_id,
    #     }
    #     return {'value': res}

    @api.multi
    def button_confirm(self):
        current_date = fields.Datetime.now()
        # emp_obj = self.env["hr.employee")
        # rec_id = self.browse(cr, uid, ids)
        # emp_record.create(cr, uid, val)
        # emp_obj.write(cr, uid, rec_id.emplyee_id.id, {'hr_letter_id': rec.id})
        self.write({'state': 'confirm',
                    'creation_date': current_date})

    @api.multi
    def test(self):
        import pdb
        pdb.set_trace()


class Bulletin(models.Model):
    _name = 'bulletin'

    hr_letter_id = fields.Many2one('hr.letter', "Details")
    detail = fields.Char("Details")


class Cc(models.Model):
    _name = 'cc'

    hr_letter_id = fields.Many2one('hr.letter', "Letters")
    employee_id = fields.Many2one("hr.employee", "Employee Name")
    department_id = fields.Many2one(
        "hr.department", "Department", related='employee_id.department_id')
    job_id = fields.Many2one("hr.job", "Job Title",
                             related='employee_id.job_id')


class SalaryBreakup(models.Model):
    _name = 'salary.breakup'

    hr_salary_id = fields.Many2one('hr.letter', "Salary Bearkup")
    discription_id = fields.Char("Description")
    per_month = fields.Float('Per month(Rs)')
    basic_pay = fields.Float('Basic Pay')
    hra = fields.Float('HRA')
    transport_allowance = fields.Float('Transport Allowance')
    mediclaim_reimbursement = fields.Float('Mediclaim Reimbursement')
    conveyance_reimbursement = fields.Float('Conveyance Reimbursement')
    special_allowance = fields.Float('Special Allowance')
    total_gross_sal = fields.Float('Total Gross', compute='_get_gros_sal')

    @api.depends('basic_pay', 'hra', 'transport_allowance', 'mediclaim_reimbursement', 'conveyance_reimbursement', 'special_allowance')
    def _get_gros_sal(self):
        amount = 0.0
        for order in self:
            amount = order.basic_pay + order.hra + order.transport_allowance + order.mediclaim_reimbursement + order.conveyance_reimbursement + order.special_allowance
            order.update({'total_gross_sal': amount})
        return amount

