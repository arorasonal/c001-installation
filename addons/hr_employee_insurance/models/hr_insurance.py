from odoo import api, fields, models, tools
from odoo.exceptions import UserError
from datetime import datetime


class HrEmployee(models.Model):
    _inherit = 'hr.employee'

    is_insurance = fields.Selection([
        ('yes', 'Yes'),
        ('no', 'No'),
        ('not_applicable', 'Not Applicable')
    ], "Is Insured")

    insurance_type_ids = fields.Many2many(
        'insurance.type', 'hr_insurance_type_rel', 'hr_insurance_id', 'insurance_type', 'Insurance Types')


class InsuranceClaim(models.Model):

    _name = 'insurance.claim'
    _inherit = 'mail.thread'
    _rec_name = 'employee_id'
    _description = 'Insurance Claim'

    employee_id = fields.Many2one('hr.employee', 'Employee')
    department_id = fields.Many2one('hr.department', 'Department')
    job_id = fields.Many2one('hr.job', 'Job Title')
    sub_department_id = fields.Many2one(
        'hr.sub.department', "Sub Department")
    parent_id = fields.Many2one(
        'hr.employee', 'Manager')
    insurance_type_ids = fields.Many2many(
        'insurance.type', 'claim_insurance_type_rel', 'claim_id', 'type_id', 'Insurnce Types')
    claimed_for = fields.Many2one('insurance.type', 'Insurance Claimed For')
    claimed_date = fields.Datetime('Claimed Date')
    claimed_amount = fields.Float('Claimed Amount')
    state = fields.Selection([
        ("in_progress", "In Progress"),
        ("awaiting_approval", "Awaiting Approval"),
        ("claimed", "Claimed"),
        ("rejected", "Rejected"),
        ("cancelled", "Cancelled"),
    ], 'State', default="in_progress")

    claim_applied_date = fields.Datetime(
        "Claim Applied Date", default=datetime.now())
    rejection_reason = fields.Text("Reason For Rejection")

    @api.onchange('employee_id')
    def onchange_employee(self):
        for rec in self:
            self.department_id = self.employee_id.department_id
            self.job_id = self.employee_id.job_id
            self.sub_department_id = self.employee_id.sub_department
            self.parent_id = self.employee_id.parent_id
            type_ids = self.employee_id.insurance_type_ids.ids
            if type_ids:
                self.insurance_type_ids = [(6, 0, type_ids)]

    @api.multi
    def button_submit_to_claim(self):
        self.write({'state': 'awaiting_approval'})
        return True

    @api.multi
    def button_approve_claim(self):
        self.write({'state': 'claimed'})
        return True

    @api.multi
    def button_rejected(self):
        if not self.rejection_reason:
            raise UserError(
                'Please fill Reason for Rejection')
        self.write({'state': 'rejected'})
        return True

    @api.multi
    def button_approve_cancel(self):
        self.write({'state': 'cancelled'})
        return True


class InsuranceType(models.Model):
    _name = 'insurance.type'

    name = fields.Char('Insurnce Name')
