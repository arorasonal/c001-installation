{
    'name': 'Hr Imployee Insurance',
    'sumary': 'To fill insurance details and to keep track of insurance claim',
    'author': 'Apagen Solutions Pvt. Ltd.',
    'websites': 'www.apagen.com',
    'category': 'Human Resources',
    'version': '10.0.0.1',
    'depends': [
        'hr_employee_register'
    ],
    'data': [
        'security/ir.model.access.csv',
        'views/hr_insurance_view.xml',
    ],
    'demo': [],
    'installable': True

}
