# -*- coding: utf-8 -*-

from odoo import models, fields, api
from datetime import datetime, timedelta
from odoo.exceptions import ValidationError,UserError
from datetime import date

class equipment_work_order(models.Model):
    _name = 'equipment.works.orders'

    name = fields.Char(string='Reference', copy=False, index=True)
    partner_id = fields.Many2one('res.partner', domain=[('parent_id', '=', False)])
    contact = fields.Many2one('res.partner', domain=[('parent_id', '!=', False)])
    work_type = fields.Many2one('equipment.order.type', string='Ewo Type')
    location_address = fields.Many2one('res.partner')
    executive = fields.Many2one('res.users', string="Executive")
    order_date = fields.Date('Order Date')
    sale_id = fields.Many2one('sale.order', 'Sale Order')
    delivery_date = fields.Datetime('Completed Date.')
    delivery_mode = fields.Char('Delivery Mode')
    work_description = fields.Text('Work Description')
    delivery_team_id = fields.Many2one('crm.team', string='Delivery Team', domain=[('type_team', '=', 'project')])
    state = fields.Selection([
        ('draft', 'Draft'),
        ('confirm', 'Confirm'),
        ('cancel', 'Cancel'),
        ('done', 'Done'),
        ], string='Status', readonly=True, copy=False, index=True, track_visibility='onchange', default='draft')
    order_line = fields.One2many('equipment.order.line', 'equipment_id', 'Item Lines')
    total_task = fields.Integer(compute='calculate_task')
    complaint_number = fields.Char('Complaint Number')
    task_stage_done = fields.Integer('Done task Count',readonly="1")
    pending_task_stage = fields.Integer('Pending Task Count', readonly="1")#, search='_search_pending_task_stage')
    user_ids = fields.Many2many('res.users',compute='get_hierarchy_user', string='User', store=True)
    team_id_equip = fields.Many2one('crm.team', string='Team', related='work_type.project_id.team_id')
    pending_task_stage_boolean = fields.Boolean()#compute='count_pending_stage_done', search='_search_pending_task_stage', string='Stage check')
    equipment_task_ids = fields.Many2many('project.task', string='Equipment Task', readonly="1")

    def _search_pending_task_stage(self, operator, value):
        res = []
        query = """
            SELECT DISTINCT pt.equipment_id
            FROM project_task as pt 
            INNER JOIN project_task_type as ptt ON ptt.id = pt.stage_id
            where pt.equipment_id is not null and ptt.stage_cancel is not TRUE and ptt.stage_done is not TRUE
        """
        self.env.cr.execute(query)
        result = self.env.cr.fetchall()
        equipment_ids = [rec[0] for rec in result if result not in [None,False,'None','false']]
        res.append(('id', 'in', equipment_ids))
        return res

    def get_parent(self, team, arr):
        if team.parent_id:
            arr.append(team.parent_id)
            # print("Inside", team.parent_id)
            self.get_parent(team.parent_id, arr)
            return arr
        else:
            if team not in arr:
                arr.append(team)
            return arr

    @api.multi
    def get_hierarchy_user(self):
        team_obj = self.env['crm.team']
        lst  = []
        for rec in self:
            if rec.team_id_equip:
                rec.get_parent(rec.team_id_equip, lst)
                parent_lst = [ls.user_id.id for ls in lst]
                user_lst = [user_data.id for user_data in rec.team_id_equip.team_members]
                rec.user_ids = [(6, 0, parent_lst + user_lst + [rec.team_id_equip.user_id.id])]

    # @api.multi
    # def count_task_stage_done(self):
    #     for rec in self:
    #         rec.task_stage_done = len(self.env['project.task'].search([
    #             ('stage_id.stage_done', '=', True),
    #             ('equipment_id','=',rec.id)]))

    # @api.multi
    # def count_pending_stage_done(self):
    #     for rec in self:
    #         rec.pending_task_stage = len(self.env['project.task'].search([
    #             ('stage_id.stage_done', '!=', True),
    #             ('stage_id.stage_cancel', '!=', True),
    #             ('equipment_id','=',rec.id)]))
    #         if rec.pending_task_stage > 0:
    #             rec.pending_task_stage_boolean = True


    

    @api.constrains('delivery_date')
    def constrains_delivery_date(self):
        today_date = date.today()
        for rec in self:
            if rec.delivery_date:
                if rec.delivery_date < str(today_date):
                    raise UserError("Completed date should be today date or future date kindly select correct date")



    @api.multi
    def action_confirm(self):
        if self.state == 'draft':
            if self.order_line:
                project_dictionary = {
                            'user_id':self.delivery_team_id.user_id.id,
                            'delivery_team_id':self.delivery_team_id.id,
                            'project_id':self.work_type.project_id.id,
                            'equipment_work_type':self.work_type.id,
                            'date_deadline':self.order_date,
                            'sale_order_id':self.sale_id.id,
                            'partner_id':self.partner_id.id,
                            'name':self.name,
                            'sale_order_date':self.delivery_date,
                            'equipment_id': self.id,
                            'products_task_ids':[(6,0,[i.product_id.id for i in self.order_line])],
                            'creation_type': 'equipment',
                            'delivery_date':self.delivery_date,
                            'delivery_mode':self.delivery_mode,
                            'delivery_instructions':self.work_description,
                            'installation_location_id':self.location_address.id,
                            'complaint_number': self.complaint_number,
                            'completed_date': self.delivery_date,
                            }
                store_task_id = self.env['project.task'].sudo().with_context(task_uid=self.env.user.id).create(project_dictionary)
                store_task_id._onchange_user()
                self.write({'equipment_task_ids': [(6,0,store_task_id.ids)]})
                reg = { 
                    'res_id': store_task_id.id, 
                    'res_model': 'project.task', 
                    'partner_id': self.env.user.partner_id.id, 
                    } 
                if not self.env['mail.followers'].search([('res_id','=',store_task_id.id),
                    ('res_model','=','project.task'),('partner_id','=',self.env.user.partner_id.id)]): 
                    follower_id = self.env['mail.followers'].create(reg)
                if store_task_id:
                    self.write({'sale_order_task_field_id': store_task_id.id})
                    for line in self.order_line:
                        task_line = self.env['project.task.line'].create({
                            'task_id': store_task_id.id,
                            'product_id':line.product_id.id,
                            'name':line.name,
                            'product_uom_qty':line.quantity,
                            'serial':line.serial.id,
                            'location_id':line.location_id.id,
                            'nature_work':line.nature_work,
                           
                            })
                self.write({'state': 'confirm'})
            else:
                raise UserError(("Please Select Item Line."))

    @api.multi
    def action_cancel(self):
        if self.state == 'confirm':
            tasks = self.env['project.task'].search([('equipment_id', '=', self.id)])
            if len(tasks) > 0:
                if tasks.filtered(lambda r: r.stage_id.stage_cancel == False):
                    raise UserError(("Please cancel the task after that we will cancel equipment order"))
            self.write({'state':'cancel'})

    @api.onchange('sale_id')
    def onchange_sale_id(self):
        if self.sale_id:
            self.partner_id = self.sale_id.partner_id.id
            self.contact = self.sale_id.partner_order_id.id
            self.location_address = self.sale_id.partner_shipping_id.id



    # @api.multi
    # def write(self, vals):
    #     sale_obj = self.env['sale.order']
    #     if 'sale_id' in vals:
    #         sale = sale_obj.browse(vals.get('sale_id'))
    #         vals['partner_id'] =  sale.partner_id.id
    #         vals['contact'] =  sale.partner_order_id.id
    #     return super(equipment_work_order, self).write(vals)

    @api.model
    def create(self, vals):
        vals['name'] = self.env['ir.sequence'].next_by_code('equipment.works.orders') or _('New')
        # sale_obj = self.env['sale.order']
        # if 'sale_id' in vals:
        #     sale = sale_obj.browse(vals.get('sale_id'))
        #     vals['partner_id'] =  sale.partner_id.id
        #     vals['contact'] =  sale.partner_order_id.id
        return super(equipment_work_order, self).create(vals)

    @api.multi
    def calculate_task(self):
        for rec in self:
            rec.total_task= len(self.env['project.task'].search([('equipment_id', '=', rec.id)]))

    @api.multi
    def calculate_total_task(self):
        for rec in self:
            total_task= self.env['project.task'].search([('equipment_id', '=', rec.id)])
            return total_task

    @api.multi
    def action_view_task(self):
        tasks = self.env['project.task'].search([('equipment_id', '=', self.id)])
        action = self.env.ref('project.action_view_task').read()[0]
        if len(tasks) > 1:
            action['domain'] = [('id', 'in', tasks.ids)]
        elif len(tasks) == 1:
            action['views'] = [(self.env.ref('project.view_task_form2').id, 'form')]
            action['res_id'] = tasks.ids[0]
        else:
            action = {'type': 'ir.actions.act_window_close'}
        return action

class equipment_order_type(models.Model):
    _name = 'equipment.order.type'

    name = fields.Char(string='Name', copy=False, index=True)
    project_id = fields.Many2one('project.project', string="Project")
    check_inst_prj = fields.Boolean(string='Installation Project Check', default=False)

    @api.constrains('check_inst_prj')
    def constrains_check_inst_prj(self):
        if self.check_inst_prj:
            record_ids = self.search([('check_inst_prj','=', True)])
            if len(record_ids)> 1:
                raise UserError("You can select only one installation project.")


class CreateTask(models.TransientModel):
    _inherit = "sale.create.task"


    @api.model
    def default_get(self, field_list):
        res = super(CreateTask, self).default_get(field_list)
        current_model = self._context.get('active_model')
        model_except_obj = self.env[current_model]
        active_ids = self._context.get('active_ids')
        active_id = active_ids[0]
        related_model_except = model_except_obj.browse(active_id)
        res.update({'deadline': related_model_except.delivery_date})
        return res

    @api.constrains('deadline')
    def constrains_deadline(self):
        today_date = date.today()
        for rec in self:
            if rec.deadline < str(today_date):
                raise UserError("Completed date should be today date or future date kindly select correct date")
        

    @api.multi
    def create_task(self):
        lst = []
        ewo_type_id = self.env['equipment.order.type'].search([
            ('project_id','=',self.project_id.id)],limit=1)
        if 'equipment_data' in self.env.context:
            for record in self:
                active_model_id = self.env.context.get('active_id')
                if active_model_id:
                    equipment_obj=self.env['equipment.works.orders'].browse(active_model_id)
                    order_name=equipment_obj.sale_id.name
                    customer_name=equipment_obj.partner_id.id
                    confirm_date=equipment_obj.delivery_date
                    products = equipment_obj.order_line
                    if products:
                        project_dictionary ={
                            'user_id':self.delivery_team_id.user_id.id,
                            'delivery_team_id':self.delivery_team_id.id,
                            'equipment_work_type':ewo_type_id.id,
                            'project_id':self.project_id.id,
                            'date_deadline':equipment_obj.order_date,
                            'sale_order_id':equipment_obj.sale_id.id,
                            'partner_id':customer_name,
                            'name':equipment_obj.name,
                            'sale_order_date':confirm_date,
                            'equipment_id': equipment_obj.id,
                            'products_task_ids':[(6,0,[i.product_id.id for i in products])],
                            'creation_type': 'equipment',
                            'delivery_date':self.deadline,
                            'delivery_mode':equipment_obj.delivery_mode,
                            'delivery_instructions':equipment_obj.work_description,
                            'installation_location_id':equipment_obj.location_address.id,
                            'complaint_number': equipment_obj.complaint_number,
                            'completed_date': equipment_obj.delivery_date,
                            }
                        store_task_id = self.env['project.task'].sudo().create(project_dictionary)
                        store_task_id._onchange_user()
                        if store_task_id:
                            task_id = self.env['project.task'].search([('equipment_id','=',equipment_obj.id)])
                            print('task_idtask_idtask_idtask_idtask_idtask_id', task_id.ids)
                            equipment_obj.write({'equipment_task_ids': [(6,0,task_id.ids)]})
                            for line in equipment_obj.order_line:
                                task_line = self.env['project.task.line'].create({
                                    'task_id': store_task_id.id,
                                    'product_id':line.product_id.id,
                                    'name':line.name,
                                    'product_uom_qty':line.quantity,
                                    'serial':line.serial.id,
                                    'location_id':line.location_id.id,
                                    'nature_work':line.nature_work,                                   
                                    })
        else:
            for record in self:
                active_model_id = self.env.context.get('active_id')
                if active_model_id:
                    order_obj=self.env['sale.order'].browse(active_model_id)
                    order_name=order_obj.name
                    customer_name=order_obj.partner_id.id
                    confirm_date=order_obj.confirmation_date
                    products = order_obj.order_line
                    if products:
                        project_dictionary ={
                            'user_id':self.delivery_team_id.user_id.id,
                            'delivery_team_id':self.delivery_team_id.id,
                            'project_id':self.project_id.id,
                            'equipment_work_type':ewo_type_id.id,
                            'date_deadline':self.deadline,
                            'sale_order_id':order_obj.id,
                            'partner_id':customer_name,'name':order_name,
                            'sale_order_date':confirm_date,
                            'creation_type': 'saleorder',
                            'products_task_ids':[(6,0,[i.product_id.id for i in products])],
                            'delivery_date':order_obj.delivery_date,
                            'delivery_mode':order_obj.delivery_mode,
                            'delivery_instructions':order_obj.delivery_instructions,
                            'installation_location_id':order_obj.partner_shipping_id.id}
                        store_task_id = self.env['project.task'].sudo().create(project_dictionary)
                        # print('store_task_id++++++++', store_task_id)
                        task_id = self.env['project.task'].search([('sale_order_id','=',order_obj.id)])
                        order_obj.write({'sale_order_task_field_id': [(6,0,task_id.ids)]})
                        if store_task_id:
                            for line in order_obj.order_line:
                                task_line = self.env['project.task.line'].sudo().create({
                                    'task_id': store_task_id.id,
                                    'product_id':line.product_id.id,
                                    'product_uom_qty':line.product_uom_qty,
                                    'name':line.name
                                })
        return True
 
class EquipmentOrderLine(models.Model):
    _name = 'equipment.order.line'

    product_id = fields.Many2one('product.product', 'Product')
    name = fields.Text(string='Description')
    quantity = fields.Float('Quantity', default=1.0)
    serial = fields.Many2one('stock.production.lot','Serial #')
    location_address = fields.Many2one('res.partner')
    location_id = fields.Many2one('stock.location')
    nature_work = fields.Char('Nature Of Work')
    equipment_id = fields.Many2one('equipment.works.orders')
    check_serial = fields.Boolean(compute="update_serial_check", default=False)

    @api.onchange('serial')
    def onchange_serial_id(self):
        if self.serial:
            self.product_id = self.serial.product_id.id
            self.name = self.serial.product_id.description_sale
            self.location_id = self.serial.quant_ids.location_id.id
            self.quantity = self.serial.quant_ids.qty

    @api.onchange('product_id')
    def onchange_product_id(self):
        if self.product_id:
            self.name = self.product_id.description_sale

    @api.depends('serial')
    def update_serial_check(self):
        for record in self:
            if record.serial:
                record.check_serial = True

    @api.multi
    def write(self, vals):
        serial_obj = self.env['stock.production.lot']
        if 'serial' in vals:
            serial_id = serial_obj.browse(vals.get('serial'))
            vals['location_id'] = serial_id.quant_ids.location_id.id
            vals['quantity'] = serial_id.quant_ids.qty
        return super(EquipmentOrderLine, self).write(vals)

    @api.model
    def create(self, vals):
        res = super(EquipmentOrderLine, self).create(vals)
        serial_obj = self.env['stock.production.lot']
        if 'serial' in vals:
            serial_id = serial_obj.browse(vals.get('serial'))
            res.location_id = serial_id.quant_ids.location_id.id
            res.quantity = serial_id.quant_ids.qty
        if vals.get('serial') in [None, False]:
            res.quantity = vals.get('quantity')
        return res


class ProjectTask(models.Model):
    _inherit = 'project.task'

    creation_type = fields.Selection([
        ('equipment', 'Equipment'),
        ('saleorder', 'Sale Order')
        ], string='Type')

    equipment_id = fields.Many2one('equipment.works.orders')
    is_team = fields.Boolean(compute='compute_team', default=False)
    allow_tasks_from_so = fields.Boolean(string='Tasks from SO / EWO', related='project_id.allow_tasks_from_so')

    @api.multi
    def compute_team(self):
        current_user = self.env.user.id
        team_obj = self.env['crm.team']
        project_mgr = self.env.user.has_group('project.group_project_manager')
        # print('project_mgr++++++++dddddddddddd+++++',project_mgr)
        for rec in self:
            if current_user == 1 or project_mgr == True:
                rec.is_team = True
            lst = []
            if current_user != 1:
                team_manager = team_obj.search([('user_id', '=', current_user), ('type_team', '=', 'project')])
                if team_manager:
                    team_managers = ','.join(str(user) for user in team_manager.ids)
                    query1 = """WITH RECURSIVE children AS (
                                 SELECT id,parent_id, name, 1 as depth
                                 FROM crm_team
                                 WHERE id in (%s) and active='t'
                                UNION
                                 SELECT op.id,op.parent_id, op.name, depth + 1
                                 FROM crm_team op
                                 JOIN children c ON op.parent_id = c.id
                                )
                                SELECT *
                                FROM children"""%(team_managers)
                    self._cr.execute(query1)
                    recursive_data =  self._cr.dictfetchall()
                    if recursive_data:
                        for ls in recursive_data:
                            crm_team = team_obj.search([('id', '=', ls.get('id'))])
                            lst.append(crm_team.user_id.id)
                            for l in crm_team.team_members:
                                lst.append(l.id)
                #project_team = rec.project_id.team_id
                if current_user in lst:
                    rec.is_team = True

class ResUsers(models.Model):
    _inherit = 'res.users'


    @api.model
    def name_search(self, name, args=None, operator='ilike',limit=100):
        if self._context.get('team_project_id'):
            lst = []
            team_obj = self.env['crm.team']
            current_user = self.env.user.id
            project_mgr = self.env.user.has_group('project.group_project_manager')
            project_id = self.env['project.project'].browse(self._context.get('team_project_id'))
            if project_id and project_id.team_id:
                if current_user != 1 or project_mgr == False:
                    team_manager = team_obj.search([('user_id', '=', current_user), ('type_team', '=', 'project')])
                    if team_manager:
                        team_managers = ','.join(str(user) for user in team_manager.ids)
                        query1 = """WITH RECURSIVE children AS (
                                     SELECT id,parent_id, name, 1 as depth
                                     FROM crm_team
                                     WHERE id in (%s) and active='t'
                                    UNION
                                     SELECT op.id,op.parent_id, op.name, depth + 1
                                     FROM crm_team op
                                     JOIN children c ON op.parent_id = c.id
                                    )
                                    SELECT *
                                    FROM children"""%(team_managers)
                        self._cr.execute(query1)
                        recursive_data =  self._cr.dictfetchall()
                        if recursive_data:
                            for ls in recursive_data:
                                crm_team = team_obj.search([('id', '=', ls.get('id'))])
                                lst.append(crm_team.user_id.id)
                                for l in crm_team.team_members:
                                    lst.append(l.id)
                    args = [('id', 'in', lst)]
        return super(ResUsers, self).name_search(name, args, operator=operator, limit=limit)

class ResPartner(models.Model):
    _inherit = 'res.partner'

    company_type_copy = fields.Selection(string='Company Type',
        selection=[('person', 'Individual'), ('company', 'Company')],
        related='parent_id.company_type')

    @api.model
    def name_search(self, name, args=None, operator='ilike',limit=100):
        partner_ids = False
        if self._context.get('equipment_partner_id'):
            partner_ids = self.env['res.partner'].search([('parent_id','=',self._context.get('equipment_partner_id'))])
            args = [('id', 'in', partner_ids.ids)]
        if self._context.get('equipment_work_loc_id'):
            equipment_id = self.env['res.partner'].search([('id','=',self._context.get('equipment_work_loc_id'))])
            partner_ship_ids = [child.id for child in equipment_id.child_ids if child.type == 'delivery'] + [self._context.get('equipment_work_loc_id')]
            args = [('id', '=', partner_ship_ids)]
        return super(ResPartner, self).name_search(name, args, operator=operator, limit=limit)

class PickingType(models.Model):
    _inherit = "stock.picking.type"

    ewo_type_id = fields.Many2many(comodel_name='equipment.order.type', string='EWO Type', 
         help='')
    ewo_required_in_delivery = fields.Boolean('EWO Required in Delivery')


    @api.onchange('ewo_required_in_delivery')
    def onchange_equipment_id(self):
        if self.ewo_required_in_delivery:
            self.so_required_in_delivery = False

    @api.onchange('so_required_in_delivery')
    def onchange_sale_id(self):
        if self.so_required_in_delivery:
            self.ewo_required_in_delivery = False        
            

class barcode_transfer(models.Model):
    _inherit="barcode.transfer"

    equipment_id = fields.Many2one('equipment.works.orders', 'Equipment')

    @api.onchange('equipment_id')
    def onchange_equipment(self):
        if self.equipment_id:
            self.task_copy_id = ''  

