# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from openerp import models, fields, api
from datetime import datetime


class Task(models.Model):
    _inherit = "project.task"

    delivery_date = fields.Datetime(string='Delivery Date', index=True, 
      help="Date on which sales order is Delivered.")
    delivery_mode = fields.Char(string='Delivery Mode', copy=False)
    task_line_ids = fields.One2many('project.task.line','task_id', string='Line')
    delivery_instructions = fields.Text('Delivery Instructions')
    installation_location_id = fields.Many2one('res.partner', string='Installation Location')
    vehicle_number = fields.Char('Vehicle #')
    closed_by = fields.Many2one('res.users', string='Executive Name')
    closed_on = fields.Datetime('Work date |Time')
    remark = fields.Text('Remarks')
    delivery_team_id = fields.Many2one('crm.team', string='Delivery Team', domain=[('type_team', '=', 'project')])
    trackwick_id =fields.Char('Trackwick Id')
    equipment_work_type = fields.Many2one('equipment.order.type', string='Ewo Type')
    complaint_number = fields.Char('Complaint Number')
    completed_date = fields.Datetime('Completed Date')
    # Constrains Section





class ProjectTaskLine(models.Model):
    _name = "project.task.line"

    product_id = fields.Many2one('product.product', string='Product', domain=[('sale_ok', '=', True)], change_default=True, ondelete='restrict')
    product_uom_qty = fields.Float(string='Quantity',default=1.0)
    name = fields.Text(string='Description')
    serial = fields.Many2one('stock.production.lot','Serial #')
    location_id = fields.Many2one('stock.location')
    nature_work = fields.Char('Nature Of Work')
    task_id = fields.Many2one('project.task', string="Task")

class ProjectTaskType(models.Model):
    _inherit = "project.task.type"

    stage_cancel = fields.Boolean(default=False, string='Stage Cancel')
    stage_done = fields.Boolean(default=False, string='Stage Done')

class ProjectProject(models.Model):
    _inherit = "project.project"

    @api.model
    def name_search(self, name, args=None, operator='ilike',limit=100):
        print('%%%%%%%%%%%%%%')
        if self._context.get('equipment_project_id'):
            print('equipment_project_id+++++++++')
            equipment_type_ids = self.env['equipment.order.type'].search([])
            lst = []
            for type_id in equipment_type_ids:
                lst.append(type_id.project_id.id)
            args = [('id', 'in', lst)]
        if self._context.get('task_project_id'):
            project_ids = self.env['project.project'].search([('allow_tasks_from_so','=',False)])
            proj_lst = []
            for rec in project_ids:
                proj_lst.append(rec.id)
            args = [('id', 'in', proj_lst)]
        return super(ProjectProject, self).name_search(name, args, operator=operator, limit=limit)
