# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from openerp import models, fields, api
from datetime import datetime, timedelta
from odoo.exceptions import ValidationError,UserError
from datetime import date


class SaleOrder(models.Model):
    _inherit = "sale.order"
    

    delivery_date = fields.Datetime(string='Delivery Date', index=True, 
      help="Date on which sales order is Delivered.")
    delivery_mode = fields.Char(string='Delivery Mode', copy=False)
    delivery_instructions = fields.Text('Delivery Instructions')
    invoice_journal_id = fields.Many2one('account.journal', string='Invoice Journal',required=True)
    task_count = fields.Integer(
        string="Task Count", compute='compute_task_count',
        copy=False
    )
    delivery_team_id = fields.Many2one('crm.team', string='Delivery Team', domain=[('type_team', '=', 'project')])

    @api.multi
    def _prepare_invoice(self):
        """
        Prepare the dict of values to create the new invoice for a sales order. This method may be
        overridden to implement custom invoice generation (making sure to call super() to establish
        a clean extension chain).
        """
        self.ensure_one()
        journal_id = self.env['account.invoice'].default_get(['journal_id'])['journal_id']
        if not journal_id:
            raise UserError(_('Please define an accounting sale journal for this company.'))
        invoice_vals = {
            'name': self.client_order_ref or '',
            'origin': self.name,
            'type': 'out_invoice',
            'account_id': self.partner_invoice_id.property_account_receivable_id.id,
            'partner_id': self.partner_id.id,
            'ordering_contract_id': self.partner_order_id.id,
            'partner_shipping_id': self.partner_shipping_id.id,
            'partner_invoice_id': self.partner_invoice_id.id,
            'journal_id': self.invoice_journal_id.id,
            'currency_id': self.pricelist_id.currency_id.id,
            'comment': self.note,
            'payment_term_id': self.payment_term_id.id,
            'fiscal_position_id': self.fiscal_position_id.id or self.partner_invoice_id.property_account_position_id.id,
            'company_id': self.company_id.id,
            'user_id': self.user_id and self.user_id.id,
            'team_id': self.team_id.id,
            'sale_type_id':self.type_id.id,
            'account_staff_id':self.account_staff_id.id,
        }
        return invoice_vals

    @api.multi
    def action_confirm(self):
        if not self.detect_exceptions():
            if not self.delivery_date:
                raise UserError(("Please enter Delivery Date"))
            elif not self.delivery_mode:
                raise UserError(("Please enter Delivery Mode"))
            elif not self.delivery_team_id:
                raise UserError(("Please enter Delivery Team"))
            products = self.order_line
            if self.type_id.project_id:
                project_dictionary ={
                    'user_id':self.delivery_team_id.user_id.id,
                    'delivery_team_id':self.delivery_team_id.id,
                    'project_id':self.type_id.project_id.id,
                    # 'equipment_work_type':ins_prj_id.id if ins_prj_id else ins_type_id.id,
                    'date_deadline':self.delivery_date,
                    'sale_order_id':self.id,
                    'partner_id':self.partner_id.id,
                    'name':self.name,
                    'sale_order_date':self.confirmation_date,
                    'creation_type': 'saleorder',
                    'products_task_ids':[(6,0,[i.product_id.id for i in products])],
                    'delivery_date':self.delivery_date,
                    'delivery_mode':self.delivery_mode,
                    'delivery_instructions':self.delivery_instructions,
                    'installation_location_id':self.partner_shipping_id.id}
                store_task_id = self.env['project.task'].sudo().create(project_dictionary)
                # print('store_id+++++++++++++++++++',store_task_id)
                if store_task_id:
                    self.write({'sale_order_task_field_id': [(6,0,store_task_id.ids)]})
                    for line in self.order_line:
                        task_line = self.env['project.task.line'].create({
                            'task_id': store_task_id.id,
                            'product_id':line.product_id.id,
                            'product_uom_qty':line.product_uom_qty,
                            'name':line.name
                            })
            if self.is_contract_type:
                self.action_button_contract()
            else:
                self.action_invoice_create(final=True)
        return super(SaleOrder, self).action_confirm()

    @api.constrains('delivery_date')
    def constrains_delivery_date(self):
        today_date = date.today()
        for rec in self:
            if rec.delivery_date:
                if rec.delivery_date < str(today_date):
                    raise UserError("Completed date should be today date or future date kindly select correct date")


    @api.multi
    @api.onchange('partner_id')
    def onchange_partner_id(self):
        vals = super(SaleOrder, self).onchange_partner_id()
        self.partner_order_id = ''

    @api.multi
    def action_view_create_task(self):
        task_id = self.env['project.task'].search([('sale_order_id','=',self.id)])
        action = self.env.ref('project.action_view_task').read()[0]
        if len(task_id) > 1:
            action['domain'] = [('id', 'in', task_id.ids)]
        elif task_id:
            action['views'] = [(self.env.ref('project.view_task_form2').id, 'form')]
            action['res_id'] = task_id.ids[0]
    	else:
            action = {'type': 'ir.actions.act_window_close'}
        return action

    # @api.depends('state')
    def compute_task_count(self):
        count = 0
        task_id = self.env['project.task'].sudo().search([('sale_order_id','=',self.id)])
        for rec in task_id:
            count += 1
        self.task_count = count

class SaleOrderLine(models.Model):
    _inherit = 'sale.order.line'

    @api.depends('qty_invoiced', 'qty_delivered', 'product_uom_qty', 'order_id.state')
    def _get_to_invoice_qty(self):
        """
        Compute the quantity to invoice. If the invoice policy is order, the quantity to invoice is
        calculated from the ordered quantity. Otherwise, the quantity delivered is used.
        """
        for line in self:
            if line.order_id.state in ['draft','sale', 'done']:
                if line.product_id.invoice_policy == 'order':
                    line.qty_to_invoice = line.product_uom_qty - line.qty_invoiced
                else:
                    line.qty_to_invoice = line.qty_delivered - line.qty_invoiced
            else:
                line.qty_to_invoice = 0

class SaleOrderType(models.Model):
    _inherit = 'sale.order.type'

    project_id = fields.Many2one('project.project', string='Project')