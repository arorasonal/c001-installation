import logging

from odoo import api, fields, models
from odoo import tools, _
import time
import math
import copy
from time import gmtime, strftime
import odoo.addons.decimal_precision as dp
from datetime import datetime,timedelta,date
from odoo.tools import DEFAULT_SERVER_DATE_FORMAT,DEFAULT_SERVER_DATETIME_FORMAT
from odoo.exceptions import UserError, ValidationError
from odoo.modules.module import get_module_resource
from openerp import exceptions


class Task(models.Model):
    _inherit = "project.task"

    picking_count = fields.Integer(
            string="Picking Count", compute='compute_picking_count',
            copy=False
        )
    check_picking = fields.Boolean(compute="action_view_picking", default=False)


    @api.model
    def create(self, vals):
        task = super(Task, self).create(vals)
        # print('task===========', task.equipment_id.id)
        if task.equipment_id.id:
            # print('if=++++++++++++++++')
            task.equipment_id.pending_task_stage = task.equipment_id.pending_task_stage + 1
        task_copy_obj = self.env['copy.project.task']
        value ={
            'name':task.name,
            'number':task.number,
            'task_id':task.id,
            'installation_location_id':task.installation_location_id.id or '',
            'sale_order_id':task.sale_order_id.id or '',
            'equipment_id':task.equipment_id.id or '',
            'project_id':task.project_id.id or ''
            }
        task_copy_obj.create(value)
        return task

    @api.multi
    def write(self, vals):
        if 'stage_id' in vals and vals.get('stage_id') not in (None,False,''): 
            new_stage_id = self.env["project.task.type"].browse(vals.get("stage_id"))
            if self.equipment_id:
                if new_stage_id.stage_done == True:
                    self.equipment_id.task_stage_done = self.equipment_id.task_stage_done + 1
                    self.equipment_id.pending_task_stage = self.equipment_id.pending_task_stage - 1  
                if new_stage_id.stage_cancel == True:
                    self.equipment_id.pending_task_stage = self.equipment_id.pending_task_stage - 1  
        # if vals.get("stage_id"):
            if (new_stage_id.stage_done == False and self.stage_id.stage_done == True) or (self.stage_id.stage_done == True and new_stage_id.stage_cancel == True):
                raise exceptions.Warning(_(u"""You can't change task status because task has been done."""))
            if (new_stage_id.stage_cancel == False and self.stage_id.stage_cancel == True):
                raise exceptions.Warning(_(u"""You can't change task status because task has been Cancel."""))
        task = super(Task, self).write(vals)
        task_copy_obj = self.env['copy.project.task'].search([('task_id','=',self.id)])
        value ={}
        value.update({
            'name':self.name,
            'number':self.number,
            'installation_location_id':self.installation_location_id.id,
            'sale_order_id':self.sale_order_id.id,
            'equipment_id':self.equipment_id.id,
            'project_id':self.project_id.id
            })
        task_copy_obj.write(value)
        return task

    def action_view_picking(self):
        # print('stock.view_picking_form')
        picking_id = self.env['stock.picking'].search([('task_id','=',self.id),('state','=','done')])
        action = self.env.ref('stock.action_picking_tree_all').read()[0]
        if len(picking_id) > 0:
            self.check_picking = True
        if len(picking_id) > 1:
            action['domain'] = [('id', 'in', picking_id.ids)]
        elif picking_id:
            action['views'] = [(self.env.ref('stock.view_picking_form').id, 'form')]
            action['res_id'] = picking_id.ids[0]
        else:
            action = {'type': 'ir.actions.act_window_close'}
        return action

    def compute_picking_count(self):
        count = 0
        picking_id = self.env['stock.picking'].search([('task_id','=',self.id),('state','=','done')])
        # print('picking_id',picking_id)
        for rec in picking_id:
            count += 1
        self.picking_count = count

    @api.multi
    def compute_picking_task(self):
        count = 0
        picking_id = self.env['stock.picking'].search([('task_id','=',self.id),('state','=','done')])
        return picking_id

    # @api.onchange('project_id')
    # def _onchange_project(self):
    #     if self.project_id:
    #         if self.project_id.team_id.user_id:
    #            self.user_id = self.project_id.team_id.user_id 
    #         if self.project_id.partner_id:
    #             self.partner_id = self.project_id.partner_id
    #         self.stage_id = self.stage_find(self.project_id.id, [('fold', '=', False)])
    #     else:
    #         self.stage_id = False

    # @api.multi
    # def name_get(self):
    #     result = []
    #     for task in self:
    #         task_name = '[%s] %s' % (
    #             task.name, task.number)
    #         result.append((task.id, task_name))
    #     return result

    # @api.model
    # def name_search(self, name, args=None, operator='ilike',limit=100):
    #     if self._context.get('task_so_equipment'):
    #         tasks = self.env['project.task']
    #         tasks = self.search(['|', ('equipment_id', '!=', False), ('sale_order_id', '!=', False)])
    #         if name:
    #             tasks = self.search(['|',('name', operator, name),('number', operator, name), ('id', 'in',tasks.ids)], limit=limit)
    #         args = [('id', 'in', tasks.ids)]
    #         return tasks.name_get()


class ProjectTaskCopy(models.Model):
    _name = "copy.project.task"
 
    name = fields.Char(string='Task Title', track_visibility='always',index=True)
    task_id = fields.Many2one('project.task', 'Task')
    number = fields.Char(string='Number')
    installation_location_id = fields.Many2one('res.partner', string='Installation Location')
    equipment_id = fields.Many2one('equipment.works.orders')
    project_id = fields.Many2one('project.project', 'Project')
    sale_order_id = fields.Many2one('sale.order', string='Source Sale Order', readonly=True, help='This field displays customer name')

    @api.multi
    def name_get(self):
        result = []
        for task in self:
            task_name = '[%s] %s' % (
                task.name, task.number)
            result.append((task.id, task_name))
        return result

    @api.model
    def name_search(self, name, args=None, operator='ilike',limit=100):
        if self._context.get('task_so_equipment'):
            tasks = self.search([
            ('sale_order_id', '=', self._context.get('task_so_equipment')),
            ('task_id.active','=',True),
            ('task_id.stage_id.stage_cancel','=',False),
            ('task_id.stage_id.stage_done','=',False)
            ])
            if name:
                tasks = self.search(['|',('name', operator, name),('number', operator, name), ('id', 'in',tasks.ids)], limit=limit)
            args = [('id', 'in', tasks.ids)]
            # print('args+++++++++', args)
            return tasks.name_get()
        elif self._context.get('task_eqipment_id'):
            # print('elif+++++++++++++++', self._context.get('task_eqipment_id'))
            tasks = self.search([
            ('equipment_id', '=', self._context.get('task_eqipment_id')),('task_id.active','=',True),
            ('task_id.stage_id.stage_cancel','=',False),('task_id.stage_id.stage_done','=',False)
            ])
            if name:
                tasks = self.search(['|',('name', operator, name),('number', operator, name), ('id', 'in',tasks.ids)], limit=limit)
            args = [('id', 'in', tasks.ids)]
            # print('args+++++++++', args)
            return tasks.name_get()


      

