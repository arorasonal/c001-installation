# -*- coding: utf-8 -*-
{
    'name': "Installation-Deinstallation Team",

    'summary': """
        Installation-Deinstallation Team""",

    'description': """
        Installation-Deinstallation Team
    """,

    'author': "Dev-Team",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/master/odoo/addons/base/module/module_data.xml
    # for the full list
    'category': 'Uncategorized',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['stock','sale_cjpl','sale_order_type','abs_sales_order_task','account','project','sale','project_cjpl','project_cjpl','sale_order_contact','warehouse_extension'],

    # always loaded
    'data': [
        'security/security_equipemnt.xml',
        'security/ir.model.access.csv',
        # 'data/security_rule.xml',
        'data/ir_sequence_data.xml',
        'views/sale_view.xml',
        'views/project_team_view.xml',
        'views/equipment_views.xml',
        'views/project_task_view.xml',
        'report/equipment_report_templates.xml',
        'report/equipment_report.xml',
    ],
    # only loaded in demonstration mode
}