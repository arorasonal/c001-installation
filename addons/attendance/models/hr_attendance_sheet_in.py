# -*- coding: utf-8 -*-
#/#############################################################################
#
#    SunInfo Tech
#
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#/#############################################################################
from odoo import api, fields, models
from odoo.exceptions import UserError
from odoo.tools.translate import _
import time
import math
import calendar
from datetime import date
from datetime import datetime
from openerp import tools
from datetime import timedelta
from dateutil import relativedelta
from dateutil.rrule import rrule, DAILY
import sys
import openerp.addons.decimal_precision as dp


class MonthlyAttendanceSheet(models.Model):
    _name = 'monthly.attendance.sheet'
    _inherit = ['mail.thread']
    _description = "Monthly Attendance Sheet"
    _order = 'creating_date desc, id desc'

    @api.multi
    def _total_days_present(self):
        res = {}
        for sheet in self:
            total_cnt = 0
            for line in sheet.attendance_line:
                total_cnt += line.present
            res[sheet.id] = total_cnt
        return res

    @api.multi
    def _total_days_month(self):
        res = {}

        for sheet in self:
            day_from = datetime.strptime(sheet.date_from, "%Y-%m-%d")
            day_to = datetime.strptime(sheet.date_to, "%Y-%m-%d")
            res[sheet.id] = (day_to - day_from).days + 1

        return res

    @api.multi
    def _total_without_pay(self):
        res = {}
        for sheet in self:
            absent_cnt = 0
            for line in sheet.attendance_line:
                if line.leave_without_pay == True:
                    absent_cnt = absent_cnt + 1
            res[sheet.id] = absent_cnt
        return res

    @api.multi
    def _total_with_pay(self):
        res = {}
        for sheet in self:
            absent_cnt = 0
            for line in sheet.attendance_line:
                if line.leave_with_pay == True:
                    absent_cnt = absent_cnt + 1
            res[sheet.id] = absent_cnt
        return res

    location_id = fields.Char('Location')

    date_from = fields.Date('Date From', required=True, states={'draft': [(
        'readonly', False)]}, default=lambda *a: time.strftime('%Y-%m-01'))
    date_to = fields.Date(
        'Date To', required=True,
        states={'draft': [('readonly', False)]}, default=lambda *a: str(
            datetime.now() + relativedelta.relativedelta(months=+1, day=1, days=-1))[:10])
    attendance_line = fields.One2many(
        'monthly.attendance.line', 'attendance_id',
        string='Attendance Line', required=True,
        states={'draft': [('readonly', False)]})
    total_days_month = fields.Integer(
        compute='_total_days_month', string='Total Days Month')
    total_days_present = fields.Float(
        compute='_total_days_present', string='Total Days Present')
    total_days_present_employee = fields.Float(string='Total Present Employee')
    user_id = fields.Many2one('res.users', 'User Id',
                              default=lambda self: self.env.uid)
    company_id = fields.Many2one(
        'res.company', 'Company', index=1,
        help='Let this field empty if this location is shared between all companies',
        states={'draft': [('readonly', False)]},
        default=lambda self: self.env['res.company']._company_default_get())
    creating_date = fields.Datetime(
        'Creation Date', default=fields.Datetime.now)
    # task_id = fields.Many2one('project.task', 'Task')
    state = fields.Selection(
        [('draft', 'Draft Attendence'),
         ('submit', 'Submit To Manager'),
         ('approved', 'Approved')],
        'Status', readonly=True, copy=False, index=True,
        states={'draft': [('readonly', False)]},
        track_visibility='onchange', default='draft')

    @api.multi
    def is_attendance_manager(self):
        ''' Function to check user if it is  Attendance Manager'''
        group_id = self.env['ir.model.data'].get_object_reference(
            'base', 'group_attendence_manager')
        group_id = group_id and group_id[1] or False
        group_rec = self.env['res.groups'].browse(group_id)
        group_user_ids = [x.id for x in group_rec.users]
        if uid in group_user_ids:
            return True
        else:
            return False

    @api.multi
    def _check_edited_by(self):
        for attendance in self:
            if attendance.state == 'approved':
                if not self.is_attendance_manager():
                    raise UserError(_(
                        "Attendance can be modified only by Attendance Manager after it is approved!!"))
        return True

    _constraints = [
        (_check_edited_by, 'Attendance can be modified only by Attendance Manager after it is approved', ['location_id',
                                                                                                          'date_from', 'date_to', 'company_id', 'location_id', 'attendance_line'])
    ]

    @api.multi
    def unlink(self):
        if not self.is_attendance_manager():
            raise UserError(_(
                "Attendance can be deleted only by Attendance Manager after it is approved!!"))
        return super(MonthlyAttendanceSheet, self).unlink(cr, uid, ids, context=context)

    @api.multi
    def submit_manager(self):
        self.write({'state': 'submit'})
        return True

    @api.multi
    def manager_approved(self):
        self.write({'state': 'approved'})
        return True

   #  @api.onchange('loc')
   #  def onchange_location_id(self):
   #      res= []

   #      # atten_pool=self.env('attendance.line')

   #      result = {'value':{
   #                    'attendance_line':[]
        #      }
   #          }
   #      if not loc:
   #          return {'value': result}
   #      employee = self.env['hr.employee'].search([('client_location', '=', loc)])
   #      for emp_id in self.env['hr.employee'].browse(employee):
   #          if emp_id.state != 'left' :
   #             data ={
   #                 'employee_id':emp_id.id,
   #                 # 'present':True,
   #                 # 'day_type':'full'

   #                }
   #             res += [data]
   #      result['value'].update({
   #                    'attendance_line': res ,
        # })
   #      return result


field_list = [
    'one', 'two', 'three', 'four', 'five', 'seven',
    'six', 'eight', 'nine', 'ten', 'one_1', 'one_2',
    'one_3', 'one_4', 'one_5', 'one_6', 'one_7',
    'one_8', 'one_9', 'one_0', 'two_1', 'two_2',
    'two_3', 'two_4', 'two_5', 'two_6', 'two_7',
    'two_8', 'two_9', 'two_0', 'three_1'
]


class MonthlyAttendanceLine(models.Model):
    _name = 'monthly.attendance.line'
    _description = "Monthly Attendance Sheet lines"

    @api.multi
    def _total_days_present(self):
        for line_id in self:
            total_cnt = 0
            for record in field_list:
                attendance = getattr(line_id, record)
                if attendance:
                    total_cnt += 1
            line_id.total_days_present = total_cnt

    attendance_id = fields.Many2one(
        'monthly.attendance.sheet', 'Attendance Reference')
    employee_id = fields.Many2one('hr.employee', 'Employee', required=True)
    hr_attendance_id = fields.Many2one('hr.indian.attendance')
    date_from = fields.Date(related='hr_attendance_id.date_from')
    date_to = fields.Date(related='hr_attendance_id.date_to')
    state = fields.Selection(related='hr_attendance_id.state')
    one = fields.Boolean('1', default=False)
    two = fields.Boolean('2', default=False)
    three = fields.Boolean('3', default=False)
    four = fields.Boolean('4', default=False)
    five = fields.Boolean('5', default=False)
    seven = fields.Boolean('7', default=False)
    six = fields.Boolean('6', default=False)
    eight = fields.Boolean('8', default=False)
    nine = fields.Boolean('9', default=False)
    ten = fields.Boolean('10', default=False)
    one_1 = fields.Boolean('11', default=False)
    one_2 = fields.Boolean('12', default=False)
    one_3 = fields.Boolean('13', default=False)
    one_4 = fields.Boolean('14', default=False)
    one_5 = fields.Boolean('15', default=False)
    one_6 = fields.Boolean('16', default=False)
    one_7 = fields.Boolean('17', default=False)
    one_8 = fields.Boolean('18', default=False)
    one_9 = fields.Boolean('19', default=False)
    one_0 = fields.Boolean('20', default=False)
    two_1 = fields.Boolean('21', default=False)
    two_2 = fields.Boolean('22', default=False)
    two_3 = fields.Boolean('23', default=False)
    two_4 = fields.Boolean('24', default=False)
    two_5 = fields.Boolean('25', default=False)
    two_6 = fields.Boolean('26', default=False)
    two_7 = fields.Boolean('27', default=False)
    two_8 = fields.Boolean('28', default=False)
    two_9 = fields.Boolean('29', default=False)
    two_0 = fields.Boolean('30', default=False)
    three_1 = fields.Boolean('31')
    remarks = fields.Char('Remarks', size=250)
    total_days_present = fields.Integer(
        compute='_total_days_present', string='Present Days')

    total_month_day = fields.Integer(
        related='attendance_id.total_days_month', string=" Month Day", readonly=True)
    date_from_related = fields.Date(
        related='attendance_id.date_from', string="Date From", readonly=True)
    date_to_related = fields.Date(
        related='attendance_id.date_to', string="Date To", readonly=True)
    employee_id = fields.Many2one('hr.employee', 'Employee')
    present = fields.Float(
        'Present Days', digits=dp.get_precision('Payroll'))
    over_time = fields.Float('Over Time(In Hours)',
                             digits=dp.get_precision('Payroll'))
    # total_leave = fields.Float(
    #     compute='_get_total_leave_per_emp', string='Legal Leaves')
    legal_leave = fields.Float(
        compute='_get_legal_leave_per_emp', string='Legal Leaves', store=True)
    # total_unpaid_leave = fields.Float(
    #     compute='_get_total_unpaid_leave_per_emp', string='LWP')
    total_unpaid_leave = fields.Float(
        compute='_get_lwp_leave', string='Unpaid Leaves', store=True)
    remarks = fields.Char('Remarks', size=250)
    week_offs = fields.Integer(
        compute='_get_number_of_days', string='Weekly Offs', store=True)
    total_days = fields.Float(string='Total Attendance')
    sr = fields.Char('Sr. No.')
    holidays = fields.Float(compute='_get_holidays',
                            string='Holidays', store=True)
    paid_days = fields.Float(compute='_get_paid_leave',
                             string='Paid Days', store=True)
    pay_days = fields.Float(compute='_get_pay_leave',
                            string='Pay Days', store=True)

    @api.multi
    @api.depends('employee_id')
    def _get_holidays(self):
        DATE_FORMAT = "%Y-%m-%d"
        for obj in self:
            from_date = datetime.strptime(obj.date_from, DATE_FORMAT)
            to_date = datetime.strptime(obj.date_to, DATE_FORMAT)
            fiscal_year = self.env['account.fiscalyear']
            hr_holiday_obj = self.env['hr.holidays.calendar']
            holiday = 0
            year_id = fiscal_year.search([
                ('date_start', '<=', obj.date_from),
                ('date_stop', '>=', obj.date_to)],
                limit=1)
            if year_id:
                holiday_id = hr_holiday_obj.search(
                    [('fiscal_id', '=', year_id.id),
                     ('state_id', '=', obj.employee_id.location.state_id.id)])
                for holidays in holiday_id.period_ids:
                    date_start = datetime.strptime(
                        holidays.date_start, DATE_FORMAT)
                    date_to = datetime.strptime(holidays.date_end, DATE_FORMAT)
                    for dt in rrule(DAILY, dtstart=date_start, until=date_to):
                        if (dt >= from_date) and (dt <= to_date):
                            holiday = holiday + 1
                    # if (date_start < from_date) and (date_to >= from_date):
                    #     holiday = (date_to - from_date).days + holiday + 1
                    # elif (date_start >= from_date) and (date_to <= date_to):
                    #     holiday = (date_to - date_start).days + holiday + 1
                    # elif (date_start >= from_date) and (date_to > to_date):
                    #     holiday = (date_to - from_date).days + holiday + 1

                obj.holidays = holiday

    @api.multi
    @api.depends('employee_id')
    def _get_legal_leave_per_emp(self):
        holiday_obj = self.env['hr.holidays']
        DATE_FORMAT = "%Y-%m-%d"
        for obj in self:
            from_date = datetime.strptime(obj.date_from, DATE_FORMAT)
            to_date = datetime.strptime(obj.date_to, DATE_FORMAT)
            leaves = 0
            legal_holidays = holiday_obj.search([('employee_id', '=', obj.employee_id.id),
                                                 ('type', '=', 'remove'),
                                                 '|',
                                                 ('state', '=', 'validate'),
                                                 ('state', '=', 'confirm'),
                                                 ('holiday_status_id.name', '!=',
                                                  'Unpaid Leave'),
                                                 ('holiday_status_id.name', 'not in',
                                                  ('Short Leave', 'On Duty Leave')),
                                                 ])
            for holiday in legal_holidays:
                date_start = datetime.strptime(holiday.date_from, DATE_FORMAT)
                date_to = datetime.strptime(holiday.date_to, DATE_FORMAT)
                # for dt in rrule(DAILY, dtstart=date_start, until=date_to):
                #         if (dt >= from_date) and (dt <= to_date):
                #             leaves = leaves + 1
                if (date_start < from_date) and (date_to >= from_date):
                    leaves = holiday.number_of_days_temp - \
                        (from_date - date_start).days + leaves
                elif (date_start >= from_date) and (date_to <= to_date):
                    leaves = holiday.number_of_days_temp + leaves
                elif (date_start <= to_date) and (date_to > to_date):
                    leaves = holiday.number_of_days_temp + \
                        (to_date - date_to).days + leaves
            obj.legal_leave = leaves

    @api.multi
    @api.depends('employee_id')
    def _get_lwp_leave(self):
        holiday_obj = self.env['hr.holidays']
        DATE_FORMAT = "%Y-%m-%d"
        for obj in self:
            from_date = datetime.strptime(obj.date_from, DATE_FORMAT)
            to_date = datetime.strptime(obj.date_to, DATE_FORMAT)
            leaves = 0
            lwp_holidays = holiday_obj.search([
                ('employee_id', '=', obj.employee_id.id),
                '|',
                ('state', '=', 'validate'),
                ('state', '=', 'confirm'),
                ('holiday_status_id.name', '=', 'Unpaid Leave')])
            for holiday in lwp_holidays:
                date_start = datetime.strptime(holiday.date_from, DATE_FORMAT)
                date_to = datetime.strptime(holiday.date_to, DATE_FORMAT)
                # for dt in rrule(DAILY, dtstart=date_start, until=date_to):
                #         if (dt >= from_date) and (dt <= to_date):
                #             leaves = leaves + 1
                if (date_start < from_date) and (date_to >= from_date):
                    leaves = holiday.number_of_days_temp - \
                        (from_date - date_start).days + leaves
                elif (date_start >= from_date) and (date_to <= to_date):
                    leaves = holiday.number_of_days_temp + leaves
                elif (date_start <= to_date) and (date_to > to_date):
                    leaves = holiday.number_of_days_temp + \
                        (to_date - date_to).days + leaves
            obj.total_unpaid_leave = leaves

    @api.multi
    @api.depends('employee_id')
    def _get_number_of_days(self):
        """ Returns a float equals to the timedelta between two dates given as string."""
        for obj in self:
            resource_day = []
            week_off_day = 0
            from_dt = fields.Datetime.from_string(obj.date_from)
            to_dt = fields.Datetime.from_string(obj.date_to)
            contract_obj = self.env['hr.contract']
            resource_calendar = contract_obj.search([
                ('employee_id', '=', obj.employee_id.id),
                ('status1', 'not in', ['close_contract', 'terminated'])
            ])
            if resource_calendar:
                resource_calendar = resource_calendar.working_hours
                working_days = obj.employee_id.calendar_id.get_working_hours(
                    start_dt=from_dt, end_dt=to_dt, resource_id=resource_calendar.id)
                now = datetime.strptime(obj.date_from, "%Y-%m-%d")
                # cal = calendar.monthcalendar(from_dt.year, from_dt.month)
                # first_week  = cal[0]
                # second_week = cal[1]
                # if first_week[calendar.SATURDAY]:
                #     holi_day = second_week[calendar.SATURDAY]
                #     print"-----------------------------------", holi_day
                for calendar1 in resource_calendar.attendance_ids:
                    resource_day.append(int(calendar1.dayofweek))
                for dt in rrule(DAILY, dtstart=from_dt, until=to_dt):
                    if dt.weekday() not in resource_day:
                        week_off_day += 1
                    obj.week_offs = week_off_day + 1
                # obj.week_offs = calendar.monthrange(now.year, now.month)[
                #     1] - working_day
                # if holi_day:

    @api.multi
    @api.depends('employee_id')
    def _get_pay_leave(self):
        for obj in self:
            start_date = datetime.strptime(
                obj.date_from, tools.DEFAULT_SERVER_DATE_FORMAT)
            end_date = datetime.strptime(
                obj.date_to, tools.DEFAULT_SERVER_DATE_FORMAT)
            delta = end_date - start_date
            obj.pay_days = delta.days + 1
            # contract_obj = self.env['hr.contract']
            # emp_pay_days = contract_obj.search(
            #     [('employee_id', '=', obj.employee_id.id),
            #      ('status1', 'not in', ['close_contract', 'terminated'])])
            # if emp_pay_days:

            #     if emp_pay_days.pay_day_calculation == 'days_in_month':
            #         now = datetime.strptime(obj.date_from, "%Y-%m-%d")
            #         obj.pay_days = calendar.monthrange(now.year, now.month)[1]
            #     else:
            #         obj.pay_days = emp_pay_days.pay_days

    @api.multi
    @api.depends('employee_id')
    def _get_paid_leave(self):
        DATE_FORMAT = "%Y-%m-%d"
        for obj in self:
            date_start = datetime.strptime(obj.date_from, DATE_FORMAT)
            date_to = datetime.strptime(obj.date_to, DATE_FORMAT)
            contract_obj = self.env['hr.contract']
            contract_rec = contract_obj.search(
                [('employee_id', '=', obj.employee_id.id),
                 ('status1', 'not in', ['close_contract', 'terminated'])])
            joining_date = datetime.strptime(
                obj.employee_id.date_of_joining, DATE_FORMAT)
            if obj.pay_days:
                if ((joining_date > date_start) and (joining_date <= date_to)) or contract_rec.date_end:
                    if (joining_date > date_start) and (joining_date <= date_to):
                        obj.paid_days = obj.total_days_present
                    if contract_rec.date_end:
                        exit_date = datetime.strptime(
                            contract_rec.date_end, "%Y-%m-%d")
                        if (exit_date >= date_start) and (exit_date <= date_to):
                            obj.paid_days = obj.pay_days - \
                                obj.total_unpaid_leave - \
                                (date_to - exit_date).days
                else:
                    obj.paid_days = obj.pay_days - obj.total_unpaid_leave
    # _constraints = [(_check_total_present, 'Invalid Present Detail .!!', ['present'])]


# class ResourceMixin(models.AbstractModel):
#     _inherit = "resource.mixin"

#     def get_work_days_data(self, from_datetime, to_datetime, calendar=None):
#         if self._context.get('attendance_flag') == 'payroll':
#             attendance = self.env['monthly.attendance.line'].search([
#                 ('employee_id', '=', self.id),
#                 ('date_from', '=', from_datetime),
#                 ('date_to', '=', to_datetime),
#                 ('state', '=', 'approved')
#             ])
#             return {
#                 'days': attendance.paid_days,
#                 'hours': attendance.paid_days * 8
#             }
#         else:
# return super(ResourceMixin, self).get_work_days_data(from_datetime,
# to_datetime, calendar)

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
