
# -*- coding: utf-8 -*-
#/#############################################################################
#
#    SunInfo Tech
#
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#/#############################################################################
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT
from odoo import api, fields, models, tools, SUPERUSER_ID, _
from odoo.tools.translate import _
from odoo.exceptions import UserError, AccessError
from datetime import date
from datetime import datetime
from datetime import timedelta
from dateutil.rrule import rrule, DAILY
from tempfile import TemporaryFile
from dateutil import relativedelta
from openerp import netsvc
import odoo.addons.decimal_precision as dp
import time
import sys
import base64
import calendar


class HrAttendanceSheet(models.Model):
    _name = 'hr.indian.attendance'
    _description = "Attendance Sheet"
    _order = 'creating_date desc, id desc'
    _rec_name = 'creating_date'
    _inherit = ['mail.thread']

    field_list = [
        'one', 'two', 'three', 'four', 'five', 'seven',
        'six', 'eight', 'nine', 'ten', 'one_1', 'one_2',
        'one_3', 'one_4', 'one_5', 'one_6', 'one_7',
        'one_8', 'one_9', 'one_0', 'two_1', 'two_2',
        'two_3', 'two_4', 'two_5', 'two_6', 'two_7',
        'two_8', 'two_9', 'two_0', 'three_1'
    ]

    @api.multi
    def _total_days_present(self):
        res = {}
        for sheet in self:
            total_cnt = 0
            for line in sheet.attendance_line:
                total_cnt += line.present
            res[sheet.id] = total_cnt
        return res

    @api.multi
    def _total_days_month(self):
        for sheet in self:
            day_from = datetime.strptime(sheet.date_from, "%Y-%m-%d")
            day_to = datetime.strptime(sheet.date_to, "%Y-%m-%d")
            sheet.total_days_month = (day_to - day_from).days + 1

    @api.multi
    def _total_without_pay(self):
        res = {}
        for sheet in self:
            absent_cnt = 0
            for line in sheet.attendance_line:
                if line.leave_without_pay is True:
                    absent_cnt = absent_cnt + 1
            res[sheet.id] = absent_cnt
        return res

    @api.multi
    def _total_with_pay(self):
        res = {}
        for sheet in self:
            absent_cnt = 0
            for line in sheet.attendance_line:
                if line.leave_with_pay is True:
                    absent_cnt = absent_cnt + 1
            res[sheet.id] = absent_cnt
        return res

    def _default_employee(self):
        return self.env['hr.employee'].search([('user_id', '=', self.env.uid)], limit=1)

    payroll_batche = fields.Many2one('payroll.batches', 'Payroll Batche')
    employee_id = fields.Many2one(
        'hr.employee',
        string="Employee",
        default=_default_employee,
        required=False,
        ondelete='cascade',
        index=True)
    date_from = fields.Date(
        'Date From', required=True, readonly=True,
        states={
            'draft': [('readonly', False)],
            'submit': [('readonly', False)]},
        default=lambda *a: time.strftime('%Y-%m-01'))
    date_to = fields.Date(
        'Date To', required=True, readonly=True,
        states={
            'draft': [('readonly', False)],
            'submit': [('readonly', False)]},
        default=lambda *a: str(datetime.now() + relativedelta.relativedelta(months=+1, day=1, days=-1))[:10])
    attendance_line = fields.One2many(
        'monthly.attendance.line', 'hr_attendance_id',
        readonly=True, string='Attendance Line', required=True,
        states={'draft': [('readonly', False)], 'submit': [('readonly', False)]})
    detailed_attendance_id = fields.One2many(
        'monthly.attendance.line', 'hr_attendance_id')
    total_days_month = fields.Integer(
        compute='_total_days_month', string='Total Days Month')
    total_days_present = fields.Float(
        compute='_total_days_present', string='Total Days Present')
    total_days_present_employee = fields.Float(string='Total Present Employee')
    user_id = fields.Many2one(
        'hr.employee', 'Responsible',
        default=_default_employee,
        states={
            'draft': [('readonly', False)],
            'submit': [('readonly', False)]})
    company_id = fields.Many2one(
        'res.company', 'Company', readonly=True, index=1,
        help='Let this field empty if this location is shared between all companies',
        states={
            'draft': [('readonly', False)],
            'submit': [('readonly', False)]},
        default=lambda self: self.env['res.company']._company_default_get('attendance.sheet'))
    creating_date = fields.Datetime(
        'Creation Date', readonly=True, default=fields.Datetime.now)
    file_import_manual = fields.Binary("Upload CSV File")
    file_import_auto = fields.Binary("Upload CSV File")
    state = fields.Selection(
        [('draft', 'Draft'),
         ('submit', 'Awaiting Approval'),
         ('approved', 'Approved')],
        'Status', readonly=True, copy=False,
        index=True, track_visibility='onchange', default='draft')
    created_by_id = fields.Many2one(
        'res.users', "Created By", default=lambda self: self.env.user)
    mode = fields.Selection([
        ('auto', 'Attendance Summary'),
        ('manual', 'Detailed Attendance'),
    ], 'Attendance Mode', copy=False, index=True, default='manual')

    sandwich_leave_ids = fields.One2many(
        'sandwich.leave', 'hr_indian_attendance_id', 'Sandwich Leave')
    short_leave_ids = fields.One2many(
        'short.leave', 'hr_indian_short_id', 'Short Leave')
    half_day_leave_ids = fields.One2many(
        'half.leave', 'hr_indian_half_id', 'Half Day Leave')
    hr_indian_full_ids = fields.One2many(
        'full.leave', 'hr_indian_full_id', 'Full Day Leave')

    @api.constrains('check_in', 'check_out', 'employee_id')
    def _check_validity(self):

        return True

    @api.onchange('date_to')
    def onchange_date_to(self):
        from_date = datetime.strptime(self.date_from, "%Y-%m-%d")
        to_date = datetime.strptime(self.date_to, "%Y-%m-%d")
        from_month = from_date.month
        to_month = to_date.month
        for rec in self:
            if rec.date_to:
                if from_month != to_month:
                    raise UserError(
                        "Period duration should be with in a month!!")

    @api.multi
    def button_generate_attendance(self):
        print('jjjjjjjjjjjjjjjjjjjjjjjjjjjjjjj')
        week_offs = 0
        holidays = 0
        DATE_FORMAT = "%Y-%m-%d"
        holiday_obj = self.env['hr.holidays']
        from_date = datetime.strptime(self.date_from, "%Y-%m-%d")
        to_date = datetime.strptime(self.date_to, "%Y-%m-%d")
        number_of_days = to_date.day - from_date.day
        emp_obj = self.env['hr.employee']
        fiscal_year = self.env['account.fiscalyear']
        hr_holiday_obj = self.env['hr.holidays.calendar']
        self.detailed_attendance_id.unlink()
        self.sandwich_leave_ids.unlink()
        self.short_leave_ids.unlink()
        self.half_day_leave_ids.unlink()
        self.hr_indian_full_ids.unlink()
        self._generate_attendance_sheet()
        line_data = []
        domain = []
        for obj in self:
            resource_day = []
            week_off_day = 0
            holiday = 0
            contract_obj = self.env['hr.contract']
            resource_calendar = contract_obj.search([
                ('employee_id', '=', obj.employee_id.id),
                ('status1', 'not in', ['close_contract', 'terminated'])
            ])
            year_id = fiscal_year.search([
                ('date_start', '<=', obj.date_from),
                ('date_stop', '>=', obj.date_to)],
                limit=1)
            if resource_calendar:
                resource_calendar = resource_calendar.working_hours
                for calendar1 in resource_calendar.attendance_ids:
                    resource_day.append(int(calendar1.dayofweek))
                for dt in rrule(DAILY, dtstart=from_date, until=to_date):
                    if dt.weekday() not in resource_day:
                        week_off_day += 1
                cal = calendar.monthcalendar(from_date.year, to_date.month)
                first_week = cal[0]
                second_week = cal[1]
                if first_week[calendar.SATURDAY]:
                    holi_day = second_week[calendar.SATURDAY]
                    # if holi_day:
                    #     week_offs = week_off_day + 1
            if year_id:
                holiday_id = hr_holiday_obj.search(
                    [('fiscal_id', '=', year_id.id),
                     ('state_id', '=', obj.employee_id.location.state_id.id)])
                for holidays in holiday_id.period_ids:
                    date_start = datetime.strptime(
                        holidays.date_start, DATE_FORMAT)
                    date_to = datetime.strptime(holidays.date_end, DATE_FORMAT)
                    for dt in rrule(DAILY, dtstart=date_start, until=date_to):
                        if (dt >= from_date) and (dt <= to_date):
                            holiday = holiday + 1
                holidays = holiday
        if self.payroll_batche:
            domain = [('payroll_batche', '=', self.payroll_batche.id),
                      ('date_of_joining', '<=', self.date_to)
                      ]
        else:
            domain = [('date_of_joining', '<=', self.date_to)]
        for count, emp in enumerate(emp_obj.search(domain)):
            line_data.append({
                'employee_id': emp.id,
            })
            for days, record in enumerate(self.field_list):
                absent_days = self.get_absent_days(from_date, to_date, emp)
                if days + 1 not in absent_days:
                    line_data[count][record] = True
                if days == number_of_days:
                    break
            date1 = self.date_from
            date2 = self.date_to
            data = self._cr.execute(
                ''' select
                    employee_id,
                    check_in,
                    check_out
                from
                    hr_attendance
                where
                    employee_id = %s and
                    (check_in)::date between '%s' and '%s'
                    order by check_in
                ''' % (emp.id, date1, date2))
            rec_attendance = self._cr.fetchall()
            ab = len(rec_attendance)
            test_attendance = ab + week_offs + holidays
            if not rec_attendance:
                holiday_rec = holiday_obj.search([
                    ('employee_id', '=', emp.id),
                    ('type', '=', 'remove'),
                    '|',
                    ('state', '=', 'validate'),
                    ('state', '=', 'confirm'),
                    ('holiday_status_id.name',
                     '=', 'Casual Leave'),
                    ('date_from', '>=', date1),
                    ('date_from', '<=', date2)])
                for rec in holiday_rec:
                    rec.write({'state': 'lapsed'})
            elif test_attendance <= 11:
                holiday_rec = holiday_obj.search([
                    ('employee_id', '=', emp.id),
                    ('type', '=', 'remove'),
                    '|',
                    ('state', '=', 'validate'),
                    ('state', '=', 'confirm'),
                    ('holiday_status_id.name',
                     '=', 'Earned Leave'),
                    ('date_from', '>=', date1),
                    ('date_from', '<=', date2)])
                for rec in holiday_rec:
                    if rec.remaining_leave == rec.number_of_days_temp:
                        rec.write({'state': 'lapsed'})
        self.attendance_line = line_data
        return True

    @api.multi
    def get_absent_days(self, from_date, to_date, employee_id):
        resource_day = []
        week_day = []
        list_att = []
        list_att1 = []
        rec_att = []
        holiday_obj = self.env['hr.holidays']
        calendar_obj = self.env['hr.holidays.calendar']
        fiscal_year = self.env['account.fiscalyear']
        contract_obj = self.env['hr.contract']
        contract_rec = contract_obj.search([
            ('employee_id', '=', employee_id.id),
            ('status1', 'not in', ['close_contract', 'terminated'])
        ])
        # if contract_rec.date_end:
        #     exit_date = datetime.strptime(contract_rec.date_end, "%Y-%m-%d")
        #     if (exit_date >= from_date) and (exit_date < to_date):
        #         for dt in rrule(DAILY, dtstart=(exit_date + timedelta(days=1)), until=to_date):
        #             absent_days.append(dt.day)
        # joining_date = datetime.strptime(
        #     employee_id.date_of_joining, "%Y-%m-%d")
        # if (joining_date > from_date) and (joining_date <= to_date):
        #     for dt in rrule(DAILY, dtstart=from_date, until=(joining_date - timedelta(days=1))):
        #         absent_days.append(dt.day)
        # year_id = fiscal_year.search([
        #     ('date_start', '<=', from_date),
        #     ('date_stop', '>=', to_date)],
        #     limit=1)
        # resource_calendar = contract_rec.working_hours
        # if resource_calendar:
        #     for calendar1 in resource_calendar.attendance_ids:
        #         resource_day.append(int(calendar1.dayofweek))
        #     for day in range(7):
        #         if day not in resource_day:
        #             week_day.append(day)
        #     for dt in rrule(DAILY, dtstart=from_date, until=to_date):
        #         if dt.weekday() in week_day:
        #             absent_days.append(dt.day)
        #     cal = calendar.monthcalendar(from_date.year, from_date.month)
        #     first_week = cal[0]
        #     second_week = cal[1]
        #     if first_week[calendar.SATURDAY]:
        #         holi_day = second_week[calendar.SATURDAY]
        #         absent_days.append(holi_day)
        # holiday_rec = holiday_obj.search([
        #                                 ('employee_id', '=', employee_id.id),
        #                                 ('type', '=', 'remove'),
        #     '|',
        #                                 ('state', '=', 'validate'),
        #                                 ('state', '=', 'confirm'),
        #                                 ('holiday_status_id.name', 'not in',
        #                                  ('On Duty Leave', 'Short Leave')),
        #                                 ('number_of_days_temp', '!=', 0.50)])
        # for rec in holiday_rec:
        #     date_from = datetime.strptime(rec.date_from, "%Y-%m-%d")
        #     date_to = datetime.strptime(rec.date_to, "%Y-%m-%d")
        #     for dt in rrule(DAILY, dtstart=date_from, until=date_to):
        #         if (dt >= from_date) and (dt <= to_date):
        #             if dt.day not in absent_days:
        #                 absent_days.append(dt.day)
        # if year_id:
        #     holiday_id = calendar_obj.search(
        #         [('fiscal_id', '=', year_id.id),
        #          ('state_id', '=', employee_id.location.state_id.id)])
        #     for holidays in holiday_id.period_ids:
        #         date_start = datetime.strptime(holidays.date_start, "%Y-%m-%d")
        #         date_to = datetime.strptime(holidays.date_end, "%Y-%m-%d")
        #         for dt in rrule(DAILY, dtstart=date_start, until=date_to):
        #             if (dt >= from_date) and (dt <= to_date):
        #                 if dt.day not in absent_days:
        #                     absent_days.append(dt.day)
        data = self._cr.execute(
            ''' select
                employee_id,
                check_in,
                check_out
            from
                hr_attendance
            where
                employee_id = %s and
                (check_in)::date between '%s' and '%s'
                order by check_in
            ''' % (employee_id.id, from_date, to_date))
        rec_attendance = self._cr.fetchall()
        delta = to_date - from_date
        absent_days = []
        for i in range(delta.days + 1):
            date_greater = from_date + timedelta(days=i)
            d1 = date_greater.strftime("%Y-%m-%d")
            list_att1.append(d1)
        for rec_att in rec_attendance:
            dates = rec_att[1].split(' ')[0]
            list_att.append(dates)
        new_list = set(list_att1).difference(list_att)
        for rec_list in new_list:
            rec_ads = rec_list.split('-')[2]
            absent_days.append(int(rec_ads))
        return absent_days

    @api.multi
    def manual_button_import(self):
        line_obj = self.env['monthly.attendance.line']
        employee_obj = self.env['hr.employee']
        for record in self.detailed_attendance_id:
            record.unlink()
        fileobj = TemporaryFile('w+')
        if self.file_import_manual:
            x = base64.decodestring(self.file_import_manual).split('\n')
            length = len(base64.decodestring(
                self.file_import_manual).split('\n'))
            f = []
            total_length = length - 1
            i = 1
            for data in range(1, total_length):
                f = x[data]
                v = f.split(",")
                attendence_search_id = employee_obj.search(
                    [('employee_code', '=', v[0])])
                if attendence_search_id:
                    row = 3
                    count = 1
                    for record in v:
                        if count <= 3 or count > 34:
                            count += 1
                        elif v[row] is "A" or v[row] is "a":
                            v[row] = False
                            row += 1
                            count += 1
                        elif v[row] is "P" or v[row] is "p":
                            v[row] = True
                            row += 1
                            count += 1
                        else:
                            v[row] = False
                            row += 1
                            count += 1
                    list_id = {
                        'employee_id': attendence_search_id.id,
                        'one': v[3], 'two': v[4], 'three': v[5],
                        'four': v[6], 'five': v[7], 'six': v[8],
                        'seven': v[9], 'eight': v[10], 'nine': v[11], 'ten': v[12],

                        'one_1': v[13], 'one_2': v[14], 'one_3': v[15],
                        'one_4': v[16], 'one_5': v[17], 'one_6': v[18],
                        'one_7': v[19], 'one_8': v[20], 'one_9': v[21], 'one_0': v[22],

                        'two_1': v[23], 'two_2': v[24], 'two_3': v[25],
                        'two_4': v[26], 'two_5': v[27], 'two_6': v[28],
                        'two_7': v[29], 'two_8': v[30], 'two_9': v[31], 'two_0': v[32],
                        'three_1': v[33], 'remarks': v[34],
                        'hr_attendance_id': self.id,
                    }
                    line_obj.create(list_id)

    @api.multi
    def get_days_week(self, days):
        dayofweek = ''
        if days == 'Monday':
            dayofweek = '0'
        elif days == 'Tuesday':
            dayofweek = '1'
        elif days == 'Wednesday':
            dayofweek = '2'
        elif days == 'Thursday':
            dayofweek = '3'
        elif days == 'Friday':
            dayofweek = '4'
        elif days == 'Saturday':
            dayofweek = '5'
        elif days == 'Sunday':
            dayofweek = '6'
        return dayofweek

    @api.multi
    def _generate_attendance_sheet(self):
        print('testtttttttttttttt')
        emp_obj = self.env['hr.employee']
        line_data = []
        line_data1 = []
        line_data2 = []
        list2 = []
        domain = []
        resource_day = []
        holiday_obj = self.env['hr.holidays']
        date1 = self.date_from
        date2 = self.date_to
        start = datetime.strptime(date1, '%Y-%m-%d')
        end = datetime.strptime(date2, '%Y-%m-%d')
        DATE_FORMAT = "%Y-%m-%d"
        if self.payroll_batche:
            domain = [('payroll_batche', '=', self.payroll_batche.id),
                      ('date_of_joining', '<=', self.date_to)
                      ]
        else:
            domain = [('date_of_joining', '<=', self.date_to)]
        for obj in self:
            from_date = datetime.strptime(obj.date_from, DATE_FORMAT)
            to_date = datetime.strptime(obj.date_to, DATE_FORMAT)
            fiscal_year = self.env['account.fiscalyear']
            hr_holiday_obj = self.env['hr.holidays.calendar']
            holiday = 0
            year_id = fiscal_year.search([
                ('date_start', '<=', obj.date_from),
                ('date_stop', '>=', obj.date_to)],
                limit=1)
        cal = calendar.monthcalendar(from_date.year, from_date.month)
        first_week = cal[0]
        second_week = cal[1]
        third_week = cal[2]
        if first_week[calendar.SATURDAY]:
            holi_day = second_week[calendar.SATURDAY]
        else:
            holi_day = third_week[calendar.SATURDAY]
        first_date = from_date.replace(day=holi_day)
        time_in_datetime2 = datetime.strptime(
            str(first_date), '%Y-%m-%d %H:%M:%S')
        list2.append(str(time_in_datetime2))
        short_list = []
        half_list = []
        for count, emp in enumerate(emp_obj.search(domain)):
            contract_obj = self.env['hr.contract']
            contract_obj_id = contract_obj.search([
                ('employee_id', '=', emp.id),
                ('status1', 'not in', ['close_contract', 'terminated'])
            ])
            holiday_rec_short = holiday_obj.search([
                ('employee_id', '=', emp.id),
                ('type', '=', 'remove'),
                '|',
                ('state', '=', 'validate'),
                ('state', '=', 'confirm'),
                ('holiday_status_id.name', '=', 'Short Leave'),
                ('date_from', '>=', start),
                ('date_from', '<=', end)])
            holiday_rec_half = holiday_obj.search([
                ('employee_id', '=', emp.id),
                ('type', '=', 'remove'),
                '|',
                ('state', '=', 'validate'),
                ('state', '=', 'confirm'),
                ('date_from', '>=', start),
                ('date_from', '<=', end),
                ('number_of_days_temp', '=', 0.50)])
            for short_date in holiday_rec_short:
                start_date_short = datetime.strptime(
                    short_date.date_from, '%Y-%m-%d')
                short_list.append(str(start_date_short.date()))
            for half_date in holiday_rec_half:
                start_date_half = datetime.strptime(
                    half_date.date_from, '%Y-%m-%d')
                half_list.append(str(start_date_half.date()))
            for dt in rrule(DAILY, dtstart=start, until=end):
                days = dt.strftime("%A")
                dayofweek = self.get_days_week(days)
                contract_ids = self.env['hr.contract'].search([
                    ('employee_id', '=', emp.id),
                    ('status1', 'not in', ['close_contract', 'terminated'])
                ])
                cal_id = contract_ids.working_hours
                resource = self.env['resource.calendar.attendance'].search([
                    ('calendar_id', '=', cal_id.id),
                    ('dayofweek', '=', dayofweek)])
                plan_time = str(resource.hour_from - 0.20) + str(0.00)
                out_time = str(resource.hour_to)
                hr_min = plan_time.split('.')
                hr_minute = out_time.split('.')
                time1 = timedelta(
                    hours=int(hr_min[0]), minutes=int(hr_min[1]))
                time2 = timedelta(
                    hours=int(hr_minute[0]), minutes=int(hr_minute[1]))
                total_act_time = time2 - time1
            data = self._cr.execute(
                ''' select
                    employee_id,
                    check_in,
                    check_out
                from
                    hr_attendance
                where
                    employee_id = %s and
                    (check_in)::date between '%s' and '%s'
                    order by check_in
                ''' % (emp.id, date1, date2))
            recs = self._cr.fetchall()

            for rec in recs:
                rec_sign_in = datetime.strptime(
                    rec[1], '%Y-%m-%d %H:%M:%S') + timedelta(
                    hours=5, minutes=30)
                rec_sign_out = datetime.strptime(
                    rec[2], '%Y-%m-%d %H:%M:%S') + timedelta(
                    hours=5, minutes=30)
                sign_in_time = rec_sign_in.time()
                sign_out_time = rec_sign_out.time()
                sign_time = timedelta(hours=sign_in_time.hour,
                                      minutes=sign_in_time.minute,
                                      seconds=sign_in_time.second)
                off_time = timedelta(hours=sign_out_time.hour,
                                     minutes=sign_out_time.minute,
                                     seconds=sign_out_time.second)
                total_act_hours = off_time - sign_time
                timee = timedelta(
                    hours=int(9), minutes=int(31))
                time1 = timedelta(
                    hours=int(10), minutes=int(1))
                time2 = timedelta(
                    hours=int(12), minutes=int(1))
                time3 = timedelta(
                    hours=int(13), minutes=int(31))
                time4 = timedelta(
                    hours=int(6), minutes=int(31))
                time5 = timedelta(
                    hours=int(4), minutes=int(00))
                time6 = timedelta(
                    hours=int(14), minutes=int(01))
                time7 = timedelta(
                    hours=int(4), minutes=int(30))
                rec_sign_in1 = rec_sign_in - timedelta(
                    hours=5, minutes=30)
                rec_sign_out2 = rec_sign_out - timedelta(
                    hours=5, minutes=30)
                time = sign_time + time5
                rec_sign_in2 = datetime.strptime(
                    str(rec_sign_in1), '%Y-%m-%d %H:%M:%S') + timedelta(hours=5, minutes=30)
                if (sign_time > time1) and (sign_time < time2) and (total_act_hours > time4):
                    if str(rec_sign_in1.date()) not in short_list:
                        line_data.append({
                            'employee_id': emp.id,
                            'start_date': rec_sign_in1,
                            'end_date': rec_sign_out2
                        })
                if ((sign_time > timee) and (sign_time < time1)) and (total_act_hours < total_act_time):
                    if str(rec_sign_in1.date()) not in short_list:
                        line_data.append({
                            'employee_id': emp.id,
                            'start_date': rec_sign_in1,
                            'end_date': rec_sign_out2
                        })
                if ((time <= time6) and ((total_act_hours >= time5) and (total_act_hours <= time4))):
                    if str(rec_sign_in1.date()) not in half_list:
                        line_data1.append({
                            'employee_id': emp.id,
                            'shift_id': "first_half" if timedelta(hours=rec_sign_in2.hour, minutes=rec_sign_in2.minute, seconds=rec_sign_in2.second) < time2 else "second_half",
                            'start_date': rec_sign_in1,
                            'end_date': rec_sign_out2
                        })
                if ((time >= time6) and ((total_act_hours >= time7) and (total_act_hours <= time4)) and (sign_time >= time2)):
                    if str(rec_sign_in1.date()) not in half_list:
                        line_data1.append({
                            'employee_id': emp.id,
                            'shift_id': "first_half" if timedelta(hours=rec_sign_in2.hour, minutes=rec_sign_in2.minute, seconds=rec_sign_in2.second) < time2 else "second_half",
                            'start_date': rec_sign_in1,
                            'end_date': rec_sign_out2
                        })
                else:
                    if ((time >= time6) and ((total_act_hours >= time7) and (total_act_hours <= time4)) and (sign_time <= time2)):
                        if str(rec_sign_in1.date()) not in short_list:
                            line_data.append({
                                'employee_id': emp.id,
                                'start_date': rec_sign_in1,
                                'end_date': rec_sign_out2
                            })
            list_id = []
            if contract_obj_id:
                absent_days = []
                resource_calendar = contract_obj_id.working_hours
                for calendar1 in resource_calendar.attendance_ids:
                    resource_day.append(int(calendar1.dayofweek))
                for dt in rrule(DAILY, dtstart=start, until=end):
                    if dt.weekday() not in resource_day:
                        absent_days.append(dt.date())
                for record in absent_days:
                    time_in_datetime = datetime.strptime(str(record), '%Y-%m-%d')
                    list_id.append(str(time_in_datetime))
            if year_id:
                list_at = []
                holi_days = []
                holiday_id = hr_holiday_obj.search(
                    [('fiscal_id', '=', year_id.id),
                     ('state_id', '=', emp.location.state_id.id)])
                for holidays in holiday_id.period_ids:
                    date_start = datetime.strptime(
                        holidays.date_start, DATE_FORMAT)
                    date_to = datetime.strptime(holidays.date_end, DATE_FORMAT)
                    for dt in rrule(DAILY, dtstart=date_start, until=date_to):
                        if (dt >= from_date) and (dt <= to_date):
                            holi_days.append(dt.date())
                for rec_holi in holi_days:
                    time_in_datetime1 = datetime.strptime(
                        str(rec_holi), '%Y-%m-%d')
                    list_at.append(str(time_in_datetime1))
            list_data = []
            line_data3 = []
            half_dict = {}
            for rec in line_data1:
                date = datetime.strptime(
                    str(rec['start_date']), '%Y-%m-%d %H:%M:%S')
                half_dict.setdefault(rec['employee_id'], {})
                half_dict[rec['employee_id']].setdefault(
                    date.date(), rec['shift_id'])
            for count, emp in enumerate(emp_obj.search(domain)):
                list_hol1 = []
                data = self._cr.execute(
                    ''' select
                    employee_id,
                    check_in,
                    check_out
                from
                    hr_attendance
                where
                    employee_id = %s and
                    (check_in)::date between '%s' and '%s'
                    order by check_in
                ''' % (emp.id, date1, date2))
                recs = self._cr.fetchall()
                list_hol1_dict = {}
                list_data.append(emp.id)
                absent_days = self.get_absent_days(from_date, to_date, emp)

                def Remove(duplicate):
                    final_list = []
                    for num in duplicate:
                        if num not in final_list:
                            final_list.append(num)
                    return final_list
                duplicate = absent_days
                for rec in recs:
                    rec_sign_in = datetime.strptime(
                        rec[1], '%Y-%m-%d %H:%M:%S') + timedelta(hours=5, minutes=30)
                    rec_sign_out = datetime.strptime(
                        rec[2], '%Y-%m-%d %H:%M:%S') + timedelta(hours=5, minutes=30)
                    list_hol1.append(str(rec_sign_in))
                    list_hol1_dict[str(rec_sign_in.date())] = str(rec_sign_in)
                    time1 = (datetime.strptime("14:01:00", '%H:%M:%S')).time()
                    time2 = (datetime.strptime("12:01:00", '%H:%M:%S')).time()
                holidays_list = sorted(list_id + list_at + list2)
                holidays = []
                holiday_rec_list = []
                holiday_rec = holiday_obj.search([
                    ('employee_id', '=', emp.id),
                    ('type', '=', 'remove'),
                    '|',
                    ('state', '=', 'validate'),
                    ('state', '=', 'confirm'),
                    ('date_from', '>=', start),
                    ('date_from', '<=', end),
                    ('number_of_days_temp', '>=', 1)])
                for rec_holi in holiday_rec:
                    d1 = datetime.strptime(rec_holi.date_to, '%Y-%m-%d')
                    d2 = datetime.strptime(rec_holi.date_from, '%Y-%m-%d')
                    delta = d1 - d2
                    for i in range(delta.days + 1):
                        day = d2 + timedelta(days=i)
                        holiday_rec_list.append(str(day.date()))
                for rec in Remove(duplicate):
                    abs_date = start.replace(day=rec)
                    if (str(abs_date.date()) not in holiday_rec_list):
                        if (str(abs_date) not in holidays_list):
                            line_data3.append({
                                'employee_id': emp.id,
                                'start_date': (datetime.strptime(str(abs_date), '%Y-%m-%d %H:%M:%S')).date(),
                                'end_date': (datetime.strptime(str(abs_date), '%Y-%m-%d %H:%M:%S')).date()
                            })
                for cnt, holiday in enumerate(holidays_list):
                    if cnt != 0:
                        if datetime.strptime(holiday, '%Y-%m-%d %H:%M:%S') - timedelta(days=1) == datetime.strptime(holidays[-1][-1], '%Y-%m-%d %H:%M:%S'):
                            if len(holidays[-1]) > 1:
                                holidays[-1][-1] = holiday
                            else:
                                holidays[-1].append(holiday)
                        else:
                            holidays.append([holiday])
                    else:
                        holidays.append([holiday])
                for holiday in holidays:
                    if len(holiday) > 1:
                        holiday_start = (datetime.strptime(
                            holiday[0], '%Y-%m-%d %H:%M:%S') - timedelta(days=1))
                        holiday_end = (datetime.strptime(
                            holiday[1], '%Y-%m-%d %H:%M:%S') + timedelta(days=1))
                    else:
                        holiday_start = (datetime.strptime(
                            holiday[0], '%Y-%m-%d %H:%M:%S') - timedelta(days=1))
                        holiday_end = (datetime.strptime(
                            holiday[0], '%Y-%m-%d %H:%M:%S') + timedelta(days=1))
                    if half_dict.get(emp.id):
                        if (str(holiday_start.date()) not in list_hol1_dict.keys()):
                            if (holiday_end.date() in half_dict.get(emp.id)) and half_dict[emp.id].get(holiday_end.date()) == 'second_half':
                                if (str((datetime.strptime(holiday[0], '%Y-%m-%d %H:%M:%S')).date()) not in holiday_rec_list and str((datetime.strptime(holiday[-1], '%Y-%m-%d %H:%M:%S')).date()) not in holiday_rec_list):
                                    delta = (holiday_end.date()) - \
                                        (holiday_start.date())
                                    line_data2.append({
                                        'employee_id': emp.id,
                                        'start_date': (datetime.strptime(holiday[0], '%Y-%m-%d %H:%M:%S')).date(),
                                        'end_date': (datetime.strptime(holiday[0], '%Y-%m-%d %H:%M:%S')).date() if len(holiday) == 1 else (datetime.strptime(holiday[-1], '%Y-%m-%d %H:%M:%S')).date(),
                                        'duration': delta.days + 0.50
                                    })
                    if half_dict.get(emp.id):
                        if (str(holiday_end.date()) not in list_hol1_dict.keys()):
                            if (holiday_start.date() in half_dict.get(emp.id)) and half_dict[emp.id].get(holiday_start.date()) == 'second_half':
                                if (str((datetime.strptime(holiday[0], '%Y-%m-%d %H:%M:%S')).date()) not in holiday_rec_list and str((datetime.strptime(holiday[-1], '%Y-%m-%d %H:%M:%S')).date()) not in holiday_rec_list):
                                    delta = (holiday_end.date()) - \
                                        (holiday_start.date())
                                    line_data2.append({
                                        'employee_id': emp.id,
                                        'start_date': (datetime.strptime(holiday[0], '%Y-%m-%d %H:%M:%S')).date(),
                                        'end_date': (datetime.strptime(holiday[0], '%Y-%m-%d %H:%M:%S')).date() if len(holiday) == 1 else (datetime.strptime(holiday[-1], '%Y-%m-%d %H:%M:%S')).date(),
                                        'duration': delta.days + 0.50
                                    })
                    if (str(holiday_start.date()) not in list_hol1_dict.keys()) and (str(holiday_end.date()) not in list_hol1_dict.keys()):
                        if (str((datetime.strptime(holiday[0], '%Y-%m-%d %H:%M:%S')).date()) not in holiday_rec_list and str((datetime.strptime(holiday[-1], '%Y-%m-%d %H:%M:%S')).date()) not in holiday_rec_list):
                            delta = (holiday_end.date()) - (holiday_start.date())
                            line_data2.append({
                                'employee_id': emp.id,
                                'start_date': (datetime.strptime(holiday[0], '%Y-%m-%d %H:%M:%S')).date(),
                                'end_date': (datetime.strptime(holiday[0], '%Y-%m-%d %H:%M:%S')).date() if len(holiday) == 1 else (datetime.strptime(holiday[-1], '%Y-%m-%d %H:%M:%S')).date(),
                                'duration': delta.days + 1
                            })
                    if half_dict.get(emp.id):
                        if ((holiday_start.date() in half_dict.get(emp.id)) and (half_dict[emp.id].get(holiday_start.date()) == 'first_half')):
                            if (holiday_end.date() in half_dict.get(emp.id)) and half_dict[emp.id].get(holiday_end.date()) == 'second_half':
                                if (str((datetime.strptime(holiday[0], '%Y-%m-%d %H:%M:%S')).date()) not in holiday_rec_list and str((datetime.strptime(holiday[-1], '%Y-%m-%d %H:%M:%S')).date()) not in holiday_rec_list):
                                    delta = (holiday_end.date()) - \
                                        (holiday_start.date())
                                    line_data2.append({
                                        'employee_id': emp.id,
                                        'start_date': (datetime.strptime(holiday[0], '%Y-%m-%d %H:%M:%S')).date(),
                                        'end_date': (datetime.strptime(holiday[0], '%Y-%m-%d %H:%M:%S')).date() if len(holiday) == 1 else (datetime.strptime(holiday[-1], '%Y-%m-%d %H:%M:%S')).date(),
                                        'duration': delta.days
                                    })
            self.short_leave_ids = line_data
            self.sandwich_leave_ids = line_data2
            self.half_day_leave_ids = line_data1
            self.hr_indian_full_ids = line_data3

    @api.multi
    def action_submit_manager(self):
        start = datetime.strptime(self.date_from, '%Y-%m-%d')
        end = datetime.strptime(self.date_to, '%Y-%m-%d')
        attendance_rec = self.search([('state', '=', 'approved'),
                                      ('date_from', '<=', start),
                                      ('date_to', '>=', end),
                                      ('id', '!=', self.id)])
        for line in self.attendance_line:
            for rec in attendance_rec:
                for pline in rec.attendance_line:
                    if pline.employee_id == line.employee_id:
                        raise UserError(
                            _("Employee cannot belong to two attendance sheets in same period!"))
        self.write({'state': 'submit'})
        return True

    @api.multi
    def action_approve(self):
        self.write({'state': 'approved'})
        return True

    @api.multi
    def action_set_to_draft(self):
        self.write({'state': 'draft'})
        return True

    @api.multi
    def is_attendance_manager(self):
        ''' Function to check user if it is  Attendance Manager'''
        group_id = self.env['ir.model.data'].get_object_reference(
            'hr_attendance', 'group_hr_attendance_user')
        group_id = group_id and group_id[1] or False
        group_rec = self.env['res.groups'].browse(group_id)
        group_user_ids = [x.id for x in group_rec.users]
        if self._uid in group_user_ids:
            return True
        else:
            return False

    @api.multi
    def check_employee(self):
        for rec in self:
            record = self.search([
                ('payroll_batche', '=', rec.payroll_batche.id),
                ('date_from', '>=', rec.date_from),
                ('date_to', '<=', rec.date_to),
            ])
            if record:
                record_lst = record.ids
                record_lst.remove(self.id)
                if record_lst:
                    return False
        return True

    @api.multi
    def _check_edited_by(self):
        for attendance in self:
            if attendance.state == 'approved':
                if not self.is_attendance_manager():
                    raise UserError(_('Warning !'), _(
                        "Attendance can be modified only by Attendance Manager after it is approved!!"))
        return True

    _constraints = [
        (check_employee, 'You can not create attendance for the same interval and same location again.', []),
        (_check_edited_by, 'Attendance can be modified only by Attendance Manager after it is approved', [
            'date_from', 'date_to', 'company_id', 'payroll_batche', 'attendance_line'])
    ]

    @api.multi
    def unlink(self):
        for val in self:
            if val.state == 'approved':
                raise UserError(_(
                    "Can not delete an approved Attendance sheet!"))
        if not self.is_attendance_manager():
            raise UserError(_(
                "Attendance can be deleted only by Attendance Manager after it is approved!!"))
        return super(HrAttendanceSheet, self).unlink()

    @api.multi
    def submit_manager(self):

        template_id = self.env.ref(
            'si_hrm_attendance.email_attendance_submit_manager')[1]
        if template_id:
            template_id.send_mail(self.id, force_send=True)
        self.write({'state': 'submit'})
        return True

    @api.multi
    def manager_approved(self):

        template_id = self.env.ref(
            'si_hrm_attendance.email_attendance_approved')[1]
        if template_id:
            template_id.send_mail(self.id, force_send=True)
        self.write({'state': 'approved'})
        holidays = self.env['hr.holidays']
        self.env.cr.execute(
            "select id from hr_holidays_status where name='EL'")
        status_id = self.env.cr.fetchone()
        status_id = status_id and status_id[0]
        if not status_id:
            raise UserError(_(
                "Please define Leave type EL under Leaves !!"))
        wf_service = netsvc.LocalService("workflow")
        for val in self:
            self.env.cr.execute('''select allowed_leaves from leave_limit_details where holiday_status_id = %s
            and company_id = %s''', (status_id, val.company_id.id,))
            allowed = self.env.cr.fetchone()
            all_leaves = allowed and allowed[0]
            if not all_leaves:
                raise UserError(_(
                    "Please defined per year allowed leaves for company %s under EL Configuration!!")
                    % (val.company_id.name))
            number_of_days_temp = all_leaves / 12

            year = val.date_from[:4]
            holidays_vals = {
                'type': 'add',
                'year': year,
                'company_id': val.company_id.id,
                'name': 'EL',
                'state': 'validate',
                'holiday_type': 'employee',
                'holiday_status_id': status_id,
                'date_to': val.date_to,
                'number_of_days_temp': number_of_days_temp,

            }
            domain = [('holiday_status_id', '=', status_id),
                      ('date_to', '=', val.date_to),
                      ('company_id', '=', val.company_id.id)]
            for line in val.attendance_line:
                if line.employee_id.date_joining >= val.date_from and line.employee_id.date_joining <= val.date_to:
                    total = line.present + line.total_leave + line.over_time / 8
                    number_of_days_temp = total / 20.0

                    holidays_vals['number_of_days_temp'] = number_of_days_temp
                # elif line.present + line.total_leave + line.over_time/8 >=
                # 20:

                domain.append(('employee_id', '=', line.employee_id.id))
                holiday_ids = holidays.search(domain)
                if not holiday_ids:
                    holidays_vals['employee_id'] = line.employee_id.id
                    holidays_vals['name'] = 'EL for ' + line.employee_id.name
                    holiday_id = holidays.create(holidays_vals)
                    wf_service.trg_validate(
                        'hr.holidays', holiday_id, 'validate')
        return True

    @api.multi
    def set_to_draft(self):
        self.env.cr.execute(
            "select id from hr_holidays_status where name='EL'")
        status_id = self.env.cr.fetchone()
        status_id = status_id and status_id[0]
        for val in self:
            date_to = val.date_to
            company_id = val.company_id.id
            for line in val.attendance_line:
                self.env.cr.execute('''delete from hr_holidays as hld where hld.holiday_status_id=%s and hld.date_to=%s
                and hld.company_id=%s and hld.employee_id = %s and hld.type = %s''',
                                    (status_id, date_to, company_id, line.employee_id.id, 'add'))

        return self.write({'state': 'draft'})


class AttendanceLines(models.Model):
    _name = 'attendance.lines'
    _description = "Attendance Sheet"

    @api.multi
    def get_grand_total(self):
        res = {}
        grand_total = 0.0
        for total in self:
            res[total.id] = 0.0
            for t in total.p_release_ids:
                grand_total += t.total
            res[total.id] = grand_total
        return res

    @api.multi
    def _get_total_leave_per_emp(self):
        for line in self:
            if line.employee_id:
                self.env.cr.execute('SELECT sum(number_of_days_temp) from hr_holidays as hp LEFT JOIN hr_holidays_status as hps on (hp.holiday_status_id = hps.id) WHERE hp.employee_id=%s AND (hp.date_from >=%s AND hp.date_to<=%s) AND (hp.state = \'validate\') AND hps.paid_unpaid = True', (
                    line.employee_id.id, line.date_from_related, line.date_to_related))
                data = [i[0] for i in self.env.cr.fetchall()]
                if data[0] is None:
                    line.total_leave = 0.0
                if not data[0] is None:
                    line.total_leave = float(data[0])

    @api.multi
    def _get_total_unpaid_leave_per_emp(self):
        res = {}
        for line in self:
            res[line.id] = 0.0
            from_date = datetime.strptime(
                line.date_from_related, "%Y-%m-%d") - timedelta(1)
            date_to = datetime.strptime(
                line.date_to_related, "%Y-%m-%d") + timedelta(1)
            if line.employee_id:
                self.env.cr.execute('SELECT sum(number_of_days_temp) from hr_holidays as hp LEFT JOIN hr_holidays_status as hps on (hp.holiday_status_id = hps.id) WHERE hp.employee_id=%s AND (hp.date_from > %s AND hp.date_to <%s) AND (hp.state = \'validate\') AND hps.paid_unpaid = False', (line.employee_id.id, from_date, date_to))
                data = [i[0] for i in self.env.cr.fetchall()]
                if data[0] is None:
                    res[line.id] = 0.0
                if not data[0] is None:
                    res[line.id] = float(data[0])
        return res

    @api.multi
    def _check_total_present(self):
        """check the total present """
        for present in self:
            t_leave = present.present + present.total_leave
            if t_leave > present.total_month_day:
                raise UserError(_("Present Days & Total Leave Days sum  %s must be the smaller then the sum of total of Current month days  %s for %s  Employee !!") % (
                    str(t_leave), str(present.total_month_day), str(present.employee_id.name)))
        return True

    @api.multi
    def _get_total_present(self):
        for val in self:

            total_days = val.present + val.total_leave + val.week_offs

            ot = val.over_time
            ot = int(ot / 8)
            val.total_days = total_days + ot

    # attendance_id = fields.Many2one('hr.attendance', 'Attendance Reference')
    # total_month_day = fields.Integer(
    #     related='attendance_id.total_days_month', string=" Month Day", readonly=True)
    # date_from_related = fields.Date(
    #     related='attendance_id.date_from', string="Date From", readonly=True)
    # date_to_related = fields.Date(
    #     related='attendance_id.date_to', string="Date To", readonly=True)
    employee_id = fields.Many2one('hr.employee', 'Employee')
    present = fields.Float(
        'Present Days', digits=dp.get_precision('Payroll'))
    over_time = fields.Float('Over Time(In Hours)',
                             digits=dp.get_precision('Payroll'))

    legal_leave = fields.Float('Legal Leaves')

    total_unpaid_leave = fields.Float('Unpaid Leave')
    remarks = fields.Char('Remarks', size=250)
    week_offs = fields.Integer('Weekly Offs')
    total_days = fields.Float(
        compute='_get_total_present', string='Total Attendance')
    sr = fields.Char('Sr. No.')
    holidays = fields.Float('Holidays')
    paid_days = fields.Float('Paid Days')

    _constraints = [
        (_check_total_present, 'Invalid Present Detail .!!', ['present'])]


class HrHolidaysStatus(models.Model):
    _inherit = "hr.holidays.status"
    _description = "Leave Type"

    paid_unpaid = fields.Boolean(
        'Paid/Unpaid', default=True,
        help="When Selected It will concider as paid leave,unselected would be unpaid leaves")
    leave_limit_ids = fields.One2many(
        'leave.limit.details', 'holiday_status_id', 'Leave Limit Details')
    leave_one = fields.Boolean('Full Allotment')


class LeaveLimitDetails(models.Model):
    _name = 'leave.limit.details'

    holiday_status_id = fields.Many2one('hr.holidays.status', 'Leave Type')
    company_id = fields.Many2one('res.company', 'Company Name')
    leave_limit = fields.Float('Maximum Leave Acuumulate Limit', default=15)
    allowed_leaves = fields.Float('Total Allowed Leaves per Year', default=0.0)


class SandwichLeave(models.Model):
    _name = 'sandwich.leave'

    employee_id = fields.Many2one('hr.employee', 'Employee')
    sandwich_count = fields.Float('Sanswich Count')
    start_date = fields.Date('Start Date')
    end_date = fields.Date('End Date')
    reference = fields.Char("Reference")
    duration = fields.Float('Duration')
    hr_indian_attendance_id = fields.Many2one('hr.indian.attendance')


class ShortLeave(models.Model):
    _name = 'short.leave'

    employee_id = fields.Many2one('hr.employee', 'Employee')
    start_date = fields.Datetime('Time In')
    end_date = fields.Datetime('Time Out')
    hr_indian_short_id = fields.Many2one('hr.indian.attendance')


class HalfLeave(models.Model):
    _name = 'half.leave'

    employee_id = fields.Many2one('hr.employee', 'Employee')
    start_date = fields.Datetime('Time In')
    end_date = fields.Datetime('Time Out')
    shift_id = fields.Selection([
        ('first_half', 'First Half'),
        ('second_half', 'Second Half'),
    ], 'Mode')
    hr_indian_half_id = fields.Many2one('hr.indian.attendance')


class FullDayLeave(models.Model):
    _name = 'full.leave'

    employee_id = fields.Many2one('hr.employee', 'Employee')
    start_date = fields.Date('Leave Date')
    hr_indian_full_id = fields.Many2one('hr.indian.attendance')

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
