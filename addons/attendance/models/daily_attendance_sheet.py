# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2010 Tiny SPRL (<http://tiny.be>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
from odoo import api, fields, models, tools, _
from odoo.exceptions import UserError, AccessError
from datetime import datetime, timedelta
from datetime import date
from openerp import netsvc


class daily_attendance_sheet(models.Model):
    _name = 'daily.attendance.sheet'
    _inherit = ['mail.thread']
    _description = " Daily Attendance Sheet"
    _order = 'date desc'

    location_id = fields.Char('Location')
    daily_attendance_line = fields.One2many(
        'daily.attendance.line', 'attendance_id', readonly=True,
        string='Attendance Line', required=True, states={'draft': [('readonly', False)]})
    user_id = fields.Many2one(
        'res.users', 'User Id', readonly=True, default=lambda self: self.env.uid,
        states={'draft': [('readonly', False)], 'submit': [('readonly', False)]})
    company_id = fields.Many2one(
        'res.company', 'Company', readonly=True, index=1,
        help='Let this field empty if this location is shared between all companies',
        states={'draft': [('readonly', False)]},
        default=lambda self: self.env['res.company']._company_default_get())
    date = fields.Date(
        'Date', readonly=True,
        states={'draft': [('readonly', False)]}, default=fields.Datetime.now)
    state = fields.Selection(
        [('draft', 'Draft'),
         ('done', 'Approved'),
         ('cancel', 'Cancelled')],
        'Status', readonly=True, copy=False, index=True, default='draft')
    attendance = fields.Selection(
        [('p', 'P'),
         ('ot', 'OT'),
         ('lwp', 'LWP')],
        'Attendance', copy=False, readonly=True, required=True,
        states={'draft': [('readonly', False)]}, index=True, default='p')

    @api.multi
    def unlink(self):
        summ_obj = self.env['attendance.summary']
        for val in self:
            if val.state == 'done':
                if self.env.uid != 1:
                    raise UserError(_(
                        "You can not delete employees attendance! Please Cancel In order to Delete"))
            for line in val.daily_attendance_line:
                if line.attendance:
                    date = line.date.split('-')
                    month = date[0] + '-' + date[1] + '-' + '01'
                    is_emp = summ_obj.search(
                        [('employee_id', '=', line.employee_id.id),
                         ('date', '=', month)])
                    if is_emp:
                        emp_att = summ_obj.browse(is_emp[0])
                        date = date[2]
                        if date[0] == '0':
                            date = date[1]
                        emp_att.write({date: ''})

        return super(daily_attendance_sheet, self).unlink()

    @api.multi
    def action_cancel(self):
        summ_obj = self.env['attendance.summary']
        for val in self:
            line_ids = []
            date_to = datetime.strptime(val.date, '%Y-%m-%d')
            date_to = date(date_to.year, date_to.month + 1, 01) - timedelta(days=1)
            self.env.cr.execute(
                "select id from hr_holidays_status where name='EL'")
            status_id = self.env.cr.fetchone()
            status_id = status_id and status_id[0]
            for line in val.daily_attendance_line:
                if line.attendance:
                    date1 = line.date.split('-')
                    month = date1[0] + '-' + date1[1] + '-' + '01'
                    is_emp = summ_obj.search(
                        [('employee_id', '=', line.employee_id.id),
                         ('date', '=', month)])
                    if is_emp:
                        emp_att = summ_obj.browse(is_emp[0])
                        date1 = date1[2]
                        if date1[0] == '0':
                            date1 = date1[1]
                        emp_att.write({'date': False})
                        self.env.cr.execute('''delete from hr_holidays where type = %s and holiday_status_id = %s
                         and employee_id = %s and date_to=%s''', (
                            'add', status_id, line.employee_id.id, date_to))
                line_ids.append(line.id)
                self.env('daily.attendance.line').write(
                    line_ids, {'state': 'draft'})

        return self.write({'state': 'cancel'})

    @api.multi
    def action_draft(self):
        return self.write({'state': 'draft'})

    @api.model
    def _create_attendance_summary(self, val):
        summ_obj = self.env['attendance.summary']
        for line in val.daily_attendance_line:

            date = line.date.split('-')
            month = date[0] + '-' + date[1] + '-' + '01'
            is_emp = summ_obj.search(
                [('employee_id', '=', line.employee_id.id),
                 ('date', '=', month)])
            if is_emp:
                emp_att = summ_obj.browse(is_emp[0])
                date = date[2]
                if date[0] == '0':
                    date = date[1]
                if line.attendance:
                    emp_att.write({date: line.attendance})
                if line.is_leave and line.holiday_status_id:
                    emp_att.write({date: line.holiday_status_id.name})
            else:
                att = ''
                date = date[2]
                if date[0] == '0':
                    date = date[1]
                if line.attendance:
                    att = line.attendance
                if line.is_leave and line.holiday_status_id:
                    att = line.holiday_status_id.name
                summ_obj.create({
                    'employee_id': line.employee_id.id,
                    'company_id': val.company_id.id,
                    # 'location_id':val.location_id.id,
                    date: att,
                    'date': month,

                })

    @api.multi
    def action_done(self):
        holiday = self.env['hr.holidays']
        leave_type = self.env['hr.holidays.status']
        p_leaves = self.env['leaves.calendar']
        work_schd = self.env['resource.calendar']
        holidays = self.env['hr.holidays']
        wf_service = netsvc.LocalService("workflow")
        daily_atten = self.env['daily.attendance.line']
        for val in self:
            att_date = datetime.strptime(val.date, '%Y-%m-%d')
            date_to = date(att_date.year, att_date.month + 1, 01)
            date_to = date_to - timedelta(days=1)
            self.env.cr.execute(
                "select id from hr_holidays_status where name='EL'")
            status_id = self.env.cr.fetchone()
            status_id = status_id and status_id[0]
            self.env.cr.execute('''select allowed_leaves from leave_limit_details where holiday_status_id = %s
                and company_id = %s''', (status_id, val.company_id.id,))
            allowed = self.env.cr.fetchone()
            all_leaves = allowed and allowed[0]
            if not all_leaves:
                raise UserError((
                    "Please defined per year allowed leaves for company %s under EL Configuration!!")
                    % (val.company_id.name))
            number_of_days_temp = all_leaves / 12
            year = val.date[:4]
            holidays_vals = {
                'type': 'add',
                'year': year,
                'company_id': val.company_id.id,
                # 'location_id': val.location_id.id,
                'name': 'EL',
                'state': 'validate',
                'holiday_type': 'employee',
                'holiday_status_id': status_id,
                'date_to': date_to,
                'number_of_days_temp': number_of_days_temp,

            }
            domain = [('holiday_status_id', '=', status_id),
                      ('date_to', '=', date_to),
                      ('company_id', '=', val.company_id.id)]
            for line in val.daily_attendance_line:

                if line.date != val.date:
                    raise UserError(_('attendance date should be same for employee %s! ') % (
                        line.employee_id.name))

                if line.attendance == 'l':
                    raise UserError(_('You can mark only attendance not leave for employee %s! ') % (
                        line.employee_id.name))
                working_day = work_schd.get_weekdays(
                    val.location_id.working_hours.id, )
                if line.attendance == 'ot':
                    h_leave_id = val.location_id.public_holiday_id
                    p_leave_id = p_leaves.search(
                        [('date', '=', line.date),
                         ('holiday_id', '=', h_leave_id.id)])
                    if att_date.weekday() in working_day:
                        if not p_leave_id:
                            raise UserError(_('You can mark OT on holidays only for employee %s! ') % (
                                line.employee_id.name))

                line.write({'state': 'done'})
                leave_id = holiday.search(
                    [('employee_id', '=', line.employee_id.id),
                     ('date_from', '<=', att_date),
                     ('date_to', '>=', att_date),
                     ('type', '=', 'remove'),
                     ('state', 'not in', ('cancel', 'refuse'))])
                if leave_id:
                    leave = holiday.browse(leave_id[0])
                    line.write(
                        {'holiday_status_id': leave.holiday_status_id.id})

                if line.attendance == 'lwp':
                    type_id = leave_type.search([('paid_unpaid', '=', False)])
                    if not type_id:
                        raise UserError(
                            _('Leave Without Pay is not configured in system. Please Configure it ! '))
                    holiday_vals = {
                        'name': 'Leave Without Pay',
                        'type': 'remove',
                        'holiday_type': 'employee',
                        'holiday_status_id': type_id[0],
                        'date_from': line.date,
                        'date_to': line.date,
                        'notes': line.remarks,
                        'number_of_days_temp': 1,
                        'employee_id': line.employee_id.id,
                        'company_id': val.company_id.id,
                        'location_id': val.location_id.id
                    }
                    holiday_id = holiday.create(holiday_vals)
                    for sig in ('confirm', 'validate', 'second_validate'):
                        holiday.signal_workflow([holiday_id], sig)
                date_from = date(att_date.year, att_date.month, 01)
                diff = (att_date.date() - date_from).days
                if diff >= 20:
                    total_p = daily_atten.search_count(
                        [('employee_id', '=', line.employee_id.id),
                         ('date', '>=', date_from),
                         ('date', '<=', att_date),
                         ('attendance', '=', 'p'),
                         ('state', '=', 'done'),
                         ('atten_type', '=', 'full')])
                    total_halfday = daily_atten.search_count(
                        [('employee_id', '=', line.employee_id.id),
                         ('date', '>=', date_from),
                         ('date', '<=', att_date),
                         ('state', '=', 'done'),
                         ('atten_type', '=', 'half')])
                    if total_halfday:
                        total_halfday /= 2.0
                    ot_total = daily_atten.search_count(
                        [('employee_id', '=', line.employee_id.id),
                         ('date', '>=', date_from),
                         ('date', '<=', att_date),
                         ('attendance', '=', 'ot'),
                         ('state', '=', 'done')])
                    total_days = total_p + total_halfday + ot_total
                    week_days = 0.0

                    for i in range(diff + 1):
                        nxt_date = date_from + timedelta(days=i)
                        if line.employee_id.leaving_date and line.employee_id.leaving_date < str(nxt_date):
                            continue
                        if line.employee_id.date_joining:
                            join_date = datetime.strptime(
                                line.employee_id.date_joining, '%Y-%m-%d').date()
                            if join_date <= nxt_date:
                                if nxt_date.weekday() not in working_day:
                                    # total_p=total_p+1
                                    week_days += 1
                        else:
                            if nxt_date.weekday() not in working_day:
                                # total_p=total_p+1
                                week_days += 1

                    self.env.cr.execute(
                        'SELECT sum(number_of_days_temp) from hr_holidays as hp LEFT JOIN hr_holidays_status as hps on (hp.holiday_status_id = hps.id) WHERE hp.employee_id=%s AND (hp.date_from >=%s AND hp.date_to<=%s) AND (hp.state = \'validate\') AND hps.paid_unpaid = True',
                        (line.employee_id.id, date_from, att_date))
                    lv_count = self.env.cr.fetchone()
                    lv_count = lv_count and lv_count[0] or 0.0
                    total_days += lv_count + week_days

                    # if line.employee_id.date_joining >= date_from and line.employee_id.date_joining <= att_date:
                    #     total = line.present + line.total_leave + line.over_time / 8
                    #     number_of_days_temp = total / 20.0
                    if total_days >= 20:
                        domain.append(
                            ('employee_id', '=', line.employee_id.id))
                        holiday_ids = holidays.search(domain)
                        if not holiday_ids:
                            holidays_vals['employee_id'] = line.employee_id.id
                            holidays_vals['name'] = 'EL for ' + \
                                line.employee_id.name
                            holiday_id = holidays.create(holidays_vals)
                            wf_service.trg_validate(
                                'hr.holidays', holiday_id, 'validate')

            template_id = self.env.ref(
                'gts_daily_attendance.email_daily_attendance_tmpl')[1]
            if template_id:
                email_to = ''
                for eml in val.message_follower_ids:
                    email_to += eml.email + ','
                if email_to:
                    self.env['email.template'].write(
                        template_id, {'email_to': email_to})
                    template_id.send_mail(self.id, force_send=True)

        self._create_attendance_summary(val)

        self.write({'state': 'done'})
        return True

    # def onchange_company_id(self, cr, uid, ids,context=None):
    #     return {
    #         'warning': {'title': 'Message!',
    #                     'message': 'Please create employee leaves before marking attendance. '
    #                                'If You already created leaves click on ok to mark attendance.'},
    #         'value': {
    #             'daily_attendance_line': []
    #         }

    #     }

    # commented

    #  @api.onchange('loc', 'attendance', 'date')
    #  def onchange_location_id(self):
    #      res= []
    #      leave_obj = self.env['hr.holidays']
    #      work_schd = self.env['resource.calendar']
    #      p_leaves = self.env['leaves.calendar']
    #      result = {'value':{'attendance_line':[]}}
    #      if not loc or not date:
    #          return {'value': result}
    #      if ids:
    #          self.env.cr.execute('DELETE FROM daily_attendance_line where attendance_id = %s'%(str(ids[0])))
    #      employee = self.env['hr.employee'].search([('client_location', '=', loc)])
    #      att_date = datetime.strptime(date, '%Y-%m-%d')
    #      loc_id = self.env['hrm.location'].browse(loc)
    #      if not loc_id.working_hours:
    #          return {
    #              'warning': {'title': 'Error!', 'message': 'Please define working schedule for %s location !'%(loc_id.name)},
    #              'value': {
    #                  'daily_attendance_line': []}}
    #      if not loc_id.public_holiday_id:
    #          return {
    #              'warning': {'title': 'Error!',
    #                          'message': 'Please define public holidays for %s location !' % (loc_id.name)},
    #              'value': {
    #                  'daily_attendance_line': []
    #              }
    #          }

    #      working_day = work_schd.get_weekdays(loc_id.working_hours.id)
    #      h_leave_id=loc_id.public_holiday_id
    #      p_leave_id = p_leaves.search([('date', '=', date), ('holiday_id', '=', h_leave_id.id)])
    #      if p_leave_id and attendance=='p':
    #          raise UserError(_(
    #              "You can mark only OT on holidays!"))
    #      if att_date.weekday() not in working_day and attendance=='p':
    #          return {
    #              'warning': {'title': 'Error!', 'message': 'You can mark P for working days only.'
    #                                                        ' If employee worked on holiday Please mark OT for that employee! !'},
    #              'value': {
    #                  'daily_attendance_line': []
    #              }
    #          }
    #      if attendance=='ot':
    #          if att_date.weekday() in working_day:
    #              if not p_leave_id:
    #                  return {
    #                      'warning': {'title': 'Error!', 'message': 'You can mark only OT on holidays !'},
    #                      'value': {
    #                          'daily_attendance_line':[]
    #                      }}
    #                  # raise osv.except_osv(_('Warning !'), _(
    #                  #     "You can mark only OT on holidays !"))

    #      for emp_id in self.env['hr.employee'].browse(uid,employee):
    #          if emp_id.state != 'left' and (emp_id.date_joining <= date or emp_id.date_joining == False):
    #              data={}
    #              leave_id = leave_obj.search(
    #                                                [('employee_id', '=', emp_id.id),
    #                                                 ('date_from', '<=', date),
    #                                                 ('date_to', '>=', date),
    #                                                 ('type', '=', 'remove'),('state', 'not in', ('cancel', 'refuse'))],
    #                                                context=context)
    #              if attendance:
    #                  data.update({'attendance':attendance,
    #                               'employee_id': emp_id.id,
    #                               'date': date,
    #                               'atten_type': 'full',
    #                              })
    #              else:
    #                  data = {
    #                     'employee_id': emp_id.id,
    #                     'attendance': 'p',
    #                      'date':date,
    #                      'atten_type':'full',
    #                    }
    #              if leave_id:
    #                  leave=leave_obj.browse(leave_id[0])
    #                  data.update({'attendance': False,
    #                               'is_leave':True,
    #                               'remarks':'Leave',
    #                               'atten_type': 'full',
    #                               })

    #              res+= [data]
    #      result['value'].update({
    #                    'daily_attendance_line': res ,
        # })
    #      return result

    # commented


class daily_attendance_line(models.Model):
    _name = 'daily.attendance.line'
    _description = "Attendance Sheet"

    attendance_id = fields.Many2one(
        'daily.attendance.sheet', 'Attendance Reference', ondelete='cascade')
    date = fields.Date('Date',)
    employee_id = fields.Many2one('hr.employee', 'Employee', required=True)
    attendance = fields.Selection([
        ('p', 'P'),
        ('ot', 'OT'),
        ('lwp', 'LWP'),

    ], 'Attendance', copy=False, index=True)
    holiday_status_id = fields.Many2one("hr.holidays.status", "Leave Type",)
    remarks = fields.Char('Remarks', size=250)
    state = fields.Selection(
        [('draft', 'Draft'),
         ('done', 'Done')],
        'Status', readonly=True, copy=False, index=True, default='draft')
    is_leave = fields.Boolean('Is Leave')
    atten_type = fields.Selection(
        [('full', 'Full Day'),
         ('half', 'Half Day')], 'Full Day/Half Day', default='full')

    _sql_constraints = [
        ('duplicate_attendance', 'unique(date,employee_id)',
         'You can not create duplicate attendance for employees'),
    ]

    @api.onchange('attendance')
    def onchange_attendance(self):
        result = {'value': {
            'holiday_status_id': False}}
        return result

    @api.multi
    def unlink(self):
        summ_obj = self.env('attendance.summary')
        for val in self:
            if val.state == 'done':
                raise UserError(_(
                    "You can not delete employees attendance!"))
            if val.attendance:
                date = val.date.split('-')
                month = date[0] + '-' + date[1] + '-' + '01'
                is_emp = summ_obj.search(
                    [('employee_id', '=', val.employee_id.id),
                     ('date', '=', month)])
                if is_emp:
                    emp_att = summ_obj.browse(is_emp[0])
                    date = date[2]
                    if date[0] == '0':
                        date = date[1]
                    emp_att.write({date: ''})
        return super(daily_attendance_line, self).unlink()
