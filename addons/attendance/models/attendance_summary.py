from odoo import api, fields, models, tools, _
from datetime import datetime
import calendar


class attendance_summary(models.Model):
    _name = 'attendance.summary'
    _order = 'date_joined'

    @api.multi
    def _get_date(self):
        res = {}
        for val in self:
            self.env.cr.execute(
                "select date_joining from hr_employee where id=%s",
                (val.employee_id.id))
            result = self.env.cr.fetchone()

            if result and result[0]:
                res[val.id] = result[0]

        return res

    location_id = fields.Char('Location')
    company_id = fields.Many2one(
        'res.company', 'Company', readonly=True, index=1,
        help='Let this field empty if this location is shared between all companies',
        states={'draft': [('readonly', False)]})
    date = fields.Date('Month')
    employee_id = fields.Many2one('hr.employee', 'Employee')
    date_joined = fields.Date(
        compute='_get_date', string='Join date', store=True)
    one = fields.Char('1')
    two = fields.Char('2')
    three = fields.Char('3')
    four = fields.Char('4')
    five = fields.Char('5')
    six = fields.Char('6')
    seven = fields.Char('7')
    eight = fields.Char('8')
    nine = fields.Char('9')
    ten = fields.Char('10')
    eleven = fields.Char('11')
    twelve = fields.Char('12')
    thirteen = fields.Char('13')
    fourteen = fields.Char('14')
    fifteen = fields.Char('15')
    sixteen = fields.Char('16')
    seventeen = fields.Char('17')
    eighteen = fields.Char('18')
    ninteen = fields.Char('19')
    twenty = fields.Char('20')
    twentyone = fields.Char('21')
    twentytwo = fields.Char('22')
    twentythree = fields.Char('23')
    twentyfour = fields.Char('24')
    twentyfive = fields.Char('25')
    twentysix = fields.Char('26')
    twentyseven = fields.Char('27')
    twentyeight = fields.Char('28')
    twentynine = fields.Char('29')
    thirty = fields.Char('30')
    thirtyone = fields.Char('31')

    _sql_constraints = [
        ('duplicate_entry', 'unique(date,employee_id)',
         'You can not create duplicate attendance entry for employees'),
    ]