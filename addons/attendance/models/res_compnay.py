# -*- coding: utf-8 -*-
#/#############################################################################
#
#    SunInfo Tech
#
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#/#############################################################################

from odoo import api, fields, models, tools, SUPERUSER_ID, _
import sys


class ResCompany(models.Model):
    _inherit = "res.company"
    _description = 'Companies'
    
    category_ids = fields.Many2many('hr.employee.category', 'company_category_rel', 'company_id', 'category_id', 'Tags')
    # company_code = fields.Char('Company Code', required=True)
    # round_off_leave = fields.Boolean('RoundOff Leave')

    @api.onchange('category_ids')
    def onchang_tags(self):
        dic = {}
        employee = self.env['hr.employee']
        for data in self:
            employee_ids = employee.search([('company_id', '=', data.id)])
            for emp in employee_ids:
                employee.write([emp], {'category_ids': category_ids })
        return dic

    @api.multi
    def assign_leave_tags(self):
        employee = self.env['hr.employee']
        for data in self:
            if data.category_ids:
                categ_ids = [cat.id for cat in data.category_ids]
                employee_ids = employee.search([('company_id', '=', data.id)])
                for emp in employee_ids:
                    employee.write([emp], {'category_ids': [(6, 0, categ_ids)]})
        return True


class HrEmployee(models.Model):
    _inherit = "hr.employee"
    _description = "Employee"

    @api.onchange('address')
    def onchange_address_id(self):
        if address:
            address = self.env['res.partner'].browse(address)
            return {'value': {'mobile_phone': address.mobile}}
        return {'value': {}}

    @api.onchange('company')
    def onchange_company(self):
        address_id = False
	list1 = []
        if company:
            company_id = self.env['res.company'].browse(company)
            address = self.env['res.partner'].address_get([company_id.partner_id.id], ['default'])
            address_id = address and address['default'] or False
	    category = company_id.category_ids
	    for data in  category:
		list1.append(data.id)
	return {'value': {'address_id': address_id,'category_ids':list1}}		