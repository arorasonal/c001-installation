from odoo import api, fields, models, tools, _
import datetime

class leaves_calendar(models.Model):
    _name = "leaves.calendar"
    _description = "Public Holidays"

    name = fields.Char('Holiday Name', size=124, required=True)
    date = fields.Date('Date', size=124,required=True)
    day = fields.Selection([('0','Monday'),('1','Tuesday'),('2','Wednesday'),('3','Thursday'),('4','Friday'),
        ('5','Saturday'),('6','Sunday')], 'Day of Week',required=True)
    description = fields.Text('Description', size=124)
    holiday_id = fields.Many2one('holidays.calendar', 'Holidays ID')

    @api.multi
    def onchange_date(self, date_from):
        if date_from:
            from_dt = datetime.datetime.strptime(date_from, "%Y-%m-%d")
            a=from_dt.weekday()
            return {'value' : {'day':str(a)}}
        return {'value' : {'day':False}}

class holidays_calendar(models.Model):
    _name = "holidays.calendar"
    _description = "Holidays Calendar"

    name = fields.Char('Holidays Calendar Name', size=124, required=True)
    holidays_line = fields.One2many('leaves.calendar','holiday_id',"Holidays Number ")
