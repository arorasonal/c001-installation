# -*- encoding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2014  Merlin Tecsol Pvt Ltd(<http://www.merlintecsol.com>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
{
    "name": "Attendance for Indian Payroll",
    "version": "10.0.0.1",
    "author": "Apagen Solutions Pvt. Ltd.",
    "website": "www apagen.com",
    "license": "AGPL-3",
    "category": "Human Resources",
    "complexity": "normal",
    "description": """\
         This module allows you to put attendace on daily basis and mult-company wise
    """,
    "depends": ['hr',
                'base',
                'hr_attendance',
                'hr_holidays',
                'hr_payroll',
                'indian_payroll',
                'hr_leaves',
                ],
    "init_xml": [],
    "data": [

        'security/ir.model.access.csv',
        # 'wizard/leave.xml',
        'views/hr_attendance_sheet_view.xml',
        'views/hr_attendance_sheet_in_view.xml',
        'views/daily_attendance_sheet_view.xml',
        'views/public_holidays_view.xml',
        'views/attendance_summary_view.xml',
        # 'views/daily_attendance_template.xml',
        # 'views/res_company.xml',
    ],
    "demo_xml": [],
    "installable": True,
    "certificate": ''
}
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
