import itertools
from lxml import etree
import json
from odoo import models, fields, api, _
from odoo.exceptions import except_orm, Warning, RedirectWarning
from odoo.tools import float_compare
import odoo.addons.decimal_precision as dp


class account_invoice(models.Model):
    _inherit = "account.invoice"
    
    @api.multi
    def fetch_date(self):
        account_line = [ ]
        account_line_obj = ' '
        for invoice in self:
           account_line  = self.env['account.invoice.line'].search([('invoice_id','=',invoice.id)])
           for val in account_line:
               account_line_obj = self.env['account.invoice.line'].browse(val.id)
               account_line_obj.write({'start_date':self.bill_period_from,
                                      'end_date':self.bill_period_to
                                      })
        return True   

    @api.multi
    def fetch_gst(self):
        for invoice in self:
           invoice.write({'partner_gst':self.partner_id.gstin,
                          'delivery_gst':self.partner_shipping_id.gstin,
                          'invoice_gst':self.partner_invoice_id.gstin
                                  })
        return True     