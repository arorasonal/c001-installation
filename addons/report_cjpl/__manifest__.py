{
    "name": "REPORT CJPL",
    "version": "10.0.0.1.0",
    'website': 'http://www.diracerp.in',
    'images': [ ],
    "depends": ['sale','account'],
    "author": "Dirac ERP",
    "category": "REPORT",
    'sequence': 16,
    "description": """
    This module will provide the detail of sample and convert it into a product and some report regarding sample  
    """,
    'data': [
     'report/credit_note_report_view.xml',
     'view/credit_note_report_template.xml',
     'report/bill_of_supply_report_view.xml',
     'view/bill_of_supply_report_template.xml',
     'view/account_invoice_view.xml',
                    ],
    'demo_xml': [],
     'js': [
           'static/src/js/view_list.js'
#     'static/src/js/subst.js'
    ],
    'css': ['static/src/css/sample_kanban.css'],
    'installable': True,
    'active': False,
    'auto_install': False,
#    'certificate': 'certificate',
}