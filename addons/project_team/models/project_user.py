# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from openerp import models, fields, api
import logging
_logger = logging.getLogger(__name__)

class CrmTeamInherit(models.Model):

    _inherit = 'crm.team'

    parent_id = fields.Many2one('crm.team', string="Parent Team",
                              domain=[('type_team', '=', 'project')])

class ResUsers(models.Model):

    _inherit = 'res.users'

    @api.multi
    def get_heirarchy_team(self, user):
        print('projext+++++++++++++++++++')
        team_obj = self.env['crm.team']
        lst = []
        domain  = [('team_id', 'in', lst)]
        print("@@@@@@@@@@@@@@!!!!!!!!!!====",self._context)
        if 'create_task_from_so' not in self._context:
            current_user = user
            if user.id != 1:
                team_manager = team_obj.search([('user_id', '=', current_user.id), ('type_team', '=', 'project')])
                if team_manager:
                    team_managers = ','.join(str(user) for user in team_manager.ids)
                    #parent_id = team_manager.parent_id
                    query1 = """WITH RECURSIVE children AS (
                                 SELECT id,parent_id, name, 1 as depth
                                 FROM crm_team
                                 WHERE id in (%s) and active='t'
                                UNION
                                 SELECT op.id,op.parent_id, op.name, depth + 1
                                 FROM crm_team op
                                 JOIN children c ON op.parent_id = c.id
                                )
                                SELECT *
                                FROM children"""%(team_managers)
                    self._cr.execute(query1)
                    recursive_data =  self._cr.dictfetchall()
                    if recursive_data:
                        for ls in recursive_data:
                            lst.append(ls.get('id'))
                        task_log = self.env['task.user.log'].sudo().search([('user_id', '=', user.id)])
                        print('taskkkkkkkkkkkk',task_log)
                        team_lst = list(set([i.team_id.id for i in task_log if task_log]))
                        # lst.append(task_log.team_id.id)
                        print('team_lst__++++++++++++', team_lst)
                        domain = [('team_id', 'in', lst+team_lst)]
                        # logger = logging.getLogger(name_)
                        _logger.info("Exceptio======:%s"%(domain))
                else:
                    query = """SELECT project_id FROM project_task  where user_id=%s"""%(current_user.id)
                    self._cr.execute(query)
                    data =  self._cr.dictfetchall()
                    print("####################", data)
                    if data:
                        for ls in data:
                            lst.append(ls.get('project_id'))
                        domain = [('id', 'in', lst)]
                    else:
                        domain += [('id', 'in', [])]
        if 'create_task_from_so' in self._context:
            domain = [('allow_tasks_from_so', '=', True)]
        return domain

    @api.multi
    def get_heirarchy_task_team(self, user):
        _logger.info("call task rule")
        team_obj = self.env['crm.team']
        lst = []
        domain  = [('id', 'in', lst)]
        current_user = user
        if user.id != 1:
            team_manager = team_obj.search([('user_id', '=', current_user.id), ('type_team', '=', 'project')])
            if team_manager:
                team_managers = ','.join(str(user) for user in team_manager.ids)
                # parent_id = team_manager.parent_id
                query1 = """WITH RECURSIVE children AS (
                             SELECT id,parent_id, name, 1 as depth
                             FROM crm_team
                             WHERE id in (%s) and active='t'
                            UNION
                             SELECT op.id,op.parent_id, op.name, depth + 1
                             FROM crm_team op
                             JOIN children c ON op.parent_id = c.id
                            )
                            SELECT *
                            FROM children"""%(team_managers)
                self._cr.execute(query1)
                recursive_data =  self._cr.dictfetchall()
                if recursive_data:
                    for ls in recursive_data:
                        crm_team = team_obj.search([('id', '=', ls.get('id'))])
                        lst.append(crm_team.user_id.id)
                        for l in crm_team.team_members:
                            lst.append(l.id)
                    total_team_ids = (', '.join(str(user) for user in lst))
                    pro_query = """SELECT pt.id FROM  project_task as pt left join project_project as pp on pt.project_id = pp.id where pt.user_id in (%s) and pp.team_id is not null """%(total_team_ids)
                    self._cr.execute(pro_query)
                    all_task = self._cr.dictfetchall()
                    all_user_task = list(set([rec.get('id') for rec in all_task if rec.get('id') not in ['None', False, 'False', None]]))
                    domain = [('id', 'in', all_user_task)]
            else:
                _logger.info("call task rule")
                query = """SELECT id FROM project_task where user_id=%s"""%(current_user.id)
                self._cr.execute(query)
                data =  self._cr.dictfetchall()
                if data:
                    for ls in data:
                        lst.append(ls.get('id'))
                    domain = [('id', 'in', lst)]
                else:
                    domain = [('id', 'in', [])]
        return domain

class TaskUserLog(models.Model):

    _name = 'task.user.log'


    project_id = fields.Many2one('project.project', string="Project")
    task_id = fields.Many2one('project.task', string="Task")
    team_id = fields.Many2one('project.team',string="project team")
    user_id = fields.Many2one('res.users', string="User")
    

