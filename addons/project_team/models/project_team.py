# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from openerp import models, fields, api
from datetime import datetime
from odoo.addons.base.res.res_partner import FormatAddress
import logging
_logger = logging.getLogger(__name__)


class CrmTeamInherit(models.Model):

    _inherit = 'crm.team'

    type_team = fields.Selection([('sale', 'Sale'), ('project', 'Project')],
                                 string="Type", default="sale")

    team_members = fields.Many2many('res.users', 'project_team_user_rel',
                                    'team_id', 'uid', 'Project Members',
                                    help="""Project's members are users who
                                     can have an access to the tasks related
                                     to this project.""")
class Task(models.Model):

    _inherit = 'project.task'



    @api.model
    def create(self, vals):
        task = super(Task,self).create(vals)
        project_id = self.env['project.project'].search([('id','=',vals['project_id'])])
        self.env['task.user.log'].create({
          'project_id':vals.get('project_id'),
          'task_id':task.id,
          'user_id':task.user_id.id,
          'team_id':project_id.team_id.id,
          })
        return task

    @api.multi
    def write(self, vals):
        task_id = self.env['task.user.log'].search([('task_id','=',self.id)])
        if vals.get('user_id'):
          task_id.write({
            'user_id':vals.get('user_id')
            })
        result = super(Task, self).write(vals)
        return result


class ProjectProject(models.Model):

    _inherit = 'project.project'

    members = fields.Many2many('res.users', 'project_user_rel', 'project_id',
                               'uid', 'Project Members', help="""Project's
                               members are users who can have an access to
                               the tasks related to this project.""",)
                               # states={'close': [('readonly', True)],
                               #         'cancelled': [('readonly', True)]}
    team_id = fields.Many2one('crm.team', string="Project Team",
                              domain=[('type_team', '=', 'project')])
    allow_tasks_from_so = fields.Boolean(string='Allow Tasks From SO')

    @api.multi
    def get_heirarchy_project_team(self):
        team_obj = self.env['crm.team']
        project_obj = self.env['project.project']
        lst = []
        domain  = [('team_id', 'in', lst)]
        # print("@@@@@@@@@@@@@@!!!!!!!!!!====",domain)
        # if 'create_task_from_so' not in self._context:
        current_user = self.env.user
        project_mgr = self.env.user.has_group('project.group_project_manager')
        if current_user.id == 1:
          print('current_user+++++++++++++++++++', project_mgr)
          res =  [('id','in', project_obj.search([]).ids)]
          print('res', res)
          return res
        if current_user.id != 1 or project_mgr == False:
            team_manager = team_obj.search([('user_id', '=', current_user.id), ('type_team', '=', 'project')])
            if team_manager:
                team_managers = ','.join(str(user) for user in team_manager.ids)
                #parent_id = team_manager.parent_id
                query1 = """WITH RECURSIVE children AS (
                             SELECT id,parent_id, name, 1 as depth
                             FROM crm_team
                             WHERE id in (%s) and active='t'
                            UNION
                             SELECT op.id,op.parent_id, op.name, depth + 1
                             FROM crm_team op
                             JOIN children c ON op.parent_id = c.id
                            )
                            SELECT *
                            FROM children"""%(team_managers)
                self._cr.execute(query1)
                recursive_data =  self._cr.dictfetchall()
                if recursive_data:
                    for ls in recursive_data:
                        lst.append(ls.get('id'))
                    # task_log = self.env['task.user.log'].sudo().search([('user_id', '=', user.id)])
                    # print('taskkkkkkkkkkkk',task_log)
                    # # team_lst = [i.team_id for i in task_log if task_log]
                    # lst.append(task_log.team_id.id)
                    # print('team_lst__++++++++++++', lst)
                    domain = [('team_id', 'in', lst)]
            else:
                query = """SELECT project_id FROM project_task  where user_id=%s"""%(current_user.id)
                self._cr.execute(query)
                data =  self._cr.dictfetchall()
                # print("####################", data)
                if data:
                    for ls in data:
                        lst.append(ls.get('project_id'))
                    domain = [('id', 'in', lst)]
                else:
                    domain += [('id', 'in', [])]
        # if 'create_task_from_so' in self._context:
        #     domain = [('allow_tasks_from_so', '=', True)]
        return domain

    @api.onchange('team_id')
    def get_team_members(self):
        self.members = [(6, 0, [rec.id for rec in self.team_id.team_members])]

    def custom_funct_date(self):
        domain = self.get_heirarchy_project_team()
        # print('domain++++++++++++++', domain)
        _logger.info("domain")
        # print "make sure that this action is called from th server action "
        # compute you date
        # my_date = ... (some stuff)
        # tree_id = self.env.ref("modul_name.view_tree_id")
        # form_id = self.env.ref("modul_name.view_form_id")
        return {
          'type': 'ir.actions.act_window',
          'name': 'Dashboard',
          'view_type': 'form',
          'view_mode': 'kanban,form',
          'res_model': 'project.project',
          'domain': domain,
          # if you don't want to specify form for example
          # (False, 'form') just pass False 
          'views': [],
          'target': 'current',
          'context': [],
        }


class ResUsers(models.Model):

    _inherit = 'res.users'

    project_team =fields.Many2one('crm.team')
    subordinate_ids =  fields.Many2many(
        'crm.team', 'user_crm_team_rel', 'res_user_rel', 'sales_team_rel', 'Teams Under')

class Lead(FormatAddress, models.Model):

    _inherit = "crm.lead"

    team_id = fields.Many2one('crm.team', string='Sales Team', oldname='section_id', default=lambda self: self.env['crm.team'].sudo()._get_default_team_id(user_id=self.env.uid),
        index=True, track_visibility='onchange', help='When sending mails, the default email address is taken from the sales team.',
        domain=[('type_team', '=', 'sale')])

