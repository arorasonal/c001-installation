# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2015 Devintelle Software Solutions (<http://devintelle.com>).
#
##############################################################################
{
    'name': 'Employee Loan',
    'version': '1.0',
    'category': 'Human Resources',
    'summary': 'Hr Loan functioality for employee',
    'description': """
        Manage  bonus Functioality for hr employee
    """,
    'author': 'Apagen Solutions Pvt. Ltd.',
    'website': 'http://www.apagen.com',
    'images': ['images/main_screenshot.png'],
    'depends': [
        # 'base_RAG',
        'hr_payroll',
        'hr_employee_register'
        # 'hr_holiday_RAG',
    ],
    'data': [
        "security/ir.model.access.csv",
        "security/ir_rule.xml",
        "views/hr_loan_view.xml",
        "views/hr_loan_menu_view.xml",
        # "wizard/loan_exception_confirm_view.xml",
        "views/hr_loan_sequence.xml",
        "views/hr_loan_data.xml",
    ],
    'demo': [],
    'test': [],
    'installable': True,
    'auto_install': False,
    'application': True,
    'price': 35.0,
    'currency': 'EUR',
}

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
