# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2015 Devintelle Software Solutions (<http://devintelle.com>).
#
##############################################################################

from datetime import datetime, timedelta, date
from odoo import models, fields, api, _
from dateutil.relativedelta import relativedelta
from odoo.exceptions import UserError, Warning
import odoo.addons.decimal_precision as dp
import time


@api.multi
def employee_get(self):
    ids = self.env['hr.employee'].search(
        [('user_id.id', '=', self.env.uid)])
    if ids:
        return ids[0]
    return False


class HrLoan(models.Model):
    _name = 'hr.loan'
    _description = "Employee Loan Request Form"
    _inherit = 'mail.thread'
    _order = 'id DESC'
    _rec_name = 'reference'

    @api.multi
    @api.depends('installment_lines', 'amortize_lines')
    def _calculate_amounts(self):
        res = {}
        paid_amount = 0.0
        for loan in self:
            if loan.installment_lines:
                for installment in loan.installment_lines:
                    paid_amount += installment.amount
            loan.amount_paid = paid_amount
            amount_with_interest = (
                loan.loan_amount + (((loan.loan_amount * loan.interest_rate) / 100) * (loan.duration / 12.0)))
            # loan.amount_to_pay = amount_with_interest - paid_amount
            if loan.amortize_lines:
                loan.amount_to_pay = sum(
                    line.schedule_pay for line in loan.amortize_lines if line.schedule_pay)
            if((loan.amount_to_pay <= 0) and (loan.state == 'loan_running')):
                loan.state = 'closed'
        return True

    reference = fields.Char('Reference #')
    request_date = fields.Datetime(
        'Loan Request Date'
        )
    start_date = fields.Date(
        'Loan Payment Start Date',
        default=time.strftime("%Y-%m-%d"))
    due_date = fields.Date('Loan Payment End Date')
    loan_amount = fields.Float(
        'Loan Amount',
        digits=dp.get_precision('Account'),
        required=True)
    duration = fields.Integer('Payment Duration (Months) ')
    loan_interest = fields.Float(
        'Loan Interest',
        digits=dp.get_precision('Account'))
    deduction_amount = fields.Float(
        'EMI',
        digits=dp.get_precision('Account'))
    employee_id = fields.Many2one(
        'hr.employee', 'Employee', required=True, default=employee_get)
    loan_type = fields.Selection(
        [('salary', 'Loan Against Salary'),
         ('service', 'Loan Against Service')],
        string="Loan Type", required=True, default='salary')
    description = fields.Text('Purpose For Loan')
    job_id = fields.Many2one(
        'hr.job',
        string='Designation',
        store=True)
    department_id = fields.Many2one(
        'hr.department',
        string="Department",
        store=True)
    sub_department_id = fields.Many2one('hr.sub.department', 'Sub Department')
    state = fields.Selection(
        [('draft', 'Draft'),
         ('hod_approval', 'Awaiting HOD Approval'),
         ('hr_approval', 'Awaiting HR Approval'),
         ('ceo_approval', 'Awaiting CEO/MD Approval'),
         ('confirm', 'Approved'),
         ('refuse', 'Refused'),
         ('cancel', 'Cancelled'),
         ('running', 'Loan Running'),
         ('close', 'Closed')],
        string="Status", default='draft', required=True, track_visibility='onchange')
    emi_based_on = fields.Selection(
        [('duration', 'By Duration'),
         # ('amount', 'By Amount')
         ],
        string='EMI based on', required=True, default='duration')
    percentage_of_gross = fields.Float(
        'Percentage of Gross',
        digits=dp.get_precision('Account'))
    installment_lines = fields.One2many(
        'installment.line',
        'loan_id',
        'Installments')
    amount_paid = fields.Float(
        compute='_calculate_amounts',
        string='Amount Paid',
        digits=dp.get_precision('Account'),
        readonly=True)
    amount_to_pay = fields.Float(
        compute='_calculate_amounts',
        string='Amount to Pay',
        digits=dp.get_precision('Account'),
        readonly=True)
    interest_rate = fields.Float("Interest Rate")
    cancellation = fields.Text('Cancelled Summary')
    amortize_lines = fields.One2many('amortization.loan', 'loan_amortize')
    previous_loan_ids = fields.One2many(
        'previous.loan',
        'loans_id',
        'Previous Loan')
    current_user = fields.Boolean(
        copy=False, compute="check_current_user", default=False)
    current_hod = fields.Boolean(
        copy=False, compute="check_current_hod", default=False)
    date_of_joining = fields.Date('Date of Joining')
    year = fields.Integer(string="Year", invisible=True)
    gross_contract_id = fields.Many2one('hr.contract', string="Contract")
    employee_gross = fields.Float(string="Gross Salary")
    ignore_exception = fields.Boolean()

    @api.multi
    def check_current_user(self):
        user = self.env.uid
        for record in self:
            if self.env.user.has_group('hr_employee_register.group_hr_department_head'):
                record.current_user = True
            elif record.department_id.manager_id.user_id.id == user:
                record.current_user = True
            else:
                record.current_user = False

    @api.multi
    def check_current_hod(self):
        user = self.env.uid
        for record in self:
            if record.department_id.manager_id.user_id.id == user:
                record.current_hod = True

    @api.model
    def create(self, vals):
        res = super(HrLoan, self).create(vals)
        val = []
        due_date = (datetime.strptime(res.start_date, '%Y-%m-%d') +
                        timedelta(res.duration * 365 / 12)).strftime('%Y-%m-%d')
        employee = res.employee_id
        contract_obj = self.env['hr.contract'].search(
            [('employee_id', '=', employee.id)])
        prev_loan = []
        prev_employee = self.env['hr.loan'].search(
            [('employee_id', '=', employee.id or False),
            ('state', '=', 'close')])
        cont_employee = self.env['hr.contract'].search(
            [('employee_id', '=', employee.id or False)])
        if prev_employee:
            for val in prev_employee:
                prev_loan.append({
                    'reference': val.reference,
                    'department_id': val.employee_id.department_id,
                    'loan_amount': val.loan_amount,
                    'amount_paid': val.amount_paid,
                    'amount_to_pay': val.amount_to_pay,
                    'state': val.state,
                })
        res.previous_loan_ids = prev_loan
        res.department_id = employee.department_id.id
        res.due_date = due_date
        res.date_of_joining = employee.date_of_joining
        res.job_id = employee.job_id.id
        res.gross_contract_id = contract_obj.id
        res.employee_gross = contract_obj.gross_sal
        res.sub_department_id = employee.sub_department.id
        return res

    @api.multi
    def write(self, vals):
        due_date = False
        employee = self.env['hr.employee'].search(
            [('id', '=', vals.get('employee_id'))])
        contract_obj = self.env['hr.contract'].search(
            [('employee_id', '=', vals.get('employee_id'))])
        due_date = (datetime.strptime(self.start_date, '%Y-%m-%d') +
                        timedelta(self.duration * 365 / 12)).strftime('%Y-%m-%d')
        if 'employee_id' in vals:
            vals.update({
                'department_id': employee.department_id.id,
                'date_of_joining': employee.date_of_joining,
                'job_id': employee.job_id.id,
                'gross_contract_id': contract_obj.id,
                'employee_gross': contract_obj.gross_sal,
                'sub_department_id': employee.sub_department.id,
                'due_date': due_date
            })
        return super(HrLoan, self).write(vals)

    @api.multi
    @api.onchange('employee_id')
    def onchange_employee(self):
        if self.employee_id:
            employee = self.env['hr.employee'].browse(self.employee_id).id
            prev_loan = []
            prev_employee = self.env['hr.loan'].search(
                [('employee_id', '=', employee.id or False),
                ('state', '=', 'close')
                ])
            cont_employee = self.env['hr.contract'].search(
                [('employee_id', '=', employee.id or False)])
            if prev_employee:
                for val in prev_employee:
                    prev_loan.append({
                        'reference': val.reference,
                        'department_id': val.employee_id.department_id,
                        'loan_amount': val.loan_amount,
                        'amount_paid': val.amount_paid,
                        'amount_to_pay': val.amount_to_pay,
                        'state': val.state,
                    })
            self.previous_loan_ids = prev_loan
            self.gross_contract_id = cont_employee.id
            self.department_id = self.employee_id.department_id
            self.sub_department_id = self.employee_id.sub_department
            self.job_id = self.employee_id.job_id
            self.date_of_joining = self.employee_id.date_of_joining
            self.employee_gross = self.gross_contract_id.gross_sal

    @api.multi
    def make_calculation(self):
        for loan in self:
            if not loan.loan_amount or loan.loan_amount < 0:
                raise Warning(
                    _('Loan Amount should be greater than zero. Kindly change the loan amount.'))
            if loan.emi_based_on == 'duration':
                if not loan.duration or loan.duration < 0:
                    raise Warning(
                        _('Payment Duration should be greater than zero.'))
                if loan.interest_rate <= 0:
                    installment = loan.loan_amount / loan.duration
                else:
                    r = (loan.interest_rate / 12) / 100
                    installment = loan.loan_amount * r * \
                        pow((1 + r), loan.duration) / \
                        (pow((1 + r), loan.duration) - 1)
                    # amount_with_interest = (loan.loan_amount+(((loan.loan_amount*loan.interest_rate)/100)*(loan.duration/12.0)))
                    # installment = (amount_with_interest/loan.duration)
                    # amount_pay = amount_with_interest - loan.amount_paid
                if loan.duration > 0:
                    end_date = (datetime.strptime(loan.start_date, '%Y-%m-%d') +
                                timedelta(loan.duration * 365 / 12)).strftime('%Y-%m-%d')
                    loan.due_date = end_date
                self.write({'deduction_amount': installment})
                loan.compute_loan_line()
            elif loan.emi_based_on == 'amount':
                if not loan.deduction_amount or loan.deduction_amount < 0:
                    raise Warning(
                        _('Please enter proper value for Deduction Amount'))
                count = 0
                r = (loan.interest_rate / 12) / 100
                loan_amount = loan.loan_amount
                while loan_amount > 0:
                    rate = loan_amount * r
                    loan_amount = loan_amount - (loan.deduction_amount - rate)
                    count += 1
                end_date = (datetime.strptime(loan.start_date, '%Y-%m-%d') +
                            timedelta(count * 365 / 12)).strftime('%Y-%m-%d')
                self.write({'duration': count, 'due_date': end_date})
        return True

    @api.multi
    def make_calculation_hr(self):
        for loan in self:
            if not loan.loan_amount or loan.loan_amount < 0:
                raise Warning(
                    _('Loan Amount should be greater than zero. Kindly change the loan amount.'))
            if loan.emi_based_on == 'duration':
                if not loan.duration or loan.duration < 0:
                    raise Warning(
                        _('Payment Duration should be greater than zero.'))
                if loan.interest_rate <= 0:
                    installment = loan.loan_amount / loan.duration
                else:
                    r = (loan.interest_rate / 12) / 100
                    installment = loan.loan_amount * r * \
                        pow((1 + r), loan.duration) / \
                        (pow((1 + r), loan.duration) - 1)
                    # amount_with_interest = (loan.loan_amount+(((loan.loan_amount*loan.interest_rate)/100)*(loan.duration/12.0)))
                    # installment = (amount_with_interest/loan.duration)
                    # amount_pay = amount_with_interest - loan.amount_paid
                self.write({'deduction_amount': installment})
                loan.compute_loan_line()
            elif loan.emi_based_on == 'amount':
                if not loan.deduction_amount or loan.deduction_amount < 0:
                    raise Warning(
                        _('Please enter proper value for Deduction Amount'))
                count = 0
                r = (loan.interest_rate / 12) / 100
                loan_amount = loan.loan_amount
                while loan_amount > 0:
                    rate = loan_amount * r
                    loan_amount = loan_amount - (loan.deduction_amount - rate)
                    count += 1
                end_date = (datetime.strptime(loan.start_date, '%Y-%m-%d') +
                            timedelta(count * 365 / 12)).strftime('%Y-%m-%d')
                self.write({'duration': count, 'due_date': end_date})
        return True

    @api.multi
    def make_calculation_hod(self):
        for loan in self:
            if not loan.loan_amount or loan.loan_amount < 0:
                raise Warning(
                    _('Loan Amount should be greater than zero. Kindly change the loan amount.'))
            if loan.emi_based_on == 'duration':
                if not loan.duration or loan.duration < 0:
                    raise Warning(
                        _('Payment Duration should be greater than zero.'))
                if loan.interest_rate <= 0:
                    installment = loan.loan_amount / loan.duration
                else:
                    r = (loan.interest_rate / 12) / 100
                    installment = loan.loan_amount * r * \
                        pow((1 + r), loan.duration) / \
                        (pow((1 + r), loan.duration) - 1)
                    # amount_with_interest = (loan.loan_amount+(((loan.loan_amount*loan.interest_rate)/100)*(loan.duration/12.0)))
                    # installment = (amount_with_interest/loan.duration)
                    # amount_pay = amount_with_interest - loan.amount_paid
                self.write({'deduction_amount': installment})
                loan.compute_loan_line()
            elif loan.emi_based_on == 'amount':
                if not loan.deduction_amount or loan.deduction_amount < 0:
                    raise Warning(
                        _('Please enter proper value for Deduction Amount'))
                count = 0
                r = (loan.interest_rate / 12) / 100
                loan_amount = loan.loan_amount
                while loan_amount > 0:
                    rate = loan_amount * r
                    loan_amount = loan_amount - (loan.deduction_amount - rate)
                    count += 1
                end_date = (datetime.strptime(loan.start_date, '%Y-%m-%d') +
                            timedelta(count * 365 / 12)).strftime('%Y-%m-%d')
                self.write({'duration': count, 'due_date': end_date})
        return True

    def compute_loan_line(self):
        loan_line = self.env['amortization.loan']
        line_id = loan_line.search([('loan_amortize', '=', self.id)])
        line_id.unlink()
        for loan in self:
            date_start_str = datetime.strptime(loan.start_date, '%Y-%m-%d')
            counter = 1
            interest_total = loan.loan_amount * \
                ((loan.interest_rate / 12) / 100)
            amount_per_time = loan.loan_amount / loan.duration
            initial_bal = loan.loan_amount
            amounts_interest = loan.loan_amount - loan.deduction_amount
            principal = loan.deduction_amount - interest_total
            ending_bal = loan.loan_amount - principal
            for i in range(0, loan.duration):
                line_ids = loan_line.create({
                    'payment_date': date_start_str,
                    'schedule_pay': loan.deduction_amount,
                    'principal': principal,
                    'employee_id': loan.employee_id.id,
                    'total_payment': loan.loan_amount,
                    'interest': interest_total,
                    'bigining_bal': initial_bal,
                    'ending_bal': ending_bal,
                    'loan_amortize': loan.id})
                counter += 1
                initial_bal = line_ids.ending_bal
                interest_total = initial_bal * \
                    ((loan.interest_rate / 12) / 100)
                date_start_str = date_start_str + relativedelta(months=1)
                principal = loan.deduction_amount - interest_total
                ending_bal = initial_bal - principal
                if ending_bal < 1:
                    ending_bal = 0.00

    @api.multi
    @api.onchange('start_date', 'emi_based_on', 'duration')
    def onchange_start_date(self):
        res = {'value': {}}
        if self.emi_based_on != 'percentage':
            if not self.start_date:
                raise Warning(_('Please Enter Payment Start Date.'))
            due_date = (datetime.strptime(self.start_date, '%Y-%m-%d') +
                        timedelta(self.duration * 365 / 12)).strftime('%Y-%m-%d')
            res['value'].update({'due_date': due_date})
        return res

    @api.multi
    def confirm_request(self):
        self.make_calculation()
        current_date = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        if not self.reference:
            seq = self.env['ir.sequence'].get('hr.loan')
            self.write({'reference': seq})
        for loan in self:
            if loan.loan_amount <= 0:
                raise Warning(
                _('Loan Amount should be greater than zero. Kindly change the loan amount.'))
            if loan.employee_gross < loan.loan_amount:
                raise Warning(
                _('Loan Amount should not exceed Gross Salary. Kindly change the loan amount.'))
            if not loan.duration or loan.duration < 0:
                raise Warning(
                _('Payment Duration should be greater than zero.'))
            due_date = False
            if loan.emi_based_on != 'percentage':
                due_date = (datetime.strptime(loan.start_date, '%Y-%m-%d') +
                            timedelta(loan.duration * 365 / 12)).strftime('%Y-%m-%d')
            self.write({'state': 'hod_approval', 'due_date': due_date, 'request_date': current_date})
        return True

    @api.multi
    def button_hod_approve(self):
        if self.state == 'hod_approval':
            if self.date_of_joining:
                d1 = datetime.strptime(self.date_of_joining, "%Y-%m-%d").date()
                d2 = date.today()
                self.year = relativedelta(d2, d1).years
                # if self.year < 2:
                #     raise UserError(
                #         _('Employee not completed two years in the company'))
            # if self.loan_amount > self.gross_contract_id.gross_sal:
            #     raise UserError(
            #         'Employee salary is less than requested loan amount')
            self.write({'state': 'hr_approval'})

    @api.multi
    def button_hr_approve(self):
        if self.state == 'hr_approval':
            self.write({'state': 'ceo_approval'})

    @api.multi
    def button_ceo_approve(self):
        if self.state == 'ceo_approval':
            self.write({'state': 'confirm'})

    @api.multi
    def start_loan(self):
        if not self.amortize_lines:
            raise UserError(
                'Please calculate loan amount first by clicking on calculate button')
        self.write({'state': 'running'})
        return True

    @api.multi
    def button_refuse(self):
        for value in self:
            if value.cancellation == 0:
                raise UserError('Please enter reason for refusal')
            else:
                self.write({'state': 'refuse'})
                return True

    @api.multi
    def close_loan(self):
        self.write({'state': 'close'})
        return True

    @api.multi
    def cancel_loan(self):
        for value in self:
            if value.cancellation == 0:
                raise UserError('Please enter reason of cancellation')
        else:
            self.write({'state': 'reject'})
            return True

    @api.multi
    def set_to_draft(self):
        self.write({'state': 'draft'})
        return True

    # @api.multi
    # def unlink(self):
    #     for rec in self:
    #         if rec.state not in ['draft', 'refuse']:
    #             raise Warning(
    #                 _('You cannot delete a loan which is in %s state.') % (rec.state))
    #     return super(HrLoan, self).unlink()


class amortization(models.Model):
    _name = 'amortization.loan'

    payment_date = fields.Date('Payment Date')
    bigining_bal = fields.Float('Begining Balance', default=0.0)
    schedule_pay = fields.Float('Installment Amount')
    total_payment = fields.Float('Total Payment', default=0.0)
    loan_amortize = fields.Many2one('hr.loan')
    principal = fields.Float('Pricipal', digits=dp.get_precision('Account'),)
    interest = fields.Float('Interest', digits=dp.get_precision('Account'),)
    ending_bal = fields.Float(
        'Ending Balance', digits=dp.get_precision('Account'),)
    employee_id = fields.Many2one('hr.employee')

    # @api.multi
    # def unlink(self):
    #     for rec in self:
    #         if rec.state not in ['draft', 'refuse']:
    #             raise Warning(
    #                 _('You cannot delete a loan which is in %s state.') % (rec.state))
    #     return super(HrLoan, self).unlink()


class InstallmentLine(models.Model):
    _name = 'installment.line'

    loan_id = fields.Many2one('hr.loan', 'Loan', required=True, readonly=True)
    payslip_id = fields.Many2one(
        'hr.payslip', 'Payslip', required=True, readonly=True)
    employee_id = fields.Many2one(
        'hr.employee', 'Employee', required=True, readonly=True)
    date = fields.Date('Date', required=True, readonly=True)
    amount = fields.Float('Installment Amount', digits=dp.get_precision(
        'Account'), required=True, readonly=True)


class PreviousLoan(models.Model):
    _name = 'previous.loan'

    loans_id = fields.Many2one('hr.loan', 'Loan', required=True, readonly=True)
    amount_to_pay = fields.Float('Amount to Pay')
    amount_paid = fields.Float('Amount Paid')
    reference = fields.Char()
    loan_amount = fields.Float('Loan Amount')
    state = fields.Selection(
        [('draft', 'Draft'),
         ('hod_approval', 'Awaiting HOD Approval'),
         ('hr_approval', 'Awaiting HR Approval'),
         ('ceo_approval', 'Awaiting CEO/MD Approval'),
         ('confirm', 'Approved'),
         ('refuse', 'Refused'),
         ('cancel', 'Cancelled'),
         ('running', 'Loan Running'),
         ('close', 'Closed')],
        string="Status", default='draft', required=True, track_visibility='onchange')
    reason = fields.Char("Reason")


class HrSkipInstallment(models.Model):
    _name = 'hr.skip.installment'

    name = fields.Char('Reason to Skip', required=True)
    loan_id = fields.Many2one(
        'hr.loan',
        'Loan',
        domain="[('employee_id','=',employee_id)]",
        required=True)
    employee_id = fields.Many2one(
        'hr.employee',
        'Employee',
        default=employee_get,
        required=True)
    employee_code = fields.Char(
        'Employee Code', related='employee_id.employee_code')
    date = fields.Date(
        'Application Date',
        default=time.strftime('%Y-%m-%d'))
    state = fields.Selection(
        [('draft', 'Draft'),
         ('open', 'Waiting Approval'),
         ('refuse', 'Refused'),
         ('confirm', 'Approved'),
         ('cancel', 'Cancelled')],
        string="Status", required=True, default='draft')

    @api.multi
    def confirm_request(self):
        return self.write({'state': 'open'})

    @api.multi
    def approve_request(self):
        self.write({'state': 'confirm'})
        return True

    @api.multi
    def refuse_request(self):
        self.write({'state': 'refuse'})
        return True

    @api.multi
    def set_to_draft(self):
        self.write({'state': 'draft'})
        return True

    @api.multi
    def set_to_cancel(self):
        return self.write({'state': 'cancel'})

    @api.multi
    def unlink(self):
        for rec in self:
            if rec.state not in ['draft', 'refuse']:
                raise UserError(
                    _('Warning!'),
                    _('You cannot delete a skip installment request which is in %s state.') % (rec.state))
        return super(HrSkipInstallment, self).unlink()


class HrEmployee(models.Model):
    _inherit = 'hr.employee'

    loan_ids = fields.One2many('hr.loan', 'employee_id', 'Loans')


class HrAmountInstallment(models.Model):
    _name = 'hr.amount.installment'

    name = fields.Char('Reason to increase/less Amount', required=True)
    loan_id_amount = fields.Many2one(
        'hr.loan',
        'Loan',
        domain="[('employee_id', '=', employee_id)]", required=True)
    employee_id = fields.Many2one(
        'hr.employee',
        'Employee',
        default=employee_get,
        required=True)
    employee_code = fields.Char(
        'Employee Code', related='employee_id.employee_code')
    date = fields.Date(
        'Date',
        required=True,
        default=time.strftime('%Y-%m-%d'))
    amount = fields.Float(
        'Loan Amount',
        digits=dp.get_precision('Account'),
        required=True)
    state = fields.Selection(
        [('draft', 'Draft'),
         ('open', 'Waiting Approval'),
         ('refuse', 'Refused'),
         ('confirm', 'Approved'),
         ('cancel', 'Cancelled')],
        string="Status", default='draft',
        required=True)

    @api.multi
    def confirm_request(self):
        return self.write({'state': 'open'})

    @api.multi
    def approve_request(self):
        self.write({'state': 'confirm'})
        return True

    @api.multi
    def refuse_request(self):
        self.write({'state': 'refuse'})
        return True

    @api.multi
    def set_to_draft(self):
        self.write({'state': 'draft'})
        return True

    @api.multi
    def set_to_cancel(self):
        return self.write({'state': 'cancel'})

    @api.multi
    def unlink(self):
        for rec in self:
            if rec.state not in ['draft', 'refuse']:
                raise UserError(
                    _('Warning!'),
                    _('You cannot delete a skip installment request which is in %s state.') % (rec.state))
        return super(HrSkipInstallment, self).unlink()


class HrPayslip(models.Model):
    _inherit = "hr.payslip"

    def hr_verify_sheet(self):
        loan_obj = self.env['hr.loan']
        skip_installment_obj = self.env['hr.skip.installment']
        slip_line_obj = self.env['hr.payslip.line']
        installment_obj = self.env['installment.line']
        self.compute_sheet()
        for payslip in self:
            loan_ids = loan_obj.search(
                [('employee_id', '=', payslip.employee_id.id),
                 ('state', '=', 'running'),
                 ('loan_type', '=', 'salary')])
            for loan in loan_ids:
                skip_installment_ids = skip_installment_obj.search(
                    [('loan_id', '=', loan.id),
                     ('state', '=', 'running'),
                     ('date', '>=', payslip.date_from),
                     ('date', '<=', payslip.date_to)])
                if skip_installment_ids and loan.emi_based_on != 'percentage':
                    due_date = datetime.strptime(
                        loan.due_date, '%Y-%m-%d') + relativedelta(months=1)
                    loan_obj.write({'due_date': due_date})
                else:
                    slip_line_ids = slip_line_obj.search(
                        [('slip_id', '=', payslip.id),
                         ('code', '=', 'LOAN' + str(loan.id))])
                    if slip_line_ids:
                        amount = slip_line_obj.read(slip_line_ids, ['total'])
                        installment_data = {
                            'loan_id': loan.id,
                            'payslip_id': payslip.id,
                            'employee_id': payslip.employee_id.id,
                            'amount': abs(slip_line_ids.amount),
                            'date': time.strftime('%Y-%m-%d')
                        }
                        new_id = installment_obj.create(installment_data)
                    if loan.amount_to_pay <= 0:
                        loan_obj.write({'state': 'done'})
        return self.write({'state': 'done'})

    @api.multi
    def check_insallments_to_pay(self):
        slip_line_obj = self.env['hr.payslip.line']
        loan_obj = self.env['hr.loan']
        rule_obj = self.env['hr.salary.rule']
        skip_installment_obj = self.env['hr.skip.installment']
        skip_amount_obj = self.env['hr.amount.installment']
        loan_amount = 0
        for payslip in self:
            loan_ids = loan_obj.search(
                [('employee_id', '=', payslip.employee_id.id),
                 ('state', '=', 'running'),
                 ('loan_type', '=', 'salary'),
                 ('start_date', '<=', payslip.date_from),
                 ('due_date', '>=', payslip.date_to)])
            rule_ids = rule_obj.search([('code', '=', 'LOAN')])
            if rule_ids:
                # rule = rule_obj.browse(rule_ids[0])
                oids = slip_line_obj.search(
                    [('slip_id', '=', payslip.id),
                     ('code', '=', 'LOAN')])
                if oids:
                    oids.unlink()
                for loan in loan_ids:
                    skip_installment_ids = skip_installment_obj.search(
                        [('loan_id', '=', loan.id),
                         ('state', '=', 'running'),
                         ('date', '>=', payslip.date_from),
                         ('date', '<=', payslip.date_to)])
                    skip_amount_ids = skip_amount_obj.search(
                        [('loan_id_amount', '=', loan.id),
                         ('state', '=', 'running'),
                         ('date', '>=', payslip.date_from),
                         ('date', '<=', payslip.date_to)])
                    if not skip_installment_ids and not skip_amount_ids:
                        slip_line_data = {
                            'slip_id': payslip.id,
                            'salary_rule_id': rule_ids.id,
                            'contract_id': payslip.contract_id.id,
                            'name': 'Loan',
                            'code': 'LOAN' + str(loan.id),
                            'category_id': rule_ids.category_id.id,
                            'sequence': rule_ids.sequence + loan.id,
                            'appears_on_payslip': rule_ids.appears_on_payslip,
                            'condition_select': rule_ids.condition_select,
                            'condition_python': rule_ids.condition_python,
                            'condition_range': rule_ids.condition_range,
                            'condition_range_min': rule_ids.condition_range_min,
                            'condition_range_max': rule_ids.condition_range_max,
                            'amount_select': rule_ids.amount_select,
                            'amount_fix': rule_ids.amount_fix,
                            'amount_python_compute': rule_ids.amount_python_compute,
                            'amount_percentage': rule_ids.amount_percentage,
                            'amount_percentage_base': rule_ids.amount_percentage_base,
                            'register_id': rule_ids.register_id.id,
                            'amount': loan.deduction_amount,
                            'employee_id': payslip.employee_id.id,
                        }
                        loan_amount = loan.deduction_amount
                        if loan.emi_based_on == 'percentage':
                            gross_ids = slip_line_obj.search(
                                [('slip_id', '=', payslip.id),
                                 ('code', '=', 'GROSS')])
                            # gross_record = slip_line_obj.browse(gross_ids[0])
                            slip_line_data.update(
                                {'amount': -(slip_line_obj.total * loan.percentage_of_gross / 100)})
                        if abs(slip_line_data['amount']) > loan.amount_to_pay:
                            slip_line_data.update(
                                {'amount': loan.amount_to_pay})
                        sid = slip_line_obj.create(slip_line_data)
                        net_ids = slip_line_obj.search(
                            [('slip_id', '=', payslip.id),
                             ('code', '=', 'NET')])
                        # net_record = slip_line_obj.browse(net_ids[0])
                        net_ids.write(
                            {'amount': net_ids.amount - slip_line_data['amount']})

                    if skip_amount_ids:
                        # skip = skip_amount_obj.browse(skip_amount_ids[0])
                        slip_line_data = {
                            'slip_id': payslip.id,
                            'salary_rule_id': rule_ids.id,
                            'contract_id': payslip.contract_id.id,
                            'name': 'loan' + str(loan.id),
                            'code': 'LOAN' + str(loan.id),
                            'category_id': rule_ids.category_id.id,
                            'sequence': rule_ids.sequence + loan.id,
                            'appears_on_payslip': rule_ids.appears_on_payslip,
                            'condition_select': rule_ids.condition_select,
                            'condition_python': rule_ids.condition_python,
                            'condition_range': rule_ids.condition_range,
                            'condition_range_min': rule_ids.condition_range_min,
                            'condition_range_max': rule_ids.condition_range_max,
                            'amount_select': rule_ids.amount_select,
                            'amount_fix': skip_amount_obj.amount,
                            'amount_python_compute': rule_ids.amount_python_compute,
                            'amount_percentage': rule_ids.amount_percentage,
                            'amount_percentage_base': rule_ids.amount_percentage_base,
                            'register_id': rule_ids.register_id.id,
                            'amount': -(skip_amount_obj.amount),
                            'employee_id': payslip.employee_id.id,
                        }
                        if loan.emi_based_on == 'percentage':
                            gross_ids = slip_line_obj.search(
                                [('slip_id', '=', payslip.id),
                                 ('code', '=', 'GROSS')])
                            # gross_record = slip_line_obj.browse(gross_ids[0])
                            slip_line_data.update(
                                {'amount': -(slip_line_obj.total * loan.percentage_of_gross / 100)})
                        if abs(slip_line_data['amount']) > loan.amount_to_pay:
                            slip_line_data.update(
                                {'amount': loan.amount_to_pay})
                        sid = slip_line_obj.create(slip_line_data)
                        net_ids = slip_line_obj.search(
                            [('slip_id', '=', payslip.id),
                             ('code', '=', 'NET')])
                        # net_record = slip_line_obj.browse(net_ids[0])
                        slip_line_obj.write(
                            net_ids, {'amount': net_ids.amount - slip_line_data['amount']})
        return loan_amount

    def compute_sheet(self):
        res = super(HrPayslip, self).compute_sheet()
        self.check_insallments_to_pay()
        return res

    def action_payslip_done(self):
        self.hr_verify_sheet()
        return super(HrPayslip, self).action_payslip_done()
