# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.


{
    'name': 'Import Attendances',
    'version': '2.0',
    'category': 'Human Resources',
    'sequence': 81,
    'summary': 'Manage employee import attendances',
    'description': """
This module aims to manage employee's import attendances.
==================================================

Keeps account of the attendances of the employees on the basis of the
actions(Check in/Check out) performed by them.
       """,
    'website': 'https://www.odoo.com/page/employees',
    'depends': ['hr', 'report', 'barcodes','attendance'],
    'data': [
       
        'views/hr_attendance_view.xml',
        'views/import_attandance_validate_views.xml',
        'views/attandance_transfer_views.xml',
        
    ],
    'demo': [
        
    ],
    'installable': True,
    'auto_install': False,
    'qweb': [
        # "static/src/xml/attendance.xml",
    ],
    'application': True,
}
