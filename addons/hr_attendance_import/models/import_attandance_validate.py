# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

import time

from odoo import api, fields, models, _
from datetime import datetime, timedelta
import odoo.addons.decimal_precision as dp
from odoo.exceptions import UserError


class ImportAttandanceWizard(models.TransientModel):
    _name = "import.attandance.wizard"
    _description = "Import Attandance Wizard"


    @api.multi
    def validate(self):
    	imp_attandance = self.env['import.attendance']
        import_attandance = self.env['import.attendance'].browse(self._context.get('active_ids', []))
        for imp in import_attandance:
            lst = []
            employee = self.env['hr.employee'].search([('old_employee_code', '=', imp.employee_code)])
            if not employee:
                reason = "Employee code is not matched"
                lst.append(reason)

            if imp.intimedatetime in [False,None,'None','False','']:
                reason = "Check in data is not defined"
                lst.append(reason)
            if imp.outtimedatetime in [False,None,'None','False','']:
                reason = "Check out data is not defined"
                lst.append(reason)
            if lst:
                reason = ', '.join(lst)
                import_data = imp.write({'reason':reason,
            })




class TransferAttandanceWizard(models.TransientModel):
    _name = "transfer.attandance.wizard"
    _description = "Transfer Attandance Wizard"



    @api.multi
    def transfer_attandance(self):
        import_attandance = self.env['import.attendance'].browse(self._context.get('active_ids', []))
        if all(not import_attand.reason for import_attand in import_attandance):
            for imp in import_attandance:
                import_data = imp.write({'state':'confirm',
                        'id':imp.id,})
                employee = self.env['hr.employee'].search([('old_employee_code', '=', imp.employee_code)])
                date_time_format = '%Y-%m-%d %H:%M:%S'
                get_intimedatetime = imp.intimedatetime.strip().replace('/','-')
                get_outtimedatetime = imp.outtimedatetime.strip().replace('/','-')
                intimedatetime = datetime.strptime(get_intimedatetime,date_time_format)
                outtimedatetime = datetime.strptime(get_outtimedatetime,date_time_format)
                query = """
                            SELECT id from hr_attendance 
                            where employee_id = %s and 
                            check_in = '%s' and check_out = '%s'
                    """%(employee.id, intimedatetime, outtimedatetime)
                self._cr.execute(query)
                check_atend = self._cr.dictfetchall()
                if not check_atend:
                    values = {
                            'employee_id':employee.id,
                            'check_in':intimedatetime,
                            'check_out':outtimedatetime,
                        }
                    attandance_employee = self.env['hr.attendance'].create(values)
        else:
            raise UserError(
                    'Please resolve the issues of records.')