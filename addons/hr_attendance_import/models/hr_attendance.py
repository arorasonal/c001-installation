# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from datetime import datetime

from odoo import models, fields, api, exceptions, _
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT
from collections import OrderedDict
from dateutil.rrule import rrule, DAILY
import pytz

dict_days = {'1': 0, '2': 0, '3': 0, '4': 0,'5': 0, 
            '6': 0, '7': 0, '8': 0,'9': 0, '10': 0, '11': 0, 
            '12': 0,'13': 0, '14': 0, '15': 0, '16': 0,
            '17': 0,'18': 0, '19': 0, '20': 0, '21': 0,
            '22': 0,'23': 0, '24': 0, '25': 0, '26': 0,
            '27': 0,'28': 0, '29': 0, '30': 0, '31': 0
            }
weekday = {0: 'Monday', 1: 'Tuesday',
        2:'Wednesday', 3:'Thrusday', 4:'Friday', 5: 'Saturday',
        6:'Sunday'}

class ImportAttendance(models.Model):
    _name = "import.attendance"
    _description = "Attendance"
    _order = "intimedatetime desc"
    _sql_constraints = [
        ('record_uniq', 'unique (employee_code, intimedatetime, outtimedatetime)',
         'The Employee code and check in check out of the statement must be unique per Employee ')
    ]


    name = fields.Char(string='Employee Name')
    employee_code = fields.Char(string='Employee Code')
    department = fields.Char(string='Department')
    reason = fields.Char(string='Reason')
    attendancedate = fields.Date(string="Attendance Date")
    intimedatetime = fields.Datetime(string="InTimeDateTime", default=fields.Datetime.now)
    outtimedatetime = fields.Datetime(string="OutTimeDateTime")
    state = fields.Selection(
        [('draft', "Draft"),
         ('confirm', 'Confirmed')], "State", track_visibility='always', default='draft')

class MonthlyAttendanceSheet(models.Model):
    _inherit = 'hr.indian.attendance'

    attendance_line_employee = fields.One2many(
        'employee.attendance.line', 'attendance_id',
        string='Attendance Line', required=True,
        states={'draft': [('readonly', False)]})

    @api.multi
    def generate_leave_attendance(self):
        lst = []
        calendar = self.env['hr.holidays.calendar']
        fiscal_year = self.env['account.fiscalyear']
        year_id = fiscal_year.search(
            [('date_start', '<=', self.date_from),
             ('date_stop', '>=', self.date_to)],
            limit=1)
        date_from = datetime.strptime(self.date_from, '%Y-%m-%d')
        date_to = datetime.strptime(self.date_to, '%Y-%m-%d')
        #print("!!!!!!!!!!!!", self.second_fourth_saturday(date_from))
        holiday_query = """
            SELECT hcp.id as hcp_id, hcp.date_start as date_start, hcp.date_end as date_stop 
            FROM hr_holidays_calendar as hc
            INNER JOIN holiday_calendar_period as hcp ON hcp.holiday_id = hc.id
            WHERE state_id = %s
        """%(609)
        self._cr.execute(holiday_query)
        holiday_data = self._cr.dictfetchall()
        employee_query = """
            select he.id as emp_id, he.name as name from hr_employee as he
            where he.active='t'
        """
        self._cr.execute(employee_query)
        employee_data = self._cr.dictfetchall()
        contract_obj = self.env['hr.contract']
        resource_day = []
        for employee in employee_data:
            resource_calendar = contract_obj.search([
                ('employee_id', '=', employee.get('emp_id')),
                ('status1', 'not in', ['close_contract', 'terminated'])
                ])
            if resource_calendar:
                resource_calendar = resource_calendar.working_hours
                for calendar1 in resource_calendar.attendance_ids:
                    resource_day.append(int(calendar1.dayofweek))
            self.get_short_leave(employee, date_from, date_to)
            if resource_calendar:
                self.get_full_leave(employee, date_from, date_to, resource_day)
                self.get_half_leave(employee, date_from, date_to, resource_day)

    def get_short_leave(self, employee, date_from, date_to):
        lst = []
        emp_dict = {}
        if employee.get('emp_id') not in emp_dict:
            emp_dict[employee.get('emp_id')] = {}
        for dt in rrule(DAILY, dtstart=date_from, until=date_to):
            new_dic = {}
            full_day_leave = {}
            #holiday_datas = [rec for rec in holiday_data if rec.get('date_start') == str(dt.date()) and rec.get('date_start','') not in (None,False,'')]
            leave_query = """
                    SELECT at.date_from, at.date_to 
                    FROM hr_holidays as at
                    LEFT JOIN hr_holidays_status as hs ON at.holiday_status_id = hs.id
                    WHERE at.date_from >= '%s' and at.date_to <= '%s' and employee_id = %s and type='remove' and hs.name = 'Short Leave'
            """%(str(dt.date()), str(dt.date()), employee.get('emp_id'))
            self._cr.execute(leave_query)
            leave_data = self._cr.dictfetchone()
            if leave_data:
                attendance_query = """
                    SELECT at.check_in, at.check_out 
                    FROM hr_attendance as at 
                    WHERE at.check_in::date >= '%s' and at.check_out::date <= '%s' and employee_id = %s
                """%(str(dt), str(dt), employee.get('emp_id'))
                self._cr.execute(attendance_query)
                attendance_data = self._cr.dictfetchone()
                if attendance_data not in [None, 'None', False, 'False']:
                    new_dic['short_leave'] = {
                        'check_in': attendance_data.get('check_in'),
                        'check_out': attendance_data.get('check_out')
                    }
                lst.append({employee.get('emp_id'): new_dic})
        #print("lst",lst)
        for ls in lst:
            for k in ls:
                #print("@@@@@@@",k, ls[k])
                if 'short_leave' in ls[k]:
                    self.env['short.leave'].create({
                        'hr_indian_short_id': self.id,
                        'start_date': ls[k]['short_leave']['check_in'],
                        'end_date': ls[k]['short_leave']['check_out'],
                        'employee_id': k
                        })

    def get_full_leave(self, employee, date_from, date_to, resource_day):
        lst = []
        contract_obj = self.env['hr.contract']
        resource_calendar = contract_obj.search([
            ('employee_id', '=', employee.get('emp_id')),
            ('status1', 'not in', ['close_contract', 'terminated'])
            ])
        emp_dict = {}
        if employee.get('emp_id') not in emp_dict:
            emp_dict[employee.get('emp_id')] = {}
        for dt in rrule(DAILY, dtstart=date_from, until=date_to):
            full_day_leave = {} 
            leave_query = """
                    SELECT at.date_from, at.date_to 
                    FROM hr_holidays as at
                    LEFT JOIN hr_holidays_status as hs ON at.holiday_status_id = hs.id
                    WHERE at.date_from >= '%s' and at.date_to <= '%s' and employee_id = %s and type='remove' 
                    and hs.name in  ('Sick Leave', 'Compensatory Leave', 'Maternity Leave', 'Earned Leave', 'Restricted Leave')
                    and full_half_day='full'
            """%(str(dt.date()), str(dt.date()), employee.get('emp_id'))
            self._cr.execute(leave_query)
            leave_data = self._cr.dictfetchone()
            if leave_data:
                if dt.weekday() in resource_day:
                    full_day_leave['full_leave'] = {
                        'date': dt.date()
                    }
                    lst.append({employee.get('emp_id'): full_day_leave})
        for ls in lst:
            for k in ls:
                if 'full_leave' in ls[k]:
                    self.env['full.leave'].create({
                        'hr_indian_full_id': self.id,
                        'start_date': ls[k]['full_leave']['date'],
                        'employee_id': k
                        })
    def get_half_leave(self, employee, date_from, date_to, resource_day):
        lst = []
        contract_obj = self.env['hr.contract']
        resource_calendar = contract_obj.search([
            ('employee_id', '=', employee.get('emp_id')),
            ('status1', 'not in', ['close_contract', 'terminated'])
            ])
        emp_dict = {}
        if employee.get('emp_id') not in emp_dict:
            emp_dict[employee.get('emp_id')] = {}
        for dt in rrule(DAILY, dtstart=date_from, until=date_to):
            half_day_leave = {} 
            leave_query = """
                    SELECT at.date_from, at.date_to 
                    FROM hr_holidays as at
                    LEFT JOIN hr_holidays_status as hs ON at.holiday_status_id = hs.id
                    WHERE at.date_from >= '%s' and at.date_to <= '%s' and employee_id = %s and type='remove' 
                    and hs.name in  ('Sick Leave', 'Compensatory Leave', 'Maternity Leave', 'Earned Leave', 'Restricted Leave')
                    and full_half_day='half'
            """%(str(dt.date()), str(dt.date()), employee.get('emp_id'))
            self._cr.execute(leave_query)
            leave_data = self._cr.dictfetchone()
            if leave_data:
                attendance_query = """
                    SELECT at.check_in, at.check_out 
                    FROM hr_attendance as at 
                    WHERE at.check_in::date >= '%s' and at.check_out::date <= '%s' and employee_id = %s
                """%(str(dt), str(dt), employee.get('emp_id'))
                self._cr.execute(attendance_query)
                attendance_data = self._cr.dictfetchone()
                if attendance_data not in [None, 'None', False, 'False']:
                    half_day_leave['half_leave'] = {
                        'check_in': attendance_data.get('check_in'),
                        'check_out': attendance_data.get('check_out')
                    }
                    lst.append({employee.get('emp_id'): half_day_leave})
        for ls in lst:
            for k in ls:
                if 'half_leave' in ls[k]:
                    self.env['half.leave'].create({
                        'hr_indian_half_id': self.id,
                        'start_date': ls[k]['half_leave']['check_in'],
                        'end_date': ls[k]['half_leave']['check_out'],
                        'employee_id': k
                        })

    @api.multi
    def button_generate_attendance(self):
        week_offs = 0
        holidays = 0
        DATE_FORMAT = "%Y-%m-%d"
        holiday_obj = self.env['hr.holidays']
        from_date = datetime.strptime(self.date_from, "%Y-%m-%d")
        to_date = datetime.strptime(self.date_to, "%Y-%m-%d")
        number_of_days = to_date.day - from_date.day
        emp_obj = self.env['hr.employee']
        fiscal_year = self.env['account.fiscalyear']
        hr_holiday_obj = self.env['hr.holidays.calendar']
        self.detailed_attendance_id.unlink()
        self.sandwich_leave_ids.unlink()
        self.short_leave_ids.unlink()
        self.half_day_leave_ids.unlink()
        self.hr_indian_full_ids.unlink()
        self.attendance_line_employee.unlink()
        #self._generate_attendance_sheet()
        self.generate_leave_attendance()
        self.update_employee_sheet()
        line_data = []
        domain = []
        for obj in self:
            resource_day = []
            week_off_day = 0
            holiday = 0
            contract_obj = self.env['hr.contract']
            resource_calendar = contract_obj.search([
                ('employee_id', '=', obj.employee_id.id),
                ('status1', 'not in', ['close_contract', 'terminated'])
            ])
            year_id = fiscal_year.search([
                ('date_start', '<=', obj.date_from),
                ('date_stop', '>=', obj.date_to)],
                limit=1)
            if resource_calendar:
                resource_calendar = resource_calendar.working_hours
                for calendar1 in resource_calendar.attendance_ids:
                    resource_day.append(int(calendar1.dayofweek))
                for dt in rrule(DAILY, dtstart=from_date, until=to_date):
                    if dt.weekday() not in resource_day:
                        week_off_day += 1
                cal = calendar.monthcalendar(from_date.year, to_date.month)
                first_week = cal[0]
                second_week = cal[1]
                if first_week[calendar.SATURDAY]:
                    holi_day = second_week[calendar.SATURDAY]
                    # if holi_day:
                    #     week_offs = week_off_day + 1
            if year_id:
                holiday_id = hr_holiday_obj.search(
                    [('fiscal_id', '=', year_id.id),
                     ('state_id', '=', obj.employee_id.location.state_id.id)])
                for holidays in holiday_id.period_ids:
                    date_start = datetime.strptime(
                        holidays.date_start, DATE_FORMAT)
                    date_to = datetime.strptime(holidays.date_end, DATE_FORMAT)
                    for dt in rrule(DAILY, dtstart=date_start, until=date_to):
                        if (dt >= from_date) and (dt <= to_date):
                            holiday = holiday + 1
                holidays = holiday
        if self.payroll_batche:
            domain = [('payroll_batche', '=', self.payroll_batche.id),
                      ('date_of_joining', '<=', self.date_to)
                      ]
        else:
            domain = [('date_of_joining', '<=', self.date_to)]
        for count, emp in enumerate(emp_obj.search(domain)):
            line_data.append({
                'employee_id': emp.id,
            })
            for days, record in enumerate(self.field_list):
                absent_days = self.get_absent_days(from_date, to_date, emp)
                if days + 1 not in absent_days:
                    line_data[count][record] = True
                if days == number_of_days:
                    break
            date1 = self.date_from
            date2 = self.date_to
            data = self._cr.execute(
                ''' select
                    employee_id,
                    check_in,
                    check_out
                from
                    hr_attendance
                where
                    employee_id = %s and
                    (check_in)::date between '%s' and '%s'
                    order by check_in
                ''' % (emp.id, date1, date2))
            rec_attendance = self._cr.fetchall()
            ab = len(rec_attendance)
            test_attendance = ab + week_offs + holidays
            if not rec_attendance:
                holiday_rec = holiday_obj.search([
                    ('employee_id', '=', emp.id),
                    ('type', '=', 'remove'),
                    '|',
                    ('state', '=', 'validate'),
                    ('state', '=', 'confirm'),
                    ('holiday_status_id.name',
                     '=', 'Casual Leave'),
                    ('date_from', '>=', date1),
                    ('date_from', '<=', date2)])
                for rec in holiday_rec:
                    rec.write({'state': 'lapsed'})
            elif test_attendance <= 11:
                holiday_rec = holiday_obj.search([
                    ('employee_id', '=', emp.id),
                    ('type', '=', 'remove'),
                    '|',
                    ('state', '=', 'validate'),
                    ('state', '=', 'confirm'),
                    ('holiday_status_id.name',
                     '=', 'Earned Leave'),
                    ('date_from', '>=', date1),
                    ('date_from', '<=', date2)])
                for rec in holiday_rec:
                    if rec.remaining_leave == rec.number_of_days_temp:
                        rec.write({'state': 'lapsed'})
        self.attendance_line = line_data
        return True

    @api.multi
    def update_employee_sheet(self):
        lst = []
        calendar = self.env['hr.holidays.calendar']
        fiscal_year = self.env['account.fiscalyear']
        year_id = fiscal_year.search(
            [('date_start', '<=', self.date_from),
             ('date_stop', '>=', self.date_to)],
            limit=1)
        date_from = datetime.strptime(self.date_from, '%Y-%m-%d')
        date_to = datetime.strptime(self.date_to, '%Y-%m-%d')
        employee_query = """
            select he.id as emp_id, he.name as name from hr_employee as he
            where he.active='t'
        """
        self._cr.execute(employee_query)
        employee_data = self._cr.dictfetchall()

        holiday_query = """
            select hcp.id as hcp_id, hcp.date_start as date_start, hcp.date_end as date_stop from hr_holidays_calendar as hc
            INNER JOIN holiday_calendar_period as hcp ON hcp.holiday_id = hc.id
            where state_id = %s
        """%(609)
        self._cr.execute(holiday_query)
        holiday_data = self._cr.dictfetchall()
        for employee in employee_data:
            emp_dict = {}
            if employee.get('emp_id') not in emp_dict:
                emp_dict[employee.get('emp_id')] = {}
            for dt in rrule(DAILY, dtstart=date_from, until=date_to):
                day_name = str(dt.date().strftime("%A"))
                holiday_datas = [rec for rec in holiday_data if rec.get('date_start') == str(dt.date()) and rec.get('date_start','') not in (None,False,'')]
                attendance_query = """
                    select at.check_in, at.check_out from hr_attendance as at where at.check_in::date
                     >= '%s' and at.check_out::date <= '%s' and employee_id = %s
                """%(str(dt), str(dt), employee.get('emp_id'))
                self._cr.execute(attendance_query)
                attendance_data = self._cr.dictfetchall()
                attendance_days = {}
                if attendance_data not in [None, 'None', False, 'False']:
                    for attendance in attendance_data:
                        check_in_date = datetime.strptime(attendance.get('check_in'), '%Y-%m-%d %H:%M:%S').date()
                        check_out_date = datetime.strptime(attendance.get('check_out'), '%Y-%m-%d %H:%M:%S').date()
                        check_in = datetime.strptime(attendance.get('check_in'), '%Y-%m-%d %H:%M:%S')
                        check_out = datetime.strptime(attendance.get('check_out'), '%Y-%m-%d %H:%M:%S')
                        diff = check_out - check_in
                        days, seconds = diff.days, diff.seconds
                        hours = days * 24 + seconds // 3600
                        if holiday_datas:
                            emp_dict[employee.get('emp_id')].update({dt.day: str(hours) +  ' ' + 'H' })
                        else:
                            emp_dict[employee.get('emp_id')].update({dt.day: str(hours)})
                if not attendance_data:
                    if holiday_datas:
                        emp_dict[employee.get('emp_id')].update({dt.day: 'H'})
                    else:
                        emp_dict[employee.get('emp_id')].update({dt.day: str(0)})
                if day_name == 'Sunday':
                    emp_dict[employee.get('emp_id')].update({dt.day: 'H'})
            new_dict = {}
            dict1 = sorted(emp_dict[employee.get('emp_id')].items(), key = lambda kv:(kv[0], kv[1]))
            new_dict.update({employee.get('emp_id'): dict1})
            lst.append(new_dict)
        for ls in lst:
            for k in ls:
                data = self.prepare_data(ls[k])
                self.env['employee.attendance.line'].create({'employee_id': k, 
                    'one': data.get('one'),
                    'two': data.get('two'),
                    'three': data.get('three'),
                    'four': data.get('four'),
                    'five': data.get('five'),
                    'six': data.get('six'),
                    'seven': data.get('seven'),
                    'eight': data.get('eight'),
                    'nine': data.get('nine'),
                    'ten': data.get('ten'),
                    'one_1': data.get('one_1'),
                    'one_2': data.get('one_2'),
                    'one_3': data.get('one_3'),
                    'one_4': data.get('one_4'),
                    'one_5': data.get('one_5'),
                    'one_6': data.get('one_6'),
                    'one_7': data.get('one_7'),
                    'one_8': data.get('one_8'),
                    'one_9': data.get('one_9'),
                    'one_0': data.get('one_0'),
                    'two_1': data.get('two_1'),
                    'two_2': data.get('two_2'),
                    'two_3': data.get('two_3'),
                    'two_4': data.get('two_4'),
                    'two_5': data.get('two_5'),
                    'two_6': data.get('two_6'),
                    'two_7': data.get('two_7'),
                    'two_8': data.get('two_8'),
                    'two_9': data.get('two_9'),
                    'two_0': data.get('two_0'),
                    'three_1': data.get('three_1'),
                    'attendance_id': self.id})
                
    @api.multi
    def prepare_data(self, value):
        vals = {}
        for val in value:
            if val[0] == 1:
                vals.update({'one': val[1]})
            if val[0] == 2:
                vals.update({'two': val[1]})
            if val[0] == 3:
                vals.update({'three': val[1]})
            if val[0] == 4:
                vals.update({'four': val[1]})
            if val[0] == 5:
                vals.update({'five': val[1]})
            if val[0] == 6:
                vals.update({'six': val[1]})
            if val[0] == 7:
                vals.update({'seven': val[1]})
            if val[0] == 8:
                vals.update({'eight': val[1]})
            if val[0] == 9:
                vals.update({'nine': val[1]})
            if val[0] == 10:
                vals.update({'ten': val[1]})
            if val[0] == 11:
                vals.update({'one_1': val[1]})
            if val[0] == 12:
                vals.update({'one_2': val[1]})
            if val[0] == 13:
                vals.update({'one_3': val[1]})
            if val[0] == 14:
                vals.update({'one_4': val[1]})
            if val[0] == 15:
                vals.update({'one_5': val[1]})
            if val[0] == 16:
                vals.update({'one_6': val[1]})
            if val[0] == 17:
                vals.update({'one_7': val[1]})
            if val[0] == 18:
                vals.update({'one_8': val[1]})
            if val[0] == 19:
                vals.update({'one_9': val[1]})
            if val[0] == 20:
                vals.update({'one_0': val[1]})
            if val[0] == 21:
                vals.update({'two_1': val[1]})
            if val[0] == 22:
                vals.update({'two_2': val[1]})
            if val[0] == 23:
                vals.update({'two_3': val[1]})
            if val[0] == 24:
                vals.update({'two_4': val[1]})
            if val[0] == 25:
                vals.update({'two_5': val[1]})
            if val[0] == 26:
                vals.update({'two_6': val[1]})
            if val[0] == 27:
                vals.update({'two_7': val[1]})
            if val[0] == 28:
                vals.update({'two_8': val[1]})
            if val[0] == 29:
                vals.update({'two_9': val[1]})
            if val[0] == 30:
                vals.update({'two_0': val[1]})
            if val[0] == 31:
                vals.update({'three_1': val[1]})
        return vals
class EmployeeAttendanceLine(models.Model):
    _name = 'employee.attendance.line'

    employee_id = fields.Many2one('hr.employee', 'Employee', required=True)
    attendance_id = fields.Many2one(
        'hr.indian.attendance', 'Attendance Reference')
    hr_attendance_id = fields.Many2one('hr.indian.attendance')
    date_from = fields.Date(related='hr_attendance_id.date_from')
    date_to = fields.Date(related='hr_attendance_id.date_to')
    state = fields.Selection(related='hr_attendance_id.state')
    one = fields.Char('1', default='0')
    two = fields.Char('2', default='0')
    three = fields.Char('3', default='0')
    four = fields.Char('4', default='0')
    five = fields.Char('5', default='0')
    seven = fields.Char('7', default='0')
    six = fields.Char('6', default='0')
    eight = fields.Char('8', default='0')
    nine = fields.Char('9', default='0')
    ten = fields.Char('10', default='0')
    one_1 = fields.Char('11', default='0')
    one_2 = fields.Char('12', default='0')
    one_3 = fields.Char('13', default='0')
    one_4 = fields.Char('14', default='0')
    one_5 = fields.Char('15', default='0')
    one_6 = fields.Char('16', default='0')
    one_7 = fields.Char('17', default='0')
    one_8 = fields.Char('18', default='0')
    one_9 = fields.Char('19', default='0')
    one_0 = fields.Char('20', default='0')
    two_1 = fields.Char('21', default='0')
    two_2 = fields.Char('22', default='0')
    two_3 = fields.Char('23', default='0')
    two_4 = fields.Char('24', default='0')
    two_5 = fields.Char('25', default='0')
    two_6 = fields.Char('26', default='0')
    two_7 = fields.Char('27', default='0')
    two_8 = fields.Char('28', default='0')
    two_9 = fields.Char('29', default='0')
    two_0 = fields.Char('30', default='0')
    three_1 = fields.Char('31')



