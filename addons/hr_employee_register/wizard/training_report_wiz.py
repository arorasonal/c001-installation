# -*- coding: utf-8 -*-
from odoo import api, fields, models, tools


class EmployeeTrainingWiz(models.Model):
    _name = 'employee.training.wiz'

    @api.multi
    def print_training_report(self, ids, context=None):
        training_obj = self.env['hr.employee']
        training_ids = training_obj.search(
            ['|', ('active', '!=', True), ('active', '=', True)]) or []
        datas = {
            'ids': training_ids,
            'model': 'hr.employee',
            # 'form': data
        }
        return {
            'type': 'ir.actions.report.xml',
            'report_name': 'employee.list.xls',
            'datas': datas,
        }


class BankDetailWiz(models.Model):
    _name = 'bank.detail.wiz'

    @api.multi
    def print_bank_report(self):
        training_obj = self.env['hr.employee']
        training_ids = training_obj.search([]) or []

        datas = {
            'ids': training_ids,
            'model': 'hr.employee',
            # 'form': data
        }
        return {
            'type': 'ir.actions.report.xml',
            'report_name': 'employee.bank.report.xls',
            'datas': datas,
        }


class PanDetailWiz(models.Model):
    _name = 'pan.detail.wiz'

    @api.multi
    def print_pan_report(self):
        training_obj = self.env['hr.employee']
        training_ids = training_obj.search([]) or []

        datas = {
            'ids': training_ids,
            'model': 'hr.employee',
            # 'form': data
        }
        return {
            'type': 'ir.actions.report.xml',
            'report_name': 'employee.pan.report.xls',
            'datas': datas,
        }
