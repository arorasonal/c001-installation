from odoo import models, api, fields


class HrEmployee(models.Model):
    _inherit = 'hr.employee'

    # override list in custom module to add/drop columns or change order
    financial_year_ids = fields.One2many('tds.financial', 'employee_id')

    def _value_report_xls_fields(self):
        return [
            'employee_id', 'name', 'active', 'joining',
            'location', 'cost_center', 'job_id', 'department_id',
            'sub_department', 'employee_type', 'band',
            'band_designation', 'source_hiring_ids', 'source_detail_ids',
            'parent_id', 'previous_employer', 'start_date',
            'end_date', 'designation', 'previous_employer1', 'start_date1',
            # 'curr_work_experience',
            'end_date1', 'designation1', 'previous_employer2',
            'start_date2', 'end_date2', 'designation2', 'work_exp',
            'birthday', 'work_phone', 'mobile_phone', 'emer_cont',
            'emer_name', 'marital', 'gender', 'work_email',
            'personal_email', 'fam_father', 'fam_father_date_of_birth',
            'fam_mother', 'fam_mother_date_of_birth', 'date_of_marriage',
            'fam_spouse', 'fam_dob', 'child_1', 'child_dob',
            'child_gender', 'child_2', 'child_dob2', 'child_gender2',
            'identification_id', 'esic_no', 'blood_group', 'pff_no', 'uan_no',
            'address_home_id', 'corres_home_id', 'degree', 'institute_name',
            'passing_date', 'degree1', 'institute_name1', 'passing_date1',
            'registration', 'last_working', 'exit_reason', 'exit_type',
        ]

    # Change/Add Template entries
    @api.model
    def _report_xls_template(self):
        """
        Template updates, e.g.

        my_change = {
            'user':{
                'header': [1, 20, 'text', _('My Move Title')],
                'lines': [1, 0, 'text', _render("line.user_id.name or ''")],
                'totals': [1, 0, 'text', None]},
        }
        return my_change
        """
        return {}

    def _value_bank_report_xls_fields(self):
        return [
            'employee_id', 'name', 'active', 'joining',
            'last_working', 'cost_center',
            'job_id', 'department_id', 'band',
            'parent_id', 'work_email',
            'bank_account_id', 'bank_account', 'bank_account_ifsc'
        ]

    # Change/Add Template entries
    @api.model
    def _report_bank_xls_template(self):
        """
        Template updates, e.g.

        my_change = {
            'user':{
                'header': [1, 20, 'text', _('My Move Title')],
                'lines': [1, 0, 'text', _render("line.user_id.name or ''")],
                'totals': [1, 0, 'text', None]},
        }
        return my_change
        """
        return {}

    def _value_pan_report_xls_fields(self):
        return [
            'employee_id', 'name', 'active',
            'joining', 'last_working',
            'office_location', 'cost_center',
            'job_id', 'department_id', 'band',
            'parent_id', 'work_email',
            'pan_id'
        ]

    # Change/Add Template entries
    @api.model
    def _report_pan_xls_template(self):
        """
        Template updates, e.g.

        my_change = {
            'user':{
                'header': [1, 20, 'text', _('My Move Title')],
                'lines': [1, 0, 'text', _render("line.user_id.name or ''")],
                'totals': [1, 0, 'text', None]},
        }
        return my_change
        """
        return {}


class Tds_Deduction(models.Model):
    _name = 'tds.deduction'

    months = fields.Char()
    tds_amount = fields.Float()
    tds_id = fields.Many2one('tds.financial')


class Tds_Financial(models.Model):
    _name = 'tds.financial'

    employee_id = fields.Many2one('hr.employee')
    start_date = fields.Date()
    end_date = fields.Date()
    tds_deduction = fields.One2many('tds.deduction', 'tds_id')
