# -*- coding: utf-8 -*-
from datetime import datetime, timedelta
from odoo.exceptions import UserError, ValidationError
from odoo.tools.translate import _
from dateutil.relativedelta import relativedelta
from odoo import api, fields, models, tools
from datetime import date
import re


YEARLY_CARRY_OVER_POLICY = 12
MAX_CARRY_OVER = 65


class HrEmployee(models.Model):
    _inherit = "hr.employee"
    _order = 'employee_code desc, id desc'

    state = fields.Selection([
        ("draft", "Draft"),
        ("awaiting_approval", "Awaiting Confirmation"),
        # ("approved", "Approved"),
        # ("refused", "Refused"),
        ("confirmed", "Confirmed"),
        ("cancelled", "Cancelled"),
    ], 'State', default="draft")
    reason = fields.Char()
    last_name = fields.Char(string="Last Name")
    gender = fields.Selection([
        ('male', 'Male'),
        ('female', 'Female'),
        ('transgender', 'Transgender')
    ], string='Gender', default=False)

    @api.multi
    def sent_for_approval(self):
        if self.state == 'draft':
            self.state = 'awaiting_approval'
            user_obj = self.env['res.users'].search([('id', '=', 1)])
            users = self.env['res.users'].search([])
            mail = ''
            for order in self:
                if order.state == "awaiting_approval":
                    template = self.env.ref(
                        'hr_employee_register.send_for_approval_process')
                    for user in users:
                        if user.has_group('hr.group_hr_manager'):
                            mail += user.login + ','
                    if template:
                        template.send_mail(order.id, force_send=True)
                        template.email_from = user_obj.login or ''
                        template.email_to = mail

    @api.multi
    def confirm(self, vals):
        if not self.employee_code:
            vals = self.env[
                'ir.sequence'].with_context(force_company=self.company_id.id).next_by_code('employee')
            if self.company_id.company_code:
                emp_code = self.company_id.company_code + vals
                self.employee_code = emp_code
            else:
                raise UserError(
                    _('Warning! Company code is not set, please configure Company code'))
        self.state = 'confirmed'

    READONLY_STATES = {
        'confirmed': [('readonly', True)],
        'awaiting_approval': [('readonly', True)],
        'draft': [('readonly', False)],
    }

    @api.multi
    def _get_country_id(self):
        contry_id = self.env['res.country']
        comp_search_id = contry_id.search([('code', '=', 'IN')])
        return comp_search_id and comp_search_id[0] or False

    company_id = fields.Many2one(
        'res.company',
        'Company',
        states=READONLY_STATES)

    name_related = fields.Char(
        related='resource_id.name',
        string='Employee Name',
        readonly=True,
        store=True)
    biomatric_id = fields.Integer(
        'Biomatric Code',
        size=15)
    old_employee_code = fields.Char(
        'Old Employee Code',
        size=15, states=READONLY_STATES)
    employee_code = fields.Char(
        'Employee Code',
        size=15)
    next_code = fields.Integer('Next Code')
    location = fields.Many2one(
        'hr.location',
        "Location",
        states=READONLY_STATES)
    shift = fields.Selection(
        [('day', 'Day'),
         ('night', 'Night'),
         ], 'Shift', default="day", states=READONLY_STATES)
    blood_group = fields.Selection(
        [
            ('O-', 'O−'),
            ('O+', 'O+'),
            ('A-', 'A−'),
            ('A+', 'A+'),
            ('B-', 'B−'),
            ('B+', 'B+'),
            ('AB-', 'AB−'),
            ('AB+', 'AB+'),
            ('not_available', 'Not Available')
        ],
        "Blood Group",
        states=READONLY_STATES)
    medical_fitness = fields.Boolean(
        'Medical Fitness',
        states=READONLY_STATES)
    background_verification = fields.Selection(
        [('Positive', 'Positive'),
         ('Negative', 'Negative'),
         ('Yet to be Initiated', 'Yet to be Initiated'),
         ('Not Required', 'Not Required'),
         ('In Progress', 'In Progress')],
        'Background Verification', states=READONLY_STATES)
    deputation = fields.Char(
        'Deputation', size=50,
        states=READONLY_STATES)
#     # office_location = fields.Many2one(
#     #     'hr.employee.service.area',
#     #     'Service Area',
#     #     states=READONLY_STATES)
    organization_details = fields.One2many(
        'hr.employee_organization_details',
        'employee_id',
        'Organization Details',
        states=READONLY_STATES)
    date_of_joining = fields.Date(
        'Date of Joining', states=READONLY_STATES)
    family_details = fields.One2many(
        'hr.employee_family_details',
        'employee_id', 'Family Details',
        states=READONLY_STATES)
    active = fields.Boolean(
        "Active", default=True)
    cost_center = fields.Many2one(
        'hr.cost.center', "Cost Centre",
        states=READONLY_STATES)
    designation = fields.Many2one(
        'hr.designation', "Designation",
        states=READONLY_STATES)
    designation_emp = fields.Many2one(
        'hr.designation', "Designation",
        states=READONLY_STATES)

    sub_department = fields.Many2one(
        'hr.sub.department', "Sub Department", states=READONLY_STATES)
    manager_code = fields.Char(
        "Manager  Code", states=READONLY_STATES)
    mobile_phone = fields.Char(
        'Personal Mobile Number',
        states=READONLY_STATES, size=10)
    work_phone = fields.Char('Work Contact Number', states=READONLY_STATES)
    personal_email = fields.Char(
        "Personal Email", states=READONLY_STATES)
    parent_id = fields.Many2one(
        'hr.employee', 'Manager')
    # employee_type = fields.Selection(
    #     [('permanent,', 'Permanent'),
    #      ('contractual', 'Contractual'),
    #      ('consultant,', 'Consultant'),
    #      ('trainee,', 'Trainee')],
    #     'Employee Type', required=True,
    #     states=READONLY_STATES)
    employee_type = fields.Many2one('emp.type', 'Employee Type', required=True,
                                    states=READONLY_STATES)
    fam_spouse = fields.Char("Spouse Name", states=READONLY_STATES)
    fam_dob = fields.Date("Spouse's DOB", states=READONLY_STATES)
    fam_spouse_employer = fields.Char("Employer", states=READONLY_STATES)
    fam_spouse_tel = fields.Char("Work Contact No", states=READONLY_STATES)

    fam_children_ids = fields.One2many(
        'hr.employee.children', 'employee_id', "Children",
        states=READONLY_STATES)
    fam_father = fields.Char("Father's Name", states=READONLY_STATES)
    identification_mark = fields.Char(
        "Identification Mark", states=READONLY_STATES)
    fam_father_date_of_birth = fields.Date(
        " Father's Date of Birth", oldname='fam_father_dob',
        states=READONLY_STATES)
    fam_mother = fields.Char(
        "Mother's Name", states=READONLY_STATES)
    fam_mother_date_of_birth = fields.Date(
        "Mother's Date of Birth", oldname='fam_mother_dob', states=READONLY_STATES)
    date_of_marriage = fields.Date(
        "Date of Marriage", states=READONLY_STATES)
    emergency_detail_ids = fields.One2many(
        'emgency.detail', 'emg_id', string="Emergency Contact Detail",
        states=READONLY_STATES)
    emergency_contact_ids = fields.One2many(
        'emgency.detail', 'emg_id',
        string="Emergency Contact Detail", states=READONLY_STATES)
    bank = fields.Many2one(
        'res.bank', "Bank (with Branch)", states=READONLY_STATES)
    confirmation_date = fields.Date(
        string='Confirmation Due Date', states=READONLY_STATES)
    # confirmation_state = fields.Char(
    #     "Confirmation Status I",
    #     states=READONLY_STATES)
    # confirmation_state_2 = fields.Char(
    #     "Confirmation Status II",
    #     states=READONLY_STATES)
    # address_home = fields.Char(
    #     'Permanent Address')
    contact_address = fields.Char(
        'Other Address', states=READONLY_STATES)
    work_history_ids = fields.One2many(
        'hr.work.history',
        'hr_work_exp',
        string="Work History",
        states=READONLY_STATES)
    category = fields. Many2one(
        'hr.category',
        "Category",
        states=READONLY_STATES)

    marital = fields.Selection(
        [('single', 'Single'),
         ('married', 'Married'),
         ('widower', 'Widower'),
         ('divorced', 'Divorced')],
        'Marital Status', copy=False,
        states=READONLY_STATES, default='single', groups=None)
    work_contact_no = fields.Char("Work Contact No")

    work_email = fields.Char(
        'Work Email',
        size=240,
        states=READONLY_STATES)
    identification_id = fields.Char(
        'PAN Card #', size=10,
        states=READONLY_STATES)
    aadhar_number = fields.Char(
        'Aadhar #', states=READONLY_STATES)
    ir_attachment_aadhar = fields.Many2many(
        'ir.attachment', 'rel_aadhar_attachment',
        'aadhar_id', 'attachment_aadhar_id', 'Aadhar Card', states=READONLY_STATES)
    pan_attachment_ids = fields.Many2many(
        'ir.attachment', 'rel_pan_attachment',
        'pan_id', 'attachment_pan_form', 'Attach PAN Card', states=READONLY_STATES)
    cheque_attachment_ids = fields.Many2many(
        'ir.attachment', 'rel_cheque_attachment', 'cheque_id',
        'attachment_cheque_form', 'Attach Cancelled Cheque', states=READONLY_STATES)
    pf_gross_limit_monthly = fields.Integer(
        'PF Gross Limit (Monthly)', states=READONLY_STATES)
    uan_number = fields.Char('UAN #', size=12, states=READONLY_STATES)
    user_id = fields.Many2one(
        'res.users', 'User', related='resource_id.user_id', states=READONLY_STATES)
    gender = fields.Selection([
        ('male', 'Male'),
        ('female', 'Female'),
        ('other', 'Other')
    ], states=READONLY_STATES)
    birthday = fields.Date(
        'Date of Birth', states=READONLY_STATES)
    notes = fields.Text('Notes', states=READONLY_STATES)
    name = fields.Char(
        related='resource_id.name',
        store=True, oldname='name_related', states=READONLY_STATES)
    esi_number = fields.Char('ESI #', size=17, states=READONLY_STATES)
    department_id = fields.Many2one(
        'hr.department',
        'Department', states=READONLY_STATES)
    curr_work_experience = fields.Char(
        compute='curr_exp',
        string="Current Work Experience",
        states=READONLY_STATES)
    total = fields.Char(
        compute='_get_total',
        string="Total Experience",
        states=READONLY_STATES)
    total_exp = fields.Char(
        compute='_total_exp',
        string="All Total Experience",
        states=READONLY_STATES)
    previous_employer = fields.Char(
        'Previous Employee',
        states=READONLY_STATES)
    start_date = fields.Date(
        "Start Date",
        states=READONLY_STATES)
    end_date = fields.Date(
        "End Date",
        states=READONLY_STATES)
    work_exp = fields.Char("Pre Work Experience", states=READONLY_STATES)
    child_1 = fields.Char("Child", states=READONLY_STATES)
    child_dob = fields.Date(
        "Date of Birth", oldname='dob', states=READONLY_STATES)
    gratuity_date = fields.Date("Gratuity Date", states=READONLY_STATES)
    child_gender = fields.Selection(
        [('male', 'Male'),
         ('female', 'Female')], string='Gender', states=READONLY_STATES)
    # appraisal_plan = fields.Many2one(
    #     'hr_evaluation.plan', "Evaluation Plan", states=READONLY_STATES)
    # appraisal_date = fields.Date(
    #     "Next Evaluation Date", states=READONLY_STATES)

    last_working = fields.Date("Last Working Date", states=READONLY_STATES)
    exit_type_id = fields.Many2one(
        'exit.type',
        "Exit Type",
        states=READONLY_STATES)
    exit_type = fields.Char("Exit Type", states=READONLY_STATES)
    # role = fields.Many2one(
    #     'hr.role',
    #     string="Role",
    #     states=READONLY_STATES)
    job_id = fields.Many2one(
        'hr.job',
        'Designation',
        states=READONLY_STATES)
    bank_account_id = fields.Many2one(
        'res.partner.bank',
        'Bank Account',
        domain="[('partner_id', '=', address_home_id)]",
        help="Employee bank salary account",
        states=READONLY_STATES,
        groups=None)
    country_id = fields.Many2one(
        'res.country',
        'Nationality',
        states=READONLY_STATES,
        default=_get_country_id)

    bank_account_ids = fields.Many2one(
        'res.partner.bank',
        'Bank Account',
        help="Employee bank salary account",
        states=READONLY_STATES)

    no_bank_account = fields.Boolean(
        "No Bank Account", states=READONLY_STATES)
    passport_status = fields.Boolean(
        "Passport Status", states=READONLY_STATES)
    provident_fund_number = fields.Char(
        "Provident Fund #", size=16, states=READONLY_STATES)
    has_pan = fields.Boolean(
        "Has PAN #", states=READONLY_STATES)
    pension = fields.Boolean(
        "Pension", states=READONLY_STATES)
    vpf_contribution = fields.Boolean(
        "VPF Contribution", states=READONLY_STATES)
    professional_tax_contribution = fields.Boolean(
        "Professional Tax Contribution")
    pension_limit = fields.Integer(
        "Pension Limit", states=READONLY_STATES)
    business_partner = fields.Many2one(
        'res.users',
        'HR Business Partner',
        default=lambda self: self.env.uid,
        states=READONLY_STATES)
    user_ids = fields.Many2many(
        'res.users',
        'user_cus_rel',
        'userid',
        'childid', 'User', states=READONLY_STATES)
    active_id = fields.Boolean(
        "Active", states=READONLY_STATES,
        default=True)
    off_emails = fields.Char(
        'Emails', states=READONLY_STATES)
    check_group = fields.Boolean(
        compute='_check_group', states=READONLY_STATES,
        string="Group")
    attachment_ids = fields.One2many(
        'ir.attachment',
        'hr_document_id',
        string="Attachment", states=READONLY_STATES)
    completion_year = fields.Char('Year', states=READONLY_STATES)
    grade_name = fields.Many2one('emp.grade', 'Grade', states=READONLY_STATES)
    employee_billability = fields.Selection([
        ('yes', 'Yes'),
        ('no', 'No')],
        'Employee Billability', states=READONLY_STATES)
#     # project_id = fields.Many2one(
#     #     "account.analytic.account", "Project/Analytic Account",
#     #     states=READONLY_STATES)
#     # staff_id = fields.Char('Staff ID')
    applicant_qualifiaction_ids = fields.One2many(
        'education.qualifiaction',
        'hr_applicant_id',
        'Qualifaction ID', states=READONLY_STATES)
    applicant_computerskills_ids = fields.One2many(
        'education.computerskills',
        'hr_applicant_skill_id',
        'Skill ID', states=READONLY_STATES)
    education_certification_ids = fields.One2many(
        'education.hr.certification',
        'hr_applicant_id', states=READONLY_STATES)
    language_known_ids = fields.One2many(
        'known.language',
        'hr_applicant_lang_id',
        string='Language Known', states=READONLY_STATES)
    applicant_employeehistory_ids = fields.One2many(
        'employee.history',
        'hr_applicant_history_id',
        'Skill ID', states=READONLY_STATES)
    domain_experience_ids = fields.One2many(
        'employee.domain',
        'hr_applicant_id', states=READONLY_STATES)
    address_home_id = fields.Many2one(
        'res.partner', 'Permanent Address', states=READONLY_STATES,
        help='Enter here the private address of the employee, not the one linked to your company.',
        groups=None)
    is_manager = fields.Boolean(
        "Is a Manager", default=True)
    age = fields.Integer(string="Age", invisible=True)
    current_leave_state = fields.Selection(compute='_compute_leave_status', string="Current Leave Status",
        selection=[
            ('draft', 'New'),
            ('confirm', 'Waiting Approval'),
            ('refuse', 'Refused'),
            ('validate1', 'Waiting Second Approval'),
            ('validate', 'Approved'),
            ('cancel', 'Cancelled'),
            ('lapsed', 'Lapsed')
        ])
    payroll_batche = fields.Many2one('payroll.batches', 'Payroll Batche')
    @api.multi
    def name_get(self):
        result = {}
        for employee in self:
            if employee.employee_code:
                result[employee.id] = '[%s] %s' % (
                    employee.employee_code, employee.name)
            else:
                result[employee.id] = employee.name

        return result.items()

    @api.model
    def name_search(self, name, args=None, operator='ilike', limit=None):
        if not args:
            args = []
        args = args[:]
        employee_view_obj = self.env['hr.employee']

        employee_code = employee_view_obj.search(
            [('employee_code', 'ilike', name)] + args,
            limit=limit
        )
        employee_name = employee_view_obj.search(
            [('name', 'ilike', name)] + args,
            limit=limit
        )
        project = employee_code + employee_name
        return project.name_get()

    @api.multi
    def unlink(self):
        """Allows to delete hr draft"""
        for rec in self:
            if rec.state not in ['draft']:
                raise UserError(
                    'You can only delete in draft state')
        return super(HrEmployee, self).unlink()

    @api.constrains('aadhar_number')
    def check_aadhar_no(self):
        """Check Aadhar number"""
        if self.aadhar_number:
            length = self.aadhar_number
            if not (len(length) == 12 or len(length) == 16):
                raise ValidationError(
                    _('You must enter a valid adhar number'))
            elif self.aadhar_number.isdigit() == False:
                raise ValidationError(
                    _('You must enter a numeric value only'))
            else:
                return False

    @api.onchange('birthday')
    def onchange_birthday(self):
        if self.birthday:
            d1 = datetime.strptime(self.birthday, "%Y-%m-%d").date()
            d2 = date.today()
            self.age = relativedelta(d2, d1).years
            if self.age < 18:
                raise UserError(
                    _('Your age is Less then 18,you are not eligible for this job'))

    @api.multi
    def hidePersonalInformation(self, ids):
        user_obj = self.env['res.users']
        user = user_obj.browse()

        hr_employee_obj = self.env['hr.employee']

        employee = hr_employee_obj.browse()

        user_groups = []
        for grp in user.groups_id:
            user_groups.append(grp.name)
        if "Manager" in user_groups or employee.user_id.id == self.env.uid:
            return dict({ids[0]: False})
        return dict({ids[0]: True})

    @api.multi
    def _get_total(self):
        res = {}
        for line in self:
            res[line.id] = {}
            prob_period = ""
            year = 0
            month = 0
            for line_id in line.work_history_ids:
                DATETIME_FORMAT = "%Y-%m-%d"
                start = datetime.strptime(line_id.start_date, DATETIME_FORMAT)
                end = datetime.strptime(line_id.end_date, DATETIME_FORMAT)
                exp = relativedelta(end, start)
                year += exp.years
                month += exp.months
            if month > 0:
                year = year + month / 12
            if month > 12:
                month = month % 12
            prob_period = str(year) + ' year ' + str(month) + ' month '
            res[line.id] = prob_period
        return res

    @api.multi
    def curr_exp(self):
        res = {}
        start = False
        for record in self:
            DATETIME_FORMAT = "%Y-%m-%d"
            current_date = fields.Date.context_today(self)
            curr = datetime.strptime(current_date, DATETIME_FORMAT)
            if record.date_of_joining is not False:
                start = datetime.strptime(record.date_of_joining, '%Y-%m-%d')
                exp = relativedelta(curr, start)
                prob_period = str(exp.years) + ' year ' \
                    + str(exp.months) + ' month '
                res[record.id] = prob_period
        return res

    @api.multi
    def _confirm_date(self):
        res = {}
        for record in self:
            if (record.band.name == 'G1') or (record.band.name == 'G2') or (record.band.name == 'G3'):
                res[record.id] = record.date_of_joining
            else:
                DATETIME_FORMAT = "%Y-%m-%d"
                join_date = datetime.strptime(
                    record.date_of_joining, DATETIME_FORMAT)
                new_date = join_date + timedelta(days=180)
                res[record.id] = new_date
        return res

    @api.multi
    def _total_exp(self):
        res = {}
        for line in self:
            DATETIME_FORMAT = "%Y-%m-%d"
            res[line.id] = {}
            prob_period = ""
            year = 0
            month = 0
            current_date = fields.Date.context_today(
                self)
            if line.date_of_joining is not False:
                curr = datetime.strptime(current_date, DATETIME_FORMAT)
                start = datetime.strptime(
                    line.date_of_joining, DATETIME_FORMAT)
                exp = relativedelta(curr, start)
                month = month + exp.months
                year = year + exp.years
                for line_id in line.work_history_ids:
                    start = datetime.strptime(
                        line_id.start_date, DATETIME_FORMAT)
                    end = datetime.strptime(line_id.end_date, DATETIME_FORMAT)
                    exp = relativedelta(end, start)
                    year += exp.years
                    month += exp.months
                if month > 0:
                    year = year + month / 12
                if month > 12:
                    month = month % 12
                prob_period = str(year) + ' year ' \
                    + str(month) + ' month '
                res[line.id] = prob_period
        return res

    @api.multi
    def _check_uan_number(self, ids):
        import re
        for val in self.read(['uan_no']):
            if val['uan_no']:
                v = self.search([('uan_no', '=', val['uan_no'])])
                if len(v) > 1:
                    raise UserError(
                        _('UAN Number!, Please enter valid Uan Number'))
                    return False
        return True

    @api.multi
    def _check_group(self):
        res = {}
        for line in self:
            group = self.env['res.groups']
            group_ids = group.search([('name', '=', 'Officer')])
            if line.state == 'awaiting':
                for group_user in group_ids:
                    if self.env.uid == group_user.id:
                        res[line.id] = True
                    else:
                        res[line.id] = False
        return res

    @api.multi
    def button_active(self):
        for data in self:
            if data.last_working:
                data.active = False
            else:
                raise UserError(
                    _('Last Working Date!, Please enter last working date before deactivating the employee'))
        return True

    @api.multi
    def button_draft(self):

        self.write({'state': 'awaiting'})
        return True

    @api.multi
    def reset_of_draft(self):
        if self.state in ['awaiting_approval', 'cancelled', 'confirmed']:
            self.write({'state': 'draft'})
            return True

    @api.multi
    def button_awaiting(self, ids):
        vals = {}
        self.env.cr.execute("SELECT max(employee_id) from hr_employee;")
        max_employee_id = self.env.cr.fetchone()
        max_manager_id = self.env.cr.fetchone()
        if max_employee_id and max_employee_id[0] is not None:
            new_emp_id = int(max_employee_id[0]) + 1
        else:
            new_emp_id = 1000
        vals['employee_code'] = new_emp_id
        vals['manager_code'] = new_emp_id
        vals['state'] = 'confirmed'
        self.write(vals)
        return True

    @api.multi
    def button_cancel(self):
        self.write({'state': 'cancelled'})
        return True

    @api.onchange('user_id')
    def onchange_user(self):
        work_email = False
        dic_new = []
        if self.user_id:
            work_email = self.env['res.users'].browse(self.user_id).id.email
            group_ids = self.env['res.groups'].search(
                [('name', '=', 'HR Business Partner')])
            if group_ids.users:
                data = group_ids.users
                for data_id in data:
                    dic_new.append(data_id.id)
            return {'value': {'work_email': work_email, 'user_ids': dic_new}}

    def total_carry_over(self):
        carry_over = 0.0
        for employee in self:
            if employee.earned_leave_year:
                for earned_leave_year in employee.earned_leave_year[-1]:
                    carry_over = earned_leave_year.carry_over
        return carry_over

    @api.multi
    def button_allocation_leave(self):
        hr_holiday = self.env['hr.holidays']
        earned_leave_year_obj = self.env['earned.leave.year']
        calendar_year_obj = self.env['account.fiscalyear']
        current_date = datetime.now()

        old_date = current_date.replace(year=current_date.year - 1)
        calendar_year_obj_ids = calendar_year_obj.search(
            [('date_start', '<=', old_date),
             ('date_stop', '>=', old_date)], limit=1)

        cal = calendar_year_obj.browse(
            calendar_year_obj_ids)
        for employee in self:
            if employee.date_of_joining:
                join_day = datetime.strptime(
                    employee.date_of_joining, '%Y-%m-%d')
            else:
                raise UserError('Warning!', 'No Joining Date')
            earned_leave_year_ids = earned_leave_year_obj.search(
                [('employee_id', '=', employee.id)])
            end_date = cal.date_stop
            start_date = cal.date_start
            hr_holiday_utilize = hr_holiday.get_utilize_earned_leave(
                employee.id,
                cal.date_stop, start_date=start_date)
            if (cal.date_start <= employee.date_of_joining and
                    cal.date_stop >= employee.date_of_joining):
                start_date = employee.date_of_joining
                hr_holiday_allocate = hr_holiday.get_earned_leave(
                    employee.id, end_date,
                    start_date=start_date)
                if join_day.day > 15:
                    hr_holiday_allocate_leave = hr_holiday_allocate - 1
                else:
                    hr_holiday_allocate_leave = hr_holiday_allocate
            else:
                start_date = cal.date_start
                hr_holiday_allocate_leave = hr_holiday.get_earned_leave(
                    employee.id, end_date,
                    start_date=start_date)

            total_carry_over_last_year = self.total_carry_over()
            if total_carry_over_last_year == 0.0:
                carry_over_current_year = hr_holiday_allocate_leave - hr_holiday_utilize
                if carry_over_current_year > YEARLY_CARRY_OVER_POLICY:
                    carry_over = YEARLY_CARRY_OVER_POLICY
                else:
                    carry_over = carry_over_current_year
            else:
                current_utilize = YEARLY_CARRY_OVER_POLICY - hr_holiday_utilize
                carry_over = total_carry_over_last_year + current_utilize
                if carry_over > MAX_CARRY_OVER:
                    carry_over = MAX_CARRY_OVER
            if earned_leave_year_ids:
                for earned_leave_year in earned_leave_year_obj.browse(
                        earned_leave_year_ids):
                    if not earned_leave_year.fiscal_id.name == cal.name:
                        val = {
                            'fiscal_id': cal.id,
                            'employee_id': employee.id,
                            'carry_over': carry_over
                        }
                        earned_leave_year_obj.create(val)
            else:
                val = {
                    'fiscal_id': cal.id,
                    'employee_id': employee.id,
                    'carry_over': carry_over
                }
                earned_leave_year_obj.create(val)
        return True

    @api.onchange('has_pan', 'identification_id')
    def onchange_has_pan(self):
        for record in self:
            if record.has_pan is True:
                if record.identification_id:
                    v = self.search(
                        [('identification_id', '=', record.identification_id)])
                    if record.identification_id.isalpha() is True:
                        raise UserError(
                            _('Pan Number!, Please enter valid Pan Number'))
                    if record.identification_id.isdigit() is True:
                        raise UserError(
                            _('Pan Number!, Please enter valid Pan Number'))
                    if record.identification_id.isalnum() is True:
                        pass
                    if len(record.identification_id) < 10:
                        raise UserError(
                            _('Pan Number!, Please enter valid Pan Number'))
                    if len(v) > 1:
                        raise UserError(
                            _('Pan Number!, Please enter valid Pan Number'))

    _sql_constraints = [
        ('name_bank_account_id', 'unique(bank_account_id)',
         'Bank Number must be Unique!'),
    ]

    # @api.onchange('appraisal_plan', 'date_of_joining')
    # def onchange_appraisal_plan_id(self):
    #     for record in self:
    #         appraisal_date = False
    #         if record.date_of_joining:
    #             appraisal_date = datetime.strptime(str(
    #                 record.date_of_joining), '%Y-%m-%d') + relativedelta(months=int(record.appraisal_plan.month_first))
    #         record.appraisal_date = appraisal_date

    # @api.multi
    # def create_employee_appraisals(self):
    #     # questions_object = self.env['assessment.question']
    #     for emp in self:
    #         questions_list = []
    #         questions_list1 = []
    #         questions_list2 = []
    #         question_list = []
    #         question_list1 = []
    #         question_list2 = []
    #         for record in emp.appraisal_plan.assesment_question_ids:
    #             questions_list.append((0, 0, {
    #                 'question': record.question.id,
    #                 'max_score': record.score
    #             }))
    #             questions_list1.append((0, 0, {
    #                 'question': record.question.id,
    #                 'max_score': record.score
    #             }))
    #             questions_list2.append((0, 0, {
    #                 'question': record.question.id,
    #                 'max_score': record.score
    #             }))
    #         for record in emp.appraisal_plan.question_id:
    #             question_list.append((0, 0, {
    #                 'performance_parameter_id': record.performance_parameter_id.id,
    #                 'name': record.name.id,
    #                 'max_score': record.score
    #             }))
    #             question_list1.append((0, 0, {
    #                 'performance_parameter_id': record.performance_parameter_id.id,
    #                 'name': record.name.id,
    #                 'max_score': record.score
    #             }))
    #             question_list2.append((0, 0, {
    #                 'performance_parameter_id': record.performance_parameter_id.id,
    #                 'name': record.name.id,
    #                 'max_score': record.score
    #             }))

    #         appraisal_vals = {
    #             'employee_id': emp.id,
    #             'evaluation_plan_id': emp.appraisal_plan.id,
    #             'appraisal_date': emp.appraisal_date,
    #             'state': 'emp_assessment',
    #             'assesment_question_employee_ids': questions_list,
    #             'assesment_question_manager_ids': questions_list1,
    #             'assesment_question_ids': questions_list2,
    #             'question_employee_id': question_list,
    #             'question_manager_id': question_list1,
    #             'question_id': question_list2
    #         }
    #         self.env['employee.appraisals'].create(appraisal_vals)

    # @api.multi
    # def employee_appraisal_schedular(self):
    #     current_date = fields.Date.context_today(self)
    #     employees = self.env['hr.employee'].search(
    #         [('appraisal_date', '=', current_date)])
    #     employees.create_employee_appraisals()
    #     return True


class IrAttachment(models.Model):
    _inherit = 'ir.attachment'
    # _rec_name = 'tender_document_type'

    name = fields.Char('Name', required=False)
    hr_document_id = fields.Many2one('hr.employee')

    ir_attachment_hr = fields.Many2many(
        'ir.attachment', 'rel_hr_attachment',
        'hr_id', 'attachment_hr_form', 'Attachment')
    remarks = fields.Char('Remarks')


class LeaveType(models.Model):
    _description = "Leave Type"
    _name = "leave.type"

    name = fields.Char('Name')


class ResPartnerBank(models.Model):
    _inherit = "res.partner.bank"

    bank_name = fields.Char('Branch')
    bank_bic = fields.Char('IFSC Code', size=11)


class HrDepartment(models.Model):
    _inherit = "hr.department"

    manager_id = fields.Many2one('hr.employee', 'Department Head')


class HrRole(models.Model):
    _name = "hr.role"
    _description = "Role"

    name = fields.Char("Role", required=True)
    role_objective = fields.Char("Role Objective", required=True)
    dutie_responsiblity = fields.Text(
        "Duties & Responsibilities",
        required=True)
    qualification = fields.Text(
        "Qualifications",
        required=True)
    experience_require = fields.Char(
        "Experience Required",
        required=True)
    professional_skill = fields.Text(
        "Personal Skills & Qualities",
        required=True)
    state = fields.Selection(
        [('open', 'Recruitment Closed'),
         ('recruit', 'Recruitment in Progress')],
        string='Status',
        readonly=True, required=True,
        track_visibility='always',
        copy=False, default='open',
        help="By default 'Closed', set it to 'In Recruitment' if recruitment process is going on for this job position.")

    @api.multi
    def set_recruit(self):
        self.write({'state': 'recruit'})
        return True

    @api.multi
    def set_open(self):
        self.write({'state': 'open'})
        return True


# class HrRoleId(models.Model):
#     _name = "asset.type.id"

#     asset_type = fields.Many2one('asset.type', 'Asset Type')
#     role_id = fields.Many2one('hr.role', 'Role')


class ResourceResource(models.Model):
    _inherit = "resource.resource"
    _description = "Resource Detail"

    name = fields.Char("Employee Name", required=False)


class HrCategory(models.Model):
    _name = "hr.category"
    _description = "Resource Detail"

    name = fields.Char("Category", required=True)


class HrWorkHistory(models.Model):
    _description = "Work History"
    _name = "hr.work.history"

    hr_work_exp = fields.Many2one('hr.employee', "Work History")
    designation = fields.Many2one('hr.designation', "Designation")
    designation_id = fields.Char("Designation")
    previous_emp = fields.Char("Previous Employer")
    start_date = fields.Date("Start Date")
    end_date = fields.Date("End Date")
    pre_work_experience = fields.Char(
        compute='privious_exp',
        string="Work Experience(Years)")

    @api.multi
    def privious_exp(self):
        res = {}
        for record in self:
            DATETIME_FORMAT = "%Y-%m-%d"
            start = datetime.strptime(record.start_date, DATETIME_FORMAT)
            end = datetime.strptime(record.end_date, DATETIME_FORMAT)
            exp = relativedelta(end, start)
            prob_period = str(exp.years) + ' year ' \
                + str(exp.months) + ' month '
            res[record.id] = prob_period
        return res


class EductionalDetail(models.Model):
    _name = "educational.detail"
    _description = "Eductional"

    eduction_id = fields.Many2one('hr.employee', "ID")
    qualification = fields.Char("Qualification")
    name = fields.Char("Name Of Institution")
    period_attended = fields.Char("Period Attended(From-To)")
    course_studied = fields.Char("Course Studied")
    application_qualification_ids = fields.Char(
        "Certificates/Diploma/degree(If Applicable)")


class HrCostCenter(models.Model):
    _description = "Cost Centre"
    _name = "hr.cost.center"

    name = fields.Char("Name")


class HrEmergencyContact(models.Model):
    _description = "Emergency Detail"
    _name = "emgency.detail"

    emg_id = fields.Many2one('hr.employee', "Emergency Contact Detail")
    name = fields.Char(" Name")
    number = fields.Char("Number")
    relation = fields.Char("Relation")


class HrDesignation(models.Model):
    _description = "Designation"
    _name = "hr.designation"

    name = fields.Char("Name")


class HrSubDepartment(models.Model):
    _description = "Sub Department"
    _name = "hr.sub.department"

    name = fields.Char("Name")
    department_id = fields.Many2one('hr.department', string="Department")


class HrSourceType(models.Model):
    _description = "Hiring Source Type"
    _name = "source.type"

    name = fields.Char("Name")


class HrEmployeeOrganizationDetails(models.Model):
    _description = "Employee Previous Organization Details"
    _name = "hr.employee_organization_details"

    employee_id = fields.Many2one('hr.employee', '_id', invisible=True)
    organization_name = fields.Char('Organization Name', size=150)
    experience_from = fields.Date('From Date')
    experience_to = fields.Date('To Date')
    duration = fields.Char('Duration')
    reporting_head = fields.Char('Reporting Head Name', size=40)
    reporting_head_contact_no = fields.Char(
        'Reporting Head Contact no.', size=20)


class HrChildren(models.Model):
    _name = 'hr.employee.children'
    _description = 'HR Employee Children'

    name = fields.Char("Name", required=True)
    date_of_birth = fields.Date("Date of Birth", oldname='dob')
    employee_id = fields.Many2one('hr.employee', "Employee")
    gender = fields.Selection(
        [('male', 'Male'),
         ('female', 'Female')], string='Gender')


class HrEmployeeFamilyDetails(models.Model):
    _description = "Employee Family Details"
    _name = "hr.employee_family_details"

    employee_id = fields.Many2one('hr.employee', '_id', invisible=True)
    name = fields.Char('Name', size=40)
    gender = fields.Selection(
        [('Male', 'Male'),
         ('Female', 'Female')], 'Gender')
    dob = fields.Date('Date of Birth')
    relation = fields.Selection(
        [('Father', 'Father'),
         ('Mother', 'Mother'),
         ('Guardian', 'Guardian'),
         ('Spouse', 'Spouse'),
         ('Child', 'Child')], 'Relation With Employee')


class HrEmployeeServiceArea(models.Model):
    _description = "Employee Service Area"
    _name = "hr.employee.service.area"
    _order = 'create_date asc, id asc'

    tin_no = fields.Char('TIN No', size=64)
    dln_no = fields.Char('DLN No', size=64)
    cst_no = fields.Char('CST No', size=64)
    street = fields.Char('Street', size=64)
    street2 = fields.Char('Street2', size=64)
    city = fields.Char('City', size=64)
    state = fields.Many2one("res.country.state", 'State')
    zip = fields.Char('Zip', size=12)
    country_id = fields.Many2one('res.country', 'Country')
    fiscal_position_config = fields.One2many(
        'fiscal.position.config',
        'service_area_id',
        'Fiscal Position')
    country_state = fields.Many2one(
        'res.country.state',
        'State')
    service_area_code = fields.Char(
        'Service Area Code',
        size=50,
        required=True)
    name = fields.Char(
        'Service Area Name',
        size=100,
        required=True)
    center_head_id = fields.Many2one(
        'hr.employee',
        'Center Head')
    active = fields.Boolean('Active', default=True)
    service_user_ids = fields.Many2many(
        'res.users',
        'serv_user_rel',
        'pid',
        'cid',
        'Service Area Users')
    service_partner_ids = fields.Many2many(
        'res.partner',
        'serv_partner_rel',
        'parid',
        'chilid',
        'Service Area Users')
    service_supplier_ids = fields.Many2many(
        'res.partner',
        'serv_supplier_rel',
        'supplierid',
        'childid',
        'Service Area Users')
    # service_loc_ids = fields.Many2many(
    #     'stock.location',
    #     'serv_loc_rel',
    #     'locid',
    #     'chiid',
    #     'Service Area Users')
    # service_war_ids = fields.Many2many(
    #     'stock.warehouse',
    #     'serv_war_rel',
    #     'warid',
    #     'childid',
    #     'Service Area Users')
    # product_pricelist_ids = fields.Many2many(
    #     'product.pricelist',
    #     'product_price_rel',
    #     'prodid',
    #     'priceid',
    #     'Service Area Pricelist')

    operations_ids = fields.One2many(
        'hr.expense.category',
        'operations_id',
        'Operations Expense Categories')
    general_ids = fields.One2many(
        'hr.expense.category',
        'general_id',
        'General Expense Categories')

#     _sql_constraints = [('unique_service_area_code',
#                          'UNIQUE (service_area_code)', 'Service Area Code has to be unique')]


class HrExpensecategory(models.Model):
    _description = "HR Expense Category"
    _name = "hr.expense.category"

    nurse_id = fields.Many2one('hr.employee.service.area', 'Nurse ID')
    physio_id = fields.Many2one('hr.employee.service.area', 'Physio ID')
    phlebo_id = fields.Many2one('hr.employee.service.area', 'Phlebo ID')
    delivery_id = fields.Many2one(
        'hr.employee.service.area', 'Delivery Boy ID')
    bd_id = fields.Many2one('hr.employee.service.area',
                            'Business Development ID')
    pharmacist_id = fields.Many2one(
        'hr.employee.service.area', 'Pharmacist ID')
    operations_id = fields.Many2one(
        'hr.employee.service.area', 'Operations ID')
    general_id = fields.Many2one('hr.employee.service.area', 'General ID')
    category_id = fields.Many2one('expense.category', 'Category')
    # product_id = fields.Many2one('product.product', 'Category')
    amount = fields.Float('Amount', default=1.0)
    employee_type = fields.Many2one('emp.type', 'Employee Type')
    # product_uom = fields.Many2one('product.uom', 'Unit of Measure')


class ExpenseCategory(models.Model):
    _description = "Expense Category"
    _name = "expense.category"

    name = fields.Char('Category Name', required=True)


class EmpType(models.Model):
    _description = "Employee Type"
    _name = "emp.type"

    name = fields.Char('Name', required=True)


class HrTag(models.Model):
    _name = "hr.tag"
    _description = "Tag"

    name = fields.Char('Name')


class HrSource(models.Model):
    _name = "hr.source"
    _description = "Source"

    name = fields.Char('Name')


class UserUpdate(models.TransientModel):
    _name = 'user.update'

    file = fields.Binary('Expenses created', required=True)


class EducationQualifiaction(models.Model):

    _name = 'education.qualifiaction'

    qualification = fields.Many2one('hr.recruitment.degree', "Qualification")
    name = fields.Char('Name of Institution')
    period_attended = fields.Char('Period Attended (From-To)')
    course_studied = fields.Char('Course Studied')
    certificate = fields.Many2many(
        'ir.attachment', 'rel_qualification_certificate',
        'certificate', 'attachment_certificate', 'Attachment')
    period_from = fields.Date("Period Attended From")
    period_to = fields.Date('Period Attended To')
    hr_applicant_id = fields.Many2one('hr.employee')

    @api.onchange('period_to')
    def onchange_period_to(self):
        if (self.period_from and self.period_to) and (self.period_from > self.period_to):
            raise UserError(
                _('The start date must be anterior to the end date.'))


class EducationHrCertifiaction(models.Model):

    _name = 'education.hr.certification'

    certification = fields.Char('Certification')
    name = fields.Char('Name of Institution')
    period_attended = fields.Char('Period Attended (From-To)')
    certi_description = fields.Char('Certificate Description')
    attachment = fields.Many2many(
        'ir.attachment', 'rel_qualification_certificatess',
        'certificate', 'attachment_certificatess', 'Attachment')
    period_from = fields.Date("Period Attended From")
    period_to = fields.Date('Period Attended To')
    hr_applicant_id = fields.Many2one('hr.employee')

    @api.onchange('period_to')
    def onchange_period_to(self):
        if (self.period_from and self.period_to) and (self.period_from > self.period_to):
            raise UserError(
                _('The start date must be anterior to the end date.'))


class EducationComputerSkills(models.Model):

    _name = 'education.computerskills'

    skill = fields.Char('Skill')
    skill_description = fields.Char('Skill Description')
    aquired_date = fields.Date('Skill Aquired Date')
    certificate = fields.Many2many(
        'ir.attachment', 'rel_certificate_att',
        'certificate', 'attachment_certificate_id', 'Attachment')
    level = fields.Selection([
        ('low', 'Low'),
        ('medium', 'Medium'),
        ('high', 'High')], 'Level of Proficiency')
    hr_applicant_skill_id = fields.Many2one('hr.employee')


class LanguageConfig(models.Model):

    _name = 'language.config'

    name = fields.Char('Name of Language')


class KnownLanguage(models.Model):

    _name = 'known.language'

    hr_applicant_lang_id = fields.Many2one('hr.employee')
    language_known = fields.Many2one(
        'language.config', string="Language Known")
    is_read = fields.Boolean('Read')
    is_write = fields.Boolean('Write')
    is_speak = fields.Boolean('Speak')


class Employeehistory(models.Model):

    _name = 'employee.history'

    company_id = fields.Char('Company')
    start_date = fields.Date('Start Date')
    end_date = fields.Date('End Date')
    salary = fields.Float('Last Salary Drawn')
    position = fields.Char('Designation')
    reason = fields.Char('Reasons for Leaving')
    hr_applicant_history_id = fields.Many2one('hr.employee')

    @api.onchange('end_date')
    def onchange_period_to(self):
        if (self.start_date and self.end_date) and (self.start_date > self.end_date):
            raise UserError(
                _('The start date must be anterior to the end date.'))


class DomainConfig(models.Model):
    _name = 'domain.config'

    name = fields.Char('Domain')


class EmployeeDomain(models.Model):

    _name = 'employee.domain'

    hr_applicant_id = fields.Many2one('hr.employee')
    name = fields.Many2one('domain.config', 'Domain')
    experience = fields.Float('Experience (Years)')


class BusinessUnit(models.Model):
    _name = "business.unit"

    name = fields.Char('Unit Name')


class FiscalPositionConfig(models.Model):
    _name = "fiscal.position.config"

    service_area_id = fields.Many2one(
        'hr.employee.service.area', 'Service Area')
    customer_type = fields.Selection(
        [('b2b', 'B2B'),
         ('b2c', 'B2C')], 'Customer Type')
    customer_location = fields.Selection(
        [('local', 'Local'),
         ('other', 'Other')], 'Customer Location')
    fiscal_position_id = fields.Many2one(
        'account.fiscal.position', 'Fiscal Position')


class ResPartner(models.Model):
    _inherit = 'res.partner'

    @api.model
    def create(self, vals):
        res = super(ResPartner, self).create(vals)
        return res


class HrEmpGrade(models.Model):
    _description = "Employee Grade"
    _name = "emp.grade"

    name = fields.Char("Grade Name")


class ResCompany(models.Model):
    _inherit = "res.company"
    _description = 'Companies'

    company_code = fields.Char('Company Code', required=True)
