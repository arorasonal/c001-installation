# -*- coding: utf-8 -*-

import xlwt
from datetime import datetime
# from odoo.report import report_sxw
from odoo.addons.report_xls.report_xls import report_xls
from odoo.addons.report_xls.utils import rowcol_to_cell, _render
from odoo.tools.translate import translate, _
from odoo import api, fields, models, tools
import logging
_logger = logging.getLogger(__name__)

_ir_translation_name = 'employee.bank.report.xls'


class EmployeeBankTemplateXlsParser(models.AbstractModel):

    @api.model_cr
    def __init__(self, name):

        super(EmployeeBankTemplateXlsParser, self).__init__(name)
        prod_obj = self.env['hr.employee']
        self.context = self.env.context
        wanted_list = prod_obj._value_bank_report_xls_fields()
        template_changes = prod_obj._report_bank_xls_template()
        self.localcontext.update({
            'datetime': datetime,
            'wanted_list': wanted_list,
            'template_changes': template_changes,
            '_': self._,
        })

    def _(self, src):
        lang = self.context.get('lang', 'en_US')
        return translate(self.cr, _ir_translation_name, 'report', lang, src) \
            or src


class employee_template_xls(report_xls):

    def __init__(self, name, table, rml=False, parser=False, header=True,
                 store=False):
        super(employee_template_xls, self).__init__(
            name, table, rml, parser, header, store)

        # Cell Styles
        _xs = self.xls_styles
        # header
        rh_cell_format = _xs['bold'] + _xs['fill'] + _xs['borders_all']
        self.rh_cell_style = xlwt.easyxf(rh_cell_format)
        self.rh_cell_style_center = xlwt.easyxf(rh_cell_format + _xs['center'])
        self.rh_cell_style_right = xlwt.easyxf(rh_cell_format + _xs['right'])
        # lines
        aml_cell_format = _xs['borders_all']
        self.aml_cell_style = xlwt.easyxf(aml_cell_format)
        self.aml_cell_style_center = xlwt.easyxf(
            aml_cell_format + _xs['center'])
        self.aml_cell_style_date = xlwt.easyxf(
            aml_cell_format + _xs['left'],
            num_format_str=report_xls.date_format)
        self.aml_cell_style_decimal = xlwt.easyxf(
            aml_cell_format + _xs['right'],
            num_format_str=report_xls.decimal_format)
        # totals
        rt_cell_format = _xs['bold'] + _xs['fill'] + _xs['borders_all']
        self.rt_cell_style = xlwt.easyxf(rt_cell_format)
        self.rt_cell_style_right = xlwt.easyxf(rt_cell_format + _xs['right'])
        self.rt_cell_style_decimal = xlwt.easyxf(
            rt_cell_format + _xs['right'],
            num_format_str=report_xls.decimal_format)

        # XLS Template
        self.col_specs_template = {
            'employee_id': {
                'header': [1, 20, 'text', _render("_('Employee Code')")],
                'lines': [1, 0, 'text', _render("line.employee_id or ''")],
            },
            'name': {
                'header': [1, 20, 'text', _render("_('Employee Name')")],
                'lines': [1, 0, 'text', _render("line.name or ''")],
            },
            'active': {
                'header': [1, 20, 'text', _render("_('Active')")],
                'lines': [1, 0, 'bool', _render("line.active or ''")],
            },
            'joining': {
                'header': [1, 20, 'text', _render("_('Date of Joining')")],
                'lines': [1, 0, 'text',
                          _render("line.date_of_joining")],
            },
            'last_working': {
                'header': [1, 13, 'text', _render("_('Last Working Date')")],
                'lines': [1, 0, 'text', _render("line.last_working or ''")],
            },
            'cost_center': {
                'header': [1, 20, 'text', _render("_('Cost Centre')")],
                'lines': [1, 0, 'text', _render("line.cost_center.name or ''")],
            },
            'job_id': {
                'header': [1, 20, 'text', _render("_('Designation')")],
                'lines': [1, 0, 'text', _render("line.job_id.name or ''")],
            },
            'department_id': {
                'header': [1, 12, 'text', _render("_('Department')")],
                'lines': [1, 0, 'text', _render("line.department_id.name or ''")],
            },
            'band': {
                'header': [1, 12, 'text', _render("_('band')")],
                'lines': [1, 0, 'text', _render("line.band.name or ''")],
            },
            'parent_id': {
                'header': [1, 13, 'text', _render("_('Manager Name')")],
                'lines': [1, 0, 'text', _render("line.parent_id.name or ''")],
            },
            'work_email': {
                'header': [1, 13, 'text', _render("_('Work E mail')")],
                'lines': [1, 0, 'text', _render("line.work_email or ''")],
            },
            'bank_account_id': {
                'header': [1, 13, 'text', _render("_('Bank Name')")],
                'lines': [1, 0, 'text', _render("line.bank_account_id.bank.name or ''")],
            },
            'bank_account': {
                'header': [1, 13, 'text', _render("_('Account Number')")],
                'lines': [1, 0, 'text', _render("line.bank_account_id.acc_number or ''")],
            },
            'bank_account_ifsc': {
                'header': [1, 13, 'text', _render("_('IFSC')")],
                'lines': [1, 0, 'text', _render("line.bank_account_id.bank_bic or ''")],
            },
        }

    def generate_xls_report(self, _p, _xs, data, objects, wb):

        wanted_list = _p.wanted_list
        self.col_specs_template.update(_p.template_changes)
        _ = _p._

        date = datetime.today().date()
        report_name = _("Bank Detail Report")
        ws = wb.add_sheet(report_name[:31])
        ws.panes_frozen = True
        ws.remove_splits = True
        ws.portrait = 0  # Landscape
        ws.fit_width_to_pages = 1
        row_pos = 1

        # set print header/footer
        ws.header_str = self.xls_headers['standard']

        # Title
        cell_style = xlwt.easyxf(_xs['xls_title'])
        c_specs = [
            ('report_name', 2, 0, 'text', report_name),

        ]

        row_data = self.xls_row_template(c_specs, ['report_name'])
        row_pos = self.xls_write_row(
            ws, row_pos, row_data, row_style=cell_style)
        row_pos += 1

        # Column headers
        c_specs = map(lambda x: self.render(
            x, self.col_specs_template, 'header', render_space={'_': _p._}),
            wanted_list)
        row_data = self.xls_row_template(c_specs, [x[0] for x in c_specs])
        row_pos = self.xls_write_row(
            ws, row_pos, row_data, row_style=self.rh_cell_style,
            set_column_size=True)
        ws.set_horz_split_pos(row_pos)
        # account move lines
        for line in objects:
            #            debit_cell = rowcol_to_cell(row_pos, debit_pos)
            #            credit_cell = rowcol_to_cell(row_pos, credit_pos)
            #            bal_formula = debit_cell + '-' + credit_cell
            #            _logger.debug('dummy call - %s', bal_formula)
            c_specs = map(
                lambda x: self.render(x, self.col_specs_template, 'lines'),
                wanted_list)
            row_data = self.xls_row_template(c_specs, [x[0] for x in c_specs])
            row_pos = self.xls_write_row(
                ws, row_pos, row_data, row_style=self.aml_cell_style_center)

        # Totals
        '''aml_cnt = len(objects)
        stock_received_qt_start = rowcol_to_cell(row_pos - aml_cnt, stock_received_qty_pos)
        stock_received_qt_stop = rowcol_to_cell(row_pos - 1, stock_received_qty_pos)
        stock_received_formula = 'SUM(%s:%s)' % (stock_received_qt_start, stock_received_qt_stop)
        _logger.debug('dummy call - %s', stock_received_formula)
        stock_opening_qt_start = rowcol_to_cell(row_pos - aml_cnt, stock_opening_qt_pos)
        stock_opening_qt_stop = rowcol_to_cell(row_pos - 1, stock_opening_qt_pos)
        stock_opening_qt_formula = 'SUM(%s:%s)' % (stock_opening_qt_start, stock_opening_qt_stop)
        _logger.debug('dummy call - %s', stock_opening_qt_formula)'''

employee_template_xls('report.employee.bank.report.xls',
                      'hr.employee',
                      parser=EmployeeBankTemplateXlsParser)
