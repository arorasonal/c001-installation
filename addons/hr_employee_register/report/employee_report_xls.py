# -*- coding: utf-8 -*-

import xlwt
from datetime import datetime
from odoo import api, fields, models, tools
# from openerp.report import report_sxw
from odoo.addons.report_xls.report_xls import report_xls
from odoo.addons.report_xls.utils import rowcol_to_cell, _render
from odoo.tools.translate import translate, _
import logging
_logger = logging.getLogger(__name__)

_ir_translation_name = 'employee.list.xls'


class EmployeeTemplateXlsParser(models.AbstractModel):

    @api.model_cr
    def __init__(self, name):
        super(EmployeeTemplateXlsParser, self).__init__(name)
        prod_obj = self.pool.get('hr.employee')
        self.context = context
        wanted_list = prod_obj._value_report_xls_fields()
	
        template_changes = prod_obj._report_xls_template()
        self.localcontext.update({
            'datetime': datetime,
            'wanted_list': wanted_list,
            'template_changes': template_changes,
            '_': self._,
        })

    def _(self, src):
        lang = self.context.get('lang', 'en_US')
        return translate(self.cr, _ir_translation_name, 'report', lang, src) \
            or src


class employee_template_xls(report_xls):

    @api.model_cr
    def __init__(self, name, table, rml=False, parser=False, header=True,
                 store=False):
        super(employee_template_xls, self).__init__(
            name, table, rml, parser, header, store)

        # Cell Styles
        _xs = self.xls_styles
        # header
        rh_cell_format = _xs['bold'] + _xs['fill'] + _xs['borders_all']
        self.rh_cell_style = xlwt.easyxf(rh_cell_format)
        self.rh_cell_style_center = xlwt.easyxf(rh_cell_format + _xs['center'])
        self.rh_cell_style_right = xlwt.easyxf(rh_cell_format + _xs['right'])
        # lines
        aml_cell_format = _xs['borders_all']
        self.aml_cell_style = xlwt.easyxf(aml_cell_format)
        self.aml_cell_style_center = xlwt.easyxf(
            aml_cell_format + _xs['center'])
        self.aml_cell_style_date = xlwt.easyxf(
            aml_cell_format + _xs['left'],
            num_format_str=report_xls.date_format)
        self.aml_cell_style_decimal = xlwt.easyxf(
            aml_cell_format + _xs['right'],
            num_format_str=report_xls.decimal_format)
        # totals
        rt_cell_format = _xs['bold'] + _xs['fill'] + _xs['borders_all']
        self.rt_cell_style = xlwt.easyxf(rt_cell_format)
        self.rt_cell_style_right = xlwt.easyxf(rt_cell_format + _xs['right'])
        self.rt_cell_style_decimal = xlwt.easyxf(
            rt_cell_format + _xs['right'],
            num_format_str=report_xls.decimal_format)

        # XLS Template
        self.col_specs_template = {
            'employee_id': {
                'header': [1, 20, 'text', _render("_('Employee Code')")],
                'lines': [1, 0, 'text', _render("line.employee_id or ''")],
                },
            'name': {
                'header': [1, 20, 'text', _render("_('Employee Name')")],
                'lines': [1, 0, 'text', _render("line.name or ''")],
                },
            'active': {
                'header': [1, 20, 'text', _render("_('Active')")],
                'lines': [1, 0, 'bool', _render("line.active or ''")],
                },
            'joining': {
                'header': [1, 20, 'text', _render("_('Date of Joining')")],
                'lines': [1, 0, 'text',
                          _render("line.date_of_joining")],
                },
            'location': {
                'header': [1, 20, 'text', _render("_('Service Area')")],
                'lines': [1, 0, 'text', _render("line.office_location.name or ''")],
                },
            'cost_center': {
                'header': [1, 20, 'text', _render("_('Cost Centre')")],
                'lines': [1, 0, 'text', _render("line.cost_center.name or ''")],
                },
            'job_id': {
                'header': [1, 20, 'text', _render("_('Designation')")],
                'lines': [1, 0, 'text', _render("line.job_id.name or ''")],
                },
            'department_id': {
                'header': [1, 12, 'text', _render("_('Department')")],
                'lines': [1, 0, 'text', _render("line.department_id.name or ''")],
                },
            'sub_department': {
                'header': [1, 13, 'text', _render("_('Sub Department')")],
                'lines': [1, 0, 'text', _render("line.sub_department.name or ''")],
                },
            'employee_type': {
                'header': [1, 13, 'text', _render("_('Category')")],
                'lines': [1, 0, 'text', _render("line.employee_type.name or ''")],
                },
                
	        'band': {
                'header': [1, 12, 'text', _render("_('Band')")],
                'lines': [1, 0, 'text', _render("line.band.name or ''")],
                },
            'band_designation': {
                'header': [1, 13, 'text', _render("_('Band Designations')")],
                'lines': [1, 0, 'text', _render("line.band_designation.name or ''")],
                },
            'source_hiring_ids': {
                'header': [1, 13, 'text', _render("_('Hiring Source Type')")],
                'lines': [1, 0, 'text', _render("line.source_hiring_ids.name or ''")],
                },
	        'source_detail_ids': {
                'header': [1, 12, 'text', _render("_('Source Detail')")],
                'lines': [1, 0, 'text', _render("line.source_detail_ids.name or ''")],
                },
            #'manager_code': {
             #   'header': [1, 13, 'text', _render("_('Manager Code')")],
              #  'lines': [1, 0, 'text', _render("line.manager_code.name or ''")],
               # },
            'parent_id': {
                'header': [1, 13, 'text', _render("_('Manager Name')")],
                'lines': [1, 0, 'text', _render("line.parent_id.name or ''")],
                },
	        'previous_employer': {
                'header': [1, 13, 'text', _render("_('Previous Employer')")],
                'lines': [1, 0, 'text', _render("line.work_history_ids[0].previous_emp if len(line.work_history_ids) > 0 else ''")],
                },
            'start_date': {
                'header': [1, 13, 'text', _render("_('Start Date')")],
                'lines': [1, 0, 'text', _render("line.work_history_ids[0].start_date if len(line.work_history_ids) > 0 else ''")],
                },
	    'end_date': {
                'header': [1, 13, 'text', _render("_('End Date')")],
                'lines': [1, 0, 'text', _render("line.work_history_ids[0].end_date if len(line.work_history_ids) > 0 else ''")],
                },
	    'designation': {
                'header': [1, 13, 'text', _render("_('Designation')")],
                'lines': [1, 0, 'text', _render("line.work_history_ids[0].designation.name if len(line.work_history_ids) > 0 else ''")],
                },
	    'previous_employer1': {
                'header': [1, 13, 'text', _render("_('Previous Employer')")],
                'lines': [1, 0, 'text', _render("line.work_history_ids[1].previous_emp if len(line.work_history_ids) > 1 else ''")],
                },
            'start_date1': {
                'header': [1, 13, 'text', _render("_('Start Date')")],
                'lines': [1, 0, 'text', _render("line.work_history_ids[1].start_date if len(line.work_history_ids) > 1 else ''")],
                },
	    'end_date1': {
                'header': [1, 13, 'text', _render("_('End Date')")],
                'lines': [1, 0, 'text', _render("line.work_history_ids[1].end_date if len(line.work_history_ids) > 1 else ''")],
                },
	    'designation1': {
                'header': [1, 13, 'text', _render("_('Designation')")],
                'lines': [1, 0, 'text', _render("line.work_history_ids[1].designation.name if len(line.work_history_ids) > 1 else ''")],
                },
	    'previous_employer2': {
                'header': [1, 13, 'text', _render("_('Previous Employer')")],
                'lines': [1, 0, 'text', _render("line.work_history_ids[2].previous_emp if len(line.work_history_ids) > 2 else ''")],
                },
            'start_date2': {
                'header': [1, 13, 'text', _render("_('Start Date')")],
                'lines': [1, 0, 'text', _render("line.work_history_ids[2].start_date if len(line.work_history_ids) > 2 else ''")],
                },
	    'end_date2': {
                'header': [1, 13, 'text', _render("_('End Date')")],
                'lines': [1, 0, 'text', _render("line.work_history_ids[2].end_date if len(line.work_history_ids) > 2 else ''")],
                },
	    'designation2': {
                'header': [1, 13, 'text', _render("_('Designation')")],
                'lines': [1, 0, 'text', _render("line.work_history_ids[2].designation.name if len(line.work_history_ids) > 2 else ''")],
                },
	    'work_exp': {
                'header': [1, 13, 'text', _render("_('Previous Work Experience')")],
                'lines': [1, 0, 'text', _render("line.total or ''")],
                },
	        #'curr_work_experience': {
               # 'header': [1, 13, 'text', _render("_('Current Work Experience')")],
               # 'lines': [1, 0, 'text', _render("line.curr_work_experience or ''")],
               # },
	        
	    'birthday': {
                'header': [1, 13, 'text', _render("_('Date of Birth')")],
                'lines': [1, 0, 'text', _render("line.birthday or ''")],
                },
	    'work_phone': {
                'header': [1, 13, 'text', _render("_('Work Contact No.')")],
                'lines': [1, 0, 'text', _render("line.work_phone or ''")],
                },
            'mobile_phone': {
                'header': [1, 13, 'text', _render("_('Personal Contact No.')")],
                'lines': [1, 0, 'text', _render("line.mobile_phone or ''")],
                },
	    'emer_cont': {
                'header': [1, 13, 'text', _render("_('Emergency Contact Name')")],
                'lines': [1, 0, 'text', _render("line.name or ''")],
                },
	    'emer_name': {
                'header': [1, 13, 'text', _render("_('Emergency Contact No.')")],
                'lines': [1, 0, 'text', _render("line.name or ''")],
                },
	    'marital': {
                'header': [1, 13, 'text', _render("_('Marital Status')")],
                'lines': [1, 0, 'text', _render("line.marital or ''")],
                },
            'gender': {
                'header': [1, 13, 'text', _render("_('Gender')")],
                'lines': [1, 0, 'text', _render("line.gender or ''")],
                },
	    'work_email': {
                'header': [1, 13, 'text', _render("_('Work E mail')")],
                'lines': [1, 0, 'text', _render("line.work_email or ''")],
                },
	    'personal_email': {
                'header': [1, 13, 'text', _render("_('Personal E mail')")],
                'lines': [1, 0, 'text', _render("line.personal_email or ''")],
                },
	    'fam_father': {
                'header': [1, 13, 'text', _render("_('Fathers Name')")],
                'lines': [1, 0, 'text', _render("line.fam_father or ''")],
                },
	    'fam_father_date_of_birth': {
                'header': [1, 13, 'text', _render("_('Father DOB')")],
                'lines': [1, 0, 'text', _render("line.fam_father_date_of_birth or ''")],
                },
	    'fam_mother': {
                'header': [1, 13, 'text', _render("_('Mother Name')")],
                'lines': [1, 0, 'text', _render("line.fam_mother or ''")],
                },
	    'fam_mother_date_of_birth': {
                'header': [1, 13, 'text', _render("_('Mother DOB')")],
                'lines': [1, 0, 'text', _render("line.fam_mother_date_of_birth or ''")],
                },
	    'date_of_marriage': {
                'header': [1, 13, 'text', _render("_('Date of Marriage')")],
                'lines': [1, 0, 'text', _render("line.date_of_marriage or ''")],
                },
	    'fam_spouse': {
                'header': [1, 13, 'text', _render("_('Spouse Name')")],
                'lines': [1, 0, 'text', _render("line.fam_spouse or ''")],
                },
	    'fam_dob': {
                'header': [1, 13, 'text', _render("_('Spouse DOB')")],
                'lines': [1, 0, 'text', _render("line.fam_dob or ''")],
                },
	    'child_1': {
                'header': [1, 13, 'text', _render("_('Child - I Name')")],
                'lines': [1, 0, 'text', _render("line.fam_children_ids[0].name if len(line.fam_children_ids) > 0 else ''")],
                },
	    'child_gender': {
                'header': [1, 13, 'text', _render("_('Child - I Gender')")],
                'lines': [1, 0, 'text', _render("line.fam_children_ids[0].gender if len(line.fam_children_ids) > 0 else ''")],
                #'lines': [1, 0, 'list', _render("[x.gender[0] for x in line.fam_children_ids] or ''")],
                },
	    'child_dob': {
                'header': [1, 13, 'text', _render("_('Child - I DOB')")],
                'lines': [1, 0, 'text', _render("line.fam_children_ids[0].date_of_birth if len(line.fam_children_ids) > 0 else ''")],
                },
	    'child_2': {
                'header': [1, 13, 'text', _render("_('Child - II Name')")],
                'lines': [1, 0, 'text', _render("line.fam_children_ids[1].name if len(line.fam_children_ids) > 1 else ''")],
                },
	    'child_gender2': {
                'header': [1, 13, 'text', _render("_('Child - II Gender')")],
                'lines': [1, 0, 'text', _render("line.fam_children_ids[1].gender if len(line.fam_children_ids) > 1 else ''")],
                #'lines': [1, 0, 'list', _render("[x.gender[0] for x in line.fam_children_ids] or ''")],
                },
	    'child_dob2': {
                'header': [1, 13, 'text', _render("_('Child - II DOB')")],
                'lines': [1, 0, 'text', _render("line.fam_children_ids[1].date_of_birth if len(line.fam_children_ids) > 1 else ''")],
                },
	    'identification_id': {
                'header': [1, 13, 'text', _render("_('PAN Card Number')")],
                'lines': [1, 0, 'text', _render("line.identification_id or ''")],
                },
        'esic_no': {
                'header': [1, 13, 'text', _render("_('ESIC Number')")],
                'lines': [1, 0, 'text', _render("line.esic_no or ''")],
                },        
                
	    'blood_group': {
                'header': [1, 13, 'text', _render("_('Blood Group')")],
                'lines': [1, 0, 'text', _render("line.blood_group or ''")],
                },
	    'pff_no': {
                'header': [1, 13, 'text', _render("_('PF No')")],
                'lines': [1, 0, 'text', _render("line.pff_no or ''")],
                },
	    'uan_no': {
                'header': [1, 13, 'text', _render("_('UAN No.')")],
                'lines': [1, 0, 'text', _render("line.uan_no or ''")],
                },
	    'address_home_id': {
                'header': [1, 13, 'text', _render("_('Permanent Address')")],
                'lines': [1, 0, 'text', _render("line.address_home_id.name or ''")],
                },
	    'corres_home_id': {
                'header': [1, 13, 'text', _render("_('Correspondance Address')")],
                'lines': [1, 0, 'text', _render("line.corres_home_id.name or ''")],
                },
	    'degree': {
                'header': [1, 13, 'text', _render("_('Degree')")],
                'lines': [1, 0, 'text', _render("line.educatonal_detail_ids[0].degree if len(line.educatonal_detail_ids) > 0 else ''")],
                },
	        'institute_name': {
                'header': [1, 13, 'text', _render("_('Institute Name')")],
                'lines': [1, 0, 'text', _render("line.educatonal_detail_ids[0].institute if len(line.educatonal_detail_ids) > 0 else ''")],
                },
	        'passing_date': {
                'header': [1, 13, 'text', _render("_('Passing Date')")],
                'lines': [1, 0, 'text', _render("line.educatonal_detail_ids[0].pass_date if len(line.educatonal_detail_ids) > 0 else ''")],
                },
            'degree1': {
                'header': [1, 13, 'text', _render("_('Degree')")],
                'lines': [1, 0, 'text', _render("line.educatonal_detail_ids[1].degree if len(line.educatonal_detail_ids) > 1 else ''")],
                },
	    'institute_name1': {
                'header': [1, 13, 'text', _render("_('Institute Name')")],
                'lines': [1, 0, 'text', _render("line.educatonal_detail_ids[1].institute if len(line.educatonal_detail_ids) > 1 else ''")],
                },
	    'passing_date1': {
                'header': [1, 13, 'text', _render("_('Passing Date')")],
                'lines': [1, 0, 'text', _render("line.educatonal_detail_ids[1].pass_date if len(line.educatonal_detail_ids) > 1 else ''")],
                },
            'registration': {
                'header': [1, 13, 'text', _render("_('Resignation Date')")],
                'lines': [1, 0, 'text', _render("line.registration or ''")],
                },
            'last_working': {
                'header': [1, 13, 'text', _render("_('Last Working Date')")],
                'lines': [1, 0, 'text', _render("line.last_working or ''")],
                },
	    'exit_reason': {
                'header': [1, 13, 'text', _render("_('Exit Reason')")],
                'lines': [1, 0, 'text', _render("line.exit_reason or ''")],
                },
	    'exit_type': {
                'header': [1, 13, 'text', _render("_('Exit Type')")],
                'lines': [1, 0, 'text', _render("line.exit_type_id.name or ''")],
                },


	 
           
        }
    def generate_xls_report(self, _p, _xs, data, objects, wb):

        wanted_list = _p.wanted_list
        self.col_specs_template.update(_p.template_changes)
        _ = _p._

        
        report_name = _("Head Count Report")
        ws = wb.add_sheet(report_name[:31])
        ws.panes_frozen = True
        ws.remove_splits = True
        ws.portrait = 0  # Landscape
        ws.fit_width_to_pages = 1
        row_pos = 1

        # set print header/footer
        ws.header_str = self.xls_headers['standard']
        ws.footer_str = self.xls_footers['standard']

        # Title
        cell_style = xlwt.easyxf(_xs['xls_title'])
        c_specs = [
            ('report_name', 2, 0, 'text', report_name),
        ]
        row_data = self.xls_row_template(c_specs, ['report_name'])
        row_pos = self.xls_write_row(
            ws, row_pos, row_data, row_style=cell_style)
        row_pos += 1

        # Column headers
        c_specs = map(lambda x: self.render(
            x, self.col_specs_template, 'header', render_space={'_': _p._}),
            wanted_list)
        row_data = self.xls_row_template(c_specs, [x[0] for x in c_specs])
        row_pos = self.xls_write_row(
            ws, row_pos, row_data, row_style=self.rh_cell_style,
            set_column_size=True)
        ws.set_horz_split_pos(row_pos)
	    
        # account move lines
        for line in objects:
#            debit_cell = rowcol_to_cell(row_pos, debit_pos)
#            credit_cell = rowcol_to_cell(row_pos, credit_pos)
#            bal_formula = debit_cell + '-' + credit_cell
#            _logger.debug('dummy call - %s', bal_formula)
            c_specs = map(
                lambda x: self.render(x, self.col_specs_template, 'lines'),
                wanted_list)
            row_data = self.xls_row_template(c_specs, [x[0] for x in c_specs])
            row_pos = self.xls_write_row(
                ws, row_pos, row_data, row_style=self.aml_cell_style_center)


        # Totals
        '''aml_cnt = len(objects)
        stock_received_qt_start = rowcol_to_cell(row_pos - aml_cnt, stock_received_qty_pos)
        stock_received_qt_stop = rowcol_to_cell(row_pos - 1, stock_received_qty_pos)
        stock_received_formula = 'SUM(%s:%s)' % (stock_received_qt_start, stock_received_qt_stop)
        _logger.debug('dummy call - %s', stock_received_formula)
        stock_opening_qt_start = rowcol_to_cell(row_pos - aml_cnt, stock_opening_qt_pos)
        stock_opening_qt_stop = rowcol_to_cell(row_pos - 1, stock_opening_qt_pos)
        stock_opening_qt_formula = 'SUM(%s:%s)' % (stock_opening_qt_start, stock_opening_qt_stop)
        _logger.debug('dummy call - %s', stock_opening_qt_formula)'''

employee_template_xls('report.employee.list.xls',
              'hr.employee',
              parser=EmployeeTemplateXlsParser)
