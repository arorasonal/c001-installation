# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2010 Tiny SPRL (<http://tiny.be>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

{
    'name': 'Indian Employee Register',
    'version': '1.0',
    'sequence': 21,
    'summary': 'Jobs, Departments, Employees Details',
    'description': """
Human Resources Management Inherited
====================================

Employee Register Customize for Indian.

You can manage:
---------------
* Employees and hierarchies : You can define your employee with User and display hierarchies
* HR Departments
* HR Jobs
    """,
    'depends': [
        'base_hr',
        'hr',
        'hr_timesheet',
        'hr_attendance',
        'hr_expense',
        'hr_recruitment',
    ],
    'data': [
        'security/base_security.xml',
        'security/ir.model.access.csv',
        'security/ir_rule.xml',
        'views/sequence.xml',
        'views/hr_department_view.xml',
        'views/expense_data.xml',
        'views/email_template.xml',
        'views/employee_data.xml',
        # 'views/units_data.xml',
        'views/my_profile.xml',
        'views/res_company.xml',
        # 'wizard/training_report_wiz.xml',
        # 'appraisals_scedular_view.xml',
    ],
    'category': 'Human Resources',
    'author': 'Apagen Solutions Pvt. Ltd.',
    'website': 'http://www.apagen.com',
    'installable': True,
    'application': True,
    'auto_install': False,
    # 'css': [ 'static/src/css/*.css' ],
    'qweb': ['static/src/xml/*.xml'],

}

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
