from odoo import models, fields, api, _
from odoo.exceptions import Warning


from odoo.exceptions import UserError, ValidationError


class lot_barcode_label(models.Model):
    _name="lot.barcode.label"
    
    @api.model
    def default_get(self, fields_list):
        prod_list = []
        user_code = ''
        res = super(lot_barcode_label, self).default_get(fields_list)
        if self.env.user:
            user_print = self.env['users.print'].search([('name','=',self.env.user.id)])
            if user_print:
                for val in user_print:
                    user_code = val.code
        if self._context.get('active_ids'):
            for product in self._context.get('active_ids'):
                prod_list.append((0, 0, {'lot_id': product}))
        res['label_lines'] = prod_list
        res['user_code'] = user_code
        return res
    
    no_copies = fields.Integer('No. of Copies')
    user_code = fields.Char ('User Code')
    user_id = fields.Many2one('res.users','User',default=lambda self: self.env.user,track_visibility="onchange")
    label_lines = fields.One2many('lot.barcode.label.line', 'lot_barcode_label_id', string = "Label")  
    
#     def get_copies(self):
#         print"dvkbfgf",self.env.context.get('no_copies')
#         return self.env.context.get('no_copies')
#   
    
    @api.multi
    def action_call_report(self):
        line_seq = []
        line_name = []
        qty = 0.0
        if self.label_lines:
            for val in self.label_lines:
                qty = qty + 1
        if self.user_id:
            user_print = self.env['users.print'].search([('name','=',self.user_id.id)])
            if user_print:
                for val in user_print:
                    if val.unlimited_copies == False:
                        if self.no_copies > 0.0:
                            balance = val.no_of_copies_allowed - val.no_of_copies_taken
                            if balance >= (qty * self.no_copies):
                                val.write({'no_of_copies_taken':(val.no_of_copies_taken + (qty * self.no_copies))})
                            else:
                                raise UserError(_('You are to allow only %s copies.') %
                                                        (balance))
                        else:
                            raise UserError(_('Please fill no. of copies.'))
            else:
                raise UserError(_('You are not allow to print labels.'))
            
        data = self.read()[0]
        data.update(self.read(['no_copies', 'label_lines'])[0])
        datas = {
            'ids': self._ids,
            'model': 'lot.barcode.label',
            'form': data
        }
        print("datas:::::::::::::::::::::::::::::::::::::::::", datas)
#         xml_id = self.env['ir.actions.report.xml'].search([('report_name', '=',
#                                                         'product_label_print.report_product_label_report1')])
#         print"fvkfnvknfk",xml_id
#         return self.env['report'].get_action(self, 'product_label_print.report_product_label_report1', data = datas)
        return {
            'type': 'ir.actions.report.xml',
            'report_name': 'product_label_print.report_lot_barcode',
            'report_type': 'qweb-pdf',
#           'context': context,
        }
    
class lot_barcode_label_line(models.TransientModel):
    _name = "lot.barcode.label.line"

    lot_id = fields.Many2one('stock.production.lot',string="Serial#",required=True)
    lot_barcode_label_id = fields.Many2one('lot.barcode.label',string="Lot Label")
