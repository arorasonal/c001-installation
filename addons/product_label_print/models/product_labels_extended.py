from odoo import models, fields, api, _
from odoo.exceptions import Warning


from odoo.exceptions import UserError, ValidationError


class product_label_extended_print(models.Model):
    _name="product.label.extended.print"
    
    @api.model
    def default_get(self, fields_list):
        prod_list = []
        user_code = ''
        res = super(product_label_extended_print, self).default_get(fields_list)
        if self.env.user:
            user_print = self.env['users.print'].search([('name','=',self.env.user.id)])
            if user_print:
                for val in user_print:
                    user_code = val.code
        if self._context.get('active_ids'):
            for product in self._context.get('active_ids'):
                prod_list.append((0, 0, {'product_id': product}))
        res['product_lines'] = prod_list
        res['user_code'] = user_code
        return res
    
    no_copies = fields.Integer('No. of Copies')
    user_code = fields.Char ('User Code')
    user_id = fields.Many2one('res.users','User',default=lambda self: self.env.user,track_visibility="onchange")
    product_lines = fields.One2many('product.label.extended.line', 'product_label_extended_id', string = "Product List")  
    
#     def get_copies(self):
#         print"dvkbfgf",self.env.context.get('no_copies')
#         return self.env.context.get('no_copies')
#   
    
    @api.multi
    def action_call_report(self):
        line_seq = []
        line_name = []
        qty = 0.0
        if self.product_lines:
            for val in self.product_lines:
                qty = qty + 1
        if self.user_id:
            user_print = self.env['users.print'].search([('name','=',self.user_id.id)])
            if user_print:
                for val in user_print:
                    if val.unlimited_copies == False:
                        if self.no_copies > 0.0:
                            balance = val.no_of_copies_allowed - val.no_of_copies_taken
                            if balance >= (qty * self.no_copies):
                                val.write({'no_of_copies_taken':(val.no_of_copies_taken + (qty * self.no_copies))})
                            else:
                                raise UserError(_('You are to allow only %s copies.') %
                                                        (balance))
                        else:
                            raise UserError(_('Please fill no. of copies.'))
            else:
                raise UserError(_('You are not allow to print labels.'))
            
        data = self.read()[0]
        data.update(self.read(['no_copies', 'product_lines'])[0])
        datas = {
            'ids': self._ids,
            'model': 'product.label.extended.print',
            'form': data
        }
        print("datas:::::::::::::::::::::::::::::::::::::::::", datas)
#         xml_id = self.env['ir.actions.report.xml'].search([('report_name', '=',
#                                                         'product_label_print.report_product_label_report1')])
#         print"fvkfnvknfk",xml_id
#         return self.env['report'].get_action(self, 'product_label_print.report_product_label_report1', data = datas)
        return {
            'type': 'ir.actions.report.xml',
            'report_name': 'product_label_print.report_product_template_extended_label',
            'report_type': 'qweb-pdf',
#           'context': context,
        }
    
class product_label_extended_line(models.TransientModel):
    _name = "product.label.extended.line"

    product_id = fields.Many2one('product.template', string = "Product(s)", required = True)
    product_label_extended_id = fields.Many2one('product.label.extended.print', string = "Product Label-Extended Print")
