from collections import namedtuple
import json
import time
from datetime import datetime
from odoo import api, fields, models, _
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT
from odoo.tools import DEFAULT_SERVER_DATE_FORMAT
from odoo.tools.float_utils import float_compare
from odoo.addons.procurement.models import procurement
from odoo.exceptions import UserError
from datetime import date


class users_print(models.Model):
    _name="users.print"
    _inherit = ['mail.thread', 'ir.needaction_mixin']
    
    
    def action_reset(self):
        for each in self:
            each.update({'no_of_copies_taken':0.0})
    
    name = fields.Many2one('res.users','User',track_visibilty="onchange")
    code = fields.Char('User Code',track_visibility="onchange")
    no_of_copies_allowed = fields.Integer('No. of Copies Allowed',track_visibilty="onchange")
    unlimited_copies = fields.Boolean('Unlimited No. Of Copies Allowed',track_visibilty="onchange")
    no_of_copies_taken = fields.Integer('No. of Copies Taken',track_visibilty="onchange")
