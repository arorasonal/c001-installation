# -*- coding: utf-8 -*-
# See LICENSE file for full copyright and licensing details.

{
    'name': 'Product Label Print',
    'version': '10.0.2.0.0',
    'category': 'Partner',
    'depends': ['mail' ,'base', 'product', 'stock'],
    'license': 'AGPL-3',
    'author': 'DiracERP Solutions',
    'description': '''
    ''',
    'data': [
        'view/product_label_view.xml',
        'view/product_label_report_template.xml',
        'view/product_label_report_view.xml',
        'view/users_print_view.xml',
        'view/product_labels_extended_view.xml',
        'view/product_label_extended_report_template.xml',
        'view/product_label_extended_report_view.xml',
        'view/lot_barcode_label_report_view.xml',
        'view/lot_barcode_label_report_template.xml',
        'view/lot_barcode_label_view.xml',
    ],
    'installable': True,
    'auto_install': False,
}
