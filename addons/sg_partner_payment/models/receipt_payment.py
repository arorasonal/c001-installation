from odoo import models, fields, api, _
from odoo.exceptions import UserError, ValidationError
from odoo.tools import DEFAULT_SERVER_DATE_FORMAT
from odoo.tools import float_round

import logging
_logger = logging.getLogger(__name__)


class ReceiptPayment(models.Model):
    _name = 'receipt.payment'
    _inherit = ['mail.thread']
    _description = 'Customer Receipts and Supplier Payments'
    _order = 'id desc'

    @api.depends('line_cr_ids', 'line_cr_ids.amount', 'line_dr_ids', 'line_dr_ids.amount')
    def _compute_amount(self):
        for record in self:
            credit = 0.0
            credit_original = 0.0
            debit = 0.0
            debit_original = 0.0
            tds_amount_debit = 0.0
            tds_amount_credit = 0.0
            for line in record.line_cr_ids:
                credit += float_round(line.amount, 2)
                credit_original += float_round(line.amount_unreconciled, 2)
                tds_amount_credit += float_round(line.tds_amount, 2)
            for line in record.line_dr_ids:
                debit += float_round(line.amount, 2)
                debit_original += float_round(line.amount_unreconciled, 2)
                tds_amount_debit += float_round(line.tds_amount, 2)

            if credit == credit_original and debit == debit_original:
                if record.type == 'customer':
                    diff_amount = record.currency_id.round(
                        float_round(record.amount, 2) + credit - debit + tds_amount_debit)
                else:
                    diff_amount = record.currency_id.round(
                        float_round(record.amount, 2) + debit - credit + tds_amount_credit)
            else:
                if record.type == 'customer':
                    diff_amount = record.currency_id.round(float_round(
                        record.amount, 2) + credit_original - debit_original + tds_amount_debit)
                else:
                    diff_amount = record.currency_id.round(float_round(
                        record.amount, 2) + debit_original - credit_original + tds_amount_credit)

            record.diff_amount = float_round(diff_amount, 2)

    name = fields.Char(default='/', readonly=True,
                       required=True, copy=False, string='Number')
    partner_id = fields.Many2one(
        'res.partner', required=True, string='Partner')
    state = fields.Selection([('draft', 'Draft'), ('cancel', 'Cancelled'), ('in_progress', 'In Progress'), ('posted', 'Posted')],
                             default='draft', copy=False, track_visibility='onchange', string='Status')
    type = fields.Selection([('customer', 'Customer'), ('supplier', 'Supplier')],
                            default='customer', required=True, string='Type')
    company_id = fields.Many2one('res.company', required=True,
                                 default=lambda self: self.env.user.company_id.id, string='Company')
    currency_id = fields.Many2one('res.currency', required=True,
                                  default=lambda self: self.env.user.company_id.currency_id.id, string='Currency')
    amount = fields.Monetary(currency_field='currency_id', string='Total')
    journal_id = fields.Many2one('account.journal', required=True, domain=[
                                 ('type', 'in', ('bank', 'cash'))], string='Payment Method')
    date = fields.Date('Date', copy=False, required=True,
                       help='Effective date for accounting entries', default=fields.Date.context_today)
    payment_ref = fields.Char(
        'Payment Ref', help='Transaction reference number')
    memo = fields.Char(size=256)
    line_cr_ids = fields.One2many(
        'receipt.payment.credit', 'receipt_payment_id', copy=False, string='Credits')
    line_dr_ids = fields.One2many(
        'receipt.payment.debit', 'receipt_payment_id', copy=False, string='Debits')
    diff_amount = fields.Monetary(
        compute='_compute_amount', currency_id='currency_id', string='Difference Amount')
    payment_id = fields.Many2one(
        'account.payment', copy=False, string='Payments')
    payment_ids = fields.Many2many(
        'account.payment', 'receipt_payment_rel', copy=False, string='Payments')
    payment_difference_handling = fields.Selection([('without_writeoff', 'Keep Open'), ('reconcile', 'Reconcile Payment Balance')],
                                                   'Payment Difference', default='without_writeoff', state={'draft': [('readonly', False)]},
                                                   help="This field helps you to choose what you want to do with the eventual difference between the paid amount and the sum of allocated amounts. You can either choose to keep open this difference on the partner's account, or reconcile it with the payment(s)")
    comment = fields.Char('Counterpart Comment', readonly=True, states={
                          'draft': [('readonly', False)]}, default='Conterpart')
    writeoff_acc_id = fields.Many2one('account.account', 'Counterpart Account', readonly=True, states={
                                      'draft': [('readonly', False)]})
    narration = fields.Text('Notes', readonly=True, states={
                            'draft': [('readonly', False)]})
    move_line_id = fields.Char('Move Line ID')
    auto_allocate = fields.Boolean(string="Auto-Allocate Amount")
    account_tax_id = fields.Many2one(
        'account.tax', "Default TDS", domain=([('tax_group_id.name', '=', 'TDS')]), readonly=True,
        states={'draft': [('readonly', False)]})

    @api.multi
    def action_send_mail(self):
        '''
        This function opens a window to compose an email.
        '''
        self.ensure_one()
        ir_model_data = self.env['ir.model.data']
        try:
            template_id = ir_model_data.get_object_reference('sg_partner_payment', 'account_receipt_payment_email_template')[1]
        except ValueError:
            template_id = False
        try:
            compose_form_id = ir_model_data.get_object_reference('mail', 'email_compose_message_wizard_form')[1]
        except ValueError:
            compose_form_id = False
        ctx = dict()
        ctx.update({
            'default_model': 'receipt.payment',
            'default_res_id': self.ids[0],
            'default_use_template': bool(template_id),
            'default_template_id': template_id,
            'default_composition_mode': 'comment',
            'mark_so_as_sent': True,
            # 'custom_layout': "sale.mail_template_data_notification_email_sale_order"
        })
        return {
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'mail.compose.message',
            'views': [(compose_form_id, 'form')],
            'view_id': compose_form_id,
            'target': 'new',
            'context': ctx,
        }

    @api.model
    def create(self, vals):
        if 'type' in vals and vals['type'] and vals['type'] == 'customer':
            vals['name'] = self.env['ir.sequence'].next_by_code(
                'receipt.payment.customer')
        else:
            vals['name'] = self.env['ir.sequence'].next_by_code(
                'receipt.payment.supplier')
        receipt_payment_id = super(ReceiptPayment, self).create(vals)
        message_ids = receipt_payment_id.sudo().message_ids.ids
        if message_ids:
            message = ''
            message_id = self.env[
                'mail.message'].sudo().browse(min(message_ids))
            if receipt_payment_id.type == 'customer':
                message = '<p>Customer Receipts created</p>'
            elif receipt_payment_id.type == 'supplier':
                message = '<p>Supplier Payments created</p>'
            if message_id and message:
                message_id.sudo().write({'body': message})
        return receipt_payment_id

    @api.onchange('journal_id')
    def onchange_journal(self):
        if self.journal_id and self.journal_id.currency_id:
            self.currency_id = self.journal_id.currency_id.id
        else:
            self.currency_id = self.company_id.currency_id.id

    @api.multi
    def compute_values(self):
        for record in self:
            record.line_cr_ids.unlink()
            record.line_dr_ids.unlink()
            line_cr_lst, line_dr_lst = record.load_data()
            record.write({'line_cr_ids': line_cr_lst,
                          'line_dr_ids': line_dr_lst})
            if record.auto_allocate:
                payment_amount = record.amount
                if record.type == 'customer':
                    for rec in record.line_dr_ids:
                        if payment_amount <= rec.amount_unreconciled:
                            rec.payment_amount = payment_amount
                            break
                        else:
                            rec.payment_amount = rec.amount_unreconciled
                            payment_amount -= rec.amount_unreconciled
                    for rec in record.line_dr_ids:
                        if record.account_tax_id:
                            rec.apply_tds = True
                            rec.account_tax_id = record.account_tax_id.id
                            rec.update_tds()
                            rec.total_paid_amount = rec.amount + rec.tds_amount
                else:
                    for rec in record.line_cr_ids:
                        if payment_amount <= rec.amount_unreconciled:
                            rec.payment_amount = payment_amount
                            break
                        else:
                            rec.payment_amount = rec.amount_unreconciled
                            payment_amount -= rec.amount_unreconciled
                    for rec in record.line_cr_ids:
                        if record.account_tax_id:
                            rec.apply_tds = True
                            rec.account_tax_id = record.account_tax_id.id
                            rec.update_tds()
                            rec.total_paid_amount = rec.amount + rec.tds_amount

    @api.onchange('auto_allocate')
    def onchange_auto_allocate(self):
        if not self.auto_allocate:
            for record in self.line_dr_ids:
                record.payment_amount = 0
                record.amount = 0
                record.expected_tds_amount = 0
                record.tds_amount = 0
                record.reconcile = False
                record.apply_tds = False
                record.account_tax_id = False
            for record in self.line_cr_ids:
                record.payment_amount = 0
                record.amount = 0
                record.expected_tds_amount = 0
                record.tds_amount = 0
                record.reconcile = False
                record.apply_tds = False
                record.account_tax_id = False
        else:
            for receipt in self:
                line_cr_lst, line_dr_lst = receipt.load_data()
                receipt.line_cr_ids = line_cr_lst
                receipt.line_dr_ids = line_dr_lst
                payment_amount = receipt.amount
                if receipt.type == 'customer':
                    for rec in receipt.line_dr_ids:
                        if payment_amount <= rec.amount_unreconciled:
                            rec.payment_amount = payment_amount
                            break
                        else:
                            rec.payment_amount = rec.amount_unreconciled
                            payment_amount -= rec.amount_unreconciled
                    for rec in receipt.line_dr_ids:
                        if receipt.account_tax_id:
                            rec.apply_tds = True
                            rec.account_tax_id = receipt.account_tax_id.id
                            rec.update_tds()
                            rec.total_paid_amount = rec.amount + rec.tds_amount
                            if rec.payment_amount + rec.tds_amount == rec.original_amount:
                                rec.reconcile = True
                else:
                    for rec in receipt.line_cr_ids:
                        if payment_amount <= rec.amount_unreconciled:
                            rec.payment_amount = payment_amount
                            rec.total_paid_amount = rec.amount + rec.tds_amount
                            break
                        else:
                            rec.payment_amount = rec.amount_unreconciled
                            payment_amount -= rec.amount_unreconciled
                    for rec in receipt.line_cr_ids:
                        if receipt.account_tax_id:
                            rec.apply_tds = True
                            rec.account_tax_id = receipt.account_tax_id.id
                            rec.update_tds()
                            rec.total_paid_amount = rec.amount + rec.tds_amount

    @api.onchange('account_tax_id')
    def onchange_account_tax_id(self):
        if not self.account_tax_id:
            for record in self.line_dr_ids:
                record.payment_amount += record.tds_amount
                record.amount += record.tds_amount
                record.apply_tds = False
                record.account_tax_id = False
                record.expected_tds_amount = 0.0
                record.tds_amount = 0.0
            for record in self.line_cr_ids:
                record.payment_amount += record.tds_amount
                record.amount += record.tds_amount
                record.apply_tds = False
                record.account_tax_id = False
                record.expected_tds_amount = 0.0
                record.tds_amount = 0.0

    @api.multi
    def load_data(self):
        line_cr_lst = []
        line_dr_lst = []
        partner_lst = []
        if self.partner_id:
            # Query to load the data from account.move.line for Cr and Dr
            base_currency_id = self.company_id.currency_id
            partner_lst.append(self.partner_id.id)
            partner_lst.append(self.partner_id.child_ids.ids)
            new_partner_lst = []
            for i in partner_lst:
                if isinstance(i, list):
                    new_partner_lst += i
                else:
                    new_partner_lst.append(i)
            currency_id = self.currency_id
            if self.type == 'customer':
                account_ids = self.env['account.account'].search(
                    [('internal_type', '=', 'receivable')]).ids
            else:
                account_ids = self.env['account.account'].search(
                    [('internal_type', '=', 'payable')]).ids
#             account_ids = tuple(account_ids)
            self.env.cr.execute('''
                SELECT id as invoice_id,state,create_date as date,date_due,account_id,amount_total as balance,
                type,residual as amount_unreconciled_currency,amount_untaxed as original_amount,currency_id as move_currency_id,residual_signed as amount_unreconciled,
                amount_total_company_signed as original_amount_currency
                FROM account_invoice WHERE state in ('open') and partner_id in %s and account_id in %s
                ''', (tuple(new_partner_lst), tuple(account_ids)))
            credit_amount = 0.0
            debit_amount = 0.0
            for result in self.env.cr.dictfetchall():
                invoice_id = self.env['account.invoice'].browse(
                    result['invoice_id'])
                if result['balance'] != 0:
                    if not result['move_currency_id']:
                        result['move_currency_id'] = base_currency_id.id
                    result['residual_balance'] = abs(
                        result['amount_unreconciled_currency'])
                    result['amount_unreconciled'] = invoice_id.currency_id.compute(
                        abs(result['amount_unreconciled_currency']), base_currency_id, False)
                    result['amount_unreconciled_currency'] = abs(
                        result['amount_unreconciled_currency'])
                    result['original_amount_currency'] = abs(result['balance'])
                    result['untaxed_amount'] = invoice_id.amount_untaxed

                    if invoice_id.type in ['out_invoice', 'in_refund'] and invoice_id.state == 'open':
                        original_amount = invoice_id.currency_id.compute(
                            abs(result['balance']), base_currency_id, False)
                        debit_amount += invoice_id.currency_id.compute(
                            abs(result['amount_unreconciled']), currency_id, False)
                        result['original_amount'] = original_amount
                        line_dr_lst.append((0, 0, result))

                    elif invoice_id.type in ['out_refund', 'in_invoice'] and invoice_id.state == 'open':
                        original_amount = invoice_id.currency_id.compute(
                            abs(result['balance']), base_currency_id, False)
                        credit_amount += invoice_id.currency_id.compute(
                            abs(result['amount_unreconciled']), currency_id, False)
                        result['original_amount'] = original_amount
                        line_cr_lst.append((0, 0, result))

            # Code to auto-fill Reconcile and Allocation amount
            if self.auto_allocate:
                dr_index = -1
                cr_index = -1
                if self.type == 'customer':
                    credit_amount += self.amount
                    # debit_amount = 0.0
                    for dr_lst in line_dr_lst:
                        dr_index += 1
                        if line_dr_lst[dr_index][2].get('move_currency_id') != currency_id.id:
                            invoice_currency_obj = self.env['res.currency'].browse(
                                line_dr_lst[dr_index][2].get('move_currency_id'))
                            credit_amount = currency_id.compute(
                                abs(credit_amount), invoice_currency_obj, False)

                        if credit_amount > 0:
                            amt = line_dr_lst[dr_index][2]['residual_balance']
                            if amt > credit_amount:
                                amt = credit_amount
                                credit_amount -= amt
                                reconcile = False
                            else:
                                credit_amount -= amt
                                reconcile = True
                            allocation_amount = amt
                            if line_dr_lst[dr_index][2].get('move_currency_id') != currency_id.id:
                                invoice_currency_obj = self.env['res.currency'].browse(
                                    line_dr_lst[dr_index][2].get('move_currency_id'))
                                allocation_amount = invoice_currency_obj.compute(
                                    abs(amt), currency_id, False)
                            line_dr_lst[dr_index][2].update(
                                {'amount': allocation_amount, 'reconcile': reconcile})
                        if line_dr_lst[dr_index][2].get('move_currency_id') != currency_id.id:
                            invoice_currency_obj = self.env['res.currency'].browse(
                                line_dr_lst[dr_index][2].get('move_currency_id'))
                            credit_amount = invoice_currency_obj.compute(
                                abs(credit_amount), currency_id, False)

                    debit_amount = debit_amount - self.amount if debit_amount > self.amount else 0.00
                    for cr_lst in line_cr_lst:
                        cr_index += 1
                        if line_cr_lst[cr_index][2].get('move_currency_id') != currency_id.id:
                            invoice_currency_obj = self.env['res.currency'].browse(
                                line_cr_lst[cr_index][2].get('move_currency_id'))
                            debit_amount = currency_id.compute(
                                abs(debit_amount), invoice_currency_obj, False)

                        if debit_amount > 0:
                            amt = line_cr_lst[cr_index][2]['residual_balance']
                            if amt > debit_amount:
                                amt = debit_amount
                                debit_amount -= amt
                                reconcile = False
                            else:
                                debit_amount -= amt
                                reconcile = True
                            allocation_amount = amt
                            if line_cr_lst[cr_index][2].get('move_currency_id') != currency_id.id:
                                invoice_currency_obj = self.env['res.currency'].browse(
                                    line_cr_lst[cr_index][2].get('move_currency_id'))
                                allocation_amount = invoice_currency_obj.compute(
                                    abs(amt), currency_id, False)
                            line_cr_lst[cr_index][2].update(
                                {'amount': allocation_amount, 'reconcile': reconcile})
                        if line_cr_lst[cr_index][2].get('move_currency_id') != currency_id.id:
                            invoice_currency_obj = self.env['res.currency'].browse(
                                line_cr_lst[cr_index][2].get('move_currency_id'))
                            debit_amount = invoice_currency_obj.compute(
                                abs(debit_amount), currency_id, False)
                else:
                    debit_amount += self.amount
                    for cr_lst in line_cr_lst:
                        cr_index += 1
                        if line_cr_lst[cr_index][2].get('move_currency_id') != currency_id.id:
                            invoice_currency_obj = self.env['res.currency'].browse(
                                line_cr_lst[cr_index][2].get('move_currency_id'))
                            debit_amount = currency_id.compute(
                                abs(debit_amount), invoice_currency_obj, False)
                        if debit_amount > 0:
                            amt = line_cr_lst[cr_index][2]['residual_balance']
                            if amt > debit_amount:
                                amt = debit_amount
                                debit_amount -= amt
                                reconcile = False
                            else:
                                debit_amount -= amt
                                reconcile = True
                            allocation_amount = amt
                            if line_cr_lst[cr_index][2].get('move_currency_id') != currency_id.id:
                                invoice_currency_obj = self.env['res.currency'].browse(
                                    line_cr_lst[cr_index][2].get('move_currency_id'))
                                allocation_amount = invoice_currency_obj.compute(
                                    abs(amt), currency_id, False)
                            line_cr_lst[cr_index][2].update(
                                {'amount': allocation_amount, 'reconcile': reconcile})
                        if line_cr_lst[cr_index][2].get('move_currency_id') != currency_id.id:
                            invoice_currency_obj = self.env['res.currency'].browse(
                                line_cr_lst[cr_index][2].get('move_currency_id'))
                            debit_amount = invoice_currency_obj.compute(
                                abs(debit_amount), currency_id, False)

                    credit_amount = credit_amount - self.amount if credit_amount > self.amount else 0.00
                    for dr_lst in line_dr_lst:
                        dr_index += 1
                        if line_dr_lst[dr_index][2].get('move_currency_id') != currency_id.id:
                            invoice_currency_obj = self.env['res.currency'].browse(
                                line_dr_lst[dr_index][2].get('move_currency_id'))
                            credit_amount = currency_id.compute(
                                abs(credit_amount), invoice_currency_obj, False)
                        if credit_amount > 0:
                            amt = line_dr_lst[dr_index][2]['residual_balance']
                            if amt > credit_amount:
                                amt = credit_amount
                                credit_amount -= amt
                                reconcile = False
                            else:
                                credit_amount -= amt
                                reconcile = True
                            allocation_amount = amt
                            if line_dr_lst[dr_index][2].get('move_currency_id') != currency_id.id:
                                invoice_currency_obj = self.env['res.currency'].browse(
                                    line_dr_lst[dr_index][2].get('move_currency_id'))
                                allocation_amount = invoice_currency_obj.compute(
                                    abs(amt), currency_id, False)
                            line_dr_lst[dr_index][2].update(
                                {'amount': allocation_amount, 'reconcile': reconcile})
                        if line_dr_lst[dr_index][2].get('move_currency_id') != currency_id.id:
                            invoice_currency_obj = self.env['res.currency'].browse(
                                line_dr_lst[dr_index][2].get('move_currency_id'))
                            credit_amount = invoice_currency_obj.compute(
                                abs(credit_amount), currency_id, False)
        return line_cr_lst, line_dr_lst

    @api.onchange('partner_id', 'amount', 'currency_id')
    def onchange_account_id(self):
        self.line_cr_ids = False
        self.line_dr_ids = False
        if self.partner_id:
            self.line_cr_ids, self.line_dr_ids = self.load_data()
        payment_amount = self.amount
        if self.auto_allocate:
            if self.type == 'customer':
                for rec in self.line_dr_ids:
                    if payment_amount <= rec.amount_unreconciled:
                        rec.payment_amount = payment_amount
                        rec.total_paid_amount = rec.amount
                        break
                    else:
                        rec.payment_amount = rec.amount_unreconciled
                        payment_amount -= rec.amount_unreconciled
                        rec.total_paid_amount = rec.amount
            else:
                for rec in self.line_cr_ids:
                    if payment_amount <= rec.amount_unreconciled:
                        rec.payment_amount = payment_amount
                        rec.total_paid_amount = rec.amount
                        break
                    else:
                        rec.payment_amount = rec.amount_unreconciled
                        payment_amount -= rec.amount_unreconciled
                        rec.total_paid_amount = rec.amount

    @api.multi
    def action_cancel(self):
        for rec in self:
            for payment in rec.payment_ids:
                for move in payment.move_line_ids.mapped('move_id'):
                    if rec.line_dr_ids:
                        move.line_ids.remove_move_reconcile()
                    for invoice in payment.invoice_ids:
                        invoice.move_id.line_ids.remove_move_reconcile()
                    move.button_cancel()
                    move.unlink()
                payment.state = 'draft'
                payment.move_name = ''
                payment.unlink()
            rec.state = 'draft'

    @api.multi
    def check_values(self):
        if not self.amount and not self.line_dr_ids and not self.line_cr_ids:
            raise ValidationError('No data for validation.')
        msg = ''
        # Check Debit Entries
        self.env.cr.execute('''
            SELECT move_id.name FROM receipt_payment_debit as rpd, account_move_line as move_line, account_move as move_id
            WHERE rpd.receipt_payment_id=%s and move_line.id=rpd.move_line_id and move_id.id=move_line.move_id and rpd.amount>0 and abs(rpd.amount_residual) != abs(move_line.amount_residual)
            ''' % self.id)
        debit_items = ''
        cnt = 0
        for item in self.env.cr.fetchall():
            if item:
                cnt += 1
                debit_items += str(cnt) + '. ' + str(item[0]) + '\n'
        if debit_items:
            msg += '\nDebits:\n' + debit_items
        # Check Credit Entries
        self.env.cr.execute('''
            SELECT move_id.name FROM receipt_payment_credit as rpc, account_move_line as move_line, account_move as move_id
            WHERE rpc.receipt_payment_id=%s and move_line.id=rpc.move_line_id and move_id.id=move_line.move_id and rpc.amount>0 and abs(rpc.amount_residual) != abs(move_line.amount_residual)
            ''' % self.id)
        credit_items = ''
        cnt = 0
        for item in self.env.cr.fetchall():
            if item:
                cnt += 1
                credit_items += str(cnt) + '. ' + str(item[0]) + '\n'
        if credit_items:
            msg += '\nCredits:\n' + credit_items
        if msg:
            raise ValidationError(
                'The following journal items are already proceed.\n' + msg)
        return True

    @api.multi
    def action_post(self):
        for record in self:
            # Validating the record
            #             if record.diff_amount < 0:
            #                 raise ValidationError('Difference amount must be greater than or equal to zero.')
            record.check_values()

            # Preparing the values
            if record.type == 'customer':
                payment_method_id = self.env['ir.model.data'].xmlid_to_res_id(
                    'account.account_payment_method_manual_in')
                payment_type = 'inbound'
            else:
                payment_method_id = self.env['ir.model.data'].xmlid_to_res_id(
                    'account.account_payment_method_manual_out')
                payment_type = 'outbound'
            payment_method_id = payment_method_id
            payment_type = payment_type
            payment_id = False

            # Processing customer receipts
            if record.type == 'customer':
                for dr_line in record.line_dr_ids:
                    if dr_line.amount and dr_line.payment_amount:
                        payment_id = record.create_payments(
                            record.line_cr_ids and record.line_cr_ids[0].invoice_id, dr_line.invoice_id,
                            payment_method_id, payment_type, dr_line.payment_amount, dr_line.account_tax_id, dr_line.expected_tds_amount,
                            dr_line.tds_amount)
                        payment_obj = self.env['account.payment'].browse(payment_id)
                        # Updating writeoff account info
                        if record.payment_difference_handling == 'reconcile' and record.diff_amount and record.writeoff_acc_id:
                            payment_obj.difference = record.diff_amount
                            payment_obj.total_residual = record.diff_amount
                            payment_obj.payment_difference_handling = 'reconcile'
                            payment_obj.writeoff_account_id = record.writeoff_acc_id.id
                            if record.type == 'supplier':
                                payment_obj.amount = record.amount - record.diff_amount
                        for invoice in payment_obj.invoice_ids:
                            if invoice.type == 'out_refund':
                                self.env.cr.execute(
                                    'DELETE from account_invoice_payment_rel where invoice_id= %s and payment_id=%s' % (invoice.id, payment_obj.id))
                        payment_obj.post()
                        record.write(
                            {'payment_ids': [(4, payment_id)], 'payment_id': payment_id})
                    amount = dr_line.amount / dr_line.invoice_id.currency_id.rate
                    # Linking credit lines
                    for cr_line in record.line_cr_ids:
                        if amount > 0.0 and cr_line.amount > 0.0 and not cr_line.move_line_id.reconciled and cr_line.invoice_id.state == 'open':
                            amount -= (cr_line.amount /
                                       cr_line.invoice_id.currency_id.rate)
                            move_lines = cr_line.invoice_id.move_id.line_ids.filtered(
                                lambda r: not r.reconciled and r.account_id.internal_type in ('payable', 'receivable'))
                            dr_line.invoice_id.register_payment(move_lines)
                    # Linking with newly created payment
                    if payment_id and amount > 0:
                        payment_obj = self.env[
                            'account.payment'].browse(payment_id)
                        move_lines = payment_obj.move_line_ids.filtered(
                            lambda r: not r.reconciled and r.account_id.internal_type in ('payable', 'receivable'))
                        dr_line.invoice_id.register_payment(move_lines)

            # Processing customer receipts
            elif record.type == 'supplier':
                for cr_line in record.line_cr_ids:
                    if cr_line.amount and cr_line.payment_amount:
                        payment_id = record.create_payments(
                            record.line_dr_ids and record.line_dr_ids[0].invoice_id, cr_line.invoice_id,
                            payment_method_id, payment_type, cr_line.payment_amount, cr_line.account_tax_id, cr_line.expected_tds_amount,
                            cr_line.tds_amount)
                        payment_obj = self.env['account.payment'].browse(payment_id)
                        # Updating writeoff account info
                        if record.payment_difference_handling == 'reconcile' and record.diff_amount and record.writeoff_acc_id:
                            payment_obj.difference = record.diff_amount
                            payment_obj.total_residual = record.diff_amount
                            payment_obj.payment_difference_handling = 'reconcile'
                            payment_obj.writeoff_account_id = record.writeoff_acc_id.id
                        # Posting payments
                        for invoice in payment_obj.invoice_ids:
                            if invoice.type == 'in_refund':
                                self.env.cr.execute(
                                    'DELETE from account_invoice_payment_rel where invoice_id= %s and payment_id=%s' % (invoice.id, payment_obj.id))
                        payment_obj.post()
                        record.write(
                            {'payment_ids': [(4, payment_id)], 'payment_id': payment_id})
                    amount = cr_line.amount / cr_line.invoice_id.currency_id.rate
                    # Linking credit lines
                    for dr_line in record.line_dr_ids:
                        if amount > 0.0 and dr_line.amount > 0.0 and not dr_line.move_line_id.reconciled:
                            amount -= (dr_line.amount /
                                       dr_line.invoice_id.currency_id.rate)
                            move_lines = dr_line.invoice_id.move_id.line_ids.filtered(
                                lambda r: not r.reconciled and r.account_id.internal_type in ('payable', 'receivable'))
                            cr_line.invoice_id.register_payment(move_lines)
                    # Linking with newly created payment
                    if payment_id and amount > 0:
                        payment_obj = self.env[
                            'account.payment'].browse(payment_id)
                        move_lines = payment_obj.move_line_ids.filtered(
                            lambda r: not r.reconciled and r.account_id.internal_type in ('payable', 'receivable'))
                        cr_line.invoice_id.register_payment(move_lines)

            # Updating the references
            if payment_id:
                payment_obj = self.env['account.payment'].browse(payment_id)
                for move_line in payment_obj.move_line_ids:
                    move_line.move_id.ref = record.name
                    # if not move_line.full_reconcile_id:
                    #     move_line.name = record.name

            self.write({'state': 'posted'})
        return True

    @api.multi
    def create_payments(
        self, credit_invoice_id, debit_invoice_id, payment_method_id,
            payment_type, amount, account_tax_id, expected_tds_amount, tds_amount):
        payment_vals = {}
        invoice_lst = []
        if credit_invoice_id and credit_invoice_id.state == 'open':
            invoice_lst.append(credit_invoice_id.id)
        if debit_invoice_id and debit_invoice_id.state == 'open':
            invoice_lst.append(debit_invoice_id.id)
        payment_vals['payment_type'] = payment_type
        payment_vals['partner_type'] = self.type
        payment_vals['payment_method_id'] = payment_method_id
        payment_vals['partner_id'] = self.partner_id.id
        payment_vals['journal_id'] = self.journal_id.id
        payment_vals['currency_id'] = self.currency_id.id
        payment_vals['company_id'] = self.company_id.id
        payment_vals['communication'] = self.memo or ''
        payment_vals['amount'] = amount
        if account_tax_id:
            payment_vals['account_tax_id'] = account_tax_id.id
            payment_vals['apply_tds'] = True
            payment_vals['expected_tds_amount'] = expected_tds_amount
            payment_vals['tds_amount'] = tds_amount
            payment_vals['amount_to_be_paid'] = amount + tds_amount
        if credit_invoice_id or debit_invoice_id:
            payment_vals['invoice_ids'] = [(6, 0, invoice_lst)]
        payment_vals['payment_date'] = self.date
        payment_id = self.env['account.payment'].create(payment_vals).id
        self.write({'payment_ids': [(4, payment_id)]})
        return payment_id

    @api.multi
    def update_move_line(self, line, amount, sign):
        convt_amount = line.currency_id.compute(
            amount, line.move_currency_id, False)
        if line.move_line_id and line.move_line_id.amount_residual_currency:
            amount_residual_currency = (
                line.amount_unreconciled_currency - convt_amount)
        else:
            amount_residual_currency = 0.0
        amount_residual = line.amount_unreconciled - amount
        if line.currency_id.id != self.env.user.company_id.currency_id.id:
            amount_residual = line.currency_id.compute(
                amount_residual, self.env.user.company_id.currency_id, False)
        if round(amount_residual) <= 0:
            reconciled = True
        else:
            reconciled = False
        if amount_residual_currency:
            if amount_residual == 0:
                amount_residual_currency = 0.0
            elif sign == '-ve':
                amount_residual_currency = -amount_residual_currency
                amount_residual = -amount_residual
            self.env.cr.execute('''
            UPDATE account_move_line SET amount_residual_currency=%s, amount_residual=%s, reconciled=%s WHERE id=%s
            ''', (amount_residual_currency, amount_residual, reconciled,
                  line.move_line_id.id if line.move_line_id else False))
        else:
            if sign == '-ve':
                amount_residual = -amount_residual
            self.env.cr.execute('''
            UPDATE account_move_line SET amount_residual=%s, reconciled=%s WHERE id=%s
            ''', (amount_residual, reconciled, line.move_line_id.id if line.move_line_id else False))

    @api.multi
    def unlink(self):
        for record in self:
            state = dict(record.fields_get(['state'])['state'][
                         'selection']).get(record.state)
            if record.type == 'customer' and state not in ['draft', 'cancel']:
                raise UserError(
                    'Cannot delete Customer Receipts in %s state.' % state)
            elif record.type == 'supplier' and state not in ['draft', 'cancel']:
                raise UserError(
                    'Cannot delete Supplier Payments in %s state.' % state)
        return super(ReceiptPayment, self).unlink()

ReceiptPayment()


class ReceiptPaymentCredit(models.Model):
    _name = 'receipt.payment.credit'
    _description = 'Customer Receipts and Supplier Payments Lines'

    @api.multi
    def compute_base_currency(self):
        for record in self:
            record.base_currency_id = self.env.user.company_id.currency_id.id

    receipt_payment_id = fields.Many2one(
        'receipt.payment', string='Recipt/Payment')
    move_line_id = fields.Many2one('account.move.line', string='Move Line')
    date = fields.Date(readonly=True)
    date_maturity = fields.Date(readonly=True, string='Due Date')
    account_id = fields.Many2one(
        'account.account', required=True, string='Account')
    currency_id = fields.Many2one(
        related='receipt_payment_id.currency_id', string='Currency')
    move_currency_id = fields.Many2one(
        'res.currency', string='Move Line Currency')
    base_currency_id = fields.Many2one(
        'res.currency', compute='compute_base_currency', string='Move Line Currency')
    original_amount_currency = fields.Monetary(
        currency_field='move_currency_id', string='Original Currency Amount')
    original_amount = fields.Monetary(
        currency_field='base_currency_id', string='Base Currency Amount')
    amount_unreconciled_currency = fields.Monetary(
        currency_field='move_currency_id', string='Original Currency Open Balance')
    amount_unreconciled = fields.Monetary(
        currency_field='base_currency_id', string='Base Currency Open Balance')
    amount_residual = fields.Float(string='Amount Residual')
    reconcile = fields.Boolean('Full Reconcile')
    amount = fields.Monetary(currency_field='currency_id', string='Allocation')
    invoice_id = fields.Many2one('account.invoice', string='Invoice')
    payment_id = fields.Many2one('account.payment', string='Payment')
    apply_tds = fields.Boolean("Apply TDS")
    account_tax_id = fields.Many2one('account.tax', "Account Tax Id", domain=([('tax_group_id.name', '=', 'TDS')]))
    expected_tds_amount = fields.Monetary("Expected TDS")
    tds_amount = fields.Float("TDS Amount")
    payment_amount = fields.Float()
    untaxed_amount = fields.Float(string="Untaxed Amount")
    total_paid_amount = fields.Float("Paid Amount")

    @api.one
    @api.constrains('amount')
    def _check_amount(self):
        open_amount = self.currency_id.compute(
            abs(self.amount_unreconciled_currency), self.move_currency_id, False)
        if open_amount > 0 and float_round(self.amount, 2) > float_round(open_amount, 2):
            raise ValidationError(
                'Allocation amount must be less than or equal to Residual Amount.')

    @api.onchange('reconcile', 'amount')
    def onchange_reconcile(self):
        ctx = self._context or {}
        if self.reconcile and ctx.get('reconcile'):
            allocation_amount = self.currency_id.compute(
                abs(self.amount_unreconciled_currency), self.move_currency_id, False)
            self.amount = allocation_amount
            self.payment_amount = allocation_amount
        elif ctx.get('reconcile') and not self.reconcile:
            self.amount = 0.0
            self.payment_amount = 0.0
        elif ctx.get('amount') and float_round(self.amount, 2) >= float_round(self.amount_unreconciled_currency, 2):
            self.reconcile = True
        elif ctx.get('amount') and float_round(self.amount, 2) < float_round(self.amount_unreconciled_currency, 2):
            self.reconcile = False

    @api.onchange('amount')
    def onchange_amount(self):
        self.payment_amount = self.amount
        self.total_paid_amount = self.amount + self.tds_amount

    @api.onchange('account_tax_id')
    def update_tds(self):
        for rec in self:
            if rec.apply_tds and rec.account_tax_id:
                rec.tds_amount = (rec.invoice_id.amount_untaxed *
                                  rec.account_tax_id.amount) / 100
                rec.expected_tds_amount = rec.tds_amount
                rec.amount = rec.amount - rec.tds_amount
                rec.payment_amount = rec.amount - rec.tds_amount
            else:
                rec.tds_amount = 0.0
                rec.expected_tds_amount = 0.0

ReceiptPaymentCredit()


class ReceiptPaymentDebit(models.Model):
    _name = 'receipt.payment.debit'
    _description = 'Customer Receipts and Supplier Payments Lines'

    @api.multi
    def compute_base_currency(self):
        for record in self:
            record.base_currency_id = self.env.user.company_id.currency_id.id

    receipt_payment_id = fields.Many2one(
        'receipt.payment', string='Recipt/Payment')
    move_line_id = fields.Many2one('account.move.line', string='Move Line')
    account_id = fields.Many2one(
        'account.account', required=True, string='Account')
    date = fields.Date(readonly=True)
    date_maturity = fields.Date(readonly=True, string='Due Date')
    currency_id = fields.Many2one(
        related='receipt_payment_id.currency_id', string='Currency')
    move_currency_id = fields.Many2one(
        'res.currency', string='Move Line Currency')
    base_currency_id = fields.Many2one(
        'res.currency', compute='compute_base_currency', string='Move Line Currency')
    original_amount_currency = fields.Monetary(
        currency_field='move_currency_id', string='Original Currency Amount')
    original_amount = fields.Monetary(
        currency_field='base_currency_id', string='Base Currency Amount')
    amount_unreconciled_currency = fields.Monetary(
        currency_field='move_currency_id', string='Original Currency Open Balance')
    amount_unreconciled = fields.Monetary(
        currency_field='base_currency_id', string='Base Currency Open Balance')
    amount_residual = fields.Float(string='Amount Residual')
    reconcile = fields.Boolean('Full Reconcile')
    amount = fields.Monetary(currency_field='currency_id', string='Allocation')
    invoice_id = fields.Many2one('account.invoice', string='Invoice')
    payment_id = fields.Many2one('account.payment', string='Payment')
    apply_tds = fields.Boolean("Apply TDS")
    account_tax_id = fields.Many2one('account.tax', "Account Tax Id", domain=([('tax_group_id.name', '=', 'TDS')]))
    expected_tds_amount = fields.Monetary("Expected TDS")
    tds_amount = fields.Float("TDS Amount")
    payment_amount = fields.Float()
    untaxed_amount = fields.Float(string="Untaxed Amount")
    total_paid_amount = fields.Float("Paid Amount")

    @api.one
    @api.constrains('amount')
    def _check_amount(self):
        open_amount = self.move_currency_id.compute(
            abs(self.amount_unreconciled_currency), self.currency_id, False)
        if open_amount > 0 and float_round(self.amount, 2) > float_round(open_amount, 2):
            raise ValidationError(
                'Allocation amount must be less than or equal to Residual Amount.')
        # if self.receipt_payment_id.amount > 0 and float_round(self.amount, 2) > float_round(self.receipt_payment_id.amount, 2):
        #     raise ValidationError(
        #         'Allocation amount must be less than or equal to Paid Amount.')

    @api.onchange('account_tax_id')
    def update_tds(self):
        for rec in self:
            if rec.apply_tds and rec.account_tax_id:
                rec.tds_amount = (rec.invoice_id.amount_untaxed *
                                  rec.account_tax_id.amount) / 100
                rec.expected_tds_amount = rec.tds_amount
                rec.amount = rec.amount - rec.tds_amount
                rec.payment_amount = rec.amount - rec.tds_amount
            else:
                rec.tds_amount = 0.0
                rec.expected_tds_amount = 0.0

    @api.onchange('amount')
    def onchange_amount(self):
        self.payment_amount = self.amount
        self.total_paid_amount = self.amount + self.tds_amount

    @api.onchange('reconcile', 'amount')
    def onchange_reconcile(self):
        ctx = self._context or {}
        if self.reconcile and ctx.get('reconcile'):
            allocation_amount = self.move_currency_id.compute(
                abs(self.amount_unreconciled_currency), self.currency_id, False)
            self.amount = allocation_amount
            self.payment_amount = allocation_amount
        elif ctx.get('reconcile') and not self.reconcile:
            self.amount = 0.0
            self.payment_amount = 0.0
        elif ctx.get('amount') and float_round(self.amount, 2) >= float_round(self.amount_unreconciled_currency, 2):
            self.reconcile = True
        elif ctx.get('amount') and float_round(self.amount, 2) < float_round(self.amount_unreconciled_currency, 2):
            self.reconcile = False

ReceiptPaymentDebit()


class AccountMove(models.Model):
    _inherit = 'account.move'

    @api.multi
    def post(self):
        res = super(AccountMove, self).post()
        if self.name and not self.ref:
            self.ref = self.name
        return res

    @api.multi
    def assert_balanced(self):
        if not self.ids:
            return True
        prec = self.env['decimal.precision'].precision_get('Account')
        self._cr.execute("""\
            SELECT      move_id
            FROM        account_move_line
            WHERE       move_id in %s
            GROUP BY    move_id
            HAVING      abs(sum(debit) - sum(credit)) > %s
            """, (tuple(self.ids), 10 ** (-max(5, prec))))
        if len(self._cr.fetchall()) != 0:
            raise UserError(_("Cannot create unbalanced journal entry."))
        return True

AccountMove()


class AccountPayment(models.Model):
    _inherit = 'account.payment'

    difference = fields.Char('Difference')
    total_residual = fields.Char('Residual')

    @api.onchange('amount', 'currency_id')
    def _onchange_amount(self):
        res = super(AccountPayment, self)._onchange_payment_type()
        res['domain']['journal_id'].append(
            ('company_id', '=', self.env.user.company_id.id))
        return res

    def _create_payment_entry(self, amount):
        """ Create a journal entry corresponding to a payment, if the payment references invoice(s) they are reconciled.
            Return the journal entry.
            "Commented indian_tds_management's method(create_payment_entry) and merge its code with this method.
             Because both modules overrides this method."
        """
        aml_obj = self.env['account.move.line'].with_context(
            check_move_validity=False)
        invoice_currency = False
        if self.invoice_ids and all([x.currency_id == self.invoice_ids[0].currency_id for x in self.invoice_ids]):
            # if all the invoices selected share the same currency, record the
            # paiement in that currency too
            invoice_currency = self.invoice_ids[0].currency_id
        debit, credit, amount_currency, currency_id = aml_obj.with_context(
            date=self.payment_date).compute_amount_fields(amount, self.currency_id, self.company_id.currency_id,
                                                          invoice_currency)

        move = self.env['account.move'].create(self._get_move_vals())

        # Write line corresponding to invoice payment
        if self.payment_type == 'outbound' and self.apply_tds:
            counterpart_aml_dict = self._get_shared_move_line_vals(
                (debit + self.tds_amount), credit, amount_currency, move.id, False)
        elif self.payment_type == 'inbound' and self.apply_tds:
            counterpart_aml_dict = self._get_shared_move_line_vals(
                debit, self.amount_to_be_paid, amount_currency, move.id, False)
        else:
            counterpart_aml_dict = self._get_shared_move_line_vals(
                debit, credit, amount_currency, move.id, False)
        counterpart_aml_dict.update(
            self._get_counterpart_move_line_vals(self.invoice_ids))
        counterpart_aml_dict.update({'currency_id': currency_id})
        if not counterpart_aml_dict.get('account_id', False):
            counterpart_aml_dict[
                'account_id'] = self.invoice_ids[0].account_id.id
        # _logger.info('1: ' + str(counterpart_aml_dict))
        counterpart_aml = aml_obj.create(counterpart_aml_dict)
        if self.apply_tds:
            tds_move_line = self.tds_move_line_get(move)
            aml_obj.create(tds_move_line[0])

        # Reconcile with the invoices
        if self.payment_difference_handling == 'reconcile' and self.payment_difference:
            writeoff_line = self._get_shared_move_line_vals(
                0, 0, 0, move.id, False)
            amount_currency_wo, currency_id = aml_obj.with_context(date=self.payment_date).compute_amount_fields(
                self.payment_difference, self.currency_id, self.company_id.currency_id, invoice_currency)[2:]
            # the writeoff debit and credit must be computed from the invoice residual in company currency
            # minus the payment amount in company currency, and not from the payment difference in the payment currency
            # to avoid loss of precision during the currency rate computations.
            # See revision 20935462a0cabeb45480ce70114ff2f4e91eaf79 for a
            # detailed example.
            total_residual_company_signed = sum(
                invoice.residual_company_signed for invoice in self.invoice_ids)
            total_payment_company_signed = self.currency_id.with_context(
                date=self.payment_date).compute(self.amount, self.company_id.currency_id)
            if self.invoice_ids[0].type in ['in_invoice', 'out_refund']:
                amount_wo = total_payment_company_signed - total_residual_company_signed
            else:
                amount_wo = total_residual_company_signed - total_payment_company_signed
            # Align the sign of the secondary currency writeoff amount with the sign of the writeoff
            # amount in the company currency
            if amount_wo > 0:
                debit_wo = amount_wo
                credit_wo = 0.0
                amount_currency_wo = abs(amount_currency_wo)
            else:
                debit_wo = 0.0
                credit_wo = -amount_wo
                amount_currency_wo = -abs(amount_currency_wo)
            writeoff_line['name'] = _('Counterpart')
            writeoff_line['account_id'] = self.writeoff_account_id.id
            writeoff_line['debit'] = debit_wo
            writeoff_line['credit'] = credit_wo
            writeoff_line['amount_currency'] = amount_currency_wo
            writeoff_line['currency_id'] = currency_id
            writeoff_line = aml_obj.create(writeoff_line)
            if counterpart_aml['debit'] or writeoff_line['credit']:
                counterpart_aml['debit'] += credit_wo - debit_wo
            if counterpart_aml['credit'] or writeoff_line['debit']:
                counterpart_aml['credit'] += debit_wo - credit_wo
            counterpart_aml['amount_currency'] -= amount_currency_wo
        elif self.payment_difference_handling == 'reconcile' and self.difference and self.total_residual:
            writeoff_line = self._get_shared_move_line_vals(
                0, 0, 0, move.id, False)
            amount_currency_wo, currency_id = aml_obj.with_context(date=self.payment_date).compute_amount_fields(
                self.difference, self.currency_id, self.company_id.currency_id, invoice_currency)[2:]
            total_residual_company_signed = self.total_residual
            total_payment_company_signed = self.currency_id.with_context(
                date=self.payment_date).compute(self.amount, self.company_id.currency_id)
            # Align the sign of the secondary currency writeoff amount with the sign of the writeoff
            # amount in the company currency
            amount_wo = -float(self.difference)
            if amount_wo > 0:
                debit_wo = amount_wo
                credit_wo = 0.0
                amount_currency_wo = abs(amount_currency_wo)
            else:
                debit_wo = 0.0
                credit_wo = -amount_wo
                amount_currency_wo = -abs(amount_currency_wo)
            writeoff_line['name'] = _('Counterpart')
            writeoff_line['account_id'] = self.writeoff_account_id.id
            writeoff_line['debit'] = debit_wo
            writeoff_line['credit'] = credit_wo
            writeoff_line['amount_currency'] = amount_currency_wo
            writeoff_line['currency_id'] = currency_id
            writeoff_line = aml_obj.create(writeoff_line)

            if counterpart_aml['debit']:
                counterpart_aml['debit'] += credit_wo - debit_wo
            if counterpart_aml['credit']:
                counterpart_aml['credit'] += debit_wo - credit_wo
            counterpart_aml['amount_currency'] -= amount_currency_wo

        # Write counterpart lines
        if not self.currency_id.is_zero(self.amount):
            if not self.currency_id != self.company_id.currency_id:
                amount_currency = 0
            liquidity_aml_dict = self._get_shared_move_line_vals(
                credit, debit, -amount_currency, move.id, False)
            liquidity_aml_dict.update(
                self._get_liquidity_move_line_vals(-amount))
            # _logger.info('2: ' + str(liquidity_aml_dict))
            aml_obj.create(liquidity_aml_dict)
        move.post()
        if self.difference and self.total_residual:
            self.env.cr.execute(
                "SELECT id from receipt_payment ORDER BY id DESC")
            payment_reciept_id = self.env.cr.fetchone()[0]
            self.env['receipt.payment'].browse(
                payment_reciept_id).move_line_id = counterpart_aml.id
        self.invoice_ids.register_payment(counterpart_aml)
        return move

    def post(self):
        if self.env.context.get('type') == 'out_invoice':
            invoice = self.env['account.invoice'].browse(
                self.env.context.get('active_id'))
            vals = {}
            vals['partner_id'] = invoice.partner_id.id
            vals['amount'] = self.amount
            vals['journal_id'] = self.journal_id.id
            vals['date'] = fields.Datetime.now()
            vals['payment_ref'] = invoice.number
            vals['type'] = 'customer'
            vals['currency_id'] = self.currency_id.id
            debit_vals = {}
            for line in invoice.move_id.line_ids:
                if line.debit:
                    debit_vals['move_line_id'] = line.id
                    debit_vals['account_id'] = line.account_id.id
                    debit_vals['date'] = line.date
                    debit_vals['date_maturity'] = line.date_maturity
                    debit_vals['move_currency_id'] = line.currency_id.id
                    if self.currency_id != invoice.currency_id:
                        debit_vals['original_amount'] = invoice.currency_id.compute(
                            abs(invoice.amount_total), self.company_id.currency_id)
                    else:
                        debit_vals['original_amount'] = abs(line.balance)
                    debit_vals['original_amount_currency'] = line.amount_currency or abs(
                        line.balance)
                    debit_vals['amount_unreconciled_currency'] = abs(
                        line.amount_currency) or abs(line.balance)
                    if self.currency_id != invoice.company_id.currency_id:
                        debit_vals['amount_unreconciled'] = invoice.currency_id.compute(
                            abs(invoice.residual), invoice.company_id.currency_id)
                    else:
                        debit_vals['amount_unreconciled'] = abs(
                            invoice.residual)
                    debit_vals['amount_residual'] = abs(line.amount_residual)
                    debit_vals['reconcile'] = True
                    amount = self.currency_id.compute(
                        abs(self.amount), invoice.currency_id)
                    if amount > invoice.residual:
                        amount = invoice.residual
                    debit_vals['amount'] = amount
                    debit_vals['invoice_id'] = invoice.id
                    debit_vals['payment_id'] = self.id
            vals['line_dr_ids'] = [(0, 0, debit_vals)]
            payment = self.env['receipt.payment'].create(vals)
            payment.write(
                {'payment_ids': [(4, self.id)], 'payment_id': self.id, 'state': 'posted'})
        elif self.env.context.get('type') == 'in_invoice':
            invoice = self.env['account.invoice'].browse(
                self.env.context.get('active_id'))
            vals = {}
            vals['partner_id'] = invoice.partner_id.id
            vals['amount'] = self.amount
            vals['journal_id'] = self.journal_id.id
            vals['date'] = fields.Datetime.now()
            vals['payment_ref'] = invoice.number
            vals['type'] = 'supplier'
            vals['currency_id'] = self.currency_id.id
            credit_vals = {}
            for line in invoice.move_id.line_ids:
                if line.credit:
                    credit_vals['move_line_id'] = line.id
                    credit_vals['account_id'] = line.account_id.id
                    credit_vals['date'] = line.date
                    credit_vals['date_maturity'] = line.date_maturity
                    credit_vals['move_currency_id'] = line.currency_id.id
                    if self.currency_id != invoice.company_id.currency_id:
                        credit_vals['original_amount'] = invoice.currency_id.compute(
                            abs(invoice.amount_total), self.company_id.currency_id)
                    else:
                        credit_vals['original_amount'] = abs(line.balance)
                    credit_vals['original_amount_currency'] = line.amount_currency or abs(
                        line.balance)
                    credit_vals['amount_unreconciled_currency'] = abs(
                        line.amount_currency) or abs(line.balance)
                    if self.currency_id != invoice.company_id.currency_id:
                        credit_vals['amount_unreconciled'] = invoice.currency_id.compute(
                            abs(invoice.residual), invoice.company_id.currency_id)
                    else:
                        credit_vals['amount_unreconciled'] = abs(
                            invoice.residual)
                    credit_vals['amount_residual'] = abs(line.amount_residual)
                    credit_vals['reconcile'] = True
                    amount = self.currency_id.compute(
                        abs(self.amount), invoice.currency_id)
                    if amount > invoice.residual:
                        amount = invoice.residual
                    credit_vals['amount'] = amount
                    credit_vals['invoice_id'] = invoice.id
                    credit_vals['payment_id'] = self.id
            vals['line_cr_ids'] = [(0, 0, credit_vals)]
            payment = self.env['receipt.payment'].create(vals)
            payment.write(
                {'payment_ids': [(4, self.id)], 'payment_id': self.id, 'state': 'posted'})
        return super(AccountPayment, self).post()
