# -*- coding: utf-8 -*-
{
    'name': 'Mass Partner Payment',
    'author': 'Apagen Solutions Pvt. Ltd.',
    'version': '1.0',
    'summary': 'Customer and supplier payment',
    'description': 'This module includes customer and supplier bulk payment functionality',
    'website': 'www.apagen.com',
    'category': 'account',
    'depends': ['account_accountant'],
    'data': [
        'security/ir.model.access.csv',
        'views/receipt_payment_view.xml',
        'views/payment_report.xml',
        'views/payment_template.xml'
    ],
    'installable': True,
    'auto_install': False,
    'application': True,
}