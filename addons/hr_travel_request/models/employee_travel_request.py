# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2010 Tiny SPRL (<http://tiny.be>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
from odoo import api, fields, models, tools
from datetime import datetime, timedelta
from odoo.exceptions import UserError


class EmployeeTravel(models.Model):
    _name = 'employee.travel.request'
    # _inherit = ['mail.thread', 'mail.activity.mixin']
    _order = 'id DESC'
    _rec_name = 'name'
    _inherit = 'mail.thread'

    READONLY_STATES = {
        'awaiting_approval': [('readonly', True)],
        'to_finance_approve': [('readonly', True)],
        'approved': [('readonly', True)],
        'refused': [('readonly', True)],
        'cancelled': [('readonly', True)],
        'paid': [('readonly', True)],
    }

    @api.multi
    def employee_get(self):
        ids = self.env['hr.employee'].search(
            [('user_id.id', '=', self.env.uid)])
        if ids:
            return ids[0]
        return False

    name = fields.Char(
        'Name',
        required=True,
        index=True,
        copy=False,
        default='New')

    employee_id = fields.Many2one(
        'hr.employee',
        "Employee",
        readonly="0",
        default=employee_get,
        states=READONLY_STATES)
    # payment_id = fields.Many2one('account.payment', 'Payment')
    department_id = fields.Many2one(
        'hr.department',
        string='Department',
        # related='employee_id.department_id',
        store=True)
    manager_id = fields.Many2one(
        # related='department_id.manager_id',
        relation='hr.employee',
        store=True,
        string="Manager")
    advance_amount = fields.Float("Advance Amount")
    # advance_type = fields.Selection([
    #     ("travel", "Travel"),
    #     ("others", "Others"),
    # ], 'Advance Type', default="others", states=READONLY_STATES)
    job_id = fields.Many2one(
        'hr.job',
        string='Designation',
        # related='employee_id.job_id',
        store=True)
    state = fields.Selection([
        ("draft", "Draft"),
        ("awaiting_approval", "Awaiting HOD Approval"),
        ('to_travel_confirm', 'Awaiting Travel Confirmation'),
        ("bookings_confirm", "Bookings Confirmed"),
        ("refused", "Refused"),
        ("cancelled", "Cancelled"),
        ("rejected", "Rejected"),
        ("returned", "Returned"),
    ], 'State', default="draft", track_visibility='onchange')
    currency_id = fields.Many2one(
        'res.currency',
        'Currency',
        states=READONLY_STATES,
        default=lambda self: self.env.user.company_id.currency_id.id)
    # requested_by = fields.Many2one(
    #     'res.users',
    #     'Requested By',
    #     required=True,
    #     track_visibility='onchange',
    #     states=READONLY_STATES,
    #     default=lambda self: self.env.user)
    approved_by = fields.Many2one('res.users', string='Approval By')
    confirm_by = fields.Many2one('res.users', string='Booking Confirmed By')
    request_date = fields.Datetime(
        string='Request Date',
        default=fields.Datetime.now)
    confirm_date = fields.Datetime(string='Booking Confirmation Date')
    approved_date = fields.Datetime(string='Approved Date')
    request_departure_date = fields.Datetime(
        string='Requested Departure Date',
        states=READONLY_STATES,
        required=True)

    available_departure_date = fields.Datetime(
        string='Available Departure Date')

    available_return_date = fields.Datetime(
        string='Available Return Date')

    request_return_date = fields.Datetime(
        string='Requested Return Date',
        states=READONLY_STATES,
        required=True)
    created_expense_sheet = fields.Char(
        string='Created Expense Sheet',
        states=READONLY_STATES)
    travel_purpose = fields.Char(
        string='Purpose of Travel',
        states=READONLY_STATES,
        required=True)
    analytic_account_id = fields.Many2one(
        'account.analytic.account',
        'Project/Analytic Account',
        states=READONLY_STATES)
    request_mode_of_travel = fields.Many2one('travel.mode', string='Mode Of Travel', states=READONLY_STATES, required=True)
    departure_mode_of_travel = fields.Selection([
        ("flight", "Flight"),
        ("car", "Car"),
        ("public_transport", "Public Transport"),
    ], 'Departure Mode Of Travel')
    return_mode_of_travel = fields.Selection([
        ("flight", "Flight"),
        ("car", "Car"),
        ("public_transport", "Public Transport"),
    ], 'Return Mode Of Travel')
    days = fields.Float(string="Days", compute="_total_days_month")
    email = fields.Char(
        "Email",
        states=READONLY_STATES)
    contact_number = fields.Char(
        "Contact Number",
        states=READONLY_STATES)
    street = fields.Char(
        'Street',
        size=64,
        states=READONLY_STATES)
    street2 = fields.Char(
        'Street2',
        size=64,
        states=READONLY_STATES)
    city2 = fields.Char(
        'City',
        size=64,
        states=READONLY_STATES)
    state2 = fields.Many2one(
        "res.country.state",
        'State',
        states=READONLY_STATES)
    zip_ = fields.Char(
        'Zip',
        size=12,
        states=READONLY_STATES)
    current_user = fields.Boolean(copy=False, compute="check_current_user")
    country2_id = fields.Many2one(
        'res.country',
        'Country', readonly=True,
        default=lambda self: self.env['res.country'].search([('code', '=', 'IN')]))
    hr_expense_id = fields.One2many(
        'hr.expense',
        'hr_travel_id')
    city = fields.Char('City', size=64, states=READONLY_STATES)
    description = fields.Text('Description', states=READONLY_STATES)
    state1 = fields.Many2one(
        "res.country.state",
        'State',
        states=READONLY_STATES)
    country_id = fields.Many2one(
        'res.country',
        'Country', readonly=True,
        default=lambda self: self.env['res.country'].search([('code', '=', 'IN')]))
    bank_id = fields.Many2one('res.bank', "Bank Name")
    cheque_number = fields.Integer("Cheque Number")
    visa_agent_id = fields.Many2one('res.partner', "Visa Agent")
    ticket_booking_agent_id = fields.Many2one(
        'res.partner', "Ticket Booking Agent")
    reason = fields.Text()
    reason_other_mode = fields.Text()
    travel_type = fields.Selection([
        ("local", "Local"),
        ("other", "Other"),
    ], 'Travel Type', states=READONLY_STATES)
    # active = fields.Boolean(default=True)

    @api.multi
    def unlink(self):
        for rec in self:
            if rec.state not in ['draft']:
                raise UserError(
                    'You can only delete in draft state')
        return super(EmployeeTravel, self).unlink()

    def amount_paid(self):
        amount = 0.0
        payment_obj = self.env['account.payment']
        payment_method = self.env['account.payment.method'].search(
            [('code', '=', 'manual')], limit=1).id
        journal_id = self.env['account.journal'].search(
            [('type', 'in', ['bank', 'cash'])], limit=1).id
        for rec_id in self:
            rec_id.write({'state': 'paid'})
            payment = payment_obj.create({
                'payment_type': 'outbound',
                'partner_id': self.requested_by.partner_id.id,
                'amount': amount,
                'payment_method_id': payment_method,
                'journal_id': journal_id,
            })
            rec_id.payment_id = payment
        return True

    @api.depends('request_departure_date', 'request_return_date')
    def _total_days_month(self):
        for sheet in self:
            if sheet.request_return_date:
                request_return_date = datetime.strptime(
                    str(sheet.request_return_date), "%Y-%m-%d %H:%M:%S")
                request_departure_date = datetime.strptime(
                    str(sheet.request_departure_date), "%Y-%m-%d %H:%M:%S")
                sheet.days = (request_return_date -
                              request_departure_date).days
        return sheet.days

    @api.multi
    def check_current_user(self):
        user = self.env.uid
        for record in self:
            if record.department_id.manager_id.user_id.id == user:
                record.current_user = True
            elif self.env.user.has_group('hr_employee_register.group_hr_department_head'):
                record.current_user = True
            else:
                record.current_user = False

    @api.model
    def create(self, vals):
        if vals.get('name', 'New') == 'New':
            vals['name'] = self.env['ir.sequence'].next_by_code(
                'employee.sequence') or '/'
        employee = self.env['hr.employee'].search(
            [('id', '=', vals.get('employee_id'))])
        contract_obj = self.env['hr.contract'].search(
            [('employee_id', '=', vals.get('employee_id'))])
        vals.update({
            'department_id': employee.department_id.id,
            'job_id': employee.job_id.id,
        })
        return super(EmployeeTravel, self).create(vals)

    @api.multi
    def write(self, vals):
        employee = self.env['hr.employee'].search(
            [('id', '=', vals.get('employee_id'))])
        contract_obj = self.env['hr.contract'].search(
            [('employee_id', '=', vals.get('employee_id'))])
        if 'employee_id' in vals:
            vals.update({
                'department_id': employee.department_id.id,
                'job_id': employee.job_id.id,
            })
        return super(EmployeeTravel, self).write(vals)

    @api.multi
    @api.onchange('employee_id')
    def onchange_employee(self):
        if self.employee_id:
            employee = self.env['hr.employee'].browse(self.employee_id).id
            self.department_id = self.employee_id.department_id
            self.job_id = self.employee_id.job_id

    @api.multi
    def sent_for_approval(self):
        template = self.env.ref(
            'hr_travel_request.email_template_employee_travel_submittion')
        user_obj = self.env['res.users'].search([('id', '=', 1)])
        users = self.env['res.users'].search([])
        mail = ''
        for user in users:
            if user.has_group('hr_employee_register.group_hr_department_head'):
                mail += user.login + ','
        if template:
            template.email_to = mail
            template.email_from = user_obj.login
            template.send_mail(self.id, force_send=True)
        self.write({
            'state': 'awaiting_approval',
            'request_date': datetime.now(),
        })
        return True

    @api.multi
    def button_approve(self):
        template = self.env.ref(
            'hr_travel_request.email_template_employee_travel_approved')
        user_obj = self.env['res.users'].search([('id', '=', 1)])
        if template:
            template.email_to = "hr@traqmatix.com"
            template.email_from = user_obj.login
            template.send_mail(self.id, force_send=True)
        if self.state == 'awaiting_approval':
            self.write({
                'state': 'to_travel_confirm',
                'approved_by': self.env.uid,
                'approved_date': datetime.now()})
        return True

    @api.multi
    def button_awaiting_approval(self):
        template = self.env.ref(
            'hr_travel_request.email_template_employee_travel_confirmed')
        user_obj = self.env['res.users'].search([('id', '=', 1)])
        if template:
            template.email_from = user_obj.login or ''
            template.send_mail(self.id, force_send=True)
        if self.state == 'to_travel_confirm':
            self.write({
                'state': 'bookings_confirm',
                'confirm_by': self.env.uid,
                'confirm_date': datetime.now()})

    @api.multi
    def action_refuse(self):
        if not self.reason:
            raise UserError('Please enter reason of Refusal')
        self.state = 'refused'

    @api.multi
    def action_cancel(self):
        self.state = 'cancelled'

    @api.multi
    def action_reject(self):
        self.state = 'rejected'

    @api.multi
    def action_return(self):
        self.state = 'returned'

    @api.multi
    def action_draft(self):
        self.write({'state': 'draft', 'current_user': False})

    @api.multi
    def action_expense_button(self, line):
        for data in self:
            move = self.env['hr.expense.sheet'].create({
                'name': data.travel_purpose,
            })
            for record in self:
                move.update({'expense_line_ids': self.hr_expense_id})
                res = {'name': ('hr.expense.sheet'),
                       'view_type': 'form',
                       'view_mode': 'form',
                       'res_model': 'hr.expense.sheet',
                       'type': 'ir.actions.act_window',
                       'res_id': move.id}
                return res


class hr_expense(models.Model):
    _inherit = "hr.expense"

    hr_travel_id = fields.Many2one('employee.travel.request', 'Hr Expense')



class TravelMode(models.Model):
    _name = 'travel.mode'

    name = fields.Char("Travel Name")
