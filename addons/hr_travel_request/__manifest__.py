# -*- coding: utf-8 -*-

{
    'name': 'Employee Travel Request',
    'version': '1.0',
    'category': 'HR',
    'sequence': 14,
    'summary': 'Employee request for travel',
    'description': """Travel Request specific usage of company
    """,
    'author': 'Apagen Solutions Pvt. Ltd.',
    'website': 'https://www.apagen.com',
    'depends': [
        'product',
        'base',
        'project',
        'hr_expense',
        'base_hr',
        'hr_employee_register'
        # 'project_financial_security'
    ],
    'data': [
        'security/ir_rules.xml',
        'security/ir.model.access.csv',
        'views/employee_travel_request_view.xml',
        'data/travel_request_sequence.xml',
        'data/email_templates_travel.xml',
    ],
    'installable': True,
    'auto_install': False,
    'application': True,
}
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
