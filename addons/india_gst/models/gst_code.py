# -*- coding: utf-8 -*-

from odoo import api, fields, models, _
import re
from odoo.exceptions import UserError
from odoo.tools.translate import _

class CountryState(models.Model):
    _description = "Country state"
    _inherit = 'res.country.state'

    state_code = fields.Char('Code', help='Numeric State Code ')
    tin_prefix_2_digit = fields.Char('TIN Prefix')


class AccountInvoice(models.Model):
    _inherit = "account.invoice"
    _name = "account.invoice"

    elec_ref = fields.Char('Electronic Reference')
    ship_bill_date = fields.Date('Shipping Bill Date')
    ship_bill_no = fields.Char('Shipping Bill No.')
    bill_period_from = fields.Datetime('Bill From')
    bill_period_to = fields.Datetime('Bill To')
    port_code = fields.Many2one('port.code')
    place_supply = fields.Many2one('res.country.state', 'Place of Supply')
    date_supply = fields.Date('Date of Supply')
    export_invoice = fields.Boolean('Export Invoice')
    reverse_charge = fields.Boolean('Reverse Charge')
    export_type = fields.Selection([('deemed', 'Deemed'),
                                    ('exp_under_bond', 'Export under bond/LUT'),
                                    ('exp_with_igst', 'Export with IGST'),
                                    ('SEZ supplies with payment',
                                     'SEZ supplies with payment'),
                                    ('SEZ supplies without payment',
                                        'SEZ supplies without payment')],
                                   default='deemed')
    # invoice_type = fields.Selection(
    #     [('Regular', 'Regular'),
    #      ('SEZ supplies with payment', 'SEZ supplies with payment'),
    #      ('SEZ supplies without payment', 'SEZ supplies without payment'),
    #      ('Deemed Export', 'Deemed Export')],
    #     string="Invoice Type", default='Regular', required=True)
    invoice_type = fields.Selection([
        ('b2b', 'B2B'),
        ('b2cl', 'B2CL'),
        ('b2cs', 'B2CS'),
        ('b2bur', 'B2BUR'),
        ('import', 'Imps/Impg'),
        ('export', 'Export'),
        ('gst', 'GST'),
        ('bill', 'Bill'),
    ], string="GST Invoice Type", default='b2b')
    country_supply = fields.Many2one('res.country', 'Country of Supply')
    gst_status = fields.Selection([
        ('not_uploaded', 'Not Uploaded'),
        ('ready_to_upload', 'Ready to upload'),
        ('uploaded', 'Uploaded to gov'),
        ('filed', 'Filed'),
    ], string="GST Status", help="", default='not_uploaded')
    itc_eligibility = fields.Selection([
        ('inputs', 'Inputs'),
        ('capital_goods', 'Capital Goods'),
        ('input_services', 'Input Services'),
        ('ineligible', 'Ineligible'),
    ], string="ITC Eligibility")

    @api.multi
    def print_gst_invoice_summary(self):
        self.write({'printed': True})
        return self.env.ref('india_gst.report_gst_invoice_menu').report_action(self)

    @api.multi
    def print_gst_summary(self):
        self.write({'printed': True})
        # return self.env.ref('india_gst.report_gst_invoice_menu_summary').report_action(self)
        return self.env['report'].get_action(self, 'india_gst.report_invoice_gst')

    def get_gst(self, inv_id, product_id):
        invoice = self.search([('id', '=', inv_id)], limit=1)
        tax_amount = 0
        rate = 0

        for num in invoice.invoice_line_ids:
            if num.product_id.id == product_id:

                tax_rate = 0
                for i in num.invoice_line_tax_ids:

                    if i.children_tax_ids:
                        tax_rate = sum(i.children_tax_ids.mapped('amount'))

                tax_amount = ((tax_rate / 100) * num.price_subtotal) / 2
                rate = tax_rate / 2
        return [rate, tax_amount]

    def get_igst(self, inv_id, product_id):
        invoice = self.search([('id', '=', inv_id)], limit=1)
        tax_amount = 0
        rate = 0
        for i in invoice.invoice_line_ids:
            if i.product_id.id == product_id:
                tax_rate = 0
                for t in i.invoice_line_tax_ids:
                    if not t.children_tax_ids:
                        tax_rate = t.amount
                tax_amount = (tax_rate / 100) * i.price_subtotal
                rate = tax_rate
        return [rate, tax_amount]


class PortCode(models.Model):
    _name = 'port.code'

    name = fields.Char('Port Name')
    code = fields.Char('Port Code')

# class ProFormaInvoice(models.Model):
#     _inherit = "pro.forma.invoice"

#     place_supply = fields.Many2one('place.supply', 'Place of Supply')
#     country_supply = fields.Many2one('res.country', 'Country of Supply')
#     port_code = fields.Many2one('port.code')

# class DeliveryChallan(models.Model):
#     _inherit = 'delivery.challan'

#     supply_place = fields.Many2one(
#         'place.supply', 'Place of Supply')

class PlaceSupply(models.Model):
    _name = 'place.supply'

    name = fields.Char('Name', required=True)


# class Partner(models.Model):
#     _inherit = 'res.partner'

#     vat = fields.Char(string="GSTIN/UIN")
#     registration_type = fields.Selection([
#         ('uin', 'UIN Holder'),
#         ('regular', 'Regular GST'),
#         ('consumer', 'Consumer'),
#         ('unregister', 'Unregister'),
#         ('composition', 'Composition'),
#         ('deemed', 'Deemed Impex'),
#         ('exempt_ecommerce', 'E-Commerce'),
#         ('exempt', 'Exempt'),
#         ('sez', 'SEZ')
#     ], string="GST Registration Type")

#     tan_no = fields.Char("TAN", size=10)
#     pan_no = fields.Char("PAN", size=10)
#     cin_no = fields.Char("CIN")
#     check_pan = fields.Boolean(default=True)

#     @api.onchange('vat')
#     def check_gst_in(self):
#         if not self.vat:
#             self.check_pan = False
#         else:
#             self.check_pan = True

#     @api.onchange('cin_no')
#     def onchange_cin_no(self):
#         for rec in self:
#             if rec.cin_no:
#                 if len(rec.cin_no) > 21:
#                     raise UserError(_('Company Identification Number(CIN) should be unique 21 digit alpha-numeric number'))

#     @staticmethod
#     def check_gstin_chksum(gstin_num):
#         gstin_num = gstin_num.upper()
#         keys = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F', 'G',
#                 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z']
#         values = range(36)
#         hash = {k: v for k, v in zip(keys, values)}
#         index = 0
#         sum = 0
#         while index < len(gstin_num) - 1:
#             lettr = gstin_num[index]
#             tmp = (hash[lettr]) * ((index % 2) + 1)  # Factor =1 fr index odd
#             sum += tmp // 36 + tmp % 36
#             index = index + 1
#         Z = sum % 36
#         Z = (36 - Z) % 36
#         if((hash[(gstin_num[-1:])]) == Z):
#             return True
#         return False

#     @api.onchange('vat')
#     def do_stuff(self):
#         try:
#             # CHECK FORMAT AND SHOW WARNING
#             if not((self.vat)):
#                 return
#             if(len(self.vat) != 15):
#                 return {
#                     'warning': {'title': 'Warning', 'message': 'Invalid GSTIN. GSTIN number must be 15 digits. Please check.', },
#                 }
#                 # raise ValidationError("Invalid GSTIN. GSTIN number must be 15 digits. Please check.")
#             if not(re.match("\d{2}[A-Z]{5}\d{4}[A-Z]{1}\d[Z]{1}[A-Z\d]{1}", self.vat.upper())):
#                 return {
#                     'warning': {'title': 'Warning', 'message': 'Invalid GSTIN format.\r\n.GSTIN must be in the format nnAAAAAnnnnA_Z_ where n=number, A=alphabet, _=either.', },
#                 }
#                 # raise ValidationError("Invalid GSTIN.\r\n.GSTIN must be in the format nnAAAAAnnnnA_Z_ where n=number, A=alphabet, _=either.")
#             if not(Partner.check_gstin_chksum(self.vat)):
#                 return {
#                     'warning': {'title': 'Warning', 'message': 'Invalid GSTIN. Checksum validation failed. It means one or more characters are probably wrong.', },
#                 }
#                 # raise ValidationError("Invalid GSTIN. Checksum validation failed. It means one or more characters are probably wrong.")
#             self.vat = self.vat.upper()
#         except:
#             pass


class HSNSACCode(models.Model):
    _name = 'hsn.sac.code'
    _inherit = ['mail.thread']
    _description = "HSN SAC Code"

    name = fields.Char(track_visibility='always')
    company_id = fields.Many2one('res.company', default=lambda self: self.env.user.company_id.id)
    customer_tax_ids = fields.Many2many(
        'account.tax',
        'account_customer_tax',
        'customer_id', 'tax_id')
    vendor_tax_ids = fields.Many2many(
        'account.tax',
        'account_vendor_tax',
        'vendor_id', 'tax_id')

    @api.model
    def create(self, vals):
        records = self.search([('name', '=', vals.get('name')), ('company_id', '=', vals.get('company_id'))])
        for hsn in records:
            if hsn.name == vals.get('name') and hsn.customer_tax_ids.ids == vals.get('customer_tax_ids')[0][2] and hsn.vendor_tax_ids.ids == vals.get('vendor_tax_ids')[0][2]:
                raise UserError(_('You can not create multiple HSN SAC Code with same taxes.'))
        return super(HSNSACCode, self).create(vals)

    @api.onchange('name')
    def onchange_name(self):
        for rec in self:
            if rec.name:
                if not rec.name.isdigit():
                    raise UserError(_('HSN/SAC Code can only be numbers.'))
                if len(rec.name) > 8:
                    raise UserError(_('HSN/SAC Code can not greater than eigth digits.'))


class Company(models.Model):
    _inherit = 'res.company'

    tan_no = fields.Char("TAN", size=10)
    vat = fields.Char("GSTIN/UIN")


class ProductTemplate(models.Model):
    _inherit = 'product.template'

    hsn_sac_id = fields.Many2one('hsn.sac.code')

    @api.onchange('hsn_sac_id')
    def onchange_hsn_sac_id(self):
        for rec in self:
            if rec.hsn_sac_id:
                rec.taxes_id = rec.hsn_sac_id.customer_tax_ids.ids
                rec.supplier_taxes_id = rec.hsn_sac_id.vendor_tax_ids.ids


class ProductProduct(models.Model):
    _inherit = 'product.product'

    hsn_sac_id = fields.Many2one('hsn.sac.code', related='product_tmpl_id.hsn_sac_id')

    @api.onchange('hsn_sac_id')
    def onchange_hsn_sac_id(self):
        for rec in self:
            if rec.hsn_sac_id:
                rec.taxes_id = rec.hsn_sac_id.customer_tax_ids.ids
                rec.supplier_taxes_id = rec.hsn_sac_id.vendor_tax_ids.ids
