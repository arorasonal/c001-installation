WITH x AS (
    SELECT
        a.id,
        a.name AS contract_name,
        a.date_start,
        a.recurring_next_date AS next_invoice_date,
        a.journal_id,
        c.name AS journal,
        a.state,
        a.partner_id,
        b.name AS customer
    FROM
        account_analytic_account a
        JOIN res_partner b ON b.id = a.partner_id
        JOIN account_journal c ON c.id = a.journal_id
    WHERE
        a.active = TRUE
        AND a.state IN ('open', 'pending'))
SELECT
    x.*,
    e.product_id,
    e.quantity,
    e.price_unit,
    pt.name AS product_name,
    e.name AS contract_name,
    pt.product_challan,
    pt.categ_id,
    pc.name AS product_category
FROM
    x
    JOIN account_analytic_invoice_line e ON x.id = e.analytic_account_id
    JOIN product_product pp ON pp.id = e.product_id
    JOIN product_template pt ON pt.id = pp.product_tmpl_id
    JOIN product_category pc ON pc.id = pt.categ_id
