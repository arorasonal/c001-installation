# -*- coding: utf-8 -*-
# © 2016 Carlos Dauden <carlos.dauden@tecnativa.com>
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl).

from odoo import fields, models, api


class AccountInvoice(models.Model):
    _inherit = 'account.invoice'

    contract_id = fields.Many2one(
        'account.analytic.account',
        string='Contract')

    @api.multi
    def action_invoice_open(self):
        # updated last billing period of contract
        res = super(AccountInvoice, self).action_invoice_open()
        for rec in self:
            if rec.contract_id:
                if rec.bill_period_from:
                    rec.contract_id.last_inv_from = rec.bill_period_from
                if rec.bill_period_to:
                    rec.contract_id.last_inv_to = rec.bill_period_to
                if rec.date_invoice:
                    rec.contract_id.last_inv_date = rec.date_invoice

        return res
