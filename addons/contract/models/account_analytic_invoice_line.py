# -*- coding: utf-8 -*-
# Copyright 2017 LasLabs Inc.
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl).

from odoo import fields, models


class AccountAnalyticInvoiceLine(models.Model):
    _name = 'account.analytic.invoice.line'
    _inherit = 'account.analytic.contract.line'

    analytic_account_id = fields.Many2one(
        'account.analytic.account',
        string='Analytic Account',
        required=True,
        ondelete='cascade',track_visibility="onchange"
    )
    product_id = fields.Many2one(
        'product.product',
        string='Product',
        required=False,track_visibility="onchange"
    )
    name = fields.Text(
        string='Description',
        required=False,track_visibility="onchange"
    )
    odoo8_id = fields.Integer()
    imported = fields.Boolean()