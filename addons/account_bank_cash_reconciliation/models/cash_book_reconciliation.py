# -*- coding: utf-8 -*-

from odoo import fields, models, api, _
from datetime import datetime
import odoo.addons.decimal_precision as dp
from odoo.exceptions import UserError


class AccountMoveLine(models.Model):
    _inherit = "account.move.line"

    amount_to_pay = fields.Float('Amount')


class CashBookReconcillation(models.Model):
    _name = "cash.book.reconcilation"
    _inherit = ['mail.thread']

    @api.multi
    def write(self, vals):
        return super(CashBookReconcillation, self).write(vals)

    @api.multi
    def unlink(self):
        "Reset the related account.move.line to be re-assigned later to statement."
        for statement in self:
            for line in statement.credit_move_line_ids:
                line.unlink()
            for line in statement.debit_move_line_ids:
                line.unlink()
        return super(CashBookReconcillation, self).unlink()

    def check_difference_balance(self):
        "Check if difference balance is zero or not."
        for statement in self:
            if statement.difference != 0.0:
                raise UserError(
                    _('Warning!'
                      "Prior to reconciling a statement, all differences must be accounted for and the Difference balance must be zero."
                      " Please review and make necessary changes."))
        return True

    @api.multi
    def action_cancel(self):
        "Cancel the the statement."
        self.write({'state': 'cancel'})
        return True

    @api.multi
    def action_review(self):
        "Change the status of statement from 'draft' to 'to_be_reviewed'."
        # If difference balance not zero prevent further processing
        self.check_difference_balance()
        self.write({
            'state': 'to_be_reviewed',
            'verified_by_user_id': self.env.uid,
            'verified_date': datetime.now()
        })
        return True

    @api.multi
    def action_process(self):
        """Set the account move lines as 'Cleared' and Assign 'Bank Acc Rec Statement ID'
        for the statement lines which are marked as 'Cleared'."""
        account_move_line_obj = self.env['account.move.line']
        statement_line_obj = self.env['cash.reconcilation.line']
        # If difference balance not zero prevent further processing
        self.check_difference_balance()
        for statement in self:
            statement_lines = statement.credit_move_line_ids + statement.debit_move_line_ids
            for statement_line in statement_lines:
                # Mark the move lines as 'Cleared'mand assign the 'Bank Acc Rec
                # Statement ID'
                account_move_line_obj.write({
                    'cleared_bank_account': statement_line.cleared_bank_account,
                    'bank_acc_rec_statement_id': statement_line.cleared_bank_account and statement.id or False
                })
            self.write({'state': 'done',
                        'verified_by_user_id': self.env.user.id,
                        'verified_date': fields.Datetime.now(),
                        })
        return True

    @api.multi
    def action_cancel_draft(self):
        """Reset the statement to draft and perform resetting operations."""
        account_move_line_obj = self.env['account.move.line']
        statement_line_obj = self.env['cash.reconcilation.line']
        for statement in self:
            statement_lines = statement.credit_move_line_ids + statement.debit_move_line_ids
            line_ids = []
            statement_line_ids = []
            for statement_line in statement_lines:
                statement_line_ids.append(statement_line.id)
                # Find move lines related to statement lines
                line_ids.append(statement_line.move_line_id.id)

            # Reset 'Cleared' and 'Bank Acc Rec Statement ID' to False
            account_move_line_obj.write({'cleared_bank_account': False,
                                         'bank_acc_rec_statement_id': False,
                                         })
            # Reset 'Cleared' in statement lines
            statement_line_obj.write({'cleared_bank_account': False,
                                      'research_required': False
                                      })
            # Reset statement
            self.write({'state': 'draft',
                        'verified_by_user_id': False,
                        'verified_date': False
                        })
        return True

    @api.multi
    def action_select_all(self):
        """Mark all the statement lines as 'Cleared'."""
        for statement in self:
            statement_lines = statement.credit_move_line_ids + statement.debit_move_line_ids
            for line in statement_lines:
                line.write({'cleared_bank_account': True})
        return True

    @api.multi
    def action_unselect_all(self):
        """Reset 'Cleared' in all the statement lines."""
        for statement in self:
            statement_lines = statement.credit_move_line_ids + statement.debit_move_line_ids
            for line in statement_lines:
                line.write({'cleared_bank_account': False})
        return True

    @api.multi
    def _get_balance(self):
        """Computed as following:
        A) Deposits, Credits, and Interest Amount: Total SUM of Amts of lines with Cleared = True
        Deposits, Credits, and Interest # of Items: Total of number of lines with Cleared = True
        B) Checks, Withdrawals, Debits, and Service Charges Amount:
        Checks, Withdrawals, Debits, and Service Charges Amount # of Items:
        Cleared Balance (Total Sum of the Deposit Amount Cleared (A) – Total Sum of Checks Amount Cleared (B))
        Difference= (Ending Balance – Beginning Balance) - cleared balance = should be zero.
"""
        res = {}
        account_precision = self.env[
            'decimal.precision'].precision_get('Account')
        for statement in self:
            for line in statement.credit_move_line_ids:
                statement.sum_of_credits += line.cleared_bank_account and round(
                    line.amount, account_precision) or 0.0
                statement.sum_of_credits_lines += line.cleared_bank_account and 1.0 or 0.0
            for line in statement.debit_move_line_ids:
                statement.sum_of_debits += line.cleared_bank_account and round(
                    line.amount, account_precision) or 0.0
                statement.sum_of_debits_lines += line.cleared_bank_account and 1.0 or 0.0
                statement.cleared_balance = round(
                    statement.sum_of_debits - statement.sum_of_credits, account_precision)
                statement.difference = round(
                    (statement.ending_balance - statement.starting_balance) - statement.cleared_balance, account_precision)

    @api.multi
    def refresh_record(self):
        account_move_line_obj = self.env['account.move.line']
        account_move_search = account_move_line_obj.search([])
        statement_line_obj = self.env['cash.reconcilation.line']
        # refresh code
        if self.account_id:
            for statement in self:
                statement_line_ids = statement_line_obj.search(
                    [('statement_id', '=', statement.id)])
                for line in statement_line_ids:
                    line.unlink()
        val = {}
        search_domain = [
            ('account_id', '=', self.account_id.id),
            ('date', '>=', self.start_date),
            ('date', '<=', self.ending_date),
            ('move_id.state', '=', 'posted'),
        ]
        account_order_ids = account_move_line_obj.search(search_domain)
        if account_order_ids:
            for line in account_order_ids:
                val = {
                    'ref': line.ref,
                    'date': line.date,
                    'partner_id': line.partner_id.id,
                    'currency_id': line.currency_id.id,
                    'amount': line.credit or line.debit,
                    'name': line.name,
                    'move_line_id': line.id,
                    'type': line.credit and 'cr' or 'dr'
                }

                if val['type'] == 'cr':
                    self.write({'credit_move_line_ids': [(0, 0, val)]})
                else:
                    self.write({'debit_move_line_ids': [(0, 0, val)]})
        return True

    def print_reconciliation_report(self):
        inv_obj = self.env["cash.book.reconcilation"]
        inv_id = inv_obj.search([('id', '=', self.id)])
        datas = {
            'ids': inv_id,
            'model': 'cash.book.reconcilation',
            #             'form': data
        }
        return {
            'type': 'ir.actions.report.xml',
            'report_name': 'cash.book.reconcilation',
            'datas': datas,
        }

    '''def onchange_account_id(self, cr, uid, ids, account_id, ending_date, suppress_ending_date_filter, context=None):
        account_move_line_obj = self.env['account.move.line')
        statement_line_obj = self.env['cash.reconcilation.line')
        val = {'value': {'credit_move_line_ids': [], 'debit_move_line_ids': []}}
        if account_id:
            for statement in self.browse(cr, uid, ids, context=context):
                statement_line_ids = statement_line_obj.search(cr, uid, [('statement_id', '=', statement.id)], context=context)
                # call unlink method to reset and remove existing statement lines and
                # mark reset field values in related move lines
                statement_line_obj.unlink(cr, uid, statement_line_ids, context=context)

            # Apply filter on move lines to allow
            #1. credit and debit side journal items in posted state of the selected GL account
            #2. Journal items which are not assigned to previous bank statements
            #3. Date less than or equal to ending date provided the 'Suppress Ending Date Filter' is not checkec
            domain = [('account_id', '=', account_id), ('move_id.state', '=', 'posted'), ('cleared_bank_account', '=', False), ('draft_assigned_to_statement', '=', False)]
            if not suppress_ending_date_filter:
                domain += [('date', '<=', ending_date)]
            line_ids = account_move_line_obj.search(cr, uid, domain, context=context)
            for line in account_move_line_obj.browse(cr, uid, line_ids, context=context):
                res = {
                       'ref': line.ref,
                       'date': line.date,
                       'partner_id': line.partner_id.id,
                       'currency_id': line.currency_id.id,
                       'amount': line.credit or line.debit,
                       'name': line.name,
                       'move_line_id': line.id,
                       'type': line.credit and 'cr' or 'dr'
                }
                if res['type'] == 'cr':
                    val['value']['credit_move_line_ids'].append(res)
                else:
                    val['value']['debit_move_line_ids'].append(res)
        return val'''

    name = fields.Char(
        'Name',
        size=64, copy=False,
        states={'done': [('readonly', True)]},
        help="This is a unique name identifying the statement (e.g. Bank X January 2012).")
    account_id = fields.Many2one(
        'account.account',
        'Account',
        required=True,
        domain=[('user_type_id', '=', 'Bank and Cash')],
        states={'done': [('readonly', True)]},
        help="The Bank/Gl Account that is being reconciled.")
    start_date = fields.Date('Start Date', copy=False,)
    ending_date = fields.Date('End Date', copy=False,)
    starting_balance = fields.Float(
        'Starting Balance',
        required=True,
        digits=dp.get_precision('Account'),
        help="The Starting Balance on your bank statement.",
        states={'done': [('readonly', True)]})
    ending_balance = fields.Float(
        'Ending Balance',
        required=True,
        digits=dp.get_precision('Account'),
        help="The Ending Balance on your bank statement.",
        states={'done': [('readonly', True)]})
    company_id = fields.Many2one(
        'res.company',
        'Company',
        required=True,
        readonly=True,
        states={'draft': [('readonly', False)]},
        default=lambda self: self._get_company())
    notes = fields.Text('Notes')
    verified_date = fields.Datetime(
        'Verified Date', copy=False,
        states={'done': [('readonly', True)]},
        help="Date in which Deposit Ticket was verified.")
    verified_by_user_id = fields.Many2one(
        'res.users', 'Verified By', copy=False,
        states={'done': [('readonly', True)]},
        help="Entered automatically by the “last user” who saved it. System generated.")
    credit_move_line_ids = fields.One2many(
        'cash.reconcilation.line',
        'statement_id', 'Credits', copy=False,
        domain=[('type', '=', 'cr')],
        context={'default_type': 'cr'},
        states={'done': [('readonly', True)]})
    debit_move_line_ids = fields.One2many(
        'cash.reconcilation.line',
        'statement_id', 'Debits', copy=False,
        domain=[('type', '=', 'dr')],
        context={'default_type': 'dr'},
        states={'done': [('readonly', True)]})
    cleared_balance = fields.Float(
        compute='_get_balance',
        string='Cleared Balance',
        help="Total Sum of the Deposit Amount Cleared – Total Sum of Checks, Withdrawals, Debits, and Service Charges Amount Cleared",
        multi="balance")
    difference = fields.Float(
        compute='_get_balance',
        string='Difference',
        help="(Ending Balance – Beginning Balance) - Cleared Balance.",
        multi="balance")
    sum_of_credits = fields.Float(
        compute='_get_balance',
        string='Withdrawals and Debits Amount',
        help="Total SUM of Amts of lines with Cleared = True",
        multi="balance")
    sum_of_debits = fields.Float(
        compute='_get_balance',
        string='Deposits and Credits Amount',
        help="Total SUM of Amts of lines with Cleared = True",
        multi="balance")
    sum_of_credits_lines = fields.Float(
        compute='_get_balance',
        string='Withdrawals and Debits Number of Items',
        help="Total of number of lines with Cleared = True",
        multi="balance")
    sum_of_debits_lines = fields.Float(
        compute='_get_balance',
        string='Deposits and Credits Number of Items',
        help="Total of number of lines with Cleared = True",
        multi="balance")
    suppress_ending_date_filter = fields.Boolean(
        'Remove Ending Date Filter',
        help="If this is checked then the Statement End Date filter on the transactions below will not occur. All transactions would come over.")
    created_by = fields.Many2one('res.users', default=lambda self: self.env.uid)
    state = fields.Selection(
        [('draft', 'Draft'),
         ('to_be_reviewed', 'Submit for Review'),
         ('done', 'Done'),
         ('cancel', 'Cancelled')],
        'State', index=True, readonly=True,
        default='draft', track_visibility='onchange')

    @api.model
    def _get_company(self):
        return self._context.get('company_id', self.env.user.company_id.id)

    # _defaults = {
    #     'state': 'draft',
    #     'company_id': lambda self, cr, uid, c: self.env['res.users').browse(cr, uid, uid, c).company_id.id,
    #     'ending_date': time.strftime('%Y-%m-%d'),
    # }
    _order = "ending_date desc"
    _sql_constraints = [
        ('name_company_uniq', 'unique (name, company_id, account_id)',
         'The name of the statement must be unique per company and G/L account!')
    ]


class CashBookReconcillationLine(models.Model):
    _name = "cash.reconcilation.line"
    _description = "Statement Line"

    name = fields.Char(
        'Name', size=64, help="Derived from the related Journal Item.")
    ref = fields.Char('Reference', size=64,
                      help="Derived from related Journal Item.")
    partner_id = fields.Many2one(
        'res.partner', string='Partner', help="Derived from related Journal Item.")
    amount = fields.Float(
        'Amount',
        digits=dp.get_precision('Account'),
        help="Derived from the 'debit' amount from related Journal Item.")
    date = fields.Date('Date', required=True,
                       help="Derived from related Journal Item.")
    statement_id = fields.Many2one(
        'cash.book.reconcilation', 'Statement', required=True, ondelete='cascade')
    move_line_id = fields.Many2one(
        'account.move.line', 'Journal Item', help="Related Journal Item.")
    cleared_bank_account = fields.Boolean(
        'Cleared? ', help='Check if the transaction has cleared from the bank')
    research_required = fields.Boolean(
        'Research Required? ', help='Check if the transaction should be researched by Accounting personal')
    currency_id = fields.Many2one(
        'res.currency', 'Currency', help="The optional other currency if it is a multi-currency entry.")
    type = fields.Selection([('dr', 'Debit'), ('cr', 'Credit')], 'Cr/Dr')

    @api.model
    def create(self, vals):
        account_move_line_obj = self.env['account.move.line']
        # Prevent manually adding new statement line.
        # This would allow only onchange method to pre-populate statement lines
        # based on the filter rules.

        # ============= khushboo ===========check it ( on fetch details button )=
        if vals.get('move_line_id') is False:
            raise UserError(
                _('Processing Error, You cannot add any new bank statement line manually as of this revision!'))
        account_move_line_obj.write({'draft_assigned_to_statement': True})
        return super(CashBookReconcillationLine, self).create(vals)

    @api.multi
    def unlink(self):
        # account_move_line_obj = self.env['account.move.line']
        # move_line_ids = map(lambda x: x.move_line_id.id)
        # # Reset field values in move lines to be added later
        # account_move_line_obj.write(
        #     {'draft_assigned_to_statement': False,
        #      'cleared_bank_account': False,
        #      'bank_acc_rec_statement_id': False})
        for record in self:
            record.draft_assigned_to_statement = False
            record.cleared_bank_account = False
            record.bank_acc_rec_statement_id = False
        return super(CashBookReconcillationLine, self).unlink()

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
