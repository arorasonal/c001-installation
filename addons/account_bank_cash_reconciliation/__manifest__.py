# -*- coding: utf-8 -*-

{
    'name': 'Bank & Cash Reconciliation',
    'version': '1.0',
    'author': 'Apagen solution pvt ltd',
    'category': 'Accounting and Financial Management',
    'description': """

This module is designed to provide an easy method in which Odoo accounting users can manually reconcile/validate their financial transactions
from their financial institution/transaction providers (e.g. Paypal, A financial institution, google wallet, etc)
against Odoo GL Chart of Account bank accounts.

Users will be able to validate and indicate if a transaction has "Cleared the Bank" using a checkmark on a new Reconcile Financial Account
Statement view on each individual financial transaction. Users will also be able to mark transactions on a bank account for future research.

The idea is that as a first step users will manually look at their paper statement and line-by-line check off which
financial transactions have cleared the bank in Odoo using the new Bank Reconciliation Wizard.
These changes will be displayed on the  new Reconcile Financial Account Statement tree view screen.
This is the process in which many companies reconcile (aka Audit) their bank account statements and accounting system
today and represents good segregation of duties

Users can save their in-process reconciliations.

BACKGROUND

Using the search view filters - users will also be able to effectively sort, filter the transactions on a particular GL Financial Account.
This new screen will display the journal items associated with a particular bank account.
Several of the field labels have been relabeled to a more common vernacular.

""",
    'author': 'Apagen Solutions Pvt. Ltd.',
    'website': ' http://www.apagen.com',
    'depends': ['account_voucher'],
    'data': [
        "security/bank_cash_reconciliation_security.xml",
        "security/ir.model.access.csv",
        "views/bank_account_reconciliation_view.xml",
        "views/cash_book_reconciliation_view.xml",
        "views/account_move_line_view.xml"
    ],
    'installable': True,
    'auto_install': False,
    'application': True,
}

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
