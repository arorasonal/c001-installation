odoo.define('ks_dashboard_ninja_list_tv.ks_dashboard_graph_preview', function (require) {
    "use strict";


    var KsGraphPreview = require('ks_dashboard_ninja_pro_list.ks_dashboard_graph_preview');

    KsGraphPreview.KsGraphPreview.include({

        render_value: function(){
            this.$el.empty();
            this.fields = this.view.get_fields_values();
            var chart_type = ['ks_bar_chart','ks_horizontalBar_chart','ks_line_chart','ks_area_chart','ks_pie_chart','ks_doughnut_chart','ks_polarArea_chart'];

            if (_.contains(chart_type, this.fields.ks_dashboard_item_type)) {
                if(this.fields.ks_data_calculation_type !== "query"){
                    this._super.apply(this, arguments);
                }
                else if(this.fields.ks_data_calculation_type === "query" && this.fields.ks_query_result) {
                    if(this.fields.ks_xlabels && this.fields.ks_ylabels){
                            this._getChartData();
                    } else {
                        this.$el.append($('<div>').text("Please choose the X-labels and Y-labels"));
                    }
                } else {
                    this.$el.append($('<div>').text("Please run the appropriate Query"));
                }
            }
        },
    });

    return KsGraphPreview;
});