odoo.define("ks_dashboard_ninja.ks_ylabels", function(require){

    var common = require('web.form_common');
    var core = require('web.core');

    var session = require('web.session');
    var utils = require('web.utils');
    var QWeb = core.qweb;
    var KsYLabels = common.AbstractField.extend(common.ReinitializeFieldMixin,{
        events: _.extend({}, common.AbstractField.prototype.events, {
            'change select': 'ks_toggle_icon_input_click',
            'blur .ks_stack_group': 'ks_group_input_click',
        }),
         start: function() {
            this.field_manager.on("view_content_has_changed", this, this.preview_update);
            this.on("change:effective_readonly", this, this.reinitialize);
            return this._super();
        },

        preview_update : function(e){
            this.render_value();
        },

        init: function(){
            this.ks_columns = {};
            this.ks_rows_keys = [];
            this.ks_rows_chart_type = {}
            this._super.apply(this, arguments);

        },

        render_value: function () {
            var self = this;
            var mode;
            self.$el.empty();
            var field = this.fields = this.view.get_fields_values();
            if(field.ks_query_result){
                var ks_query_result = JSON.parse(field.ks_query_result);
                if (ks_query_result.records.length){
                    if (field.ks_dashboard_item_type !== 'ks_list_view' && field.ks_dashboard_item_type !=='ks_kpi' && field.ks_dashboard_item_type !== 'ks_tile'){
                        self.ks_check_for_labels();
                        var $view = $(QWeb.render('ks_y_label_table',{
                            label_rows: self.ks_columns,
                            chart_type: self.ks_rows_chart_type,
                            mode: !self.get("effective_readonly"),
                            ks_is_group_column: ks_query_result.ks_is_group_column,
                        }));

                        self.ks_rows_keys.forEach(function(key){
                            $view.find('#' + key).val(self.ks_rows_chart_type[key]);
                        })

                        self.$el.append($view);
                        if (!self.get("effective_readonly")){
                            self.ks_toggle_icon_input_click();
                        }
                    }
                } else {
                    this.$el.append("No Data Available");
                }
            } else {
                this.$el.append("Please Enter the Appropriate Query for this");
            }
            if (self.get("effective_readonly")) {
               this.$el.find('td.ks_stack_group').addClass('ks_not_click');
            }
        },
        ks_check_for_labels: function(){
            var field = this.view.get_fields_values();
            var self = this;
            self.ks_columns = {};
            var value = this.get_value();
            if(value){
                var ks_columns = JSON.parse(value);
                Object.keys(ks_columns).forEach(function(key){
                    var chart_type = ks_columns[key]['chart_type'];
                    self.ks_rows_chart_type[key] = chart_type;
                    ks_columns[key]['chart_type'] = {}
                    if(field.ks_dashboard_item_type === 'ks_bar_chart'){
                        var chart_type_temp = field.ks_dashboard_item_type.split("_")[1];
                        if (chart_type_temp !== chart_type) {
                            chart_type = chart_type_temp;
                        }
                        ks_columns[key]['chart_type'][chart_type] = self.ks_title(chart_type);
                        if (chart_type === "bar"){
                            ks_columns[key]['chart_type']["line"] = "Line";
                        } else {
                            ks_columns[key]['chart_type']["bar"] = "Bar"
                        }
                    } else {
                        var chart_type = field.ks_dashboard_item_type.split("_")[1];
                        ks_columns[key]['chart_type'][chart_type] = self.ks_title(chart_type);
                        if (chart_type === "bar") ks_columns[key]['chart_type']["line"] = "Line";
                    }
                    self.ks_rows_keys.push(key);
                });
                self.ks_columns = ks_columns;
            } else {
                var query_result = JSON.parse(field.ks_query_result).records;

                Object.keys(query_result[0]).forEach(function(key){
                    for(var i =0;i< query_result.length; i++){
                        if(query_result[i][key] === null){
                            continue;
                        }
                        if(typeof(query_result[i][key]) === "number") {
                            var ks_row = {}
                            ks_row['measure'] = self.ks_title(key.replace("_", " "));
                            ks_row['chart_type'] = {}
                            var chart_type = field.ks_dashboard_item_type.split("_")[1];
                            ks_row['chart_type'][chart_type] = self.ks_title(chart_type);
                            if (chart_type === "bar") ks_row['chart_type']["line"] = "Line";
                            ks_row['group'] = " ";
                            self.ks_columns[key] = ks_row;
                        }
                        break;
                    }
                });
            }
        },

        ks_toggle_icon_input_click: function(e){
            var self = this;

            var ks_tbody = this.$el.find('tbody.ks_y_axis');
            ks_tbody.find('select').each(function(){
                self.ks_columns[this.id]['chart_type'] = this.value;
            });
            var value = JSON.stringify(self.ks_columns);
            self.set_value(value);
        },

        ks_title: function(str) {
            var split_str = str.toLowerCase().split(' ');
            for (var i = 0; i < split_str.length; i++) {
                split_str[i] = split_str[i].charAt(0).toUpperCase() + split_str[i].substring(1);
                str = split_str.join(' ');
            }
            return str;
        },

        ks_group_input_click: function(e){
            var self = this;
            var ks_tbody = this.$el.find('tbody.ks_y_axis');
            ks_tbody.find('select').each(function(){
                self.ks_columns[this.id]['chart_type'] = this.value;
            });
            self.ks_columns[e.currentTarget.id]['group'] = e.currentTarget.textContent.trim();
            var value = JSON.stringify(self.ks_columns);
            self.set_value(value);
        }
    });

    core.form_widget_registry.add('ks_y_labels', KsYLabels);

    return KsYLabels;
});