odoo.define('ks_dn_advance.ks_labels', function (require) {
    "use strict";


    var common = require('web.form_common');
    var core = require('web.core');
    var session = require('web.session');
    var utils = require('web.utils');

    var QWeb = core.qweb;

    var KsLabels = common.AbstractField.extend(common.ReinitializeFieldMixin,{
        events: _.extend({}, common.AbstractField.prototype.events, {
            'change select': 'ks_toggle_icon_input_click',
        }),

         start: function() {
            this.field_manager.on("view_content_has_changed", this, this.preview_update);
            this.on("change:effective_readonly", this, this.reinitialize);
            return this._super();
        },

        preview_update : function(e){
            this.render_value();
        },

        init: function(){
            this.ks_columns = {};
            this._super.apply(this, arguments);
        },

        render_value : function(){
            var self = this;
            self.$el.empty();
            var field = this.view.get_fields_values();
            var value = this.get_value();

            if(field.ks_query_result){
                var ks_query_result = JSON.parse(field.ks_query_result);
                if (ks_query_result.records.length){
                    self.ks_check_for_labels();
                    var $view = $(QWeb.render('ks_select_labels',{
                        ks_columns_list: self.ks_columns,
                        value: self.ks_columns[value],
                        mode: !self.get("effective_readonly"),
                    }));

                    if (value) {
                        $view.val(value);
                    }
                    this.$el.append($view)

                } else {
                    this.$el.append("No Data Available");
                }
            } else {
               this.$el.append("Please Enter the Appropriate Query for this");
            }
        },

        ks_toggle_icon_input_click: function(e){
            var self = this;
            self.set_value(e.currentTarget.value);
        },

        ks_check_for_labels: function(){
            var field = this.view.get_fields_values();
            var self = this;
            self.ks_columns = {false:false};
            var query_result = JSON.parse(field.ks_query_result).records;
            if (self.name === "ks_ylabels"){
                Object.keys(query_result[0]).forEach(function(key){
                    if(typeof(query_result[0][key]) === "number") {
                        self.ks_columns[key] = self.ks_title(key.replace("_", " "));
                    }
                });
            } else {
                Object.keys(query_result[0]).forEach(function(key){
                    self.ks_columns[key] = self.ks_title(key.replace("_", " "));
                });
            }
        },

        ks_title: function(str) {
            var split_str = str.toLowerCase().split(' ');
            for (var i = 0; i < split_str.length; i++) {
                split_str[i] = split_str[i].charAt(0).toUpperCase() + split_str[i].substring(1);
                str = split_str.join(' ');
            }
            return str;
        }
    });
    core.form_widget_registry.add('ks_labels', KsLabels);

    return KsLabels

});