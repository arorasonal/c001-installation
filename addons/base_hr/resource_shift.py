from operator import itemgetter

from odoo import api, fields, models, _
from odoo.exceptions import ValidationError
from odoo.tools.float_utils import float_compare


class Resourceshift(models.Model):
    """ Shift model for a resource. """
    _name = "resource.shift"
    _description = "Resource Shift"

    name = fields.Char(required=True)
    attendance_ids = fields.One2many(
        'resource.shift.attendance', 'calendar_id', string='Working Time',
        copy=True)


class ResourceCalendarAttendance(models.Model):
    _name = "resource.shift.attendance"
    _description = "Work Detail"
    _order = 'dayofweek, hour_from'

    name = fields.Char(required=True)
    dayofweek = fields.Selection([
        ('0', 'Monday'),
        ('1', 'Tuesday'),
        ('2', 'Wednesday'),
        ('3', 'Thursday'),
        ('4', 'Friday'),
        ('5', 'Saturday'),
        ('6', 'Sunday')
    ], 'Day of Week', required=True, index=True, default='0')
    hour_from = fields.Float(string='Work from', required=True,
                             index=True, help="Start and End time of working.")
    hour_to = fields.Float(string='Work to', required=True)
    calendar_id = fields.Many2one(
        "resource.shift", string="Resource's Shift", required=True, ondelete='cascade')
    odd_working = fields.Boolean("Odd Working")
    even_working = fields.Boolean("Even Working")

    def hours_time_string(hours):
        """ convert a number of hours (float) into a string with format '%H:%M' """
        minutes = int(round(hours * 60))
        return "%02d:%02d" % divmod(minutes, 60)
