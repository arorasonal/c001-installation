# Copyright (c) 2015: Apagen Solutions (P) Ltd.

from odoo import api, fields, models, tools, _
from odoo.exceptions import UserError


class HRLocation(models.Model):
    _name = "hr.location"

    name = fields.Char("Location")
    state_id = fields.Many2one('res.country.state', 'State')


class HRJob(models.Model):
    _inherit = "hr.job"

    job_code = fields.Char("Job Code")

    @api.multi
    def name_get(self):
        result = {}
        for record in self:
            if record.job_code:
                result[record.id] = '[%s] %s' % (
                    record.job_code, record.name)
            else:
                result[record.id] = record.name

        return result.items()

    @api.model
    def name_search(self, name, args=None, operator='ilike', limit=None):
        if not args:
            args = []
        args = args[:]
        job_view_obj = self.env['hr.job']
        job_obj = job_view_obj.search(
            [('job_code', 'ilike', name)] + args,
            limit=limit
        )
        job_obj_name = job_view_obj.search(
            [('name', 'ilike', name)] + args,
            limit=limit
        )
        jobs = job_obj + job_obj_name
        return jobs.name_get()


class HrDepartment(models.Model):
    _inherit = "hr.department"

    email_id = fields.Char("Employee Department Email")



class Employee(models.Model):
    _inherit = "hr.employee"

    @api.model
    def create(self, vals):
        res = super(Employee, self).create(vals)
        if ('user_id' in vals) and vals['user_id'] is not False:
            user = self.env['res.users'].search([('id', '=', vals['user_id'])])
            employee = self.env['hr.employee'].search(
                [('user_id', '=', vals['user_id']), ('id', '!=', res.id)])
            if employee:
                raise UserError(_('Employee already exists with \
                related user: "%s" !!!\n\n One related user can be associated with\
                    only one employee.') % (user.name))
        return res

    @api.multi
    def write(self, vals):
        res = super(Employee, self).write(vals)
        if 'user_id' in vals and vals['user_id'] is not False:
            for record in self:
                user = self.env['res.users'].search(
                    [('id', '=', vals['user_id'])])
                employee = self.env['hr.employee'].search(
                    [('user_id', '=', vals['user_id']), ('id', '!=', record.id)])
                if employee:
                    raise UserError(_('Employee already exists with \
                related user: "%s" !!!\n\n One related user can be associated with\
                    only one employee.') % (user.name))
        return res

# class partner(models.Model):
#     _inherit = "res.partner"

#     '''
#         an user should not be customer by default.'''

#     customer = fields.Boolean(string='Is a Customer', default=False,
#                                help="Check this box if this contact is a customer.")