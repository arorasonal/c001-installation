{
    'name': 'Automaticaly Task Creation',
    'version': '1.1',
    'author': "CJPL",
    'category': 'Automatically',
    'description': """
        Module automatically create task, invoice and contract.
    """,
    'website': '',
    
    'summary': 'Module automatically create task, invoice and contract.',
    'depends': ['base','sale'],
    'data': [
        # 'security/ir.model.access.csv',
        # 'views/automatically_task_create.xml',
    ],
   
    'installable': True,
    'auto_install': False,
}

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4: