from odoo import models, fields, api, _
from odoo.exceptions import Warning


class SaleOrder(models.Model):
    _inherit = 'sale.order'


#     @api.multi
#     def action_confirm(self):
#         res =  super(SaleOrder, self).action_confirm()
#         automate_task_obj = self.env['automaticaly.task.creation'].search([
#             ('obj_type','=','sale'),('automatic_type','=','task')],limit=1)
#         products = self.order_line
#         print('########',self.delivery_team_id.user_id.id,self.delivery_team_id.id)
#         project_dictionary ={
#             'user_id':self.delivery_team_id.user_id.id,
#             'delivery_team_id':self.delivery_team_id.id,
#             'project_id':automate_task_obj.project_id.id,
#             'date_deadline':self.delivery_date,
#             'sale_order_id':self.id,
#             'partner_id':self.partner_id.id,
#             'name':self.name,
#             'sale_order_date':self.confirmation_date,
#             'creation_type': 'saleorder',
#             'products_task_ids':[(6,0,[i.product_id.id for i in products])],
#             'delivery_date':self.delivery_date,
#             'delivery_mode':self.delivery_mode,
#             'delivery_instructions':self.delivery_instructions,
#             'installation_location_id':self.partner_shipping_id.id}
#         store_task_id = self.env['project.task'].sudo().create(project_dictionary)
#         if store_task_id:
#             self.write({'sale_order_task_field_id': store_task_id.id})
#             print('lst++++++++++++++', store_task_id)
#             # a = self.env['project.task'].create({'task_line_ids': lst})
#             for line in self.order_line:
#                 task_line = self.env['project.task.line'].create({
#                     'task_id': store_task_id.id,
#                     'product_id':line.product_id.id,
#                     'product_uom_qty':line.product_uom_qty,
#                     'name':line.name
#                     })
#                 print('task+++++++++++',task_line)
#         if self.is_contract_type:
#             self.action_button_contract()
#         else:
#             self.action_invoice_create(final=True)
#         return res

# class SaleOrderLine(models.Model):
#     _inherit = 'sale.order.line'

#     @api.depends('qty_invoiced', 'qty_delivered', 'product_uom_qty', 'order_id.state')
#     def _get_to_invoice_qty(self):
#         """
#         Compute the quantity to invoice. If the invoice policy is order, the quantity to invoice is
#         calculated from the ordered quantity. Otherwise, the quantity delivered is used.
#         """
#         for line in self:
#             if line.order_id.state in ['draft','sale', 'done']:
#                 if line.product_id.invoice_policy == 'order':
#                     line.qty_to_invoice = line.product_uom_qty - line.qty_invoiced
#                 else:
#                     line.qty_to_invoice = line.qty_delivered - line.qty_invoiced
#             else:
#                 line.qty_to_invoice = 0

