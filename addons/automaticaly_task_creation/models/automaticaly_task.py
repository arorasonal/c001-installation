from odoo import models, fields, api, _
from odoo.exceptions import Warning


class AutmaticallyTask(models.Model):
    _name = 'automaticaly.task.creation'
    _rec_name = 'obj_type'

    name = fields.Char(string="Name")
    active = fields.Boolean(string = "Active", default = 1)
    obj_type = fields.Selection([('sale', 'Sale Order'), ('equipment', 'Equipment Order')],
                                   string = 'Object')
    automatic_type = fields.Selection([('invoice', 'Invoice'), 
                                    ('task', 'Task')],
                                   string = 'Automatically Type')
    model_id = fields.Many2one('ir.model', string='Object', ondelete='set null') 
    project_id = fields.Many2one('project.project', 'Project')
    assigned_to_id = fields.Many2one('res.users', string='Assigned To',help='Displays the assigned user name')

