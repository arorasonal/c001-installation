# -*- coding: utf-8 -*-

from odoo import api, fields, models


class UpdateSalespersonWiz(models.TransientModel):
    _name = 'update.salesperson.wiz'

    lead_id = fields.Many2one('crm.lead', 'Lead')
    user_id = fields.Many2one('res.users', 'Salesperson')

    @api.onchange('lead_id')
    def onchange_lead_id(self):
        lis = []
        if self.lead_id:
            for sales_team in self.env.user.sales_team_ids:
                for member in sales_team.member_ids:
                    if member.id not in lis:
                        lis.append(member.id)
            if self.env.user.has_group('base.group_erp_manager'):
                sales_team_ids = self.env['crm.team'].search([])
                for sales_team in sales_team_ids:
                    for member in sales_team.member_ids:
                        if member.id not in lis:
                            lis.append(member.id)
            return {
                'domain': {
                    'user_id': [('id', 'in', lis)]
                }
            }

    @api.multi
    def update_sale_person(self):
        for rec in self:
            if rec.user_id:
                rec.lead_id.sudo().write({'user_id': rec.user_id.id})
