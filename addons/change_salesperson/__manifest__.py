# -*- coding: utf-8 -*-

{
    'name': 'Change Salesperson Lead/Opportunity',
    'version': '10.0.1.0',
    'category': 'Sale/CRM',
    'sequence': 14,
    'summary': 'Update sale person in Lead/Opportunity',
    'description': """
    """,
    'author': '',
    'website': '',
    'depends': [
        'crm'
    ],
    'data': [
        'views/crm_view.xml',
        'wizards/update_saleperson_wiz.xml',
    ],
    'installable': True,
    'auto_install': False,
    'application': True,
}
