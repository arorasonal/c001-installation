# -*- coding: utf-8 -*-

from odoo import api, fields, models


class Opportunity(models.Model):
    _inherit = 'crm.lead'

    is_team_leader = fields.Boolean(compute="_check_team_leader")

    def _check_team_leader(self):
        if self.team_id and self.user_id:
            if self.team_id in self.env.user.sales_team_ids or self.env.user.has_group('base.group_erp_manager') and self.stage_id.name not in ['Won', 'Lost']:
                self.is_team_leader = True
        else:
            self.is_team_leader = False

    @api.multi
    def action_update_sale_person(self):
        form_id = self.env['ir.model.data'].get_object_reference('change_salesperson', 'update_sale_person_view_wiz')
        return {
            'name': ('Update Salesperson'),
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'update.salesperson.wiz',
            'view_id': form_id and form_id[1] or False,
            'target': 'new',
            'context': {'default_lead_id': self.id}
        }
