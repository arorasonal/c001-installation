import itertools
from lxml import etree
import json
from odoo import models, fields, api, _
from odoo.exceptions import except_orm, Warning, RedirectWarning
from odoo.tools import float_compare
import odoo.addons.decimal_precision as dp


class account_invoice(models.Model):
    _inherit = "account.invoice"
   
    invoice_type = fields.Selection([('gst','GST'),('bill','Bill')],'Invoice Type')
    
    
    # @api.multi
    # def invoice_validate(self):
    #     if self.invoice_type == 'bill':
    #         return self.write({'state': 'open'})
    #     elif self.invoice_type == 'gst':
    #         if self.partner_id.gst:
    #             return self.write({'state': 'open'})
    #         else:
    #             raise UserError(_('Please fill the Customer GSTIN First.'))