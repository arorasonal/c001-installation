import time
from odoo import models, api, _
from odoo.exceptions import UserError
from odoo.tools import amount_to_text
from datetime import datetime
import math


class report_gst_invoice_cjpl(models.AbstractModel):
    _name = 'report.gst_report_cjpl.report_gst_invoice_cjpl'

    def get_hcs(self, account_invoice_line):
        hsn = ' '
        if account_invoice_line.invoice_id.sale_type.is_contract == True:
            hsn = account_invoice_line.product_id.categ_id.hsn_no
        else:
            hsn = account_invoice_line.invoice_id.sale_type_id.sac

        return hsn

    def get_convert_amt(self, account_invoice):
        amt_en_fill = ''
        amt_en = amount_to_text(account_invoice.amount_total, currency='INR')
        amt_en_fill = amt_en
        if 'and Zero Cent' in amt_en:
            amt_en_fill = amt_en.replace('and Zero Cent', 'Only')
        elif 'Cents' in amt_en:
            amt_en_fill = amt_en.replace('Cents', ' Only')

        return amt_en_fill

    def get_cgst_rate(self, account_invoice_line):
        rate = 0.00
        tax_obj = ' '
        if account_invoice_line:
            if account_invoice_line.invoice_id.partner_id.state_id == account_invoice_line.invoice_id.journal_id.bill_from.partner_id.state_id:
                tax_obj = self.env['account.tax'].browse(
                    account_invoice_line.invoice_line_tax_ids.id)
                rate = (tax_obj.amount / 2) * 100
        return rate

    def get_cgst_amount(self, account_invoice_line):
        amount = 0.00
        tax_obj = ' '
        rate = 0.0
        account_obj = ' '
        if account_invoice_line:
            if account_invoice_line.invoice_id.partner_id.state_id == account_invoice_line.invoice_id.journal_id.bill_from.partner_id.state_id:
                tax_obj = self.env['account.tax'].browse(
                    account_invoice_line.invoice_line_tax_ids.id)
                rate = (tax_obj.amount / 2) * 100
                amount = (rate * account_invoice_line.price_subtotal) / 100
                amount = int(round(amount))
        return amount

    def get_cgst_total_amount(self, account_invoice):
        amount = 0.00
        account_obj = ' '
        rate = 0.00
        if account_invoice.invoice_line_ids:
            for val in account_invoice.invoice_line_ids:
                if account_invoice.partner_id.state_id == account_invoice.journal_id.bill_from.partner_id.state_id:
                    tax_obj = self.env['account.tax'].browse(
                        val.invoice_line_tax_ids.id)
                    rate = (tax_obj.amount / 2) * 100
                    amount = amount + (rate * val.price_subtotal) / 100
                    amount = int(round(amount))
        return amount

    def get_igst_rate(self, account_invoice_line):
        rate = 0.00
        tax_obj = ' '
        if account_invoice_line:
            if account_invoice_line.invoice_id.partner_id.state_id != account_invoice_line.invoice_id.journal_id.bill_from.partner_id.state_id:
                tax_obj = self.env['account.tax'].browse(
                    account_invoice_line.invoice_line_tax_ids.id)
                rate = (tax_obj.amount * 100)
        return rate

    def get_igst_amount(self, account_invoice_line):
        amount = 0.00
        tax_obj = ' '
        rate = 0.0
        account_obj = ' '
        if account_invoice_line:
            if account_invoice_line.invoice_id.partner_id.state_id != account_invoice_line.invoice_id.journal_id.bill_from.partner_id.state_id:
                tax_obj = self.env['account.tax'].browse(
                    account_invoice_line.invoice_line_tax_ids.id)
                rate = (tax_obj.amount * 100)
                amount = (rate * account_invoice_line.price_subtotal) / 100
                amount = int(round(amount))
        return amount

    def get_igst_total_amount(self, account_invoice):
        amount = 0.00
        account_obj = ' '
        rate = 0.00
        if account_invoice.invoice_line_ids:
            for val in account_invoice.invoice_line_ids:
                if account_invoice.partner_id.state_id != account_invoice.journal_id.bill_from.partner_id.state_id:
                    tax_obj = self.env['account.tax'].browse(
                        val.invoice_line_tax_ids.id)
                    rate = (tax_obj.amount * 100)
                    amount = amount + (rate * val.price_subtotal) / 100
                    amount = int(round(amount))
        return amount

    def get_sgst_rate(self, account_invoice_line):
        rate = 0.00
        tax_obj = ' '
        if account_invoice_line:
            if account_invoice_line.invoice_id.partner_id.state_id == account_invoice_line.invoice_id.journal_id.bill_from.partner_id.state_id:
                tax_obj = self.env['account.tax'].browse(
                    account_invoice_line.invoice_line_tax_ids.id)
                rate = (tax_obj.amount / 2) * 100
        return rate

    def get_sgst_amount(self, account_invoice_line):
        amount = 0.00
        tax_obj = ' '
        rate = 0.0
        account_obj = ' '
        if account_invoice_line:
            if account_invoice_line.invoice_id.partner_id.state_id == account_invoice_line.invoice_id.journal_id.bill_from.partner_id.state_id:
                tax_obj = self.env['account.tax'].browse(
                    account_invoice_line.invoice_line_tax_ids.id)
                rate = (tax_obj.amount / 2) * 100
                amount = (rate * account_invoice_line.price_subtotal) / 100
                amount = int(round(amount))
        return amount

    def get_sgst_total_amount(self, account_invoice):
        amount = 0.00
        account_obj = ' '
        rate = 0.00
        if account_invoice.invoice_line_ids:
            for val in account_invoice.invoice_line_ids:
                if account_invoice.partner_id.state_id == account_invoice.journal_id.bill_from.partner_id.state_id:
                    tax_obj = self.env['account.tax'].browse(
                        val.invoice_line_tax_ids.id)
                    rate = (tax_obj.amount / 2) * 100
                    amount = amount + (rate * val.price_subtotal) / 100
                    amount = int(round(amount))
        return amount

    def get_subtotal(self, account_invoice_line):
        subtotal = 0.00
        amount1 = 0.0
        amount2 = 0.0
        amount3 = 0.0
        price = 0.0
        rate1 = 0.0
        rate2 = 0.0
        rate3 = 0.0
        if account_invoice_line:
            for val1 in account_invoice_line:
                if account_invoice_line.invoice_id.partner_id.state_id == account_invoice_line.invoice_id.journal_id.bill_from.partner_id.state_id:
                    tax_obj = self.env['account.tax'].browse(
                        account_invoice_line.invoice_line_tax_ids.id)
                    rate1 = (tax_obj.amount / 2) * 100
                    amount1 = (
                        rate1 * account_invoice_line.price_subtotal) / 100
                    amount1 = int(round(amount1))
                if account_invoice_line.invoice_id.partner_id.state_id == account_invoice_line.invoice_id.journal_id.bill_from.partner_id.state_id:
                    tax_obj = self.env['account.tax'].browse(
                        account_invoice_line.invoice_line_tax_ids.id)
                    rate2 = (tax_obj.amount / 2) * 100
                    amount2 = (
                        rate2 * account_invoice_line.price_subtotal) / 100
                    amount2 = int(round(amount2))
                if account_invoice_line.invoice_id.partner_id.state_id != account_invoice_line.invoice_id.journal_id.bill_from.partner_id.state_id:
                    tax_obj = self.env['account.tax'].browse(
                        account_invoice_line.invoice_line_tax_ids.id)
                    rate3 = (tax_obj.amount * 100)
                    amount3 = (
                        rate3 * account_invoice_line.price_subtotal) / 100
                    amount3 = int(round(amount3))
            subtotal = amount1 + amount2 + amount3 + val1.price_subtotal
        return subtotal

    def get_total_qty(self, account_invoice):
        qty = 0.0
        if account_invoice.invoice_line_ids:
            for val in account_invoice.invoice_line_ids:
                qty = qty + val.quantity
        return qty

    def get_tax_subtotal(self, account_invoice):
        price = 0.0
        if account_invoice.invoice_line_ids:
            for val in account_invoice.invoice_line_ids:
                price = price + val.price_subtotal
        return price

    def get_price_subtotal(self, account_invoice):
        price = 0.0
        if account_invoice.invoice_line_ids:
            for val in account_invoice.invoice_line_ids:
                price = price + (val.quantity * val.price_unit)
        return price

    def get_total(self, account_invoice):
        price = 0.0
        amount1 = 0.0
        amount2 = 0.0
        amount3 = 0.0
        amount4 = 0.0
        rate1 = 0.0
        rate2 = 0.0
        rate3 = 0.0
        if account_invoice.invoice_line_ids:
            for val in account_invoice.invoice_line_ids:
                if account_invoice.partner_id.state_id == account_invoice.journal_id.bill_from.partner_id.state_id:
                    tax_obj = self.env['account.tax'].browse(
                        val.invoice_line_tax_ids.id)
                    rate1 = (tax_obj.amount / 2) * 100
                    amount4 = amount4 + (rate1 * val.price_subtotal) / 100
                    amount4 = int(round(amount4))
                if account_invoice.partner_id.state_id == account_invoice.journal_id.bill_from.partner_id.state_id:
                    tax_obj = self.env['account.tax'].browse(
                        val.invoice_line_tax_ids.id)
                    rate2 = (tax_obj.amount / 2) * 100
                    amount4 = amount4 + (rate2 * val.price_subtotal) / 100
                    amount4 = int(round(amount4))
                if account_invoice.partner_id.state_id != account_invoice.journal_id.bill_from.partner_id.state_id:
                    tax_obj = self.env['account.tax'].browse(
                        val.invoice_line_tax_ids.id)
                    rate3 = (tax_obj.amount * 100)
                    amount4 = amount4 + (rate3 * val.price_subtotal) / 100
                    amount4 = int(round(amount4))
                amount4 = val.price_subtotal + amount4

        return amount4

    def get_tax(self, account_invoice):

        amount1 = 0.0
        amount2 = 0.0
        amount3 = 0.0
        amount4 = 0.0

        if account_invoice.invoice_line_ids:
            for val in account_invoice.invoice_line_ids:
                if account_invoice.partner_id.state_id == account_invoice.journal_id.bill_from.partner_id.state_id:
                    tax_obj = self.env['account.tax'].browse(
                        val.invoice_line_tax_ids.id)
                    rate1 = (tax_obj.amount / 2) * 100
                    amount4 = amount4 + (rate1 * val.price_subtotal) / 100
                    amount4 = int(round(amount4))
                if account_invoice.partner_id.state_id == account_invoice.journal_id.bill_from.partner_id.state_id:
                    tax_obj = self.env['account.tax'].browse(
                        val.invoice_line_tax_ids.id)
                    rate2 = (tax_obj.amount / 2) * 100
                    amount4 = amount4 + (rate2 * val.price_subtotal) / 100
                    amount4 = int(round(amount4))
                if account_invoice.partner_id.state_id != account_invoice.journal_id.bill_from.partner_id.state_id:
                    tax_obj = self.env['account.tax'].browse(
                        val.invoice_line_tax_ids.id)
                    rate3 = (tax_obj.amount * 100)
                    amount4 = amount4 + (rate3 * val.price_subtotal) / 100
                    amount4 = int(round(amount4))
                amount4 = amount4

        return amount4

    @api.model
    def render_html(self, docids, data=None):
        model = 'account.invoice'
        docs = self.env[model].browse(docids)
        # if not data.get('form') or not self.env.context.get('active_model') or not self.env.context.get('active_id'):
        #     print data.get('form'), "--------------------"
        #     print self.env.context.get('active_model'), "======================"
        #     print self.env.context.get('active_id'), "++++++++++++++++++++++"
        #     raise UserError(_("Form content is missing, this report cannot be printed."))

        # total = []
        # model = self.env.context.get('active_model')
        # docs = self.env[model].browse(self.env.context.get('active_id'))

        # target_move = data['form'].get('target_move', 'all')
        # date_from = data['form'].get('date_from', time.strftime('%Y-%m-%d'))

        # if data['form']['result_selection'] == 'customer':
        #     account_type = ['receivable']
        # elif data['form']['result_selection'] == 'supplier':
        #     account_type = ['payable']
        # else:
        #     account_type = ['payable', 'receivable']

        movelines = self
        docargs = {
            'doc_ids': self.ids,
            'doc_model': model,
            # 'data': data['form'],
            'docs': docs,
            'time': time,
            'get_partner_lines': movelines,
            # 'get_direction': total,
            # 'get_amt':self.get_amt,
            'get_cgst_rate': self.get_cgst_rate,
            'get_cgst_amount': self.get_cgst_amount,
            'get_sgst_rate': self.get_sgst_rate,
            'get_sgst_amount': self.get_sgst_amount,
            'get_igst_rate': self.get_igst_rate,
            'get_igst_amount': self.get_igst_amount,
            'get_subtotal': self.get_subtotal,
            'get_total_qty': self.get_total_qty,
            'get_cgst_total_amount': self.get_cgst_total_amount,
            'get_sgst_total_amount': self.get_sgst_total_amount,
            'get_igst_total_amount': self.get_igst_total_amount,
            'get_price_subtotal': self.get_price_subtotal,
            'get_tax_subtotal': self.get_tax_subtotal,
            'get_total': self.get_total,
            'get_tax': self.get_tax,
            'get_convert_amt': self.get_convert_amt,
            'get_hcs': self.get_hcs,
        }
        return self.env['report'].render('gst_report_cjpl.report_gst_invoice_cjpl', docargs)
