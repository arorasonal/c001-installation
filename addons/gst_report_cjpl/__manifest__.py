{
    "name": "GST REPORT(CJPL)",
    "version": "10.0.0.1.0",
    'website': 'http://www.diracerp.in',
    'images': [ ],
    "depends": ['sale','account','warehouse_custom'],
    "author": "Dirac ERP",
    "category": "GST REPORT",
    'sequence': 16,
    "description": """
    This module will provide the detail of a GST Invoice of a CJPL.
    """,
    'data': [
     
     'report/gst_invoice_report_view.xml',
     'view/gst_invoice_report_template.xml',
     'view/product_category_view.xml',
     'view/account_invoice_view.xml',
                    ],
    'demo_xml': [],
     'js': [
           'static/src/js/view_list.js'
#     'static/src/js/subst.js'
    ],
    'css': ['static/src/css/sample_kanban.css'],
    'installable': True,
    'active': False,
    'auto_install': False,
#    'certificate': 'certificate',
}