# -*- coding: utf-8 -*-
##############################################################################
#
#    India-GST
#
#    Merlin Tecsol Pvt. Ltd.
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
from odoo import api, fields, models
import odoo.addons.decimal_precision as dp


class AccountTax(models.Model):
    _inherit = 'account.tax'

    tax_type = fields.Selection(selection=[(
        'cgst', 'CGST'), ('sgst', 'SGST'), ('igst', 'IGST')], string="Tax Type")


class PortCode(models.Model):
    _name = 'port.code'

    name = fields.Char('Port Name')
    code = fields.Char('Port Code')


class Partner(models.Model):
    _inherit = 'res.partner'

    def _default_country(self):
        return self.env['res.country'].search([('name', '=', 'India')], limit=1)

    cin_no = fields.Char('CIN', help='Customer Identification Number')
    pan_no = fields.Char('PAN', help='PAN Number')
    #state_code = fields.Char('State Code', help='State Code')
    country_id = fields.Many2one('res.country', default=_default_country)
    gstin_registered = fields.Boolean('GSTIN-Registered')
    gstin = fields.Char('GSTIN')
    e_commerce = fields.Boolean('E-Commerce')
    e_commerce_tin = fields.Char('E-Commerce GSTIN')
    accounts_approval = fields.Boolean('Approved by Accounts')
    core_code = fields.Char('Core Software Party Code')
    watchlist = fields.Selection([
        ('normal', 'Normal'),
        ('watchlist', 'Watchlist'),
        ('extreem_watchlist', 'Extreem Watchlist'),
        ('blacklist', 'Blacklist')], string='Watchlist Status')

    @api.onchange('state_id')
    def onchange_state_id(self):
        obj1 = self.env['account.fiscal.position'].search(
            [('name', '=', 'Intra State')])
        obj2 = self.env['account.fiscal.position'].search(
            [('name', '=', 'Inter State')])
        if self.state_id:
            if self.env.user.company_id.state_id.id == self.state_id.id:
                self.property_account_position_id = obj1
            else:
                self.property_account_position_id = obj2

    @api.onchange('country_id')
    def onchange_country_id(self):
        obj = self.env['account.fiscal.position'].search(
            [('name', '=', 'Export')])
        if self.country_id:
            if self.country_id.name != 'India':
                self.property_account_position_id = obj
            else:
                self.property_account_position_id = ""


class Product(models.Model):
    _inherit = 'product.template'

    hsn_code = fields.Char('HSN/SAC')


class CountryState(models.Model):
    _description = "Country state"
    _inherit = 'res.country.state'

    state_code = fields.Char('Code', help='Numeric State Code ')
