# -*- coding: utf-8 -*-
##############################################################################
#
#    India-GST
#
#    Merlin Tecsol Pvt. Ltd.
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
{

    'name': 'India-GST',
    'description': "Goods & Service-Tax.",
    'version': '1.0.1',
    'category': 'Accounting',
    'author': 'Merlin Tecsol Pvt. Ltd. -- & Ravi Krishnan',
    'website': 'http://www.merlintecsol.com',
    'summary': 'Indian GST Reports',
    'license': 'AGPL-3',
    'depends': ['sale','purchase','account','report_xlsx'],
    'data': [
        "views/gst_view.xml",
        "data/res.country.state.csv",
        'views/port_code.xml',

    ],
    'images': ['static/description/banner.png'],
}
