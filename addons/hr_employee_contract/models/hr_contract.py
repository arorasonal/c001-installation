from datetime import date, timedelta, datetime
from odoo.tools.translate import _
import time
from dateutil.relativedelta import relativedelta
from odoo import api, fields, models, tools
from odoo.tools import DEFAULT_SERVER_DATE_FORMAT
from dateutil.rrule import rrule, MONTHLY
from odoo.exceptions import UserError

STATE_MAPPING = {
    'on_probation': 'open',
    'prob_extended': 'open',
    'prob_lapsed': 'pending',
    'confirmed': 'open',
    'close_contract': 'close',
    'terminated': 'close'
}


class HrContract(models.Model):
    _name = 'hr.contract'
    _inherit = ['mail.thread', 'hr.contract']

    months_ = fields.Char(
        compute='_get_month',
        string='Months',
        store=False
    )
    house_alw = fields.Float(
        compute='_house_alw',
        string='House Allowance'
    )
    mobile_alw = fields.Float(
        compute='_mobile_alw',
        string='Mobile Allowance'
    )
    fuel_alw = fields.Float(
        compute='_fuel_alw',
        string='Fuel Allowance'
    )
    travel_alw = fields.Float(
        compute='_travel_alw',
        string='Travel Allowance'
    )
    icea_deduct = fields.Float(
        compute='_icea_deduct',
        string='ICEA'
    )
    insurance_deduct = fields.Float(
        compute='_insurance_deduct',
        string='Insurance Deductions'
    )
    stanbic_loan_deduct = fields.Float(
        compute='_stanbic_loan_deduct',
        string='Stanbic Loan Deductions'
    )
    qway_sacco = fields.Float(
        compute='_qway_sacco',
        string='Q/Way Sacco'
    )
    aar_deduct = fields.Float(
        compute='_aar_deduct',
        string='AAR Deduction'
    )
    icea_endowment = fields.Float(
        compute='_icea_endowment',
        string='ICEA Endowment'
    )
    nation_sacco = fields.Float(
        compute='_nation_sacco',
        string='Nation Sacco'
    )
    staff_welfare = fields.Float(
        compute='_staff_welfare',
        string='Staff Welfare'
    )
    name = fields.Char(
        'Contract Reference #',
        size=32, required=False)
    wage = fields.Float(
        'Basic Salary',
        digits=(16, 2),
        required=True,
        help="Basic Salary of the employee"
    )
    gross_sal = fields.Float(
        compute='_total_gross',
        string='Gross Salary',
        help="Gross Salary of the employee"
    )
    net_sal = fields.Float(
        compute='_total_net',
        string="NET Salary"
    )
    resource_calendar_id = fields.Many2one('resource.calendar',
                                           required=False,
                                           string="Working Schedule",
                                           help="Employee's working schedule.")

    job_id = fields.Many2one(
        'hr.job',
        string="Designation",
        related='employee_id.job_id',
        store=True)
    company_id = fields.Many2one(
        'res.company',
        string="Company",
        store=True
    )
    salary_structure_id = fields.Many2one(
        'hr.payroll.structure',
        string="Salary Structure",
    )
    department_id = fields.Many2one(
        'hr.department',
        related='employee_id.department_id',
        string="Department"
    )
    # ethnicity_id = fields.Many2one("hr.ethnicity", string='Ethnicity')
    nationality = fields.Many2one(
        # related='employee_id.country_id',
        "res.country",
        string='Nationality'
    )
    employment_date = fields.Date(
        # related='employee_id.employment_date',
        string='Employment Date '
    )
    # name = fields.Char('Contract Reference', required=False)
    trial_date_start = fields.Date('Probation Period')
    date_start = fields.Date(
        'Duration', required=True, related='employee_id.date_of_joining',
        help="Start date of the contract.")
    visa_expire = fields.Date('Visa Expiry Date')
    visa_number = fields.Char('Visa #')
    work_permit_number = fields.Char('Work Permit #')
    notice_period = fields.Float('Notice Period (Days)')
    mid_probation_date = fields.Date('Mid-Probation Date')
    joining_date = fields.Date(
        "Date of Joining", store=True,
        related='employee_id.date_of_joining')
    appointed_date = fields.Date("Appointed Date")
    # status = fields.Selection([
    #     ('draft', "Draft"),
    #     ('confirmed', "Confirmed"),
    #     ('on_probation', "On Probation"),
    #     ('terminated', "Terminated")], "Status"
    # )
    # scheduled_pay = fields.Many2one('scheduled.pay', "Scheduled Pay")
    status1 = fields.Selection([
        ('on_probation', "On Probation"),
        ('prob_extended', "Probation Extended"),
        ('prob_lapsed', "Probation Lapsed"),
        ('confirmed', "Confirmed"),
        ('close_contract', "Closed"),
        ('terminated', "Terminated")], "Status", default='on_probation',
    )
    probation_period = fields.Char(
        compute='_get_probation_period', string="Probation Period",
    )
    doc_con_ref = fields.Many2one('hr.contract', "Closed Contract Reference")
    salary_allowance_id = fields.One2many(
        'allowance.allowance', 'hr_contract_id',
        string="Allowance"
    )
    salary_deduction_id = fields.One2many(
        'deduction.deduction',
        'hr_contract_id',
        string="Deduction"
    )
    amount = fields.Float(
        compute='_total_allowance',
        string="Total Allowance"
    )
    amount_total = fields.Float(
        compute='_total_deduction',
        string="Total Deduction"
    )
    salary_structure_line_ids = fields.One2many(
        'hr.payslip.line', 'salary_contract_id')
    probation_extend_ids = fields.One2many(
        'probation.extend', 'contract_id',
        string="Probation Extended"
    )
    commission = fields.Float('Commission (%)')
    # settlement = fields.Selection([
    #     ("monthly", "Monthly"),
    #     ("quaterly", "Quarterly"),
    #     ("semi", "Semi-annual"),
    #     ("annual", "Annual")],
    #     string="Settlement period",
    #     default="monthly",
    #     required=True
    # )
    total_commission_ids = fields.One2many(
        'employee.commission.settlement',
        'total_commission_id',
        'Commissions'
    )
    commission_total = fields.Float(
        compute='_total_commission',
        string="Total Amount Fix"
    )
    remaining_amount = fields.Float(
        compute='_remianing_commission',
        string="Remaining Amount To Fix"
    )
    # commission_id = fields.Many2one("sale.commission", "Commission")
    # commision_details_id = fields.Many2one(
    #     "commission.details", "Commission Details"
    # )
    reason = fields.Text('Reason For Termination')
    # hr_groups = fields.Many2many(
    #     'res.users', 'rel_hr2', 'hr_idss',
    #     'hr_group_ide', 'HR Group'
    # )
    employment_type = fields.Char('Employment Type', store=True)
    parent_id = fields.Many2one(
        'hr.employee', 'Manager', related='employee_id.parent_id', store=True)
    exit_date = fields.Date('Exit Date', store=True)
    previous_position = fields.Many2one('hr.job', 'Previous Position')
    previous_start_date = fields.Date("Previous Start Date")
    previous_end_date = fields.Date("Previous End Date")
    total_salary_for_current_month = fields.Integer('Salary for current Month')
    two_weeks_salary = fields.Integer('Salary for notice period')
    debts_to_be_paid = fields.Integer('Debts to be paid by employee')
    notice_date = fields.Date('Warning Date')
    reason_for_performance = fields.Text(
        'Employee reason for poor performance')
    rehiring_employee = fields.Selection(
        [('yes', "Yes"),
         ('no', "No")], "Rehiring Employee", default='no')
    employee_type = fields.Many2one("emp.type", "Employee Type")
    type_id = fields.Many2one('hr.contract.type', string="Contract Type", required=False,
                              default=lambda self: self.env['hr.contract.type'].search([], limit=1))

    @api.onchange('working_hours')
    def onchange_resource_calander_id(self):
        self.resource_calendar_id = self.working_hours

    @api.multi
    def write(self, vals):
        for rec in self:
            if 'status1' in vals:
                rec.state = STATE_MAPPING[vals['status1']]
            if 'working_hours' in vals:
                vals.update({
                    'resource_calendar_id': self.working_hours.id
                })
        return super(HrContract, self).write(vals)

    @api.multi
    def unlink(self):
        """Allows to delete hr draft"""
        for rec in self:
            if rec.status1 != 'on_probation':
                raise UserError(
                    'You can only delete in On Probation state')
        return super(HrContract, self).unlink()

    @api.multi
    def _check_dates(self):
        for contract in self:
            domain = [
                ('date_start', '<=', contract.date_end),
                ('date_end', '>=', contract.date_start),
                ('employee_id', '=', contract.employee_id.id),
                ('id', '!=', contract.id),
                ('status1', 'not in', ['close_contract', 'terminated'])
            ]
            contracts = self.search_count(domain)
            contract_without_end_date = self.search_count(
                [
                    ('employee_id', '=', contract.employee_id.id),
                    ('date_end', '=', False),
                    ('id', '!=', contract.id),
                    ('status1', 'not in', ['close_contract', 'terminated'])
                ]
            )
            if contracts or contract_without_end_date:
                return False
        return True

    _constraints = [
        (_check_dates, 'You can not have 2 Contract that overlaps on same Period!', [
         'date_start', 'date_end']),
    ]

    @api.multi
    def _get_month(self):
        res = {}
        for record_id in self:
            date_ = datetime.strptime(
                record_id.date_start, '%Y-%m-%d'
            ).strftime("%B %Y")
            res[record_id.id] = date_
        return res

    @api.multi
    def _travel_alw(self):
        res = {}
        for record in self:
            amount = 0.0
            for alw in record.salary_allowance_id:
                if alw.salary_rule_id.name == 'Transport Allowance':
                    amount = alw.amount
            res[record.id] = amount
        return res

    @api.multi
    def _house_alw(self):
        res = {}
        for record in self:
            amount = 0.0
            for alw in record.salary_allowance_id:
                if alw.salary_rule_id.name == 'House Allowance':
                    amount = alw.amount
            res[record.id] = amount
        return res

    @api.depends('salary_allowance_id.amount')
    def _total_allowance(self):
        res = {}
        for record in self:
            val = 0.0
            for line in record.salary_allowance_id:
                val += line.amount
            # res[record.id] = val
            record.update({
                'amount': val
            })
        return res

    @api.multi
    def _total_deduction(self):
        res = {}
        for record in self:
            # res[record.id] = {
            #     'amount': 0.0,
            # }
            val = 0.0
            for line in record.salary_deduction_id:
                val += line.amount
            # res[record.id] = val
            record.update({
                'amount_total': val
            })
        return res

    @api.depends('gross_sal')
    def _total_gross(self):
        res = {}
        for rec in self:
            res[rec.id] = {
                'gross_sal': 0.0,
            }
            rec.gross_sal = rec.amount + rec.wage
            res[rec.id] = rec.gross_sal
        return res

    def _check_gross_sal(self):
        current_date = str(date.today())
        financial_year = self.env['account.fiscal.year'].search([
            ('date_start', '<=', current_date),
            ('date_stop', '>=', current_date)], limit=1)
        if financial_year:
            if self.gross_sal > financial_year.salary_allowed:
                return False
        return True

    _constraints = [(_check_gross_sal, "Warning!\nGross salary greater than salary allowed.", [])]

    @api.multi
    def write(self, vals):
        for rec in self:
            if rec.wage > 30000:
                print rec.gross_sal,
                raise UserError('Basic salary amount is over 30k')
        return super(HrContract, self).write(vals)


    @api.multi
    def _mobile_alw(self):
        res = {}
        for record in self:
            amount = 0.0
            for alw in record.salary_allowance_id:
                if alw.salary_rule_id.name == 'Mobile Allowance':
                    amount = alw.amount
            res[record.id] = amount
        return res

    @api.multi
    def _fuel_alw(self):
        res = {}
        for record in self:
            amount = 0.0
            for alw in record.salary_allowance_id:
                if alw.salary_rule_id.name == 'Fuel Allowance':
                    amount = alw.amount
            res[record.id] = amount
        return res

    @api.multi
    def _stanbic_loan_deduct(self):
        res = {}
        for record in self:
            amount = 0.0
            for alw in record.salary_deduction_id:
                if alw.salary_rule_id_ded.name == 'Stanbic Loan Deductions':
                    amount = alw.amount
            res[record.id] = amount
        return res

    @api.multi
    def _qway_sacco(self):
        res = {}
        for record in self:
            amount = 0.0
            for alw in record.salary_deduction_id:
                if alw.salary_rule_id_ded.name == 'Queensway Sacco':
                    amount = alw.amount
            res[record.id] = amount
        return res

    @api.multi
    def _aar_deduct(self):
        res = {}
        for record in self:
            amount = 0.0
            for alw in record.salary_deduction_id:
                if alw.salary_rule_id_ded.name == 'AAR Deduction':
                    amount = alw.amount
            res[record.id] = amount
        return res

    @api.multi
    def _icea_endowment(self):
        res = {}
        for record in self:
            amount = 0.0
            for alw in record.salary_deduction_id:
                if alw.salary_rule_id_ded.name == 'ICEA Endowment':
                    amount = alw.amount
            res[record.id] = amount
        return res

    @api.multi
    def _nation_sacco(self):
        res = {}
        for record in self:
            amount = 0.0
            for alw in record.salary_deduction_id:
                if alw.salary_rule_id_ded.name == 'Nation Sacco':
                    amount = alw.amount
            res[record.id] = amount
        return res

    @api.multi
    def _staff_welfare(self):
        res = {}
        for record in self:
            amount = 0.0
            for alw in record.salary_deduction_id:
                if alw.salary_rule_id_ded.name == 'Staff Welfare':
                    amount = alw.amount
            res[record.id] = amount
        return res

    @api.multi
    def _total_net(self):
        res = {}
        for record in self:
            res[record.id] = {
                'net_sal': 0.0,
            }
            record.net_sal = record.gross_sal - record.amount_total
            res[record.id] = record.net_sal
        return res

    @api.multi
    def _icea_deduct(self):
        res = {}
        for record in self:
            amount = 0.0
            for alw in record.salary_deduction_id:
                if alw.salary_rule_id_ded.name == 'ICEA':
                    amount = alw.amount
            res[record.id] = amount
        return res

    @api.multi
    def _insurance_deduct(self):
        res = {}
        for record in self:
            amount = 0.0
            for alw in record.salary_deduction_id:
                if alw.salary_rule_id_ded.name == 'Insurance Deductions':
                    amount = alw.amount
            res[record.id] = amount
        return res

    @api.multi
    def _get_probation_period(self):
        res = {}
        for record_id in self:
            prob_period = ""
            if record_id.trial_date_start and record_id.trial_date_end:
                prob_start = datetime.strptime(str(
                    record_id.trial_date_start
                ), '%Y-%m-%d')
                prob_end = datetime.strptime(str(
                    record_id.trial_date_end
                ), '%Y-%m-%d')
                delta = relativedelta(prob_end, prob_start)
                deceased = ''
                prob_period = str(delta.years) + ' year ' \
                    + str(delta.months) + ' month ' + str(delta.days) + ' day '
            res[record_id.id] = prob_period
        return res

    @api.multi
    def _total_commission(self):
        result = dict.fromkeys(self.ids, False)
        agg_commision = self.env['employee.commission.settlement']
        for rec in self:
            if rec.total_commission_ids:
                line_ids = agg_commision.search(
                    [
                        ('total_commission_id', '=', rec.id),
                        ('state', '=', 'invoiced')
                    ]
                )
                if line_ids:
                    amount_total = []
                    for lines in line_ids:
                        amount = agg_commision.browse(lines).amount
                        amount_total.append(amount)
                    result[rec.id] = sum(amount_total)
        return result

    @api.multi
    def _remianing_commission(self):
        result = dict.fromkeys(self.ids, False)
        agg_commision = self.env['employee.commission.settlement']
        for rec in self:
            if rec.total_commission_ids:
                line_ids = agg_commision.search(
                    [
                        ('total_commission_id', '=', rec.id),
                        ('state', '=', 'open')
                    ]
                )
                if line_ids:
                    amount_total = []
                    for lines in line_ids:
                        amount = agg_commision.browse(lines).amount
                        amount_total.append(amount)
                    result[rec.id] = sum(amount_total)
        return result

    def clear_commission(self):
        agg_commision = self.env['employee.commission.settlement']
        for rec in self:
            if rec.total_commission_ids:
                line_ids = agg_commision.search(
                    [
                        ('total_commission_id', '=', rec.id),
                        ('state', '=', 'open')
                    ]
                )
                if line_ids:
                    for lines in line_ids:
                        agg_commision.write(
                            {'state': 'invoiced'},
                        )
        return True

    @api.model
    def create(self, vals):
        if 'status1' in vals:
            vals['state'] = STATE_MAPPING[vals['status1']]
        if 'working_hours' in vals:
            vals.update({
                'resource_calendar_id': self.working_hours.id
            })
        vals['name'] = self.env['ir.sequence'].next_by_code('hr.contract')
        return super(HrContract, self).create(vals)
        #  ==================by khushboo====================
        if data_ids.employee_id.employee_history_id:
            for prv_contract in data_ids.employee_id.employee_history_id[-1]:

                result = {
                    'previous_position': prv_contract.job_title.id,
                    'previous_department': prv_contract.department_id.id,
                    'previous_manager': prv_contract.parent_id,
                    'previous_start_date': prv_contract.contract_id.date_start,
                    'previous_end_date': prv_contract.contract_id.date_end,
                    'last_salary_drawn': prv_contract.contract_id.wage
                }
        result['reference'] = seq
        self.write(res)
        emp_data.write(
            {'contract_date': data_ids.date_start}
        )

    @api.onchange('struct_id')
    def onchange_struct(self):
        hr_payroll_structure_obj = self.env['hr.payroll.structure']
        allw_lines = []
        ded_lines = []
        hr_payroll_structure_ids = hr_payroll_structure_obj.search(
            [('id', '=', self.struct_id.id)])
        for data in hr_payroll_structure_ids:
            for rule in data.rule_ids:
                if rule.category_id.code == 'ALW' or rule.category_id.code == 'ALWT':
                    if not rule.input_ids:
                        allw_lines.append((0, 0, {
                            'salary_rule_id': rule.id,
                        }))
                if rule.category_id.code == 'DED':
                    if not rule.input_ids:
                        ded_lines.append((0, 0, {
                            'salary_rule_id_ded': rule.id,
                        }))
        return {'value': {
                'salary_allowance_id': allw_lines,
                'salary_deduction_id': ded_lines
                }}

    def action_confirm(self):
        rec = self.browse(self.ids)
        contracts = self.search(
            [
                ('employee_id', '=', rec.employee_id.id),
                ('id', '!=', rec.id)],
            order='id DESC',
            limit=1)
        res = {
            'status1': 'confirmed',
        }
        self.write(res)
        for contract in self:
            domain = [
                ('employee_id', '=', contract.employee_id.id),
                ('status1', '=', 'confirmed'),
                ('id', '!=', contract.id),
            ]
            contracts = self.search_count(domain)
            if contracts >= 1:
                raise UserError(
                    _('Warning!'),
                    _('Two Contracts for same employee \
                        cannot be in confirm state'))
        return True

    @api.multi
    def action_close_contract(self):
        self.write({'status1': 'close_contract'})
        return True

    @api.multi
    def action_prob_extended(self):
        self.write({'status1': 'prob_extended'})
        return True

    @api.multi
    def action_prob_lapsed(self):
        self.write({'status1': 'prob_lapsed'})
        return True

    @api.multi
    def action_terminate(self):
        user_id = self.env['res.users'].browse().id
        if self.reason is False:
            raise UserError(
                _('Warning!, Please first write Reason For Termination'))
        else:
            self.write(
                {'status1': 'terminated', 'reject_by': user_id},
            )
        return True

    @api.multi
    def get_date_end(self):
        date_today = datetime.now()
        self.env.cursor.execute('select id from hr_contract')
        res = self.env.cursor.fetchall()
        mail_obj = self.env['mail.mail']
        email_template_obj = self.env['mail.template']
        for prod_id in res:
            rec = self.browse(prod_id[0])

            if rec.date_end == str(date_today - timedelta(days=14)):
                ir_model_data = self.env['ir.model.data']
                template_id = self.env['ir.model.data'].get_object(
                    'hr_employee_contract',
                    'email_template_edi_contract_expiry_notification_apagen',
                )
                assert template_id._name == 'email.template'
                mail_id = email_template_obj.send_mail(
                    template_id.id,
                    rec.id, True
                )
                mail_state = mail_obj.read(['state'],
                                           )
                # Checking if the mail is sent properly
                if mail_state and mail_state['state'] == 'exception':
                    raise UserError(_("Cannot send email. \
                        Please check the connection settings \
                        and email address properly."), '')
            else:
                print "Right"
        return True

    @api.multi
    def get_trial_date_end(self):
        date_today = datetime.date.now()
        self.env.cursor.execute('select id from hr_contract')
        res = self.env.cursor.fetchall()
        mail_obj = self.env['mail.mail']
        email_template_obj = self.env['email.template']
        for prod_id1 in res:
            rec = self.browse(prod_id1[0])

            if rec.trial_date_end == str(date_today - timedelta(days=7)):
                ir_model_data = self.env['ir.model.data']
                template_id = self.env['ir.model.data'].get_object(
                    'hr_employee_contract',
                    'email_template_edi_probation_period_end_notification',
                )
                assert template_id._name == 'email.template'
                mail_id = email_template_obj.send_mail(
                    template_id.id,
                    rec.id, True
                )
                mail_state = mail_obj.read(
                    mail_id, ['state'],
                )
                # Checking if the mail is sent properly
                if mail_state and mail_state['state'] == 'exception':
                    raise UserError(_("Cannot send email. \
                        Please check the connection \
                        settings and email address properly."), '')
            else:
                print "Mail not sending"
        return True

    @api.multi
    def get_mid_probation_date(self):
        date_today = datetime.date.now()
        self.env.cursor.execute('select id from hr_contract')
        res = self.env.cursor.fetchall()
        mail_obj = self.env['mail.mail']
        email_template_obj = self.env['mail.template']
        for prod_id in res:
            rec = self.browse(prod_id[0])

            a = date_today.strftime('%Y-%m-%d')
            b = str(datetime.datetime.strptime(
                rec.mid_probation_date,
                DEFAULT_SERVER_DATE_FORMAT).date() - datetime.timedelta(7))
            if a == b:
                ir_model_data = self.env['ir.model.data']
                template_id = self.env['ir.model.data'].get_object(
                    'hr_employee_contract',
                    'email_template_edi_mid_probation_period_end_notification',
                )
                assert template_id._name == 'email.template'
                mail_id = email_template_obj.send_mail(
                    template_id.id,
                    rec.id, True
                )
                mail_state = mail_obj.read(
                    mail_id, ['state'],
                )
                # Checking if the mail is sent properly
                if mail_state and mail_state['state'] == 'exception':
                    raise UserError(_("Cannot send email. \
                        Please check the connection settings \
                        and email address properly."), '')
                print "Mail sending"
            else:
                print "Mail not sending"
        return True

    # @api.multi
    # def action_compute_sheet(self):
    #     for contract in self:
    #         contract.salary_structure_line_ids.unlink()
    #         lines = [(0, 0, line) for line in self._get_payslip_lines(contract)]
    #         contract.write({'salary_structure_line_ids': lines})
    #     return True

    # def _get_payslip_lines(self, contract):
    #     def _sum_salary_rule_category(localdict, category, amount):
    #         if category.parent_id:
    #             localdict = _sum_salary_rule_category(localdict, category.parent_id, amount)
    #         localdict['categories'].dict[category.code] = category.code in localdict['categories'].dict and localdict['categories'].dict[category.code] + amount or amount
    #         return localdict

    #     class BrowsableObject(object):
    #         def __init__(self, employee_id, dict, env):
    #             self.employee_id = employee_id
    #             self.dict = dict
    #             self.env = env

    #         def __getattr__(self, attr):
    # return attr in self.dict and self.dict.__getitem__(attr) or 0.0

    #     class InputLine(BrowsableObject):
    #         """a class that will be used into the python code, mainly for usability purposes"""
    #         def sum(self, code, from_date, to_date=None):
    #             if to_date is None:
    #                 to_date = fields.Date.today()
    #             self.env.cr.execute("""SELECT sum(case when hp.credit_note = False then (pl.total) else (-pl.total) end)
    #                         FROM hr_payslip as hp, hr_payslip_line as pl
    #                         WHERE hp.employee_id = %s AND hp.state = 'done'
    #                         AND hp.date_from >= %s AND hp.date_to <= %s AND hp.id = pl.slip_id AND pl.code = %s""",
    #                                 (self.employee_id, from_date, to_date, code))
    #             res = self.env.cr.fetchone()
    #             return res and res[0] or 0.0
    #     result_dict = {}
    #     rules_dict = {}
    #     worked_days_dict = {}
    #     inputs_dict = {}
    #     blacklist = []
    #     # payslip = self.env['hr.payslip'].browse(payslip_id)
    #     # for worked_days_line in payslip.worked_days_line_ids:
    #     #     worked_days_dict[worked_days_line.code] = worked_days_line
    #     # for input_line in payslip.input_line_ids:
    #     #     inputs_dict[input_line.code] = input_line

    #     categories = BrowsableObject(contract.employee_id.id, {}, self.env)
    #     rules = BrowsableObject(contract.employee_id.id, rules_dict, self.env)
    #     inputs = InputLine(contract.employee_id.id, inputs_dict, self.env)

    #     baselocaldict = {'categories': categories, 'rules': rules, 'inputs': inputs}
    #     # get the ids of the structures on the contracts and their parent id as well
    #     if len(contract) == 1 and contract.struct_id:
    #         structure_ids = list(set(contract.struct_id._get_parent_structure().ids))
    #     else:
    #         structure_ids = contract.get_all_structures()
    #     # get the rules of the structure and thier children
    #     rule_ids = self.env['hr.payroll.structure'].browse(structure_ids).get_all_rules()
    #     # run the rules by sequence
    #     sorted_rule_ids = [id for id, sequence in sorted(rule_ids, key=lambda x:x[1])]
    #     sorted_rules = self.env['hr.salary.rule'].browse(sorted_rule_ids)

    #     employee = contract.employee_id
    #     localdict = dict(baselocaldict, employee=employee, contract=contract)
    #     for rule in sorted_rules:
    #         key = rule.code + '-' + str(contract.id)
    #         localdict['result'] = None
    #         localdict['result_qty'] = 1.0
    #         localdict['result_rate'] = 100
    #         # check if the rule can be applied
    #         if rule._satisfy_condition(localdict) and rule.id not in blacklist:
    #             # compute the amount of the rule
    #             amount, qty, rate = rule._compute_rule(localdict)
    #             # check if there is already a rule computed with that code
    #             previous_amount = rule.code in localdict and localdict[rule.code] or 0.0
    #             # set/overwrite the amount computed for this rule in the localdict
    #             tot_rule = amount * qty * rate / 100.0
    #             localdict[rule.code] = tot_rule
    #             rules_dict[rule.code] = rule
    #             # sum the amount for its salary category
    #             localdict = _sum_salary_rule_category(localdict, rule.category_id, tot_rule - previous_amount)
    #             # create/overwrite the rule in the temporary results
    #             result_dict[key] = {
    #                 'salary_rule_id': rule.id,
    #                 'contract_id': contract.id,
    #                 'name': rule.name,
    #                 'code': rule.code,
    #                 'category_id': rule.category_id.id,
    #                 'sequence': rule.sequence,
    #                 'appears_on_payslip': rule.appears_on_payslip,
    #                 'condition_select': rule.condition_select,
    #                 'condition_python': rule.condition_python,
    #                 'condition_range': rule.condition_range,
    #                 'condition_range_min': rule.condition_range_min,
    #                 'condition_range_max': rule.condition_range_max,
    #                 'amount_select': rule.amount_select,
    #                 'amount_fix': rule.amount_fix,
    #                 'amount_python_compute': rule.amount_python_compute,
    #                 'amount_percentage': rule.amount_percentage,
    #                 'amount_percentage_base': rule.amount_percentage_base,
    #                 'register_id': rule.register_id.id,
    #                 'amount': amount,
    #                 'employee_id': contract.employee_id.id,
    #                 'quantity': qty,
    #                 'rate': rate,
    #             }
    #         else:
    #             # blacklist this rule and its children
    #             blacklist += [id for id, seq in rule._recursive_search_of_rules()]

    #     return list(result_dict.values())


class Allowance(models.Model):
    _name = 'allowance.allowance'

    hr_contract_id = fields.Many2one('hr.contract')
    salary_rule_id = fields.Many2one('hr.salary.rule', 'Salary Rule')
    amount = fields.Float('Amount')
    allowance = fields.Selection([
        ('mobile_allowance', "Mobile Allowance"),
        ('fuel_allowance', "Fuel Allowance"),
        ('monitoring_allowance', "Monitoring Fee"),
        ('travel_alw', "Transport Allowance"),
        ('acting_allowance', "Acting Allowance"),
        ('responsibility_allowance', "Responsibility Allowance"),
        ('entertainment_allowance', "Entertainment Allowance"),
        ('house_allowance', "House Allowance")], "Allowance")


# class SalaryStructure(models.Model):
#     _name = 'scheduled.pay'

#     name = fields.Char('Name')


class Deduction(models.Model):
    _name = 'deduction.deduction'

    hr_contract_id = fields.Many2one('hr.contract')
    salary_rule_id_ded = fields.Many2one('hr.salary.rule', 'Salary Rule')
    amount = fields.Float('Amount', default=0.0)
    deduction = fields.Selection([
        ('icea_pension', "ICEA Pension"),
        ('cfc_pension', "CFC Pension"),
        ('insurance_deduction', "Insurance Deduction"),
        ('staff_welfare', "Staff Welfare"),
        ('mobile_top_up_deduction', "Mobile Top Up Deduction"),
        ('aar_deduction', "AAR Deduction"),
        ('queens_way_sacco', "Queens Way Sacco"),
        ('nation_sacco', "Nation Sacco"),
        ('icea', "ICEA")], "Deduction"
    )


class EmployeeCommissionSettlement(models.Model):
    _name = 'employee.commission.settlement'

    total_commission_id = fields.Many2one(
        "hr.contract", string="Commission")
    source = fields.Char('Date')
    amount = fields.Float('Amount')
    state = fields.Selection([
        ("open", "Open"),
        ("invoiced", "Amount Fixed")
    ], 'State')
    total_amount = fields.Float('Total Amount')
    advertiser_id = fields.Many2one('res.partner', 'Advertiser')
    date = fields.Date('Date', default=fields.date.today())


class HrPayslipLine(models.Model):
    _inherit = 'hr.payslip.line'

    salary_contract_id = fields.Many2one('hr.contract')
    slip_id = fields.Many2one(
        'hr.payslip', string='Pay Slip', required=False, ondelete='cascade')


class ProbationExtend(models.Model):
    _name = 'probation.extend'

    old_date_start = fields.Date('Initial Probation Start Date')
    reason_ext = fields.Text('Extension Reason')
    old_date_end = fields.Date('Initial Probation End Date')
    contract_id = fields.Many2one('hr.contract')
