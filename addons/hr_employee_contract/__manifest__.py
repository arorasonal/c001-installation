# -*- coding: utf-8 -*-

{
    'name': 'Indian Employee Contract',
    'version': '1.0',
    'author': 'Apagen Solutions Pvt. Ltd.',
    'category': 'Human Resources',
    'sequence': 6,
    'website': 'http://www.apagen.com',
    'summary': '',
    'description': """This application extends the functionality of Employee Contract""",
    'depends': [
        'hr',
        'hr_contract',
        'hr_payroll_account',
        'hr_employee_register'
    ],
    'data': [
        'security/ir.model.access.csv',
        'views/hr_contract_sequence.xml',
        'wizard/probation_extend_view.xml',
        'views/hr_contract_view.xml',
        'views/hr_contract_template.xml',
        'views/hr_contract_scheduler.xml',
        'wizard/basic_salary_wiz.xml',
        'wizard/probation_period_wiz_view.xml',
        # 'report/contract_report_template.xml',
    ],
    'installable': True,
    'application': True,
    'auto_install': False,
}
