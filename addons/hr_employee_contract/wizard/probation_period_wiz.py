# -*- coding: utf-8 -*-

from odoo import api, fields, models, tools
from odoo.tools.translate import _
from cStringIO import StringIO
import base64
import xlwt


class probation_wiz(models.Model):
    _name = 'probation.wiz'

    company_id = fields.Many2one(
        'res.company',
        'Company',
        required=True,
        default=lambda self: self.env[
            'res.company']._company_default_get('probation.wiz'))
    date_start = fields.Date('Start Date', required=True)
    date_end = fields.Date('End Date', required=True)
    status1 = fields.Selection(
        [('draft', "Draft"),
         ('confirmed', "Confirmed"),
         ('on_probation', "On Probation"),
         ('prob_extended', "Probation Extended"),
         ('prob_lapsed', "Probation Lapsed"),
         ('promote', "Promote"),
         ('terminated', "Terminated")], "Status", default='prob_extended')

    @api.multi
    def print_report_probation(self):
        ir_model_data = self.env['ir.model.data']
        template_id = ir_model_data.get_object_reference(
            'hr_employee_contract',
            'hr_contract_report_tree_view')[1]
        for data in self:
            date_start = data.date_start
            date_end = data.date_end
            company_id = data.company_id.id
            self.with_context(
                {
                    'company': data.company_id.name,
                    'from_date': data.date_start,
                    'to_date': data.date_end,
                }
            )

        return {
            'name': _('Probationer’s Report'),
            'view_type': 'form',
            'view_mode': 'tree',
            'res_model': 'hr.contract',
            'type': 'ir.actions.act_window',
            'view_id': template_id,
            'context': self.env.context,
            'domain': [
                ('status1', 'in', ('on_probation', 'prob_extended')),
                ('employee_id.active', '=', True),
                ('company_id', '=', company_id),
                ('trial_date_start', '>=', date_start),
                ('trial_date_end', '<=', date_end)],
        }
