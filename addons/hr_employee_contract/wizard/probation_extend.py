# -*- coding: utf-8 -*-

from odoo import api, fields, models, tools
from odoo.tools.translate import _
from cStringIO import StringIO
import base64
import xlwt
from datetime import date


class ProbationExtendWiz(models.Model):
    _name = 'probation.extend.wiz'

    @api.model
    def default_get(self, fields):
        """ To get default values for the object.
        @param self: The object pointer.
        @param cr: A database cursor
        @param uid: ID of the user currently logged in
        @param fields: List of fields for which we want default values
        @param context: A standard dictionary
        @return: A dictionary which of fields with values.
        """
        res = {}
        record_id = self.env.context.get('active_id')
        if record_id:
            contract = self.env['hr.contract']
#           contract_id = contract.browse(cr, uid, record_id, context)
            contract_ids = contract.search([('id', '=', record_id)], limit=1)
            print contract_ids
            if contract_ids:
                contract_id = contract_ids[0]
                contract_id = contract.browse(record_id)
                res['initial_prob_start_date'] = contract_id.trial_date_start
                res['initial_prob_end_date'] = contract_id.trial_date_end

        return res

    new_prob_start_date = fields.Date('New Probation Start Date')
    new_prob_end_date = fields.Date('New Probation End Date')
    initial_prob_start_date = fields.Date('Initial Probation Start Date')
    initial_prob_end_date = fields.Date('Initial Probation End Date')
    company_id = fields.Many2one(
        'res.company',
        'Company',
        default=lambda self: self.env[
            'res.company']._company_default_get('basic.salary.wiz'))
    reason_ext = fields.Text('Extension Reason')

    @api.multi
    def button_prob_extend(self):
        """ To extend the probation period for the active contract """
        contract_obj = self.env['hr.contract']
        contract = contract_obj.browse(self.env.context['active_ids'])
        new_id = contract.write(
            {'trial_date_start': self.new_prob_start_date,
             'trial_date_end': self.new_prob_end_date,
             'status1': 'prob_extended',
             'probation_extend_ids': [(0, 0, {
                 'old_date_start': self.initial_prob_start_date,
                 'old_date_end': self.initial_prob_end_date,
                 'reason_ext': self.reason_ext})]
             })
        return new_id
