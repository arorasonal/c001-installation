from odoo import api, fields, models
from odoo.tools.translate import _


class WizardMoveHistory(models.TransientModel):

    _name = 'wizard.move.history'
    _description = 'Wizard that print the stock move history'

    choose_date = fields.Boolean('Choose a Particular Date', default=False)
    date = fields.Datetime('Date', required=True, default=fields.Datetime.now)

    @api.multi
    def open_table(self):

        data = self.read(self.ids)[0]
        ctx = self.env.context.copy()
        ctx['history_date'] = data['date']
        ctx['search_default_group_by_product'] = True
        ctx['search_default_group_by_location'] = True
        return {
            'domain': "[('date', '<=', '" + data['date'] + "')]",
            'name': _('Stock Value At Date'),
            'view_type': 'form',
            'view_mode': 'tree,graph',
            'res_model': 'stock.history',
            'type': 'ir.actions.act_window',
            'context': ctx,
        }
