# -*- coding: utf-8 -*-
import time
from datetime import datetime
from odoo import api, fields, models, tools
from odoo.tools.translate import _


class BasicSalaryWiz(models.Model):
    _name = 'basic.salary.wiz'

    company_id = fields.Many2one(
        'res.company',
        'Company',
        required=True,
        default=lambda self: self.env[
            'res.company']._company_default_get('basic.salary.wiz'))
    date_start = fields.Date('Date Start', required=True)
    date_end = fields.Date('Date End', required=True)

    @api.multi
    def print_report_basic_salary(self):
        ir_model_data = self.env['ir.model.data']
        template_id = ir_model_data.get_object_reference(
            'hr_employee_contract',
            'hr_basic_salary_view_tree'
        )[1]
        for data in self:
            company_id = data.company_id.id
            date_start = data.date_start
            date_end = data.date_end
            self.env.context.update({'company': data.company_id.name})
        return {
            'name': _('Basic Salary Report'),
            'view_type': 'form',
            'view_mode': 'tree',
            'res_model': 'hr.contract',
            'type': 'ir.actions.act_window',
            'view_id': template_id,
            'context': self.env.context,
            'domain': [
                ('company_id', '=', company_id), '|',
                ('date_start', '>=', date_start),
                ('date_end', '<=', date_end)],
        }
