# -*- coding: utf-8 -*-

from odoo import api, fields, models, tools
from odoo.tools.translate import _

class reassignement_wiz(models.Model):
    _name = 'reassignement.wiz'

    company_id = fields.Many2one(
        'res.company',
        'Company',
        required=True,
        default=lambda self: self.env['res.company']._company_default_get('reassignement.wiz'))
    date_start = fields.Date('Start Date', required=True)
    date_end = fields.Date('End Date', required=True)

    @api.multi
    def print_report_reassignement(self):

        ir_model_data = self.env['ir.model.data']
        contract_obj = self.env["hr.contract"]
        final_contracts = []
        context = self.env.context.copy() #Ajay
        '''self.env.context.update(
                {
                    'company': data.company_id.name,
                    'from_date': data.date_start,
                    'to_date': data.date_end,
                }
            )'''
        self.env.cr.execute('''
            select DISTINCT employee_id from hr_contract where company_id = %s''', [self.company_id.id])
        empls = self.env.cr.fetchall()
        for employee in empls:
            contracts = contract_obj.search(
                [('company_id', '=', self.company_id.id),
                 ('employee_id', '=', employee[0])],
                limit=2)
            if len(contracts) > 1:
                latest_contract = contract_obj.search(
                    [('company_id', '=', self.company_id.id),
                     ('employee_id', '=', employee[0]), 
                     ('date_start', '>=', self.date_start),
                     ('date_start', '<=', self.date_end)],
                    order='id DESC' ,limit=1
                )
                if latest_contract:
                    final_contracts.append(latest_contract.id)
        template_id = ir_model_data.get_object_reference(
            'hr_employee_contract', 'hr_contract_reassignement_view_tree')[1]
        
        return {
            'name': _('Reassignment Report'),
            'view_type': 'form',
            'view_mode': 'tree',
            'res_model': 'hr.contract',
            'type': 'ir.actions.act_window',
            'view_id': template_id,
            'domain': [('id', 'in', final_contracts)],
            'context': self.env.context,
        }   