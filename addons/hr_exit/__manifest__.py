{
    'name': 'Employee Exit',
    'sumary': 'Request for exit',
    'author': 'Apagen Solutions Pvt. Ltd.',
    'websites': 'www.apagen.com',
    'category': 'Human Resources',
    'version': '10.0.0.1',
    'depends': [
        'hr',
        'survey',
        'hr_employee_register'
    ],
    'data': [
        'security/ir.model.access.csv',
        # 'security/ir_rule.xml',
        'views/employee_exit.xml',
        'views/employee_exit_menu_view.xml',
        'views/exit_sequence.xml',
        'views/exit_email_template.xml',
        'views/exit_interview_email_template.xml',
        'data/exit_data.xml'
    ],
    'demo': [],
    'installable': True

}
