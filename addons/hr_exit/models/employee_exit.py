from odoo import api, fields, models, tools
from datetime import datetime, timedelta
from odoo.exceptions import UserError
from dateutil.relativedelta import relativedelta
from odoo.tools.translate import _


class EmployeeExit(models.Model):
    _name = 'employee.exit'
    _order = 'id DESC'
    _rec_name = "employee_id"
    _inherit = 'mail.thread'

    @api.multi
    def _get_current_employee(self):
        ids = self.env['hr.employee'].search(
            [('user_id.id', '=', self.env.uid)])
        if ids:
            return ids[0]
        return False

    NOTICE_PAY_STATUS = [
        ('yes', 'Yes'),
        ('no', 'No'),
        ('held', 'Held'),
    ]

    state = fields.Selection([
        ('in_progress', 'In Progress'),
        ('awaiting_clearance', 'Awaiting Clearance'),
        ('exit_survey', 'Exit Interview'),
        ('validate', 'Full & Final'),
        ('reject', 'Rejected'),
        ('cancelled', 'Cancelled'),
        ('done', 'Done'),
    ], 'State', default="in_progress", track_visibility='onchange')

    employee_id = fields.Many2one(
        'hr.employee',
        'Employee', required=True,
        readonly=False, states={
            'in_progress': [('readonly', False)]},
        default=_get_current_employee,
    )
    job_id = fields.Many2one(
        'hr.job', 'Designation',
        required=True, readonly=False, states={
            'in_progress': [('readonly', False)]}
    )
    joining_date = fields.Date(
        'Joining Date', readonly=True, states={
            'in_progress': [('readonly', False)]}
    )
    department_id = fields.Many2one(
        'hr.department', 'Department',
        required=True, readonly=False, states={
            'in_progress': [('readonly', False)]}
    )
    manager_id = fields.Many2one(
        'hr.employee', ' Employee Manager',
        required=True, readonly=False, states={
            'in_progress': [('readonly', False)]}
    )
    trial_date_start = fields.Date('Trial Start Date')
    trial_date_end = fields.Date('Trial End Date')
    type_id = fields.Many2one('exit.type', 'Exit Type', required=True)
    name = fields.Char('Exit Reference')
    remark = fields.Text('HR Remarks')
    notification_date = fields.Date(
        'Exit Notification Date', required=True,
        readonly=True, states={
            'in_progress': [('readonly', False)]}
    )
    interview_date = fields.Date(
        'Exit Interview Date', readonly=True, states={
            'in_progress': [('readonly', False)]}
    )

    exit_date = fields.Date(
        'Actual Last working Date'
    )
    notice_pay_recv = fields.Selection(
        NOTICE_PAY_STATUS,
        'Notice Pay Received'
    )
    reason = fields.Char('Reason')
    staff_no = fields.Char('Staff No.')
    company_id = fields.Many2one(
        'res.company', 'Company', readonly=True, states={
            'in_progress': [('readonly', False)]}
    )
    length_of_service = fields.Char(
        type='char', compute="get_service_duration"
    )
    last_salary_drawn = fields.Float(
        string="Last Salary Drawn",
        type='float', store=False
    )
    reason_note_id = fields.Text("Reason For Rejection")
    exit_day = fields.Char(
        compute="get_exit_day", string="Exit Day",
        type='char', store=False
    )
    exit_year = fields.Date(
        compute="get_exit_year", string="Exit Year",
        type='date', store=False
    )
    acceptance_issued_on = fields.Date('Exit Acceptance Date')

    store_fname = fields.Char('Stored Filename')
    db_datas = fields.Binary('Database Data')
    employment_type = fields.Many2one("emp.type", "Employee Type")
    employment_date = fields.Date('Date of Joining')
    leave_balanc = fields.Float(
        compute="get_leave_balance", string="Earned Leave Balance",
        type='float'
    )
    seat_vacant = fields.Selection([
        ('yes', 'Yes'),
        ('no', 'No')], 'Seat Vacant'
    )
    notice_salary = fields.Integer('Notice Period Salary')
    total_salary_for_current_month = fields.Integer(
        'Salary for current Month'
    )
    half_month_salary_for_every_year_worked = fields.Integer(
        'Half month salary for every year'
    )
    debts_to_be_paid = fields.Integer('Debts to be paid by employee')
    restructuring = fields.Char('Restructuring related to')
    notice_date = fields.Date('Date')
    description = fields.Text('Description')
    performance_hearing_date = fields.Date('Performance hearing date')
    appraisal_score = fields.Integer('Appraisal Score')
    total_sales = fields.Integer('Total Sales')
    total_sales_date = fields.Date('Date')
    notice_period_salary = fields.Integer('Salary in lieu of notice')
    commission_payable = fields.Integer('Commission Payable')
    contract_date = fields.Date('Employment Date')
    month_to_year_end = fields.Integer(
        compute="get_year_month",
        string='Month to Year End',
        type='Integer'
    )
    notice_pay_received = fields.Char('Notice Pay Received')
    medical_cover_status = fields.Char('Medical Cover Status')
    certificate_issued_on_date = fields.Date(
        'Employment Certificate Issue Date'
    )
    gross_salary = fields.Float('Last Salary Drawn')
    termination_reason = fields.Text("Exit Reason")
    notice_period = fields.Float(
        "Notice Period (Days)")
    termination_bol = fields.Boolean("Termination Done", default=False)
    registration_bol = fields.Boolean("Registration Done", default=False)
    clearance_line_ids = fields.One2many(
        'clearance.line', 'clearance_id', "Clearance Lines")
    clearance_checklist_id = fields.One2many(
        'clearance.checklist', 'clear_id', 'Clearance Checklist')
    company_id = company_id = fields.Many2one(
        'res.company',
        'Company')
    resignation_date = fields.Date('Resignation Date')
    last_work_date = fields.Date('Last Working Date')
    responsible_id = fields.Many2one('hr.employee', 'Responsible',
                                     default=_get_current_employee)
    creation_date = fields.Date('Creation Date', default=fields.Datetime.now,
                                readonly=True)
    start_date = fields.Datetime('Start Date')
    end_date = fields.Datetime('End Date')
    public_url = fields.Char(
        compute="_get_public_url", string="Public url",
        type='char', store=False
    )
    current_user = fields.Boolean(copy=False, compute="check_current_user")

    @api.multi
    def check_current_user(self):
        user = self.env.uid
        for record in self:
            if record.department_id.manager_id.user_id.id == user:
                record.current_user = True
            elif self.env.user.has_group('hr_employee_register.group_hr_department_head'):
                record.current_user = True
            else:
                record.current_user = False

    @api.multi
    def unlink(self):
        for rec in self:
            if rec.state not in ['in_progress']:
                raise UserError(
                    'You can only delete in In Progress state')
        return super(EmployeeExit, self).unlink()

    def state_draft(self):
        for record in self.clearance_checklist_id:
            if record.status == 'pending':
                raise UserError(_('Warning! No Dues check list is penidng'))
        current_date = fields.Datetime.now()
        template = self.env.ref(
            'hr_exit.submit_for_clearance')
        user_obj = self.env['res.users'].search([('id', '=', 1)])
        users = self.env['res.users'].search([])
        mail = ''
        for user in users:
            if user.has_group('hr_employee_register.group_hr_department_head'):
                mail += user.login + ','
        if template:
            for record in self:
                if record.department_id:
                    template.email_to = record.department_id.email_id or ''
                    template.email_from = user_obj.login
                    template.email_cc = mail
                    template.send_mail(self.id, force_send=True)
        self.write({'state': 'awaiting_clearance', 'start_date': current_date})
        return True

    def state_in_progress(self):
        self.write({'state': 'validate'})
        return True

    def state_reject(self):
        if not self.reason_note_id:
            raise UserError(_('Warning! Please enter reason for Cancellation'))
        else:
            self.write({'state': 'cancelled'})
        return True

    def button_done(self):
        employee_exit = self.env['hr.employee'].search(
            [('id', '=', self.employee_id.id)])
        current_date = fields.Datetime.now()
        self.write({'state': 'done', 'end_date': current_date})
        for record in employee_exit:
            employee_exit.write({
                'active': False,
                'last_working': self.exit_date
            })
        return True

    def _get_public_url(self):
        for record in self:
            record.public_url = record.type_id.survey_id.public_url

    def button_exit_survey(self):
        res = {}
        template = self.env.ref(
            'hr_exit.submit_for_exit_interview')
        user_obj = self.env['res.users'].search([('id', '=', 1)])
        users = self.env['res.users'].search([])
        mail = ''
        for user in users:
            if user.has_group('hr_employee_register.group_hr_department_head'):
                mail += user.login + ','
        if template:
            for record in self:
                if record.employee_id:
                    template.email_to = record.employee_id.work_email or ''
                    template.email_from = user_obj.login
                    template.email_cc = mail
                    template.send_mail(self.id, force_send=True)
        for record in self:
            if record.type_id.survey_id:
                if record.type_id.survey_id.stage_id != 'Closed':
                    res = record.type_id.survey_id.action_test_survey()
            else:
                raise UserError(
                    _('Warning! Please configure survey in Exit configuration where exit type is define'))
        for rec in self.clearance_line_ids:
            if rec.clearence_done == 'pending':
                raise UserError(
                    _('Warning! No Dues clearence details is penidng'))
        self.write({'state': 'exit_survey'})
        return res

    def current_employee_get(self):
        ids = self.env['hr.employee'].search([('user_id', '=', self.env.uid)])
        if ids:
            return ids[0]
        return False

    @api.model
    def create(self, vals):
        clearance_list = self.env['clearance.checklist.conf']
        clearance_ids = clearance_list.search([('active', '=', True)])
        DATE_FORMAT = "%Y-%m-%d"
        # vals['employee_id'] = self.env.uid
        employee = self.env['hr.employee'].search(
            [('id', '=', vals.get('employee_id'))])
        contract_obj = self.env['hr.contract'].search(
            [('employee_id', '=', vals.get('employee_id'))])
        vals.update({
            'name': self.env['ir.sequence'].next_by_code('employee.exit'),
            'employment_date': employee.date_of_joining,
            'employment_type': employee.employee_type.id,
            'department_id': employee.department_id.id,
            'manager_id': employee.parent_id.id,
            'job_id': employee.job_id.id,
            'company_id': employee.company_id.id,
            'notice_period': contract_obj.notice_period
        })

        comp_ids = self.env['hr.department'].search(
            [('company_id', '=', vals.get('company_id'))])
        if 'clearance_line_ids' in vals:
            for i, lines in enumerate(comp_ids):
                performance_id = vals['clearance_line_ids'][i][2]
                performance_id['department_id'] = lines.id
                vals['clearance_line_ids'][i][2] = performance_id
        if 'clearance_checklist_id' in vals:
            for i, lines in enumerate(clearance_ids):
                clearance_id = vals['clearance_checklist_id'][i][2]
                clearance_id['name'] = lines.id
                vals['clearance_checklist_id'][i][2] = clearance_id
        if 'notification_date' in vals:
            pfrom = datetime.strptime(
                vals.get('notification_date'), DATE_FORMAT)
            date_last = (pfrom + timedelta(days=contract_obj.notice_period)).strftime(
                DATE_FORMAT)
            vals['last_work_date'] = date_last
        return super(EmployeeExit, self).create(vals)

    @api.multi
    def write(self, vals):
        # res = super(EmployeeExit, self).write(vals)
        DATE_FORMAT = "%Y-%m-%d"
        clearance_list = self.env['clearance.checklist.conf']
        clearance_ids = clearance_list.search([('active', '=', True)])
        employee = self.env['hr.employee'].search(
            [('id', '=', vals.get('employee_id'))])
        contract_obj = self.env['hr.contract'].search(
            [('employee_id', '=', vals.get('employee_id'))])
        if 'employee_id' in vals:
            vals.update({
                'employment_date': employee.date_of_joining,
                'employment_type': employee.employee_type.id,
                'department_id': employee.department_id.id,
                'manager_id': employee.parent_id.id,
                'job_id': employee.job_id.id,
                'company_id': employee.company_id.id,
                'notice_period': contract_obj.notice_period
            })
        comp_ids = self.env['hr.department'].search(
            [('company_id', '=', self.company_id.id)])
        if 'clearance_line_ids' in vals and 'employee_id' in vals:
            for i, lines in enumerate(comp_ids):
                performance_id = vals['clearance_line_ids'][i][2]
                performance_id['department_id'] = lines.id
                vals['clearance_line_ids'][i][2] = performance_id
        if 'clearance_checklist_id' in vals and 'employee_id' in vals:
            for i, lines in enumerate(clearance_ids):
                clearance_id = vals['clearance_checklist_id'][i][2]
                clearance_id['name'] = lines.id
                vals['clearance_checklist_id'][i][2] = clearance_id
        if 'notification_date' in vals:
            pfrom = datetime.strptime(
                vals.get('notification_date'), DATE_FORMAT)
            date_last = (pfrom + timedelta(days=self.notice_period)).strftime(
                DATE_FORMAT)
            vals['last_work_date'] = date_last
        return super(EmployeeExit, self).write(vals)

    @api.onchange('employee_id')
    def onchange_employee_id(self):
        check_list = []
        clearance_list = self.env['clearance.checklist.conf']
        clearance_ids = clearance_list.search([('active', '=', True)])
        for exit_record_id in self:
            contract_obj = self.env['hr.contract'].search([
                ('employee_id', '=', exit_record_id.employee_id.id)])
            self.employment_date = self.employee_id.date_of_joining
            self.employment_type = self.employee_id.employee_type
            self.job_id = self.employee_id.job_id
            self.department_id = self.employee_id.department_id
            self.manager_id = self.employee_id.parent_id
            self.company_id = self.employee_id.company_id
            self.notice_period = contract_obj.notice_period
            self.get_salary_drawn()
            self.get_leave_balance()
        for clearance in clearance_ids:
            check_list.append((0, 0, {
                'name': clearance.id,
                'status': 'pending'}))
            self.clearance_checklist_id = check_list

    @api.onchange('type_id')
    def onchange_type_id(self):
        if self.type_id.name == "Termination":
            self.termination_bol = True
            self.registration_bol = False
        elif self.type_id.name == "Registration":
            self.termination_bol = False
            self.registration_bol = True
        else:
            self.termination_bol = False
            self.registration_bol = False

    @api.onchange('department_id')
    def onchange_department_id(self):
        """Onchange Employee Method."""
        check_list = []
        comp_ids = self.env['hr.department'].search(
            [('company_id', '=', self.company_id.id)])
        for comp in comp_ids:
            check_list.append((0, 0, {
                'department_id': comp.id,
                'clearence_done': 'pending'
            }))
            self.clearance_line_ids = check_list

    @api.onchange('notification_date')
    def onchange_notification_date(self):
        """Onchange Employee Method."""
        DATE_FORMAT = "%Y-%m-%d"
        for rec in self:
            if rec.notification_date:
                pfrom = datetime.strptime(
                    rec.notification_date, DATE_FORMAT)
                date_last = (pfrom + timedelta(days=rec.notice_period)).strftime(
                    DATE_FORMAT)
                self.last_work_date = date_last

    @api.multi
    def get_service_duration(self):
        years_months_days = ''
        m = ''
        y = ''
        for record_id in self:
            if self.exit_date:
                if record_id.employee_id:
                    date2 = datetime.strptime(
                        record_id.exit_date, '%Y-%m-%d'
                    )
                if record_id.employment_date:
                    date1 = datetime.strptime(str(record_id.employment_date),
                                              '%Y-%m-%d')
                    delta = relativedelta(date2, date1)
                    if delta.years > 1:
                        y = str(delta.years) + 'years' + ' '
                    else:
                        y = str(delta.years) + 'year' + ' '
                    if delta.months > 1:
                        m = str(delta.months) + 'months'
                    else:
                        m = str(delta.months) + 'month'
                    years_months_days = y + m
                self.length_of_service = years_months_days

    @api.multi
    def get_exit_day(self):
        for record in self:
            if self.exit_date:
                date_id = record.exit_date
                dat_id_obj = datetime.strptime(date_id, '%Y-%m-%d')
                day = dat_id_obj.strftime("%A")
                self.exit_day = day

    @api.multi
    def get_exit_year(self):
        for record in self:
            if self.exit_date:
                exit_dates = datetime.strptime(record.exit_date, '%Y-%m-%d')
                if exit_dates.month > 6:
                    year_change = exit_dates + relativedelta(years=1)
                    year = year_change.year
                    last_date = exit_dates.replace(day=30, month=6, year=year)
                else:
                    year = exit_dates.year
                    last_date = exit_dates.replace(day=30, month=6, year=year)
                self.exit_year = last_date

    @api.multi
    def get_year_month(self):
        for record in self:
            date_exit = record.exit_date
            if date_exit:
                date_exit_format = datetime.strptime(date_exit, '%Y-%m-%d')
                exit_month = (date_exit_format.month) + 6
                if exit_month < 13:
                    self.month_to_year_end = (12 - exit_month)
                else:
                    self.month_to_year_end = (12 - (exit_month - 12))

    @api.multi
    def get_salary_drawn(self):
        for exit_record_id in self:
            last_sal_drawn = 0.0
            contract_obj = self.env['hr.contract'].search([
                ('employee_id', '=', exit_record_id.employee_id.id),
                ('status1', '=', 'confirmed')])
            if contract_obj:
                for contract_id in contract_obj:
                    if exit_record_id.employee_id.id == contract_id.employee_id.id:
                        last_sal_drawn = contract_id.gross_sal
            else:
                contract_obj1 = self.env['hr.contract'].search([
                    ('employee_id', '=', exit_record_id.employee_id.id),
                    ('status1', '=', 'close_contract')])
                if contract_obj1:
                    for contract_id in contract_obj1:
                        if exit_record_id.employee_id.id == contract_id.employee_id.id:
                            last_sal_drawn = contract_id.gross_sal
                else:
                    contract_obj2 = self.env['hr.contract'].search([
                        ('employee_id', '=', exit_record_id.employee_id.id),
                        ('status1', '=', 'terminated')])
                    if contract_obj2:
                        for contract_id in contract_obj2:
                            if exit_record_id.employee_id.id == contract_id.employee_id.id:
                                last_sal_drawn = contract_id.gross_sal
            self.last_salary_drawn = last_sal_drawn

    @api.multi
    def get_leave_balance(self):
        for record_id in self:
            holiday_obj = self.env['hr.holidays'].search([
                ('employee_id', '=', record_id.employee_id.id),
                ('holiday_status_id.name', '=', 'Earned Leave'),
                ('state', '=', 'validate')])
            if holiday_obj:
                self.leave_balanc = holiday_obj[0].current_remaining_leave


class ExitType(models.Model):
    _name = "exit.type"

    name = fields.Char("Exit Type")
    survey_id = fields.Many2one('survey.survey', 'Exit Interview Form')


class ClearenceLines(models.Model):
    """docstring for ClassName"""
    _name = "clearance.line"

    department_id = fields.Many2one(
        'hr.department', 'Department')
    clearence_date = fields.Date(
        'Clearance Date'
    )
    clearence_done = fields.Selection([
        ('pending', 'Pending'),
        ('in_progress', 'In Progress'),
        ('hold', 'Hold'),
        ('done', 'Done'),
        ('not_applicable', 'Not Applicable')
    ], 'Clearance Status', default="pending")
    datas = fields.Binary(string='Clearance Form', nodrop=True)
    datas_fname = fields.Char('File Name')
    clearance_id = fields.Many2one('employee.exit', 'Clearance')
    remarks = fields.Char('Remarks')


class ClearanceChecklistConf(models.Model):
    _name = 'clearance.checklist.conf'
    _rec_name = 'nameclearance'

    nameclearance = fields.Char('Name')
    active = fields.Boolean('Active', default=True)


class clearanceChecklist(models.Model):
    _name = 'clearance.checklist'

    name = fields.Many2one('clearance.checklist.conf', 'Description')
    clear_id = fields.Many2one('employee.exit', 'Clearance Line')
    status = fields.Selection([
        ('pending', 'Pending'),
        ('in_progress', 'In Progress'),
        ('done', 'Done'),
        ('hold', 'Hold'),
        ('not_applicable', 'Not Applicable')
    ], 'Status', default="pending")
    ir_attachment_joining = fields.Many2many(
        'ir.attachment', 'rel_employee_exit_attachment',
        'exit_id', 'attachment_clearance_form', 'Attachment')
    remarks = fields.Char('Remarks')
