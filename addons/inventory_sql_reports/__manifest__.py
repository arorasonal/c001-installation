# -*- coding: utf-8 -*-
# See LICENSE file for full copyright and licensing details.

{
    'name': 'Inventory Sql Reports',
    'version': '10.0.1.0.0',
    'category': 'Reports',
    'depends': ['stock','account'],
    'license': 'AGPL-3',
    'author': 'Ravi Krishnan',
    'maintainer': 'Ravi Krishnan',
    'summary': 'Reports based on SQL Views',
    'data': [
    "security/location_security.xml",
    "views/menu.xml",  
	"views/lot_serial_location_view.xml",
    "views/delivery_movements_view.xml",
    # "views/rental_location_stock_view.xml",
    # "views/non_rental_location_stock_view.xml",
    "views/add_remove_alterations_view.xml",
    "views/report_location_stock_view.xml",
    'wizards/customer_serial_stock_summary_view.xml',
    'wizards/customer_serial_stock_ledger_view.xml',
    'wizards/all_customer_serial_stock_summary_view.xml',
    "data/cron_update_report_location_stock.xml"
             ],
    'installable': True,
    'auto_install': False,
}
