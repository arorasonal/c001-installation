WITH move_lines AS (
    SELECT
        sp.name AS doc_no,
        date(sp.date_done) AS move_date,
        sp.partner_id,
        COALESCE(rp.parent_id, rp.id) AS parent_id,
        sm.product_id,
        sm.product_qty,
        CASE WHEN ((sla.name)::text = 'Customers'::text) THEN
        (('-1'::integer)::numeric * sm.product_qty)
        WHEN ((sla.name)::text = 'Rental Out Stock'::text) THEN
        (('-1'::integer)::numeric * sm.product_qty)
        WHEN ((sla.name)::text = 'Repair & Warrnty Replacement'::text) THEN
        (('-1'::integer)::numeric * sm.product_qty)
        WHEN ((slb.name)::text = 'Customers'::text) THEN
        ((1)::numeric * sm.product_qty)
        WHEN ((slb.name)::text = 'Rental Out Stock'::text) THEN
        ((1)::numeric * sm.product_qty)
        WHEN ((slb.name)::text = 'Repair & Warrnty Replacement'::text) THEN
        ((1)::numeric * sm.product_qty)
        WHEN ((spt.name)::text = 'Delivery Note - Repair & Warranty Replacement'::text) THEN
        ((1)::numeric * sm.product_qty)
        WHEN ((spt.name)::text = 'Delivery Orders - Demo'::text) THEN
        ((1)::numeric * sm.product_qty)
        WHEN ((spt.name)::text = 'Delivery Orders - for View Purpose'::text) THEN
        ((1)::numeric * sm.product_qty)
        WHEN ((spt.name)::text = 'Delivery Orders - Rental'::text) THEN
        ((1)::numeric * sm.product_qty)
        WHEN ((spt.name)::text = 'Delivery Orders - Repair'::text) THEN
        ((1)::numeric * sm.product_qty)
        WHEN ((spt.name)::text = 'Delivery Orders - Replacement'::text) THEN
        ((1)::numeric * sm.product_qty)
        WHEN ((spt.name)::text = 'Delivery Orders - Sale'::text) THEN
        ((1)::numeric * sm.product_qty)
        WHEN ((spt.name)::text = 'Delivery Orders Purpose - RI'::text) THEN
        ((1)::numeric * sm.product_qty)
        WHEN ((spt.name)::text = 'Receipts - against Purchase'::text) THEN
        (('-1'::integer)::numeric * sm.product_qty)
        WHEN ((spt.name)::text = 'Returns from Customer'::text) THEN
        (('-1'::integer)::numeric * sm.product_qty)
        WHEN ((spt.name)::text = 'Returns from Supplier'::text) THEN
        (('-1'::integer)::numeric * sm.product_qty)
        WHEN ((spt.name)::text = 'Returns to Supplier'::text) THEN
        ((1)::numeric * sm.product_qty)
        WHEN ((spt.code)::text = 'outgoing'::text) THEN
            sm.product_qty
        WHEN ((spt.code)::text = 'incoming'::text) THEN
        (- sm.product_qty)
    ELSE
        NULL::numeric
        END AS move_qty,
        COALESCE(sm.serial_id, 0) AS serial_id,
        lot.name AS serial_number,
        row_number() OVER () AS distinct_key,
        ((((((COALESCE(rp.parent_id, rp.id))::text || '-'::text) || (lot.name)::text) || '-'::text) || to_char(sp.date_done, ('yyyy-mm-dd hh24:mi:ss'::text || '-'::text))) || (row_number() OVER ())::text) AS partition_key
    FROM ((((((stock_picking sp
                            JOIN stock_picking_type spt ON ((sp.picking_type_id = spt.id)))
                        JOIN stock_location sla ON ((sla.id = sp.location_id)))
                    JOIN stock_location slb ON ((slb.id = sp.location_dest_id)))
                JOIN stock_move sm ON ((sm.picking_id = sp.id)))
            JOIN res_partner rp ON ((sp.partner_id = rp.id)))
        JOIN stock_production_lot lot ON ((lot.id = sm.serial_id)))
    WHERE ((sm.serial_id IS NOT NULL)
        AND (sp.partner_id IS NOT NULL)
        AND ((sp.state)::text = 'done'::text)
        AND ((spt.code)::text = ANY (ARRAY[('incoming'::character varying)::text,
                ('outgoing'::character varying)::text]))
        AND ((rp.id = 11485)
            OR (rp.parent_id = 11485))
        AND (sp.date_done <= '2020-10-08'::date))
),
op_moves AS (
    SELECT
        move_lines.doc_no,
        move_lines.move_date,
        move_lines.partner_id,
        move_lines.parent_id,
        move_lines.product_id,
        move_lines.product_qty,
        move_lines.move_qty,
        move_lines.serial_id,
        move_lines.serial_number,
        move_lines.distinct_key,
        move_lines.partition_key
    FROM
        move_lines
    WHERE (move_lines.move_date < '2019-12-02'::date)
),
op_bal_sum AS (
    SELECT
        opm.parent_id,
        opm.serial_number,
        sum(opm.move_qty) AS op_qty
    FROM
        op_moves opm
    GROUP BY
        opm.parent_id,
        opm.serial_number
    HAVING (sum(opm.move_qty) <> (0)::numeric)
),
pd_moves AS (
    SELECT
        move_lines.doc_no,
        move_lines.move_date,
        move_lines.partner_id,
        move_lines.parent_id,
        move_lines.product_id,
        move_lines.product_qty,
        move_lines.move_qty,
        move_lines.serial_id,
        move_lines.serial_number,
        move_lines.distinct_key,
        move_lines.partition_key
    FROM
        move_lines
    WHERE (move_lines.move_date >= '2019-12-02'::date)
),
pd_sum AS (
    SELECT
        pm.parent_id,
        pm.serial_number,
        sum(
            CASE WHEN (pm.move_qty > (0)::numeric) THEN
                pm.move_qty
            ELSE
                (0)::numeric
            END) AS in_qty,
        string_agg(
            CASE WHEN (pm.move_qty > (0)::numeric) THEN
            (((pm.doc_no)::text || ' '::text) || pm.move_date)
ELSE
    ''::text
            END, ','::text) AS in_desc,
        sum(
            CASE WHEN (pm.move_qty < (0)::numeric) THEN
            (- pm.move_qty)
        ELSE
            (0)::numeric
            END) AS out_qty,
        string_agg(
            CASE WHEN (pm.move_qty < (0)::numeric) THEN
            (((pm.doc_no)::text || ' '::text) || pm.move_date)
ELSE
    ''::text
            END, ','::text) AS out_desc
    FROM
        pd_moves pm
    GROUP BY
        pm.parent_id,
        pm.serial_number
),
serial_list AS (
    SELECT
        op_bal_sum.serial_number
    FROM
        op_bal_sum
    UNION
    SELECT
        pd_sum.serial_number
    FROM
        pd_sum
),
serial_dtls AS (
    SELECT
        sl.serial_number,
        COALESCE(op.op_qty, (0)::numeric) AS open_qty,
    COALESCE(pd.in_qty, (0)::numeric) AS in_qty,
    COALESCE(pd.out_qty, (0)::numeric) AS out_qty,
    COALESCE(pd.in_desc, ''::text) AS in_desc,
    COALESCE(pd.out_desc, ''::text) AS out_desc
FROM ((serial_list sl
        LEFT JOIN op_bal_sum op ON (((op.serial_number)::text = (sl.serial_number)::text)))
        LEFT JOIN pd_sum pd ON (((pd.serial_number)::text = (sl.serial_number)::text)))
),
move_lines_rank AS (
    SELECT
        m.doc_no,
        m.move_date,
        m.partner_id,
        m.parent_id,
        m.product_id,
        m.product_qty,
        m.move_qty,
        m.serial_id,
        m.serial_number,
        m.distinct_key,
        m.partition_key,
        row_number() OVER (PARTITION BY m.parent_id,
            m.serial_number ORDER BY m.partition_key DESC) AS rn
    FROM
        move_lines m
)
SELECT
    row_number() OVER () AS id,
        pa.name AS company_name,
        pb.name AS company_partner_name,
        mlr.serial_number,
        pc.name AS category_name,
        t.name AS product_name,
        t.product_challan,
        sd.open_qty,
        sd.in_qty,
        sd.out_qty,
        ((sd.open_qty + sd.in_qty) - sd.out_qty) AS close_qty,
    sd.in_desc,
    sd.out_desc
FROM ((((((move_lines_rank mlr
                        JOIN serial_dtls sd ON (((sd.serial_number)::text = (mlr.serial_number)::text)))
                    JOIN res_partner pa ON ((pa.id = mlr.parent_id)))
                JOIN res_partner pb ON ((pb.id = mlr.partner_id)))
            JOIN product_product p ON ((mlr.product_id = p.id)))
        JOIN product_template t ON ((p.product_tmpl_id = t.id)))
    JOIN product_category pc ON ((pc.id = t.categ_id)))
WHERE (mlr.rn = 1);

;

