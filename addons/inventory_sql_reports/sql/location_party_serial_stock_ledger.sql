DROP FUNCTION IF EXISTS public.location_party_serial_stock_ledger (int4,date,date) ;
CREATE OR REPLACE FUNCTION public.location_party_serial_stock_ledger (p_party_id integer, p_from_date date, p_end_date date)
    RETURNS TABLE (
        r_id bigint,
        r_customer_name character varying,
        r_serial_number character varying,
        r_category_name character varying,
        r_product_name character varying,
        r_product_challan character varying,
        r_open_qty integer,
        r_in_qty integer,
        r_out_qty integer,
        r_close_qty integer,
        r_in_desc  Text,
        r_out_desc  text)
    LANGUAGE plpgsql
    AS $function$
BEGIN
    RETURN QUERY WITH move_party_lines AS (
        SELECT
            sp.name AS doc_no,
            DATE(sp.date_done) AS move_date,
            sp.partner_id,
            sp.location_id,
            sp.location_dest_id,
            COALESCE(rp.parent_id, rp.id) AS parent_id,
            sm.product_id,
            sm.product_qty,
            CASE WHEN ((sla.name)::text = 'Customers'::text) THEN
            (('-1'::integer)::numeric * sm.product_qty)
            WHEN ((sla.name)::text = 'Rental Out Stock'::text) THEN
            (('-1'::integer)::numeric * sm.product_qty)
            WHEN ((sla.name)::text = 'Repair & Warrnty Replacement'::text) THEN
            (('-1'::integer)::numeric * sm.product_qty)
            WHEN ((slb.name)::text = 'Customers'::text) THEN
            ((1)::numeric * sm.product_qty)
            WHEN ((slb.name)::text = 'Rental Out Stock'::text) THEN
            ((1)::numeric * sm.product_qty)
            WHEN ((slb.name)::text = 'Repair & Warrnty Replacement'::text) THEN
            ((1)::numeric * sm.product_qty)
            WHEN ((spt.name)::text = 'Delivery Note - Repair & Warranty Replacement'::text) THEN
            ((1)::numeric * sm.product_qty)
            WHEN ((spt.name)::text = 'Delivery Orders - Demo'::text) THEN
            ((1)::numeric * sm.product_qty)
            WHEN ((spt.name)::text = 'Delivery Orders - for View Purpose'::text) THEN
            ((1)::numeric * sm.product_qty)
            WHEN ((spt.name)::text = 'Delivery Orders - Rental'::text) THEN
            ((1)::numeric * sm.product_qty)
            WHEN ((spt.name)::text = 'Delivery Orders - Repair'::text) THEN
            ((1)::numeric * sm.product_qty)
            WHEN ((spt.name)::text = 'Delivery Orders - Replacement'::text) THEN
            ((1)::numeric * sm.product_qty)
            WHEN ((spt.name)::text = 'Delivery Orders - Sale'::text) THEN
            ((1)::numeric * sm.product_qty)
            WHEN ((spt.name)::text = 'Delivery Orders Purpose - RI'::text) THEN
            ((1)::numeric * sm.product_qty)
            WHEN ((spt.name)::text = 'Receipts - against Purchase'::text) THEN
            (('-1'::integer)::numeric * sm.product_qty)
            WHEN ((spt.name)::text = 'Returns from Customer'::text) THEN
            (('-1'::integer)::numeric * sm.product_qty)
            WHEN ((spt.name)::text = 'Returns from Supplier'::text) THEN
            (('-1'::integer)::numeric * sm.product_qty)
            WHEN ((spt.name)::text = 'Returns to Supplier'::text) THEN
            ((1)::numeric * sm.product_qty)
            WHEN ((spt.code)::text = 'outgoing'::text) THEN
                sm.product_qty
            WHEN ((spt.code)::text = 'incoming'::text) THEN
            (- sm.product_qty)
        	ELSE
            NULL::numeric
            END AS move_qty,
            COALESCE(sm.serial_id, 0) AS serial_id,
            lot.name AS serial_number,
			lot.name::text || '-'::text || to_char(sp.date_done, ('yyyy-mm-dd hh24:mi:ss'::text) || '-'::text || (row_number() OVER ())::text) AS partition_key
        FROM
            stock_picking sp
            JOIN stock_picking_type spt ON sp.picking_type_id = spt.id
            JOIN stock_location sla ON sla.id = sp.location_id
            JOIN stock_location slb ON slb.id = sp.location_dest_id
            JOIN stock_move sm ON sm.picking_id = sp.id
            JOIN res_partner rp ON sp.partner_id = rp.id
            JOIN stock_production_lot lot ON lot.id = sm.serial_id
        WHERE ((sm.serial_id IS NOT NULL)
            AND (sp.partner_id IS NOT NULL)
            AND ((sp.STATE)::text = 'done'::text)
            AND ((spt.code)::text = ANY (ARRAY[('incoming'::character varying)::text,
                    ('outgoing'::character varying)::text]))
            AND ((rp.id = p_party_id)
                OR (rp.parent_id = p_party_id))
            AND (sp.date_done < '2020-08-15'::date))
),
move_wh_lines AS (
    SELECT
        sp.name AS doc_no,
        DATE(sp.date_done) AS move_date,
        sp.partner_id,
        sp.location_id,
        sp.location_dest_id,
        COALESCE(rp.parent_id, rp.id) AS parent_id,
        sm.product_id,
        sm.product_qty,
        CASE WHEN sla.customer_id = p_party_id  THEN
        (('-1'::integer)::numeric * sm.product_qty)
        WHEN slb.customer_id = p_party_id  THEN
        (('1'::integer)::numeric * sm.product_qty)
        WHEN ((sla.name)::text = 'Customers'::text) THEN
        (('-1'::integer)::numeric * sm.product_qty)
        WHEN ((sla.name)::text = 'Rental Out Stock'::text) THEN
        (('-1'::integer)::numeric * sm.product_qty)
        WHEN ((sla.name)::text = 'Repair & Warrnty Replacement'::text) THEN
        (('-1'::integer)::numeric * sm.product_qty)
        WHEN ((slb.name)::text = 'Customers'::text) THEN
        ((1)::numeric * sm.product_qty)
        WHEN ((slb.name)::text = 'Rental Out Stock'::text) THEN
        ((1)::numeric * sm.product_qty)
        WHEN ((slb.name)::text = 'Repair & Warrnty Replacement'::text) THEN
        ((1)::numeric * sm.product_qty)
        WHEN ((spt.name)::text = 'Delivery Note - Repair & Warranty Replacement'::text) THEN
        ((1)::numeric * sm.product_qty)
        WHEN ((spt.name)::text = 'Delivery Orders - Demo'::text) THEN
        ((1)::numeric * sm.product_qty)
        WHEN ((spt.name)::text = 'Delivery Orders - for View Purpose'::text) THEN
        ((1)::numeric * sm.product_qty)
        WHEN ((spt.name)::text = 'Delivery Orders - Rental'::text) THEN
        ((1)::numeric * sm.product_qty)
        WHEN ((spt.name)::text = 'Delivery Orders - Repair'::text) THEN
        ((1)::numeric * sm.product_qty)
        WHEN ((spt.name)::text = 'Delivery Orders - Replacement'::text) THEN
        ((1)::numeric * sm.product_qty)
        WHEN ((spt.name)::text = 'Delivery Orders - Sale'::text) THEN
        ((1)::numeric * sm.product_qty)
        WHEN ((spt.name)::text = 'Delivery Orders Purpose - RI'::text) THEN
        ((1)::numeric * sm.product_qty)
        WHEN ((spt.name)::text = 'Receipts - against Purchase'::text) THEN
        (('-1'::integer)::numeric * sm.product_qty)
        WHEN ((spt.name)::text = 'Returns from Customer'::text) THEN
        (('-1'::integer)::numeric * sm.product_qty)
        WHEN ((spt.name)::text = 'Returns from Supplier'::text) THEN
        (('-1'::integer)::numeric * sm.product_qty)
        WHEN ((spt.name)::text = 'Returns to Supplier'::text) THEN
        ((1)::numeric * sm.product_qty)
        WHEN ((spt.code)::text = 'outgoing'::text) THEN
            sm.product_qty
        WHEN ((spt.code)::text = 'incoming'::text) THEN
        (- sm.product_qty)
	    ELSE
        NULL::numeric
        END AS move_qty,
        COALESCE(sm.serial_id, 0) AS serial_id,
        lot.name AS serial_number,
		lot.name::text || '-'::text || to_char(sp.date_done, ('yyyy-mm-dd hh24:mi:ss'::text) || '-'::text || (row_number() OVER ())::text) AS partition_key
    FROM
        stock_picking sp
        JOIN stock_picking_type spt ON sp.picking_type_id = spt.id
        JOIN stock_location sla ON sla.id = sp.location_id
        JOIN stock_location slb ON slb.id = sp.location_dest_id
        JOIN stock_move sm ON sm.picking_id = sp.id
        LEFT JOIN res_partner rp ON sp.partner_id = rp.id
        JOIN stock_production_lot lot ON lot.id = sm.serial_id
    WHERE ((sm.serial_id IS NOT NULL)
        AND ((sp.STATE)::text = 'done'::text)
        AND ((spt.code)::text = ANY (ARRAY[('incoming'::character varying)::text,
                ('outgoing'::character varying)::text]))
        AND ((sla.customer_id = p_party_id)
            OR (slb.customer_id = p_party_id))
        AND (sp.date_done > '2020-08-15'::date))
),
all_move_lines AS (
    SELECT
        doc_no,
        move_date,
        partner_id,
        parent_id,
        location_id,
        location_dest_id,
        product_id,
        product_qty,
        move_qty,
        serial_id,
        serial_number,
		partition_key
    FROM
        move_party_lines
        where move_date <= p_end_date
    UNION
    SELECT
        doc_no,
        move_date,
        partner_id,
        parent_id,
        location_id,
        location_dest_id,
        product_id,
        product_qty,
        move_qty,
        serial_id,
        serial_number,
		partition_key
    FROM
        move_wh_lines
        where move_date <= p_end_date
),
op_bal_sum AS (
    SELECT
        opm.serial_number,
        sum(opm.move_qty) AS op_qty
FROM
    all_move_lines opm
    WHERE (opm.move_date < p_from_date)
GROUP BY
    opm.serial_number
HAVING (sum(opm.move_qty) <> (0)::numeric)
),
pd_moves AS (
    SELECT
        doc_no,
        move_date,
        partner_id,
        parent_id,
        product_id,
        product_qty,
        move_qty,
        serial_id,
        serial_number
    FROM
        all_move_lines
    WHERE (move_date >= p_from_date)
),
pd_sum AS (
	SELECT pm.serial_number,
		sum(CASE 
				WHEN (pm.move_qty > (0)::NUMERIC)
					THEN pm.move_qty
				ELSE (0)::NUMERIC
				END) AS in_qty,
		string_agg(CASE 
				WHEN (pm.move_qty > (0)::NUMERIC)
					THEN (((pm.doc_no)::TEXT || ' '::TEXT) || pm.move_date)
				ELSE ''::TEXT
				END, ','::TEXT) AS in_desc,
		sum(CASE 
				WHEN (pm.move_qty < (0)::NUMERIC)
					THEN (- pm.move_qty)
				ELSE (0)::NUMERIC
				END) AS out_qty,
		string_agg(CASE 
				WHEN (pm.move_qty < (0)::NUMERIC)
					THEN (((pm.doc_no)::TEXT || ' '::TEXT) || pm.move_date)
				ELSE ''::TEXT
				END, ','::TEXT) AS out_desc
	FROM pd_moves pm
	GROUP BY pm.serial_number
),
serial_list AS (
    SELECT
        op_bal_sum.serial_number
    FROM
        op_bal_sum
    UNION
    SELECT
        pd_sum.serial_number
    FROM
        pd_sum
),
serial_dtls AS (
    SELECT
        sl.serial_number,
        COALESCE(op.op_qty, (0)::numeric) AS open_qty,
    COALESCE(pd.in_qty, (0)::numeric) AS in_qty,
    COALESCE(pd.out_qty, (0)::numeric) AS out_qty,
    COALESCE(pd.in_desc, ''::text) AS in_desc,
    COALESCE(pd.out_desc, ''::text) AS out_desc
FROM ((serial_list sl
        LEFT JOIN op_bal_sum op ON (((op.serial_number)::text = (sl.serial_number)::text)))
        LEFT JOIN pd_sum pd ON (((pd.serial_number)::text = (sl.serial_number)::text)))
),
move_lines_rank AS (
    SELECT
        m.doc_no,
        m.move_date,
        m.partner_id,
        m.parent_id,
        m.product_id,
        m.product_qty,
        m.move_qty,
        m.serial_id,
        m.serial_number,
        row_number() OVER (PARTITION BY 
            m.serial_number ORDER BY m.partition_key DESC) AS rn
    FROM
        all_move_lines m
)
SELECT
    row_number() OVER () AS id,
    rp.name as customer_name,
    mlr.serial_number,
    pc.name AS category_name,
    t.name AS product_name,
    t.product_challan,
    sd.open_qty::integer,
    sd.in_qty::integer,
    sd.out_qty::integer,
    ((sd.open_qty + sd.in_qty) - sd.out_qty)::integer AS close_qty,
    sd.in_desc,
    sd.out_desc
FROM move_lines_rank mlr
	JOIN serial_dtls sd ON sd.serial_number::text = mlr.serial_number::text
    JOIN product_product p ON mlr.product_id = p.id
    JOIN product_template t ON p.product_tmpl_id = t.id
    JOIN product_category pc ON pc.id = t.categ_id
    CROSS JOIN res_partner rp 
WHERE (mlr.rn = 1) 
AND rp.id = p_party_id;
END;
$function$
