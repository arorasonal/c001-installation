WITH work_locations
AS (
	SELECT DISTINCT item_location_id AS employee_id
	FROM item_alteration a
	JOIN stock_location b ON a.item_location_id = b.id
	WHERE (DATE (a.DATE) > CURRENT_DATE - 10)
	), all_move
AS (
	SELECT id, 'IN' AS move_type, product_qty AS qty, product_qty AS in_qty, 0 AS out_qty, location_id AS loc_id, 
		DATE (DATE) AS date_move, product_id
	FROM PUBLIC.stock_move
	WHERE DATE (DATE) > CURRENT_DATE - 10
		AND location_id IN (
			SELECT employee_id
			FROM work_locations
			)
		AND STATE = 'done'
	
	UNION ALL
	
	SELECT id, 'OUT' AS move_type, - product_qty AS qty, 0 AS in_qty, product_qty AS out_qty, location_dest_id AS 
		loc_id, DATE (DATE) AS date_move, product_id
	FROM PUBLIC.stock_move
	WHERE DATE (DATE) > CURRENT_DATE - 10
		AND location_dest_id IN (
			SELECT employee_id
			FROM work_locations
			)
		AND STATE = 'done'
	)
SELECT a.move_type, a.in_qty, a.out_qty, a.date_move, l.name as location, ptl.name as product_name, ptl.product_challan, coalesce(m.origin, '') || 
	coalesce(p.name || '-' || p.origin, '') AS narr, coalesce(pt.name, 'Alteration') AS movement_type
FROM all_move a
JOIN stock_move m ON a.id = m.id
JOIN stock_location l ON a.loc_id = l.id
LEFT JOIN stock_picking p ON m.picking_id = p.id
LEFT JOIN stock_picking_type pt ON p.picking_type_id = pt.id
JOIN product_product pp ON a.product_id = pp.id
JOIN product_template ptl ON pp.product_tmpl_id = ptl.id
where left(l.name,1) ='P'



