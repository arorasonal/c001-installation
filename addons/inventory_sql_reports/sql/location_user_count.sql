WITH active_locations AS (
    SELECT
        id AS location_id,
        active,
        complete_name AS location_name,
        usage,
        coalesce(allow_negative_stock, FALSE) AS allow_negative,
        (
            SELECT
                count(*)
            FROM
                stock_location_users_rel
            WHERE
                slid = id) AS users_allowed
        FROM
            stock_location
        WHERE
            active
)
SELECT
    *
FROM
    active_locations
WHERE
    users_allowed = 0
ORDER BY
    usage,
    location_name
