WITH partner_is_not_company_with_no_parent AS (
    SELECT
        a.id,
        a.parent_id,
        a.is_company
    FROM
        res_partner a
    WHERE
        NOT a.is_company
        AND a.parent_id IS NULL
),
partner_is_a_company AS (
    SELECT
        a.id,
        a.parent_id,
        a.is_company
    FROM
        res_partner a
    WHERE
        a.is_company
),
partner_is_not_company_but_has_parent AS (
    SELECT
        a.id,
        a.parent_id,
        a.is_company
    FROM
        res_partner a
    LEFT JOIN partner_is_a_company b ON a.parent_id = b.id
WHERE
    NOT a.is_company
    AND a.parent_id IS NOT NULL
),
partner_all_companies AS (
    SELECT
        a.id,
        a.parent_id,
        a.is_company
    FROM
        partner_is_a_company a
    UNION
    SELECT
        b.id,
        b.parent_id,
        b.is_company
    FROM
        partner_is_not_company_but_has_parent b
    UNION
    SELECT
        c.id,
        c.parent_id,
        c.is_company
    FROM
        partner_is_not_company_with_no_parent c
)
SELECT
    id,
    coalesce(parent_id, id),
    is_company
FROM
    partner_all_companies;

