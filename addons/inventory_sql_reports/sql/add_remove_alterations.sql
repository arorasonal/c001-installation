WITH alterations
AS (
	SELECT ia.id, spl.name AS serial_no, sli.complete_name AS item_location, sls.
		complete_name AS source_location, ia.DATE AS alteration_date, ia.name AS 
		alteration_no, ia.product_id, ia.new_product_id, un.name AS validated_by, DATE 
			(ia.validate_date) AS validated_on
	FROM item_alteration ia
	JOIN stock_production_lot spl ON spl.id = ia.serial_no
	JOIN stock_location sli ON ia.item_location_id = sli.id
	JOIN stock_location sls ON ia.source_location_id = sls.id
	JOIN res_users ru ON ru.id = ia.validate_by
	JOIN res_partner un ON ru.partner_id = un.id
	WHERE ia.STATE IN ('transfer')
     AND ia.validate_date > ('now'::TEXT::DATE - '14 days'::interval)
	), alt_item_add
AS (
	SELECT iaa.item_alteration_id, 'ADD'::TEXT AS item_add_remove, iaa.product_id, iaa
		.qty
	FROM item_add iaa
	), alt_item_rem
AS (
	SELECT iar.item_alteration_id, 'REM'::TEXT AS item_add_remove, iar.product_id, iar
		.qty
	FROM item_remove iar
	), alt_items_all
AS (
	SELECT item_alteration_id, product_id, item_add_remove, qty
	FROM alt_item_add
	
	UNION
	
	SELECT item_alteration_id, product_id, item_add_remove, qty
	FROM alt_item_rem
	), alteration_items
AS (
	SELECT aia.item_alteration_id, aia.item_add_remove, ptl.name AS product_name, ptl.
		product_challan, pp.barcode, aia.qty
	FROM alt_items_all aia
	JOIN product_product pp ON aia.product_id = pp.id
	JOIN product_template ptl ON pp.product_tmpl_id = ptl.id
	)
SELECT row_number() OVER () AS id, alt.serial_no, alt.item_location, alt.source_location, alt.alteration_date, 
	alt.alteration_no, alt.validated_by, alt.validated_on, alti.item_add_remove, alti
	.product_name, alti.product_challan, alti.barcode, alti.qty
FROM alterations alt
JOIN alteration_items alti ON alt.id = alti.item_alteration_id
