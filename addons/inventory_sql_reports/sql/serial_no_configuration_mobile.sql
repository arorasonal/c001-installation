SELECT
    serial_number,
    category_name,
    product_challan,
    product_name,
    location_name,
    last_move_date,
    last_move_no
FROM
    report_location_stock
WHERE
    lot_id IS NOT NULL
    AND location_function LIKE '%Rent%'
