WITH x AS (
    SELECT
        wap.id,
        wap.name,
        wap.create_date,
        wap.product_id,
        wap.category_id,
        wap.state,
        pc.name AS category_name,
        apl.specification,
        apl.valid_value,
        vsv.name AS specification_value,
        vsv.short_name
    FROM
        wiz_add_product wap
        JOIN add_product_line apl ON wap.id = apl.wiz_add_product_id
        JOIN product_category pc ON wap.category_id = pc.id
        JOIN valid_specification_value vsv ON apl.valid_value = vsv.id
    WHERE
        wap.product_id IS NOT NULL
)
SELECT
    category_name,
    specification,
    count(*)
FROM
    x
GROUP BY
    category_name,
    specification
