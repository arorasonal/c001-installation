DROP FUNCTION IF EXISTS public.location_party_serial_stock_summary (int4,date) ;
CREATE
	OR REPLACE FUNCTION PUBLIC.location_party_serial_stock_summary (
	p_party_id INTEGER,
	p_end_date DATE
	)
RETURNS TABLE (
		r_id BIGINT,
		r_customer_name CHARACTER VARYING,
		r_movement_name CHARACTER VARYING,
		r_transaction_type CHARACTER VARYING,
		r_from_location CHARACTER VARYING,
		r_to_location CHARACTER VARYING,
		r_serial_number CHARACTER VARYING,
		r_alpha_no CHARACTER VARYING,
		r_category_name CHARACTER VARYING,
		r_product_name CHARACTER VARYING,
		r_product_challan CHARACTER VARYING,
		r_doc_no CHARACTER VARYING,
		r_move_date DATE,
		r_so_no CHARACTER VARYING,
		r_balance_qty INTEGER
		) LANGUAGE plpgsql AS $FUNCTION$
BEGIN
	RETURN QUERY
	WITH move_party_lines AS (
			SELECT sp.id,
				sp.name AS doc_no,
				DATE (sp.date_done) AS move_date,
				sp.origin,
				sp.partner_id,
				COALESCE(rp.parent_id, rp.id) AS parent_id,
				sp.picking_type_id,
				sp.transaction_type_id,
				sp.order_id,
				sp.location_id,
				sla.name AS from_location,
				sp.location_dest_id,
				slb.name AS to_location,
				spt.name AS movement_name,
				spt.code AS movement_type,
				sm.product_id,
				sm.product_qty,
				CASE 
					WHEN ((sla.name)::TEXT = 'Customers'::TEXT)
						THEN (('-1'::INTEGER)::NUMERIC * sm.product_qty)
					WHEN ((sla.name)::TEXT = 'Rental Out Stock'::TEXT)
						THEN (('-1'::INTEGER)::NUMERIC * sm.product_qty)
					WHEN ((sla.name)::TEXT = 'Repair & Warrnty Replacement'::TEXT)
						THEN (('-1'::INTEGER)::NUMERIC * sm.product_qty)
					WHEN ((slb.name)::TEXT = 'Customers'::TEXT)
						THEN ((1)::NUMERIC * sm.product_qty)
					WHEN ((slb.name)::TEXT = 'Rental Out Stock'::TEXT)
						THEN ((1)::NUMERIC * sm.product_qty)
					WHEN ((slb.name)::TEXT = 'Repair & Warrnty Replacement'::TEXT)
						THEN ((1)::NUMERIC * sm.product_qty)
					WHEN ((spt.name)::TEXT = 'Delivery Note - Repair & Warranty Replacement'::TEXT)
						THEN ((1)::NUMERIC * sm.product_qty)
					WHEN ((spt.name)::TEXT = 'Delivery Orders - Demo'::TEXT)
						THEN ((1)::NUMERIC * sm.product_qty)
					WHEN ((spt.name)::TEXT = 'Delivery Orders - for View Purpose'::TEXT)
						THEN ((1)::NUMERIC * sm.product_qty)
					WHEN ((spt.name)::TEXT = 'Delivery Orders - Rental'::TEXT)
						THEN ((1)::NUMERIC * sm.product_qty)
					WHEN ((spt.name)::TEXT = 'Delivery Orders - Repair'::TEXT)
						THEN ((1)::NUMERIC * sm.product_qty)
					WHEN ((spt.name)::TEXT = 'Delivery Orders - Replacement'::TEXT)
						THEN ((1)::NUMERIC * sm.product_qty)
					WHEN ((spt.name)::TEXT = 'Delivery Orders - Sale'::TEXT)
						THEN ((1)::NUMERIC * sm.product_qty)
					WHEN ((spt.name)::TEXT = 'Delivery Orders Purpose - RI'::TEXT)
						THEN ((1)::NUMERIC * sm.product_qty)
					WHEN ((spt.name)::TEXT = 'Receipts - against Purchase'::TEXT)
						THEN (('-1'::INTEGER)::NUMERIC * sm.product_qty)
					WHEN ((spt.name)::TEXT = 'Returns from Customer'::TEXT)
						THEN (('-1'::INTEGER)::NUMERIC * sm.product_qty)
					WHEN ((spt.name)::TEXT = 'Returns from Supplier'::TEXT)
						THEN (('-1'::INTEGER)::NUMERIC * sm.product_qty)
					WHEN ((spt.name)::TEXT = 'Returns to Supplier'::TEXT)
						THEN ((1)::NUMERIC * sm.product_qty)
					WHEN ((spt.code)::TEXT = 'outgoing'::TEXT)
						THEN sm.product_qty
					WHEN ((spt.code)::TEXT = 'incoming'::TEXT)
						THEN (- sm.product_qty)
					ELSE NULL::NUMERIC
					END AS move_qty,
				COALESCE(sm.serial_id, 0) AS serial_id,
				lot_1.name AS serial_number,
				lot_1.name::TEXT || '-'::TEXT || to_char(sp.date_done, ('yyyy-mm-dd hh24:mi:ss'::TEXT) || '-'::TEXT || (row_number() OVER ())::TEXT) AS partition_key
			FROM stock_picking sp
			JOIN stock_picking_type spt ON sp.picking_type_id = spt.id
			JOIN stock_location sla ON sla.id = sp.location_id
			JOIN stock_location slb ON slb.id = sp.location_dest_id
			JOIN stock_move sm ON sm.picking_id = sp.id
			JOIN res_partner rp ON sp.partner_id = rp.id
			JOIN stock_production_lot lot_1 ON lot_1.id = sm.serial_id
			WHERE sm.serial_id IS NOT NULL
				AND sp.partner_id IS NOT NULL
				AND sp.STATE::TEXT = 'done'::TEXT
				AND spt.code::TEXT = ANY (
					ARRAY [('incoming'::character varying)::text,
                ('outgoing'::character varying)::text]
					)
				AND (
					(rp.id = p_party_id)
					OR (rp.parent_id = p_party_id)
					)
				AND sp.date_done < '2020-08-15'::DATE
			),
		move_wh_lines AS (
			SELECT sp.id,
				sp.name AS doc_no,
				DATE (sp.date_done) AS move_date,
				sp.origin,
				sp.partner_id,
				p_party_id AS parent_id,
				sp.picking_type_id,
				sp.transaction_type_id,
				sp.order_id,
				sp.location_id,
				sla.name AS from_location,
				sp.location_dest_id,
				slb.name AS to_location,
				spt.name AS movement_name,
				spt.code AS movement_type,
				sm.product_id,
				sm.product_qty,
				CASE 
					WHEN sla.customer_id = p_party_id
						THEN (('-1'::INTEGER)::NUMERIC * sm.product_qty)
					WHEN slb.customer_id = p_party_id
						THEN (('1'::INTEGER)::NUMERIC * sm.product_qty)
					WHEN ((sla.name)::TEXT = 'Customers'::TEXT)
						THEN (('-1'::INTEGER)::NUMERIC * sm.product_qty)
					WHEN ((sla.name)::TEXT = 'Rental Out Stock'::TEXT)
						THEN (('-1'::INTEGER)::NUMERIC * sm.product_qty)
					WHEN ((sla.name)::TEXT = 'Repair & Warrnty Replacement'::TEXT)
						THEN (('-1'::INTEGER)::NUMERIC * sm.product_qty)
					WHEN ((slb.name)::TEXT = 'Customers'::TEXT)
						THEN ((1)::NUMERIC * sm.product_qty)
					WHEN ((slb.name)::TEXT = 'Rental Out Stock'::TEXT)
						THEN ((1)::NUMERIC * sm.product_qty)
					WHEN ((slb.name)::TEXT = 'Repair & Warrnty Replacement'::TEXT)
						THEN ((1)::NUMERIC * sm.product_qty)
					WHEN ((spt.name)::TEXT = 'Delivery Note - Repair & Warranty Replacement'::TEXT)
						THEN ((1)::NUMERIC * sm.product_qty)
					WHEN ((spt.name)::TEXT = 'Delivery Orders - Demo'::TEXT)
						THEN ((1)::NUMERIC * sm.product_qty)
					WHEN ((spt.name)::TEXT = 'Delivery Orders - for View Purpose'::TEXT)
						THEN ((1)::NUMERIC * sm.product_qty)
					WHEN ((spt.name)::TEXT = 'Delivery Orders - Rental'::TEXT)
						THEN ((1)::NUMERIC * sm.product_qty)
					WHEN ((spt.name)::TEXT = 'Delivery Orders - Repair'::TEXT)
						THEN ((1)::NUMERIC * sm.product_qty)
					WHEN ((spt.name)::TEXT = 'Delivery Orders - Replacement'::TEXT)
						THEN ((1)::NUMERIC * sm.product_qty)
					WHEN ((spt.name)::TEXT = 'Delivery Orders - Sale'::TEXT)
						THEN ((1)::NUMERIC * sm.product_qty)
					WHEN ((spt.name)::TEXT = 'Delivery Orders Purpose - RI'::TEXT)
						THEN ((1)::NUMERIC * sm.product_qty)
					WHEN ((spt.name)::TEXT = 'Receipts - against Purchase'::TEXT)
						THEN (('-1'::INTEGER)::NUMERIC * sm.product_qty)
					WHEN ((spt.name)::TEXT = 'Returns from Customer'::TEXT)
						THEN (('-1'::INTEGER)::NUMERIC * sm.product_qty)
					WHEN ((spt.name)::TEXT = 'Returns from Supplier'::TEXT)
						THEN (('-1'::INTEGER)::NUMERIC * sm.product_qty)
					WHEN ((spt.name)::TEXT = 'Returns to Supplier'::TEXT)
						THEN ((1)::NUMERIC * sm.product_qty)
					WHEN ((spt.code)::TEXT = 'outgoing'::TEXT)
						THEN sm.product_qty
					WHEN ((spt.code)::TEXT = 'incoming'::TEXT)
						THEN (- sm.product_qty)
					ELSE NULL::NUMERIC
					END AS move_qty,
				COALESCE(sm.serial_id, 0) AS serial_id,
				lot_1.name AS serial_number,
				lot_1.name::TEXT || '-'::TEXT || to_char(sp.date_done, ('yyyy-mm-dd hh24:mi:ss'::TEXT) || '-'::TEXT || (row_number() OVER ())::TEXT) AS partition_key
			FROM stock_picking sp
			JOIN stock_picking_type spt ON sp.picking_type_id = spt.id
			JOIN stock_location sla ON sla.id = sp.location_id
			JOIN stock_location slb ON slb.id = sp.location_dest_id
			JOIN stock_move sm ON sm.picking_id = sp.id
			LEFT JOIN res_partner rp ON sp.partner_id = rp.id
			JOIN stock_production_lot lot_1 ON lot_1.id = sm.serial_id
			WHERE sm.serial_id IS NOT NULL
				AND sp.STATE::TEXT = 'done'::TEXT
				AND spt.code::TEXT = ANY (
					ARRAY [('incoming'::character varying)::text,
                    ('outgoing'::character varying)::text]
					)
				AND (
					(sla.customer_id = p_party_id)
					OR (slb.customer_id = p_party_id)
					)
				AND (sp.date_done > '2020-08-15'::DATE)
			),
		all_move_lines AS (
			SELECT mpl.doc_no,
				mpl.move_date,
				mpl.origin,
				mpl.partner_id,
				mpl.parent_id,
				mpl.picking_type_id,
				mpl.transaction_type_id,
				mpl.order_id,
				mpl.location_id,
				mpl.from_location,
				mpl.location_dest_id,
				mpl.to_location,
				mpl.movement_name,
				mpl.movement_type,
				mpl.product_id,
				mpl.product_qty,
				mpl.move_qty,
				mpl.serial_id,
				mpl.serial_number,
				mpl.partition_key
			FROM move_party_lines mpl
			WHERE mpl.move_date <= p_end_date
			
			UNION
			
			SELECT mwl.doc_no,
				mwl.move_date,
				mwl.origin,
				mwl.partner_id,
				mwl.parent_id,
				mwl.picking_type_id,
				mwl.transaction_type_id,
				mwl.order_id,
				mwl.location_id,
				mwl.from_location,
				mwl.location_dest_id,
				mwl.to_location,
				mwl.movement_name,
				mwl.movement_type,
				mwl.product_id,
				mwl.product_qty,
				mwl.move_qty,
				mwl.serial_id,
				mwl.serial_number,
				mwl.partition_key
			FROM move_wh_lines mwl
			WHERE mwl.move_date <= p_end_date
			),
		move_lines_sum AS (
			SELECT move_lines.parent_id,
				move_lines.serial_number,
				sum(move_lines.move_qty) AS balance_qty
			FROM all_move_lines move_lines
			GROUP BY move_lines.parent_id,
				move_lines.serial_number
			HAVING (sum(move_lines.move_qty) <> (0)::NUMERIC)
			),
		move_lines_rank AS (
			SELECT m.doc_no,
				m.move_date,
				m.origin,
				m.partner_id,
				m.parent_id,
				m.picking_type_id,
				m.transaction_type_id,
				m.order_id,
				m.location_id,
				m.from_location,
				m.location_dest_id,
				m.to_location,
				m.movement_name,
				m.movement_type,
				m.product_id,
				m.product_qty,
				m.move_qty,
				m.serial_id,
				m.serial_number,
				m.partition_key,
				row_number() OVER (
					PARTITION BY m.parent_id,
					m.serial_number ORDER BY m.partition_key DESC
					) AS rn
			FROM all_move_lines m
			)
	SELECT row_number() OVER () AS id,
		rp.name AS customer_name,
		mlr.movement_name,
		tt.name AS transaction_type,
		mlr.from_location,
		mlr.to_location,
		mlr.serial_number,
		lot.alpha_serial_no AS alpha_no,
		pc.name AS category_name,
		t.name AS product_name,
		t.product_challan,
		mlr.doc_no,
		mlr.move_date,
		COALESCE(sale_order.name, ''::CHARACTER VARYING) AS so_no,
		mls.balance_qty::INTEGER AS balance_qty
	FROM move_lines_rank mlr
	JOIN move_lines_sum mls ON mls.parent_id = mlr.parent_id
		AND mls.serial_number::TEXT = mlr.serial_number::TEXT
	JOIN product_product p ON mlr.product_id = p.id
	JOIN product_template t ON p.product_tmpl_id = t.id
	JOIN product_category pc ON pc.id = t.categ_id
	JOIN stock_production_lot lot ON lot.id = mlr.serial_id
	LEFT JOIN sale_order ON mlr.order_id = sale_order.id
	LEFT JOIN transaction_type tt ON tt.id = mlr.transaction_type_id
    CROSS JOIN res_partner rp 
	WHERE (mlr.rn = 1)
		AND rp.id = p_party_id;
END;
$FUNCTION$
