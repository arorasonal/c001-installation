CREATE OR REPLACE FUNCTION report_location_stock_with_last_move ()
RETURNS void -- LANGUAGE SQL
AS
$$

BEGIN
	TRUNCATE TABLE report_location_stock RESTART IDENTITY;

	WITH non_serial_product
	AS (
		SELECT a.id
		FROM product_product a
		JOIN product_template b ON a.product_tmpl_id = b.id
		WHERE b.tracking = 'none'
		), rental_location
	AS (
		SELECT stock_location.id
		FROM stock_location
		WHERE usage = 'internal'
		), out_move
	AS (
		SELECT m.product_id, m.location_id AS loc_id, sum(0) AS in_qty, sum(
				product_qty) AS out_qty
		FROM stock_move m
		JOIN non_serial_product nsp ON nsp.id = m.product_id
		JOIN rental_location rl ON rl.id = m.location_id
		WHERE m.STATE LIKE 'done'
		GROUP BY m.product_id, m.location_id
		), in_move
	AS (
		SELECT m.product_id, m.location_dest_id AS loc_id, sum(product_qty) AS 
			in_qty, sum(0) AS out_qty
		FROM stock_move m
		JOIN non_serial_product nsp ON nsp.id = m.product_id
		JOIN rental_location rl ON rl.id = m.location_dest_id
		WHERE m.STATE LIKE 'done'
		GROUP BY m.product_id, m.location_dest_id
		), all_move
	AS (
		SELECT a.product_id, a.loc_id, a.in_qty, a.out_qty
		FROM in_move a
		
		UNION
		
		SELECT b.product_id, b.loc_id, b.in_qty, b.out_qty
		FROM out_move b
		), sum_move
	AS (
		SELECT product_id, loc_id, sum(in_qty) AS in_qty, sum(out_qty) AS 
			out_qty
		FROM all_move
		GROUP BY product_id, loc_id
		), all_non_serial_move
	AS (
		SELECT NULL::INTEGER AS lot_id, loc_id AS location_id, product_id, 
			in_qty - out_qty AS bal_qty
		FROM sum_move
		WHERE in_qty <> out_qty
		), all_serial_move
	AS (
		SELECT sq.lot_id, sq.location_id, sq.product_id, 1 AS bal_qty
		FROM stock_quant sq
		JOIN stock_production_lot spl ON sq.lot_id = spl.id
			AND sq.product_id = spl.product_id
		JOIN rental_location sl ON sl.id = sq.location_id
		), final_stock
	AS (
		SELECT lot_id, location_id, product_id, bal_qty
		FROM all_serial_move
		
		UNION ALL
		
		SELECT lot_id, location_id, product_id, bal_qty
		FROM all_non_serial_move
		), alt_group
	AS (
		SELECT DISTINCT rps.product_id, pc.id AS category_id, ctp.name AS 
			category_pg_id, cts.name AS category_sc_id
		FROM final_stock rps
		JOIN product_product ppa ON rps.product_id = ppa.id
		JOIN product_template pta ON pta.id = ppa.product_tmpl_id
		JOIN product_category pc ON pta.categ_id = pc.id
		LEFT JOIN category_template ctp ON pc.id = ctp.category_id1
			AND pc.primary_alt_grouping_id = ctp.id
		LEFT JOIN category_template cts ON pc.id = cts.category_id1
			AND pc.secondary_alt_grouping_id = cts.id
		WHERE coalesce(pc.can_altered, FALSE) = TRUE
		), alt_group_short
	AS (
		SELECT x.product_id, x.category_id, x.category_pg_id, x.
			category_sc_id, pt.add_product_id, clp.short_name, clp.
			product_id AS product_pg_id, cls.product_id AS product_sc_id
		FROM alt_group x
		JOIN product_product pp ON pp.id = x.product_id
		JOIN product_template pt ON pp.product_tmpl_id = pt.id
		JOIN wiz_add_product wap ON pt.add_product_id = wap.id
		LEFT JOIN category_template_line clp ON clp.wiz_add_product_id = wap.
			id
			AND clp.category_id = x.category_pg_id
		LEFT JOIN category_template_line cls ON cls.wiz_add_product_id = wap.
			id
			AND cls.category_id = x.category_sc_id
		), alt_multi_group_name
	AS (
		SELECT y.product_id, y.product_pg_id, y.product_sc_id, pg_pt.
			short_name_product AS primary_group, sc_pt.short_name_product AS 
			secondary_group
		FROM alt_group_short y
		LEFT JOIN product_product pg_pp ON pg_pp.id = y.product_pg_id
		LEFT JOIN product_template pg_pt ON pg_pp.product_tmpl_id = pg_pt.id
		LEFT JOIN product_product sc_pp ON sc_pp.id = y.product_sc_id
		LEFT JOIN product_template sc_pt ON sc_pp.product_tmpl_id = sc_pt.id
		), alt_group_name
	AS (
		SELECT product_id, string_agg(DISTINCT primary_group, '') AS 
			primary_group, string_agg(DISTINCT secondary_group, ',') AS 
			secondary_group
		FROM alt_multi_group_name
		GROUP BY product_id
		), non_alt_group
	AS (
		SELECT DISTINCT rps.product_id, pc.id AS category_id, ctp.name AS 
			category_pg_name, cts.name AS category_sc_name
		FROM final_stock rps
		JOIN product_product ppa ON rps.product_id = ppa.id
		JOIN product_template pta ON pta.id = ppa.product_tmpl_id
		JOIN product_category pc ON pta.categ_id = pc.id
		LEFT JOIN specification_line ctp ON pc.id = ctp.category_id1
			AND pc.primary_na_grouping_id = ctp.id
		LEFT JOIN specification_line cts ON pc.id = cts.category_id1
			AND pc.secondary_na_grouping_id = cts.id
		WHERE coalesce(pc.can_altered, FALSE) = FALSE
		), non_alt_group_name
	AS (
		SELECT x.product_id, x.category_id, x.category_pg_name, x.
			category_sc_name, vsvp.name AS primary_group, vsvs.name AS 
			secondary_group
		FROM non_alt_group x
		JOIN product_product pp ON pp.id = x.product_id
		JOIN product_template pt ON pp.product_tmpl_id = pt.id
		JOIN wiz_add_product wap ON pp.id = wap.product_id
		LEFT JOIN add_product_line clp ON clp.wiz_add_product_id = wap.id
			AND clp.specification = x.category_pg_name
		LEFT JOIN add_product_line cls ON cls.wiz_add_product_id = wap.id
			AND cls.specification = x.category_sc_name
		LEFT JOIN valid_specification_value vsvp ON clp.valid_value = vsvp.id
		LEFT JOIN valid_specification_value vsvs ON cls.valid_value = vsvs.id
		), report_stock
	AS (
		SELECT row_number() OVER () AS id, a.lot_id, a.location_id, 
			t.categ_id AS category_id, a.product_id, "substring" (
				l.complete_name::TEXT, "position" (l.complete_name::TEXT, '/'::TEXT
					) + 1
				) AS location_name, coalesce(slt.name, '') AS location_function, 
			pc.name AS category_name, CASE 
				WHEN coalesce(pc.can_altered, FALSE)
					THEN 'ALTERABLE'
				ELSE 'NON-ALTERABLE'
				END AS alterable, CASE 
				WHEN coalesce(pc.numbered, FALSE)
					THEN 'NUMBERED'
				ELSE 'NON-NUMBERED'
				END AS numbered, t.name AS product_name, t.product_challan, 
			coalesce(agn.primary_group, coalesce(nagn.primary_group, '')) AS 
			primary_product_attribute, coalesce(agn.secondary_group, 
				coalesce(nagn.secondary_group, '')) AS 
			secondary_product_attribute, spl.name AS serial_number, a.bal_qty 
			AS stock_in_hand
		FROM final_stock a
		JOIN product_product p ON a.product_id = p.id
		JOIN product_template t ON p.product_tmpl_id = t.id
		JOIN stock_location l ON a.location_id = l.id
		LEFT JOIN stock_location_function_type slt ON l.
			location_function_type_id = slt.id
		JOIN product_category pc ON t.categ_id = pc.id
		LEFT JOIN stock_production_lot spl ON a.lot_id = spl.id
		LEFT JOIN alt_group_name agn ON a.product_id = agn.product_id
		LEFT JOIN non_alt_group_name nagn ON a.product_id = nagn.product_id
		WHERE coalesce(p.active, FALSE) = TRUE
		), getting_last_move_non_serial
	AS (
		SELECT row_number() OVER (
				PARTITION BY sm.location_dest_id, sm.product_id ORDER BY sm.DATE 
					DESC
				) AS rn, sm.location_dest_id AS location_id, sm.product_id, sm.
			picking_id, sm.origin, sm.DATE
		FROM stock_move sm
		JOIN report_stock rs ON sm.location_dest_id = rs.location_id
			AND sm.product_id = rs.product_id
		WHERE sm.STATE = 'done'
			--        AND sm.picking_id IS NOT NULL
			AND rs.lot_id IS NULL
		), final_last_move_non_serial
	AS (
		SELECT location_id, product_id, picking_id, origin, DATE
		FROM getting_last_move_non_serial
		WHERE rn = 1
		),
		/*
stock_move of serialised items are of three types:
 1. created thru BT transactions, they have serial_id (lot_id) filled
 2. created by alterations and other documents, they have serial_id as NULL 
 3. special transactions done on 15-aug-2020

stock_move of serialised items created by alterations and other documents, 
 they have serial_id as NULL . origin field will be filled up
 */
	serial_stock_moves_with_origin
	AS (
		SELECT row_number() OVER (
				PARTITION BY sm.location_dest_id, sm.product_id, sm.origin 
					ORDER BY sm.DATE DESC
				) AS rn, sm.location_dest_id AS location_id, sm.product_id, sm.
			picking_id, sm.origin, sm.DATE
		FROM stock_move sm
		JOIN report_stock rs ON sm.location_dest_id = rs.location_id
			AND sm.product_id = rs.product_id
		WHERE sm.STATE = 'done'
			AND sm.origin IS NOT NULL
			AND rs.lot_id IS NOT NULL
		), aug_15_special
	AS (
		SELECT stw.name, product AS product_id, serial_no, 
			destination_location AS location_id
		FROM stock_transfer_wizard_line stwl
		JOIN stock_transfer_wizard stw ON stw.id = stwl.
			stock_transfer_wizard_id
		WHERE stwl.transfer
		),
		/*
alterations are of three types:
 1. alterations of alterable items - changing components
 2. conversion of non-alterable items - adding components
 3. alterations of non-alterable items - adding specifications
 */
	alteration_serial_no_temp
	AS (
		SELECT ia.name AS origin, ia.new_product_id AS product_id, ia.
			source_location_id AS location_id, ia.serial_no, spl.name
		FROM item_alteration ia
		JOIN stock_production_lot spl ON spl.id = ia.serial_no
		WHERE STATE = 'transfer'
		), alteration_serial_no
	AS (
		SELECT xx.origin, xx.product_id, xx.location_id, spl.id AS serial_no
		FROM alteration_serial_no_temp xx
		JOIN stock_production_lot spl ON xx.name = spl.name
			AND xx.product_id = spl.product_id
		), non_alteration_serial_no_temp
	AS (
		SELECT ia.name AS origin, ia.new_product_id AS product_id, ia.
			source_location_id AS location_id, ia.serial_no, spl.name
		FROM non_item_alteration ia
		JOIN stock_production_lot spl ON spl.id = ia.serial_no
		WHERE STATE = 'transfer'
		), non_alteration_serial_no
	AS (
		SELECT xx.origin, xx.product_id, xx.location_id, spl.id AS serial_no
		FROM non_alteration_serial_no_temp xx
		JOIN stock_production_lot spl ON xx.name = spl.name
			AND xx.product_id = spl.product_id
		), conversion_non_alterable_serial_no_temp
	AS (
		SELECT ia.name AS origin, ia.new_product_id AS product_id, ia.
			source_location_id AS location_id, ia.serial_no, spl.name
		FROM conversion_non_alterable ia
		JOIN stock_production_lot spl ON spl.id = ia.serial_no
		WHERE STATE = 'transfer'
		), conversion_non_alterable_serial_no
	AS (
		SELECT xx.origin, xx.product_id, xx.location_id, spl.id AS serial_no
		FROM conversion_non_alterable_serial_no_temp xx
		JOIN stock_production_lot spl ON xx.name = spl.name
			AND xx.product_id = spl.product_id
		), all_alterations
	AS (
		SELECT origin, product_id, location_id, serial_no
		FROM alteration_serial_no
		
		UNION
		
		SELECT origin, product_id, location_id, serial_no
		FROM non_alteration_serial_no
		
		UNION
		
		SELECT origin, product_id, location_id, serial_no
		FROM conversion_non_alterable_serial_no
		
		UNION
		
		SELECT name AS origin, product_id, location_id, serial_no
		FROM aug_15_special
		), serial_stock_moves_with_origin_latest
	AS (
		SELECT smol.location_id, smol.product_id, ia.serial_no AS lot_id, smol
			.picking_id, smol.origin, smol.DATE
		FROM serial_stock_moves_with_origin smol
		JOIN all_alterations ia ON ia.origin = smol.origin
			AND smol.location_id = ia.location_id
			AND smol.product_id = ia.product_id
		WHERE smol.rn = 1
		), serial_stock_move_without_origin
	AS (
		SELECT row_number() OVER (
				PARTITION BY sm.location_dest_id, sm.product_id, sm.serial_id 
					ORDER BY sm.DATE DESC
				) AS rn, sm.location_dest_id AS location_id, sm.product_id, sm.
			serial_id AS lot_id, sm.picking_id, sm.origin, sm.DATE
		FROM stock_move sm
		JOIN report_stock rs ON sm.location_dest_id = rs.location_id
			AND sm.product_id = rs.product_id
			AND sm.serial_id = rs.lot_id
		WHERE sm.STATE = 'done'
			AND sm.serial_id IS NOT NULL
			AND rs.lot_id IS NOT NULL
		), serial_stock_move_without_origin_latest
	AS (
		SELECT location_id, product_id, lot_id, picking_id, origin, DATE
		FROM serial_stock_move_without_origin
		WHERE rn = 1
		), all_serial_stock_moves
	AS (
		SELECT *
		FROM serial_stock_moves_with_origin_latest
		
		UNION
		
		SELECT *
		FROM serial_stock_move_without_origin_latest
		), final_last_move_serial
	AS (
		SELECT DISTINCT ON (location_id, product_id, lot_id) 
			location_id, product_id, lot_id, picking_id, origin, DATE
		FROM all_serial_stock_moves
		ORDER BY location_id, product_id, lot_id, DATE DESC
		), getting_report_stock
	AS (
		SELECT a.*, coalesce(flms.picking_id, flmns.picking_id) AS picking_id
			, coalesce(flms.origin, flmns.origin) AS origin, coalesce(flms.
				DATE, flmns.DATE) AS DATE
		FROM report_stock a
		LEFT JOIN final_last_move_serial flms ON a.product_id = flms.
			product_id
			AND a.location_id = flms.location_id
			AND a.lot_id = flms.lot_id
		LEFT JOIN final_last_move_non_serial flmns ON a.product_id = flmns.
			product_id
			AND a.location_id = flmns.location_id
		), ready_for_insert
	AS (
		SELECT grs.location_id, grs.lot_id, grs.product_id, grs.category_id, 
			grs.origin, grs.picking_id, sp.picking_type_id, sp.
			transaction_type_id, grs.location_name, grs.location_function, 
			grs.category_name, grs.alterable, grs.numbered, grs.product_name
			, grs.product_challan, grs.primary_product_attribute, grs.
			secondary_product_attribute, grs.serial_number, grs.
			stock_in_hand, coalesce(sp.name, grs.origin) AS last_move_no, 
			coalesce(sp.DATE, grs.DATE) AS last_move_date, spt.name AS 
			picking_type, tt.name AS transaction_type
		FROM getting_report_stock grs
		LEFT JOIN stock_picking sp ON sp.id = grs.picking_id
		LEFT JOIN stock_picking_type spt ON spt.id = sp.picking_type_id
		LEFT JOIN transaction_type tt ON tt.id = sp.transaction_type_id
		)
	INSERT INTO report_location_stock (
		location_id, category_id, product_id, lot_id, location_name, 
		location_function, product_name, product_challan, 
		primary_product_attribute, secondary_product_attribute, alterable, 
		numbered, serial_number, category_name, stock_in_hand, last_move_no, 
		last_move_date, picking_type, transaction_type
		)
	SELECT location_id, category_id, product_id, lot_id, location_name, 
		location_function, product_name, product_challan, 
		primary_product_attribute, secondary_product_attribute, alterable, 
		numbered, serial_number, category_name, stock_in_hand, last_move_no, 
		last_move_date, picking_type, transaction_type
	FROM ready_for_insert;
END;$$

LANGUAGE plpgsql;