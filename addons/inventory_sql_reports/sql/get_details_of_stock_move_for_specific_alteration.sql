SELECT
    a.id,
    a.origin,
    a.product_uom_qty,
    a.picking_type_id,
    d.name AS picking_type,
    a.product_id,
    t.product_challan AS product,
    a.location_id,
    b.complete_name AS from_loc,
    a.location_dest_id,
    c.complete_name AS to_loc
FROM
    stock_move a
    JOIN stock_location b ON a.location_id = b.id
    JOIN stock_location c ON a.location_dest_id = c.id
    LEFT JOIN stock_picking_type d ON a.picking_type_id = d.id
    JOIN product_product p ON a.product_id = p.id
    JOIN product_template t ON p.product_tmpl_id = t.id
WHERE
    a.origin = 'ALT-014050'
ORDER BY
    a.id
