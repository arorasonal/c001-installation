WITH dupl AS (
    SELECT
        product_id,
        origin
    FROM
        stock_move
    WHERE
        origin IS NOT NULL
        AND picking_id IS NULL
    GROUP BY
        product_id,
        origin
    HAVING
        count(*) > 1
)
SELECT
    sm.id,
    sm.origin,
    sm.create_date,
    sm.product_uom_qty,
    sm.location_id,
    sm.state,
    sm.date,
    sm.product_id,
    sm.location_dest_id,
    sm.write_date
FROM
    stock_move sm
    JOIN dupl d ON sm.product_id = d.product_id
        AND sm.origin = d.origin
WHERE
    picking_id IS NULL
ORDER BY
    sm.id
