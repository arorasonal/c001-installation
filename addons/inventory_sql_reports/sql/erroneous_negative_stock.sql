WITH internal_loc_name
AS (
	SELECT stock_location.id,
		allow_negative_stock,
		"substring" (
			stock_location.complete_name::TEXT,
			"position" (
				stock_location.complete_name::TEXT,
				'/'::TEXT
				) + 1
			) AS location_name
	FROM stock_location
	WHERE usage = 'internal'
	),
out_move
AS (
	SELECT m.product_id,
		m.location_id AS loc_id,
		sum(0) AS in_qty,
		sum(product_qty) AS out_qty
	FROM stock_move m
	WHERE m.STATE LIKE 'done'
		AND m.location_id IN (
			SELECT id
			FROM internal_loc_name
			)
	GROUP BY m.product_id,
		m.location_id
	),
in_move
AS (
	SELECT m.product_id,
		m.location_dest_id AS loc_id,
		sum(product_qty) AS in_qty,
		sum(0) AS out_qty
	FROM stock_move m
	WHERE m.STATE LIKE 'done'
		AND m.location_dest_id IN (
			SELECT id
			FROM internal_loc_name
			)
	GROUP BY m.product_id,
		m.location_dest_id
	),
all_move
AS (
	SELECT a.product_id,
		a.loc_id,
		a.in_qty,
		a.out_qty
	FROM in_move a
	
	UNION
	
	SELECT b.product_id,
		b.loc_id,
		b.in_qty,
		b.out_qty
	FROM out_move b
	),
sum_move
AS (
	SELECT product_id,
		loc_id,
		sum(in_qty) AS in_qty,
		sum(out_qty) AS out_qty
	FROM all_move
	GROUP BY product_id,
		loc_id
	),
neg_stock
AS (
	SELECT a.product_id,
		a.loc_id,
		a.in_qty - a.out_qty AS stock_in_hand
	FROM sum_move a
	JOIN internal_loc_name l ON a.loc_id = l.id
	WHERE (a.in_qty - a.out_qty) < 0
		AND coalesce(l.allow_negative_stock, FALSE)
	),
item_stk_move
AS (
	SELECT ma.picking_id,
		ma.product_id,
		ma.location_id AS loc_id,
		sum(0) AS in_qty,
		sum(ma.product_qty) AS out_qty
	FROM stock_move ma
	JOIN neg_stock ns ON ma.product_id = ns.product_id
		AND ma.location_id = ns.loc_id
	WHERE ma.STATE LIKE 'done'
		AND ma.picking_id IS NOT NULL
	GROUP BY ma.picking_id,
		ma.product_id,
		ma.location_id
	
	UNION
	
	SELECT mb.picking_id,
		mb.product_id,
		mb.location_dest_id AS loc_id,
		sum(mb.product_qty) AS in_qty,
		sum(0) AS out_qty
	FROM stock_move mb
	JOIN neg_stock nt ON mb.product_id = nt.product_id
		AND mb.location_dest_id = nt.loc_id
	WHERE mb.STATE LIKE 'done'
		AND mb.picking_id IS NOT NULL
	GROUP BY mb.picking_id,
		mb.product_id,
		mb.location_dest_id
	),
item_alt_move
AS (
	SELECT ma.origin,
		ma.product_id,
		ma.location_id AS loc_id,
		sum(0) AS in_qty,
		sum(ma.product_qty) AS out_qty
	FROM stock_move ma
	JOIN neg_stock ns ON ma.product_id = ns.product_id
		AND ma.location_id = ns.loc_id
	WHERE ma.STATE LIKE 'done'
		AND ma.picking_id IS NULL
	GROUP BY ma.origin,
		ma.product_id,
		ma.location_id
	
	UNION
	
	SELECT mb.origin,
		mb.product_id,
		mb.location_dest_id AS loc_id,
		sum(mb.product_qty) AS in_qty,
		sum(0) AS out_qty
	FROM stock_move mb
	JOIN neg_stock nt ON mb.product_id = nt.product_id
		AND mb.location_dest_id = nt.loc_id
	WHERE mb.STATE LIKE 'done'
		AND mb.picking_id IS NULL
	GROUP BY mb.origin,
		mb.product_id,
		mb.location_dest_id
	),
item_all_move
AS (
	SELECT ism.loc_id,
		ism.product_id,
		ism.in_qty,
		ism.out_qty,
		spk.name,
		spk.write_date AS write_date,
		DATE (spk.date_done) AS move_date
	FROM item_stk_move ism
	JOIN stock_picking spk ON ism.picking_id = spk.id
	
	UNION
	
	SELECT iam.loc_id,
		iam.product_id,
		iam.in_qty,
		iam.out_qty,
		iam.origin AS name,
		coalesce(ialt.write_date, coalesce(cna.write_date, nia.write_date)) AS write_date,
		DATE (coalesce(ialt.validate_date, coalesce(cna.validate_date, nia.validate_date))) 
		AS move_date
	FROM item_alt_move iam
	LEFT JOIN item_alteration ialt ON ialt.name = iam.origin
	LEFT JOIN conversion_non_alterable cna ON cna.name = iam.origin
	LEFT JOIN non_item_alteration nia ON nia.name = iam.origin
	)
SELECT g.loc_id,
	l.location_name AS LOCATION,
	g.product_id,
    pc.name AS category_name,
	t.product_challan,
	t.name AS product_name,
	g.name AS document,
	g.move_date,
	g.write_date,
	g.in_qty,
	g.out_qty,
	sum(g.in_qty - g.out_qty) OVER (
		PARTITION BY g.loc_id,
		g.product_id ORDER BY g.move_date
		) AS bal_qty
FROM item_all_move g
JOIN product_product p ON g.product_id = p.id
JOIN product_template t ON p.product_tmpl_id = t.id
JOIN internal_loc_name l ON g.loc_id = l.id
JOIN product_category pc ON t.categ_id = pc.id
ORDER BY g.loc_id,
	g.product_id,
	g.move_date