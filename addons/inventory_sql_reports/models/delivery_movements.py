# -*- coding: utf-8 -*-
# Copyright 2019 Ravi Krishnan
# See LICENSE file for full copyright and licensing details.

from odoo import api, fields, models, tools

class DeliveryMovements(models.Model):
    _name = "delivery.movement.serial.non.internal"
    _auto = False


    doc_type = fields.Char(string='Doc Type')
    doc_no = fields.Char(string='Doc No')
    ri_status = fields.Char(string='RI Status')
    transaction_type = fields.Char(string='Transaction Type')
    origin = fields.Char(string='Origin')
    delivery_date = fields.Date(string='Delivery Date')
    customer_vendor_name = fields.Char(string='Customer / Vendor')
    customer_vendor_parent_name = fields.Char(string='Customer / Vendor - Parent')
    customer_vendor_city = fields.Char(string='[Customer / Vendor] City')
    customer_vendor_parent_city = fields.Char(string='[Customer / Vendor] - Parent City')
    service_number = fields.Char(string='Install / Deinstall Doc #')
    from_location = fields.Char(string='From')
    to_location = fields.Char(string='To') 
    category_name = fields.Char(string='Category')
    product = fields.Char(string='Product')
    product_barcode = fields.Char(string='Product Barcode')
    product_qty = fields.Float(string='Product Qty')
    so_no = fields.Char(string='SO #')
    lot_no = fields.Char(string='Lot Serial #')
    lot_qty = fields.Float(string='Lot Qty')
    lot_barcode = fields.Char('Lot Barcode')

    @api.model_cr
    def init(self):
        print "Connected"
        tools.drop_view_if_exists(self._cr, 'delivery_movement_serial_non_internal')
        self._cr.execute("""
            CREATE OR REPLACE VIEW delivery_movement_serial_non_internal AS (
                WITH location
                AS (
                    SELECT stock_location.id,
                        "substring" (
                            stock_location.complete_name::TEXT,
                            "position" (
                                stock_location.complete_name::TEXT,
                                '/'::TEXT
                                ) + 1
                            ) AS location_name
                    FROM stock_location
                    ),
                delivery_main
                AS (
                    SELECT stock_picking.id,
                        stock_picking.name AS doc_no,
                        stock_picking.origin,
                        DATE (stock_picking.date_done) AS delivery_date,
                        stock_picking.location_id,
                        stock_picking.picking_type_id,
                        stock_picking.order_id,
                        stock_picking.partner_id,
                        "stock_picking".note,
                        stock_picking.transaction_type_id,
                        stock_picking.location_dest_id
                    FROM stock_picking
                    WHERE stock_picking.STATE::TEXT = 'done'::TEXT
                        AND stock_picking.date_done > ('now'::TEXT::DATE - '45 days'::interval)
                    ),
                partner_is_not_company_with_no_parent
                AS (
                    SELECT a.id,
                        a.parent_id,
                        a.is_company
                    FROM res_partner a
                    WHERE NOT a.is_company
                        AND a.parent_id IS NULL
                    ),
                partner_is_a_company
                AS (
                    SELECT a.id,
                        a.parent_id,
                        a.is_company
                    FROM res_partner a
                    WHERE a.is_company
                    ),
                partner_is_not_company_but_has_parent
                AS (
                    SELECT a.id,
                        a.parent_id,
                        a.is_company
                    FROM res_partner a
                    LEFT JOIN partner_is_a_company b ON a.parent_id = b.id
                    WHERE NOT a.is_company
                        AND a.parent_id IS NOT NULL
                    ),
                partner_all_companies
                AS (
                    SELECT a.id,
                        a.parent_id,
                        a.is_company
                    FROM partner_is_a_company a
                    
                    UNION
                    
                    SELECT b.id,
                        b.parent_id,
                        b.is_company
                    FROM partner_is_not_company_but_has_parent b
                    
                    UNION
                    
                    SELECT c.id,
                        c.parent_id,
                        c.is_company
                    FROM partner_is_not_company_with_no_parent c
                    ),
                delivery
                AS (
                    SELECT dm.id,
                        dm.doc_no,
                        dm.origin,
                        dm.delivery_date,
                        dm.location_id,
                        dm.picking_type_id,
                        dm.order_id,
                        dm.partner_id,
                        dm.transaction_type_id,
                        dm.location_dest_id,
                        dm.note,
                        re.display_name AS customer_vendor_parent_name,
                        re.city AS customer_vendor_parent_city,
                        rc.display_name AS customer_vendor_name,
                        rc.city AS customer_vendor_city,
                        sp.name AS doc_type,
                        tt.name AS transaction_type
                    FROM delivery_main dm
                    JOIN stock_picking_type sp ON dm.picking_type_id = sp.id
                    LEFT JOIN partner_all_companies pac ON pac.id = dm.partner_id
                    LEFT JOIN res_partner re ON re.id = pac.parent_id
                    LEFT JOIN res_partner rc ON rc.id = dm.partner_id
                    LEFT JOIN transaction_type tt ON tt.id = dm.transaction_type_id
                    WHERE sp.code::TEXT <> 'internal'::TEXT
                    ),
                movement
                AS (
                    SELECT m.id,
                        m.picking_id,
                        t1.name AS category_name,
                        m.product_id,
                        t.product_challan,
                        p.barcode AS product_barcode,
                        m.product_qty,
                        s.lot_id,
                        s.qty AS lot_qty,
                        sl.name AS lot_no,
                        sl.actual_barcode AS lot_barcode
                    FROM stock_move m
                    JOIN product_product p ON m.product_id = p.id
                    JOIN product_template t ON p.product_tmpl_id = t.id
                    LEFT JOIN stock_quant_move_rel q ON q.move_id = m.id
                    LEFT JOIN stock_quant s ON q.quant_id = s.id
                    LEFT JOIN stock_production_lot sl ON s.lot_id = sl.id
                    LEFT JOIN product_category t1 ON t1.id = t.categ_id
                    WHERE (
                            m.picking_id IN (
                                SELECT DISTINCT delivery.id
                                FROM delivery
                                )
                            )
                    )
                SELECT row_number() OVER () AS id,
                    a.doc_type,
                    a.doc_no,
                    CASE 
                        WHEN "substring" (
                                lb.location_name,
                                "position" (
                                    lb.location_name,
                                    '/'::TEXT
                                    ) + 1
                                ) = 'Purchase Receive'::TEXT
                            THEN 'INTERNAL'::TEXT
                        WHEN "substring" (
                                "substring" (
                                    a.doc_no::TEXT,
                                    "position" (
                                        a.doc_no::TEXT,
                                        '-'::TEXT
                                        ) + 1
                                    ),
                                1,
                                3
                                ) = 'IN-'::TEXT
                            THEN 'RI'::TEXT
                        WHEN "substring" (
                                "substring" (
                                    a.doc_no::TEXT,
                                    "position" (
                                        a.doc_no::TEXT,
                                        '-'::TEXT
                                        ) + 1
                                    ),
                                1,
                                3
                                ) = 'OUT'::TEXT
                            THEN 'RO'::TEXT
                        ELSE 'INTERNAL'::TEXT
                        END AS ri_status,
                    a.transaction_type,
                    a.origin,
                    a.delivery_date,
                    a.note AS service_number,
                    a.customer_vendor_name,
                    a.customer_vendor_city,
                    a.customer_vendor_parent_name,
                    a.customer_vendor_parent_city,
                    la.location_name AS from_location,
                    lb.location_name AS to_location,
                    mt.category_name,
                    mt.product_challan AS product,
                    mt.product_barcode,
                    so.name AS so_no,
                    mt.product_qty,
                    mt.lot_no,
                    mt.lot_qty,
                    mt.lot_barcode
                FROM delivery a
                LEFT JOIN location la ON a.location_id = la.id
                LEFT JOIN location lb ON a.location_dest_id = lb.id
                JOIN movement mt ON a.id = mt.picking_id
                LEFT JOIN sale_order so ON a.order_id = so.id
                ORDER BY a.delivery_date DESC,
                    a.picking_type_id,
                    a.doc_no DESC
            )""")
            