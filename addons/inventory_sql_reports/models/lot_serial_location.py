# -*- coding: utf-8 -*-
# Copyright 2019 Ravi Krishnan
# See LICENSE file for full copyright and licensing details.

# -- first try

# WITH lot_info
# AS (
# 	SELECT a.product_id,
# 		a.id AS lot_id,
# 		e.DATE AS picking_date,
# 		b.location_id,
# 		e.partner_id
# 	FROM stock_production_lot a
# 	JOIN stock_quant b ON a.id = b.lot_id
# 	JOIN stock_quant_move_rel c ON c.quant_id = b.id
# 	JOIN stock_move d ON d.id = c.move_id
# 	JOIN stock_picking e ON e.id = d.picking_id
# 	WHERE b.location_id <> 18
# 		AND d.picking_id IS NOT NULL
# 		AND b.location_id = d.location_dest_id
# 		AND a.product_id = d.product_id
# 	),
# distinct_lots
# AS (
# 	SELECT DISTINCT ON (lot_id) *
# 	FROM lot_info
# 	ORDER BY lot_id,
# 		picking_date DESC
# 	)
# SELECT row_number() OVER () AS id,
# 	a.lot_id,
# 	a.product_id,
# 	a.location_id,
# 	a.partner_id,
# 	pt.categ_id,
# 	pt.product_challan
# FROM distinct_lots a
# JOIN product_product pp ON pp.id = a.product_id
# JOIN product_template pt ON pp.product_tmpl_id = pt.id

# -- second try. was working. but error.

# WITH lot_info
# AS (
#     SELECT a.product_id,
#         a.id AS lot_id,
#         d.DATE AS move_date,
#         e.DATE AS picking_date,
#         b.location_id,
#         e.partner_id
#     FROM stock_production_lot a
#     JOIN stock_quant b ON a.id = b.lot_id
#     JOIN stock_quant_move_rel c ON c.quant_id = b.id
#     JOIN stock_move d ON d.id = c.move_id
#     LEFT JOIN stock_picking e ON e.id = d.picking_id
#     WHERE b.location_id <> 18
#         AND d.STATE = 'done'
#         AND b.location_id = d.location_dest_id
#         AND a.product_id = d.product_id
#     )
# SELECT DISTINCT ON (lot_id) row_number() OVER () AS id,
#     lot_id,
#     product_id,
#     location_id,
#     partner_id
# FROM lot_info
# ORDER BY lot_id,
#     picking_date DESC nulls last

# -- after rental stocks kept in serparate locations, this code was wrong --

# SELECT DISTINCT ON (a.name) row_number() OVER () AS id,
#     a.product_id,
#     a.id AS lot_id,
#     a.name AS serial_number,
#     d.location_dest_id AS location_id,
#     e.partner_id
# FROM stock_production_lot a
# JOIN stock_move d ON a.id = d.serial_id
# LEFT JOIN stock_picking e ON e.id = d.picking_id
# WHERE d.location_dest_id <> 18
#     AND d.STATE::TEXT = 'done'::TEXT
#     AND a.product_id = d.product_id
# ORDER BY a.name,
#     e.DATE DESC NULLS LAST

from odoo import api, fields, models, tools

class LotSerialLocation(models.Model):
    _name = "lot.serial.location"
    _auto = False
 
    lot_id = fields.Many2one(comodel_name='stock.production.lot', string='Serial #')
    product_id = fields.Many2one(comodel_name='product.product', string='Product')
    location_id = fields.Many2one(comodel_name='stock.location', string='Location')
    categ_id = fields.Many2one(comodel_name='product.category', string='Category')


    @api.model_cr
    def init(self):
        print("Connected ... 1") 
        tools.drop_view_if_exists(self._cr, 'lot_serial_location')
        self._cr.execute("""
            CREATE OR REPLACE VIEW lot_serial_location AS (
                SELECT row_number() OVER () AS id,
                    sq.lot_id,
                    sq.location_id,
                    sq.product_id,
                    pt.categ_id
                FROM stock_quant sq
                JOIN stock_production_lot spl ON sq.lot_id = spl.id
                    AND sq.product_id = spl.product_id
                JOIN stock_location sl ON sl.id = sq.location_id
                JOIN product_product pp ON pp.id = sq.product_id
                JOIN product_template pt ON pt.id = pp.product_tmpl_id
                JOIN product_category pc ON pc.id = pt.categ_id
                WHERE sq.lot_id IS NOT NULL
                    AND sq.location_id <> 18
            )""")



