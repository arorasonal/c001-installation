# -*- coding: utf-8 -*-
# Copyright 2019 Ravi Krishnan
# See LICENSE file for full copyright and licensing details.

from odoo import api, fields, models, tools

class DeliveryLotSerialPartner(models.Model):
    _name = "delivery.lot.serial.partner"
    _auto = False


    origin = fields.Char(string='Origin')
    delivery_date = fields.Date(string='Delivery Date')
    delivery_note_no = fields.Char(string='Delivery #')
    location_id = fields.Many2one(comodel_name='stock.location', string='From')
    location_dest_id = fields.Many2one(comodel_name='stock.location', string='To')
    picking_type_id = fields.Many2one(comodel_name='stock.picking.type', string='Picking Type')
    partner_id = fields.Many2one(comodel_name='res.partner', string='Partner')
    transaction_type_id = fields.Many2one(comodel_name='transaction.type', string='Partner')
    order_id = fields.Many2one(comodel_name='sale.order', string='SO #')
    product_id = fields.Many2one(comodel_name='product.product', string='Product')
    product_qty = fields.fields.Float(string='Product Qty')
    lot_id = fields.Many2one(comodel_name='stock.production.lot', string='Lot Serial #')

    @api.model_cr
    def init(self):
        print "Connected"
        tools.drop_view_if_exists(self._cr, 'delivery_lot_serial_partner')
        self._cr.execute("""
            CREATE OR REPLACE VIEW delivery_lot_serial_partner AS (
                SELECT
                    row_number() OVER () AS id,
                    a.origin,
                    date(a.date_done) AS delivery_date,
                    a.location_id,
                    a.location_dest_id,
                    a.picking_type_id,
                    a.partner_id,
                    a.name AS delivery_note_no,
                    a.transaction_type_id,
                    a.order_id,
                    c.product_id,
                    c.product_qty,
                    d.lot_id
                FROM
                    stock_picking a
                    JOIN stock_picking_type b ON a.picking_type_id = b.id
                    JOIN stock_move c ON c.picking_id = a.id
                    LEFT JOIN stock_quant_move_rel ON c.id = stock_quant_move_rel.move_id
                    JOIN stock_quant d ON d.id = stock_quant_move_rel.quant_id
                WHERE
                    a.state = 'done'
                    AND b.code <> 'internal'
                    AND a.date_done > ('now'::text::date - '31 days'::interval)
                ORDER BY
                    a.date_done DESC,
                    a.name DESC
            )""")
            