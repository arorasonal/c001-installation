# -*- coding: utf-8 -*-
# Copyright 2019 Ravi Krishnan
# See LICENSE file for full copyright and licensing details.

from . import lot_serial_location
from . import delivery_movements
# -- from . import internal_location_stock -- removed - 2020-08-16
# 2019-12-03 -- next 1 line added
from . import add_remove_alterations
# 2020-08-16 -- next 2 line added
# -- from . import rental_location_stock -- removed - 2020-10-14
# -- from . import non_rental_location_stock -- removed - 2020-10-14
# 2020-09-02 -- next 1 line added
from . import report_location_stock
