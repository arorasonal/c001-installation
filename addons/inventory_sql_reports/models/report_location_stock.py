import logging

from odoo import api, fields, models
from odoo import tools, _
import time
import math
from time import gmtime, strftime
import odoo.addons.decimal_precision as dp
from datetime import datetime,timedelta,date
from odoo.tools import DEFAULT_SERVER_DATE_FORMAT,DEFAULT_SERVER_DATETIME_FORMAT
from odoo.exceptions import UserError, ValidationError
from odoo.modules.module import get_module_resource

class report_location_stock(models.Model):
    _name="report.location.stock"

    location_id = fields.Many2one(comodel_name='stock.location', string='Location', index=True)
    category_id = fields.Many2one(comodel_name='product.category', string='Category', index=True)
    product_id = fields.Many2one(comodel_name='product.product', string='Product', index=True)
    lot_id = fields.Many2one(comodel_name='stock.production.lot', string='Serial #', index=True)

    stock_in_hand = fields.Float(string='Product Qty')
    location_name = fields.Char(string='Location Name', index=True)
    category_name = fields.Char(string='Category Name', index=True)
    product_name = fields.Char(string='Product Name')
    product_challan = fields.Char('Print on Challan')
    serial_number = fields.Char(string='Serial Number', index=True)
    location_function = fields.Char(string='Location Function', index=True)
    primary_product_attribute = fields.Char(string='Primary Product Attribute')
    secondary_product_attribute = fields.Char(string='Secondary Product Attribute')
    alterable = fields.Char(string='Alterable')
    numbered = fields.Char(string='Numbered')

    last_move_no = fields.Char(string='Last Move Document #')
    last_move_date = fields.Datetime(string='Last Move Date')
    picking_type = fields.Char(string='Picking Type')
    transaction_type = fields.Char(string='Transaction Type')
    # comp_ids = fields.Text(string='Component Ids')
    # comp_values = fields.Text(string='Component Values')


    @api.model_cr
    def cron_update_report_location_stock(self):
        self._cr.execute("select report_location_stock_with_last_move() ;")
        # self._cr.execute("TRUNCATE TABLE report_location_stock RESTART IDENTITY;")
        # self._cr.execute("""
        #         WITH non_serial_product
        #         AS (
        #             SELECT a.id
        #             FROM product_product a
        #             JOIN product_template b
        #                 ON a.product_tmpl_id = b.id
        #             WHERE b.tracking = 'none'
        #             ),
        #         rental_location
        #         AS (
        #             SELECT stock_location.id
        #             FROM stock_location
        #             WHERE usage = 'internal'
        #             ),
        #         out_move
        #         AS (
        #             SELECT m.product_id,
        #                 m.location_id AS loc_id,
        #                 sum(0) AS in_qty,
        #                 sum(product_qty) AS out_qty
        #             FROM stock_move m
        #             JOIN non_serial_product nsp
        #                 ON nsp.id = m.product_id
        #             JOIN rental_location rl
        #                 ON rl.id = m.location_id
        #             WHERE m.STATE LIKE 'done'
        #             GROUP BY m.product_id,
        #                 m.location_id
        #             ),
        #         in_move
        #         AS (
        #             SELECT m.product_id,
        #                 m.location_dest_id AS loc_id,
        #                 sum(product_qty) AS in_qty,
        #                 sum(0) AS out_qty
        #             FROM stock_move m
        #             JOIN non_serial_product nsp
        #                 ON nsp.id = m.product_id
        #             JOIN rental_location rl
        #                 ON rl.id = m.location_dest_id
        #             WHERE m.STATE LIKE 'done'
        #             GROUP BY m.product_id,
        #                 m.location_dest_id
        #             ),
        #         all_move
        #         AS (
        #             SELECT a.product_id,
        #                 a.loc_id,
        #                 a.in_qty,
        #                 a.out_qty
        #             FROM in_move a
                    
        #             UNION
                    
        #             SELECT b.product_id,
        #                 b.loc_id,
        #                 b.in_qty,
        #                 b.out_qty
        #             FROM out_move b
        #             ),
        #         sum_move
        #         AS (
        #             SELECT product_id,
        #                 loc_id,
        #                 sum(in_qty) AS in_qty,
        #                 sum(out_qty) AS out_qty
        #             FROM all_move
        #             GROUP BY product_id,
        #                 loc_id
        #             ),
        #         all_non_serial_move
        #         AS (
        #             SELECT NULL::INTEGER AS lot_id,
        #                 loc_id AS location_id,
        #                 product_id,
        #                 in_qty - out_qty AS bal_qty
        #             FROM sum_move
        #             WHERE in_qty <> out_qty
        #             ),
        #         all_serial_move
        #         AS (
        #             SELECT sq.lot_id,
        #                 sq.location_id,
        #                 sq.product_id,
        #                 1 AS bal_qty
        #             FROM stock_quant sq
        #             JOIN stock_production_lot spl
        #                 ON sq.lot_id = spl.id
        #                     AND sq.product_id = spl.product_id
        #             JOIN rental_location sl
        #                 ON sl.id = sq.location_id
        #             ),
        #         final_stock
        #         AS (
        #             SELECT lot_id,
        #                 location_id,
        #                 product_id,
        #                 bal_qty
        #             FROM all_serial_move
                    
        #             UNION ALL
                    
        #             SELECT lot_id,
        #                 location_id,
        #                 product_id,
        #                 bal_qty
        #             FROM all_non_serial_move
        #             ),
        #         alt_group
        #         AS (
        #             SELECT DISTINCT rps.product_id,
        #                 pc.id as category_id,
        #                 ctp.name AS category_pg_id,
        #                 cts.name AS category_sc_id
        #             FROM final_stock rps
        #             JOIN product_product ppa
        #                 ON rps.product_id = ppa.id 
        #             JOIN product_template pta 
        #                 ON pta.id = ppa.product_tmpl_id	
        #             JOIN product_category pc
        #                 ON pta.categ_id = pc.id
        #             LEFT JOIN category_template ctp
        #                 ON pc.id = ctp.category_id1
        #                     AND pc.primary_alt_grouping_id = ctp.id
        #             LEFT JOIN category_template cts
        #                 ON pc.id = cts.category_id1
        #                     AND pc.secondary_alt_grouping_id = cts.id
        #             WHERE coalesce(pc.can_altered, FALSE) = TRUE
        #             ),
        #         alt_group_short
        #         AS (
        #             SELECT x.product_id,
        #                 x.category_id,
        #                 x.category_pg_id,
        #                 x.category_sc_id,
        #                 pt.add_product_id,
        #                 clp.short_name,
        #                 clp.product_id AS product_pg_id,
        #                 cls.product_id AS product_sc_id
        #             FROM alt_group x
        #             JOIN product_product pp
        #                 ON pp.id = x.product_id
        #             JOIN product_template pt
        #                 ON pp.product_tmpl_id = pt.id
        #             JOIN wiz_add_product wap
        #                 ON pt.add_product_id = wap.id
        #             LEFT JOIN category_template_line clp
        #                 ON clp.wiz_add_product_id = wap.id
        #                     AND clp.category_id = x.category_pg_id
        #             LEFT JOIN category_template_line cls
        #                 ON cls.wiz_add_product_id = wap.id
        #                     AND cls.category_id = x.category_sc_id
        #             ),
        #         alt_multi_group_name
        #         AS (
        #             SELECT y.product_id,
        #                 y.product_pg_id,
        #                 y.product_sc_id,
        #                 pg_pt.short_name_product AS primary_group,
        #                 sc_pt.short_name_product AS secondary_group
        #             FROM alt_group_short y
        #             LEFT JOIN product_product pg_pp
        #                 ON pg_pp.id = y.product_pg_id
        #             LEFT JOIN product_template pg_pt
        #                 ON pg_pp.product_tmpl_id = pg_pt.id
        #             LEFT JOIN product_product sc_pp
        #                 ON sc_pp.id = y.product_sc_id
        #             LEFT JOIN product_template sc_pt
        #                 ON sc_pp.product_tmpl_id = sc_pt.id
        #             ),
        #         alt_group_name
        #         AS (                    
        #             SELECT product_id, 
        #                 string_agg(distinct primary_group,'') as primary_group,
        #                 string_agg(distinct secondary_group,',') as secondary_group 
        #             FROM alt_multi_group_name
        #             GROUP by product_id
        #             ),
        #         non_alt_group 
        #         AS (
        #             SELECT DISTINCT
        #                     rps.product_id,
        #                     pc.id AS category_id,
        #                     ctp.name AS category_pg_name,
        #                     cts.name AS category_sc_name
        #             FROM
        #                     final_stock rps
        #                     JOIN product_product ppa ON rps.product_id = ppa.id
        #                     JOIN product_template pta ON pta.id = ppa.product_tmpl_id
        #                     JOIN product_category pc ON pta.categ_id = pc.id
        #                     LEFT JOIN specification_line ctp ON pc.id = ctp.category_id1
        #                         AND pc.primary_na_grouping_id = ctp.id
        #                     LEFT JOIN specification_line cts ON pc.id = cts.category_id1
        #                         AND pc.secondary_na_grouping_id = cts.id
        #             WHERE
        #                     coalesce(pc.can_altered, FALSE) = FALSE
        #             ),
        #         non_alt_group_name 
        #         AS (
        #             SELECT
        #                 x.product_id,
        #                 x.category_id,
        #                 x.category_pg_name,
        #                 x.category_sc_name,
        #                 vsvp.name AS primary_group,
        #                 vsvs.name AS secondary_group
        #             FROM
        #                 non_alt_group x
        #                 JOIN product_product pp ON pp.id = x.product_id
        #                 JOIN product_template pt ON pp.product_tmpl_id = pt.id
        #                 JOIN wiz_add_product wap ON pp.id = wap.product_id
        #                 LEFT JOIN add_product_line clp ON clp.wiz_add_product_id = wap.id
        #                     AND clp.specification = x.category_pg_name
        #                 LEFT JOIN add_product_line cls ON cls.wiz_add_product_id = wap.id
        #                     AND cls.specification = x.category_sc_name
        #                 LEFT JOIN valid_specification_value vsvp ON clp.valid_value = vsvp.id
        #                 LEFT JOIN valid_specification_value vsvs ON cls.valid_value = vsvs.id
        #             ),    
        #         report_stock
        #         AS (
        #             SELECT row_number() OVER () AS id,
        #                 a.lot_id,
        #                 a.location_id,
        #                 t.categ_id AS category_id,
        #                 a.product_id,
        #                 "substring" (
        #                     l.complete_name::TEXT,
        #                     "position" (
        #                         l.complete_name::TEXT,
        #                         '/'::TEXT
        #                         ) + 1
        #                     ) AS location_name,
        #                 coalesce(slt.name, '') AS location_function,
        #                 pc.name AS category_name,
        #                 CASE 
        #                     WHEN coalesce(pc.can_altered, FALSE)
		# 		                THEN 'ALTERABLE'
		# 	                ELSE 'NON-ALTERABLE'
		# 	                END AS alterable,
        #                 CASE 
        #                     WHEN coalesce(pc.numbered, FALSE)
		# 		                THEN 'NUMBERED'
		# 	                ELSE 'NON-NUMBERED'
		# 	                END AS numbered,
        #                 t.name AS product_name,
        #                 t.product_challan,
        #                 coalesce(agn.primary_group, coalesce(nagn.primary_group,'')) AS primary_product_attribute,
        #                 coalesce(agn.secondary_group, coalesce(nagn.secondary_group,'')) AS secondary_product_attribute,
        #                 spl.name AS serial_number,
        #                 a.bal_qty AS stock_in_hand
        #             FROM final_stock a
        #             JOIN product_product p
        #                 ON a.product_id = p.id
        #             JOIN product_template t
        #                 ON p.product_tmpl_id = t.id
        #             JOIN stock_location l
        #                 ON a.location_id = l.id
        #             LEFT JOIN stock_location_function_type slt
        #                 ON l.location_function_type_id = slt.id
        #             JOIN product_category pc
        #                 ON t.categ_id = pc.id
        #             LEFT JOIN stock_production_lot spl
        #                 ON a.lot_id = spl.id
        #             LEFT JOIN alt_group_name agn
        #                 ON a.product_id = agn.product_id
        #             LEFT JOIN non_alt_group_name nagn
        #                 ON a.product_id = nagn.product_id
        #             WHERE
        #                 coalesce(p.active, FALSE) = TRUE    
        #             )
        #         INSERT INTO report_location_stock (
        #             location_id,
        #             category_id,
        #             product_id,
        #             lot_id,
        #             location_name,
        #             location_function,
        #             product_name,
        #             product_challan,
        #             primary_product_attribute,
        #             secondary_product_attribute,
        #             alterable,
        #             numbered,
        #             serial_number,
        #             category_name,
        #             stock_in_hand
        #             )
        #         SELECT  location_id,
        #             category_id,
        #             product_id,
        #             lot_id,
        #             location_name,
        #             location_function,
        #             product_name,
        #             product_challan,
        #             primary_product_attribute,
        #             secondary_product_attribute,
        #             alterable,
        #             numbered,
        #             serial_number,
        #             category_name,
        #             stock_in_hand
        #         FROM report_stock;
        #     """)
        return True

