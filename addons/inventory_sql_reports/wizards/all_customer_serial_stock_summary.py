# -*- coding: utf-8 -*-

from odoo import api, exceptions, fields, models
from odoo.exceptions import UserError, ValidationError
from datetime import datetime,timedelta,date
from odoo import tools, _

class AllCustomerSerialStockSummaryWizard(models.TransientModel) :
    _name = 'all.customer.serial.stock.summary'
    _description = 'All Customer Serial Stock Summary Wizard'

    as_on_date = fields.Date('As On',default=lambda *a: datetime.now(),required=True)

    @api.multi
    def button_get(self):
        self.ensure_one()
        summary_date = self.as_on_date

        # sql_qry = "select * from jasper.cjpl_js_party_serial_stock_summary where partner_id  = " + str(partner)

        sql_qry = """
                    SELECT
                		r_id as id,
                        r_customer_name as customer_name,
                        r_movement_name as movement_name,
                        r_transaction_type as transaction_type,
                        r_from_location as from_location,
                        r_to_location as to_location,
                        r_serial_number as serial_number,
                        r_alpha_no as alpha_no,
                        r_category_name as category_name,
                        r_product_name as product_name,
                        r_product_challan as product_challan,
                        r_doc_no as doc_no,
                        r_move_date as move_date,
                        r_so_no as so_no,
                		r_balance_qty as balance_qty
                    FROM
                        location_all_party_serial_stock_summary (
                """
        sql_qry = sql_qry + "date('" + summary_date + "'))"

        sql_qry = 'CREATE OR REPLACE VIEW all_customer_serial_stock_summary_report AS ( ' + sql_qry + ' )'

        tools.drop_view_if_exists(self._cr, 'all_customer_serial_stock_summary_report')
        # tools.drop_view_if_exists(cr, self._table)

        self.env.cr.execute(sql_qry)
        # result = self.env.cr.fetchall()


        tree_view_id = self.env.ref('inventory_sql_reports.action_all_customer_serial_stock_summary_report_tree').id
        return {
            'name': 'All Customer Serial Stock Summary',
            'view_type': 'form',
            'view_mode': 'tree',
            'views': [[tree_view_id, 'tree']],
            'res_model': 'all.customer.serial.stock.summary.report',
            'type': 'ir.actions.act_window',
            'target': 'current',
        }
      

class AllCustomerSerialStockSummaryReport(models.Model):
    _name = "all.customer.serial.stock.summary.report"
    _description = "All Customer Serial Stock Summary Report"
    _auto = False
    _order = "customer_name"

    customer_name = fields.Char(string='Company')
    serial_number = fields.Char(string='Serial Number')
    alpha_no = fields.Char(string='Alpha Serial')
    movement_name = fields.Char(string='Movement Name')
    from_location = fields.Char(string='From')
    to_location = fields.Char(string='To')
    transaction_type = fields.Char(string='Transaction Type')
    category_name = fields.Char(string='Category')
    product_name = fields.Char(string='Product')
    product_challan = fields.Char(string='Product - Challan')
    doc_no = fields.Char(string='Delivery Document')
    move_date = fields.Date(string='Date')
    so_no = fields.Char(string='SO #')
    balance_qty = fields.Float(string='Quantity')



