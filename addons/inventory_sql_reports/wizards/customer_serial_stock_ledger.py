# -*- coding: utf-8 -*-

from odoo import api, exceptions, fields, models
from odoo.exceptions import UserError, ValidationError
from datetime import datetime,timedelta,date
from odoo import tools, _

class CustomerSerialStockLedger(models.TransientModel) :
    _name = 'customer.serial.stock.ledger'
    _description = 'Customer Serial Stock Ledger'

    partner_id = fields.Many2one('res.partner','Partner',required=True)
    start_date = fields.Date('From',default=lambda *a: datetime.now(),required=True)
    end_date = fields.Date('To',default=lambda *a: datetime.now(),required=True)

    @api.multi
    def button_get(self):
        self.ensure_one()
        for each in self.partner_id:
            partner = each.id
        start_date = self.start_date
        end_date = self.end_date

        sql_qry = """
                    SELECT
                        r_id AS id,
                        r_customer_name AS customer_name,
                        r_serial_number AS serial_number,
                        r_category_name AS category_name,
                        r_product_name AS product_name,
                        r_product_challan AS product_challan,
                        r_open_qty AS open_qty,
                        r_in_qty AS in_qty,
                        r_out_qty AS out_qty,
                        r_close_qty AS close_qty,
                        r_in_desc AS in_desc,
                        r_out_desc AS out_desc
                    FROM
                        location_party_serial_stock_ledger (
                """
        sql_qry = sql_qry + str(partner) + "," + "date('" + start_date + "')," + "date('" + end_date + "'))"

        sql_qry = 'CREATE OR REPLACE VIEW customer_serial_stock_ledger_report AS ( ' + sql_qry + ' )'

        tools.drop_view_if_exists(self._cr, 'customer_serial_stock_ledger_report')
        # tools.drop_view_if_exists(cr, self._table)

        self.env.cr.execute(sql_qry)
        # result = self.env.cr.fetchall()

        tree_view_id = self.env.ref('inventory_sql_reports.action_customer_serial_stock_ledger_report_tree').id
        return {
            'name': 'Customer Serial Stock Ledger',
            'view_type': 'form',
            'view_mode': 'tree',
            'views': [[tree_view_id, 'tree']],
            'res_model': 'customer.serial.stock.ledger.report',
            'type': 'ir.actions.act_window',
            'target': 'current',
        }


        # raise UserError(_(sql_qry))

        # self.env.cr.execute(sql_qry)
        # result = self.env.cr.fetchall()

        # raise UserError(_('Button is working'))


class CustomerSerialStockLedgerReport(models.Model):
    _name = "customer.serial.stock.ledger.report"
    _description = "Customer Serial Stock Ledger Report"
    _auto = False

    customer_name = fields.Char(string='Customer')
    serial_number = fields.Char(string='Serial Number')
    category_name = fields.Char(string='Category')
    product_name = fields.Char(string='Product')
    product_challan = fields.Char(string='Product - Challan')
    open_qty = fields.Integer(string='Opening Stock')
    in_qty = fields.Integer(string='Received')
    out_qty = fields.Integer(string='Returned')
    close_qty = fields.Integer(string='Closing Stock')
    in_desc = fields.Char(string='Incoming Documents')
    out_desc = fields.Char(string='Outgoing Documents')
