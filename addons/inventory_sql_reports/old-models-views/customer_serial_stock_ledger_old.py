# -*- coding: utf-8 -*-

from odoo import api, exceptions, fields, models
from odoo.exceptions import UserError, ValidationError
from datetime import datetime,timedelta,date
from odoo import tools, _

class CustomerSerialStockLedger(models.TransientModel) :
    _name = 'customer.serial.stock.ledger'
    _description = 'Customer Serial Stock Ledger'

    partner_id = fields.Many2one('res.partner','Partner',required=True)
    start_date = fields.Date('From',default=lambda *a: datetime.now(),required=True)
    end_date = fields.Date('To',default=lambda *a: datetime.now(),required=True)

    @api.multi
    def button_get(self):
        self.ensure_one()
        for each in self.partner_id:
            partner = each.id
        start_date = self.start_date
        end_date = self.end_date

        sql_qry = """
                    WITH move_lines AS (
                    SELECT 
                        sp.name AS doc_no,
                        date(sp.date_done) AS move_date,
                        sp.partner_id,
                        COALESCE(rp.parent_id, rp.id) AS parent_id,
                        sm.product_id,
                        sm.product_qty,
                            CASE
                                WHEN sla.name::text = 'Customers'::text THEN '-1'::integer::numeric * sm.product_qty
                                WHEN sla.name::text = 'Rental Out Stock'::text THEN '-1'::integer::numeric * sm.product_qty
                                WHEN sla.name::text = 'Repair & Warrnty Replacement'::text THEN '-1'::integer::numeric * sm.product_qty
                                WHEN slb.name::text = 'Customers'::text THEN 1::numeric * sm.product_qty
                                WHEN slb.name::text = 'Rental Out Stock'::text THEN 1::numeric * sm.product_qty
                                WHEN slb.name::text = 'Repair & Warrnty Replacement'::text THEN 1::numeric * sm.product_qty
                                WHEN spt.name::text = 'Delivery Note - Repair & Warranty Replacement'::text THEN 1::numeric * sm.product_qty
                                WHEN spt.name::text = 'Delivery Orders - Demo'::text THEN 1::numeric * sm.product_qty
                                WHEN spt.name::text = 'Delivery Orders - for View Purpose'::text THEN 1::numeric * sm.product_qty
                                WHEN spt.name::text = 'Delivery Orders - Rental'::text THEN 1::numeric * sm.product_qty
                                WHEN spt.name::text = 'Delivery Orders - Repair'::text THEN 1::numeric * sm.product_qty
                                WHEN spt.name::text = 'Delivery Orders - Replacement'::text THEN 1::numeric * sm.product_qty
                                WHEN spt.name::text = 'Delivery Orders - Sale'::text THEN 1::numeric * sm.product_qty
                                WHEN spt.name::text = 'Delivery Orders Purpose - RI'::text THEN 1::numeric * sm.product_qty
                                WHEN spt.name::text = 'Receipts - against Purchase'::text THEN '-1'::integer::numeric * sm.product_qty
                                WHEN spt.name::text = 'Returns from Customer'::text THEN '-1'::integer::numeric * sm.product_qty
                                WHEN spt.name::text = 'Returns from Supplier'::text THEN '-1'::integer::numeric * sm.product_qty
                                WHEN spt.name::text = 'Returns to Supplier'::text THEN 1::numeric * sm.product_qty
                                WHEN spt.code::text = 'outgoing'::text THEN sm.product_qty
                                WHEN spt.code::text = 'incoming'::text THEN - sm.product_qty
                                ELSE NULL::numeric
                            END AS move_qty,
                        COALESCE(sm.serial_id, 0) AS serial_id,
                        lot.name AS serial_number,
                        row_number() OVER () AS distinct_key,
                        ((((COALESCE(rp.parent_id, rp.id)::text || '-'::text) || lot.name::text) || '-'::text) || to_char(sp.date_done, ('yyyy-mm-dd hh24:mi:ss'::text || '-'::text))) ||  row_number() OVER ()::text AS partition_key
                    FROM stock_picking sp
                        JOIN stock_picking_type spt ON sp.picking_type_id = spt.id
                        JOIN stock_location sla ON sla.id = sp.location_id
                        JOIN stock_location slb ON slb.id = sp.location_dest_id
                        JOIN stock_move sm ON sm.picking_id = sp.id
                        JOIN res_partner rp ON sp.partner_id = rp.id
                        JOIN stock_production_lot lot ON lot.id = sm.serial_id
                    WHERE sm.serial_id IS NOT NULL AND sp.partner_id IS NOT NULL AND sp.state::text = 'done'::text AND (spt.code::text = ANY (ARRAY['incoming'::character varying::text, 'outgoing'::character varying::text]))
                """
        sql_qry = sql_qry + " AND (rp.id = " + str(partner) + " OR rp.parent_id = " + str(partner) + " ) "
        sql_qry = sql_qry + " AND sp.date_done <= " + "date('" + end_date + "')"
        sql_qry = sql_qry + '),'
        # -- get all moves before start-date
        sql_qry = sql_qry + " op_moves AS (SELECT * FROM move_lines where move_lines.move_date < " 
        sql_qry = sql_qry + "date('" + start_date + "')" + ' ),'
        # -- get opening balance
        sql_qry = sql_qry + """
                        op_bal_sum AS (
                                SELECT opm.parent_id,
                                    opm.serial_number,
                                    sum(opm.move_qty) AS op_qty
                                FROM op_moves opm
                                GROUP BY opm.parent_id,
                                    opm.serial_number
                                HAVING sum(opm.move_qty) <> 0::NUMERIC
                                ),
                """
        # -- get moves for period in question       
        sql_qry = sql_qry + """
                        pd_moves AS (
                            SELECT *
                            FROM move_lines
                            WHERE move_lines.move_date >=
                """ 
        sql_qry = sql_qry + " date('" + start_date + "')" + ' ), '
        # -- period summary
        sql_qry = sql_qry + """
                        pd_sum AS (
                            SELECT pm.parent_id,
                                pm.serial_number,
                                sum(CASE WHEN pm.move_qty > 0 THEN pm.move_qty ELSE 0 END) AS in_qty,
                                string_agg(CASE WHEN pm.move_qty > 0 THEN pm.doc_no || ' ' || pm.move_date::DATE ELSE '' END, ',') AS in_desc,
                                sum(CASE WHEN pm.move_qty < 0 THEN - pm.move_qty ELSE 0 END) AS out_qty,
                                string_agg(CASE WHEN pm.move_qty < 0 THEN pm.doc_no || ' ' || pm.move_date::DATE ELSE '' END, ',') AS out_desc
                            FROM pd_moves pm
                            GROUP BY pm.parent_id,
                                pm.serial_number
                        	),
                """
        # -- get list of serial numbers
        sql_qry = sql_qry + """ 
                        serial_list AS (
                            SELECT serial_number
                            FROM op_bal_sum
                            UNION
                            SELECT serial_number
                            FROM pd_sum
                    		),
                """
        # -- for each serial number get opening balance as well as movements during period
        sql_qry = sql_qry + """
                        serial_dtls AS (
                                SELECT sl.serial_number,
                                    coalesce(op.op_qty, 0) AS open_qty,
                                    coalesce(pd.in_qty, 0) AS in_qty,
                                    coalesce(pd.out_qty, 0) AS out_qty,
                                    coalesce(pd.in_desc, '') AS in_desc,
                                    coalesce(pd.out_desc, '') AS out_desc
                                FROM serial_list sl
                                LEFT JOIN op_bal_sum op ON op.serial_number = sl.serial_number
                                LEFT JOIN pd_sum pd ON pd.serial_number = sl.serial_number
                            ),        
                """
        # -- for details of serial number, sort by serial_number , date desc and set rank
        sql_qry = sql_qry + """
                        move_lines_rank AS (
                                SELECT m.doc_no,
                                    m.move_date,
                                    m.partner_id,
                                    m.parent_id,
                                    m.product_id,
                                    m.product_qty,
                                    m.move_qty,
                                    m.serial_id,
                                    m.serial_number,
                                    m.distinct_key,
                                    m.partition_key,
                                    row_number() OVER (
                                        PARTITION BY m.parent_id,
                                        m.serial_number ORDER BY m.partition_key DESC
                                        ) AS rn
                                FROM move_lines m
                                )
                """
        # -- final output - compute closing qty also
        sql_qry = sql_qry + """
                        SELECT row_number() OVER () AS id,
                            pa.name AS company_name,
                            pb.name AS company_partner_name,
                            mlr.serial_number,
                            pc.name AS category_name,
                            t.name AS product_name,
                            t.product_challan,
                            sd.open_qty,
                            sd.in_qty,
                            sd.out_qty,
                            sd.open_qty + sd.in_qty - sd.out_qty as close_qty,
                            sd.in_desc,
                            sd.out_desc
                        FROM move_lines_rank mlr
                            JOIN serial_dtls sd ON sd.serial_number::text = mlr.serial_number::text
                            JOIN res_partner pa ON pa.id = mlr.parent_id
                            JOIN res_partner pb ON pb.id = mlr.partner_id
                            JOIN product_product p ON mlr.product_id = p.id
                            JOIN product_template t ON p.product_tmpl_id = t.id
                            JOIN product_category pc ON pc.id = t.categ_id
                        WHERE mlr.rn = 1
                """

        sql_qry = 'CREATE OR REPLACE VIEW customer_serial_stock_ledger_report AS ( ' + sql_qry + ' )'

        tools.drop_view_if_exists(self._cr, 'customer_serial_stock_ledger_report')
        # tools.drop_view_if_exists(cr, self._table)

        self.env.cr.execute(sql_qry)
        # result = self.env.cr.fetchall()

        tree_view_id = self.env.ref('inventory_sql_reports.action_customer_serial_stock_ledger_report_tree').id
        return {
            'name': 'Customer Serial Stock Ledger',
            'view_type': 'form',
            'view_mode': 'tree',
            'views': [[tree_view_id, 'tree']],
            'res_model': 'customer.serial.stock.ledger.report',
            'type': 'ir.actions.act_window',
            'target': 'current',
        }


        # raise UserError(_(sql_qry))

        # self.env.cr.execute(sql_qry)
        # result = self.env.cr.fetchall()

        # raise UserError(_('Button is working'))


class CustomerSerialStockLedgerReport(models.Model):
    _name = "customer.serial.stock.ledger.report"
    _description = "Customer Serial Stock Ledger Report"
    _auto = False

    company_name = fields.Char(string='Company')
    company_partner_name = fields.Char(string='Company Partner')
    serial_number = fields.Char(string='Serial Number')
    category_name = fields.Char(string='Category')
    product_name = fields.Char(string='Product')
    product_challan = fields.Char(string='Product - Challan')
    open_qty = fields.Float(string='Opening Stock')
    in_qty = fields.Float(string='Received')
    out_qty = fields.Float(string='Returned')
    close_qty = fields.Float(string='Closing Stock')
    in_desc = fields.Char(string='Incoming Documents')
    out_desc = fields.Char(string='Outgoing Documents')
