# -*- coding: utf-8 -*-
# Copyright 2019 Ravi Krishnan
# See LICENSE file for full copyright and licensing details.

from odoo import api, fields, models, tools

class NonRentalInternalStock(models.Model):
    _name = "non.rental.location.stock"
    _auto = False
 
    location = fields.Char(string='Location')
    category_name = fields.Char(string='Category')
    product_name = fields.Char(string='Product')
    product_challan = fields.Char('Print on Challan')
    stock_in_hand = fields.Float(string='Product Qty')


    @api.model_cr
    def init(self):
        tools.drop_view_if_exists(self._cr, 'non_rental_location_stock')
        self._cr.execute("""
            CREATE OR REPLACE VIEW non_rental_location_stock AS (
                WITH rental_loc_name AS (
                    SELECT
                        stock_location.id,
                        "substring" (stock_location.complete_name::text, "position" (stock_location.complete_name::text, '/'::text) + 1) AS location_name
                    FROM
                        stock_location
                    WHERE
                        complete_name NOT LIKE '%Rent%'
                        AND usage = 'internal'
                ),
                out_move AS (
                    SELECT
                        m.product_id,
                        m.location_id AS loc_id,
                        sum(0) AS in_qty,
                        sum(product_qty) AS out_qty
                    FROM
                        stock_move m
                    WHERE
                        m.state LIKE 'done'
                        AND m.location_id IN (
                            SELECT
                                id
                            FROM
                                rental_loc_name)
                        GROUP BY
                            m.product_id,
                            m.location_id
                ),
                in_move AS (
                    SELECT
                        m.product_id,
                        m.location_dest_id AS loc_id,
                        sum(product_qty) AS in_qty,
                        sum(0) AS out_qty
                    FROM
                        stock_move m
                    WHERE
                        m.state LIKE 'done'
                        AND m.location_dest_id IN (
                            SELECT
                                id
                            FROM
                                rental_loc_name)
                        GROUP BY
                            m.product_id,
                            m.location_dest_id
                ),
                all_move AS (
                    SELECT
                        a.product_id,
                        a.loc_id,
                        a.in_qty,
                        a.out_qty
                    FROM
                        in_move a
                    UNION
                    SELECT
                        b.product_id,
                        b.loc_id,
                        b.in_qty,
                        b.out_qty
                    FROM
                        out_move b
                ),
                sum_move AS (
                    SELECT
                        product_id,
                        loc_id,
                        sum(in_qty) AS in_qty,
                        sum(out_qty) AS out_qty
                    FROM
                        all_move
                    GROUP BY
                        product_id,
                        loc_id
                )
                SELECT row_number() OVER () AS id,
                    l.location_name AS location,
                    pc.name as category_name,
                    t.name AS product_name,
                    t.product_challan,
                    a.in_qty - a.out_qty AS stock_in_hand
                FROM
                    sum_move a
                    JOIN product_product p ON a.product_id = p.id
                    JOIN product_template t ON p.product_tmpl_id = t.id
                    JOIN rental_loc_name l ON a.loc_id = l.id
                    JOIN product_category pc ON t.categ_id = pc.id
                WHERE (a.in_qty - a.out_qty) <> 0
            )""")