# -*- coding: utf-8 -*-
# Copyright 2019 Ravi Krishnan
# See LICENSE file for full copyright and licensing details.

from odoo import api, fields, models, tools

class NonRentalInternalStock(models.Model):
    _name = "non.rental.location.stock"
    _auto = False
 
    location = fields.Char(string='Location')
    category_name = fields.Char(string='Category')
    product_name = fields.Char(string='Product')
    product_challan = fields.Char('Print on Challan')
    stock_in_hand = fields.Float(string='Product Qty')
    serial_number = fields.Char(string='Serial Number')

    @api.model_cr
    def init(self):
        tools.drop_view_if_exists(self._cr, 'non_rental_location_stock')
        self._cr.execute("""
            CREATE OR REPLACE VIEW non_rental_location_stock AS (
                WITH non_serial_product
                AS (
                    SELECT a.id
                    FROM product_product a
                    JOIN product_template b ON a.product_tmpl_id = b.id
                    WHERE b.tracking = 'none'
                    ),
                rental_location
                AS (
                    SELECT stock_location.id
                    FROM stock_location
                    WHERE complete_name NOT LIKE '%Rent%'
                        AND usage = 'internal'
                    ),
                out_move
                AS (
                    SELECT m.product_id,
                        m.location_id AS loc_id,
                        sum(0) AS in_qty,
                        sum(product_qty) AS out_qty
                    FROM stock_move m
                    JOIN non_serial_product nsp ON nsp.id = m.product_id
                    JOIN rental_location rl ON rl.id = m.location_id
                    WHERE m.STATE LIKE 'done'
                    GROUP BY m.product_id,
                        m.location_id
                    ),
                in_move
                AS (
                    SELECT m.product_id,
                        m.location_dest_id AS loc_id,
                        sum(product_qty) AS in_qty,
                        sum(0) AS out_qty
                    FROM stock_move m
                    JOIN non_serial_product nsp ON nsp.id = m.product_id
                    JOIN rental_location rl ON rl.id = m.location_dest_id
                    WHERE m.STATE LIKE 'done'
                    GROUP BY m.product_id,
                        m.location_dest_id
                    ),
                all_move
                AS (
                    SELECT a.product_id,
                        a.loc_id,
                        a.in_qty,
                        a.out_qty
                    FROM in_move a
                    
                    UNION
                    
                    SELECT b.product_id,
                        b.loc_id,
                        b.in_qty,
                        b.out_qty
                    FROM out_move b
                    ),
                sum_move
                AS (
                    SELECT product_id,
                        loc_id,
                        sum(in_qty) AS in_qty,
                        sum(out_qty) AS out_qty
                    FROM all_move
                    GROUP BY product_id,
                        loc_id
                    ),
                all_non_serial_move
                AS (
                    SELECT 0 AS lot_id,
                        loc_id AS location_id,
                        product_id,
                        in_qty - out_qty AS bal_qty
                    FROM sum_move
                    WHERE in_qty <> out_qty
                    ),
                all_serial_move
                AS (
                    SELECT sq.lot_id,
                        sq.location_id,
                        sq.product_id,
                        1 AS bal_qty
                    FROM stock_quant sq
                    JOIN stock_production_lot spl ON sq.lot_id = spl.id
                        AND sq.product_id = spl.product_id
                    JOIN rental_location sl ON sl.id = sq.location_id
                    ),
                final_stock
                AS (
                    SELECT lot_id,
                        location_id,
                        product_id,
                        bal_qty
                    FROM all_serial_move
                    
                    UNION ALL
                    
                    SELECT lot_id,
                        location_id,
                        product_id,
                        bal_qty
                    FROM all_non_serial_move
                    )
                SELECT row_number() OVER () AS id,
                    a.lot_id,
                    a.location_id,
                    a.product_id,
                    "substring" (l.complete_name::text, "position" (l.complete_name::text, '/'::text) + 1)  AS location,
                    pc.name AS category_name,
                    t.name AS product_name,
                    t.product_challan,
                    spl.name AS serial_number,
                    a.bal_qty AS stock_in_hand
                FROM final_stock a
                JOIN product_product p ON a.product_id = p.id
                JOIN product_template t ON p.product_tmpl_id = t.id
                JOIN stock_location l ON a.location_id = l.id
                JOIN product_category pc ON t.categ_id = pc.id
                LEFT JOIN stock_production_lot spl ON a.lot_id = spl.id
            )""")