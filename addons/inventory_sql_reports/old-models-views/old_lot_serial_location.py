# -*- coding: utf-8 -*-
# Copyright 2019 Ravi Krishnan
# See LICENSE file for full copyright and licensing details.

from odoo import api, fields, models, tools

class LotSerialLocation(models.Model):
    _name = "lot.serial.location"
    _auto = False
 
    serial_number = fields.Char(string='Serial #')
    location = fields.Char(string='Location')
    product = fields.Char(string='Product')
    short_name = fields.Char(string='Short Name')
    oem_serial_no = fields.Char(string='OEM #')
    alpha_serial_no = fields.Char(string='Alpha #')
    barcode = fields.Char(string='Barcode')
    short_name_product = fields.Char(string='Short Name Product')
    long_name = fields.Char(string='Long Name')
    category_name = fields.Char(string='Category')
    product_challan = fields.Char('Print on Challan')
    partner_id = fields.Many2one(comodel_name='res.partner', string='Partner')

    @api.model_cr
    def init(self):
        print "Connected"
        tools.drop_view_if_exists(self._cr, 'lot_serial_location')
        self._cr.execute("""
            CREATE OR REPLACE VIEW lot_serial_location AS (
                WITH lot AS (
                        SELECT
                            a.product_id,
                            a.name AS serial_number,
                            a.oem_serial_no,
                            a.alpha_serial_no,
                            a.actual_barcode AS barcode,
                            b.location_id,
                            e.partner_id
                        FROM
                            stock_production_lot a
                            JOIN stock_quant b ON a.id = b.lot_id
                            JOIN stock_quant_move_rel c ON c.quant_id = b.id
                            JOIN stock_move d ON d.id = c.move_id
                            JOIN stock_picking e ON e.id = d.picking_id
                        WHERE
                            b.location_id <> 18
                            AND d.picking_id IS NOT NULL
                            AND b.location_id = d.location_dest_id
                        ), product AS (
                        SELECT pp.id,
                            pp.product_tmpl_id,
                            pt.name AS product,
                            pt.short_name,
                            pt.short_name_product,
                            pt.long_name,
                            pc.name AS category_name,
                            pt.product_challan
                        FROM product_product pp
                            JOIN product_template pt ON pp.product_tmpl_id = pt.id
                            JOIN product_category pc ON pc.id = pt.categ_id
                        )
                SELECT row_number() OVER () AS id,
                    ax.serial_number,
                    sl.name AS location,
                    ax.partner_id,
                    px.product,
                    px.short_name,
                    ax.oem_serial_no,
                    ax.alpha_serial_no,
                    ax.barcode,
                    px.short_name_product,
                    px.long_name,
                    px.category_name,
                    px.product_challan
                FROM lot ax
                    JOIN product px ON ax.product_id = px.id
                    JOIN stock_location sl ON ax.location_id = sl.id
            )""")


with lot as (SELECT
                            a.product_id,
                            a.name AS serial_number,

                            b.in_date as quant_date,
                            d.date as move_date,
                            d.date_expected as move_date_expected,
                            e.min_date as picking_min_date,
                            e.date as picking_date,
                            b.location_id,
                            e.partner_id
                        FROM
                            stock_production_lot a
                            JOIN stock_quant b ON a.id = b.lot_id
                            JOIN stock_quant_move_rel c ON c.quant_id = b.id
                            JOIN stock_move d ON d.id = c.move_id
                            JOIN stock_picking e ON e.id = d.picking_id
                        WHERE
                            b.location_id <> 18
                            AND d.picking_id IS NOT NULL
                            AND b.location_id = d.location_dest_id
                            AND a.product_id = d.product_id),
                            dup_serial as (
                            select serial_number  from lot
                            group by serial_number
                            having count(*) > 1)
                            select * from lot where serial_number in (select serial_number from dup_serial)
                            order by serial_number, picking_date;            



select sq.id , sq.lot_id , sq.write_date , sq.location_id , sq.product_id , spl.name as serial_number , sl.complete_name as location_name 
 from stock_quant  sq 
 join stock_production_lot spl
 on sq.lot_id = spl.id and sq.product_id = spl.product_id 
 join stock_location sl on sl.id = sq.location_id 
 where sq.lot_id is not null and sq.location_id <> 18
 order by spl.name         


 SELECT sq.id,
	sq.lot_id,
	sq.location_id,
	sq.product_id,
	spl.name AS serial_number,
	sl.complete_name AS location_name,
	pc.name AS category_name,
	pt.product_challan,
	pt.name AS product_name
FROM stock_quant sq
JOIN stock_production_lot spl ON sq.lot_id = spl.id
	AND sq.product_id = spl.product_id
JOIN stock_location sl ON sl.id = sq.location_id
JOIN product_product pp ON pp.id = sq.product_id
JOIN product_template pt ON pt.id = pp.product_tmpl_id
JOIN product_category pc ON pc.id = pt.categ_id
WHERE sq.lot_id IS NOT NULL
	AND sq.location_id <> 18
ORDER BY sl.complete_name,
	pc.name,
	pt.name,
	spl.name                    