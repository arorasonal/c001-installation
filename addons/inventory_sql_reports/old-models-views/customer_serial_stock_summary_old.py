# -*- coding: utf-8 -*-

from odoo import api, exceptions, fields, models
from odoo.exceptions import UserError, ValidationError
from datetime import datetime,timedelta,date
from odoo import tools, _

class CustomerSerialStockSummaryWizard(models.TransientModel) :
    _name = 'customer.serial.stock.summary'
    _description = 'Customer Serial Stock Summary Wizard'

    partner_id = fields.Many2one('res.partner','Partner',required=True)
    as_on_date = fields.Date('As On',default=lambda *a: datetime.now(),required=True)

    @api.multi
    def button_get(self):
        self.ensure_one()
        for each in self.partner_id:
            partner = each.id
        summary_date = self.as_on_date

        # sql_qry = "select * from jasper.cjpl_js_party_serial_stock_summary where partner_id  = " + str(partner)

        sql_qry = """
                    WITH move_lines AS (
                    SELECT sp.id,
                        sp.name AS doc_no,
                        date(sp.date_done) AS move_date,
                        sp.origin,
                        sp.partner_id,
                        COALESCE(rp.parent_id, rp.id) AS parent_id,
                        sp.picking_type_id,
                        sp.transaction_type_id,
                        sp.order_id,
                        sp.location_id,
                        sla.name AS from_location,
                        sp.location_dest_id,
                        slb.name AS to_location,
                        spt.name AS movement_name,
                        spt.code AS movement_type,
                        sm.product_id,
                        sm.product_qty,
                            CASE
                                WHEN sla.name::text = 'Customers'::text THEN '-1'::integer::numeric * sm.product_qty
                                WHEN sla.name::text = 'Rental Out Stock'::text THEN '-1'::integer::numeric * sm.product_qty
                                WHEN sla.name::text = 'Repair & Warrnty Replacement'::text THEN '-1'::integer::numeric * sm.product_qty
                                WHEN slb.name::text = 'Customers'::text THEN 1::numeric * sm.product_qty
                                WHEN slb.name::text = 'Rental Out Stock'::text THEN 1::numeric * sm.product_qty
                                WHEN slb.name::text = 'Repair & Warrnty Replacement'::text THEN 1::numeric * sm.product_qty
                                WHEN spt.name::text = 'Delivery Note - Repair & Warranty Replacement'::text THEN 1::numeric * sm.product_qty
                                WHEN spt.name::text = 'Delivery Orders - Demo'::text THEN 1::numeric * sm.product_qty
                                WHEN spt.name::text = 'Delivery Orders - for View Purpose'::text THEN 1::numeric * sm.product_qty
                                WHEN spt.name::text = 'Delivery Orders - Rental'::text THEN 1::numeric * sm.product_qty
                                WHEN spt.name::text = 'Delivery Orders - Repair'::text THEN 1::numeric * sm.product_qty
                                WHEN spt.name::text = 'Delivery Orders - Replacement'::text THEN 1::numeric * sm.product_qty
                                WHEN spt.name::text = 'Delivery Orders - Sale'::text THEN 1::numeric * sm.product_qty
                                WHEN spt.name::text = 'Delivery Orders Purpose - RI'::text THEN 1::numeric * sm.product_qty
                                WHEN spt.name::text = 'Receipts - against Purchase'::text THEN '-1'::integer::numeric * sm.product_qty
                                WHEN spt.name::text = 'Returns from Customer'::text THEN '-1'::integer::numeric * sm.product_qty
                                WHEN spt.name::text = 'Returns from Supplier'::text THEN '-1'::integer::numeric * sm.product_qty
                                WHEN spt.name::text = 'Returns to Supplier'::text THEN 1::numeric * sm.product_qty
                                WHEN spt.code::text = 'outgoing'::text THEN sm.product_qty
                                WHEN spt.code::text = 'incoming'::text THEN - sm.product_qty
                                ELSE NULL::numeric
                            END AS move_qty,
                        COALESCE(sm.serial_id, 0) AS serial_id,
                        lot.name AS serial_number,
                        row_number() OVER () AS distinct_key,
                        ((((COALESCE(rp.parent_id, rp.id)::text || '-'::text) || lot.name::text) || '-'::text) || to_char(sp.date_done, ('yyyy-mm-dd hh24:mi:ss'::text) || '-'::text)) || row_number() OVER ()::text AS partition_key
                    FROM stock_picking sp
                        JOIN stock_picking_type spt ON sp.picking_type_id = spt.id
                        JOIN stock_location sla ON sla.id = sp.location_id
                        JOIN stock_location slb ON slb.id = sp.location_dest_id
                        JOIN stock_move sm ON sm.picking_id = sp.id
                        JOIN res_partner rp ON sp.partner_id = rp.id
                        JOIN stock_production_lot lot ON lot.id = sm.serial_id
                    WHERE sm.serial_id IS NOT NULL AND sp.partner_id IS NOT NULL AND sp.state::text = 'done'::text AND (spt.code::text = ANY (ARRAY['incoming'::character varying::text, 'outgoing'::character varying::text]))
                """
        sql_qry = sql_qry + " AND (rp.id = " + str(partner) + " OR rp.parent_id = " + str(partner) + " ) "
        sql_qry = sql_qry + " AND sp.date_done <= " + "date('" + summary_date + "')"

        sql_qry = sql_qry + """
                        ), move_lines_sum AS (
                        SELECT move_lines.parent_id,
                            move_lines.serial_number,
                            sum(move_lines.move_qty) AS balance_qty
                        FROM move_lines
                        GROUP BY move_lines.parent_id, move_lines.serial_number
                        HAVING sum(move_lines.move_qty) <> 0::numeric
                        ), move_lines_rank AS (
                        SELECT m.id,
                            m.doc_no,
                            m.move_date,
                            m.origin,
                            m.partner_id,
                            m.parent_id,
                            m.picking_type_id,
                            m.transaction_type_id,
                            m.order_id,
                            m.location_id,
                            m.from_location,
                            m.location_dest_id,
                            m.to_location,
                            m.movement_name,
                            m.movement_type,
                            m.product_id,
                            m.product_qty,
                            m.move_qty,
                            m.serial_id,
                            m.serial_number,
                            m.distinct_key,
                            m.partition_key,
                            row_number() OVER (PARTITION BY m.parent_id, m.serial_number ORDER BY m.partition_key DESC) AS rn
                        FROM move_lines m
                        )
                SELECT row_number() OVER () AS id,
                    pa.name AS company_name,
                    pb.name AS company_partner_name,
                    mlr.movement_name,
                    tt.name AS transaction_type,
                    mlr.from_location,
                    mlr.to_location,
                    mlr.serial_number,
                    lot.alpha_serial_no AS alpha_no,
                    pc.name AS category_name,
                    t.name AS product_name,
                    t.product_challan,
                    mlr.doc_no,
                    mlr.move_date,
                    COALESCE(sale_order.name, ''::character varying) AS so_no,
                    mls.balance_qty
                FROM move_lines_rank mlr
                    JOIN move_lines_sum mls ON mls.parent_id = mlr.parent_id AND mls.serial_number::text = mlr.serial_number::text
                    JOIN res_partner pa ON pa.id = mlr.parent_id
                    JOIN res_partner pb ON pb.id = mlr.partner_id
                    JOIN product_product p ON mlr.product_id = p.id
                    JOIN product_template t ON p.product_tmpl_id = t.id
                    JOIN product_category pc ON pc.id = t.categ_id
                    JOIN stock_production_lot lot ON lot.id = mlr.serial_id
                    LEFT JOIN sale_order ON mlr.order_id = sale_order.id
                    LEFT JOIN transaction_type tt ON tt.id = mlr.transaction_type_id
                WHERE mlr.rn = 1
        """
        sql_qry = 'CREATE OR REPLACE VIEW customer_serial_stock_summary_report AS ( ' + sql_qry + ' )'

        tools.drop_view_if_exists(self._cr, 'customer_serial_stock_summary_report')
        # tools.drop_view_if_exists(cr, self._table)

        self.env.cr.execute(sql_qry)
        # result = self.env.cr.fetchall()

        tree_view_id = self.env.ref('inventory_sql_reports.action_customer_serial_stock_summary_report_tree').id
        return {
            'name': 'Customer Serial Stock Summary',
            'view_type': 'form',
            'view_mode': 'tree',
            'views': [[tree_view_id, 'tree']],
            'res_model': 'customer.serial.stock.summary.report',
            'type': 'ir.actions.act_window',
            'target': 'current',
        }


class CustomerSerialStockSummaryReport(models.Model):
    _name = "customer.serial.stock.summary.report"
    _description = "Customer Serial Stock Summary Report"
    _auto = False

    company_name = fields.Char(string='Company')
    company_partner_name = fields.Char(string='Company Partner')
    serial_number = fields.Char(string='Serial Number')
    alpha_no = fields.Char(string='Alpha Serial')
    movement_name = fields.Char(string='Movement Name')
    from_location = fields.Char(string='From')
    to_location = fields.Char(string='To')
    transaction_type = fields.Char(string='Transaction Type')
    category_name = fields.Char(string='Category')
    product_name = fields.Char(string='Product')
    product_challan = fields.Char(string='Product - Challan')
    doc_no = fields.Char(string='Delivery Document')
    move_date = fields.Date(string='Date')
    so_no = fields.Char(string='SO #')
    balance_qty = fields.Float(string='Quantity')
