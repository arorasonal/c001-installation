# -*- coding: utf-8 -*-
{
    'name': "Sales Dashboard Ninja",

    'summary': """
    Sales Dashboard Ninja!
    """,

    'description': """
        Dashboard Ninja v10.0,
        Odoo Dashboard, 
        Dashboard,
        Odoo apps,
        Dashboard app,
        HR Dashboard,
        Sales Dashboard, 
        inventory Dashboard, 
        Lead Dashboard, 
        Opportunity Dashboard, 
        CRM Dashboard,
	    POS,
	    POS Dashboard,
	    Connectors,
	    Web Dynamic,
	    Report Import/Export,
	    Date Filter,
	    HR,
	    Sales,
	    Theme,
	    Tile Dashboard,
	    Dashboard Widgets,
	    Dashboard Manager,
	    Debranding,
	    Customize Dashboard,
	    Graph Dashboard,
	    Charts Dashboard,
	    Invoice Dashboard,
	    Project management,
	    dashboard, 
	odoo dashboard, 
	odoo sales apps, 
	all in one sales dashboard, 
	sales dashboard odoo, 
	dashboard items, 
	sales multiple dashboards, 
	sales dashboard menu, 
	dashboard view, 
	create multiple dashboards, 
	multiple dashboard menu, 
	edit dashboard items, 
	sales dahsboard view, 
        ksolves,
        ksolves apps,
        ksolves india pvt. ltd.
    """,

    'author': "Ksolves India Pvt. Ltd.",
    'live_test_url': 'https://dashboardninja10.kappso.com',
    'license': 'OPL-1',
    'currency': 'EUR',
    'price': 0.0,
    'website': "https://www.ksolves.com",
    'maintainer': 'Ksolves India Pvt. Ltd.',
    'category': 'Tools',
    'support': 'sales@ksolves.com',
    'images': ['static/description/banner.png'],

    'depends': ['ks_dashboard_ninja', 'sale'],

    'data': [
        'data/ks_sales_data.xml',
    ],

}
