# -*- coding: utf-8 -*-

from odoo import api,fields,models,_
from odoo.exceptions import UserError
from datetime import datetime
from tempfile import TemporaryFile
import requests
import json              
import base64

supply_type = {'b2b': "B2B", 'sezwp': "SEZWP", "sezwop": "SEZWOP"}

class AccountInvoice(models.Model):
    _inherit="account.invoice"

    irn = fields.Char(string='IRN')
    ack_number = fields.Char('ACK Number')
    ack_time = fields.Datetime(string='ACK Date Time')
    ewb_number = fields.Char('EWB Number')
    ewb_time = fields.Datetime('EWB Date Time')
    ewb_validity = fields.Datetime('EWB Validity')
    irn_qr_code = fields.Binary('IRN QE Code')
    irn_invoice_code = fields.Binary('IRN Invoice Code')
    irn_error_details = fields.Char(string='IRN Error Details')

    e_inv_supply_type = fields.Selection([('b2b', 'B2B'), ('sezwp', 'SEZWP'), ('sezwop', 'SEZWOP')], string='Supply Type')
    e_inv_trn_type = fields.Selection([('reg', 'REG'), ('shp', 'SHP'), ('dis', 'DIS'), ('cmb', 'CMB')], string='Trn Type')
    

    def get_line_tax(self, line):
        tax_rate = 0.00
        igst = 0.00
        cgst = 0.00
        sgst = 0.00
        tax_amt = 0.00

        for val in line:
            taxable_amount = val.price_subtotal
            for each_tax_id in val.invoice_line_tax_ids:
                rate = each_tax_id.e_invoice_tax_rate
                # -- next line e_invoice_tax_rate not visible **PENDING**
                rate = 18 
                if each_tax_id.tax_group_id.name in ['IGST']:
                    tax_rate += each_tax_id.amount
                    tax_amt += round(taxable_amount * each_tax_id.amount / 100,2)
                    igst += round(taxable_amount * each_tax_id.amount / 100,2)
                elif each_tax_id.tax_group_id.name == 'Taxes' and each_tax_id.children_tax_ids:
                    for each_child in each_tax_id.children_tax_ids:
                        if each_child.tax_group_id.name in ['CGST', 'SGST']:
                            tax_amt += round(taxable_amount * each_child.amount / 100,2)
                            tax_rate += each_child.amount
                            if each_child.tax_group_id.name in ['CGST']:
                                cgst += round(taxable_amount * each_child.amount / 100,2)
                            else:
                                sgst += round(taxable_amount * each_child.amount / 100,2)
                else:
                    tax_amt += round(taxable_amount * each_tax_id.amount / 100,2)
                    
        return {'tax_amt': tax_amt, 'tax_rate': tax_rate, 'igst': igst, 'cgst': cgst, 'sgst': sgst}


    @api.multi
    def get_irn_einvoice(self):

        e_inv_values = {"TranDtls": {}, "DocDtls": {}, "SellerDtls": {}, "BuyerDtls": {}, "ShipDtls": {}, "ItemList": [], 'ValDtls': {}}
        if self.number:
            e_inv_values.update({'CDKey': self.company_id.cdkey, 
                'EInvUserName': self.company_id.e_inv_web_user_name, 
                'EInvPassword': self.company_id.e_inv_web_pwd,
                'EFUserName': self.company_id.efusername, 
                'EFPassword': self.company_id.efpassword})
        # -- next line is error -- 'GSTIN' should be CJPL GSTN **PENDING**
        if self.invoice_gst:
            e_inv_values.update({'GSTIN': self.invoice_gst, 
                'GetQRImg': "1", 
                'GetSignedInvoice': "1"})
        # -- next line is part of  testing credentials 
        e_inv_values.update({'GSTIN': '07AAACW3775F006'})

        #TransDrls Details 
        e_inv_values['TranDtls'].update({'SupTyp': supply_type.get(self.e_inv_supply_type), 
                'RegRev': "Y", 
                "EcmGstin": None, 
                "IgstOnIntra": "N"})
        
        #DocDtls Details
        if self.number:
            e_inv_values['DocDtls'].update({'Typ': "INV", "No": self.number})
        if self.date_invoice:
            e_inv_values['DocDtls'].update({"Dt": datetime.strptime(str(self.date_invoice),'%Y-%m-%d').strftime("%d/%m/%Y")})

        #SellerDtls Details
        if self.journal_id.bill_from.partner_id.gstin:
            e_inv_values['SellerDtls'].update({"Gstin": self.journal_id.bill_from.partner_id.gstin})
        # -- next line is part of  testing credentials 
        e_inv_values['SellerDtls'].update({"Gstin": '07AAACW3775F006'})
        if self.journal_id.bill_from.partner_id.name:
            e_inv_values['SellerDtls'].update({"LglNm": self.journal_id.bill_from.partner_id.name})
        if self.journal_id.bill_from.partner_id.street:
            e_inv_values['SellerDtls'].update({"Addr1": self.journal_id.bill_from.partner_id.street})
        if self.journal_id.bill_from.partner_id.street2:
            e_inv_values['SellerDtls'].update({"Addr2": self.journal_id.bill_from.partner_id.street2})
        if self.journal_id.bill_from.partner_id.state_id:
            e_inv_values['SellerDtls'].update({"Loc": self.journal_id.bill_from.partner_id.state_id.name})
        if self.journal_id.bill_from.partner_id.state_id:
            e_inv_values['SellerDtls'].update({"Stcd": self.journal_id.bill_from.partner_id.state_id.state_code})
        if self.journal_id.bill_from.partner_id.zip:
            e_inv_values['SellerDtls'].update({"Pin": self.journal_id.bill_from.partner_id.zip})

        #BuyerDtls Details
        if self.partner_invoice_id.gstin:
            e_inv_values['BuyerDtls'].update({"Gstin": self.partner_invoice_id.gstin})
        if self.partner_invoice_id.name:
            e_inv_values['BuyerDtls'].update({"LglNm": self.partner_invoice_id.name})
        if self.partner_invoice_id.state_id.code:
            e_inv_values['BuyerDtls'].update({"Pos": self.partner_invoice_id.state_id.state_code})
        if self.partner_invoice_id.street:
            e_inv_values['BuyerDtls'].update({"Addr1": self.partner_invoice_id.street})
        if self.partner_invoice_id.state_id:
            e_inv_values['BuyerDtls'].update({"Loc": self.partner_invoice_id.state_id.name})
        if self.partner_invoice_id.zip:
            e_inv_values['BuyerDtls'].update({"Pin": self.partner_invoice_id.zip})
        if self.partner_invoice_id.state_id.code:
            e_inv_values['BuyerDtls'].update({"Stcd": self.partner_invoice_id.state_id.state_code})

        #ShipDtls Details
        if self.partner_shipping_id.gstin:
            e_inv_values['ShipDtls'].update({"Gstin": self.partner_shipping_id.gstin})
        if self.partner_shipping_id.name:
            e_inv_values['ShipDtls'].update({"LglNm": self.partner_shipping_id.name})
        if self.partner_shipping_id.state_id.code:
            e_inv_values['ShipDtls'].update({"Pos": self.partner_shipping_id.state_id.state_code})
        if self.partner_shipping_id.street:
            e_inv_values['ShipDtls'].update({"Addr1": self.partner_shipping_id.street})
        if self.partner_shipping_id.state_id:
            e_inv_values['ShipDtls'].update({"Loc": self.partner_shipping_id.state_id.name})
        if self.partner_shipping_id.zip:
            e_inv_values['ShipDtls'].update({"Pin": self.partner_shipping_id.zip})
        if self.partner_shipping_id.state_id.code:
            e_inv_values['ShipDtls'].update({"Stcd": self.partner_shipping_id.state_id.state_code})

        # Item information
        cgst_total = 0.00
        sgst_total = 0.00
        igst_total = 0.00
        oth_chrg_total = 0.00
        round_off_value = 0.00

        #Item Details
        if self.invoice_line_ids:
            seq = 1
            for line in self.invoice_line_ids:
                product_type = ''
                if line.product_id.type == 'service':
                    product_type = 'Y'
                else:
                    product_type = 'N'
                line_dict = {}
                line_dict.update({'SlNo': seq, 'IsServc': product_type})
                # -- is service must be true for all rentals . base it on sale type !! **PENDING**
                line_dict.update({'SlNo': seq, 'IsServc': 'Y'})
                if line.hsn_no:
                    line_dict.update({'HsnCd': line.hsn_no})
                if line.product_id.barcode:
                    line_dict.update({'Barcde': line.product_id.barcode})
                if line.product_id.uom_id:
                    line_dict.update({'Unit': line.product_id.uom_id.name})
                # -- update uom with e-invoicing code **PENDING**   
                line_dict.update({'Unit': 'UNT'})
                if line.quantity:
                    line_dict.update({'Qty': line.quantity})
                # -- totamt is same as assamt = line.price_subtotal    
                if line.price_unit:
                    line_dict.update({'UnitPrice': line.price_unit, 'AssAmt': line.price_subtotal,
                        'TotAmt': line.price_subtotal})
                #if line.invoice_line_tax_ids:
                # -- totitemval is price_subtotal + tax
                # -- following tax amounts are needed
                # -- "IgstAmt", "CgstAmt", "SgstAmt", "OthChrg" -- other charges **PENDING** 
                line_dict.update({'GstRt': self.get_line_tax(line)['tax_rate'], 
                    'TotItemVal': line.price_subtotal + self.get_line_tax(line)['tax_amt'],
                    'IgstAmt': self.get_line_tax(line)['igst'],
                    'CgstAmt': self.get_line_tax(line)['cgst'],
                    'SgstAmt': self.get_line_tax(line)['sgst']})                    
                e_inv_values['ItemList'].append(line_dict)

                cgst_total += self.get_line_tax(line)['cgst']
                sgst_total += self.get_line_tax(line)['sgst']
                igst_total += self.get_line_tax(line)['igst']

                seq += 1

        # -- following tax amounts are needed
        # -- "CgstVal", "SgstVal", "IgstVal", "OthChrg", "RndOffAmt": 0,

        #ValDtls Details
        if self.amount_untaxed:
            e_inv_values['ValDtls'].update({'AssVal': self.amount_untaxed, 
                'TotInvVal': self.amount_total,
                'CgstVal': cgst_total,
                'SgstVal': sgst_total,
                'IgstVal': igst_total
                })

        url = self.company_id.e_inv_web_url
        headers = {
           'Content-Type': 'application/json',
        }
        # response = requests.post(url, data = e_inv_values).text

        irn_response = requests.post(url, headers=headers, data = json.dumps(e_inv_values))
        # -- raise UserError(_(response.json()))
        # -- raise Warning(_(irn_response))
        # -- raise UserError(_(response.json()))

        if (irn_response.status_code == 200 or irn_response.status_code == 201):
            irn_response_json = json.loads(irn_response.text)
            if irn_response_json:
                actual_response = irn_response_json[0]
                self.create_einvoice_log(actual_response)
                if 'Status' in actual_response and actual_response.get('Status') == '1':
                    self.irn = actual_response.get('Irn')
                    self.ack_time= actual_response.get('AckDate')
                    self.ack_number = actual_response.get('AckNo')
                    self.ewb_number = actual_response.get('EwbNo')
                    self.ewb_time = actual_response.get('EwbDt')
                    self.ewb_validity = actual_response.get('EwbValidTill')
                    self.irn_error_details = ''
                    # self.irn_qr_code = response.get('SignedQRCode')
                    # self.irn_invoice_code = response.get('SignedInvoice')
                else:
                    self.irn_error_details = actual_response.get('ErrorMessage')
                    # -- else:
                    # --    raise UserError(_('Error in Data'))
            else:
                raise UserError(_('Unable to get json response'))
        else:
            raise UserError(_('Unable to get response'))


        f = TemporaryFile('w+')
        f.write(str(json.dumps(e_inv_values)))
        f.seek(0) # this will send you to the beginning of the file
        file_data = f.read()
        f.close()

        # file = open("e_invoice_json.txt", 'w+')
        # file.write(str(json.dumps(e_inv_values)))
        # file.close()
        # f=open("e_invoice_json.txt", "r")
        # file_data = f.read()
        
        values = {
            'name': "E-Invoice Json",
            'datas_fname': 'e_invoice_json.txt',
            'res_model': 'account.invoice',
            'res_id': self.id,
            'type': 'binary',
            'public': True,
            'datas': file_data.encode('utf8').encode('base64'),
        }
        attachment_id = self.env['ir.attachment'].sudo().create(values)

        return True

    def create_einvoice_log(self,response):
        values = {'invoice_id': self.id}
        if response.get('DocNo'):
            values.update({'doc_no':response.get('DocNo')})
        if response.get('DocDate'):
            values.update({'doc_dt':datetime.strptime(response.get('DocDate'),'%d/%m/%Y').strftime("%Y-%m-%d")})
        if response.get('Status'):
            values.update({'irn_status':response.get('Status')})
        if response.get('ErrorMessage'):
            values.update({'irn_error_msg':response.get('ErrorMessage')})
        if response.get('ErrorCode'):
            values.update({'irn_error_code':response.get('ErrorCode')})
        self.env['e.invoicing.log'].create(values)

class EinvoiceLog(models.Model):
    _name = "e.invoicing.log"

    invoice_id = fields.Many2one('account.invoice')
    picking_id = fields.Many2one('stock.picking')
    doc_no = fields.Char('Doc no')
    doc_dt = fields.Date('Doc dt')
    irn_post_time = fields.Datetime('Irn post time')
    irn_get_time = fields.Datetime('Irn get time')
    irn_status = fields.Char('Irn status')
    irn_error_msg = fields.Char('Irn error msg')
    irn_error_code = fields.Char(string='Irn error code')


class ResCompany(models.Model):
    _inherit="res.company"

    e_inv_web_url = fields.Char('E-Inv Web Url')
    e_inv_web_user_name = fields.Char('E-Inv web user name')
    e_inv_web_pwd = fields.Char('E Inv Web Pwd')
    efusername = fields.Char('EF Username')
    cdkey = fields.Char('CDkey')
    efpassword = fields.Char('EF Password')
    e_inv_legal_name = fields.Char('E-Inv Legal Name')
    e_inv_trade_name = fields.Char('E-Inv Trade Name')

    
class ResPartner(models.Model):
    _inherit="res.partner"

    e_inv_supply_type = fields.Selection([('b2b', 'B2B'), ('sezwp', 'SEZWP'), ('sezwop', 'SEZWOP')], string='Supply Type')
    e_inv_trn_type = fields.Selection([('reg', 'REG'), ('shp', 'SHP'), ('dis', 'DIS'), ('cmb', 'CMB')], string='Trn Type')
    e_inv_cust_type = fields.Selection([('dmc', 'Domestic Customer'), ('sec', 'SEZ Customer'), ('exc', 'Export Customer'), ('dec', 'Deemed Export Customer')], string='E-inv customer type')

class AccountTax(models.Model):
    _inherit = 'account.tax'

    e_invoice_tax_rate = fields.Float('Tax Rate')


