# -*- coding: utf-8 -*-
{
    'name': "E-Invoicing",
    'author': 'Sonal Arora',
    'category': 'Invoice',
    'summary': """Get IRN Number""",
    'description': """
""",
    'version': '10.0.1.0.0',
    'depends': ['account', 'stock','invoice_cjpl','base'],
    'data':[
    'views/e_invoice.xml',
    'views/res_company.xml'],
    'installable': True,
    'application': True,
    'auto_install': False,
}
