# -*- coding: utf-8 -*-

{
    'name': 'Base Finance',
    'version': '1.0',
    'category': 'Account',
    'sequence': 14,
    'summary': 'Base Finance',
    'description': """
    """,
    'author': 'Apagen Solutions Pvt. Ltd.',
    'website': 'https://www.apagen.com',
    'depends': [
        'account',
        'base',
        # 'hr',
        'analytic',
        'account_cancel',
        'sale',
        'web',
        ],
    'data': [
        # 'security/base_finance_security.xml',
        # 'security/ir.model.access.csv',
        # 'reports/account_move_report.xml',
        # # 'reports/account_payment_report.xml',
         'views/base_finance_view.xml',
        # 'data/account_payment_mail.xml',
    ],
    'installable': True,
    'auto_install': False,
    'application': True,
}
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
