# -*- coding: utf-8 -*-
import logging
from odoo import api, fields, models, tools, _
# from odoo.tools.translate import
from odoo.tools.safe_eval import safe_eval
from odoo.exceptions import UserError, ValidationError, Warning
try:
    from num2words import num2words
except ImportError:
    logging.getLogger(__name__).warning(
        "The num2words python library is not installed, l10n_mx_edi features won't be fully available.")
    num2words = None


class AccountAnalyticInvoiceLine(models.Model):
    _inherit = 'res.partner'

    payment_bank_account = fields.Many2one(
        'res.partner.bank', "Payment Bank Account")


class AccountInvoice(models.Model):
    _inherit = "account.invoice"

    user_id = fields.Many2one(
        'res.users', string='Salesperson', track_visibility='onchange',
        readonly=True, states={'draft': [('readonly', False)]},
        default=lambda self: self.env.user, copy=False)

    amount_tax_words = fields.Char(
        "Tax (In Words)")#, compute="_compute_amount_tax_words")

    amount_untaxed_words = fields.Char(
        "Untaxed Amount (In Words)")#, compute="_compute_amount_tax_words")

    # @api.depends('amount_tax')
    # def _compute_amount_tax_words(self):
    #     for invoice in self:
    #         invoice.amount_tax_words = invoice.currency_id.amount_to_text(
    #             invoice.amount_tax)
    #         invoice.amount_untaxed_words = invoice.currency_id.amount_to_text(
    #             invoice.amount_untaxed)


class Company(models.Model):
    _inherit = "res.company"

    company_registry = fields.Char("CIN")
    pan_no = fields.Char(string='PAN', size=10)


class Currency(models.Model):
    _inherit = "res.currency"

    currency_unit_label = fields.Char(
        string="Currency Unit", help="Currency Unit Name")

    @api.multi
    def amount_to_text(self, amount):
        self.ensure_one()

        def _num2words(number, lang):
            try:
                return num2words(number, lang=lang).title()
            except NotImplementedError:
                return num2words(number, lang='en').title()

        if num2words is None:
            logging.getLogger(__name__).warning(
                "The library 'num2words' is missing, cannot render textual amounts.")
            return ""

        formatted = "%.{0}f".format(self.decimal_places) % amount
        parts = formatted.partition('.')
        integer_value = int(parts[0])
        fractional_value = int(parts[2] or 0)

        lang_code = self.env.context.get('lang') or self.env.user.lang
        lang = self.env['res.lang'].search([('code', '=', lang_code)])
        amount_words = tools.ustr('{amt_value} {amt_word}').format(
            amt_value=_num2words(integer_value, lang=lang.iso_code),
            amt_word=self.currency_unit_label,
        )
        if not self.is_zero(amount - integer_value):
            amount_words += ' ' + _('and') + tools.ustr(' {amt_value} {amt_word}').format(
                amt_value=_num2words(fractional_value, lang=lang.iso_code),
                amt_word=self.currency_subunit_label,
            )
        return amount_words


class AccountPayment(models.Model):
    _name = 'account.payment'
    _inherit = ['account.payment', 'mail.thread']

#    state = fields.Selection(
#        [
#            ('draft', 'Draft'),
#            ('waiting_approval', 'Waiting Approval'),
#            ('print', 'Print'),
#            ('approved', 'Approved'),
#            ('refuse', 'Refused'),
#            ('posted', 'Posted'),
#            ('sent', 'Sent'),
#            ('reconciled', 'Reconciled'),
#            ('cancelled', 'Cancelled')
#        ], readonly=True, default='draft',
#        copy=False, string="Status")
    partner_type = fields.Selection(selection_add=[('partner', 'Partner')])
    reason_note = fields.Text("Reasons for Cancellation/Refusal")

#    @api.multi
#    def action_waiting_approval(self):
#        self.state = 'waiting_approval'

#    @api.multi
#    def action_approved(self):
#        self.state = 'approved'

    # @api.multi
    # def action_print(self):
    #     self.write({'print': True})
    #     data = {'ids': self.env.context.get('active_ids', [])}
    #     res = self.read()
    #     res = res and res[0] or {}
    #     data.update({'form': res})
    #     # return self.env.ref('account.action_report_payment_receipt').report_action(self)
    # return self.env['report'].get_action(self,
    # 'account.action_report_payment_receipt', data=data)

#    @api.multi
#   def action_refused(self):
#      if self.reason_note is False:
#            raise Warning(_('Please write reason for Refusal.'))
#        else:
#            self.write({'state': 'refuse'})
#        return True

    # @api.onchange('partner_type')
    # def _onchange_partner_type(self):
    #     # Set partner_id domain
    #     if self.partner_type:
    #         if self.partner_type in ('supplier', 'customer'):
    #             domain = [(self.partner_type, '=', True)]
    #         else:
    #             domain = [
    #                 ('supplier', '!=', True),
    #                 ('customer', '!=', True)
    #             ]
    #         return {'domain': {'partner_id': domain}}

    # @api.model
    # def _fields_view_get(self, view_id=None, view_type='form', toolbar=False,
    #                      submenu=False):
    #     res = super(AccountPayment, self)._fields_view_get(
    #         view_id, view_type, toolbar, submenu)
    #     from lxml import etree
    #     if view_type == 'form':
    #         doc = etree.XML(res['arch'])
    #         default_payment_type = self.env.context.get('default_payment_type')
    #         for node in doc.xpath("//field[@name='partner_type']"):
    #             if default_payment_type == 'outbound':
    #                 node.set('domain', "[('supplier', '=', True)]")
    #             elif default_payment_type == 'partner':
    #                 node.set(
    #                     "domain",
    #                     "[('supplier','!=',True),('customer','!=',True)]"
    #                 )
    #             else:
    #                 node.set('domain', "[('customer', '=', True)]")
    #     return res

    # @api.multi
    # def action_paymet_advice(self):
    #     '''
    #     This function opens a window to compose an email,
    #     with the edi purchase template message loaded by default
    #     '''
    #     self.ensure_one()
    #     ir_model_data = self.env['ir.model.data']
    #     try:
    #         template_id = ir_model_data.get_object_reference(
    #             'base_finance', 'account_payment_email_template')[1]

    #     except ValueError:
    #         template_id = False
    #     try:
    #         compose_form_id = ir_model_data.get_object_reference(
    #             'mail', 'email_compose_message_wizard_form')[1]
    #     except ValueError:
    #         compose_form_id = False
    #     ctx = dict(self.env.context or {})
    #     ctx.update({
    #         'default_model': 'account.payment',
    #         'default_res_id': self.ids[0],
    #         'default_use_template': bool(template_id),
    #         'default_template_id': template_id,
    #         'default_composition_mode': 'comment',
    #         'custom_layout': "base_finance.mail_template_data_notification_email_account_payment",
    #         'force_email': True
    #     })
    #     return {
    #         'name': _('Compose Email'),
    #         'type': 'ir.actions.act_window',
    #         'view_type': 'form',
    #         'view_mode': 'form',
    #         'res_model': 'mail.compose.message',
    #         'views': [(compose_form_id, 'form')],
    #         'view_id': compose_form_id,
    #         'target': 'new',
    #         'context': ctx,
    #     }


class AccountMoveLine(models.Model):
    _inherit = "account.move.line"

    location_id = fields.Many2one(
        'stock.location', 'Location', domain="[('usage', '=', 'internal')]")


class AccountMove(models.Model):
    _name = 'account.move'
    _inherit = ['account.move', 'mail.thread']

    state = fields.Selection(
        [('draft', 'Unposted'), ('posted', 'Posted')], string='Status',
        required=True, readonly=True, copy=False, default='draft',
        help='All manually created new journal entries are usually in the status \'Unposted\', '
        'but you can set the option to skip that status on the related journal. '
        'In that case, they will behave as journal entries automatically created by the '
        'system on document validation (invoices, bank statements...) and will be created '
        'in \'Posted\' status.', track_visibility="onchange")

    # @api.multi
    # def _post_validate(self):
    #     # Issue was coming in invoice when record is made form sale.
    #     # In Move line company was differ.
    #     for move in self:
    #         if move.line_ids:
    #             # So to make company same, I add this for loop.
    #             for x in move.line_ids:
    #                 x.company_id = move.company_id.id
    #             if not all([x.company_id.id == move.company_id.id for x in move.line_ids]):
    #                 raise UserError(
    #                     _("Cannot create moves for different companies."))
    #     self.assert_balanced()
    #     return self._check_lock_date()


class AccountInvoiceRefund(models.TransientModel):
    _inherit = 'account.invoice.refund'

    # @api.multi
    # def compute_refund(self, mode='refund'):
    #     inv_obj = self.env['account.invoice']
    #     inv_tax_obj = self.env['account.invoice.tax']
    #     inv_line_obj = self.env['account.invoice.line']
    #     context = dict(self._context or {})
    #     xml_id = False

    #     for form in self:
    #         created_inv = []
    #         date = False
    #         description = False
    #         for inv in inv_obj.browse(context.get('active_ids')):
    #             if inv.state in ['draft', 'proforma2', 'cancel']:
    #                 raise UserError(
    #                     _('Cannot refund draft/proforma/cancelled invoice.'))
    #             if inv.reconciled and mode in ('cancel', 'modify'):
    #                 raise UserError(
    #                     _('Cannot refund invoice which is already reconciled, invoice should be unreconciled first. You can only refund this invoice.'))

    #             date = form.date or False
    #             description = form.description or inv.name
    #             refund = inv.refund(form.date_invoice, date,
    #                                 description, inv.journal_id.id)

    #             created_inv.append(refund.id)
    #             if mode in ('cancel', 'modify'):
    #                 movelines = inv.move_id.line_ids
    #                 to_reconcile_ids = {}
    #                 to_reconcile_lines = self.env['account.move.line']
    #                 for line in movelines:
    #                     if line.account_id.id == inv.account_id.id:
    #                         to_reconcile_lines += line
    #                         to_reconcile_ids.setdefault(
    #                             line.account_id.id, []).append(line.id)
    #                     if line.reconciled:
    #                         line.remove_move_reconcile()
    #                 refund.action_invoice_open()
    #                 for tmpline in refund.move_id.line_ids:
    #                     if tmpline.account_id.id == inv.account_id.id:
    #                         to_reconcile_lines += tmpline
    #                 to_reconcile_lines.filtered(
    #                     lambda l: l.reconciled == False).reconcile()
    #                 if mode == 'modify':
    #                     invoice = inv.read(
    #                         inv_obj._get_refund_modify_read_fields())
    #                     invoice = invoice[0]
    #                     del invoice['id']
    #                     invoice_lines = inv_line_obj.browse(
    #                         invoice['invoice_line_ids'])
    #                     invoice_lines = inv_obj.with_context(
    #                         mode='modify')._refund_cleanup_lines(invoice_lines)
    #                     tax_lines = inv_tax_obj.browse(invoice['tax_line_ids'])
    #                     tax_lines = inv_obj._refund_cleanup_lines(tax_lines)
    #                     invoice.update({
    #                         'type': inv.type,
    #                         'date_invoice': form.date_invoice,
    #                         'state': 'draft',
    #                         'number': False,
    #                         'invoice_line_ids': invoice_lines,
    #                         'tax_line_ids': tax_lines,
    #                         'date': date,
    #                         'origin': inv.origin,
    #                         'fiscal_position_id': inv.fiscal_position_id.id,
    #                     })
    #                     for field in inv_obj._get_refund_common_fields():
    #                         if inv_obj._fields[field].type == 'many2one':
    #                             invoice[field] = invoice[
    #                                 field] and invoice[field][0]
    #                         else:
    #                             invoice[field] = invoice[field] or False
    #                     inv_refund = inv_obj.create(invoice)
    #                     if inv_refund.payment_term_id.id:
    #                         inv_refund._onchange_payment_term_date_invoice()
    #                     created_inv.append(inv_refund.id)
    #             xml_id = inv.type == 'out_invoice' and 'account_action_invoice_out_refund' or \
    #                 inv.type == 'out_refund' and 'action_invoice_tree1' or \
    #                 inv.type == 'in_invoice' and 'account_action_invoice_in_refund' or \
    #                 inv.type == 'in_refund' and 'action_invoice_tree2'
    #             # Put the reason in the chatter
    #             subject = _("Invoice refund")
    #             body = description
    #             refund.message_post(body=body, subject=subject)
    #     if xml_id:
    #         result = self.env.ref('base_finance.%s' % (xml_id)).read()[0]
    #         invoice_domain = safe_eval(result['domain'])
    #         invoice_domain.append(('id', 'in', created_inv))
    #         result['domain'] = invoice_domain
    #         return result
    #     return True
