from datetime import date, datetime
from dateutil import relativedelta
import json
import time

from odoo import fields, models
from odoo.tools.float_utils import float_compare, float_round
from odoo.tools.translate import _
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT, DEFAULT_SERVER_DATE_FORMAT
from odoo.exceptions import Warning
from odoo import SUPERUSER_ID, api
import odoo.addons.decimal_precision as dp
# from odoo.addons.procurement import procurement
import logging


class stock_picking(models.Model):
    _inherit="stock.picking"
#     
    def _get_invoice_vals(selfkey, inv_type, journal_id, move):
        sale_no = ' '
        po = ' '
        partner, currency_id, company_id, user_id = key
        sale_no = move.group_id.name
        print"saleee",sale_no
        query="select id from sale_order where name='"+str(move.group_id.name)+"'"
        self.execute(query)
        temp = self.fetchall()
        for val in temp:
            sale_obj = self.env['sale.order'].browse(val)
            po = sale_obj.client_order_ref
        if inv_type in ('out_invoice', 'out_refund'):
            account_id = partner.property_account_receivable.id
            payment_term = partner.property_payment_term.id or False
   
        else:
            account_id = partner.property_account_payable.id
            payment_term = partner.property_supplier_payment_term.id or False
        return {
            'origin': move.picking_id.name,
            'date_invoice': context.get('date_inv', False),
            'user_id': user_id,
            'partner_id': partner.id,
            'account_id': account_id,
            'payment_term': payment_term,
            'type': inv_type,
            'fiscal_position': partner.property_account_position.id,
            'company_id': company_id,
            'currency_id': currency_id,
            'journal_id': journal_id,
            'sale_order':sale_no,
            'x_po_ref':po,
        }
        
    def _invoice_create_line(selfmoves, journal_id, inv_type='out_invoice'):
        invoice_obj = self.env['account.invoice']
        move_obj = self.env['stock.move']
        invoices = {}
        hsn = ' '
        is_extra_move, extra_move_tax = move_obj._get_moves_taxes(moves, inv_type)
        product_price_unit = {}
        for move in moves:
            company = move.company_id
            origin = move.picking_id.name
            partner, user_id, currency_id = move_obj._get_master_data(move, company)

            key = (partner, currency_id, company.id, user_id)
            invoice_vals = self._get_invoice_vals(key, inv_type, journal_id, move)

            if key not in invoices:
                # Get account and payment terms
                invoice_id = self._create_invoice_from_picking(move.picking_id, invoice_vals)
                invoices[key] = invoice_id
            else:
                invoice = invoice_obj.browse(invoices[key])
                if not invoice.origin or invoice_vals['origin'] not in invoice.origin.split(', '):
                    invoice_origin = filter(None, [invoice.origin, invoice_vals['origin']])
                    invoice.write({'origin': ', '.join(invoice_origin)})
            hsn = move.product_id.categ_id.hsc
            
            invoice_line_vals = move_obj._get_invoice_line_vals(move, partner, inv_type)
            invoice_line_vals['invoice_id'] = invoices[key]
            invoice_line_vals['hsn'] = hsn
            invoice_line_vals['origin'] = origin
            if not is_extra_move[move.id]:
                product_price_unit[invoice_line_vals['product_id'], invoice_line_vals['uom_id']] = invoice_line_vals['price_unit']
            if is_extra_move[move.id] and (invoice_line_vals['product_id'], invoice_line_vals['uom_id']) in product_price_unit:
                invoice_line_vals['price_unit'] = product_price_unit[invoice_line_vals['product_id'], invoice_line_vals['uom_id']]
            if is_extra_move[move.id]:
                desc = (inv_type in ('out_invoice', 'out_refund') and move.product_id.product_tmpl_id.description_sale) or \
                    (inv_type in ('in_invoice','in_refund') and move.product_id.product_tmpl_id.description_purchase)
                invoice_line_vals['name'] += ' ' + desc if desc else ''
                if extra_move_tax[move.picking_id, move.product_id]:
                    invoice_line_vals['invoice_line_tax_id'] = extra_move_tax[move.picking_id, move.product_id]
                #the default product taxes
                elif (0, move.product_id) in extra_move_tax:
                    invoice_line_vals['invoice_line_tax_id'] = extra_move_tax[0, move.product_id]

            move_obj._create_invoice_line_from_vals(move, invoice_line_vals)
            move_obj.write(move.id, {'invoice_state': 'invoiced'})

        invoice_obj.button_compute(invoices.values(), set_total=(inv_type in ('in_invoice', 'in_refund')))
        return invoices.values()
