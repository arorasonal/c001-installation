from datetime import datetime, timedelta
import time
from odoo import fields, models
from odoo.tools.translate import _
from odoo.tools import DEFAULT_SERVER_DATE_FORMAT, DEFAULT_SERVER_DATETIME_FORMAT
import odoo.addons.decimal_precision as dp
from odoo import workflow



class sale_order(models.Model):
    _inherit = "sale.order"
    
    def _prepare_invoice(self, order, lines):
        """Prepare the dict of values to create the new invoice for a
           sales order. This method may be overridden to implement custom
           invoice generation (making sure to call super() to establish
           a clean extension chain).

           :param browse_record order: sale.order record to invoice
           :param list(int) line: list of invoice line IDs that must be
                                  attached to the invoice
           :return: dict of value to create() the invoice
        """
        journal_ids = self.env['account.journal'].search(
            [('type', '=', 'sale'), ('company_id', '=', order.company_id.id)],
            limit=1)
        if not journal_ids:
            raise UserError(
                _('Please define sales journal for this company: "%s" (id:%d).') % (order.company_id.name, order.company_id.id))
        invoice_vals = {
            'name': order.client_order_ref or '',
            'origin': order.name,
            'type': 'out_invoice',
            'reference': order.client_order_ref or order.name,
            'account_id': order.partner_invoice_id.property_account_receivable.id,
            'partner_id': order.partner_invoice_id.id,
            'journal_id': journal_ids[0],
            'invoice_line': [(6, 0, lines)],
            'currency_id': order.pricelist_id.currency_id.id,
            'comment': order.note,
            'payment_term': order.payment_term and order.payment_term.id or False,
            'fiscal_position': order.fiscal_position.id or order.partner_invoice_id.property_account_position.id,
            'date_invoice': context.get('date_invoice', False),
            'company_id': order.company_id.id,
            'user_id': order.user_id and order.user_id.id or False,
            'team_id' : order.team_id.id,
            'sale_order':order.name,
        }

        # Care for deprecated _inv_get() hook - FIXME: to be removed after 6.1
        invoice_vals.update(self._inv_get(order))
        return invoice_vals
    def _make_invoice(self, order, lines):
            inv_obj = self.env['account.invoice']
            obj_invoice_line = self.env['account.invoice.line']

            invoiced_sale_line_ids = self.env['sale.order.line'].search([('order_id', '=', order.id), ('invoiced', '=', True)])
            from_line_invoice_ids = []
            for invoiced_sale_line_id in self.env['sale.order.line'].browse(invoiced_sale_line_ids):
                for invoice_line_id in invoiced_sale_line_id.invoice_lines:
                    print"RRRr",invoiced_sale_line_id
                    if invoice_line_id.invoice_id.id not in from_line_invoice_ids:
                        from_line_invoice_ids.append(invoice_line_id.invoice_id.id)
            for preinv in order.invoice_ids:
                if preinv.state not in ('cancel',) and preinv.id not in from_line_invoice_ids:
                    for preline in preinv.invoice_line:
                        inv_line_id = obj_invoice_line.copy(preline.id, {'invoice_id': False, 'price_unit': -preline.price_unit})
                        lines.append(inv_line_id)
            inv = self._prepare_invoice(order, lines)
            inv_id = inv_obj.create(inv)
            data = inv_obj.onchange_payment_term_date_invoice([inv_id], inv['payment_term'], time.strftime(DEFAULT_SERVER_DATE_FORMAT))
            if data.get('value', False):
                inv_obj.write([inv_id], data['value'])
            inv_obj.button_compute([inv_id])
            return inv_id
