#coding: utf-8
from openerp import models, fields, api, _
from openerp import exceptions

class project_task(models.Model):
    _inherit = "project.task"

    @api.multi
    @api.depends("stage_id.default_check_list_ids")
    def _compute_check_list_len(self):
        """
        Compute method for 'check_list_len'
        -----------------------------------
        We calculate how many check lines should be entered
        """
        for task_id in self:
            task_id.check_list_len = task_id.stage_id and len(task_id.stage_id.default_check_list_ids) or 0

    check_list_line_ids = fields.Many2many(
        "check.list",
        "project_task_check_list_rel_table",
        "project_task_id",
        "check_list_id",
        string="Check list",
        help="Confirm that you finished all the points. Otherwise, you would not be able to move the task forward"
    )
    check_list_len = fields.Integer(
        string= "Number of Check Lines (Tech)",
        compute= _compute_check_list_len,
        store= True,
        help="We compute it to interface visibility purposes"
    )

    @api.model
    def create(self, vals):
        """
        CRUD create method inheritance
        ------------------------------
        In case any some checklist lines were confirmed right at the moment of creation:
            * Check for user rigths
        """
        if vals.get("check_list_line_ids"):
            changed_items = vals.get("check_list_line_ids")[0][2]
            for item in self.env["check.list"].browse(changed_items).filtered(lambda line: len(line.group_ids) > 0):
                if len(set(self.env.user.groups_id.ids) & set(item.group_ids.ids)) == 0:
                    raise exceptions.Warning(_(u"""Sorry, but you don't have rights to confirm/disapprove '{0}'!
Contact your system administator for assistance.""".format(item.name)))
        return super(project_task, self).create(vals)

    @api.multi
    def write(self,vals):
        """
        CRUD write method inheritance
        -----------------------------
        In case of check_list_line_ids write:
            * We find what check lines were confirmed / unconfirmed (both actions require user rights)
            * We check for user rigths

        In case of stage_id write:
            * We check whether new stage is further than old one (by sequence)
            * We check that new stage is not forced as no check list required (e.g. for 'Canceled')
            * If so, check list should be entered (its len should be equal stage_id default check lines len)
            * Clean previous check list thereafter
        """
        if vals.get("check_list_line_ids"):
            new_check_line_ids = vals.get("check_list_line_ids")[0][2]
            for task_id in self:
                old_check_line_ids = task_id.check_list_line_ids.ids
                changed_items = list((set(new_check_line_ids) - set(old_check_line_ids)) | \
                    (set(old_check_line_ids) - set(new_check_line_ids)))
                for item in self.env["check.list"].browse(changed_items).filtered(lambda line: len(line.group_ids) > 0):
                    if len(set(self.env.user.groups_id.ids) & set(item.group_ids.ids)) == 0:
                        raise exceptions.Warning(_(u"""Sorry, but you don't have rights to confirm/disapprove '{0}'!
Contact your system administator for assistance.""".format(item.name)))

        if vals.get("stage_id"):
            new_stage_id = self.env["project.task.type"].browse(vals.get("stage_id"))
            for task_id in self:
                if new_stage_id.sequence >= task_id.stage_id.sequence and not new_stage_id.no_need_for_checklist:
                    entered_len = vals.get("check_list_line_ids") and len(vals.get("check_list_line_ids")) or \
                                    len(task_id.check_list_line_ids)
                    required_len = vals.get("check_list_len") and vals.get("check_list_len") or task_id.check_list_len
                    if entered_len != required_len:
                        raise exceptions.Warning(_(u"""Please enter check list for the task '{0}'!
You can't move this task forward until you confirm all jobs have been done.""".format(task_id.name)))

            vals.update({"check_list_line_ids": [(5,)]})

        res = super(project_task, self).write(vals)
        return res