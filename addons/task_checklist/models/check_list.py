#coding: utf-8
from openerp import models, fields, api, _

class check_list(models.Model):
    _name = "check.list"

    name = fields.Char(string="What should be done on this stage", required=True, )
    project_task_type_id = fields.Many2one(
        "project.task.type",
        string="Task Type",
        required=True,
    )
    group_ids = fields.Many2many(
        "res.groups",
        "res_groups_check_list_rel_table",
        "res_groups_id",
        "check_list_id",
        string="User groups",
        help="Leave it empty if any user may confirm this checklist item,"
    )
    sequence = fields.Integer(string="Sequence")

    _order = "sequence, id"