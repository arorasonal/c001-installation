# -*- coding: utf-8 -*-
{
    "name": "Task Check List and Approval Process",
    "version": "10.0.1.0.1",
    "category": "Project",
    "author": "faOtools",
    "website": "https://faotools.com/apps/10.0/task-check-list-and-approval-process-251",
    "license": "Other proprietary",
    "application": True,
    "installable": True,
    "auto_install": False,
    "depends": [
        "project"
    ],
    "data": [
        "views/project_task_type.xml",
        "views/project_task.xml",
        "data/data.xml",
        "security/ir.model.access.csv"
    ],
    "qweb": [
        
    ],
    "js": [
        
    ],
    "demo": [
        
    ],
    "external_dependencies": {},
    "summary": "The tool to make sure required jobs are carefully done on this task stage",
    "description": """

For the full details look at static/description/index.html

* Features * 

- Check lists are assigned per each task stage. Set of check list points is up to you

- As soon as a task is moved to a certain stage, its check list is updated to a topical one from this stage. For example, actions for 'draft' and 'in progress' stages should be different, shouldn't they?

- To move a task forward through your project pipeline, a check list should be fully confirmed. By 'moving forward' any change of stage with lesser sequence to a bigger sequence is implied

- Confirmation might assume involvement of different user roles. Certain check list points might be approved only by top-level employees. For example, cost calculations are confirmed by Financial managers. In such a case just assign a user group for this check list item 

- For certain situations you do not need a check list fulfilment even a new stage is further. For example, for the 'Cancelled' stage. In such a case just mark this stage as 'No need for checklist'
 
* Extra Notes *



#odootools_proprietary

    """,
    "images": [
        "static/description/main.png"
    ],
    "price": "23.0",
    "currency": "EUR",
}