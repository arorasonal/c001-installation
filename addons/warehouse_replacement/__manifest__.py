{
    "name": "Warehouse Replacement",
    "version": "1.0",
    'website': 'http://www.diracerp.in',
    'images': [ ],
    "depends": ['web','base','product','stock','warehouse_extension'],
    "author": "Dirac ERP",
    "category": "Stock",
#     'sequence': 16,
    "description": """
    This module for the warehouse Replacement. 
    """,
    'data': [
        'security/ir.model.access.csv',    
        'view/stock_picking_type_view.xml',
        'security/security_view.xml',
        'view/res_partner_view.xml',
        'view/stock_location_view.xml',
        'view/account_invoice_view.xml',
        'view/sale_order_view.xml',
        'view/account_staff_update_view.xml',
        'view/master_creditnote_reason.xml',
        'wizard/account_invoice_refund_view.xml'

#
                    ],
    'demo_xml': [],
     'js': [
            'static/src/js/view_list.js'
#            'static/src/js/view_list.js'
#     'static/src/js/subst.js'
    ],
    'css': [
#             'static/src/css/sample_kanban.css'
            ],
#     'pre_init_hook': 'pre_init_hook',
#     'post_init_hook': 'post_init_hook',
    'installable': True,
    'active': False,
    'auto_install': False,
#    'certificate': 'certificate',
}
