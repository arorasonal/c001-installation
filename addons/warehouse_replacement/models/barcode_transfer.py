import logging

from odoo import api, fields, models
from odoo import tools, _
import time
import math
from time import gmtime, strftime
import odoo.addons.decimal_precision as dp
from datetime import datetime,timedelta,date
from odoo.tools import DEFAULT_SERVER_DATE_FORMAT,DEFAULT_SERVER_DATETIME_FORMAT
from odoo.exceptions import UserError, ValidationError
from odoo.modules.module import get_module_resource
class barcode_transfer(models.Model):
    _inherit="barcode.transfer"
    
    
    
    @api.onchange('picking_type_id')
    def _onchange_picking_type_id(self):
        result={}
        source_location_list = []
        destination_location_list = []
        source_location_id = ''
        destination_location_id = ''
        transaction = []
        if self.picking_type_id:
            if self.picking_type_id.transaction_type_id:
                for val in self.picking_type_id.transaction_type_id:
                    transaction.append(val.id)
            print"Fbfgbnfgknkfgb",transaction
            challan = self.picking_type_id.challan_price_from
            source_location_list.append(self.picking_type_id.default_location_src_id.id)
            destination_location_list.append(self.picking_type_id.default_location_dest_id.id)
            source_location_id = self.picking_type_id.default_location_src_id.id
            destination_location_id = self.picking_type_id.default_location_dest_id.id
            result.update({'value':{'source_location_id':source_location_id,
                                    'so_required_in_delivery':self.picking_type_id.so_required_in_delivery,
                                    'destination_location_id':destination_location_id,
                                    'challan_price_from':challan,
                                    },
                           'domain':{'transaction_type_id':[('id','in',transaction)]}
                                  
                               
                               })
        return result 
    
    
    
    def action_mark(self):
        product_list = []
        prioduct_list1 = []
        barcode_list = []
        tax = []
        tax1 = []
        product_ids = {}
        location_name = ''
        user_list = []
        partner_obj = ''
        location_id = ''
        origin_location = ''
        template_loc_id = ''
        if self.state =="draft":
            if self.picking_type_id.create_stock_location == True:
                if self.order_id.partner_id.location_id:
                    self.write({'destination_location_id':self.order_id.partner_id.location_id.id})
                else:
                    if self.order_id.partner_id.is_company == True:
                        partner_obj = self.env['res.partner'].browse(self.order_id.partner_id.id)
                        location =  self.env['stock.location'].search([('name','=',self.order_id.partner_id.name)])
                        if location:
                            for val in location:
                                location_id = val
                        else:
                            location_name = self.order_id.partner_id.name
                    # elif self.order_id.partner_id.is_company == False:
                    #     partner_obj = self.env['res.partner'].browse(self.order_i)

		        # else:
                #     if self.order_id.partner_id.is_company == True:
                #         partner_obj  = self.env['res.partner'].browse(self.order_id.partner_id.id)
                #         location = self.env['stock.location'].search([('name','=',self.order_id.partner_id.name)])
                #         if location:
                #             for val in location:
                #                 location_id = val
                #
                #         else:
                #             location_name = self.order_id.partner_id.name
                    elif self.order_id.partner_id.is_company == False:
                        partner_obj = self.env['res.partner'].browse(self.order_id.partner_id.parent_id.id)
                        location = self.env['stock.location'].search([('name','=',self.order_id.partner_id.parent_id.name)])
                        if location:
                            for val in location:
                                location_id = val
                        else:
                            location_name = self.order_id.partner_id.parent_id.name
                    else:
                        raise UserError(_('Customer is not Company Type.'))
                    if location_name:
                        template_loc = self.env['stock.location'].search([('template','=',True)])
                        if template_loc:
                            for x in template_loc:
                                template_loc_id = x.id
                        origin_location = self.env['stock.location'].browse(template_loc_id)
                        if origin_location.user_ids:
                            for user in origin_location.user_ids:
                                user_list.append(user.id)
                    location_id = self.env['stock.location'].create({'name':location_name,'wr_id':origin_location.wr_id.id,
                                                                 'location_id':origin_location.location_id.id,
                                                                 'usage':origin_location.usage,
                                                                 'location_function_type_id':origin_location.location_function_type_id.id,
                                                                 'allow_negative_stock':origin_location.allow_negative_stock,
                                                                 'company_id':origin_location.company_id.id,
                                                                'user_ids':[(6,0,user_list)]})
                    location_obj = self.env['stock.location'].browse(location_id.id)
                    location_obj.write({'customer_id':partner_obj.id})
            	    partner_obj.write({'location_id':location_id.id})
            	    self.write({'destination_location_id':location_id.id})
            for each in self:
                if each.barcode_serial_lines:
                    if each.order_id:
                        for val in each.barcode_serial_lines:
                            if not val.order_line_id:
                                raise UserError(_('Please fill the SO Product First.'))
                    for val in each.barcode_serial_lines:
                        if product_ids:
                            for key,value,in product_ids.items():
                                if key == val.product_id.id:
                                    if value != val.rate:
                                        raise UserError(_('There is different price for the product %s')%(val.product_id.name))
                                    else:
                                        product_ids[val.product_id.id] = val.rate
                                else:
                                    product_ids[val.product_id.id] = val.rate
                        else:
                            product_ids[val.product_id.id] = val.rate
                if each.picking_type_id.challan_price_from == 'so':
                    if each.barcode_serial_lines:
                        for val in each.barcode_serial_lines:
                            if not val.order_line_id:
                                val.rate = 0.0
                                val.tax_id = [(6,0,tax)]
                if each.tax_type == 'local':
                    if each.barcode_serial_lines:
                        for val in each.barcode_serial_lines:
                            if val.tax_id:
                                tax1 = []
                                for each44 in val.tax_id:
                                    if each44.local_central == 'local':
                                        tax1.append(each44.id)
                                    else:
                                        tax1.append(each44.local_tax_id.id)
                                val.tax_id = [(6,0,tax1)]
                elif each.tax_type == 'central':
                    if each.barcode_serial_lines:
                        for val in each.barcode_serial_lines:
                            if val.tax_id:
                                tax1 = []
                                for each44 in val.tax_id:
                                    if each44.local_central == 'central':
                                        tax1.append(each44.id)
                                    else:
                                        tax1.append(each44.central_tax_id.id)
                                val.tax_id = [(6,0,tax1)]
                elif each.tax_type == 'sez':
                    if each.barcode_serial_lines:
                        for val in each.barcode_serial_lines:
                            if val.tax_id:
                                tax1 = []
                                for each44 in val.tax_id:
                                    tax1.append(each44.sez_tax_id.id)
                                val.tax_id = [(6,0,tax1)]
                else:
                    raise UserError(_('Please select the Local/Central Tax first.'))
            if self.barcode_serial_lines:
                for each in self.barcode_serial_lines:
                    if each.product_id.tracking != 'none':
                        if each.location_id.id != self.source_location_id.id:
                            raise UserError(_('The Source Location and Current Location is not matched for the product %s.') %
                                                    (each.product_id.name))
            if self.barcode_transfer_lines:
                for val in self.barcode_transfer_lines:
                    val.unlink()
            if self.barcode_serial_lines:
                for val1 in self.barcode_serial_lines:
                    if val1.barcode or val1.serial_id:
                        serial = self.env['barcode.serial.line'].search([('barcode','=',val1.barcode),('serial_id','=',val1.serial_id.id),('barcode_transfer_id','=',self.id)])
                        if serial:
                            if len(serial) > 1:
                               raise UserError(_('Barcode "%s" or Serial %s  is used in more than one line.') %
                                                    (val1.barcode,val1.serial_id.name))
            if self.barcode_serial_lines:
                for val in self.barcode_serial_lines:
                    product_list.append(val.product_id.id)
                if product_list:
                    product_list1 = list(set(product_list))
                if product_list1:
                    for val1 in product_list1:
                        product_obj = self.env['product.product'].browse(val1)
                        qty = 0.0
                        rate = 0.0
                        tax = []
                        serial = self.env['barcode.serial.line'].search([('barcode_transfer_id','=',self.id),('product_id','=',val1)])
                        if serial:
                            for val in serial:
                                if val.tax_id:
                                    qty = qty + val.qty
                                    rate = val.rate
                                    for each43 in val.tax_id:
                                        tax.append(each43.id)
                                else:
                                    qty = qty + val.qty
                                    rate = val.rate
                        p = self.env['barcode.transfer.line'].create({'barcode_transfer_id':self.id,'product_id':val1,'qty':qty,'rate':rate,
                                                                  'tax_id':[(6,0,tax)]})
                        if not p:
                            raise UserError(_('Barcode Transfer line for the product %s not created.')%(product_obj.name))

                        else:
                            serial1 = self.env['barcode.serial.line'].search([('barcode_transfer_id','=',self.id),('product_id','=',val1)])
                            if serial1:
                                for val45 in serial1:
                                    val45.write({'transfer_line_id':p.id})
            if self.barcode_serial_lines:
                for each in self.barcode_serial_lines:
                    each.write({'state':'validate'})
            self.write({'state':'validate'})

        account_staff_id = fields.Many2one('res.users','Account Staff')


