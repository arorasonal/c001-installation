from odoo import fields, models, api
from odoo.tools.translate import _
from datetime import datetime, timedelta
from odoo import tools
import time
# import os,re


class account_staff_update(models.TransientModel):
    _name = "account.staff.update"
    _inherit = ['mail.thread', 'ir.needaction_mixin']

    code = fields.Boolean('Code', track_visibility="onchange")
    user_id = fields.Many2one(
        'res.users', string='Account Staff', track_visibility="onchange")

    partner_ids = fields.Many2many('res.partner', 'mass_field1_rel',
                                 'mass1_id1', 'field1_id', 'Fields', track_visibility="onchange")



    @api.multi
    def update_account_staff(self):
        lines = self.partner_ids
        for val1 in lines:
            if val1.customer:
                sale = self.env['sale.order'].search(
                    [('partner_id', '=', val1.id), ('state', 'in', ('draft', 'sent'))])
                # opp = self.env['crm.lead'].search(
                #     [('partner_id', '=', val1.id), ('probability', 'not in', (0, 100))])
                contract = self.env['account.analytic.account'].search(
                    [('partner_id', '=', val1.id), ('state', 'not in', ('close', 'cancelled'))])
                account = self.env['account.invoice'].search(
                    [('partner_id', '=', val1.id), ('type', '=', 'out_invoice'), ('state', '=', 'draft')])
                if sale:
                    for each in sale:
                        each.write({'account_staff_id': self.user_id.id})
                # if opp:
                #     for each in opp:
                #         each.write({'account_staff_id': self.user_id.id})
                if contract:
                    for each in contract:
                        each.write({'account_staff_id': self.user_id.id})
                if account:
                    for each in account:
                        each.write({'account_staff_id': self.user_id.id,
                            })
                val1.write({'account_staff_id': self.user_id.id,
                            })
            self.write({'code': True})
        return True
