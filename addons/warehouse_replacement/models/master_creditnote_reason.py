# -*- coding: utf-8 -*-

from odoo import models, fields, api
from datetime import datetime, timedelta
from odoo.exceptions import ValidationError,UserError
from datetime import date

class CreditNoteReason(models.Model):
    _name = 'credit.note.reason'
    _rec_name = "name"

    name = fields.Char(string='Name', required="1")
    active = fields.Boolean(default=True)