# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from collections import namedtuple
import json
import time

from odoo import api, fields, models, _
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT
from odoo.tools.float_utils import float_compare
from odoo.addons.procurement.models import procurement
from odoo.exceptions import UserError

class PickingType(models.Model):
    _inherit = "stock.picking.type"
    
    @api.onchange('so_required_in_delivery')
    def _onchange_so_required_in_delivery(self):
        if self.so_required_in_delivery == False:
            self.create_stock_location = False
    
    transaction_type_id = fields.Many2many('transaction.type',track_visibility="onchange")
    create_stock_location = fields.Boolean('Create Stock Location',track_visibility="onchange")
    party_changed = fields.Boolean('Party Can be Changed',track_visibility="onchange")
