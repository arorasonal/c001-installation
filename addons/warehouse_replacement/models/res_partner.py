import base64
import datetime
import hashlib
import pytz
import threading
import urllib2
import urlparse

from email.utils import formataddr
from lxml import etree

from odoo import api, fields, models, tools, SUPERUSER_ID, _
from odoo.modules import get_module_resource
from odoo.osv.expression import get_unaccent_wrapper
from odoo.exceptions import UserError, ValidationError
from odoo.osv.orm import browse_record

ADDRESS_FORMAT_CLASSES = {
    '%(city)s %(state_code)s\n%(zip)s': 'o_city_state',
    '%(zip)s %(city)s': 'o_zip_city'
}
class FormatAddress(object):
    @api.model
    def fields_view_get_address(self, arch):
        address_format = self.env.user.company_id.country_id.address_format or ''
        for format_pattern, format_class in ADDRESS_FORMAT_CLASSES.iteritems():
            if format_pattern in address_format:
                doc = etree.fromstring(arch)
                for address_node in doc.xpath("//div[@class='o_address_format']"):
                    # add address format class to address block
                    address_node.attrib['class'] += ' ' + format_class
                    if format_class.startswith('o_zip'):
                        zip_fields = address_node.xpath("//field[@name='zip']")
                        city_fields = address_node.xpath("//field[@name='city']")
                        if zip_fields and city_fields:
                            # move zip field before city field
                            city_fields[0].addprevious(zip_fields[0])
                arch = etree.tostring(doc)
                break
        return arch
    
class Partner(models.Model, FormatAddress):
    _inherit = "res.partner"

    def action_location_count(self):
        for each in self:
            each.location_count = len(self.env['stock.location'].search(
                [('customer_id', '=', each.id)]))
    
    location_id = fields.Many2one('stock.location','Location')
    sez = fields.Boolean('SEZ')
    location_count = fields.Integer('Locations', compute='action_location_count')
    account_staff_id = fields.Many2one('res.users','Account Staff')
