# -*- coding: utf-8 -*-

import json
from lxml import etree
from datetime import datetime
from dateutil.relativedelta import relativedelta

from odoo import api, fields, models, _
from odoo.tools import float_is_zero, float_compare
from odoo.tools.misc import formatLang

from odoo.exceptions import UserError, RedirectWarning, ValidationError

import odoo.addons.decimal_precision as dp
import logging

_logger = logging.getLogger(__name__)

# mapping invoice type to journal type
TYPE2JOURNAL = {
    'out_invoice': 'sale',
    'in_invoice': 'purchase',
    'out_refund': 'sale',
    'in_refund': 'purchase',
}

# mapping invoice type to refund type
TYPE2REFUND = {
    'out_invoice': 'out_refund',        # Customer Invoice
    'in_invoice': 'in_refund',          # Vendor Bill
    'out_refund': 'out_invoice',        # Customer Refund
    'in_refund': 'in_invoice',          # Vendor Refund
}

MAGIC_COLUMNS = ('id', 'create_uid', 'create_date', 'write_uid', 'write_date')

class AccountInvoice(models.Model):
    _inherit = "account.invoice"

    cn_reason = fields.Many2one('credit.note.reason', 'Reason')

    @api.model
    def create(self, vals):
        onchanges = {
            '_onchange_partner_id': ['account_id', 'payment_term_id', 'fiscal_position_id', 'partner_bank_id'],
            '_onchange_journal_id': ['currency_id'],
        }
        for onchange_method, changed_fields in onchanges.items():
            if any(f not in vals for f in changed_fields):
                invoice = self.new(vals)
                getattr(invoice, onchange_method)()
                for field in changed_fields:
                    if field not in vals and invoice[field]:
                        vals[field] = invoice._fields[field].convert_to_write(invoice[field], invoice)
        if not vals.get('account_id', False):
            raise UserError(_(
                'Configuration error!\nCould not find any account to create the invoice, are you sure you have a chart of account installed?'))

        invoice = super(AccountInvoice, self.with_context(mail_create_nolog=True)).create(vals)

        if any(line.invoice_line_tax_ids for line in invoice.invoice_line_ids) and not invoice.tax_line_ids:
            invoice.compute_taxes()
       # if invoice.partner_invoice_id.sez == True:
        #    if invoice.tax_type == 'sez':
         #       print"fvdfvdf"
          #  else:
           #     raise UserError(_('Please fill the Correct Tax'))
        #elif invoice.partner_invoice_id.sez == False:
         #   if invoice.tax_type == 'sez':
          #      raise UserError(_('Please fill the Correct Tax'))

        return invoice

    @api.multi
    def _write(self, vals):
        for each in self:
            pre_not_reconciled = self.filtered(lambda invoice: not invoice.reconciled)
            pre_reconciled = self - pre_not_reconciled
            res = super(AccountInvoice, self)._write(vals)
            account_obj =  self.env['account.invoice'].browse(each.id)
            reconciled = self.filtered(lambda invoice: invoice.reconciled)
            not_reconciled = self - reconciled
            (reconciled & pre_reconciled).filtered(lambda invoice: invoice.state == 'open').action_invoice_paid()
            (not_reconciled & pre_not_reconciled).filtered(lambda invoice: invoice.state == 'paid').action_invoice_re_open()
           # if account_obj.partner_invoice_id.sez == True:
            #    if account_obj.tax_type == 'sez':
             #       print"fvdfvdf"
               # else:
              #      raise UserError(_('Please fill the Correct Tax'))
            #elif account_obj.partner_invoice_id.sez == False:
             #   if account_obj.tax_type == 'sez':
              #      raise UserError(_('Please fill the Correct Tax'))
            return res


    def action_tax(self):
        for inv in self:
            print"gbfgnkbnfgkbnkfgnbknfgkb",inv.tax_type
            tax1 = []
            # if inv.tax_type == 'local':
            #     if inv.invoice_line_ids:
            #         for val in inv.invoice_line_ids:
            #             if val.invoice_line_tax_ids:
            #                 tax1 = []
            #                 for val1 in val.invoice_line_tax_ids:
            #                     if val1.local_central == 'local':
            #                         tax1.append(val1.id)
            #                     else:
            #                         tax1.append(val1.local_tax_id.id)
            #             val.invoice_line_tax_ids = [(6, 0, tax1)]
            # elif inv.tax_type == 'central':
            #     if inv.invoice_line_ids:
            #         for val in inv.invoice_line_ids:
            #             if val.invoice_line_tax_ids:
            #                 tax1 = []
            #                 for val1 in val.invoice_line_tax_ids:
            #                     if val1.local_central == 'central':
            #                         tax1.append(val1.id)
            #                     else:
            #                         tax1.append(val1.central_tax_id.id)
            #             val.invoice_line_tax_ids = [(6, 0, tax1)]
            if inv.tax_type == 'sez':
                if inv.invoice_line_ids:
                    for val in inv.invoice_line_ids:
                        if val.invoice_line_tax_ids:
                            tax1 = []
                            for val1 in val.invoice_line_tax_ids:
                                tax1.append(val1.sez_tax_id.id)
                        val.invoice_line_tax_ids = [(6,0,tax1)]
            elif inv.journal_id.bill_from.state_id == inv.partner_invoice_id.state_id:
		inv.tax_type = 'local'
                if inv.invoice_line_ids:
                        for val in inv.invoice_line_ids:
                            if val.invoice_line_tax_ids:
                                tax1 = []
                                for val1 in val.invoice_line_tax_ids:
                                    if val1.local_central == 'local':
                                        tax1.append(val1.id)
                                    else:
                                        tax1.append(val1.local_tax_id.id)
                            val.invoice_line_tax_ids = [(6,0,tax1)]
            else:
		inv.tax_type = 'central'
                if inv.invoice_line_ids:
                    for val in inv.invoice_line_ids:
                        if val.invoice_line_tax_ids:
                            tax1 = []
                            for val1 in val.invoice_line_tax_ids:
                                if val1.local_central == 'central':
                                    tax1.append(val1.id)
                                else:
                                    tax1.append(val1.central_tax_id.id)
                        val.invoice_line_tax_ids = [(6, 0, tax1)]
            # else:
            #     raise UserError(_('Please select the Local/Central Tax first.'))
	    inv.compute_taxes()
            # if inv.journal_id.bill_from.state_id == inv.partner_invoice_id.state_id:
            #     if inv.invoice_line_ids:
            #         for val in inv.invoice_line_ids:
            #             if val.invoice_line_tax_ids:
            #                 tax1 = []
            #                 for val1 in val.invoice_line_tax_ids:
            #                     if val1.local_central == 'local':
            #                         tax1.append(val1.id)
            #                     else:
            #                         tax1.append(val1.local_tax_id.id)
            #             val.invoice_line_tax_ids = [(6,0,tax1)]
            # else:
            #     if inv.invoice_line_ids:
            #         for val in inv.invoice_line_ids:
            #             if val.invoice_line_tax_ids:
            #                 tax1 = []
            #                 for val1 in val.invoice_line_tax_ids:
            #                     if val1.local_central == 'central':
            #                         tax1.append(val1.id)
            #                     else:
            #                         tax1.append(val1.central_tax_id.id)
            #             val.invoice_line_tax_ids = [(6, 0, tax1)]



    @api.multi
    def action_invoice_open(self):
        # lots of duplicate calls to action_invoice_open, so we remove those already open
        to_open_invoices = self.filtered(lambda inv: inv.state != 'open')
        if to_open_invoices.filtered(lambda inv: inv.state not in ['proforma2', 'draft']):
            raise UserError(_("Invoice must be in draft or Pro-forma state in order to validate it."))
        to_open_invoices.action_check_tax()
        to_open_invoices.action_date_assign()
        to_open_invoices.action_move_create()
        return to_open_invoices.invoice_validate()


    def action_check_tax(self):
        for inv in self:
            if inv.partner_invoice_id.sez == True:
                if inv.invoice_line_ids:
                    for val in inv.invoice_line_ids:
                        if val.invoice_line_tax_ids:
                            for val1 in val.invoice_line_tax_ids:
                                if val1.sez == False:
                                    raise UserError(_("Tax must be of SEZ Type."))
            else:
                if inv.journal_id.bill_from.state_id == inv.partner_invoice_id.state_id:
                    if inv.invoice_line_ids:
                        for val2 in inv.invoice_line_ids:
                            if val2.invoice_line_tax_ids:
                                for val3 in val2.invoice_line_tax_ids:
                                    if val3.local_central == 'central':
                                        raise UserError(_("Tax must be of Local Type."))

                else:
                    if inv.invoice_line_ids:
                        for val2 in inv.invoice_line_ids:
                            if val2.invoice_line_tax_ids:
                                for val3 in val2.invoice_line_tax_ids:
                                    if val3.local_central == 'local':
                                        raise UserError(_("Tax must be of Central Type."))

    def _get_refund_modify_read_fields(self):
        read_fields = ['type', 'number', 'invoice_line_ids', 'tax_line_ids', 'date','partner_invoice_id']
        return self._get_refund_common_fields() + self._get_refund_prepare_fields() + read_fields

    @api.multi
    @api.returns('self')
    def refund(self, date_invoice=None, date=None, description=None, journal_id=None,partner_invoice_id = None, cn_reason=None):
        new_invoices = self.browse()
        for invoice in self:
            # create the new invoice
            values = self._prepare_refund(invoice, date_invoice=date_invoice, date=date,
                                          description=description, journal_id=journal_id,partner_invoice_id = partner_invoice_id, cn_reason=cn_reason)
            refund_invoice = self.create(values)
            invoice_type = {'out_invoice': ('customer invoices refund'),
                            'in_invoice': ('vendor bill refund')}
            message = _(
                "This %s has been created from: <a href=# data-oe-model=account.invoice data-oe-id=%d>%s</a>") % (
                      invoice_type[invoice.type], invoice.id, invoice.number)
            refund_invoice.message_post(body=message)
            new_invoices += refund_invoice
        return new_invoices

    @api.model
    def _prepare_refund(self, invoice, date_invoice=None, date=None, description=None, journal_id=None,partner_invoice_id = None, cn_reason = None):
        """ Prepare the dict of values to create the new refund from the invoice.
            This method may be overridden to implement custom
            refund generation (making sure to call super() to establish
            a clean extension chain).

            :param record invoice: invoice to refund
            :param string date_invoice: refund creation date from the wizard
            :param integer date: force date from the wizard
            :param string description: description of the refund from the wizard
            :param integer journal_id: account.journal from the wizard
            :return: dict of value to create() the refund
        """
        values = {}
        for field in self._get_refund_copy_fields():
            if invoice._fields[field].type == 'many2one':
                values[field] = invoice[field].id
            else:
                values[field] = invoice[field] or False

        values['invoice_line_ids'] = self._refund_cleanup_lines(invoice.invoice_line_ids)

        tax_lines = invoice.tax_line_ids
        taxes_to_change = {
            line.tax_id.id: line.tax_id.refund_account_id.id
            for line in tax_lines.filtered(lambda l: l.tax_id.refund_account_id != l.tax_id.account_id)
        }
        cleaned_tax_lines = self._refund_cleanup_lines(tax_lines)
        values['tax_line_ids'] = self._refund_tax_lines_account_change(cleaned_tax_lines, taxes_to_change)

        if journal_id:
            journal = self.env['account.journal'].browse(journal_id)
        elif invoice['type'] == 'in_invoice':
            journal = self.env['account.journal'].search([('type', '=', 'purchase')], limit=1)
        else:
            journal = self.env['account.journal'].search([('type', '=', 'sale')], limit=1)
        values['journal_id'] = journal.id

        values['type'] = TYPE2REFUND[invoice['type']]
        values['date_invoice'] = date_invoice or fields.Date.context_today(invoice)
        if values.get('date_due', False) and values['date_invoice'] > values['date_due']:
            values['date_due'] = values['date_invoice']
        values['state'] = 'draft'
        values['number'] = False
        values['origin'] = invoice.number
        values['payment_term_id'] = False
        values['refund_invoice_id'] = invoice.id
        values['sale_type_id'] = invoice.sale_type_id.id
        if cn_reason:
            values['cn_reason'] = cn_reason
        if partner_invoice_id:
            partner_invoice = self.env['res.partner'].browse(partner_invoice_id)
            values['partner_invoice_id'] =  partner_invoice.id

        if date:
            values['date'] = date
        if description:
            values['name'] = description
        return values

    @api.model
    def _refund_tax_lines_account_change(self, lines, taxes_to_change):
        # Let's change the account on tax lines when
        # @param {list} lines: a list of orm commands
        # @param {dict} taxes_to_change
        #   key: tax ID, value: refund account

        if not taxes_to_change:
            return lines

        for line in lines:
            if isinstance(line[2], dict) and line[2]['tax_id'] in taxes_to_change:
                line[2]['account_id'] = taxes_to_change[line[2]['tax_id']]
        return lines

    @api.onchange('partner_id', 'company_id')
    def _onchange_partner_id(self):
        account_id = False
        payment_term_id = False
        fiscal_position = False
        bank_id = False
        warning = {}
        domain = {}
        company_id = self.company_id.id
        self.partner_gst = self.partner_id.gstin
        p = self.partner_id if not company_id else self.partner_id.with_context(force_company=company_id)
        type = self.type
        if p:
            rec_account = p.property_account_receivable_id
            pay_account = p.property_account_payable_id
            if not rec_account and not pay_account:
                action = self.env.ref('account.action_account_config')
                msg = _(
                    'Cannot find a chart of accounts for this company, You should configure it. \nPlease go to Account Configuration.')
                raise RedirectWarning(msg, action.id, _('Go to the configuration panel'))

            if type in ('in_invoice', 'in_refund'):
                account_id = pay_account.id
                payment_term_id = p.property_supplier_payment_term_id.id
            else:
                account_id = rec_account.id
                payment_term_id = p.property_payment_term_id.id

            delivery_partner_id = self.get_delivery_partner_id()
            fiscal_position = self.env['account.fiscal.position'].get_fiscal_position(self.partner_id.id,
                                                                                      delivery_id=delivery_partner_id)

            # If partner has no warning, check its company
            if p.invoice_warn == 'no-message' and p.parent_id:
                p = p.parent_id
            if p.invoice_warn != 'no-message':
                # Block if partner only has warning but parent company is blocked
                if p.invoice_warn != 'block' and p.parent_id and p.parent_id.invoice_warn == 'block':
                    p = p.parent_id
                warning = {
                    'title': _("Warning for %s") % p.name,
                    'message': p.invoice_warn_msg
                }
                if p.invoice_warn == 'block':
                    self.partner_id = False


        self.account_id = account_id
        self.payment_term_id = payment_term_id
        self.date_due = False
        self.fiscal_position_id = fiscal_position
        self.account_staff_id = self.partner_id.account_staff_id.id

        if type in ('in_invoice', 'out_refund'):
            bank_ids = p.commercial_partner_id.bank_ids
            bank_id = bank_ids[0].id if bank_ids else False
            self.partner_bank_id = bank_id
            domain = {'partner_bank_id': [('id', 'in', bank_ids.ids)]}

        res = {}
        if warning:
            res['warning'] = warning
        if domain:
            res['domain'] = domain
        return res

    @api.onchange('tax_type')
    def _onchange_tax_type(self):
        for each in self:
            if self.tax_type == 'sez':
                self.customer_sez = True
            elif self.tax_type == 'local':
                self.customer_sez = False
            elif self.tax_type == 'central':
                self.customer_sez = False

    @api.onchange('partner_invoice_id')
    def _onchange_partner_invoice_id(self):
        self.invoice_gst = self.partner_invoice_id.gstin
        if self.partner_invoice_id.sez == True:
            self.tax_type = 'sez'
            self.customer_sez = True


    partner_gst = fields.Char('Customer GST No',track_visibility="onchange")
    customer_sez = fields.Boolean('Customer SEZ',track_visibility="onchange")
    account_staff_id = fields.Many2one('res.users','Account Staff',track_visibility="onchange")
    tax_type = fields.Selection([('local','Local'),('central','Central'),('sez','SEZ')],'Local/Central Tax',default="local",track_visibility="onchange")










