# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from collections import namedtuple
import json
import time
from datetime import datetime
from odoo import api, fields, models, _
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT
from odoo.tools import DEFAULT_SERVER_DATE_FORMAT
from odoo.tools.float_utils import float_compare
from odoo.addons.procurement.models import procurement
from odoo.exceptions import UserError
from datetime import date


class Location(models.Model):
    _inherit = "stock.location"

    @api.model
    def create(self, vals):
        stock_location_id = super(Location, self).create(vals)
        location_obj = self.env['stock.location'].browse(stock_location_id.id)
        if location_obj.template == True:
            location = self.env['stock.location'].search([('template','=',True)])
            if location:
                for x in location:
                    raise UserError(_('Template is already tick for location %s.')%(x.name))
        return stock_location_id

    @api.multi
    def write(self,vals):
        barcode = self.env['stock.location'].browse(self.id)
        if 'template' in vals and vals['template']:
            if vals['template'] == True:
                location = self.env['stock.location'].search([('template','=',True)])
                if location:
                    for x in location:
                        raise UserError(_('Template is already tick for location %s.')%(x.name))
        stock_location_id =  super(Location,self).write(vals)
        return stock_location_id

    def action_update_users(self):
        user_list = []
        for each in self:
            if each.user_ids:
                for val in each.user_ids:
                    user_list.append(val.id)
            location = self.env['stock.location'].search([('location_id','=',each.location_id.id)])
            if location:
                for val in location:
                    location_obj = self.env['stock.location'].browse(val.id)
                    location_obj.write({'user_ids':[(6,0,user_list)]})


    def action_partner_count(self):
        for each in self:
            each.partner_count = len(self.env['res.partner'].search(
                [('location_id', '=', each.id)]))



    customer_id = fields.Many2one('res.partner', 'Customer', track_visibility="onchange")
    template  = fields.Boolean('Template',track_visibility="onchange")
    partner_count = fields.Integer('Customers', compute='action_partner_count')
    location_function_type_id = fields.Many2one('stock.location.function.type','Location Function Type',track_visibility="onchange")
