{
    'name': 'Project CJPL',
    'sumary': 'Project Customization',
    'author': 'Apagen Solutions Pvt. Ltd.',
    'websites': 'www.apagen.com',
    'category': 'Project Management',
    'version': '10.0.0.1',
    'depends': [
        'project', 'project_issue',
    ],
    'data': [
        'views/project_views.xml',
    ],
    'demo': [],
    'installable': True

}
