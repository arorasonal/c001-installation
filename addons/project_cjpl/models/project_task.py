from odoo import api, fields, models, tools, SUPERUSER_ID, _


class Task(models.Model):

    _inherit = "project.task"

    review_id = fields.Many2one('res.users', 'Reviewer')
    odoo8_id = fields.Integer()


class Project(models.Model):

    _inherit = "project.project"

    odoo8_id = fields.Integer()


class Issue(models.Model):

    _inherit = "project.issue"

    odoo8_id = fields.Integer()
