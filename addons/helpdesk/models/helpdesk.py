from odoo import api, fields, models, tools
from datetime import datetime, date
from odoo.exceptions import UserError
from dateutil.relativedelta import relativedelta

LIST_OF_TASKS = [
    ('task_rework',
        'Rework of Task given by (E.g. Person Name) due to '
        '(E.g. Reason of Rework)'),
    ('improve_code', 'Improving code of (E.g. Programer Name)'),
    ('coding', 'Coding'),
    ('configuration', 'Configuration'),
    ('testing', 'Testing'),
    ('training', 'Training'),
    ('client_support', 'Client Support'),
    ('uat', 'UAT'),
    ('gap_analysis', 'Gap Analysis'),
    ('system_design', 'System Design'),
    ('server', 'Server Setup'),
    ('debug', 'Debugging'),
    ('person_leave_support', 'Person on Leave Support'),
    ('documentation', 'Documentation'),
    ('requirement_gathering_client', 'Requirement Gathering from Client'),
    ('requirement_gathering_senior',
        'Requirement Gathering/Discussion with Colleague'),
    ('pre_sale_support', 'Extra Work tdepartmento Support Pre-Sales'),
    ('learning', 'Learning'),
    ('party', 'Party'),
    ('others', 'Others'),
]


class HelpdeskTicket(models.Model):

    _name = 'helpdesk.ticket'
    _rec_name = 'ticket_id'
    _inherit = 'mail.thread'

    @api.multi
    def _get_current_employee(self):
        ids = self.env['hr.employee'].search(
            [('user_id.id', '=', self.env.uid)])
        if ids:
            return ids[0]
        return False

    ticket_id = fields.Char('Ticket #', copy=False, default='New')
    name = fields.Char('Name')
    helpdesk_team_id = fields.Many2one(
        'helpdesk.team',
        string='Helpdesk Team')
    assign_to = fields.Many2one(
        'hr.employee',
        string='Assign To',
    )
    is_ticket_closed = fields.Boolean('Is Ticket Closed')
    customer_id = fields.Many2one(
        'hr.employee',
        string='Raised By',
        default=_get_current_employee,
    )
    designation = fields.Many2one(
        'hr.job',
        related='customer_id.job_id',
    )
    department = fields.Many2one(
        'hr.department',
        string='Department',
        related='customer_id.department_id',
    )
    company_id = fields.Many2one(
        'res.company',
        string='Company',
        invisible=True,
        default=lambda self: self.env.user.company_id)
    email_id = fields.Char(
        string='Email',
        related='assign_to.work_email')
    mob_no = fields.Char(
        string='Mobile',
        related='assign_to.mobile_phone', size=10)
    team_leader = fields.Many2one(
        'hr.employee',
        'Team Leader')
    project_id = fields.Many2one(
        'account.analytic.account',
        'Project/Analytic Account')
    analytic_account_id = fields.Many2one(
        'account.analytic.account',
        string='Analytic Account')
    department_id = fields.Many2one(
        'hr.department',
        'Department',
        related='assign_to.department_id')
    priority = fields.Selection([
        ('low', 'Low'),
        ('medium', 'Medium'),
        ('high', 'High')])
    state = fields.Selection(
        [('new', "New"),
         ('open', "Open"),
         ('assigned', "Assigned"),
         ('in_progress', "In Progress"),
         ('closed', "Closed"),
         ('reopened', "Reopened"),
         ('hold', "Hold"),
         ('cancel', 'Cancelled'),
         ], "Status", default='new',
        track_visibility='always')
    close_date = fields.Datetime(
        'Close Date',
    )
    target_hours = fields.Date(
        'Target Resolution(Hours)')
    ticket_status = fields.Boolean(
        'Is Ticket Closed ?')
    helpdesk_level_id = fields.Many2one('helpdesk.level',
                                        'Helpdesk Level')
    create_date = fields.Datetime(
        'Creation Date',
    )
    deadline_date = fields.Datetime(
        'Deadline Date',
    )
    total_hours = fields.Float(
        'Total Time Spent', compute='get_total_time')
    time_log_id = fields.One2many(
        'helpdesk.time.log',
        'helpdesk_ticket_id',
        'Time Log')
    issue_description = fields.Text(
        'Issue Description')
    solution_description = fields.Text(
        'Solution Description')
    timesheet_id = fields.One2many(
        'timesheet',
        'helpdesk_ticket_id',
        'Time Log')
    timesheet_ids = fields.One2many(
        'account.analytic.line',
        'tasks_id',
        'Timesheets')
    current_user = fields.Boolean(
        copy=False,
        compute="check_current_user",
        default=False)
    check_assign = fields.Boolean(
        copy=False,
        compute="check_assign_user",
        default=False)
    team_members = fields.Many2many(
        'hr.employee',
        'rel_team_members',
        'helpdesk_team_id',
        'team_member_id')

    @api.depends('timesheet_ids.unit_amount')
    def get_total_time(self):
        self.total_hours = sum(
            timesheet.unit_amount for timesheet in self.timesheet_ids)

    @api.multi
    def check_current_user(self):
        user = self.env.uid
        for record in self:
            if self.env.user.has_group('helpdesk.group_helpdesk_manager'):
                record.current_user = False
            elif self.env.user.has_group('helpdesk.group_helpdesk_user') and record.helpdesk_team_id.user_id.user_id.id != user:
                record.current_user = True
            else:
                record.current_user = False

    @api.multi
    def check_assign_user(self):
        user = self.env.uid
        for record in self:
            if self.env.user.has_group('helpdesk.group_helpdesk_manager'):
                record.check_assign = False
            elif self.env.user.has_group('helpdesk.group_helpdesk_user') and record.assign_to.user_id.id != user:
                record.check_assign = True
            else:
                record.check_assign = False

    @api.multi
    def get_estimated_time(self):
        estimated_time = 0
        helpdesk_team = self.env['helpdesk.team'].search(
            [('id', '=', self.helpdesk_team_id.id)])
        if helpdesk_team:
            for val in helpdesk_team.service_line_id:
                if (val.stage_id == self.state):
                    estimated_time = val.estimated_time
        return estimated_time

    @api.multi
    def get_actual_time(self):
        hours = 0.00
        datetimeFormat = '%Y-%m-%d %H:%M:%S'
        current_time = datetime.now()
        time_log = self.time_log_id[-1]
        if time_log:
            start_dt = datetime.strptime(time_log.start_at, datetimeFormat)
            finish_dt = datetime.strptime(
                str(current_time), '%Y-%m-%d %H:%M:%S.%f')
            timedelta = finish_dt - start_dt
            days = timedelta.days
            hours = (timedelta.seconds) / 3600.00
        return hours

    @api.multi
    def get_working_hour(self):
        for val in self.env['helpdesk.time.log'].search([]):
            val.working_hours = self.total_hours
            if val.working_hours > 0:
                return val.working_hours
            else:
                return 0

    @api.multi
    def get_delay(self):
        for val in self.env['helpdesk.time.log'].search([]):
            val.delay = val.actual_time - val.estimated_time
            return val.delay
            if val.delay > 0:
                return val.delay
            else:
                return 0

    @api.multi
    def create_time_log(self):
        lis = []
        if self.time_log_id:
            self.time_log_id[-1].write(
                {
                    'done_at': datetime.now(),
                    'actual_time': self.get_actual_time(),
                })
        lis.append(
            {
                'stage': self.state,
                'start_at': datetime.now(),
                'done_at': datetime.now(),
                'estimated_time': self.get_estimated_time(),
                'delay': self.get_delay(),
                'working_hours': self.get_working_hour(),
                'helpdesk_ticket_id': self.id
            })
        return lis

    @api.onchange('helpdesk_team_id')
    def onchange_team_id(self):
        val = []
        for rec in self.helpdesk_team_id.team_line_id:
            val.append(rec.employee_id.id)
        self.team_members = val
        return {
            'domain': {'assign_to': [('id', 'in', val)]},
        }

    @api.multi
    def action_assigned(self):
        if self.state == 'open':
            self.state = 'assigned'
            timelog = self.create_time_log()
            self.time_log_id = timelog
            for order in self:
                if order.state == "assigned":
                    template_id = self.env.ref(
                        'helpdesk.helpdesk_email_template2')
                if template_id:
                    vals = template_id.send_mail(order.id, force_send=True)

    @api.multi
    def action_start(self):
        seq = self.env['ir.sequence'].get('helpdesk.ticket')
        self.write({'ticket_id': seq})
        self.write({'state': 'open'})
        self.write({'create_date': datetime.now()})
        timelog = self.create_time_log()
        self.time_log_id = timelog
        for order in self:
            if order.state == "open":
                template_id = self.env.ref('helpdesk.helpdesk_email_submit')
            if template_id:
                vals = template_id.send_mail(order.id, force_send=True)
        return True

    @api.multi
    def action_in_progress(self):
        self.write({'state': 'in_progress'})
        timelog = self.create_time_log()
        self.time_log_id = timelog
        return True

    @api.multi
    def action_hold(self):
        self.write({'state': 'hold'})
        timelog = self.create_time_log()
        self.time_log_id = timelog
        return True

    @api.multi
    def action_reassigned(self):
        self.write({'state': 'assigned'})
        timelog = self.create_time_log()
        self.time_log_id = timelog
        return True

    @api.multi
    def action_reopened(self):
        current_date = datetime.now()
        if self.close_date:
            threshold_date = current_date - \
                datetime.strptime(str(self.close_date), '%Y-%m-%d %H:%M:%S')
            if threshold_date.days > self.helpdesk_team_id.reopen_days_limit:
                raise UserError(
                    'Last date to reopen this ticket has been exceed, please create a new ticket')
        if self.state == 'closed':
            self.state = 'in_progress'
            timelog = self.create_time_log()
            self.time_log_id = timelog
            for order in self:
                if order.state == "in_progress":
                    template_id = self.env.ref(
                        'helpdesk.helpdesk_email_template1')
                    temp_id = self.env.ref(
                        'helpdesk.helpdesk_email_temp_reopen')
                if template_id:
                    vals = template_id.send_mail(order.id, force_send=True)
                if temp_id:
                    val = temp_id.send_mail(order.id, force_send=True)

    @api.multi
    def action_closed(self):
        if self.state == 'in_progress':
            self.state = 'closed'
            timelog = self.create_time_log()
            self.time_log_id = timelog
            self.write({'close_date': datetime.now()})
            for order in self:
                if order.solution_description == 0:
                    raise UserError('Please Enter the Solution Description')
                if order.state == "closed":
                    template_id = self.env.ref(
                        'helpdesk.helpdesk_email_template_close')
                    temp_id = self.env.ref('helpdesk.helpdesk_email_template')
                if template_id:
                    vals = template_id.send_mail(order.id, force_send=True)
                if temp_id:
                    val = temp_id.send_mail(order.id, force_send=True)

    @api.multi
    def action_cancel(self):
        self.write({'state': 'cancel'})
        timelog = self.create_time_log()
        self.time_log_id = timelog
        return True

    @api.multi
    def unlink(self):
        self.env['helpdesk.analysis'].search(
            [('helpdesk_ticket_id', '=', self.id)]).unlink()
        return super(HelpdeskTicket, self).unlink()


class HelpdeskTeam(models.Model):
    _name = 'helpdesk.team'
    _rec_name = 'team_name'

    team_name = fields.Char(
        string='Name')
    working_schedule = fields.Char(
        string='Working Schedule')
    user_id = fields.Many2one(
        'hr.employee',
        string='Team Leader')
    notes = fields.Text(
        'Notes')
    service_line_id = fields.One2many('service.line',
                                      'helpdesk_team_id',
                                      string='Service Line')
    team_line_id = fields.One2many('helpdesk.team_member', 'member_id')
    email = fields.Char(string="Email")
    reopen_days_limit = fields.Integer(
        "Reopen Days Limit", help='Limit to restrict reopen ticket after closing')


class ServiceLine(models.Model):
    _name = 'service.line'

    helpdesk_team_id = fields.Many2one(
        'helpdesk.team')
    helpdesk_ticket_id = fields.Many2one(
        'helpdesk.ticket')
    stage_id = fields.Selection(
        string='Stage',
        related='helpdesk_ticket_id.state',
        store=True)
    estimated_time = fields.Float(
        'Estimated time(HH:MM)')
    employee_id = fields.Many2one("hr.employee")


class HelpdeskTimeLog(models.Model):
    _name = 'helpdesk.time.log'

    helpdesk_ticket_id = fields.Many2one(
        'helpdesk.ticket',
        string='Support Ticket')
    support_ticket = fields.Char(
        'Support Ticket',
        related='helpdesk_ticket_id.ticket_id')
    team_id = fields.Many2one(
        'helpdesk.team',
        'Team',
        related='helpdesk_ticket_id.helpdesk_team_id')
    stage = fields.Selection(
        [('new', "New"),
         ('open', "Open"),
         ('assigned', "Assigned"),
         ('in_progress', "In Progress"),
         ('closed', "Closed"),
         ('reopened', "Reopened"),
         ('cancel', 'Cancelled'),

         ],
        string='Stage')
    # related='helpdesk_ticket_id.state')
    start_at = fields.Datetime(
        string='Start At')
    done_at = fields.Datetime(
        'Done At')
    estimated_time = fields.Float(
        'Estimated Time(HH:MM)')
    working_hours = fields.Float(
        'Working Hours(HH:MM)')
    actual_time = fields.Float(
        string='Actual Time(HH:MM)')
    delay = fields.Float(
        'Delay(HH:MM)')
    description = fields.Text(
        'Description')

    # @api.depends('start_at','done_at')
    # def _total_hours(self):
    #     hours = 0
    #     datetimeFormat = '%Y-%m-%d %H:%M:%S'
    #     for obj in self:
    #         if obj.start_at and obj.done_at:
    #             start_dt = datetime.strptime(obj.start_at, datetimeFormat)
    #             finish_dt = datetime.strptime(obj.done_at, datetimeFormat)
    #             timedelta = finish_dt - start_dt
    #             hour1 = timedelta.days * 24
    #             hours = (timedelta.seconds) / 3600
    #             obj.actual_time = hours
    #             print "=================", obj.actual_time

    @api.model
    def create(self, vals):
        res = super(HelpdeskTimeLog, self).create(vals)
        helpdesk_ticket_id = vals['helpdesk_ticket_id']
        ticket_obj = self.env['helpdesk.ticket'].browse(helpdesk_ticket_id)
        self.env['helpdesk.analysis'].create({
            'support_ticket': ticket_obj.ticket_id,
            'team_id': ticket_obj.helpdesk_team_id.id,
            'stage': vals['stage'],
            'start_at': vals['start_at'],
            'done_at': vals['done_at'],
            'estimated_time': vals['estimated_time'],
            # 'actual_time': vals['actual_time'],
            'delay': vals['delay'],
            'helpdesk_ticket_id': vals['helpdesk_ticket_id'],
            'helpdesk_time_log_id': res.id
        })
        return res

    @api.model
    def write(self, vals):
        res = super(HelpdeskTimeLog, self).write(vals)
        if 'done_at' in vals:
            self.env['helpdesk.analysis'].write({
                'done_at': vals['done_at'],
            })
        return res


class Timesheet(models.Model):
    _name = 'timesheet'

    helpdesk_ticket_id = fields.Many2one(
        'helpdesk.ticket')
    task_date = fields.Date(
        'Date')
    analytic_account_id = fields.Many2one(
        'account.analytic.account',
        relation="helpdesk.ticket",
        string='Analytic Account')
    description = fields.Char(
        'Description')
    total_hours = fields.Float(
        'Hours')


class HelpdeskLeavel(models.Model):
    _name = 'helpdesk.level'

    name = fields.Char(
        'Name')
    working_time_id = fields.One2many(
        'working.time.history',
        'helpdesk_level_id')


class WorkingTimeHistory(models.Model):
    _name = 'working.time.history'

    helpdesk_level_id = fields.Many2one(
        'helpdesk.level')
    category = fields.Char(
        'Ticket Category')
    priority = fields.Selection([
        ('low', 'Low'),
        ('medium', 'Medium'),
        ('high', 'High')], 'Priority')
    gap = fields.Char(
        'Gap')
    period_type = fields.Char(
        'Period Type')


class HelpdeskAnalysis(models.Model):
    _name = 'helpdesk.analysis'
    _rec_name = 'support_ticket'
    _order = 'id DESC'

    helpdesk_ticket_id = fields.Many2one(
        'helpdesk.ticket',
        string='Support Ticket')
    helpdesk_time_log_id = fields.Many2one('helpdesk.time.log', 'Time Log')
    support_ticket = fields.Char(
        'Support Ticket')
    team_id = fields.Many2one(
        'helpdesk.team',
        'Team')
    stage = fields.Selection(
        [('new', "New"),
         ('open', "Open"),
         ('assigned', "Assigned"),
         ('in_progress', "In Progress"),
         ('reopened', "Reopened"),
         ('closed', "Closed"),
         ('cancel', "Cancelled")], "Stage")
    start_at = fields.Datetime(
        'Start At')
    done_at = fields.Datetime(
        'Done At')
    estimated_time = fields.Float(
        'Estimated Time(HH:MM)')
    working_hours = fields.Float(
        'Working Hours(HH:MM)')
    actual_time = fields.Float(
        'Actual Time(HH:MM)')
    delay = fields.Float(
        'Delay(HH:MM)')
    description = fields.Text(
        'Description')


class AccountAnalytic(models.Model):
    """This class used for adding line."""
    _inherit = "account.analytic.line"

    account_id = fields.Many2one(
        'account.analytic.account', 'Analytic Account', required=False, ondelete='restrict', index=True)
    tasks_id = fields.Many2one('helpdesk.ticket', 'Task', copy=False)
    department_id = fields.Many2one(
        'hr.department', "Department", related='user_id.employee_ids.department_id', store=True, readonly=True)
    state = fields.Selection([
        ('in_progress', "In Progress)"),
        ('hold', "Hold"),
        ('done', "Done"),
    ], string="Status", default='in_progress')
    employee_id = fields.Many2one("hr.employee", "Employee")


class TeamMember(models.Model):
    _name = "helpdesk.team_member"

    member_id = fields.Many2one("helpdesk.team")
    employee_id = fields.Many2one("hr.employee")
