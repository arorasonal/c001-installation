from odoo import api, fields, models, _
class HelpdeskWizard(models.TransientModel):
    _name = "helpdesk.wizard"
    _description = "Helpdesk Products" 

    helpdesk_team_id = fields.Many2one(
        'helpdesk.team',
        string='Helpdesk Team')
    assign_to = fields.Many2one(
        'hr.employee',
        string='Assign To')

    @api.multi
    def set_assigned(self):
        for order in self:
            template_id = self.env.ref('helpdesk.helpdesk_email_wiz')
            if template_id:
                vals = template_id.send_mail(order.id, force_send=True)
        helpdesk_team_id = self.env['helpdesk.ticket'].browse(self.env.context.get('active_id'))
        for helpdesk_id in helpdesk_team_id:
            helpdesk_id.helpdesk_team_id = self.helpdesk_team_id
        for help_id in helpdesk_team_id:
            help_id.assign_to = self.assign_to

    @api.onchange('helpdesk_team_id')
    def onchange_team_id(self):
        val = []
        for rec in self.helpdesk_team_id.team_line_id:
            val.append(rec.employee_id.id)
        return {
                'domain': {'assign_to': [('id', 'in', val)]},
            }
