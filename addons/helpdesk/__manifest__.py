# --*-- utf-8 --*--

{
    'name': 'Employee Helpdesk',
    'summary': 'This application allows internal users to create ticket',
    'author': 'Apagen Solutions Pvt. Ltd.',
    'website': 'http://www.apagen.com',
    'category': 'Construction Industry',
    'version': '10.0.0.1',
    'depends': ['base',
                'hr',
                'project',
                'analytic',
                'hr_timesheet',
                ],
    'data': [
        'security/base_security.xml',
        'security/ir.model.access.csv',
        'wizard/helpdesk_wizard.xml',
        'views/helpdesk_view.xml',
        'views/helpdesk_sequence.xml',
        'views/email_view.xml',
    ],
    'installable': True
}
