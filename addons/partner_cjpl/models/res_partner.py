import datetime
from lxml import etree
import math
import pytz
import urlparse
from datetime import datetime, timedelta, date
from odoo import tools, api, fields, models, _
# from openerp.osv.expression import get_unaccent_wrapper
# from odoo.tools.translate import


class res_partner(models.Model):
    _inherit = "res.partner"

    @api.model
    def create(self, vals):
        if vals.get('website'):
            vals['website'] = self._clean_website(vals['website'])
        partner = super(res_partner, self).create(vals)
        res_obj = self.env['res.partner'].browse(partner.id)
        if vals.get('user_id'):
            self.env['saleperson.history'].create({'partner_id': partner.id,
                                                   'user_id': res_obj.user_id.id,
                                                   'date': date.today().strftime('%Y-%m-%d'),
                                                   })
        partner._fields_sync(vals)
        partner._handle_first_contact_creation()
        return partner

    @api.multi
    def write(self, vals):
        # if vals.get('website'):
        #     vals['website'] = self._clean_website(vals['website'])
        # if vals.get('company_id'):
        #     company = self.env['res.company'].browse(vals['company_id'])
        #     for partner in self:
        #         if partner.user_ids:
        #             companies = set(
        #                 user.company_id for user in partner.user_ids)
        #             if len(companies) > 1 or company not in companies:
        #                 raise osv.except_osv(_("Warning"), _(
        #                     "You can not change the company as the partner/user has multiple user linked with different companies."))
        result = super(res_partner, self).write(vals)
        # for partner in self:
        #     partner._fields_sync(vals)
        if vals.get('user_id'):
            for partner in self:
                partner_obj = self.env['res.partner'].browse(partner.id)
                sale = self.env['saleperson.history'].search(
                    [('partner_id', '=', partner.id)],limit=1)
                sale_obj = ' '
                if sale:
                    for val in sale:
                        sale_obj = self.env['saleperson.history'].browse(val[
                                                                         0].id)
                if sale_obj != ' ':
                    if sale_obj.user_id.id != vals['user_id']:
                        self.env['saleperson.history'].create({'partner_id': partner.id,
                                                               'user_id': partner.user_id.id,
                                                               'date': date.today().strftime('%Y-%m-%d'),
                                                               'user1_id': self.env.uid
                                                               })
                else:
                    self.env['saleperson.history'].create({'partner_id': partner.id,
                                                           'user_id': partner.user_id.id,
                                                           'date': date.today().strftime('%Y-%m-%d'),
                                                           'user1_id': self.env.uid
                                                           })

        return result

    saleperson_history_lines = fields.One2many(
        'saleperson.history', 'partner_id', 'Saleperson')
    date = fields.Date('Date')
    tin = fields.Char("TIN #")
    kyc_template = fields.Char("KYC Template")
    kyc_form = fields.Char("KYC Form")
    roc = fields.Char("ROC #")
    team_id = fields.Many2one('crm.team', 'Sales Team', domain=[('type_team','=','sale')])

    property_account_payable_id = fields.Many2one('account.account', company_dependent=True,
                                                  string="Account Payable", oldname="property_account_payable",
                                                  domain="[('internal_type', '=', 'payable'), ('deprecated', '=', False)]",
                                                  help="This account will be used instead of the default one as the payable account for the current partner",
                                                  required=False)
    property_account_receivable_id = fields.Many2one('account.account', company_dependent=True,
                                                     string="Account Receivable", oldname="property_account_receivable",
                                                     domain="[('internal_type', '=', 'receivable'), ('deprecated', '=', False)]",
                                                     help="This account will be used instead of the default one as the receivable account for the current partner",
                                                     required=False)


class saleperson_history(models.Model):
    _name = "saleperson.history"
    _order = 'date desc'

    date = fields.Date('Date')
    partner_id = fields.Many2one('res.partner', 'Partner')
    user_id = fields.Many2one('res.users', 'Salesperson')
    user1_id = fields.Many2one('res.users', 'Created By')

    # _defaults={
    #            'user1_id':lambda obj, cr, uid, cnt: uid,
    #            }
