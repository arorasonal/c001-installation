{
    "name": "Partner Cjpl",
    "version": "10.0.0.1.0",
    'website': 'https://www.apagen.com',
    "depends": ['base', 'india_gst_cjpl', 'base_hr'],
    "author": "Apagen Solutions Pvt. Ltd.",
    "category": "GTR",
    'sequence': 16,
    "description": """
  This module will provide the Partner details 
  """,
    'data': [
        'security/ir.model.access.csv',
        'views/res_partner_view.xml',
    ],
    'demo_xml': [],
    'installable': True,
    'active': False,
    'auto_install': False,
}
