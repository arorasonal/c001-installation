import logging

from odoo import api, fields, models
from odoo import tools, _
import time
import math
from time import gmtime, strftime
import odoo.addons.decimal_precision as dp
from datetime import datetime,timedelta,date
from odoo.tools import DEFAULT_SERVER_DATE_FORMAT,DEFAULT_SERVER_DATETIME_FORMAT
from odoo.exceptions import UserError, ValidationError
from odoo.modules.module import get_module_resource

class location_update(models.Model):
    _name="location.update"


    def location_update(self):
        for each in self:
            location_name = ''
            location_id = ''
            user_list  =[]
            if each.customer_id.name:
                partner_obj = self.env['res.partner'].browse(each.customer_id.id)
                location = self.env['stock.location'].search([('name','=',each.customer_id.name)])
                if location:
                    for val in location:
                        location_id = val
                else:
                    location_name = each.customer_id.name
                if location_name:
                    origin_location = self.env['stock.location'].browse(367)
                    if origin_location.user_ids:
                        for user in origin_location.user_ids:
                            user_list.append(user.id)
                    location_id = self.env['stock.location'].create({'name': location_name, 'wr_id': origin_location.wr_id.id,
                         'location_id': origin_location.location_id.id,
                         'usage': origin_location.usage,
                         'allow_negative_stock': origin_location.allow_negative_stock,
                         'company_id': origin_location.company_id.id,
                         'user_ids': [(6, 0, user_list)]})
                    location_obj = self.env['stock.location'].browse(location_id.id)
                    location_obj.write({'customer_id': partner_obj.id})
                    partner_obj.write({'location_id': location_id.id})



    customer_id = fields.Many2one('res.partner','Customer')