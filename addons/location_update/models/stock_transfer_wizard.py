import logging

from odoo import api, fields, models
from odoo import tools, _
import time
import math
from time import gmtime, strftime
import odoo.addons.decimal_precision as dp
from datetime import datetime,timedelta,date
from odoo.tools import DEFAULT_SERVER_DATE_FORMAT,DEFAULT_SERVER_DATETIME_FORMAT
from odoo.exceptions import UserError, ValidationError
from odoo.modules.module import get_module_resource

class stock_transfer_wizard(models.Model):
    _name="stock.transfer.wizard"

    def action_transfer(self):
        print"fbnfkbnfkbn"
        for each in self:
            print"dblngfblnfgbnf"
            if each.stock_transfer_wizard_lines:
                print"dflnbgbnrgk"
                for val in each.stock_transfer_wizard_lines:
                    s = ''
                    p1= []
                    quant_obj = ' '
                    if val.qty > 0:
                        if val.serial_no:
                            serial = self.env['stock.production.lot'].search([('product_id','=',val.product),('id','=',val.serial_no)])
                            if serial:
                                for val1 in serial:
                                    if val1.quant_ids.location_id.id == val.current_location:
                                        product_obj = self.env['product.product'].browse(val.product)
                                        quant = self.env['stock.quant'].search([('lot_id', '=', val.serial_no),('product_id','=',val.product)])
                                        if quant:
                                             for val2 in quant:
                                                 if val2.location_id.id == val.current_location:
                                                     val2.write({'location_id': val.destination_location})
                                                 quant_obj  = self.env['stock.quant'].browse(val2.id)
                                        if quant_obj: 
                                       	    s1 = self.env['stock.move'].create({'product_id': val.product,
                                                                            'product_uom_qty': val.qty,
                                                                            'product_uom': product_obj.uom_id.id,
                                                                            'name': product_obj.name,
                                                                            'origin': each.name,
                                                                            'location_id': val.current_location,
                                                                            'location_dest_id': val.destination_location})
                                            stock_obj = self.env['stock.move'].browse(s1.id)
                                            p1.append(quant_obj.id)
                                            s1 = stock_obj.write({'quant_ids': [(6, 0, p1)], 'state': 'done'})
                                            val.write({'transfer':True})
                                        else:
                                            val.write({'no_transfer_reason':'Missing Quant Obj'})
                                    else:
                                        val.write({'no_transfer_reason':'Location is not Current Location'})            
                        elif val.product:
                            product_obj = self.env['product.product'].browse(val.product)
                            if product_obj.categ_id.numbered == False:
                                s = self.env['stock.move'].create({'product_id': val.product,
                                                                   'product_uom_qty': val.qty,
                                                                   'product_uom': product_obj.uom_id.id,
                                                                   'name': product_obj.name,
                                                                   'origin':each.name,
                                                                   'location_dest_id':val.destination_location,
                                                                   'location_id': val.current_location})
                                stock_obj = self.env['stock.move'].browse(s.id)
                                stock_obj.action_done()
                                s2 = self.env['stock.quant'].create({'product_id': val.product,
                                                 'qty': val.qty, 'location_id': val.destination_location,
                                                 })
                                quant_obj = self.env['stock.quant'].browse(s2.id)
                                p1.append(quant_obj.id)                                                                   
                                s = stock_obj.write({'quant_ids': [(6, 0, p1)], 'state': 'done'})
                                val.write({'transfer':True})
            each.write({'transfer':True})

    name = fields.Char('Name',required=True)
    date  = fields.Date('Date')
    transfer = fields.Boolean('Transfer')
    stock_transfer_wizard_lines = fields.One2many('stock.transfer.wizard.line','stock_transfer_wizard_id','Stock Transfer Lines')


class stock_transfer_wizard_line(models.Model):
    _name="stock.transfer.wizard.line"
    stock_transfer_wizard_id = fields.Many2one('stock.transfer.wizard','Stock Transfer Wizard')
    product = fields.Integer('Product')
    stock_picking_type = fields.Integer('Stock Picking Type')
    transaction_type = fields.Integer('Transaction Type')
    partner = fields.Integer('Partner')
    serial_no = fields.Integer('Serial No')
    qty = fields.Integer('Quantity')
    current_location = fields.Integer('Current Location')
    destination_location = fields.Integer('Destination Location')
    transfer = fields.Boolean('Transfer')
    no_transfer_reason = fields.Text('No Transfer Reason')
