{
    "name": "Location Update",
    "version": "1.0",
    'website': 'http://www.diracerp.in',
    'images': [],
    "depends": ['web', 'base', 'product', 'stock'],
    "author": "Dirac ERP",
    "category": "Stock",
    #     'sequence': 16,
    "description": """
    This module for the update location in warehouse. 
    """,
    'data': [

        'view/location_update_view.xml',
        'view/stock_transfer_wizard_view.xml',

        #
    ],
    'demo_xml': [],
    'js': [
        'static/src/js/view_list.js'
        #            'static/src/js/view_list.js'
        #     'static/src/js/subst.js'
    ],
    'css': [
        #             'static/src/css/sample_kanban.css'
    ],
    #     'pre_init_hook': 'pre_init_hook',
    #     'post_init_hook': 'post_init_hook',
    'installable': True,
    'active': False,
    'auto_install': False,
    #    'certificate': 'certificate',
}
