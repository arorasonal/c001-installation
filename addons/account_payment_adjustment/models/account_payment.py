# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2010 Tiny SPRL (<http://tiny.be>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from odoo import models, fields, api, _
from odoo.exceptions import UserError, ValidationError


class account_payment(models.Model):
    _name = 'account.payment'
    _inherit = ['account.payment', 'mail.thread']

   
    def _make_journal_search(self, ttype, context=None):
        journal_pool = self.env['account.journal']
        return journal_pool.search([('type', '=', ttype)], limit=1)

    def _get_journal(self):
        context = self._context
        invoice_pool = self.env['account.invoice']
        journal_pool = self.env['account.journal']
        if context.get('invoice_id', False):
            invoice = invoice_pool.browse(context['invoice_id'])
            journal_id = journal_pool.search(
                [('currency', '=', invoice.currency_id.id), ('company_id', '=', invoice.company_id.id)], limit=1)
            return journal_id and journal_id[0] or False
        if context.get('journal_id', False):
            return context.get('journal_id')
        if not context.get('journal_id', False) and context.get('search_default_journal_id', False):
            return context.get('search_default_journal_id')

        ttype = context.get('type', 'bank')
        if ttype in ('payment', 'receipt'):
            ttype = 'bank'
        res = self._make_journal_search(ttype, context=context)
        return res and res[0] or False

    def _get_currency_help_label(self, currency_id, payment_rate, payment_rate_currency_id, context=None):
        """
        This function builds a string to help the users to understand the behavior of the payment rate fields they can specify on the voucher. 
        This string is only used to improve the usability in the voucher form view and has no other effect.

        :param currency_id: the voucher currency
        :type currency_id: integer
        :param payment_rate: the value of the payment_rate field of the voucher
        :type payment_rate: float
        :param payment_rate_currency_id: the value of the payment_rate_currency_id field of the voucher
        :type payment_rate_currency_id: integer
        :return: translated string giving a tip on what's the effect of the current payment rate specified
        :rtype: str
        """
        rml_parser = report_sxw.rml_parse(
            'currency_help_label', context=context)
        currency_pool = self.env['res.currency']
        currency_str = payment_rate_str = ''
        if currency_id:
            currency_str = rml_parser.formatLang(
                1, currency_obj=currency_pool.browse(cr, uid, currency_id, context=context))
        if payment_rate_currency_id:
            payment_rate_str = rml_parser.formatLang(payment_rate, currency_obj=currency_pool.browse(
                cr, uid, payment_rate_currency_id, context=context))
        currency_help_label = _('At the operation date, the exchange rate was\n%s = %s') % (
            currency_str, payment_rate_str)
        return currency_help_label

    def _compute_writeoff_amount(self, line_dr_ids, line_cr_ids, amount, type):
        debit = credit = 0.0
        sign = type == 'outbound' and -1 or 1
        for l in line_dr_ids:
            if isinstance(l, dict):
                debit += l['amount']
        for l in line_cr_ids:
            if isinstance(l, dict):
                credit += l['amount']
        return amount - sign * (credit - debit)

    @api.depends('invoice_ids', 'amount', 'payment_date', 'currency_id', 'line_dr_ids', 'line_cr_ids')
    def _compute_payment_differences(self):
        for rec in self:
            # if len(rec.invoice_ids) == 0:
            #     return
            dr_ids = []
            cr_ids = []
            for line in rec.line_dr_ids:
                dr_ids.append({'amount': line.amount})
            for line in rec.line_cr_ids:
                cr_ids.append({'amount': line.amount})
	    #print('amount+++++++++++++++', line.amount)
            rec.payment_difference = rec._compute_writeoff_amount(
                dr_ids, cr_ids, rec.amount, rec.payment_type)
	    #print('rec.payment_difference+++++++', rec.payment_difference)

    line_dr_ids = fields.One2many('account.payment.lines', 'payment_id', domain=[('type', '=', 'dr')],
                                  context={'default_type': 'dr'}, string='Debits',
                                  states={'draft': [('readonly', False)]})
    line_cr_ids = fields.One2many('account.payment.lines', 'payment_id', domain=[('type', '=', 'cr')],
                                  context={'default_type': 'cr'}, string='Credits',
                                  states={'draft': [('readonly', False)]})
    journal_id = fields.Many2one(
        'account.journal', string='Payment Journal')  # , default=_get_journal_id)
    # journal_id = fields.Many2one('account.journal', string='Payment Journal', required=True,
    #     domain=[('type', 'in', ('bank', 'cash'))], default=_get_journal)
    amount = fields.Float('Payment Amount',digits=(14,2))
    #amount = fields.Monetary('Payment Amount',digits=(14,2))
    payment_difference = fields.Float(
        compute='_compute_payment_differences')
    company_id = fields.Many2one(
        'res.company', related=False, default=lambda self: self.env.user.company_id)
    writeoff_account_id = fields.Many2one(
        'account.account', string="Difference Account", copy=False)
    state = fields.Selection([
        ('draft', 'Draft'),
        ('posted', 'Posted'),
        ('cancel', 'Cancel'),
    ], readonly=True, default='draft', copy=False, string="Status")
    instrument_type = fields.Many2one('instrument.type', 'Instrument Type')
    # instrument_type = fields.Selection([
    #     ('cheque', 'Cheque'),
    #     ('dd', 'DD'),
    #     ('neft', 'NEFT'),
    #     ('rtgs', 'RTGS'),
    #     ('cash', 'Cash')], 'Instrument Type', default='cheque')
    acc_payment_type = fields.Many2one('payment.type', "Payment Type")
    # account_id = fields.Many2one('account.account', string='TDS Account')
    check_date = fields.Date('Instrument Date')
    entry_date = fields.Date('Instrument Entry Date')
    bank_deposite_dt = fields.Date('Bank Deposite Dt')
    instrument = fields.Char('Instrument #')
    cheque_clearance_dt = fields.Date('Cheque Clearance Dt')
    apply_tds = fields.Boolean("Apply TDS")
    description = fields.Char("Bank Name")
    remarks = fields.Char("Remarks")
    # partner_id = fields.Many2one(
    #     'res.partner', string='Partner1', domain=([
    #         ('company_type', '=', 'company')]))

    @api.one
    @api.constrains('amount')
    def _check_amount(self):
        pass

    @api.constrains('partner_id', 'state')
    def _check_partner_state(self):
        for rec in self:
            domain = [
                ('partner_id', '=', rec.partner_id.id),
                ('state', '=', 'draft'),
                ('id', '!=', rec.id)
            ]
        payment_ids = self.search_count(domain)
        if payment_ids >= 1:
            raise UserError(
                _('Two Payment for same Partner \
                    cannot be in Draft state'))

    @api.onchange('company_id')
    def default_company_id(self):
        # if self.journal_id:
        self.journal_id = False
        self.partner_id = False
        self.writeoff_account_id = False
        self.line_dr_ids = False
        self.line_cr_ids = False
        # return {'domain': {
        #     'journal_id': self._get_journal_id(),
        #     'writeoff_account_id': self._get_writeoff_account()
        # }}

    @api.onchange('line_dr_ids', 'line_cr_ids')
    def onchange_line_ids(self):
        context = self._context or {}
        if not self.line_dr_ids and not self.line_cr_ids:
            return {'value': {'payment_difference': 0.0}}

        # resolve lists of commands into lists of dicts
        dr_ids = []
        cr_ids = []
        for line in self.line_dr_ids:
            dr_ids.append({'amount': line.amount})
        for line in self.line_cr_ids:
            cr_ids.append({'amount': line.amount})
        return {'value': {'payment_difference': self._compute_writeoff_amount(dr_ids, cr_ids, self.amount, self.payment_type)}}

#     @api.multi
#     def write(self, vals):
#         context = self._context or {}
#         if not vals.get('line_dr_ids') and not vals.get('line_cr_ids'):
#             return super(account_payment, self).write(vals)
#         # resolve lists of commands into lists of dicts
#         dr_ids = []
#         cr_ids = []
#         for line in self.line_dr_ids:
#             dr_ids.append(line.id)
#         for line in self.line_cr_ids:
#             cr_ids.append(line.id)
#         line_dr_ids = self.resolve_2many_commands('line_dr_ids', vals.get('line_dr_ids'), ['amount'])
#         line_cr_ids = self.resolve_2many_commands('line_cr_ids', vals.get('line_cr_ids'), ['amount'])
#         vals.update({'payment_difference': self._compute_writeoff_amount(line_dr_ids, line_cr_ids, self.amount, self.payment_type)})
#         return super(account_payment, self).write(vals)

    @api.model
    def create(self, vals):
        invoice_obj = self.env['account.invoice']
        move_line_obj = self.env['account.move.line']
        invoice_ids = []
        if self._context.has_key('default_payment_type') and self._context['default_payment_type'] == 'inbound':
            for item in vals.get('line_cr_ids'):
                if item[2].get('move_line_id'):
                    move_line = move_line_obj.browse(
                        item[2].get('move_line_id'))
                    if move_line and move_line.invoice_id:
                        invoices = invoice_obj.search([
                            ('id', '=', move_line.invoice_id.id),
                            ('state', '=', 'open'),
                            # ('company_id', '=', 'self.company_id.id')
                        ])
                        for invoice in invoices:
                            invoice_ids.append((4, [invoice.id]))
            vals['invoice_ids'] = invoice_ids

        if self._context.has_key('default_payment_type') and self._context['default_payment_type'] == 'outbound':
            for item in vals.get('line_dr_ids'):
                if item[2].get('move_line_id'):
                    move_line = move_line_obj.browse(
                        item[2].get('move_line_id'))
                    if move_line and move_line.invoice_id:
                        invoices = invoice_obj.search([
                            ('id', '=', move_line.invoice_id.id),
                            ('state', '=', 'open'),
                            ('company_id', '=', 'self.company_id.id')])
                        for invoice in invoices:
                            invoice_ids.append((4, [invoice.id]))
            vals['invoice_ids'] = invoice_ids
        payment = super(account_payment, self).create(vals)
        return payment

    @api.multi
    def cancel(self):
        for rec in self:
            for move in rec.move_line_ids.mapped('move_id'):
                if rec.invoice_ids:
                    move.line_ids.remove_move_reconcile()
                move.button_cancel()
                move.unlink()
            rec.move_name = ''
            rec.state = 'cancel'

    @api.multi
    def reset_draft(self):
        for rec in self:
            rec.state = 'draft'

    @api.multi
    def remove_allocation_amount(self):
        for rec in self:
            for line in rec.line_cr_ids:
                line.reconcile = False
                line.amount = 0.0
                line.tds_amount = 0.0
                line.tds_rate = ''
            for line in rec.line_dr_ids:
                line.reconcile = False
                line.amount = 0.0
                line.tds_amount = 0.0
                line.tds_rate = ''

    def onchange_rate(self, rate, amount, currency_id, payment_rate_currency_id, company_id, context=None):
        # , 'currency_help_label': self._get_currency_help_label(currency_id, rate, payment_rate_currency_id, context=context)}}
        res = {'value': {'paid_amount_in_company_currency': amount}}
        if rate and amount and currency_id:
            if company_id:
                company_currency = self.env[
                    'res.company'].browse(company_id).currency_id
            else:
                company_currency = currency_id
            # context should contain the date, the payment currency and the
            # payment rate specified on the voucher
            amount_in_company_currency = self.env[
                'res.currency'].compute(amount, company_currency)
            res['value']['paid_amount_in_company_currency'] = amount_in_company_currency
        return res

    @api.onchange('amount')
    def onchange_amount(self):
        if self.amount >= 0:
            amount = self.amount
            rate = self.currency_id.rate
            partner_id = self.partner_id
            journal_id = self.journal_id
            amount = self.amount
            currency_id = self.currency_id
            ttype = self.payment_type
            date = self.payment_date
            company_id = self.company_id
            payment_rate_currency_id = self.currency_id
    #         ctx = self._context.copy()
    #         ctx.update({'date': date})
            # read the voucher rate with the right date in the context
            currency_id = currency_id or self.env[
                'res.company'].browse(company_id).currency_id.id
            voucher_rate = self.env['res.currency'].browse(currency_id.id).rate
    #         ctx.update({
    #             'voucher_special_currency': payment_rate_currency_id,
    #             'voucher_special_currency_rate': rate * voucher_rate})
            res = self.recompute_voucher_lines(
                partner_id, journal_id, amount, currency_id, ttype, date)
            vals = self.onchange_rate(
                rate, amount, currency_id, payment_rate_currency_id, company_id.id)
            for key in vals.keys():
                res[key].update(vals[key])
            return res

    def recompute_payment_rate(self, vals, currency, date, ttype, journal_id, amount, context=None):
        if context is None:
            context = {}
        # on change of the journal, we need to set also the default value for
        # payment_rate and payment_rate_currency_id
        currency_obj = self.env['res.currency']
        journal = self.env['account.journal'].browse(journal_id.id)
        company_id = journal.company_id.id
        payment_rate = 1.0
        currency_id = currency.id or journal.company_id.currency_id.id
        payment_rate_currency_id = currency_id
        ctx = context.copy()
        ctx.update({'date': date})
        o2m_to_loop = False
        if ttype == 'inbound':
            o2m_to_loop = 'line_cr_ids'
        elif ttype == 'outbound':
            o2m_to_loop = 'line_dr_ids'
        if o2m_to_loop and 'value' in vals and o2m_to_loop in vals['value']:
            for voucher_line in vals['value'][o2m_to_loop]:
                if not isinstance(voucher_line, dict):
                    continue
                if voucher_line['currency_id'] != currency_id:
                    # we take as default value for the payment_rate_currency_id, the currency of the first invoice that
                    # is not in the voucher currency
                    payment_rate_currency_id = voucher_line['currency_id']
                    tmp = currency_obj.browse(
                        payment_rate_currency_id, context=ctx).rate
                    payment_rate = tmp / \
                        currency_obj.browse(currency_id, context=ctx).rate
                    break
        vals['value'].update({
            'payment_rate': payment_rate,
            'currency_id': currency_id,
            'payment_rate_currency_id': payment_rate_currency_id
        })
        # read the voucher rate with the right date in the context
        # self.env['res.currency'].read([currency_id], ['rate'])[0]['rate']
        voucher_rate = currency.rate
        ctx.update({
            'voucher_special_currency_rate': payment_rate * voucher_rate,
            'voucher_special_currency': payment_rate_currency_id})
        res = self.onchange_rate(
            payment_rate, amount, currency_id, payment_rate_currency_id, company_id, context=ctx)
        for key in res.keys():
            vals[key].update(res[key])
        return vals

    def basic_onchange_partner(self, partner, journal, ttype, context=None):
        partner_pool = self.env['res.partner']
        journal_pool = self.env['account.journal']
        res = {'value': {'account_id': False}}
        if not partner or not journal:
            return res

#         journal = journal_pool.browse(journal_id)
#         partner = partner_pool.browse(partner_id)
        account_id = False
        if journal.type in ('sale', 'sale_refund'):
            account_id = partner.property_account_receivable.id
        elif journal.type in ('purchase', 'purchase_refund', 'expense'):
            account_id = partner.property_account_payable.id
        elif ttype in ('inbound'):
            account_id = journal.default_debit_account_id.id
        elif ttype in ('outbound'):
            account_id = journal.default_credit_account_id.id
        else:
            account_id = journal.default_credit_account_id.id or journal.default_debit_account_id.id

        res['value']['account_id'] = account_id
        return res

    @api.onchange('partner_id', 'journal_id')
    def onchange_partner_id(self):
        self.line_dr_ids = False
        self.line_cr_ids = False
        partner_id = self.partner_id
        journal_id = self.journal_id
        amount = self.amount
        currency_id = self.currency_id
        ttype = self.payment_type
        date = self.payment_date
        context = self._context
        if not journal_id:
            return {}
        if context is None:
            context = self._context
        # TODO: comment me and use me directly in the sales/purchases views
        res = self.basic_onchange_partner(
            partner_id, journal_id, ttype, context=context)
#         if ttype in ['inbound', 'outbound']:
#             return res
        ctx = context.copy()
        # not passing the payment_rate currency and the payment_rate in the
        # context but it's ok because they are reset in recompute_payment_rate
        ctx.update({'date': date})
#         vals = self.recompute_voucher_lines(partner_id, journal_id, amount, currency_id, ttype, date, context=ctx)
        vals = self.onchange_amount()
        vals2 = self.recompute_payment_rate(
            vals, currency_id, date, ttype, journal_id, amount, context=context)
        for key in vals.keys():
            res[key].update(vals[key])
        for key in vals2.keys():
            res[key].update(vals2[key])
        # TODO: can probably be removed now
        # TODO: onchange_partner_id() should not returns [pre_line, line_dr_ids, payment_rate...] for type sale, and not
        # [pre_line, line_cr_ids, payment_rate...] for type purchase.
        # We should definitively split account.voucher object in two and make distinct on_change functions. In the
        # meanwhile, bellow lines must be there because the fields aren't present in the view, what crashes if the
        # onchange returns a value for them
#         if ttype == 'inbound':
#             del(res['value']['line_dr_ids'])
#             del(res['value']['pre_line'])
#             del(res['value']['payment_rate'])
#         elif ttype == 'outbound':
#             del(res['value']['line_cr_ids'])
#             del(res['value']['pre_line'])
#             del(res['value']['payment_rate'])
        return res

    def recompute_voucher_lines(self, partner_id, journal_id, price, currency_id, ttype, date, context=None):
        """
        Returns a dict that contains new values and context

        @param partner_id: latest value from user input for field partner_id
        @param args: other arguments
        @param context: context arguments, like lang, time zone

        @return: Returns a dict which contains new values, and context
        """
        def _remove_noise_in_o2m():
            """if the line is partially reconciled, then we must pay attention to display it only once and
                in the good o2m.
                This function returns True if the line is considered as noise and should not be displayed
            """
            if line.matched_debit_ids or line.matched_credit_ids:
                if currency_id == line.currency_id.id:
                    if line.amount_residual_currency == 0:
                        return True
                else:
                    if line.amount_residual == 0:
                        return True
            return False

        if context is None:
            context = self._context
        context_multi_currency = context.copy()

        currency_pool = self.env['res.currency']
        move_line_pool = self.env['account.move.line']
        partner_pool = self.env['res.partner']
        journal_pool = self.env['account.journal']
        line_pool = self.env['account.payment.lines']

        # set default values
        default = {
            'value': {'invoice_ids': [], 'line_dr_ids': [], 'line_cr_ids': [], 'pre_line': False},
        }

        # drop existing lines
        line_ids = [rec for rec in self.line_cr_ids] + \
            [rec for rec in self.line_dr_ids]
        if line_ids:
            for line in line_ids:
                if line.type == 'cr':
                    default['value']['line_cr_ids'].append((2, line.id))
                else:
                    default['value']['line_dr_ids'].append((2, line.id))
#                 if line.type == 'cr':
#                     default['value']['line_cr_ids'].append((2, line.id))
#                 else:
#                     default['value']['line_dr_ids'].append((2, line.id))

        if not partner_id or not journal_id:
            return default

        journal = journal_id
        partner = partner_id
        currency_id = currency_id or journal.company_id.currency_id.id

        total_credit = 0.0
        total_debit = 0.0
        account_type = None
        if context.get('account_id'):
            account_type = self.pool['account.account'].browse(
                context['account_id']).type
        if ttype == 'outbound':
            if not account_type:
                account_type = 'payable'
            total_debit = price or 0.0
        else:
            total_credit = price or 0.0
            if not account_type:
                account_type = 'receivable'

#         account_type_id = self.pool['account.account.type'].search([('')])
#         account_type_id =

        if not context.get('move_line_ids', False):
            list_partner = []
            partner_ids = self.env['res.partner'].search([('parent_id', '=', partner_id.id)])
            list_partner = partner_ids.ids
            list_partner.append(partner_id.id)
            ids = move_line_pool.search([('account_id.user_type_id.type', '=', account_type),
                                         ('full_reconcile_id', '=',
                                          False), ('partner_id', 'in', list_partner),
                                         ('company_id', '=', self.company_id.id)])
        else:
            ids = context['move_line_ids']
        invoice_id = context.get('invoice_id', False)
        company_currency = journal.company_id.currency_id.id
        move_lines_found = []

        # order the lines by most old first
        ids = [i for i in ids]
        ids.sort(key=lambda x: x.date, reverse=False)
        account_move_lines = ids  # move_line_pool.browse(ids)

        # compute the total debit/credit and look for a matching open amount or
        # invoice
        for line in account_move_lines:
            if _remove_noise_in_o2m():
                continue
            invoices = self.env['account.invoice'].search([
                ('move_id', '=', line.move_id.id),
                ('state', '=', 'open'),
                ('company_id', '=', self.company_id.id)
            ])
            for invoice in invoices:
                if ttype == 'inbound' and invoice.type == 'out_invoice':
                    default['value']['invoice_ids'].append((4, invoice.id))
                if ttype == 'outbound' and invoice.type == 'in_invoice':
                    default['value']['invoice_ids'].append((4, invoice.id))
                move_lines_found.append(line.id)

            if invoice_id:
                if line.invoice.id == invoice_id:
                    # if the invoice linked to the voucher line is equal to the invoice_id in context
                    # then we assign the amount on that line, whatever the
                    # other voucher lines
                    move_lines_found.append(line.id)
            elif currency_id.id == company_currency:
                # otherwise treatments is the same but with other field names
                if line.amount_residual == price:
                    # if the amount residual is equal the amount voucher, we assign it to that voucher
                    # line, whatever the other voucher lines
                    move_lines_found.append(line.id)
                    break
                # otherwise we will split the voucher amount on each line (by
                # most old first)
                total_credit += line.credit and line.amount_residual or 0.0
                total_debit += line.debit and line.amount_residual or 0.0
            elif currency_id == line.currency_id.id:
                if line.amount_residual_currency == price:
                    move_lines_found.append(line.id)
                    break
                line_residual = currency_pool.compute(company_currency, currency_id, abs(
                    line.amount_residual), context=context_multi_currency)
                total_credit += line.credit and line_residual or 0.0
                total_debit += line.debit and line_residual or 0.0

        remaining_amount = price
        # voucher line creation
        for line in account_move_lines:

            if _remove_noise_in_o2m():
                continue

            if line.currency_id and currency_id.id == line.currency_id.id:
                amount_original = abs(line.amount_currency)
                amount_unreconciled = abs(line.amount_residual_currency)
            else:
                # always use the amount booked in the company currency as the
                # basis of the conversion into the voucher currency
                amount_original = currency_pool.compute(
                    line.credit or line.debit or 0.0, currency_id)
                amount_unreconciled = currency_pool.compute(
                    abs(line.amount_residual), currency_id)
            line_currency_id = line.currency_id and line.currency_id.id or company_currency
            rs = {
                'name': line.move_id.name,
                'type': line.credit and 'dr' or 'cr',
                'move_line_id': line.id,
                'account_id': line.account_id.id,
                'amount_original': amount_original,
                'amount': (line.id in move_lines_found) and min(abs(remaining_amount), amount_unreconciled) or 0.0,
                'date': line.date,
                'date_maturity': line.date_maturity,
                'amount_unreconciled': amount_unreconciled,
                'currency_id': line_currency_id,
                'untax_amount': line.invoice_id and line.invoice_id.amount_untaxed
            }
            if ttype == 'inbound':
                if rs['type'] == 'cr':
                    remaining_amount -= rs['amount']
                else:
                    rs['amount'] = 0
            if ttype == 'outbound':
                if rs['type'] == 'dr':
                    remaining_amount -= rs['amount']
                else:
                    rs['amount'] = 0
            # in case a corresponding move_line hasn't been found, we now try to assign the voucher amount
            # on existing invoices: we split voucher amount by most old first,
            # but only for lines in the same currency
            if not move_lines_found:
                if currency_id.id == line_currency_id:
                    if line.credit:
                        amount = min(amount_unreconciled, abs(total_debit))
                        rs['amount'] = amount
                        total_debit -= amount
                    else:
                        amount = min(amount_unreconciled, abs(total_credit))
                        rs['amount'] = amount
                        total_credit -= amount
            if ttype == 'inbound' and rs['type'] == 'cr':
                if rs['amount_unreconciled'] == rs['amount']:
                    rs['reconcile'] = True
            if ttype == 'outbound' and rs['type'] == 'dr':
                if rs['amount_unreconciled'] == rs['amount']:
                    rs['reconcile'] = True

            if rs['type'] == 'cr':
                default['value']['line_cr_ids'].append(rs)
            else:
                default['value']['line_dr_ids'].append(rs)

            if len(default['value']['line_cr_ids']) > 0:
                default['value']['pre_line'] = 1
            elif len(default['value']['line_dr_ids']) > 0:
                default['value']['pre_line'] = 1
            default['value']['writeoff_amount'] = self._compute_writeoff_amount(
                default['value']['line_dr_ids'], default['value']['line_cr_ids'], price, ttype)
        return default

    def _create_payment_entry(self, amount, move_name=None):
        """ Create a journal entry corresponding to a payment, if the payment references invoice(s) they are reconciled.
            Return the journal entry.
        """
        aml_obj = self.env['account.move.line'].with_context(
            check_move_validity=False)
        invoice_currency = False
        if self.invoice_ids and all([x.currency_id == self.invoice_ids[0].currency_id for x in self.invoice_ids]):
            # if all the invoices selected share the same currency, record the
            # paiement in that currency too
            invoice_currency = self.invoice_ids[0].currency_id
        debit, credit, amount_currency, currency_id = aml_obj.with_context(date=self.payment_date).compute_amount_fields(
            amount, self.currency_id, self.company_id.currency_id, invoice_currency)

        move = self.env['account.move'].create(self._get_move_vals())

        # Write line corresponding to invoice payment
        counterpart_aml_dict = self._get_shared_move_line_vals(
            debit, credit, amount_currency, move.id, False)
        counterpart_aml_dict.update(
            self._get_counterpart_move_line_vals(move_name))
        counterpart_aml_dict.update({'currency_id': currency_id})
        counterpart_aml = aml_obj.create(counterpart_aml_dict)
        if self._context.get('cr_line_id'):
            line = self.env['account.payment.lines'].browse(
                self._context.get('cr_line_id'))
            for i in range(0, 2):
                debit_tds = line.tds_amount if i == 0 else 0
                credit_tds = line.tds_amount if i == 1 else 0
                account = line.tds_rate.account_id.id if i == 0 else line.account_id.id
                dict_tds = self._get_shared_move_line_vals(
                    debit_tds, credit_tds, amount_currency, move.id, False)
                dict_tds.update({
                    'name': move_name,
                    'account_id': account,
                    'journal_id': self.journal_id.id,
                    'currency_id': self.currency_id != self.company_id.currency_id and self.currency_id.id or False,
                    'payment_id': self.id
                })
                aml_obj.create(dict_tds)
        # Reconcile with the invoices
        if self.payment_difference_handling == 'reconcile' and self.payment_difference:
            writeoff_line = self._get_shared_move_line_vals(
                0, 0, 0, move.id, False)
            amount_currency_wo, currency_id = aml_obj.with_context(date=self.payment_date).compute_amount_fields(
                self.payment_difference, self.currency_id, self.company_id.currency_id, invoice_currency)[2:]
            # the writeoff debit and credit must be computed from the invoice residual in company currency
            # minus the payment amount in company currency, and not from the payment difference in the payment currency
            # to avoid loss of precision during the currency rate computations.
            # See revision 20935462a0cabeb45480ce70114ff2f4e91eaf79 for a
            # detailed example.
            total_residual_company_signed = sum(
                invoice.residual_company_signed for invoice in self.invoice_ids)
            total_payment_company_signed = self.currency_id.with_context(
                date=self.payment_date).compute(self.amount, self.company_id.currency_id)
            if self.invoice_ids[0].type in ['in_invoice', 'out_refund']:
                amount_wo = total_payment_company_signed - total_residual_company_signed
            else:
                amount_wo = total_residual_company_signed - total_payment_company_signed
            # Align the sign of the secondary currency writeoff amount with the sign of the writeoff
            # amount in the company currency
            if amount_wo > 0:
                debit_wo = amount_wo
                credit_wo = 0.0
                amount_currency_wo = abs(amount_currency_wo)
            else:
                debit_wo = 0.0
                credit_wo = -amount_wo
                amount_currency_wo = -abs(amount_currency_wo)
            writeoff_line['name'] = _('Counterpart')
            writeoff_line['account_id'] = self.writeoff_account_id.id
            writeoff_line['debit'] = debit_wo
            writeoff_line['credit'] = credit_wo
            writeoff_line['amount_currency'] = amount_currency_wo
            writeoff_line['currency_id'] = currency_id
            writeoff_line = aml_obj.create(writeoff_line)
            if counterpart_aml['debit']:
                counterpart_aml['debit'] += credit_wo - debit_wo
            if counterpart_aml['credit']:
                counterpart_aml['credit'] += debit_wo - credit_wo
            counterpart_aml['amount_currency'] -= amount_currency_wo
        self.invoice_ids.register_payment(counterpart_aml)

        # Write counterpart lines
        if not self.currency_id != self.company_id.currency_id:
            amount_currency = 0
        liquidity_aml_dict = self._get_shared_move_line_vals(
            credit, debit, -amount_currency, move.id, False)
        liquidity_aml_dict.update(self._get_liquidity_move_line_vals(-amount))
        aml_obj.create(liquidity_aml_dict)
        move.post()
        return move

    def _get_counterpart_move_line_vals(self, move_name=False):
        if self.payment_type == 'transfer':
            name = self.name
        else:
            name = ''
            if self.partner_type == 'customer':
                if self.payment_type == 'inbound':
                    name += _("Customer Payment")
                elif self.payment_type == 'outbound':
                    name += _("Customer Refund")
            elif self.partner_type == 'supplier':
                if self.payment_type == 'inbound':
                    name += _("Vendor Refund")
                elif self.payment_type == 'outbound':
                    name += _("Vendor Payment")
            if move_name:
                name += ': '
                # for inv in invoice:
                #     if inv.move_id:
                #         name += inv.number + ', '
                name += move_name
                # name = name[:len(name)]
        return {
            'name': name,
            'account_id': self.destination_account_id.id,
            'journal_id': self.journal_id.id,
            'currency_id': self.currency_id != self.company_id.currency_id and self.currency_id.id or False,
            'payment_id': self.id,
        }

    def post(self):
        # import pdb
        # pdb.set_trace()
        #res = super(account_payment, self).post()
        # aml_obj = self.env['account.move.line'].with_context(check_move_validity=False)
        move_line_dict = {}
        #lines_to_reconcile = []
        cr_payment_lines = []
        dr_payment_lines = []
        for rec in self:

            if rec.state != 'draft':
                raise UserError(
                    _("Only a draft payment can be posted. Trying to post a payment in state %s.") % rec.state)

            if any(inv.state != 'open' for inv in rec.invoice_ids):
                raise ValidationError(
                    _("The payment cannot be processed because the invoice is not open!"))
            if rec.payment_difference < 0:
                raise ValidationError(
                    _("The payment diffrence cannot be negative!"))

            # Use the right sequence to set the name
            if rec.payment_type == 'transfer':
                sequence_code = 'account.payment.transfer'
            else:
                if rec.partner_type == 'customer':
                    if rec.payment_type == 'inbound':
                        sequence_code = 'account.payment.customer.invoice'
                    if rec.payment_type == 'outbound':
                        sequence_code = 'account.payment.customer.refund'
                if rec.partner_type == 'supplier':
                    if rec.payment_type == 'inbound':
                        sequence_code = 'account.payment.supplier.refund'
                    if rec.payment_type == 'outbound':
                        sequence_code = 'account.payment.supplier.invoice'
            rec.name = self.env['ir.sequence'].with_context(
                ir_sequence_date=rec.payment_date).next_by_code(sequence_code)
            if not rec.name and rec.payment_type != 'transfer':
                raise UserError(
                    _("You have to define a sequence for %s in your company.") % (sequence_code,))

            # Create the journal entry
            print('sssssssssssssssssssssss',rec.amount,rec.line_cr_ids,rec.line_dr_ids)
            total_payment_amount = rec.amount
            if rec.line_cr_ids:  # and rec.payment_type == 'inbound':
                for line in rec.line_cr_ids:
                    if line.amount > 0:
                        amount = line.amount * -1
                        if abs(amount) != 0:
                            ctx = dict(rec._context)
                            # if rec.invoice_ids:
                            if line.tds_amount > 0:
                                ctx.update(
                                    {'not_register_payment': True, 'cr_line_id': line.id})
                            else:
                                ctx.update(
                                    {'not_register_payment': True, 'cr_line_id': False})
                            move = rec.with_context(ctx)._create_payment_entry(
                                amount, line.move_line_id.move_id.name)
                            rec.write(
                                {'state': 'posted', 'move_name': move.name})
                            move_line_dict.update({line.id: move})
                            total_payment_amount -= line.amount
                            if not line.move_line_id.invoice_id:
                                for mline in move.line_ids:
                                    if mline.credit:
                                        cr_payment_lines.append((mline, amount))


            if rec.line_dr_ids:  # and rec.payment_type == 'outbound':
                for line in rec.line_dr_ids:
                    # if total_payment_amount <= 0:
                    #     break
                    amount = line.amount * 1
                    if abs(amount) != 0:
                        ctx = dict(rec._context)
                        ctx.update({'not_register_payment': True})
                        move = rec.with_context(ctx)._create_payment_entry(
                            amount, line.move_line_id.move_id.name)
                        rec.write({'state': 'posted', 'move_name': move.name})
                        move_line_dict.update({line.id: move})
                        total_payment_amount -= line.amount
                        if not line.move_line_id.invoice_id:
                            for mline in move.line_ids:
                                if mline.debit:
                                    dr_payment_lines.append((mline, amount))
                                    
            if total_payment_amount > 0:
                ctx = dict(rec._context)
                ctx.update({'not_register_payment': True})
                move = rec.with_context(ctx)._create_payment_entry(
                    total_payment_amount * (rec.payment_type in ('outbound', 'transfer') and 1 or -1))
                if rec.amount == rec.payment_difference:
                    rec.write({'state': 'posted', 'move_name': move.name})
            if rec.amount == 0:
                if rec.payment_difference > 0:
                    ctx = dict(rec._context)
                    ctx.update({'not_register_payment': True})
                    move = rec.with_context(ctx)._create_payment_entry(
                        rec.payment_difference * (rec.payment_type in ('outbound', 'transfer') and 1 or -1))
                    rec.write({'state': 'posted', 'move_name': move.name})

            # if not rec.line_cr_ids and not rec.line_dr_ids:
            #     print('not++++++++++++++++++')
            #     amount = rec.amount * \
            #         (rec.payment_type in ('outbound', 'transfer') and 1 or -1)
            #     move = rec._create_payment_entry(amount)
            #     rec.write({'state': 'posted', 'move_name': move.name})
            #     print('rec++++++++++++++++++++++=', rec)

            # In case of a transfer, the first journal entry created debited the source liquidity account and credited
            # the transfer account. Now we debit the transfer account and
            # credit the destination liquidity account.
            if rec.payment_type == 'transfer':
                transfer_credit_aml = move.line_ids.filtered(
                    lambda r: r.account_id == rec.company_id.transfer_account_id)
                transfer_debit_aml = rec._create_transfer_entry(amount)
                (transfer_credit_aml + transfer_debit_aml).reconcile()

#         move_lines = self.move_line_ids
#         move_lines_dict = {}

        if self.line_dr_ids:
            for dr_line in self.line_dr_ids:
                if dr_line.move_line_id.invoice_id:
                    inv_rec = dr_line.move_line_id.invoice_id
                    if inv_rec:
                        dr_move = move_line_dict.get(dr_line.id)
                        if dr_move:
                            for line in dr_move.line_ids:
                                if line.debit:
                                    inv_rec.assign_outstanding_credit(line.id)
                                    break
                

        if self.line_cr_ids:
            for cr_line in self.line_cr_ids:
                if cr_line.move_line_id.invoice_id:
                    inv_rec = cr_line.move_line_id.invoice_id
                    if inv_rec:
                        cr_move = move_line_dict.get(cr_line.id)
                        if cr_move:
                            for line in cr_move.line_ids:
                                if line.credit:  # and line.name != 'TDS':
                                    inv_rec.assign_outstanding_credit(line.id)
                                    # break
                
        if dr_payment_lines:
            payment = dr_payment_lines[0][0]
            payment_amount = dr_payment_lines[0][1]
            curr_payment_ind = 0
            for line  in self.line_dr_ids :
                if not line.move_line_id.invoice_id and line.amount > 0:
                    available_amount = line.amount
                    while payment:
                        (payment + line.move_line_id).reconcile()
                        if payment_amount >= available_amount:
                            payment_amount -= available_amount                            
                            break
                        else:
                            available_amount -= payment_amount
                            if curr_payment_ind == len(dr_payment_lines) - 1:
                                payment = False
                                break
                            else:
                                curr_payment_ind += 1
                                payment = dr_payment_lines[curr_payment_ind][0]
                                payment_amount = dr_payment_lines[curr_payment_ind][1]


        if cr_payment_lines:
            payment = cr_payment_lines[0][0]
            payment_amount = cr_payment_lines[0][1]
            curr_payment_ind = 0
            for line  in self.line_cr_ids :
                if not line.move_line_id.invoice_id and line.amount > 0:
                    available_amount = line.amount
                    print('payment+++++++++++++++++++', payment)
                    while payment:

                        
                        (payment + line.move_line_id).reconcile()
                        if payment_amount >= available_amount:
                            payment_amount -= available_amount                            
                            break
                        else:
                            available_amount -= payment_amount
                            if curr_payment_ind == len(cr_payment_lines) - 1:
                                payment = False
                                break
                            else:
                                curr_payment_ind += 1
                                payment = cr_payment_lines[curr_payment_ind][0]
                                #payment = cr_payment_lines[curr_payment_ind][1]
        return True


class account_payment_lines(models.Model):
    _name = 'account.payment.lines'
    _description = 'Payment Lines'
    _order = "date"

    def _compute_balance(self):
        context = self._context
        currency_pool = self.env['res.currency']
        rs_data = {}
        for line in self.browse(self.ids):
            ctx = context.copy()
            voucher_rate = self.env['res.currency'].search(
                [('id', '=', line.payment_id.currency_id.id)]).rate
            res = {}
            company_currency = line.payment_id.journal_id.company_id.currency_id.id
            voucher_currency = line.payment_id.currency_id and line.payment_id.currency_id.id or company_currency
            move_line = line.move_line_id or False

            if not move_line:
                res['amount_original'] = 0.0
                res['amount_unreconciled'] = 0.0
            elif move_line.currency_id and voucher_currency == move_line.currency_id.id:
                res['amount_original'] = abs(move_line.amount_currency)
                res['amount_unreconciled'] = abs(
                    move_line.amount_residual_currency)
            else:
                # always use the amount booked in the company currency as the
                # basis of the conversion into the voucher currency
                res['amount_original'] = currency_pool.compute(
                    (move_line.credit or move_line.debit) * 1.0 or 0.0, voucher_currency, False)
                res['amount_unreconciled'] = currency_pool.compute(
                    abs(move_line.amount_residual), voucher_currency, False)

            rs_data[line.id] = res
        return rs_data

    def _currency_id(self):
        '''
        This function returns the currency id of a voucher line. It's either the currency of the
        associated move line (if any) or the currency of the voucher or the company currency.
        '''
        res = {}
        for line in self.browse(self.ids):
            move_line = line.move_line_id
            if move_line:
                res[line.id] = move_line.currency_id and move_line.currency_id.id or move_line.company_id.currency_id.id
            else:
                res[line.id] = line.voucher_id.currency_id and line.voucher_id.currency_id.id or line.voucher_id.company_id.currency_id.id
        return res

    def _check_tds_paid(self):
        for rec in self:
            if rec.move_line_id and rec.move_line_id.invoice_id and rec.move_line_id.invoice_id.tds_paid:
                rec.tds_paid = rec.move_line_id.invoice_id.tds_paid

    payment_id = fields.Many2one('account.payment', 'Payment')
    name = fields.Char('Description')
    account_id = fields.Many2one('account.account', 'Account', required=True)
    partner_id = fields.Many2one(
        'res.partner', related='payment_id.partner_id', string='Partner')
    date = fields.Date('Date')
    date_maturity = fields.Date('Due Date')
    amount_original = fields.Float(
        compute='_compute_balance', multi='dc', string='Original Amount', store=True)
    amount_unreconciled = fields.Float(
        compute='_compute_balance', multi='dc', string='Open Balance', store=True)
    untax_amount = fields.Float('Untax Amount')
    tds_amount = fields.Float('TDS Amount')
    # tds_rate = fields.Float('TDS Rate')
    tds_rate = fields.Many2one('account.tax', 'TDS Rate', domain=([
        ('tax_group_id.name', '=', 'TDS')
    ]))
    reconcile = fields.Boolean('Full Reconcile')
    type = fields.Selection([('dr', 'Debit'), ('cr', 'Credit')], 'Dr/Cr')
    amount = fields.Float('Allocation')
    currency_id = fields.Float(
        compute='_currency_id', string='Currency', readonly=True)
    company_id = fields.Many2one(
        'res.company', related='payment_id.company_id', string='Company')
    move_line_id = fields.Many2one(
        'account.move.line', 'Journal Item', copy=False)
    tds_paid = fields.Boolean("TDS Paid")  # , compute='_check_tds_paid')
#     state = fields.Selection('payment_id.state', string='State', readonly=True)

    @api.onchange('reconcile')
    def onchange_reconcile(self):
        # self.amount = 0.0
        if self.reconcile:
            self.amount = self.amount_unreconciled

    @api.onchange('amount', 'amount_unreconciled')
    def onchange_amount(self):
        if self.amount:
            self.reconcile = (self.amount == self.amount_unreconciled)
        amt = self.tds_amount + self.amount
        if amt > self.amount_unreconciled:
            raise UserError(_("Allocation amount is geater than Open balance"))

    @api.onchange('move_line_id')
    def onchange_move_line_id(self):
        res = {}
        move_line_pool = self.env['account.move.line']
        if self.move_line_id:
            move_line = move_line_pool.browse(move_line_id)
            if move_line.credit:
                self.type = 'dr'
            else:
                self.type = 'cr'
            self.account_id = move_line.account_id.id
            self.currency_id = move_line.currency_id and move_line.currency_id.id or move_line.company_id.currency_id.id,

    @api.onchange('tds_rate')
    def onchange_tds_rate(self):
        # am = 0.0
        for rec in self:
            rec.tds_amount = round((rec.tds_rate.amount * rec.untax_amount) / 100,2)
            amt = rec.tds_amount + rec.amount
            if amt > rec.amount_unreconciled:
                amount = rec.amount_unreconciled - rec.tds_amount
                rec.amount = amount
                rec.update({'amount': amount})
            # if rec.amount:
            #     self.reconcile = (self.amount == self.amount_unreconciled)

    @api.onchange('tds_amount')
    def onchange_tds_amount(self):
        # am = 0.0
        for rec in self:
            amt = rec.tds_amount + rec.amount
            # if rec.tds_amount > 0:
            if amt > rec.amount_unreconciled:
                amount = rec.amount_unreconciled - rec.tds_amount
                rec.amount = amount

class account_journal(models.Model):
    _inherit = "account.journal"

    @api.model
    def name_search(self, name, args=None, operator='ilike', limit=100):
        if self._context.get('journal_company_id'):
            journal_ids = self.search([
                ('company_id', '=', self._context.get('journal_company_id')),
                ('type', 'in', ('bank', 'cash'))])
            args = [('id', 'in', journal_ids.ids)]
        return super(account_journal, self).name_search(name, args, operator=operator, limit=limit)


class AccountAccount(models.Model):
    _inherit = "account.account"

    @api.model
    def name_search(self, name, args=None, operator='ilike', limit=100):
        if self._context.get('account_company_id'):
            account_ids = self.env['account.account'].search([
                ('company_id', '=', self._context.get('account_company_id')),
                ('deprecated', '=', False)])
            args = [('id', 'in', account_ids.ids)]
        return super(AccountAccount, self).name_search(name, args, operator=operator, limit=limit)


class PaymenType(models.Model):
    _name = 'payment.type'

    name = fields.Char("Payment Type")
    active = fields.Boolean("Active")


class Partner(models.Model):
    _inherit = 'res.partner'

    @api.model
    def name_search(self, name, args=None, operator='ilike', limit=100):
        if self._context.get('default_is_company'):
            payment_ids = self.search([
                ('is_company', '=', True),
                ('parent_id', '=', False)])
            args = [('id', 'in', payment_ids.ids)]
        return super(Partner, self).name_search(name, args, operator=operator, limit=limit)
