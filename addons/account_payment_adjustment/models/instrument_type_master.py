from odoo import _, api, fields, models


class InstrumentTypeMaster(models.Model):
    _name = "instrument.type"

    name = fields.Char(string="Instrument Type")
    active = fields.Boolean("Active")
