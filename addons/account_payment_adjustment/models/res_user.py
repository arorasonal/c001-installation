from odoo import models, fields, api, _

class User(models.Model):
    _inherit = "res.users"

    @api.multi
    def button_invoice_update(self):
        invoice_ids = self.env['account.invoice'].search([
            ('state', '=', 'open')
        ])
        print"invoice_id===",len(invoice_ids)
        count = 0
        for invoice in invoice_ids:
            invoice.move_id.button_cancel()
            for journal in invoice.move_id.line_ids:#.filtered(lambda x: x.invoice_id or  x.date_maturity):
                journal.invoice_id = invoice.id
                journal.date_maturity = invoice.date_invoice
                invoice.move_id.date = invoice.date_invoice
                print"journal++++++++++++++", invoice.id
            invoice.move_id.post()
            count += 1
            print"count++++++++++++++++++", count
        print"******************done*********************"
        return True
