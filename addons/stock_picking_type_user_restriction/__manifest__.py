# -*- coding: utf-8 -*-
{
    "name": "Stock picking type - Restrict users",
    "summary": "Restrict some users to see and use only certain picking types",
    "version": "12.0.1.0.0",
    "development_status": "Beta",
    "category": "Warehouse",
    "website": "https://github.com/OCA/stock-logistics-warehouse",
    "author": "TAKOBI, Odoo Community Association (OCA)",
    "maintainers": ["eLBati"],
    "license": "LGPL-3",
    "application": False,
    "installable": True,
    "depends": [
        "stock",
    ],
    "data": [
        "security/stock_security.xml",
        "views/stock_picking_type_views.xml",
    ],
}