{
    'name': 'Employee Joining',
    'version': '1.0',
    'category': 'Human Resources',
    'summary': 'This application allows management of joining/on-boarding of new employees in the company.',
    'description': """

    """,
    "author": "Apagen Solutions Pvt. Ltd.",
    'website': 'http://www.apagen.com',
    'depends': [
        'base',
        'hr',
        'hr_recruitment',
        'hr_employee_register'
    ],
    'data': [
        'security/ir.model.access.csv',
        'data/joining_data.xml',
        'views/emp_joining_view.xml',
        'views/emp_joining_sequence.xml',
        'views/employee_joining_data.xml',
        'wizard/turn_over_report_wiz_view.xml',
        'views/email_template.xml',
    ],
    'test': [
    ],
    'demo': [
    ],
    'installable': True,
    'auto_install': False,
    'application': True,
}
