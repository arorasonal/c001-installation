# -*- coding: utf-8 -*-
from odoo import api, fields, models, tools
from odoo.tools.translate import _
from cStringIO import StringIO
import base64
import xlwt


class TurnOverReport(models.Model):
    _name = 'turn.over.report'
    _rec_name = 'name'

    name = fields.Char("Turn over Report", default="Turn over Report")
    company_id = fields.Many2one(
        'res.company',
        'Company',
        default=lambda self: self.env['res.company']._company_default_get('turn.over.report'),
        required=True)
    start_date = fields.Date("Start Date", required=True)
    end_date = fields.Date("End Date", required=True)
    no_of_joining = fields.Integer("Staff Establishment")
    no_of_exit = fields.Integer("No. of Exits")
    turnover = fields.Float("Turnover %")

    @api.multi
    def print_emp_sevice_length_report(self):

        total_ids = 0
        total_ids_del = 0
        joining_count = 0.0
        exit_count = 0.0
        turn_o_r = self.env['turn.over.report']
        turn_o_r_ids = turn_o_r.search([]) or []
        print "turn_o_r_ids", turn_o_r_ids
        for turn_o_r_id in turn_o_r.browse(turn_o_r_ids):
            total_ids = total_ids + 1
        for turn_o_r_id in turn_o_r.browse(turn_o_r_ids):
            total_ids_del = total_ids_del + 1
            if total_ids_del < total_ids:
                turn_o_r.unlink(turn_o_r_id.id)

        # self.env['turn.over.report').unlink(turn_o_r_ids)
        ir_model_data = self.env['ir.model.data']
        emp_joining = self.env['joining']
        emp_exit = self.env['exit']
        emp_joining_ids = emp_joining.search([]) or []
        emp_exit_ids = emp_exit.search([]) or []
        for emp_joining_id in emp_joining.browse(emp_joining_ids):
            if self.start_date <= emp_joining_id.joining_date and self.end_date >=  emp_joining_id.joining_date:
                if emp_joining_id.state == "w_c_a" or emp_joining_id.state == "closed" or emp_joining_id.state == "induction":
                    joining_count = joining_count + 1

        for emp_exit_id in emp_exit_ids:
            if self.start_date <= emp_exit_id.exit_date and emp_exit_id.exit_date <= self.end_date:
                if emp_exit_id.state == "done":
                    exit_count = exit_count + 1

        self.no_of_joining = joining_count
        self.no_of_exit = exit_count
        if joining_count > 0.0:
            self.turnover = (exit_count * 100) / joining_count
        template_id = ir_model_data.get_object_reference('employee_joining', 'view_turn_over_report')[1]            
        for data in self:
            company_id = data.company_id.id
            self.env.context.update({
                'company': data.company_id.name,
                'from_date': data.start_date,
                'to_date': data.end_date,
            })

        return {
            'name': _('Turn over Report'),
            'view_type': 'form',
            'view_mode': 'tree',
            'res_model': 'turn.over.report',
            'type': 'ir.actions.act_window',
            'view_id': template_id,
            'target': 'current',
            'context': self.env.context,
            # 'domain': [('company', '=', company_id)],
        }

    '''def print_stock_valuation(self, ids, context=None):
        if context is None:
            context= {}

        joining_obj = self.env['joining')

        domain = [
            ('company', '=', self.company_id.id)
        ]
        data = self.read(ids)[0]
        joining_ids = joining_obj.search(cr, uid,[]) or []
        datas = {
             'ids': joining_ids,
             'model': 'joining',
#             'form': data
        }
        return {
            'type': 'ir.actions.report.xml',
            'report_name': 'length_of_service_aeroo_report',
            'datas': datas,
        }'''
