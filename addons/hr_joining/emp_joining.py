from odoo import api, fields, models, tools
from datetime import datetime
# from odoo import netsvc
# from odoo import pooler
from odoo.tools.translate import _
# from json import dumps
from odoo.exceptions import UserError


class Joining(models.Model):
    _name = 'joining'
    _description = 'Employee Joining'
    _inherit = 'mail.thread'
    _rec_name = 'employee_id'
    _order = 'id DESC'

    def _default_employee(self):
        return self.env.context.get('default_employee_id') or self.env['hr.employee'].search([('user_id', '=', self.env.uid)], limit=1)

    @api.multi
    def _current_employee_get(self):
        ids = self.env['hr.employee'].search([('user_id', '=', self.env.uid)])
        if ids:
            return ids[0]
        return False

    @api.multi
    def _length_of_service(self):
        res = {}
        employee_object = self.env['hr.employee']
        for record_id in self:
            if record_id.employee_id:
                lenghth_of_service = employee_object.get_service_duration(
                    record_id)
                res[record_id.id] = lenghth_of_service
        return res

    state = fields.Selection(
        [('draft', "Draft"),
         ('in_progress', "In Progress"),
         ('induction', "Induction"),
         ('hr_review', "HR Review"),
         ('done', "Done"),
         ('cancelled', "Cancelled")], "Status", default='draft', track_visibility='onchange')
    employee_id = fields.Many2one(
        'hr.employee',
        'Employee',
        required=True)
    designation_id = fields.Many2one(
        'hr.job',
        'Designation',
        related='employee_id.job_id',
        required=True,
        readonly=True)
    department_id = fields.Many2one(
        'hr.department',
        'Department',
        related='employee_id.department_id',
        required=True,
        readonly=True)
    joining_date = fields.Date(
        'Joining Date',
        related='employee_id.date_of_joining',
        required=True,
        readonly=True)
    emp_joining_ref = fields.Char('Employee Joining Reference')
    joining_sum = fields.Text('Joining Summary')
    cancellation = fields.Text('Cancellation Summary')
    emp_exit_date = fields.Date("Exit Date")
    company = fields.Many2one("res.company", 'Company')
    length_of_service = fields.Char(
        compute='_length_of_service',
        store=True,
        string="Length of Service")
    staff_no = fields.Char('Staff No.')
    parent_id = fields.Many2one(
        'hr.employee',
        'Manager',
        store=True)
    employment_date = fields.Date('Employment Date')
    active = fields.Boolean('Active', default=True)
    responsible = fields.Many2one(
        'res.users', 'Responsible', track_visibility='onchange', default=lambda self: self.env.uid, readonly=True)
    creation_date = fields.Datetime(
        'Creation Date', default=fields.Datetime.now, readonly=True)
    start_date = fields.Date('Start Date', readonly=True)
    end_date = fields.Date('End Date', readonly=True)
    joining_checklist_id = fields.One2many(
        'joining.checklist', 'joining_id', 'Joining Checklist')

    @api.multi
    def _track_subtype(self, init_values):
        self.ensure_one()
        if 'state' in init_values and self.state == 'in_progress':
            return 'hr_joining.joining_mail_track'
        return super(Joining, self)._track_subtype(init_values)

    @api.multi
    @api.onchange("employee_id")
    def onchange_employee_id(self):
        check_list = []
        val = {}
        checklist = self.env['joining.checklist.conf']
        checklist_ids = checklist.search([('active', '=', True)])
        for check in checklist_ids:
            check_list.append((0, 0, {
                'name': check.id,
                'responsible_employee': self.env['hr.employee'].search([
                    ('user_id', '=', self.env.uid)]),
                'deadline': datetime.now(),
                'status': 'pending'}))
            self.joining_checklist_id = check_list
        # for line in self.joining_checklist_id :
        #     if self.employee_id:
        #         line.responsible_employee = self.employee_id

    @api.multi
    def unlink(self):
        """Allows to delete hr draft"""
        for rec in self:
            if rec.state not in ['draft']:
                raise UserError(
                    'You can only delete in draft state')
        return super(Joining, self).unlink()

    @api.model
    def copy(self, default=None):
        if not default:
            default = {}
        default.update({
            'state': 'in_progress',
            'emp_joining_ref': self.env['ir.sequence'].next_by_code('medical.premium')})

    @api.multi
    def action_draft(self):
        self.write({'state': 'draft'})
        return True

    @api.multi
    def action_inprogress(self):
        current_date = fields.Datetime.now()
        emp_joining_ref = self.env['ir.sequence'].next_by_code('joining')
        self.write({'state': 'in_progress', 'emp_joining_ref': emp_joining_ref,
                    'start_date': current_date})
        for order in self:
            if order.state == "in_progress":
                self.ensure_one()
                for line in self.joining_checklist_id:
                    line.send_mail_responsible()
            for data in self.joining_checklist_id:
                if data.deadline == 0:
                    raise UserError(
                        "Please Fill Deadline in Joining Checklist")
                if data.status == 0:
                    raise UserError("Please Fill Status in Joining Checklist")
        return True

    @api.multi
    def action_induction(self):
        self.write({'state': 'induction'})
        return True

    @api.multi
    def action_hr_review(self):
        self.write({'state': 'hr_review'})
        return True

    @api.multi
    def action_done(self):
        current_date = fields.Datetime.now()
        self.write({'state': 'done', 'end_date': current_date})
        for rec in self:
            if rec.joining_sum == 0:
                raise UserError(
                    "Joining Summary is needed to be filled before You move to done.")
        for data in self.joining_checklist_id:
            for status in data:
                if status.status not in ["done", "not_applicable"]:
                    raise UserError(
                        "Joining Checklist enteries all Status is not done")
        return True

    @api.multi
    def action_cancel(self):
        for value in self:
            if value.cancellation == 0:
                raise UserError('Please enter reason of cancellation')
        else:
            self.write({'state': 'cancelled'})
            return True


class JoiningChecklistConf(models.Model):
    _name = 'joining.checklist.conf'
    _rec_name = 'namecheck'

    namecheck = fields.Char('Name')
    active = fields.Boolean('Active', default=True)


class JoiningChecklist(models.Model):
    _name = 'joining.checklist'

    name = fields.Many2one('joining.checklist.conf', 'Name')
    joining_id = fields.Many2one('joining', 'Joining')
    employee_id = fields.Many2one('hr.employee', 'Employee')
    # responsible = fields.Many2one('res.users', 'Responsible', default=lambda self: self.env.uid)
    responsible_employee = fields.Many2one('hr.employee', 'Responsible')
    deadline = fields.Datetime('Deadline', default=fields.Datetime.now())
    status = fields.Selection([
        ('pending', 'Pending'),
        ('in_progress', 'In Progress'),
        ('done', 'Done'),
        ('hold', 'Hold'),
        ('not_applicable', "Not Applicable")
    ], 'Status', default="pending")
    ir_attachment_joining = fields.Many2many(
        'ir.attachment', 'rel_joining_attachment',
        'joining_id', 'attachment_joining_form', 'Attachment')
    remarks = fields.Char('Remarks')

    @api.multi
    def send_mail_responsible(self):
        template = self.env.ref(
            'hr_joining.send_for_responsible')
        user_obj = self.env['res.users'].search([('id', '=', 1)])
        if template:
            for joining in self:
                if joining.responsible_employee:
                    template.email_to = joining.responsible_employee.work_email or ''
                    template.email_from = user_obj.login
                    template.send_mail(self.id, force_send=True)
        return True
