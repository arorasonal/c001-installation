from odoo import api, fields, models
from datetime import datetime, timedelta
import datetime
import time
from dateutil.rrule import rrule
from dateutil.rrule import DAILY
from odoo.exceptions import UserError
from odoo.tools.translate import _


class AttendanceSheet(models.Model):
    _name = "hr.attendance.sheet"
    _inherit = ['mail.thread']

    employee_id = fields.Many2one(
        'hr.employee',
        string="Employee",
        required=True,
        default=lambda self: self.env['hr.employee'].search([('user_id', '=', self.env.uid)]))
    department_id = fields.Many2one(
        'hr.department',
        'Department', required=True)
    manager_id = fields.Many2one(
        'hr.employee', 'Manager', required=True)
    date_from = fields.Date(
        'From Date', required=True)
    date_to = fields.Date(
        'To Date', required=True)
    name = fields.Char('Name', compute="add_name")
    attendance_sheet_ids = fields.One2many("hr.attendance.lines", "attend_id",
                                           string="Attendance Sheet Lines")
    late_not = fields.Integer("Number of times")
    late_hours = fields.Char("Hours")
    early_going_not = fields.Integer("Number of times")
    early_hours = fields.Char("Hours")
    overtime_not = fields.Integer("Number of times")
    overtime_hours = fields.Char("Hours")
    state = fields.Selection([
        ('draft', 'Draft'),
        ('waiting_manager_approval', 'Awaiting Manager Approval'),
        ('waiting_hr_approval', 'Awaiting Hr Approval'),
        ('approve', 'Approved'),
        ('refused', 'Refused')
    ], default='draft', track_visibility='onchange')
    refused = fields.Text('Reason For Refusal/Cancellation')

    @api.onchange('employee_id')
    def onchange_employee_id(self):
        """Onchange Employee Method."""
        if self.employee_id:
            self.department_id = self.employee_id.department_id.id
            self.manager_id = self.employee_id.parent_id.id

    @api.multi
    def unlink(self):
        """Allows to delete hr draft"""
        for rec in self:
            if rec.state not in ['draft']:
                raise UserError(
                    'You can only delete in draft state')
        return super(AttendanceSheet, self).unlink()

    @api.model
    def create(self, vals):
        """Create Method."""
        employee = self.env['hr.employee'].search([
            ('id', '=', vals.get('employee_id'))
        ])
        attendance_emp_id = self.search(['&', '&', '&',
                                         ('date_from', '<=', vals.get('date_from')),
                                         ('date_to', '>=', vals.get('date_from')),
                                         ('id', '!=', vals.get('id')),
                                         ('employee_id', "=",
                                          vals.get('employee_id'))
                                         ])
        if attendance_emp_id:
            raise UserError(
                "Attendance Sheet can not be generated with the same date interval")
        if employee:
            vals['department_id'] = employee.department_id.id
            vals['manager_id'] = employee.parent_id.id
        return super(AttendanceSheet, self).create(vals)

    @api.multi
    def write(self, vals):
        """Write Method."""
        employee = self.env['hr.employee'].search([
            ('id', '=', vals.get('employee_id'))
        ])
        if employee:
            vals.update({
                'department_id': employee.department_id.id,
                'manager_id': employee.parent_id.id
            })
        return super(AttendanceSheet, self).write(vals)

    @api.multi
    def add_name(self):
        for val in self:
            if (val.employee_id and val.date_from and val.date_to):
                val.name = "Attendance sheet of " + str(
                    val.employee_id.name) + " From " + str(val.date_from) + " To " + str(val.date_to)
            else:
                val.name = "Attendance sheet of " + str(
                    val.employee_id.name) + " From FALSE To FALSE "

    @api.multi
    def action_submit_button(self):
        """employee submit Method."""
        if self.state == "draft":
            self.write({'state': 'waiting_manager_approval'})

    @api.multi
    def action_manager_approve(self):
        """Manager submit Method."""
        if self.state == "waiting_manager_approval":
            self.write({'state': 'waiting_hr_approval'})

    @api.multi
    def action_reset_to_draft(self):
        if self.state == "waiting_manager_approval":
            self.write({'state': 'draft'})

    @api.multi
    def action_hr_button(self):
        if self.state == "waiting_hr_approval":
            self.write({'state': 'approve'})

    @api.multi
    def action_reset_to_draft_hr(self):
        if self.state == "waiting_hr_approval":
            self.write({'state': 'draft'})

    @api.multi
    def action_manager_refuse(self):
        for value in self:
            if value.refused == 0:
                raise UserError('Please enter reason for Cancellation')
            else:
                if self.state == "waiting_manager_approval":
                    self.write({'state': 'refused'})

    @api.multi
    def action_hr_refuse(self):
        for value in self:
            if value.refused == 0:
                raise UserError('Please enter reason for Cancellation')
            else:
                if self.state == "waiting_hr_approval":
                    self.write({'state': 'refused'})

    @api.multi
    def get_days_week(self, days):
        dayofweek = ''
        if days == 'Monday':
            dayofweek = '0'
        elif days == 'Tuesday':
            dayofweek = '1'
        elif days == 'Wednesday':
            dayofweek = '2'
        elif days == 'Thursday':
            dayofweek = '3'
        elif days == 'Friday':
            dayofweek = '4'
        elif days == 'Saturday':
            dayofweek = '5'
        elif days == 'Sunday':
            dayofweek = '6'
        return dayofweek

    @api.multi
    def generate_attendance_sheet(self):
        attendance_emp_id = self.search(['&', '&', '&',
                                         ('date_from', '<=', self.date_from),
                                         ('date_to', '>=', self.date_from),
                                         ('id', '!=', self.id),
                                         ('employee_id', "=", self.employee_id.id)
                                         ])
        if not attendance_emp_id:
            late_come_hours = 0
            early_go_hours = 0
            late_come = []
            early_go = []
            over_time = []
            date1 = self.date_from
            date2 = self.date_to
            start = datetime.datetime.strptime(date1, '%Y-%m-%d')
            end = datetime.datetime.strptime(date2, '%Y-%m-%d')
            result = []
            if self.attendance_sheet_ids:
                self.attendance_sheet_ids.unlink()
            for dt in rrule(DAILY, dtstart=start, until=end):
                days = dt.strftime("%A")
                dayofweek = self.get_days_week(days)
                contract_ids = self.env['hr.contract'].search([
                    ('employee_id', '=', self.employee_id.id)])
                cal_id = contract_ids.resource_calendar_id
                resource = self.env['resource.calendar.attendance'].search([
                    ('calendar_id', '=', cal_id.id),
                    ('dayofweek', '=', dayofweek)])
                query = '''
                select
                    check_in,
                    check_out
                from
                    hr_attendance
                where
                    employee_id = %s and
                    (check_in)::date between '%s' and '%s'
                    order by check_in
                ''' % (self.employee_id.id, dt.date(), dt.date())
                self._cr.execute(query)
                recs = self._cr.fetchall()
                if recs:
                    act_sign_in = recs[0][0]
                    act_sign_out = recs[-1][1]
                    sign_in = act_sign_in
                    sign_out = act_sign_out
                    rec_sign_in = datetime.datetime.strptime(
                        sign_in, '%Y-%m-%d %H:%M:%S') + timedelta(
                        hours=5, minutes=30)
                    rec_sign_out = datetime.datetime.strptime(
                        sign_out, '%Y-%m-%d %H:%M:%S') + timedelta(
                        hours=5, minutes=30)
                    sign_in_time = rec_sign_in.time()
                    sign_out_time = rec_sign_out.time()
                    sign_time = datetime.timedelta(hours=sign_in_time.hour,
                                                   minutes=sign_in_time.minute,
                                                   seconds=sign_in_time.second)
                    off_time = datetime.timedelta(hours=sign_out_time.hour,
                                                  minutes=sign_out_time.minute,
                                                  seconds=sign_out_time.second)
                    plan_time = str(resource.hour_from)
                    out_time = str(resource.hour_to)
                    hr_min = plan_time.split('.')
                    hr_minute = out_time.split('.')
                    time1 = timedelta(
                        hours=int(hr_min[0]), minutes=int(hr_min[1]))
                    time2 = timedelta(
                        hours=int(hr_minute[0]), minutes=int(hr_minute[1]))
                    if sign_time > time1:
                        late_in = str(sign_time - time1)
                        if late_in > 0:
                            late_come.append(late_in)
                            total_secs = 0
                            for tm in late_come:
                                time_parts = [int(s) for s in tm.split(':')]
                                total_secs += (time_parts[0] * 60 +
                                               time_parts[1]) * 60 + time_parts[2]
                            total_secs, sec = divmod(total_secs, 60)
                            hr, min = divmod(total_secs, 60)
                            late_come_hours = "%d:%02d:%02d" % (hr, min, sec)
                            no_of_late_in = len(late_come)
                            self.late_not = no_of_late_in
                            self.late_hours = str(late_come_hours)
                    else:
                        late_in = "0:00:00"
                    if (time2 > off_time) and (rec_sign_out.date() <= rec_sign_in.date()):
                        early_out = str(time2 - off_time)
                        if early_out > 0:
                            early_go.append(early_out)
                            total_secs1 = 0
                            for eg in early_go:
                                time_parts = [int(e) for e in eg.split(':')]
                                total_secs1 += (time_parts[0] * 60 +
                                                time_parts[1]) * 60 + time_parts[2]
                            total_secs1, sec = divmod(total_secs1, 60)
                            hr, min = divmod(total_secs1, 60)
                            early_go_hours = "%d:%02d:%02d" % (hr, min, sec)
                            no_early_go = len(early_go)
                            self.early_going_not = no_early_go
                            self.early_hours = str(early_go_hours)
                    else:
                        early_out = "0:00:00"
                    total_over_time = 0.0
                    if (rec_sign_out.date() == rec_sign_in.date()) and ((time1 > sign_time or time2 < off_time)):
                        total_sign_time = off_time - sign_time
                        total_act_time = time2 - time1
                        if total_sign_time > total_act_time:
                            total_over_time = str(
                                total_sign_time - total_act_time)
                            if (total_over_time > 0):
                                over_time.append(total_over_time)
                                total_secs2 = 0
                                for ot in over_time:
                                    time_parts = [int(o)
                                                  for o in ot.split(':')]
                                    total_secs2 += (time_parts[0] * 60 +
                                                    time_parts[1]) * 60 + time_parts[2]
                                total_secs2, sec = divmod(total_secs2, 60)
                                hr, min = divmod(total_secs2, 60)
                                over_time_hours = "%d:%02d:%02d" % (
                                    hr, min, sec)
                                over_time_cal = len(over_time)
                                self.overtime_not = over_time_cal
                                self.overtime_hours = str(over_time_hours)
                    else:
                        total_over_time = "0.00.00"

                    result.append((0, 0, {
                        'plan_sign_in': resource.hour_from,
                        'plan_sign_out': resource.hour_to,
                        'act_sign_in': str(sign_in_time),
                        'act_sign_out': str(sign_out_time),
                        'attendance_days': days,
                        'attendance_date': dt,
                        'late_attended': late_in,
                        'early_leave': early_out,
                        'attend_overtime': total_over_time
                    }))
            self.attendance_sheet_ids = result
        else:
            raise UserError(
                "Attendance Sheet can not be generated with the same date interval")


class AttendanceLines(models.Model):
    _name = "hr.attendance.lines"

    attendance_date = fields.Date("Date")
    attendance_days = fields.Char("Day")
    plan_sign_in = fields.Float("Planned Sign In")
    plan_sign_out = fields.Float("Planned Sign Out")
    act_sign_in = fields.Char("Actual Sign In")
    act_sign_out = fields.Char("Actual Sign Out")
    late_attended = fields.Char("Late In")
    attend_overtime = fields.Char("Overtime")
    early_leave = fields.Char("Early Going")
    attend_id = fields.Many2one("hr.attendance.sheet", "Employee Attendance")


class HrAttendance(models.Model):
    _inherit = "hr.attendance"

    state = fields.Selection([('draft', "Draft"),
                              ('submitted', "Submitted")],
                             "State", track_visibility='always',
                             default='draft')
    employee_code = fields.Char(string='Employee Code')
    biomatric_id = fields.Integer(
        'Biomatric Code',
        size=15)

    @api.multi
    def submit(self):
        self.write({'state': 'submitted'})
