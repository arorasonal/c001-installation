# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2010 Tiny SPRL (<http://tiny.be>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

{
    'name': 'Employee Attendance',
    'version': '10.0.0.1',
    'sequence': 21,
    'summary': 'Access rights and menu',
    'category': 'HR',
    'author': 'Apagen Solutions Pvt. Ltd.',
    'website': 'http://www.apagen.com',
    'depends': [
        'hr_attendance',
        'base',
        'hr',
        'base_hr'
    ],
    'data': [
        'views/employee_attendance_view.xml',
        'views/emp_attendance_view.xml',
        'security/security.xml',
        'security/ir.model.access.csv',
    ],
    'css': ['static/src/css/*.css'],
    'installable': True,
    'application': True,
    'auto_install': False,
}

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
