{
    'name': 'Employee Performance Management',
    'version': '0.1',
    'author': 'Apagen Solutions Pvt. Ltd.',
    'sequence': 10,
    'category': 'Human Resource',
    'website': 'https://www.apagen.com',
    'summary': 'It describes the employee performance Evaluation by KRA/Value Rating',
    'description': """
""",
    'depends': [
        'base', 'hr', 'hr_employee_register', 'hr_leaves'
    ],
    'data': [
        'sequence/kra_sequence_view.xml',
        'security/ir.model.access.csv',
        'views/kra.xml',
        'views/kracofigform.xml',
        'views/jobconfig.xml',
        'wizard/wizard_kra.xml',
        'views/email_view.xml',
        'security/ir_rule.xml',
    ],
    'installable': True,
    'application': True,
    'auto_install': False,
    
}
