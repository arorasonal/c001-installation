from openerp import api, fields, models, _
from datetime import datetime, date


class KraWizard(models.TransientModel):
    _name = 'kra.wizard'

    month = fields.Selection([
        ('january', 'January'),
        ('february', 'February'),
        ('march', 'March'),
        ('april', 'April'),
        ('may', 'May'),
        ('june', 'June'),
        ('july', 'July'),
        ('august', 'August'),
        ('september', 'September'),
        ('october', 'October'),
        ('november', 'November'),
        ('december', 'December'),
    ], string="Month")
    kra_quarter = fields.Selection([('first quarter', 'First Quarter'), (
        'second quarter', 'Second Quarter')], string="Kra Quarter")
    year = fields.Char(
        'Year', required=True, default=datetime.now().strftime('%Y')
    )
    employee_ids = fields.Many2many('hr.employee',
                                    string='Employees',
                                    )
    all_employees = fields.Boolean('All Employees')

    @api.multi
    def create_emp_kra(self):
        for record in self:
            kra_obj = self.env['kra.kra'].create({
                'kra_quarter': record.kra_quarter,
                'kra_month': record.month,
                'year': record.year,
                'employee_id': record.employee_ids.id,
            })
