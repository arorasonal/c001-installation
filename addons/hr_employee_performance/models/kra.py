from openerp import models, fields, api
from datetime import datetime, date
from odoo.exceptions import UserError


class KraKra(models.Model):
    _name = 'kra.kra'
    _rec_name = 'employee_id'
    _inherit = ['mail.thread']

    @api.depends('performance_kra_ids.total_score')
    def _amount_all(self):
        amount = 0.0
        for order in self.performance_kra_ids:
            amount += order.total_score
        self.update({'total_score': amount})

    @api.depends('total_score')
    def _amount_grade(self):
        grade = False
        grade_id = self.env['kra.grade'].search([])
        for grade_lines in grade_id:
            if ((grade_lines.range_start <= self.total_score) and (grade_lines.range_end >= self.total_score)):
                self.update({'grade': grade_lines.name})

    reference_no = fields.Char('Reference #')
    employee_id = fields.Many2one('hr.employee', 'Employee', required=True,
                                  default=lambda self: self.env['hr.employee'].search([('user_id', '=', self.env.uid)]))
    parent_id = fields.Many2one(
        'hr.employee', 'Employee Manager')
    department_id = fields.Many2one(
        'hr.department', 'Department',)
    job_id = fields.Many2one('hr.job', 'Designation')
    fiscal_year_id = fields.Many2one('account.fiscalyear', 'Fiscal Year')
    kra_quarter = fields.Selection([
        ('monthly', 'Monthly'),
        ('quarter', 'Quarter'),
        ('semi_annual', 'Semi-Annual'),
        ('annual', 'Annual')], string="KRA Quarter")
    kra_id = fields.Many2one(
        'evaluation.structure', 'Evaluation Structure')
    total_score = fields.Float('Total Score', compute='_amount_all')
    kra_questions = fields.Integer("KRA Question")
    state = fields.Selection([
        ('draft', 'Draft'),
        ('awaiting_manager_approval', 'Awaiting Manager Approval'),
        ('awaiting_ceo_approval', 'Awaiting CEO Approval'),
        ('kpi_defined', 'KPI Defined'),
        ('review_employee', 'Employee Review'),
        ('review_manager', 'Manager Review'),
        ('review_hr', 'HR Review'),
        ('done', 'Done'),
        ('refused', 'Refused'),
        ('cancelled', 'Cancelled'),
    ], default='draft', track_visibility='onchange')
    performance_kra_ids = fields.One2many('kra.employee.question', 'kra_ids')
    competency_kra_ids = fields.One2many('kra.competency.lines', 'comp_kra')
    career_aspirations = fields.Text('Career Aspirations')
    training = fields.Text('Training Needs')
    grade = fields.Char('Grade', compute='_amount_grade' )
    refused = fields.Text('Reason For Refusal/Cancellation')
    current_user = fields.Boolean(
        copy=False, compute="check_current_user", default=False)
    curr_user = fields.Boolean('is current user ?', compute='_get_current_user')

    @api.depends('employee_id')
    def _get_current_user(self):
        for e in self:
            e.curr_user = (True if e.env.user.id == e.employee_id.user_id.id else False)
    
    @api.multi
    def check_current_user(self):
        user = self.env.uid
        for record in self:
            if self.env.user.has_group('hr_employee_register.group_base_ceo_md'):
                record.current_user = True
            elif record.department_id.manager_id.user_id.id == user:
                record.current_user = True
            else:
                record.current_user = False

    @api.constrains('performance_kra_ids')
    def constrains_on_line(self):
        total_weightage = 0.0
        for record in self:
            for line in record.performance_kra_ids:
                total_weightage += line.weightage
            if total_weightage > 100:
                raise UserError(
                    "Sum of weightage can not be greater than 100")

    @api.multi
    def unlink(self):
        """Allows to delete hr draft"""
        for rec in self:
            if rec.state not in ['draft']:
                raise UserError(
                    'You can only delete in draft state')
        return super(KraKra, self).unlink()

    # @api.multi
    # def compute_total_weightage_score(self):
    #     for record in self:
    #         sum_employee_score = 0.0
    #         for line in record.employee_kra_ids:
    #             sum_employee_score += line.employee_score
    #         sum_manager_score = 0.0
    #         for line in record.manager_kra_ids:
    #             sum_manager_score += line.manager_score
    #         total_score = (sum_employee_score + sum_manager_score) / 2
    #         record.total_score = total_score
    #         if record.total_score <= 35:
    #             self.grade = 'unsatisfactory'
    #         elif self.total_score > 35 and self.total_score <= 50:
    #             record.grade = 'improvement'
    #         elif self.total_score > 50 and self.total_score <= 70:
    #             record.grade = 'good'
    #         elif self.total_score > 70 and self.total_score <= 90:
    #             record.grade = 'very_good'
    #         elif self.total_score > 90:
    #             record.grade = 'excellent'

    @api.onchange('employee_id')
    def onchange_employee_id(self):
        """Onchange Employee Method."""
        if self.employee_id:
            check_list = []
            comp_list = self.env['kra.competency']
            comp_ids = comp_list.search([('active', '=', True)])
            if self.employee_id:
                self.department_id = self.employee_id.department_id.id
                self.parent_id = self.employee_id.parent_id.id
                self.job_id = self.employee_id.job_id.id
                self.kra_id = self.employee_id.performance_structure_id.id
                for comp in comp_ids:
                    check_list.append((0, 0, {
                        'comp_question': comp.id,
                    }))
                    self.competency_kra_ids = check_list
        else:
            self.competency_kra_ids = False


    @api.onchange('kra_id')
    def onchange_kra_id(self):
        """Onchange Kra Method."""
        if self.kra_id:
            val1 = []
            for record in self:
                if record.kra_id:
                    for lines in record.kra_id.kra_relation:
                        val1.append((0, 0, {
                            'name': lines.id,
                            'state': record.state
                        }))
                        self.performance_kra_ids = val1
        else:
            self.performance_kra_ids = False

    @api.model
    def create(self, vals):
        """Create Method."""
        if vals.get('reference_no', '/') == '/':
            vals['reference_no'] = self.env[
                'ir.sequence'].next_by_code('kra.kra') or '/'
        employee = self.env['hr.employee'].search([
            ('id', '=', vals.get('employee_id'))
        ])
        if employee:
            vals['department_id'] = employee.department_id.id
            vals['parent_id'] = employee.parent_id.id
            vals['job_id'] = employee.job_id.id
        comp_list = self.env['kra.competency']
        comp_ids = comp_list.search([('active', '=', True)])
        lst_qst = []
        if 'competency_kra_ids' in vals:
            for cnt, rec in enumerate(vals['competency_kra_ids']):
                lst_qst.append((0, 0, {
                    'comp_question': comp_ids.ids[cnt],
                    'remark': rec[2]['remark']
                }))
            vals['competency_kra_ids'] = lst_qst
        else:
            raise UserError(
                "First fill the Compentancy Master")
        if 'kra_id' in vals:
            kra_struct = self.env['evaluation.structure'].search([
                ('id', '=', vals.get('kra_id'))
            ])
            for i, lines in enumerate(kra_struct.kra_relation):
                performance_id = vals['performance_kra_ids'][i][2]
                performance_id['name'] = lines.id
                vals['performance_kra_ids'][i][2] = performance_id
        return super(KraKra, self).create(vals)

    @api.multi
    def write(self, vals):
        """Write Method."""
        employee = self.env['hr.employee'].search([
            ('id', '=', vals.get('employee_id'))
        ])
        if employee:
            vals.update({
                'department_id': employee.department_id.id,
                'parent_id': employee.parent_id.id,
                'job_id': employee.job_id.id,
            })
        dict_vals = []
        comp_list = self.env['kra.competency']
        comp_ids = comp_list.search([('active', '=', True)])
        if 'competency_kra_ids' in vals and 'employee_id' in vals:
            print"hhhhhhhhh", vals['competency_kra_ids']
            for cnt, rec in enumerate(vals['competency_kra_ids']):
                dict_vals.append((0, 0, {
                    'comp_question': comp_ids.ids[cnt],
                    'remark': rec[2]['remark']
                }))
                if cnt == len(comp_ids) - 1:
                    break
            self.competency_kra_ids.unlink()
            vals['competency_kra_ids'] = dict_vals

        kra_id_struct = self.env['evaluation.structure'].search([
            ('id', '=', self.kra_id.id)
        ])
        if 'performance_kra_ids' in vals:
            for i, lines in enumerate(kra_id_struct.kra_relation):
                performance_id = vals['performance_kra_ids'][i][2]
                performance_id['name'] = lines.id
                vals['performance_kra_ids'][i][2] = performance_id
        return super(KraKra, self).write(vals)

    @api.multi
    def action_submit(self):
        """Send To Employee"""
        if self.state == "draft":
            self.write({'state': 'awaiting_manager_approval'})
        kra_state = self.env['kra.employee.question'].search(
            [('kra_ids', '=', self.id)])
        kra_state.write({'state': self.state})
        return True

    @api.multi
    def action_approval(self):
        if self.state == "awaiting_manager_approval":
            self.write({'state': 'kpi_defined'})
        kra_state = self.env['kra.employee.question'].search(
            [('kra_ids', '=', self.id)])
        kra_state.write({'state': self.state})
        return True

    @api.multi
    def action_ceo_approval(self):
        if self.state == "awaiting_ceo_approval":
            self.write({'state': 'done'})
        kra_state = self.env['kra.employee.question'].search(
            [('kra_ids', '=', self.id)])
        kra_state.write({'state': self.state})
        return True

    @api.multi
    def action_review(self):
        if self.state == "kpi_defined":
            self.write({'state': 'review_employee'})
        kra_state = self.env['kra.employee.question'].search(
            [('kra_ids', '=', self.id)])
        kra_state.write({'state': self.state})
        return True

    @api.multi
    def action_review_manager(self):
        if self.state == "review_employee":
            self.write({'state': 'review_manager'})
        kra_state = self.env['kra.employee.question'].search(
            [('kra_ids', '=', self.id)])
        kra_state.write({'state': self.state})
        return True

    @api.multi
    def action_review_hr(self):
        if self.state == "review_manager":
            self.write({'state': 'review_hr'})
        kra_state = self.env['kra.employee.question'].search(
            [('kra_ids', '=', self.id)])
        kra_state.write({'state': self.state})
        return True

    @api.multi
    def action_review_complete(self):
        if self.state == "review_hr":
            self.write({'state': 'awaiting_ceo_approval'})
        kra_state = self.env['kra.employee.question'].search(
            [('kra_ids', '=', self.id)])
        kra_state.write({'state': self.state})
        return True

    @api.multi
    def action_refuse(self):
        for value in self:
            if value.refused == 0:
                raise UserError('Please enter reason for refusal')
            else:
                self.write({'state': 'refused'})
                kra_state = self.env['kra.employee.question'].search(
                    [('kra_ids', '=', self.id)])
                kra_state.write({'state': self.state})
        return True

    @api.multi
    def action_cancel(self):
        for value in self:
            if value.refused == 0:
                raise UserError('Please enter reason for refusal')
            else:
                if self.state in ("review_employee,review_manager,review_hr"):
                    self.write({'state': 'cancelled'})
                kra_state = self.env['kra.employee.question'].search(
                    [('kra_ids', '=', self.id)])
                kra_state.write({'state': self.state})
        return True

    @api.multi
    def action_cancel_manager(self):
        for value in self:
            if value.refused == 0:
                raise UserError('Please enter reason for refusal')
            else:
                self.write({'state': 'cancelled'})
                kra_state = self.env['kra.employee.question'].search(
                    [('kra_ids', '=', self.id)])
                kra_state.write({'state': self.state})
        return True

    @api.multi
    def action_reset_draft(self):
        self.write({'state': 'draft'})
        kra_state = self.env['kra.employee.question'].search(
            [('kra_ids', '=', self.id)])
        kra_state.write({'state': self.state})
        return True

    @api.multi
    def action_reset_draft1(self):
        self.write({'state': 'draft'})
        kra_state = self.env['kra.employee.question'].search(
            [('kra_ids', '=', self.id)])
        kra_state.write({'state': self.state})
        return True


class KraCompetencyLines(models.Model):
    _name = 'kra.competency.lines'

    comp_question = fields.Many2one(
        'kra.competency', 'Kra Competency')
    remark = fields.Char('Remark')
    comp_kra = fields.Many2one('kra.kra', 'Competency Kra')
