from openerp import models, fields, api
from odoo.exceptions import UserError


class HrJob(models.Model):
    _inherit = 'hr.employee'

    performance_structure_id = fields.Many2one(
        'evaluation.structure', string='Evaluation Structure')


class EvaluationStructure(models.Model):
    _name = 'evaluation.structure'

    name = fields.Char('Name', required=True)
    active = fields.Boolean('Active', default=True)
    notes = fields.Text('Notes')
    kra_relation = fields.Many2many(
        'kra.master',
        'rel_kra_master',
        'kra_id',
        'materr1',
        'Kra Master')


class KraEmployeeQuestions(models.Model):
    _name = 'kra.employee.question'

    name = fields.Many2one('kra.master', 'KRA')
    kpi_id = fields.Char('KPI')
    uom = fields.Char('UoM')
    weightage = fields.Float('Weightage(%)')
    target = fields.Float('Target')
    kra_ids = fields.Many2one('kra.kra', "KRA")
    achievement_employee = fields.Float('Achievement Employee')
    achievement_manager = fields.Float('Achievement Manager')
    employee_score = fields.Float('Employee Score', compute="calculate")
    employee_comments = fields.Char('Employee Comment')
    manager_score = fields.Float('Manager Score',compute="calculate")
    manager_comments = fields.Char('Manager Comments')
    total_score = fields.Float('Total Score', compute="calculate")
    state = fields.Selection([
        ('draft', 'Draft'),
        ('awaiting_manager_approval', 'Awaiting Manager Approval'),
        ('awaiting_ceo_approval', 'Awaiting CEO Approval'),
        ('kpi_defined', 'KPI Defined'),
        ('review_employee', 'Employee Review'),
        ('review_manager', 'Manager Review'),
        ('review_hr', 'HR Review'),
        ('done', 'Done'),
        ('refused', 'Refused'),
        ('cancelled', 'Cancelled'),
    ], default='draft', track_visibility='onchange')
    checks_user = fields.Boolean(compute="check_check_user")

    @api.depends('kra_ids')
    def check_check_user(self):
        user_id = self.env.user.id
        for rec in self:
            if (user_id == rec.kra_ids.employee_id.user_id.id):
                rec.checks_user = True

    @api.depends('achievement_employee', 'achievement_manager', 'weightage', 'target', 'manager_score')
    def calculate(self):
        for rec in self:
            if rec.target < rec.achievement_employee:
                raise UserError(
                    ('The Target must be anterior to the Employee Achievement.'))
            else:
                if rec.target != 0.0:
                    rec.employee_score = (rec.achievement_employee * rec.weightage) / rec.target
                    rec.manager_score = (rec.achievement_manager * rec.weightage) / rec.target
                    rec.total_score = rec.employee_score + rec.manager_score       


class KraMaster(models.Model):
    _name = 'kra.master'

    name = fields.Char('Name', required=True)
    active = fields.Boolean('Active', default=True)
    kra_ques_id = fields.Many2one('kra.config', 'KRA Master')

    # @api.constrains('name')
    # def constrains_on_name(self):
    #     for rec in self:
    #         kra_question = rec.search_count([
    #             ('name', '=', rec.name),
    #         ])
    #         if kra_question > 1:
    #             raise UserError("This KRA Question is already exist"
    #                             ", Please select another Question.")


class KraCompetency(models.Model):
    _name = 'kra.competency'
    name = fields.Char('Name', required=True)
    active = fields.Boolean('Active', default=True)


class KraGrade(models.Model):
    _name = 'kra.grade'

    name = fields.Char('Name', required=True)
    range_start = fields.Integer()
    range_end = fields.Integer()
