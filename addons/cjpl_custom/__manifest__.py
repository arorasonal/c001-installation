{
    "name": "CJPL Custom",
    "version": "10.0.0.1.0",
    'website': 'http://www.apagen.com',
    'images': [],
    "depends": ['sale', 'account', 'sale_order_type', 'base', 'account_invoice_shipping_address', 'web', 'base_finance', 'report'],
    "author": "Apagen Solution PVT LTD",
    "category": "GTR",
    'sequence': 16,
    "description": """
    This module will provide the detail of sample and convert it into a product and some report regarding sample  
    """,
    'data': [
        'view/sale_order_type_view.xml',
        'view/product_template_view.xml',
        'view/invoice_domestic_report.xml',
        'view/res_company_view.xml',
        'view/res_partner_bank.xml',
        'view/analytic_view.xml',
        'wizard/invoice_report_view.xml',
        'report/tax_invoice_type.xml',
        'view/tax_invoice_template.xml',
        'view/res_users_view.xml',
        'view/res_users_update_view.xml',
    ],
    'demo_xml': [],
    'js': [
        'static/src/js/view_list.js'
    ],
    'css': ['static/src/css/sample_kanban.css'],
    'installable': True,
    'active': False,
    'auto_install': False,
    #    'certificate': 'certificate',
}
