from dateutil.relativedelta import relativedelta
import datetime
import logging
import time

from odoo import api, fields, models, _
import odoo.addons.decimal_precision as dp

_logger = logging.getLogger(__name__)


class account_analytic_account(models.Model):
    _inherit = "account.analytic.account"  
    
    @api.multi
    def _prepare_invoice_data(self):
        context = context or {}
        sale_obj = ' '
        journal_obj = self.env['account.journal']
        fpos_obj = self.env['account.fiscal.position']
        partner = contract.partner_id

        if not partner:
            raise UserError(_('No Customer Defined!'),_("You must first select a Customer for Contract %s!") % contract.name )

        fpos_id = fpos_obj.get_fiscal_position(partner.company_id.id)
        journal_ids = journal_obj.search([('type', '=','sale'),('company_id', '=', contract.company_id.id or False)], limit=1)
        if not journal_ids:
            raise UserError(_('Error!'),
            _('Please define a sale journal for the company "%s".') % (contract.company_id.name or '', ))

        partner_payment_term = partner.property_payment_term and partner.property_payment_term.id or False

        currency_id = False
        if contract.pricelist_id:
            currency_id = contract.pricelist_id.currency_id.id
        elif partner.property_product_pricelist:
            currency_id = partner.property_product_pricelist.currency_id.id
        elif contract.company_id:
            currency_id = contract.company_id.currency_id.id
        sale = self.env['sale.order'].search([('name','=',contract.name)])
        for val1 in sale:
            sale_obj = self.env['sale.order'].browse(val1)
        invoice = {
           'account_id': partner.property_account_receivable.id,
           'type': 'out_invoice',
           'sale_type':sale_obj.type_id.id,
           'partner_id': partner.id,
           'sale_order':contract.name,
           'currency_id': currency_id,
           'journal_id': len(journal_ids) and journal_ids[0] or False,
           'date_invoice': contract.recurring_next_date,
           'origin': contract.code,
           'fiscal_position': fpos_id,
           'payment_term': partner_payment_term,
           'company_id': contract.company_id.id or False,
        }
        return invoice

    # @api.multi 
    # def _prepare_invoice_line(self, fiscal_position):
    #     fpos_obj = self.env['account.fiscal.position']
    #     res = line.product_id
    #     account_id = res.property_account_income.id
    #     if not account_id:
    #         account_id = res.categ_id.property_account_income_categ.id
    #     account_id = fpos_obj.map_account(fiscal_position, account_id)
    #     sale = self.env['sale.order'].search([('name','=',line.analytic_account_id.name)])
    #     for val1 in sale:
    #         sale_obj= self.env['sale.order'].browse(val1)
    #     taxes = res.taxes_id or False
    #     tax_id = fpos_obj.map_tax(fiscal_position, taxes)
    #     values = {
    #         'name': line.name,
    #         'hsn':sale_obj.type_id.sac,
    #         'account_id': account_id,
    #         'account_analytic_id': line.analytic_account_id.id,
    #         'price_unit': line.price_unit or 0.0,
    #         'quantity': line.quantity,
    #         'uom_id': line.uom_id.id or False,
    #         'product_id': line.product_id.id or False,
    #         'invoice_line_tax_id': [(6, 0, tax_id)],
    #     }
    #     return values
