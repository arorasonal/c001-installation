from odoo import fields, models, api
from odoo.tools.translate import _
from datetime import datetime, timedelta
from odoo import tools
import time
# import os,re


class res_users_update(models.TransientModel):
    _name = "res.users.update"
    _inherit = ['mail.thread', 'ir.needaction_mixin']

    code = fields.Boolean('Code', track_visibility="onchange")
    user_id = fields.Many2one(
        'res.users', string='Salesperson', track_visibility="onchange")
    team_id = fields.Many2one(
        'crm.team', string="Sales Team", track_visibility="onchange")
    field_ids = fields.Many2many('res.partner', 'mass_field_rel',
                                 'mass_id1', 'field_id', 'Fields', track_visibility="onchange")

    @api.multi
    @api.onchange('user_id')
    def onchange_user_id(self):
        res = {}
        bom = []
        if self.user_id:
            crm = self.env['crm.team'].search([('active', '=', True)])
            for each in crm:
                if each.member_ids:
                    for val in each.member_ids:
                        if val.id == self.user_id.id:
                            bom.append(each.id)
            res['domain'] = {'team_id': [('id', 'in', bom)]}
        return res

    @api.multi
    def update_salesperson(self):
        lines = self.field_ids
        vals = {}
        team_id = False
        if self.team_id:
            vals.update({'team_id': self.team_id.id})
        if self.user_id:
            vals.update({'user_id': self.user_id.id})
        vals_contract = {}
        if self.team_id:
            vals_contract.update({'team_id': self.team_id.id})
        if self.user_id:
            vals_contract.update({'manager_id': self.user_id.id})
        for val1 in lines:
            if val1.customer:
                sale = self.env['sale.order'].search(
                    [('partner_id', '=', val1.id), ('state', 'in', ('draft', 'sent'))])
                opp = self.env['crm.lead'].search(
                    [('partner_id', '=', val1.id), ('probability', 'not in', (0, 100))])
                contract = self.env['account.analytic.account'].search(
                    [('partner_id', '=', val1.id), ('state', 'not in', ('close', 'cancelled'))])
                account = self.env['account.invoice'].search(
                    [('partner_id', '=', val1.id), ('type', '=', 'out_invoice'), ('state', '=', 'draft')])
                if sale:
                    for each in sale:  
                        each.write(vals)
                if opp:
                    for each in opp:
                        each.write(vals)
                if contract:
                    for each in contract:
                        each.write(vals_contract)
                if account:
                    for each in account:
                        each.write(vals)
                val1.write({'user_id': self.user_id.id,
                            'team_id': self.team_id.id})
            self.write({'code': True})
        return True
