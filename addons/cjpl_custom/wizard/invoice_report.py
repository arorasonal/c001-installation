from odoo import fields, models
from datetime import datetime, timedelta
import base64, urllib
import os
import re
from PIL import Image
# from openerp.tools import amount_to_text
from odoo.tools.translate import _
from lxml  import etree
from odoo.tools import DEFAULT_SERVER_DATE_FORMAT, DEFAULT_SERVER_DATETIME_FORMAT, DATETIME_FORMATS_MAP, float_compare
# from openerp.osv.orm import setup_modifiers
# from odoo.tools.translate import 
import time
import xlwt
import xlsxwriter
from xlsxwriter.workbook import Workbook
from xlwt import Workbook, XFStyle, Borders, Pattern, Font, Alignment,  easyxf
import odoo.addons.decimal_precision as dp
import math
from odoo.tools import amount_to_text_en

class wiz_invoice_report(models.Model):
    _name="wiz.invoice.report"
    _inherit = ['mail.thread', 'ir.needaction_mixin']

    def invoice_report(self):
        wb = xlsxwriter.Workbook('Invoice Report.xls')
        ws = wb.add_worksheet()
        ws.set_landscape()
        ws.fit_to_pages(1,1)
        ws.set_margins(left=0.7,right=0.7,top=0.75,bottom=0.75)
        payterm=self
        ws.set_column(0, 0, 12)
        ws.set_column(1, 1, 17)   
        ws.set_column(2, 2, 16)
        ws.set_column(3, 3, 16)
        ws.set_column(4, 4, 10)
        ws.set_column(5, 5, 8)
        ws.set_column(6, 6, 8)
        ws.set_column(7, 7, 8)
        ws.set_column(8, 8, 8)
        ws.set_column(9, 9, 10)
        ws.set_column(10, 10, 10)
        ws.set_column(11, 11, 10)
        ws.set_column(12, 12, 10)
        ws.set_column(13, 13, 10)
        ws.set_column(14, 14, 8)
        ws.set_column(15, 15, 10)
        ws.set_column(16, 16, 16)
        ws.set_column(17, 17, 16)
        ws.set_column(18, 18, 16)
        ws.set_column(19, 19, 16)
        ws.set_column(20, 20, 16)
        ws.set_column(21, 21, 16)
        ws.set_column(22, 22, 16)
         
        cell_format = wb.add_format({'align': 'center',
                                   'valign': 'vcenter',
                                   'bold':1
                                   })
        cell_format1=wb.add_format({'align':'center',
                                    'valign':'vcenter',
                                    'bold':1,
                                    'right':1,
                                    'left':1,
                                    'top':1,
                                    'bottom':1,
                                    'font_size':10,
                                    })
        cell_format2=wb.add_format({'align':'center',
                                    'valign':'vcenter',
                                    
                                    'right':1,
                                    'left':1,
                                    'top':1,
                                    'bottom':1,
                                    'font_size':10,
                                    })
        cell_format3=wb.add_format({'align':'center',
                                    'valign':'vcenter',
                                   'bold':1,
                                    'font_size':10,
                                    })
        cell_format4=wb.add_format({'align':'center',
                                    'valign':'vcenter',
                                    
                                    'font_size':10,
                                    })
        ws.merge_range('A2:E2','Invoice Report',cell_format3)
        ws.write('A3','Customer',cell_format3)
        ws.write('A4','From Date',cell_format3)
#         ws.write('H3','Product',cell_format3)
        ws.write('D4','To Date',cell_format3)
        if payterm.partner_id:
            ws.write('B3',str(payterm.partner_id.name or ' '),cell_format4)
        if payterm.from_date:
            ws.write('B4',str(payterm.from_date or ' '),cell_format4)
        if payterm.to_date:
            ws.write('E4',str(payterm.to_date or ' '),cell_format4)
        ws.write('A5','Customer',cell_format1)
        ws.write('B5','Date ',cell_format1)
        ws.write('C5','Invoice No',cell_format1)
        ws.write('D5','Source',cell_format1)
        ws.write('E5','Amount',cell_format1)
        ws.write('F5','State ',cell_format1)
        i = 5 
        for val in self.browse().invoice_lines:
            ws.write(i,0,str(val.partner_id.name or ' '),cell_format2)
            ws.write(i,1,(time.strftime('%d/%m/%Y', time.strptime(val.date_invoice, '%Y-%m-%d'))or ''),cell_format2)
            ws.write(i,2,str(val.number or ' '),cell_format2)
            ws.write(i,3,str(val.source or ' '),cell_format2)
            ws.write(i,4,val.amount,cell_format2)
            ws.write(i,5,str(val.state or ' '),cell_format2)
            i = i +1
        wb.close()
        s = 'Invoice Report.xls'
        f = open("Invoice Report.xls")
        out = base64.encodestring(f.read())
        self.write({'export_data1':out, 'filename1':s})
        f.close()
        return True 
 
    partner_id = fields.Many2one('res.partner','Customer')
    user_id = fields.Many2one('res.users','Created By')
    from_date = fields.Date('From Date')
    to_date = fields.Date('To Date')
    invoice_lines = fields.One2many('wiz.invoice.report.line','wiz_invoice_id','Invoice Lines')
    export_data1 = fields.Binary('FILE', readonly=True)
    filename1 = fields.Char('FILE NAME',size=250,readonly=True)
              
    def create_line(self, val):
        res={}
        p = ''
        s = ''
        qty = 0.0
        stock_qty = 0.0
        partner_list = []
        mch_obj = self.env['wiz.invoice.report']
        if mch_obj.from_date and mch_obj.to_date and mch_obj.partner_id:
            partner_obj = self.env['res.partner'].browse(mch_obj.partner_id.id)
            partner_list.append(partner_obj.id)
            part = self.env['res.partner'].search([('customer','=',True),('parent_id','=',partner_obj.id)])
            for val in part:
                partner_list.append(val)
            if partner_list:
                for val1 in partner_list:
                    partner1_obj = self.env['res.partner'].browse(val1)
                    if mch_obj.user_id.id == partner1_obj.user_id.id:
                        account = self.env['account.invoice'].search([('date_invoice','>=',mch_obj.from_date),('date_invoice','<=',mch_obj.to_date),('type','=','out_invoice'),('partner_id','=',partner1_obj.id),('state','!=','draft')])
                        for val in account:
                            account_obj = self.env['account.invoice'].browse(val)
                            self.env['wiz.invoice.report.line'].create({'wiz_invoice_id':mch_obj.id,'partner_id':account_obj.partner_id.id,
                                                                                'date_invoice':account_obj.date_invoice,'number':account_obj.number,
                                                                                'source':account_obj.origin,'amount':account_obj.amount_total,
                                                                                'state':account_obj.state})
        elif mch_obj.partner_id:
            partner_list = []
            partner_obj = self.env['res.partner'].browse(mch_obj.partner_id.id)
            partner_list.append(partner_obj.id)
            part = self.env['res.partner'].search([('customer','=',True),('parent_id','=',partner_obj.id)])
            for val in part:
                partner_list.append(val)
            if partner_list:
                for val1 in partner_list:
                    partner1_obj = self.env['res.partner'].browse(val1)
                    if mch_obj.user_id.id == partner1_obj.user_id.id:
                        account = self.env['account.invoice'].search([('type','=','out_invoice'),('partner_id','=',partner1_obj.id),('state','!=','draft')])
                        for val in account:
                            account_obj = self.env['account.invoice'].browse(val)
                            self.env['wiz.invoice.report.line'].create({'wiz_invoice_id':mch_obj.id,'partner_id':account_obj.partner_id.id,
                                                                                'date_invoice':account_obj.date_invoice,'number':account_obj.number,
                                                                                'source':account_obj.origin,'amount':account_obj.amount_total,
                                                                                'state':account_obj.state})
#             
        return res
    # _defaults={
    #            'user_id':lambda obj, cr, uid, cnt: uid,
    #            }
class wiz_invoice_report_line(models.Model):
    _name="wiz.invoice.report.line"
    _inherit = ['mail.thread', 'ir.needaction_mixin']
   
    wiz_invoice_id = fields.Many2one('wiz.invoice.report','Invoice Report')
    partner_id = fields.Many2one('res.partner','Buyer')
    date_invoice = fields.Date('Invoice Date')
    number = fields.Char('Invoice No')
    source = fields.Char('Source Document')
    amount = fields.Float('Amount')
    state = fields.Char('State')
               
        