import itertools
import logging
from functools import partial
from itertools import repeat

from lxml import etree
from lxml.builder import E

from odoo import SUPERUSER_ID, models
from odoo import tools
from odoo.exceptions import UserError, ValidationError, Warning
from odoo import fields, models, api, _


_logger = logging.getLogger(__name__)


class res_users(models.Model):
    """ User class. A res.users record models an OpenERP user and is different
        from an employee.

        res.users class now inherits from res.partner. The partner model is
        used to store the data related to the partner: lang, name, address,
        avatar, ... The user model is now dedicated to technical data.
    """
    __admin_ids = {}
    __uid_cache = {}
    _inherits = {
        'res.partner': 'partner_id',
    }
    _inherit = "res.users"
    _description = 'Users'
#     _order = 'name, login'

    partner_lines = fields.One2many(
        'user.partner', 'user_id', 'Partner Lines', track_visibility="onchange")

    # @api.model
    # def create(self, vals):
    #     partner_obj = ' '
    #     user_id = super(res_users, self).create(vals)
    #     user = self.browse(user_id)
    #     if user.partner_id.company_id:
    #         user.partner_id.write({'company_id': user.company_id.id})
    #     if user.partner_lines:
    #         for val in user.partner_lines:
    #             partner_obj = self.env['user.partner'].browse(val.id)
    #             part_obj = self.env['res.partner'].browse(partner_obj.name.id)
    #             part_obj.write({'user_id': user_id})
    #     return user_id

    @api.multi
    def write(self, values):
        for rec in self:
            partner = rec
            if partner.partner_lines:
                for val in partner.partner_lines:
                    partner_obj = self.env['user.partner'].browse(val.id)
                    part_obj = self.env['res.partner'].browse(
                        partner_obj.name.id)
                    part_obj.write({'user_id': partner.id})
        # if not hasattr(ids, '__iter__'):
        #     ids = [ids]
            if rec.id == rec.env.uid:
                for key in values.keys():
                    if not (key in rec.SELF_WRITEABLE_FIELDS or key.startswith('context_')):
                        break
                else:
                    if 'company_id' in values:
                        user = rec.browse(SUPERUSER_ID)
                        if not (values['company_id'] in user.company_ids.ids):
                            del values['company_id']
                    uid = 1  # safe fields only, so we write as super-user to bypass access rights

        res = super(res_users, self).write(values)
        if 'company_id' in values:
            # if partner is global we keep it that way
            if self.partner_id.company_id and self.partner_id.company_id.id != values['company_id']:
                self.partner_id.write({'company_id': self.company_id.id})
            # clear default ir values when company changes
            self.env['ir.values'].get_defaults_dict.clear_cache(self.env[
                                                                'ir.values'])
        # clear caches linked to the users
        self.env['ir.model.access'].call_cache_clearing_methods()
        clear = self.env['ir.rule'].clear_cache()
        #clear = partial(self.env['ir.rule'].clear_cache())
        # map(clear, self.ids)
        db = self._cr.dbname
        if db in self._Users__uid_cache:
            for rec in self:
                if rec.id in self._Users__uid_cache[db]:
                    del self._Users__uid_cache[db][rec]
        # self._context_get.clear_cache(self)

        # self.has_group.clear_cache(self)
        return res


class user_partner(models.Model):
    _name = "user.partner"
    _inherit = ['mail.thread', 'ir.needaction_mixin']

    name = fields.Many2one('res.partner', 'Buyer', track_visibility="onchange")
    user_id = fields.Many2one('res.users', 'User', track_visibility="onchange")
