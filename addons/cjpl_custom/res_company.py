from odoo import fields, models


class res_company(models.Model):
    _inherit = 'res.company'

    gst_no = fields.Char('GST NO')
    pan_no = fields.Char('PAN NO')
    cin_no = fields.Char('CIN NO')
