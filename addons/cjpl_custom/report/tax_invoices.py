# -*- coding: utf-8 -*-

import time
from odoo import api, models, _
from odoo.exceptions import UserError
from odoo.tools import float_is_zero
from odoo.tools import amount_to_text
from datetime import datetime
from dateutil.relativedelta import relativedelta


class tax_invoice_report_first(models.AbstractModel):
    _name = 'report.cjpl_custom.print_domestic_report'

    def get_hsn_sac(self, account_invoice_line):
        hsn = ''
        if account_invoice_line.invoice_id.sale_type_id.is_contract == True:
            hsn = account_invoice_line.invoice_id.sale_type_id.sac
        else:
            if account_invoice_line.product_id.hsn_no:
                hsn = account_invoice_line.product_id.hsn_no
            else:
                hsn = account_invoice_line.product_id.categ_id.hsn_no
        return hsn

    @api.multi
    def amount_to_text(self, amount, currency='INR'):
        return amount_to_text(amount, currency)

    def get_convert_amt(self, account_invoice,):
        amt_en_fill = ''
        amt_en = amount_to_text(account_invoice.rounded_total, currency='INR')
        amt_en_fill = amt_en
        if 'and Zero Cent' in amt_en:
            amt_en_fill = amt_en.replace('and Zero Cent', 'Only')
        elif 'Cents' in amt_en:
            amt_en_fill = amt_en.replace('Cents', ' Only')

        return amt_en_fill.replace('INR', '')

    def get_amt(self, account_invoice_line):
        print "account_invoice_line", account_invoice_line
        amt = 0.00
        amount = 0.00
        taxable_value = 0.00
        for val in account_invoice_line:
            amount = val.price_subtotal
#             tax = val.invoice_line_tax_id.amount * 100
            amt = amount * val.invoice_line_tax_id.amount
            return amt

    def get_rate(self, account_invoice_line):
        amt = 0.00
        amount = 0.00
        taxable_value = 0.00
        # rate = 0
        for val in account_invoice_line:
            rate = 0
            amount = val.price_subtotal
            for loop in val.invoice_line_tax_ids:
                if loop.tax_group_id.name in ['IGST', 'CGST', 'SGST']:
                    rate += loop.amount
                elif loop.tax_group_id.name == 'Taxes' and loop.children_tax_ids:
                    for rec in loop.children_tax_ids:
                        if rec.tax_group_id.name in ['IGST', 'CGST', 'SGST']:
                            rate += rec.amount
                else:
                    rate += (loop.amount) * 100
            return rate

        # for tax in account_invoice_line.invoice_line_tax_ids:
        #     rate += tax.amount
        # return rate * 100

    def get_line_tax(self, account_invoice_line):
        amt = 0.00
        amount = 0.00
        taxable_value = 0.00
        for val in account_invoice_line:
            amount = val.price_subtotal
            for loop in val.invoice_line_tax_ids:
                if loop.tax_group_id.name in ['IGST', 'CGST', 'SGST']:
                    amt += round(amount * loop.amount / 100,2)
                elif loop.tax_group_id.name == 'Taxes' and loop.children_tax_ids:
                    for rec in loop.children_tax_ids:
                        if rec.tax_group_id.name in ['IGST', 'CGST', 'SGST']:
                            amt += round(amount * rec.amount / 100,2)
                else:
                    amt += round(amount * loop.amount / 100,2)
            return amt

    def get_total_tax(self, account_invoice):
        amt = 0.00
        account_invoice_line = self.env['account.invoice.line'].search(
            [('invoice_id', '=', account_invoice.id)])
        for val in account_invoice_line:
            amt = amt + self.get_line_tax(val)
        amt = amt
        return amt

    def get_total_with_tax(self, account_invoice):
        amt = 0.00
        account_invoice_line = self.env['account.invoice.line'].search(
            [('invoice_id', '=', account_invoice.id)])
        for val in account_invoice_line:
            amt = amt + val.price_subtotal + self.get_line_tax(val)
        amt = amt
        return amt

    def get_amt_total(self, account_invoice):
        amt = 0.00
        amount = 0.00
        taxable_value = 0.00

        account_invoice_line = self.env['account.invoice.line'].search(
            [('invoice_id', '=', account_invoice.id)])
        for val in account_invoice_line:
            amount = val.price_subtotal
            amt = amt + amount * (self.get_rate(val) / 200) * 2 
        amt = amt
        return amt

    def get_amt_total1(self, account_invoice):
        amt = 0.00
        amount = 0.00
        total = 0.0
        taxable_value = 0.00
        account_invoice_line = self.env['account.invoice.line'].search(
            [('invoice_id', '=', account_invoice.id)])
        if account_invoice_line:
            for val in account_invoice_line:
                amount = val.price_subtotal
                amt = amount * (self.get_rate(val) / 200) * 2 
                total = total + amount + amt
        total = total
        return total

    def get_total(self, account_invoice_line):
        total = 0.00
        amt = 0.00
        amount = 0.00
        taxable_value = 0.00

        for val in account_invoice_line:
            amount = val.price_subtotal
            amt = amount * self.get_rate(val)
            total = amount + amt
            return total

    def get_total_qty(self, account_invoice):
        total_qty = 0
        for val in account_invoice.invoice_line_ids:
            total_qty = total_qty + val.quantity
        return total_qty

    def get_amt_total2(self, account_invoice):
        amount = 0
        for val in account_invoice.invoice_line_ids:
            amount = amount + val.quantity
            return total2

    def get_total_rate(self, account_invoice):
        total_rate = 0.0
        for val in account_invoice.invoice_line_ids:
            total_rate = total_rate + val.price_unit
        return total_rate

    def get_text_rate_name(self, account_invoice):
        rate = 0.0
        tax_list = []
        tax_list1 = []
        for val in account_invoice.invoice_line_ids:
            tax_list.append(val.invoice_line_tax_ids.id)
        if tax_list:
            tax_list = list(set(tax_list))
            tax_list1 = sorted(tax_list)
        return tax_list1

    def get_name(self, tax_list1):
        str1 = ' '
        if tax_list1:
            obj = self.env['account.tax'].browse(tax_list1)
            str1 = ''.join(obj.name)
        return str1

    def get_gst_basic_rate(self, tax_list1, account_invoice):
        value = 0.0
        if tax_list1:
            tax_obj = self.env['account.tax'].browse(tax_list1)
            line_obj = self.env['account.invoice.line'].search(
                [('invoice_line_tax_ids', '=', tax_obj.id), ('invoice_id', '=', account_invoice.id)])
            amount = 0.0
            value = 0.0
            for val in line_obj:
                amount = val.price_subtotal
                value = value + amount
        return value

    def get_cgst_sgst_tax(self, tax_list1, account_invoice):
        rate = 0
        if account_invoice.journal_id.bill_from.state_id == account_invoice.partner_id.state_id:
            if tax_list1:
                tax_obj = self.env['account.tax'].browse(tax_list1)
                line_obj = self.env['account.invoice.line'].search(
                    [('invoice_line_tax_ids', '=', tax_obj.id), ('invoice_id', '=', account_invoice.id)])
                amount = 0.0
                value = 0.0
                for val3 in line_obj:
                    r = val3.invoice_line_tax_ids.amount
                    rate = (r / 2 * 100)
        return rate

    def get_cgst_sgst_amt(self, tax_list1, account_invoice):
        rate = 0
        amt = 0.0
        if account_invoice.journal_id.bill_from.state_id == account_invoice.partner_id.state_id:
            line_obj_ref = ' '
            if tax_list1:
                tax_obj = self.env['account.tax'].browse(tax_list1)
                line_obj = self.env['account.invoice.line'].search(
                    [('invoice_line_tax_ids', '=', tax_obj.id), ('invoice_id', '=', account_invoice.id)])
                amount = 0.0
                value = 0.0
                for val3 in line_obj:
                    line_obj_ref = val3
                    amount = amount + val3.price_subtotal
                r = line_obj_ref.invoice_line_tax_ids.amount
                rate = (r / 2 * 100)
                amt = amount * rate / 100
#                 amt = round(amt)
        return amt

    def get_igst_tax(self, tax_list1, account_invoice):
        rate = 0
        if account_invoice.journal_id.bill_from.state_id != account_invoice.partner_id.state_id:
            if tax_list1:
                tax_obj = self.env['account.tax'].browse(tax_list1)
                line_obj = self.env['account.invoice.line'].search(
                    [('invoice_line_tax_ids', '=', tax_obj.id), ('invoice_id', '=', account_invoice.id)])
                amount = 0.0
                value = 0.0
                for val3 in line_obj:
                    r = val3.invoice_line_tax_ids.amount
                    rate = r
        return rate

    def get_igst_amt(self, tax_list1, account_invoice):
        rate = 0
        amt = 0.00
        line_obj_ref = ' '
        if account_invoice.journal_id.bill_from.state_id != account_invoice.partner_id.state_id:
            if tax_list1:
                tax_obj = self.env['account.tax'].browse(tax_list1)
                line_obj = self.env['account.invoice.line'].search(
                    [('invoice_line_tax_ids', '=', tax_obj.id), ('invoice_id', '=', account_invoice.id)])
                amount = 0.0
                value = 0.0
                for val3 in line_obj:
                    # line_obj_ref = self.env[
                    #     'account.invoice.line'].browse(val3)
                    amount = amount + val3.price_subtotal
                r = line_obj.invoice_line_tax_ids.amount
                rate = (r * 100)
                amt = amount * rate / 100

    def get_total_tax_amt(self, tax_list1, account_invoice):
        rate = 0
        amt = 0.0
        if tax_list1:
            tax_obj = self.env['account.tax'].browse(tax_list1)
            line_obj = self.env['account.invoice.line'].search(
                ['|', ('invoice_line_tax_ids', '=', tax_obj.id), ('invoice_id', '=', account_invoice.id)])
            amount = 0.0
            value = 0.0
            for val3 in line_obj:
                amount = amount + val3.price_subtotal

            r = line_obj.invoice_line_tax_ids.amount
            rate = (r * 100)
            amt = amount * rate / 100
            amt = int(round(amt))
        return amt

##################### For add CGST and SGST #########################
    def get_add_cgst_sgst(self, account_invoice):
        add_cgst_sgst = 0.0
        if account_invoice.journal_id.bill_from.state_id == account_invoice.partner_id.state_id:
            for val in account_invoice.invoice_line:
                amount = val.price_subtotal
                tax = (val.invoice_line_tax_ids.amount / 2) * 100
                amt = amount * tax / 100
                add_cgst_sgst = add_cgst_sgst + amt
        return add_cgst_sgst

    def get_add_igst(self, account_invoice):
        add_igst = 0.0
        if account_invoice.journal_id.bill_from.state_id != account_invoice.partner_id.state_id:
            for val in account_invoice.invoice_line_ids:
                amount = val.price_subtotal
                tax = val.invoice_line_tax_ids.amount * 100
                amt = amount * tax / 100
                add_igst = add_igst + amt
        return add_igst

    def get_add_tcs(self, account_invoice):
        add_tcs = 0.0
        for val in account_invoice.tax_line_ids:
            if val.name == 'TCS @ 1%':
                add_tcs = val.amount
        return add_tcs

    def get_total_amt_after_gst(self, account_invoice):
        amt = 0.0
        add_tcs = 0.0
        for val in account_invoice.tax_line_ids:
            if val.name == 'TCS @ 1%':
                add_tcs = val.amount

        amt = account_invoice.amount_total - add_tcs
        return amt


    def get_tax_table(self, account_invoice):
        tax_table = {}
        dict1 = {}
        dic = {}
        make_dict = {}
        keys = ['total_value', 'taxable_value', 'igst', 'cgst']
        lis1 = []
        for invoice_lines in account_invoice.invoice_line_ids:
            print"invoice_line++++++++++++++", invoice_lines
            #if self.invoice_line_tax_ids:
            rate = sum(
                lines.amount for line in invoice_lines.invoice_line_tax_ids for lines in line.children_tax_ids)
            taxes = invoice_lines.invoice_line_tax_ids.compute_all(
                invoice_lines.price_unit, invoice_lines.invoice_id.currency_id,
                invoice_lines.quantity, product=invoice_lines.product_id,
                partner=invoice_lines.invoice_id.partner_id)
            subtotal = taxes[
                'total_excluded'] if taxes else (
                    invoice_lines.quantity * invoice_lines.price_unit)
            price_subtotal = taxes[
                'total_included'] if taxes else subtotal
            tax_amount1 = price_subtotal - subtotal
            tax_amount = tax_amount1 * (1 - (invoice_lines.discount or 0.0) / 100.0)
            dic.setdefault(self.get_hsn_sac(invoice_lines), {})
            for key in keys:
                dic[self.get_hsn_sac(invoice_lines)].setdefault(key, 0)
            for tax in invoice_lines.mapped('invoice_line_tax_ids'):
                if tax.tax_group_id.name == 'Taxes' and tax.amount_type == 'group':
                    for child_tax in tax.children_tax_ids:
                        print"child_taxchild_tax", child_tax.tax_group_id.name
                        if self.get_hsn_sac(invoice_lines) not in make_dict:
                                if (child_tax.tax_group_id.name == 'CGST')and child_tax.amount_type == 'percent':
                                    make_dict[self.get_hsn_sac(invoice_lines)] = [{
                                        rate: {
                                            'igst': 0,
                                            'rate': child_tax.amount,
                                            'cgst': (round((invoice_lines.price_subtotal * child_tax.amount) / 100,2)),
                                            'sgst': 0,
                                            'taxable_value': invoice_lines.price_subtotal
                                        }
                                    }]
                                elif (child_tax.tax_group_id.name == 'SGST')and child_tax.amount_type == 'percent':
                                    make_dict[self.get_hsn_sac(invoice_lines)] = [{
                                        rate: {
                                            'igst': 0,
                                            'rate': child_tax.amount,
                                            'cgst': 0,
                                            'sgst': (round((invoice_lines.price_subtotal * child_tax.amount) / 100,2)),
                                        }
                                    }]
                        elif self.get_hsn_sac(invoice_lines) in make_dict:
                            if (child_tax.tax_group_id.name == 'CGST')and child_tax.amount_type == 'percent':
                                for ls in make_dict[self.get_hsn_sac(invoice_lines)]:
                                    lis1.append(ls.keys()[0])
                                if rate not in lis1:
                                    make_dict[self.get_hsn_sac(invoice_lines)].append({
                                        rate: {
                                            'igst': 0,
                                            'rate': child_tax.amount,
                                            'cgst': (round((invoice_lines.price_subtotal * child_tax.amount) / 100,2)),
                                            'sgst': 0,
                                            'taxable_value': invoice_lines.price_subtotal
                                        }
                                    })
                                else:
                                    for count, ls in enumerate(make_dict[self.get_hsn_sac(invoice_lines)]):
                                        if rate in ls.keys():
                                            make_dict[self.get_hsn_sac(invoice_lines)][count][rate]['cgst'] +=  (round((invoice_lines.price_subtotal * child_tax.amount) / 100,2))
                                            make_dict[self.get_hsn_sac(invoice_lines)][count][rate]['taxable_value'] +=  invoice_lines.price_subtotal
                            elif (child_tax.tax_group_id.name == 'SGST')and child_tax.amount_type == 'percent':
                                for ls in make_dict[self.get_hsn_sac(invoice_lines)]:
                                    lis1.append(ls.keys()[0])
                                if rate not in lis1:
                                    make_dict[self.get_hsn_sac(invoice_lines)].append({
                                        rate: {
                                            'igst': 0,
                                            'rate': child_tax.amount,
                                            'sgst': (round((invoice_lines.price_subtotal * child_tax.amount) / 100,2)),
                                            'cgst': 0,
                                        }
                                    })
                                else:
                                    for count, ls in enumerate(make_dict[self.get_hsn_sac(invoice_lines)]):
                                        if rate in ls.keys():
                                            make_dict[self.get_hsn_sac(invoice_lines)][count][rate]['sgst'] +=  (round((invoice_lines.price_subtotal * child_tax.amount) / 100,2))
                else:
                    rate = sum(
                        line.amount for line in invoice_lines.invoice_line_tax_ids
                    )
                    if self.get_hsn_sac(invoice_lines) not in make_dict:
                        # for tax in tax.children_tax_ids:
                        if tax.tax_group_id.name == 'IGST' and tax.amount_type == 'percent':
                            make_dict[self.get_hsn_sac(invoice_lines)] = [{
                                rate: {
                                    'igst': (round((invoice_lines.price_subtotal * tax.amount) / 100,2)),
                                    'cgst': 0,
                                    'sgst': 0,
                                    'taxable_value': invoice_lines.price_subtotal,
                                    'rate': 0,
                                }
                            }]
                    # elif rate not in make_dict[self.get_hsn_sac(invoice_lines)]:
                    elif self.get_hsn_sac(invoice_lines) in make_dict:
                        for ls in make_dict[self.get_hsn_sac(invoice_lines)]:
                            lis1.append(ls.keys()[0])
                        if rate not in lis1:
                            make_dict[self.get_hsn_sac(invoice_lines)].append({
                                rate: {
                                    'igst': (round((invoice_lines.price_subtotal * tax.amount) / 100,2)),
                                    'cgst': 0,
                                    'sgst': 0,
                                    'taxable_value': invoice_lines.price_subtotal,
                                    'rate': 0,
                                }
                            })
                        else:
                            for count, ls in enumerate(make_dict[self.get_hsn_sac(invoice_lines)]):
                                if rate in ls.keys():
                                    make_dict[self.get_hsn_sac(invoice_lines)][count][rate]['igst'] += (round((invoice_lines.price_subtotal * tax.amount) / 100,2))
                                    make_dict[self.get_hsn_sac(invoice_lines)][count][rate]['taxable_value'] +=  invoice_lines.price_subtotal
        print"make_dictmake_dictmake_dict%%%%%%%%%%%%%%%%%%%%%%%", make_dict
        return make_dict

    def cgst_rate_total(self, taxable_value, total_cgst_amount):
        total_rate = (total_cgst_amount / taxable_value) * 100
        return total_rate

    def sgst_rate_total(self, taxable_value, total_sgst_amount):
        total_rate = (total_sgst_amount / taxable_value) * 100
        return total_rate

    def igst_rate_total(self, taxable_value, total_igst_amount):
        total_rate = (total_igst_amount / taxable_value) * 100
        return total_rate

    @api.model
    def render_html(self, docids, data=None):
        model = 'account.invoice'
        docs = self.env[model].browse(docids)
        movelines = self
        docargs = {
            'doc_ids': self.ids,
            'doc_model': model,
            # 'data': data['form'],
            'docs': docs,
            'time': time,
            'get_partner_lines': movelines,
            # 'get_direction': total,
            'get_rate': self.get_rate,
            'get_amt': self.get_amt,
            'get_amt_total': self.get_amt_total,
            'get_amt_total1': self.get_amt_total1,
            'get_line_tax': self.get_line_tax,
            'get_total_tax': self.get_total_tax,
            'get_total_with_tax': self.get_total_with_tax,
            # 'get_total': self.get_total,
            'get_total_qty': self.get_total_qty,
            # 'get_total_rate': self.get_total_rate,
            # 'get_amt_total2': self.get_amt_total2,
            'get_igst_amt': self.get_igst_amt,
            # 'get_add_igst': self.get_add_igst,
            'get_total_tax_amt': self.get_total_tax_amt,
            'get_text_rate_name': self.get_text_rate_name,
            'get_name': self.get_name,
            # 'get_gst_basic_rate': self.get_gst_basic_rate,
            'get_cgst_sgst_tax': self.get_cgst_sgst_tax,
            'get_cgst_sgst_amt': self.get_cgst_sgst_amt,
            # 'get_add_cgst_sgst': self.get_add_cgst_sgst,
            'get_igst_tax': self.get_igst_tax,
            'get_convert_amt': self.get_convert_amt,
            'get_hsn_sac': self.get_hsn_sac,
            'get_add_tcs': self.get_add_tcs,
            'get_total_amt_after_gst': self.get_total_amt_after_gst,
            'get_tax_table': self.get_tax_table,
            'cgst_rate_total': self.cgst_rate_total,
            'sgst_rate_total': self.sgst_rate_total,
            'igst_rate_total': self.igst_rate_total,
        }
        return self.env['report'].render('cjpl_custom.print_domestic_report', docargs)
