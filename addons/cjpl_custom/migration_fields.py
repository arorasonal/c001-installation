from odoo import api, fields, models


class AccountTax(models.Model):

    _inherit = 'account.tax'

    odoo8_id = fields.Integer()  # for migration
    imported = fields.Boolean()


class AccountTax(models.Model):

    _inherit = 'mail.message'

    odoo8_id = fields.Integer()  # for migration
    parent = fields.Integer()
    imported = fields.Boolean()
