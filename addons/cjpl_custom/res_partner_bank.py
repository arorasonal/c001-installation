from odoo import fields, models


class PartnerBank(models.Model):
    _inherit = "res.partner.bank"

    show_in_invoice = fields.Boolean('Show in Invoice')
