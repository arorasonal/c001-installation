from odoo import fields, models, api, _

class ProductTemplate(models.Model):

	_inherit = "product.template"

	imported = fields.Boolean("Imported from Odoo8")
