{
    "name": "Invoice Cjpl",
    "version": "10.0.0.1.0",
    'website': 'http://www.apagen.com',
    'images': [],
    "depends": ['account', 'sale_order_type', 'base', 'account_invoice_shipping_address', 'eway_bill'],
    "author": "Apagen Solution Pvt Ltd.",
    "category": "GTR",
    'sequence': 16,
    "description": """
    This module will provide the detail of sample and convert it into a product and some report regarding sample  
    """,
    'data': [
        'security/ir.model.access.csv',
        'views/account_invoice_view.xml',
        'views/account_journal.xml',
    ],
    'demo_xml': [],
    'css': ['static/src/css/sample_kanban.css'],
    'installable': True,
    'active': False,
    'auto_install': False,
    #    'certificate': 'certificate',
}
