import itertools
from lxml import etree
import json
from odoo import models, fields, api, _
from odoo.exceptions import UserError, ValidationError, Warning
from odoo.tools import float_compare
import odoo.addons.decimal_precision as dp
from datetime import datetime
# -- 31-03-2020 
# date of invoice cannot be less than the maximum date of any invoice in the system
# the default date of the invoice is set to today
# however if a system parameter "invoice_date" is found , then the default date will be the value of this parameter
# system parameter "invoice_date" must be entered in yyyy-mm-dd format
# this will allow making of bills after the end of year
# this can be used at any time of the year - may be at the end of the month  
from odoo.http import request


class account_invoice(models.Model):
    _inherit = "account.invoice"

    # @api.model
    # def _onchange_user(self, user_id):
    #     if self.prepared_by and self._context.get('team_id'):
    #         team = self.env['crm.team'].browse(self._context['team_id'])
    #         if user_id in team.member_ids.ids:
    #             return {}
    #     team_id = self.env['crm.team']._get_default_team_id(user_id=user_id)
    #     return {'checked_by': team_id}

    # @api.onchange('prepared_by')
    # def _onchange_user_id(self):
    #     values = self._onchange_user(self.user_id.id)
    #     self.update(values)

    @api.multi
    def amount_to_text(self, amount, currency='INR'):
        return amount_to_text(amount, currency)

    invoice_history_lines = fields.One2many(
        'invoice.history', 'invoice_id', 'Invoice Lines')
    date = fields.Date('Date')
    sale_type = fields.Many2one('sale.order.type', string='Sale Type')
    kind_attn = fields.Char("Contact (Kind Attn:)")
    po_ref = fields.Char("PO Number")
    checked_by = fields.Many2one('res.users', 'Checked By')
    received_by = fields.Char('Received By')
    received_date = fields.Date('Received Date')
    prepared_by = fields.Many2one(
        'res.users', 'Prepared By', default=lambda self: self.env.user)
    transportation_mode_id = fields.Many2one(
        'eway.bill.trans', "Transportation Mode")
    vehicle_no = fields.Char("Vehicle No")
    sale_order_id = fields.Char('Sale Order')
    no_of_packages = fields.Integer("No. of Packages")
    ordering_contract_id = fields.Many2one(
        'res.partner', 'Ordering Contact',
        readonly=True,
        states={
            'draft': [('readonly', False)],
            'sent': [('readonly', False)]
        })
    partner_invoice_id = fields.Many2one('res.partner', 'Invoice Address')
    account_id = fields.Many2one(
        'account.account', string='Account',
        required=True, readonly=True, states={'draft': [('readonly', False)]},
        domain=[('deprecated', '=', False)], help="The partner account used for this invoice.")
    partner_shipping_id = fields.Many2one(
        'res.partner',
        string='Shipping Address',
        readonly=True,
        states={'draft': [('readonly', False)]},
        help="Delivery address for current invoice.")
    date_invoice = fields.Date(string='Invoice Date',
                               readonly=True, states={'draft': [('readonly', False)]}, index=True,
                               help="Keep empty to use the current date", copy=False)

    line_conbined = fields.One2many(
        'account.line.combine', 'invoice_combine_id', "Combined Line")
    amount_total_words = fields.Char(
        "Total (In Words)", compute="_compute_amount_total_words")
    company_id = fields.Many2one('res.company', string='Company', change_default=True,
                                 required=True, readonly=True, states={'draft': [('readonly', False)]},
                                 default=lambda self: self.env['res.company']._company_default_get('account.invoice'))

    # for database migration
    imported = fields.Boolean("Imported")
    odoo8_id = fields.Integer()
    journal_id8 = fields.Integer()
    bank_account_id8 = fields.Integer()
    move_id8 = fields.Integer()
    account_id8 = fields.Integer()
    validate_flag = fields.Boolean(default=False)
    delivery_gst = fields.Char('Delivery GSTIN')
    invoice_gst = fields.Char('Invoice GSTIN')

    @api.onchange('partner_shipping_id')
    def _onchange_partner_shipping_id(self):
        """
        Trigger the change of fiscal position when the shipping address is modified.
        """
        fiscal_position = self.env['account.fiscal.position'].get_fiscal_position(self.partner_id.id,
                                                                                  self.partner_shipping_id.id)
        if fiscal_position:
            self.fiscal_position_id = fiscal_position
        self.delivery_gst = self.partner_shipping_id.gstin

    @api.onchange('partner_invoice_id')
    def _onchange_partner_invoice_id(self):
        self.invoice_gst = self.partner_invoice_id.gstin

    @api.depends('amount_total')
    def _compute_amount_total_words(self):
        for invoice in self:
            try:
                # invoice.amount_total_words = invoice.currency_id.amount_to_text(
                #     invoice.amount_total)
                invoice.amount_total_words = num2words(
                    invoice.amount_total, lang='en_IN')
            except:
                pass

    @api.model
    def create(self, vals):
        res = super(account_invoice, self).create(vals)
        res.get_combined_line()
        # if res.state:
        #     for rec in self.line_conbined:
        #         rec.update({
        #             'price_subtotal': self.amount_untaxed,
        #         })
        return res

    @api.multi
    def write(self, vals):
        res = super(account_invoice, self).write(vals)
        self.get_combined_line()
        return res

    @api.multi
    @api.onchange('sale_type_id')
    def onchange_sale_type(self):
        for line in self.invoice_line_ids:
            if self.sale_type_id.is_contract:
                line.hsn_no = self.sale_type_id.sac
            else:
                if line.product_id.hsn_no:
                    line.hsn_no = line.product_id.hsn_no
                else:
                    line.hsn_no = line.product_id.categ_id.hsn_no

    @api.multi
    def get_combined_line(self):
        for inv in self:
            if inv._name == 'account.invoice':
                for loop in inv.line_conbined:
                    loop.unlink()
                list1 = []
                list2 = []
                for record in inv.invoice_line_ids:
                    if record.get_hsn(inv) not in list1:
                        list_id = {'hsn_sac_id': record.get_hsn(inv),
                                   'price_subtotal': record.price_subtotal,
                                   # 'invoice_line_tax_ids': [(6, 0, record.invoice_line_tax_ids.ids)],
                                   'invoice_combine_id': inv.id,
                                   }
                        combine_id = self.env[
                            'account.line.combine'].create(list_id)
                        self.env['line.combine'].create(
                            {'combine_id': combine_id.id,
                             'price_subtotal': record.price_subtotal,
                             'invoice_line_tax_ids': [(6, 0, record.invoice_line_tax_ids.ids)]})
                        list1.append(record.get_hsn(inv))

                    else:
                        a = self.env['account.line.combine'].search(
                            [('invoice_combine_id', '=', inv.id),
                             ('hsn_sac_id', '=', record.get_hsn(inv))], limit=1)
                        a.price_subtotal += record.price_subtotal
                        a.write({'price_subtotal': a.price_subtotal})
                        self.env['line.combine'].create(
                            {'combine_id': a.id,
                             'price_subtotal': record.price_subtotal,
                             'invoice_line_tax_ids': [(6, 0, record.invoice_line_tax_ids.ids)]})

        return True

    @api.model
    def add_invoice(self):
        sale2 = []
        sale3 = []
        sale = self.env['account.invoice'].search([('state', '=', 'draft')])
        for val in sale:
            sale2 = []
            sale3 = []
            invoice = []
            invoice_obj = ' '
            sale_obj = self.env['account.invoice'].browse(val.id)
            invoice = self.env['invoice.history'].search(
                [('invoice_id', '=', sale_obj.id)])
            invoice = sorted(invoice, reverse=True)
            sale3_obj = ' '
            sale1 = self.env['account.invoice'].search(
                [('partner_id', '=', sale_obj.partner_id.id), ('state', '!=', 'draft')])
            sale2 = []
            if sale1:
                for val1 in sale1:
                    sale1_obj = self.env['account.invoice'].browse(val1.id)
                    sale2.append(sale1_obj.date_invoice)
                    sale2 = sorted(sale2, reverse=True)
            if sale2:
                date = sale2[0]
            sale3 = self.env['account.invoice'].search(
                [('date_invoice', '=', date), ('partner_id', '=', sale_obj.partner_id.id)])
            for val3 in sale3:
                sale3_obj = self.env['account.invoice'].browse(val3.id)
            if invoice:
                for val4 in invoice:
                    invoice_obj = self.env[
                        'invoice.history'].browse(val4[0].id)
            if sale3_obj != ' ':
                if invoice_obj != ' ':
                    if invoice_obj.number != sale3_obj.number:
                        self.env['invoice.history'].create({'invoice_id': sale_obj.id,
                                                            'number': sale3_obj.number,
                                                            'state': sale3_obj.state,
                                                            'origin': sale3_obj.origin,
                                                            'amount': sale3_obj.amount_total,
                                                            'bill_period_from': sale3_obj.bill_period_from,
                                                            'bill_period_to': sale3_obj.bill_period_to,
                                                            'date': sale3_obj.date_invoice, 'checked_by': sale3_obj.checked_by.id,
                                                            'user_id': sale3_obj.user_id.id, 'team_id': sale3_obj.team_id.id})
                else:
                    self.env['invoice.history'].create({'invoice_id': sale_obj.id,
                                                        'number': sale3_obj.number,
                                                        'state': sale3_obj.state,
                                                        'origin': sale3_obj.origin,
                                                        'amount': sale3_obj.amount_total,
                                                        'bill_period_from': sale3_obj.bill_period_from,
                                                        'bill_period_to': sale3_obj.bill_period_to,
                                                        'date': sale3_obj.date_invoice, 'checked_by': sale3_obj.checked_by.id,
                                                        'user_id': sale3_obj.user_id.id, 'team_id': sale3_obj.team_id.id})
        return True

    @api.multi
    def apply_tcs_tax(self):
        p = 0
        obj = self.env['account.invoice'].browse()
        tax_obj_ref = ' '
        amt = (obj.amount_total * 1) / 100
        tax_account_obj = self.env['account.tax'].search(
            [('name', '=', 'TCS @ 1%')])
        tax_account_obj_ref = self.env['account.tax'].browse(tax_account_obj)

        tax_obj = self.env['account.invoice.tax'].search(
            [('invoice_id', '=', obj.id)])
        if tax_obj:
            for val in tax_obj:
                tax_obj_ref = self.env['account.invoice.tax'].browse(val)

                if tax_obj_ref.name == 'TCS @ 1%':
                    p = 1
                    break
                else:
                    p = 0
        else:
            p = 0
        if p == 0:
            self.env['account.invoice.tax'].create({'invoice_id': obj.id, 'name': tax_account_obj_ref.name, 'account_id': tax_account_obj_ref.account_collected_id.id,
                                                    'amount': amt, 'base': obj.amount_untaxed})

    # date of invoice cannot be less than the maximum date of any invoice in the system
    # the default date of the invoice is set to today
    # however if a system parameter "invoice_date" is found , then the default date will be the value of this parameter
    # system parameter "invoice_date" must be entered in yyyy-mm-dd format
    # this will allow making of bills after the end of year
    # this can be used at any time of the year - may be at the end of the month  
    @api.multi
    def action_invoice_open(self):
        if not self.move_name:
            for invoice in self:
                if invoice.type in ('out_invoice', 'out_refund'):
                    if self.search([
                        ('type', '=', invoice.type),
                            ('date_invoice', '>', invoice.date_invoice),
                            ('move_name', '!=', False)]):
                        raise UserError(
                            _('Cannot create invoice!'
                              'Post the invoice with a greater date'))
                    else:
                        default_invoice_date  = request.env['ir.config_parameter'].sudo().get_param('invoice_date')
                        if default_invoice_date:
                            self.write({'date_invoice': default_invoice_date})
                        else:
                            self.write({'date_invoice': datetime.now()})
                self.write({'prepared_by': self.env.user.id})
        return super(account_invoice, self).action_invoice_open()


class invoice_history(models.Model):
    _name = "invoice.history"
    _order = "date desc"

    invoice_id = fields.Many2one('account.invoice', 'Account Invoice')
    number = fields.Char('Number')
    state = fields.Char('State')
    origin = fields.Char('Source Document')
    amount = fields.Float('Amount')
    bill_period_from = fields.Datetime('Bill From')
    bill_period_to = fields.Datetime('Bill To')
    date = fields.Date('Date')
    checked_by = fields.Many2one('res.users', 'Checked By')
    user_id = fields.Many2one('res.users', 'Salesperson')
    team_id = fields.Many2one('crm.team', 'Sales Team')


# class account_invoice(models.Model):
#     _name="transportation.mode"

#     name = fields.Char()


class AccountInvoiceLine(models.Model):
    _inherit = "account.invoice.line"

    imported = fields.Boolean()
    account_id8 = fields.Integer()
    account_id = fields.Many2one('account.account', string='Account',
                                 required=False, domain=[('deprecated', '=', False)],
                                 help="The income or expense account related to the selected product.")
    invoice_id8 = fields.Integer()
    hsn_no = fields.Char('HSN')
    odoo8_id = fields.Integer()
    # uom_id = fields.Many2one('product.uom', string='Unit of Measure',
    #     ondelete='set null', index=True, oldname='uos_id', force_save="1")

    @api.multi
    def write(self, vals):
        res = super(AccountInvoiceLine, self).write(vals)
        if 'product_id' in vals:
            vals.update({'price_unit': vals.get('price_unit'),
                         }
                        )
        return res

    @api.model
    def create(self, vals):
        # res = super(AccountInvoiceLine, self).create(vals)
        if vals.get('product_id'):
            vals.update({'price_unit': vals.get('price_unit'),
                         }
                        )
        return super(AccountInvoiceLine, self).create(vals)

    @api.multi
    def _onchange_hsn(self, invoic):
        sale_type_order = self.env['sale.order.type'].search(
            [('is_contract', '!=', True)])
        sale_type = self.env['account.invoice'].browse(sale_type_order)
        if sale_type:
            if self.product_id.hsn_no:
                self.hsn_no = self.product_id.hsn_no
            else:
                self.hsn_no = self.product_id.categ_id.hsn_no
        else:
            self.hsn_no = sale_type.sale_type_id.sac

    @api.multi
    def get_hsn(self, invoice_id):
        if invoice_id.sale_type_id.is_contract:
            return invoice_id.sale_type_id.sac
        else:
            if self.product_id.hsn_no:
                return self.product_id.hsn_no
            else:
                return self.product_id.categ_id.hsn_no

    @api.onchange('product_id')
    def _onchange_product_id(self):
        domain = {}
        if not self.invoice_id:
            return

        part = self.invoice_id.partner_id
        fpos = self.invoice_id.fiscal_position_id
        company = self.invoice_id.company_id
        currency = self.invoice_id.currency_id
        type = self.invoice_id.type

        if not part:
            warning = {
                'title': _('Warning!'),
                'message': _('You must first select a partner!'),
            }
            return {'warning': warning}

        if not self.product_id:
            if type not in ('in_invoice', 'in_refund'):
                self.price_unit = 0.0
            domain['uom_id'] = []
        else:
            if part.lang:
                product = self.product_id.with_context(lang=part.lang)
            else:
                product = self.product_id

            self.name = product.partner_ref
            account = self.get_invoice_line_account(
                type, product, fpos, company)
            if account:
                self.account_id = account.id
            self._set_taxes()

            if type in ('in_invoice', 'in_refund'):
                if product.description_purchase:
                    self.name += '\n' + product.description_purchase
            else:
                if product.description_sale:
                    self.name += '\n' + product.description_sale
            self.hsn_no = self.get_hsn(self.invoice_id)

            if not self.uom_id or product.uom_id.category_id.id != self.uom_id.category_id.id:
                self.uom_id = product.uom_id.id
            domain['uom_id'] = [
                ('category_id', '=', product.uom_id.category_id.id)]

            if company and currency:

                if self.uom_id and self.uom_id.id != product.uom_id.id:
                    self.price_unit = product.uom_id._compute_price(
                        self.price_unit, self.uom_id)
        return {'domain': domain}


class AccountInvoiceCombine(models.Model):
    _name = "account.line.combine"
    _rec_name = 'hsn_sac_id'

    invoice_combine_id = fields.Many2one('account.invoice', string='Invoice')
    hsn_sac_id = fields.Char("HSN / SAC")
    price_subtotal = fields.Float(string='Amount')
    line_combined = fields.One2many(
        'line.combine', 'combine_id', "Combined Line")


class AccountInvoiceCombineChild(models.Model):
    _name = "line.combine"

    combine_id = fields.Many2one('account.line.combine')
    price_subtotal = fields.Float()
    invoice_line_tax_ids = fields.Many2many(
        'account.tax', 'combine_line_tax', 'line_combine_id', 'tax_id',
        string='Taxes', domain=[
            ('type_tax_use', '!=', 'none'), '|', ('active', '=', False),
            ('active', '=', True)])


class AccountMove(models.Model):
    _inherit = "account.move"

    odoo8_id = fields.Integer()
    imported = fields.Boolean()


class AccountMoveLine(models.Model):
    _inherit = "account.move.line"

    odoo8_id = fields.Integer()
    imported = fields.Boolean()
    date_maturity = fields.Date(string='Due date', index=True, required=False,
                                help="This field is used for payable and receivable journal entries. You can put the limit date for the payment of this line.")
    name = fields.Char(required=False, string="Label")
    account_id = fields.Many2one('account.account', string='Account', required=False, index=True,
                                 ondelete="cascade", domain=[('deprecated', '=', False)], default=lambda self: self._context.get('account_id', False))

    # @api.multi
    # def _update_check(self):

    #     return True

    # def calculate_residual(self):
    #     """ Computes the residual amount of a move line from a reconciliable account in the company currency and the line's currency.
    #         This amount will be 0 for fully reconciled lines or lines from a non-reconciliable account, the original line amount
    #         for unreconciled lines, and something in-between for partially reconciled lines.
    #     """
    #     for line in self:
    #         # if not line.account_id.reconcile and line.account_id.internal_type != 'liquidity':
    #         #     line.reconciled = False
    #         #     line.amount_residual = 0
    #         #     line.amount_residual_currency = 0
    #         #     continue
    #         # amounts in the partial reconcile table aren't signed, so we need
    #         # to use abs()
    #         amount = abs(line.debit - line.credit)
    #         amount_residual_currency = abs(line.amount_currency) or 0.0
    #         sign = 1 if (line.debit - line.credit) > 0 else -1
    #         if not line.debit and not line.credit and line.amount_currency and line.currency_id:
    #             # residual for exchange rate entries
    #             sign = 1 if float_compare(
    # line.amount_currency, 0, precision_rounding=line.currency_id.rounding)
    # == 1 else -1

    #         for partial_line in (line.matched_debit_ids + line.matched_credit_ids):
    #             # If line is a credit (sign = -1) we:
    #             #  - subtract matched_debit_ids (partial_line.credit_move_id == line)
    #             #  - add matched_credit_ids (partial_line.credit_move_id != line)
    #             # If line is a debit (sign = 1), do the opposite.
    #             sign_partial_line = sign if partial_line.credit_move_id == line else (
    #                 -1 * sign)

    #             amount += sign_partial_line * partial_line.amount
    #             # getting the date of the matched item to compute the
    #             # amount_residual in currency
    #             if line.currency_id:
    #                 if partial_line.currency_id and partial_line.currency_id == line.currency_id:
    #                     amount_residual_currency += sign_partial_line * partial_line.amount_currency
    #                 else:
    #                     if line.balance and line.amount_currency:
    #                         rate = line.amount_currency / line.balance
    #                     else:
    #                         date = partial_line.credit_move_id.date if partial_line.debit_move_id == line else partial_line.debit_move_id.date
    #                         rate = line.currency_id.with_context(
    #                             date=date).rate
    #                     amount_residual_currency += sign_partial_line * \
    #                         line.currency_id.round(partial_line.amount * rate)

    #         line.amount_residual = line.company_id.currency_id.round(
    #             amount * sign)
    #         line.amount_residual_currency = line.currency_id and line.currency_id.round(
    #             amount_residual_currency * sign) or 0.0

    #     return True


class account_journal(models.Model):
    _inherit = "account.journal"

    bill_from = fields.Many2one('res.company', "Bill Address")
