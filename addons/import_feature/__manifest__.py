# -*- coding: utf-8 -*-
#See LICENSE file for full copyright and licensing details.

{
    'name': "Import Feature",
    'category': "Attendance",
    'version': "1.0",
    'depends': ['hr_attendance'],
    'author':'Developer',
    'description': """
Import Feature
============

This module allows you to...
    Show detail of import attendance
""",
    'data': [
        'views/attendance_data.xml',
        
    ],
    'installable': True,
    'application': False,

}
