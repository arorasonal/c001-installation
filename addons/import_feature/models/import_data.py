# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import api, fields, models,_,SUPERUSER_ID
import io
import openpyxl
import base64
try:
    from StringIO import StringIO
except ImportError:
    from io import StringIO
import re
import csv,json
import logging
from datetime import datetime
from os.path import expanduser
from odoo.exceptions import UserError
import xlsxwriter
import os
_logger = logging.getLogger(__name__)
import time
import csv
from datetime import datetime, timedelta


attendance_data = ['Employee Code', 'Check In', 'Check Out']

attendance_data_key = ['employee_id', 'check_in', 'check_out']

MappingDic = {
    
    'Employee Code': 'employee_id',
    'Check In': 'check_in',
    'Check Out': 'check_out'
}

class AttendanceImport(models.Model):
    _name='import.attendance.data'

    name = fields.Char('Name')
    import_file = fields.Binary(string='Upload File')
    file_name = fields.Char("File Name")
    error_file =fields.Binary("Error File")
    error_file_name = fields.Char("File Name")
    active = fields.Boolean('Active', default=False)
    state = fields.Selection(
        [('draft', "Draft"),
         ('confirm', 'Confirmed')], "State", track_visibility='always', default='draft')



    @api.model
    def create(self, vals):
        if 'file_name' in vals and vals.get('file_name') not in [None,'None',False,'']:
            name, ext = os.path.splitext(vals.get('file_name'))
            if ext not in ('.csv','.xlsx'):
                raise UserError(_("Please select CSV or Excel File!"))
        return super(AttendanceImport, self).create(vals) 


    @api.multi
    def write(self,vals):
        if 'file_name' in vals and vals.get('file_name') not in [None,'None',False,'']:
            name, ext = os.path.splitext(vals.get('file_name'))
            if ext not in ('.csv','.xlsx'):
                raise UserError(_("Please select CSV or Excel File!"))
        return super(AttendanceImport, self).write(vals)

    @api.multi
    def import_attendance_data(self):
        for attendance in self:
            if attendance.file_name:
                name, ext = os.path.splitext(attendance.file_name)
                if ext == '.csv':
                    csv_dt = attendance._read_csv(options={})
                    self.create_attendance_data(csv_dt)
                elif ext == '.xlsx' or ext == '.xls':
                    exl_dt = self._read_excel(options={})
                    exl_hdr = self._validate_header(exl_dt)
                    rep_header = tuple(attendance_data_key)
                    exl_data_dic,total_lst = self.excel2Dict_header_dynamic(exl_dt,rep_header)
                    result = self.check_data(exl_data_dic)


    def excel2Dict_header_dynamic(self,sheet,MappinfDic):
        records = []
        count = 0
        total_lst = []
        keys = list(MappinfDic)
        for row in sheet.rows:
            temp = []
            i = 0
            for cell in row:
                if i == 50:
                    break
                if count>0:
                    vals = cell.value
                    if vals in [None,'None',False]:
                        vals = ''
                    temp.append(str(vals))
                i +=1
            if count > 0:
                total_lst.append(temp)
                records.append(dict(zip(keys, temp)))
            else:
                count += 1
        return records,total_lst

    @api.multi
    def _read_excel(self, options):
        """ Returns a excel-parsed iterator of all empty lines in the file
            :throws csv.Error: if an error is detected during excel parsing
            :throws UnicodeDecodeError: if ``options.encoding`` is incorrect
        """
        exl_data = self.import_file
        reader_info = []
        try:
            decoded_data = base64.b64decode(exl_data)
            xls_filelike = io.BytesIO(decoded_data)
            workbook = openpyxl.load_workbook(xls_filelike)
            ws = workbook.active
            return ws
        except Exception as e:
            raise UserError(_("Unable to read the Excel Sheet !: %s") % (e))


    def _find_headers(self, sheet):
        """ Attempt to find the header in the excel sheet and return a list
        of headers
        """
        headers = [c.value for c in list(sheet.rows)[0] if c.value]
        return headers

    def _validate_header(self,exl_dt):        
        exl_hdr = self._find_headers(exl_dt)
        match_header = self._match_headers(exl_hdr,attendance_data)
        if match_header:
            raise UserError(_("Unable to process the file due to Header mismatch %s") %(match_header))
        return exl_hdr

    @api.multi
    def create_attendance_data(self, csv_data):
        for data in csv_data:
            attendance_data = data
            date_time_format = '%Y-%m-%d %H:%M:%S'
            employee_id = self.env['hr.employee'].search([('employee_code', '=', data.get('employee_id'))])
            for employee in employee_id:
                if employee:
                    get_check_in = attendance_data.get('check_in').strip().replace('/','-')
                    get_check_out = attendance_data.get('check_out').strip().replace('/','-')
                    check_in = datetime.strptime(get_check_in,date_time_format)-timedelta(hours=5,minutes=30)
                    check_out = datetime.strptime(get_check_out,date_time_format)-timedelta(hours=5,minutes=30)
                    data.update({'employee_id': employee.id, 'check_in':check_in, 'check_out': check_out})
                    attandance_employee = self.env['hr.attendance'].create(data)
                    self.write({'state': 'confirm'})


    @api.multi
    def check_data(self, csv_data):
        home = expanduser("~")

        anb = home+'/import_new'

        if not os.path.exists(anb):
            os.makedirs(anb)
        anb = anb+'/'+'error_log_sample.xlsx'
        workbook = xlsxwriter.Workbook(anb)  # Create xlsxwriter Object
        worksheet = workbook.add_worksheet()
        new_header = []
        exl_dt = self._read_excel(options={})
        latest_exl_hdr = self._find_headers(exl_dt) # Header of the file.
        latest_exl_hdr.append('Reason') # Add Reason in Header List
        latest_exl_header = self._find_headers(exl_dt) # Header of the file.
        row = 0 
        col = 0
        for each_latest in latest_exl_hdr:
            worksheet.write(row, col, each_latest)
            col +=1
            tech_name = MappingDic.get(each_latest)
            if each_latest != 'Reason':
                new_header.append(tech_name)
        row = 1
        employee_code_dic={}
        make_dict ={}
        flag = False
        i = 0
        for data in csv_data:
            print('data+++++++++++++++++++++', data.get('check_out'))
            # try:
            lst = []
            attendance_data = data
            employee = self.env['hr.employee'].search([('employee_code', '=', data.get('employee_id'))])
            if not employee:
                reason = "Employee code is not matched"
                lst.append(reason)
            if data.get('check_in') in [False,None,'None','False','']:
                print("check_in+++++++++++++++++++++++++")
                reason = "Check in data is not defined"
                lst.append(reason)
            if data.get('check_out') in [False,None,'None','False','']:
                print("check_out+++++++++++++++++++++++++")
                reason = "Check out data is not defined"
                lst.append(reason)
            if data.get('employee_id') not in employee_code_dic:
                employee_code_dic[data.get('employee_id')] = {'check_in':[data.get('check_in')],
                'check_out':[data.get('check_out')], 'duplicate': False}
            else:
                employee_code_dic[data.get('employee_id')]['duplicate'] = True
            for k,v in employee_code_dic.items():
                if v['duplicate'] == True:
                    for vals in v['check_in']:
                        #if i > 0:
                        if data.get('check_in') == vals and data.get('employee_id') == k:
                            reason = "Check in date should not be same"
                            lst.append(reason)
            i = i+ 1
            print('lst++++++++++++++++++++', lst)
            if lst:
                flag = True
                reason = ', '.join(lst)
                self._fail_list_entry(row,new_header,data,worksheet,reason)
                row +=1
                continue
            else:
                date_time_format = '%Y-%m-%d %H:%M:%S'
                get_check_in = attendance_data.get('check_in').strip().replace('/','-')
                get_check_out = attendance_data.get('check_out').strip().replace('/','-')
                check_in = datetime.strptime(get_check_in,date_time_format)-timedelta(hours=5,minutes=30)
                print('check_in++++++++++++++++',check_in,"get_check_in", get_check_in)
                check_out = datetime.strptime(get_check_out,date_time_format)-timedelta(hours=5,minutes=30)
                # self._fail_list_entry(row,new_header,data,worksheet)
                # row +=1
                query = """
                        SELECT id from hr_attendance 
                        where employee_id = %s and 
                        check_in = '%s' and check_out = '%s'
                """%(employee.id, check_in, check_out)
                self._cr.execute(query)
                check_atend = self._cr.dictfetchall()
                print('check_atend++++++++++++++++',check_atend)
                if not check_atend:
                    values = {
                        'employee_id':employee.id,
                        'check_in':check_in,
                        'check_out':check_out,
                    }
                    attandance_employee = self.env['hr.attendance'].create(values)
                    
                    
            # except Exception as e:
            #     reason = str(e)
            #     self._fail_list_entry(row,new_header,data,worksheet,reason)
            #     row +=1
        workbook.close()
        read_dat = base64.b64encode(open(anb, "rb").read())
        # pppppppppppppp
        self.write({'state': 'confirm'})
        if flag == True:
            self.write({'error_file': read_dat, 'error_file_name': 'error_log_sample.xlsx'})
        




    def _match_headers(self, exl_header,header):

        """ Attempt to match the header of the excel sheet and 

        predefine header data

        """
        res = list(set(exl_header)^set(header))
        return res

    @api.multi
    def _read_csv(self, options):
        data = base64.b64decode(self.import_file)
        data = data.decode("utf-8")
        file_input = StringIO(data)
        file_input.seek(0)
        reader_info = []
        reader = csv.reader(file_input, delimiter=',')
        header_fix = []
        keys = attendance_data_key
        try:
            reader_info.extend(reader)
        except Exception:
            raise exceptions.Warning(_("Not a valid file!"))
        values = {}
        valslist = []
        for i in range(len(reader_info)):
            if i ==0:
                header_fix = reader_info[i]
            if header_fix:
                match_header = self._match_headers(header_fix,attendance_data)
                if match_header:
                    raise UserError(_("Unable to process the file due to Header mismatch %s") %(match_header))
            val = {}
            field = map(str, reader_info[i])
            values = dict(zip(keys, field))
            if values:
                if i == 0:
                    continue
                else:
                    valslist.append(values)
        if valslist:
            return valslist


    def _fail_list_entry(self,row,header,rec,worksheet,reason=False):

        """

        This method is used for create Failer record entry in a failed sheet.

        """

        col = 0

        row = row

        sequence_lst_data = []

        for single_header in header:

            single_header = str(single_header)

            sequence_lst_data.append(rec.get(single_header," "))

        if reason:

            sequence_lst_data.append(str(reason))
        for item in sequence_lst_data:

            if item in [None,'None',False,'']:

                item = ' '

            worksheet.write(row, col, item)

            col +=1
