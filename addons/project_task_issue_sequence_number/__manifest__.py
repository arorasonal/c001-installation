# -*- coding: utf-8 -*-

# Part of Probuse Consulting Service Pvt Ltd. See LICENSE file for full copyright and licensing details.

{
    'name': "Project Task Issue Unique Sequence Number",
    'license': 'Other proprietary',
    'price': 49.0,
    'currency': 'EUR',
    'license': 'Other proprietary',
    'summary': """This module will generate task and issue unique number for your projects.""",
    'description': """
This module will generate Task's and Issue Sequence's project wise Unique Number.
    It will be also generate Project Unique Number,
    If user want to set prefix manually it was be there,
    Task Unique sequence Number will generate by projects if there,
    Project has not be selected it's generate Task's Unique Number,
    Issue Unique sequence Number will generate by projects if there,
    Project has not be selected it's generate issue's Unique Number.
project issue unique serial number
issue unique serial number
issue number
task number
task unique number
issue unique number
project number
project issue serial number
unique number project task and issue
project task 
project issue
task numbering
issue numbering
unique numbering of task
unique numbering of issue
document number issue
document number task
sequence task
sequence issue
sequence project task
sequence project issue
project task sequence
project issue sequence
issue sequence
task sequence


""",
    'author': "Probuse Consulting Service Pvt. Ltd.",
    'website': "http://www.probuse.com",
    'support': 'contact@probuse.com',
    'images': ['static/description/img1.jpg'],
    'live_test_url': 'https://youtu.be/R2ePzoiRvbg',
    'version': '1.1',
    'category' : 'Project',
    'depends': [
                'project','project_issue',
                ],
    'data':[
        'data/project_sequence.xml',
        'views/project_view.xml',
        'views/project_task_view.xml',
        'views/project_issue_view.xml',
    ],
    'installable' : True,
    'application' : False,
}

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
