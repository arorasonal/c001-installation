# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

# from itertools import groupby
from odoo import api, fields, models,_


class EWayBillTrans(models.Model):
    _name = 'eway.bill.trans'

    name = fields.Char(string='Transportor Name', help="Transport Name")
    transportation_mode = fields.Selection([
        ('1', 'Road'),
        ('2', 'Rail'),
        ('3', 'Air'),
        ('4', 'Ship'),
    ], string="Transportation Mode", help="Mode of Transportation")
    transporter_id = fields.Char(string='Transporter Id')
    email = fields.Char('Email Id')
    Doc_num = fields.Char('Document Number')
    mobile_num = fields.Char('Mobile Number')
    doc_date = fields.Date("Document Date")
    street1 = fields.Char(string='Street 1')
    street2 = fields.Char(string='Street 2')
    state_id = fields.Many2one('res.country.state', 'State')
    country_id = fields.Many2one('res.country', 'Country')
    pincode = fields.Char('Pincode')
    place = fields.Char('Place')


class SaleOrder(models.Model):
    _inherit = "sale.order"

    source = fields.Many2one('res.country.state', 'Source')
    supply_type = fields.Selection([
        ('inward', 'Inward'),
        ('outward', 'Outward')
    ], string="Supply Type", help="Supply whethere it is Outward/Inward")
    transportation_mode = fields.Many2one('eway.bill.trans', 'Transportation Mode')
    distance = fields.Float('Distance(KM)')
    vehicle_no = fields.Char('Vehicle Number')
    destination = fields.Many2one('res.country.state', 'Destination')
    sub_suply = fields.Selection([
        ('1', 'Supply'),
        ('2', 'Import'),
        ('3', 'Export'),
        ('4', 'job_work'),
        ('5', 'For Own Use'),
        ('6', 'Job Work Returns'),
        ('7', 'Sales Return'),
        ('8', 'Others'),
        ('9', 'SKD/CKD'),
        ('10', 'Line Sales'),
        ('11', 'Recipient Not Known'),
        ('12', 'Exhibition Or Fairs'),
    ], string='Sub Supply Type')
    transporter = fields.Char('Transporter')
    transporter_id = fields.Char('Transporter Id')
    e_way_bill = fields.Char('E-Way Bill Number')
    generate_e_way_bill = fields.Boolean('Generate E-Way Bill')
