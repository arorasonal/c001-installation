# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

{
    'name': 'Indian GST E-Way Bill',
    'version': '1.1',
    'category': 'E-Way Bill',
    'summary': 'Indian GST E-Way Bill',
    'author': 'Apagen Solutions Pvt. Ltd.',
    'website': 'http://www.apagen.com',
    'description': """
        GST E-Way bill is an electronic way bill which is required for the movement of goods. It is applicable for any consignment value exceeding INR 50,000. The bill can be generated from the GSTN portal(E-Way Bill Portal) and every registered taxpayer must require this E-Way bill along with the goods transferred.
        Our application helps to generate E-Way Bill/Consolidated E-Way Bill/Vehicle-No Updation in JSON Format which helps to file E-Way Bill on GSTN Portal(E-Way Bill Portal)
    """,
    'depends': ['sale', 'account', 'crm', 'india_gst'],
    'data': [
        'wizard/vehical_no_update.xml',
        'views/eway_bill_transport.xml',
    ],
    'demo': [
        # 'data/sale_demo.xml',
        # 'data/product_product_demo.xml',
    ],
    'css': ['static/src/css/sale.css'],
    'installable': True,
    'auto_install': False,
}
