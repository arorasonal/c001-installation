# -*- coding: utf-8 -*-
from openerp import api, fields, models, _


class VehicalNumUpdation(models.TransientModel):
    _name = 'vehical.num.wizard'

    vehicle_no = fields.Char('Vehicle No')
    reason = fields.Char('Reason')
    remarks = fields.Text('Remarks')

    @api.multi
    def updateVehicleNo(self):
    	ir_model_data = self.env['ir.model.data']
        context = self._context or {}
        updated_rec = self.env['sale.order'].browse(
            context.get('active_ids', []))
        rec = updated_rec.write({'vehicle_no': self.vehicle_no})
        return True
         # wizard_form = self.env.ref('vehical_no_update.msg_vehical_num_wizard_form', False)
        # wizard_form = self.env['msg.vehical.num.wizard']
        # return {
        #     'name': _('Hi I am wizard, I am from python code'),
        #     'type': 'ir.actions.act_window',
        #     'res_model': 'msg.vehical.num.wizard',
        #     'res_id': self.id,
        #     'view_id': wizard_form,
        #     'view_mode': 'form',
        #     'target': 'new'
        # }


# class MsgVehicalNumUpdation(models.TransientModel):
#     _name = 'msg.vehical.num.wizard'

#     text = fields.Char('Test')

#     @api.multi
#     def msgupdateVehicleNo(self):
#         print"kkkkkkkkkkkk"
#         return True
