# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

{
    'name': 'CRM',
    'version': '1.0',
    'category': 'Sales',
    'sequence': 5,
    'summary': 'Leads, Opportunities, Activities Tracking',
    'description': """
""",
    'website': 'https://www.odoo.com/page/crm',
    'depends': [
        'crm'
    ],
    'data': [
    'view/crm_lead_view.xml'
        
    ],
    'demo': [
        
    ],
    'css': ['static/src/css/crm.css'],
    'installable': True,
    'application': True,
    'auto_install': False,
}
