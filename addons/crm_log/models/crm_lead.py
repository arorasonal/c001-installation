# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

import logging
from datetime import datetime, timedelta, date
from dateutil.relativedelta import relativedelta

from odoo import api, fields, models, tools, SUPERUSER_ID
from odoo import api, exceptions, fields, models
from odoo.tools.translate import _
from odoo.tools import email_re, email_split
from odoo.exceptions import UserError, ValidationError


from odoo.addons.base.res.res_partner import FormatAddress

_logger = logging.getLogger(__name__)


class Lead(FormatAddress, models.Model):

    _inherit = "crm.lead"


    name = fields.Char(track_visibility='onchange')
    lead_lost_reason = fields.Many2one(
        'crm.lost.reason', string='Reason for loss of Lead', 
        index=True, track_visibility='onchange',domain=[('show_lead', '=', True)])
    lead_lost_date = fields.Date('Lost Date',track_visibility='onchange')
    email_from = fields.Char(track_visibility='onchange')
    email_cc = fields.Text(track_visibility='onchange')
    description = fields.Text(track_visibility='onchange')
    # create_date = fields.Datetime('Create Date', readonly=True)
    # write_date = fields.Datetime('Update Date', readonly=True)
    contact_name = fields.Char(track_visibility='onchange')
    partner_name = fields.Char(track_visibility='onchange')
    priority = fields.Selection(track_visibility='onchange')
    date_closed = fields.Datetime(track_visibility='onchange')
    referred = fields.Char(track_visibility='onchange')

    # Only used for type opportunity
    probability = fields.Float(track_visibility='onchange')
    date_deadline = fields.Date(track_visibility='onchange')

    # CRM Actions
    next_activity_id = fields.Many2one(track_visibility='onchange')
    date_action = fields.Date(track_visibility='onchange')
    title_action = fields.Char(track_visibility='onchange')

    color = fields.Integer('Color Index', default=0)
    partner_address_name = fields.Char('Partner Contact Name', related='partner_id.name', readonly=True)
    partner_address_email = fields.Char('Partner Contact Email', related='partner_id.email', readonly=True)
    company_currency = fields.Many2one(string='Currency', related='company_id.currency_id', readonly=True, relation="res.currency")
    user_email = fields.Char(track_visibility='onchange')
    user_login = fields.Char(track_visibility='onchange')
    phone = fields.Char('Phone',track_visibility='onchange')
    mobile = fields.Char('Mobile',track_visibility='onchange')
    function = fields.Char(track_visibility='onchange')
    title = fields.Many2one(track_visibility='onchange')
    check_lead = fields.Boolean('Check Lead')

    @api.multi
    def write(self, vals):
        if 'stage_id' in vals and vals.get('stage_id') not in (None,False,''):
            new_stage_id = self.env["crm.stage"].browse(vals.get("stage_id"))
            if new_stage_id.dead_stage == True:
                print('vals.get')
                vals['lead_lost_date'] = date.today().strftime('%Y-%m-%d')
                if self.type == 'lead':
                    if not self.lead_lost_reason:
                        raise exceptions.Warning(_(u"""Please fill the Reason for Lost first."""))
                if self.type == 'opportunity':
                    if not self.lost_reason:
                        raise exceptions.Warning(_(u"""Please click on Mark Lost button."""))
            if new_stage_id.dead_stage == False:
                if self.type == 'lead':
                    if self.lead_lost_reason:
                        self.lead_lost_reason = ''
                if self.type == 'opportunity':
                    if self.lost_reason:
                        self.lost_reason = ''
        lead = super(Lead, self).write(vals)
        return lead



class CrmStage(models.Model):
    _inherit = "crm.stage"

    dead_stage = fields.Boolean('Check Dead Stage')

class LostReason(models.Model):
    _inherit = "crm.lost.reason"

    show_lead = fields.Boolean('Show Lead')
    show_opportunity = fields.Boolean('Show Opportunity')
    show_quotation = fields.Boolean('Show Quotation')


class CrmLeadLost(models.TransientModel):
    _inherit = 'crm.lead.lost'
    _description = 'Get Lost Reason'

    lost_reason_id = fields.Many2one('crm.lost.reason', 'Lost Reason', domain=[('show_opportunity', '=', True)])

class UtmMixin(models.AbstractModel):

    """Mixin class for objects which can be tracked by marketing. """
    _inherit = 'utm.mixin'

    campaign_id = fields.Many2one(track_visibility='onchange')
    source_id = fields.Many2one(track_visibility='onchange')
    medium_id = fields.Many2one(track_visibility='onchange')

class Lead2OpportunityPartner(models.TransientModel):

    _inherit = 'crm.lead2opportunity.partner'

    

    @api.multi
    def action_apply(self):
        leads = self.env['crm.lead'].browse(self._context.get('active_ids', []))
        """ Convert lead to opportunity or merge lead and opportunity and open
            the freshly created opportunity view.
        """
        leads.write({'check_lead': True})
        res = super(Lead2OpportunityPartner, self).action_apply()
        return res

# class Lead2OpportunityMassConvert(models.TransientModel):

#     _inherit = 'crm.lead2opportunity.partner.mass'

#     @api.multi
#     def mass_convert(self):
#         lead_selected = self.env['crm.lead'].browse(self._context.get('active_ids', []))
#         for lead in lead_selected:
#             lead.write({'check_lead': True})
#         res = super(Lead2OpportunityMassConvert, self).mass_convert()
#         return res
